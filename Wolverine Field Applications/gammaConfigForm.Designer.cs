﻿namespace Wolverine_Field_Applications
{
    partial class gammaConfigForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(gammaConfigForm));
            this.label1 = new System.Windows.Forms.Label();
            this.calibratorApiBox = new System.Windows.Forms.TextBox();
            this.setCalibratorApiButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.label1.Location = new System.Drawing.Point(17, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(147, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "CALIBRATOR API";
            // 
            // calibratorApiBox
            // 
            this.calibratorApiBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.calibratorApiBox.Location = new System.Drawing.Point(170, 42);
            this.calibratorApiBox.Name = "calibratorApiBox";
            this.calibratorApiBox.Size = new System.Drawing.Size(100, 26);
            this.calibratorApiBox.TabIndex = 1;
            // 
            // setCalibratorApiButton
            // 
            this.setCalibratorApiButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.setCalibratorApiButton.Location = new System.Drawing.Point(287, 42);
            this.setCalibratorApiButton.Name = "setCalibratorApiButton";
            this.setCalibratorApiButton.Size = new System.Drawing.Size(75, 26);
            this.setCalibratorApiButton.TabIndex = 2;
            this.setCalibratorApiButton.Text = "SET";
            this.setCalibratorApiButton.UseVisualStyleBackColor = true;
            this.setCalibratorApiButton.Click += new System.EventHandler(this.setCalibratorApiButton_Click);
            // 
            // gammaConfigForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(393, 111);
            this.Controls.Add(this.setCalibratorApiButton);
            this.Controls.Add(this.calibratorApiBox);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "gammaConfigForm";
            this.Text = "CONFIG";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox calibratorApiBox;
        private System.Windows.Forms.Button setCalibratorApiButton;
    }
}