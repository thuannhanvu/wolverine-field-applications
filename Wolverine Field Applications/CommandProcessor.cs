﻿//--------------------------------------|---------------------------------------
//                                PC PDD V1.0.0.0
//                                File: CommandProcessor.cs
//------------------------------------------------------------------------------
// Author: Thuan Vu
// Created on April 14, 2021
//--------------------------------------|---------------------------------------
//  This drawing/document is copyright and the property of
//  Wolverine OilField Technologies LLC. and Remote Measurement Systems Ltd.
//  It must not be copied (in whole or in part),
//  used for manufacture or otherwise disclosed without prior written consent.
//  Any  copies of this drawing/document made by any method must also
//  include a copy of this legend. This document is supplied  without
//  liability for errors or omissions.
//
// (c) COPYRIGHT WOLVERINE OILFIELD TECHNOLOGIES LLC. AND
//               REMOTE MEASUREMENT SYSTEMS LIMITED 2021.
//-----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;

namespace PC_PDD
{
    internal class CommandProcessor
    {
        /// <summary>
        ///     Build a message without paramaters
        /// </summary>
        /// <param name="nodeID"> Node address </param>
        /// <param name="cmd"> Command to send </param>
        /// <returns>Byte array[] reply message</returns>
        public byte[] buildMessage(byte nodeID, byte cmd)
        {
            var lCommand = new List<byte>();

            var countLSB = Common.MESSAGE_OVERHEAD;
            byte countMSB = 0;

            lCommand.Add(nodeID); // TO: node
            lCommand.Add(0x01); // FROM: node (PC node address = 01)
            lCommand.Add(countLSB); // message byte count LSB
            lCommand.Add(countMSB); // message byte count MSB 
            lCommand.Add(cmd); // Command

            // Add checksum
            byte checkSum = 0;
            for (var i = 0; i < lCommand.Count; i++) checkSum += lCommand[i];
            lCommand.Add(checkSum);

            return lCommand.ToArray();
        }

        /// <summary>
        ///     Build a command message with a one byte parameter
        /// </summary>
        /// <param name="nodeID">Node address</param>
        /// <param name="cmd">Command to be sent</param>
        /// <param name="param">Paramater byte</param>
        /// <returns>Byte array[] reply message</returns>
        public byte[] buildMessage(byte nodeID, byte cmd, byte param)
        {
            var lCommand = new List<byte>();

            byte countLSB = Common.MESSAGE_OVERHEAD + 1;
            byte countMSB = 0;

            lCommand.Add(nodeID); // TO: node
            lCommand.Add(0x01); // FROM: node (PC node address = 01)
            lCommand.Add(countLSB); // message byte count LSB
            lCommand.Add(countMSB); // message byte count MSB 
            lCommand.Add(cmd); // Command
            lCommand.Add(param); // One byte parameter (Mode or Action)

            // Add checksum
            byte checkSum = 0;
            for (var i = 0; i < lCommand.Count; i++) checkSum += lCommand[i];
            lCommand.Add(checkSum);

            return lCommand.ToArray();
        }

        /// <summary>
        ///     Build a message with a data payload
        /// </summary>
        /// <param name="nodeID">Node address</param>
        /// <param name="cmd">Command to send</param>
        /// <param name="payload">
        ///     Data payload List<></param>
        /// <returns>Byte array[] reply message</returns>
        public byte[] buildMessage(byte nodeID, byte cmd, List<byte> payload)
        {
            var lCommand = new List<byte>();

            var len = (byte)payload.Count;
            var length = new byte[2];
            var sLength = (short)(Common.MESSAGE_OVERHEAD + len);

            length[0] = (byte)sLength; // Message length LSB
            length[1] = (byte)(sLength >> 8); // Message lenght MSB

            lCommand.Add(nodeID); // TO: node
            lCommand.Add(0x01); // FROM: node (PC node address = 01)
            lCommand.Add(length[0]); // LSB
            lCommand.Add(length[1]); // MSB 
            lCommand.Add(cmd); // Command
            lCommand.AddRange(payload); // Add data payload

            // Add checksum
            byte checkSum = 0;
            for (var i = 0; i < lCommand.Count; i++) checkSum += lCommand[i];
            lCommand.Add(checkSum);

            return lCommand.ToArray();
        }

        /// <summary>
        /// Build a message with a parameter and data payload
        /// </summary>
        /// <param name="nodeID"></param>
        /// <param name="cmd"></param>
        /// <param name="param"></param>
        /// <param name="payload"></param>
        /// <returns></returns>
        public byte[] buildMessage(byte nodeID, byte cmd, byte param, List<byte> payload)
        {
            List<byte> lCommand = new List<byte>();

            //byte len = (byte)payload.Count;
            UInt16 len = (UInt16)payload.Count;
            len += 1;
            byte[] length = new byte[2];
            short sLength = (short)(Common.MESSAGE_OVERHEAD + len);

            length[0] = (byte)sLength;          // Message length LSB
            length[1] = (byte)(sLength >> 8);   // Message lenght MSB

            lCommand.Add(nodeID);       // TO: node
            lCommand.Add(0x01);         // FROM: node (PC node address = 01)
            lCommand.Add(length[0]);    // LSB
            lCommand.Add(length[1]);    // MSB 
            lCommand.Add(cmd);          // Command
            lCommand.Add(param);        // Param
            lCommand.AddRange(payload); // Add data payload

            // Add checksum
            byte checkSum = 0;
            for (int i = 0; i < lCommand.Count; i++) checkSum += lCommand[i];
            lCommand.Add(checkSum);

            return lCommand.ToArray();
        }

        /// <summary>
        /// Build a message with a parameter and byte payload
        /// </summary>
        /// <param name="nodeID"></param>
        /// <param name="cmd"></param>
        /// <param name="param"></param>
        /// <param name="payload"></param>
        /// <returns></returns>
        public byte[] buildMessage(byte nodeID, byte cmd, byte param, byte payload)
        {
            List<byte> lCommand = new List<byte>();

            //byte len = (byte)payload.Count;
            UInt16 len = 1;
            len += 1;
            byte[] length = new byte[2];
            short sLength = (short)(Common.MESSAGE_OVERHEAD + len);

            length[0] = (byte)sLength;          // Message length LSB
            length[1] = (byte)(sLength >> 8);   // Message lenght MSB

            lCommand.Add(nodeID);       // TO: node
            lCommand.Add(0x01);         // FROM: node (PC node address = 01)
            lCommand.Add(length[0]);    // LSB
            lCommand.Add(length[1]);    // MSB 
            lCommand.Add(cmd);          // Command
            lCommand.Add(param);        // Param
            lCommand.Add(payload); // Add data payload

            // Add checksum
            byte checkSum = 0;
            for (int i = 0; i < lCommand.Count; i++) checkSum += lCommand[i];
            lCommand.Add(checkSum);

            return lCommand.ToArray();
        }

        /// <summary>
        ///     Write change mode
        /// </summary>
        /// <param name="nodeID">Node address</param>
        /// <param name="mode">Mode desired</param>
        /// <returns>Byte array[] reply message</returns>
        public byte[] writeMode(byte nodeID, short mode)
        {
            var payload = new List<byte>();
            var bytes = new byte[2];

            bytes[0] = (byte)mode;
            bytes[1] = (byte)(mode >> 8);

            payload.Add(bytes[0]); // mode LSB
            payload.Add(bytes[1]); // mode MSB

            return buildMessage(nodeID, Common.CMD_MODE_RDWR, payload);
        }

        public byte[] writeModeNDMaster(short mode)
        {
            var payload = Convert.ToByte(mode);

            return buildMessage(Common.NODE_NDMASTER, Common.CMD_MODE_RDWR, payload);
        }

        /// <summary>
        ///     Read firmware ID
        /// </summary>
        /// <param name="nodeID">Node address</param>
        /// <returns>Firmware ID byte[]</returns>
        public byte[] readFirmwareID(byte nodeID)
        {
            return buildMessage(nodeID, Common.CMD_FIRMWARE_ID_RD);
        }

        /// <summary>
        /// Write the serial number
        /// </summary>
        /// <param name="nodeID">Node address</param>
        /// <param name="sn">Serial number bytes</byte></param>
        /// <returns></returns>
        public byte[] writeSerialNumber(byte nodeID, List<byte> sn)
        {
            return buildMessage(nodeID, Common.CMD_SERIALNUMBER_RDWR, sn);
        }

        /// <summary>
        /// Read the tool configuration file
        /// </summary>
        /// <param name="nodeID"></param>
        /// <returns></returns>
        public byte[] readToolConfig(byte nodeID)
        {
            return buildMessage(nodeID, Common.CMD_NONVOLATILE_RDWR, Common.PDD_NONVOLATILE_CONFIGURATION);
        }

        /// <summary>
        /// Write the configuration paramaters
        /// </summary>
        /// <param name="nodeID">Node Address</param>
        /// <param name="payload">List of bytes containing configuration parameters</param>
        /// <returns></returns>
        public byte[] writeToolConfig(byte nodeID, List<byte> payload)
        {
            return buildMessage(nodeID, Common.CMD_NONVOLATILE_RDWR, Common.PDD_NONVOLATILE_CONFIGURATION, payload);
        }

        /// <summary>
        /// Write the calibration paramaters
        /// </summary>
        /// <param name="nodeID">Node Address</param>
        /// <param name="payload">List of bytes containing configuration parameters</param>
        /// <returns></returns>
        public byte[] writeToolCalib(byte nodeID, List<byte> payload)
        {
            return buildMessage(nodeID, Common.CMD_NONVOLATILE_RDWR, Common.PDD_NONVOLATILE_CALIBRATION, payload);
        }

        /// <summary>
        ///     Read serial number
        /// </summary>
        /// <param name="nodeID">Node address</param>
        /// <returns>Serial number byte[]</returns>
        public byte[] readSerialNumber(byte nodeID)
        {
            return buildMessage(nodeID, Common.CMD_SERIALNUMBER_RDWR);
        }

        /// <summary>
        ///     Read the system record (Note: This is a node common so the same data structure applies to any node)
        /// </summary>
        /// <param name="nodeID">Node address</param>
        /// <returns>Byte array[] reply message</returns>
        public byte[] readSystemRecord(byte nodeID)
        {
            return buildMessage(nodeID, Common.CMD_SYSTEM_RDWR);
        }

        /// <summary>
        ///     Read memmory
        /// </summary>
        /// <param name="nodeID">Node address</param>
        /// <param name="pageNum">Page number</param>
        /// <param name="paragraph">Paragraph number</param>
        /// <returns></returns>
        public byte[] readMemory(byte nodeID, uint pageNum, byte paragraph)
        {
            var payload = new List<byte>();
            var bytes = HexEncoding.GetBytes(pageNum, true);

            for (int i = 0; i < bytes.Length; i++)
                payload.Add(bytes[i]);

            payload.Add(paragraph);

            return buildMessage(nodeID, Common.CMD_MEMORY_RDWR, payload);
        }

        /// <summary>
        ///     Write memmory
        /// </summary>
        /// <param name="nodeID">Node address</param>
        /// <param name="pageNum">Page number</param>
        /// <param name="paragraph">Paragraph number</param>
        /// <returns></returns>
        public byte[] writeMemory(byte nodeID, uint pageNum, byte paragraph, byte testByte)
        {
            var payload = new List<byte>();
            var bytes = HexEncoding.GetBytes(pageNum, true);

            for (int i = 0; i < bytes.Length; i++)
                payload.Add(bytes[i]);

            //payload.Add(paragraph);

            for (int i = 0; i < 264; i++)
                payload.Add(testByte);

            return buildMessage(nodeID, Common.CMD_MEMORY_RDWR, payload);
        }

        public byte[] readMemoryND(byte nodeID, uint pageNum, byte paragraph)
        {
            var payload = new List<byte>();
            var bytes = HexEncoding.GetBytes(pageNum, true);

            for (int i = 0; i < bytes.Length; i++)
                payload.Add(bytes[i]);

            payload.Add(paragraph);

            return buildMessage(nodeID, Common.CMD_MEMORY_RDWR, payload);
        }

        public byte[] broadcastWriteMode(short mode)
        {
            var payload = new List<byte>();
            var bytes = new byte[2];

            bytes[0] = (byte)mode;
            bytes[1] = (byte)(mode >> 8);

            payload.Add(bytes[0]); // mode LSB
            payload.Add(bytes[1]); // mode MSB
            payload.Add(0); // Add two more null bytes 12-20-18 RP
            payload.Add(0);

            return buildMessage(Common.BROADCAST, Common.CMD_MODE_RDWR, payload);
        }

        private enum comStatus
        {
            Good,
            Bad
        }

        internal byte[] buildMessage(byte nodeID, byte cMD_ACTION, int nDCC_ACTION_SET_RPM, byte[] v)
        {
            throw new NotImplementedException();
        }
    }
}