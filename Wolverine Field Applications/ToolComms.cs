﻿//--------------------------------------|---------------------------------------
//                                PC PDD V1.0.0.0
//                                File: ToolComms.cs
//------------------------------------------------------------------------------
// Author: Thuan Vu
// Created on April 14, 2021
//--------------------------------------|---------------------------------------
//  This drawing/document is copyright and the property of
//  Wolverine OilField Technologies LLC. and Remote Measurement Systems Ltd.
//  It must not be copied (in whole or in part),
//  used for manufacture or otherwise disclosed without prior written consent.
//  Any  copies of this drawing/document made by any method must also
//  include a copy of this legend. This document is supplied  without
//  liability for errors or omissions.
//
// (c) COPYRIGHT WOLVERINE OILFIELD TECHNOLOGIES LLC. AND
//               REMOTE MEASUREMENT SYSTEMS LIMITED 2021.
//-----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PC_PDD
{
    public class ToolComms
    {
        /// <summary>
        /// Contains all of the received bytes 
        /// </summary>
        public List<byte> rxBuffer
        {
            get
            {
                return this.RxBuffer;
            }
        }
        /// <summary>
        /// Contains the recieved data minus the echo
        /// </summary>
        public List<byte> rxNoEcho
        {
            get
            {
                return this.RxNoEcho;
            }
        }
        /// <summary>
        /// Contains only the data payload of the received data
        /// </summary>
        public List<byte> rxData
        {
            get
            {
                return this.RxData;
            }
        }
        public List<byte> rxStreamBuffer
        {
            get
            {
                return this.streamBuffer;
            }
        }
        public bool rxFlagTimeout
        {
            get
            {
                return flagTimeout;
            }
        }
        public void setFlagTimeout(bool status)
        {
            flagTimeout = status;
        }

        private List<byte> RxBuffer = new List<byte>(); // Contains all of the received bytes 
        private List<byte> RxNoEcho = new List<byte>(); // Contains the recieved data minus the echo
        private List<byte> RxData = new List<byte>();   // Contains only the data payload of the received data

        private List<byte> streamBuffer = new List<byte>();

        //Debug stuff
        //byte[] buffer = new byte[512];
        //int bytes = 0;
        //bool msgLength = false;

        private int RxIndex;
        private SerialPort _serialPort;
        private static bool flagrxDataReady;
        private static bool flagTimeout;
        private static bool flagTransmit;
        private int timeoutMs = 50;
        Stopwatch msgReceiveTimer = new Stopwatch();
        byte[] buffer;

        // Asynchronious communuications timeout timer
        System.Timers.Timer asyncTimer = new System.Timers.Timer(20);
        private int messageLength = 0;
        public bool flagRxDataReady;

        public ToolComms()
        {
            asyncTimer.Enabled = false;
            asyncTimer.Elapsed += new System.Timers.ElapsedEventHandler(asyncTimerEvent);
            asyncTimer.AutoReset = true;
            flagTransmit = false;
        }

        public async Task<bool> FTDI_SEND(byte[] message, FTD2XX_NET.FTDI myFtdiDevice, FTD2XX_NET.FTDI.FT_STATUS ftStatus, Wolverine_Field_Applications.MainForm NT, bool echo)
        {
            try
            {
                RxData.Clear(); //Clear RxData Buffer
                RxBuffer.Clear(); //Clear RxBuffer
                RxNoEcho.Clear(); //Clear RxNoEcho
                UInt32 numBytesAvailable = 0;

                // Perform msg write from FTDI text box
                // Write bytes data to the device

                byte[] dataToWrite = message;

                messageLength = dataToWrite.Count();

                if (dataToWrite.Count() == 0)
                {
                    //FTDI_Text_Box.AppendText("Error, Nothing to Send! \n");
                    return false;
                }

                ftStatus = myFtdiDevice.GetRxBytesAvailable(ref numBytesAvailable);
                if (numBytesAvailable != 0)
                {
                    myFtdiDevice.Purge(1);
                    myFtdiDevice.Purge(2);
                }

                UInt32 numBytesWritten = 0;
                // Note that the Write method is overloaded, so can write string or byte array data
                ftStatus = myFtdiDevice.Write(dataToWrite, dataToWrite.Length, ref numBytesWritten);

                ////Download page increase speed testing
                //if (dataToWrite[(int)Common.COM_PROTOCOL.MSG_CMD] == Common.CMD_MEMORY_RDWR)
                //{
                //    byte[] readData1 = new byte[4096];
                //    UInt32 numBytesRead1 = 0;

                //    while (RxBuffer.Count() < (Common.MFPWR_MEMORY_PAGE_SIZE + Common.MESSAGE_OVERHEAD))
                //    {
                //        myFtdiDevice.GetRxBytesAvailable(ref numBytesAvailable);

                //        myFtdiDevice.Read(readData1, numBytesAvailable, ref numBytesRead1);

                //        RxBuffer.AddRange(readData1.ToList().GetRange(0, Convert.ToInt32(numBytesRead1)));
                //    }
                //    RxData.AddRange(RxBuffer.Skip(Common.MESSAGE_OVERHEAD));

                //    if (echo)
                //        RxNoEcho = new List<byte>(RxBuffer.Skip(Common.MESSAGE_OVERHEAD));
                //    else
                //        RxNoEcho = RxBuffer;

                //    myFtdiDevice.Purge(1); // Clean up the com port buffer
                //    flagRxDataReady = true;

                //    myFtdiDevice.Purge(2); // Clean up the com port buffer
                //    RxIndex = 0; // And reset the index for the next batch of data
                //    return true;
                //}

                if (dataToWrite[(int)Common.COM_PROTOCOL.MSG_CMD] != Common.CMD_MEMORY_RDWR)
                {
                    // Are we transmitting a broadcast command? If so do not wait for a reply message 
                    if (dataToWrite[0] == Common.BROADCAST)
                    {
                        await Task.Delay(100);
                        NT.Log(Wolverine_Field_Applications.MainForm.LogMsgType0.Outgoing, "TX: " + BitConverter.ToString(message), 1);
                        flagRxDataReady = true;
                        return true;
                    }
                }

                if (dataToWrite[(int)Common.COM_PROTOCOL.MSG_CMD] == Common.CMD_MODE_RDWR)
                {
                    await Task.Delay(100);
                }

                // Check the amount of data available to read
                // In this case we know how much data we are expecting, 
                // so wait until we have all of the bytes we have sent.
                RxIndex = 0;
                // Now that we have the amount of data we want available, read it
                byte[] readData = new byte[4096];
                var RxByteCountExpected = 999;
                UInt32 numBytesRead = 0;
                int TxByteCountOffset = 2; // Get the Tx byte count LSB index
                int TxByteCount = Common.MESSAGE_OVERHEAD;
                int counter = 0, timeout = timeoutMs;

                // Obtain the number of bytes waiting in the port's buffer
                var bytes = numBytesAvailable;

                do
                {
                    if (counter > timeout)
                    {
                        NT.Log(Wolverine_Field_Applications.MainForm.LogMsgType0.Outgoing, "TX: " + BitConverter.ToString(message), 1);
                        NT.Log(Wolverine_Field_Applications.MainForm.LogMsgType0.Outgoing, "RX:\n", 1);
                        return false;
                    }

                    ftStatus = myFtdiDevice.GetRxBytesAvailable(ref numBytesAvailable);

                    // Note that the Read method is overloaded, so can read string or byte array data
                    ftStatus = myFtdiDevice.Read(readData, numBytesAvailable, ref numBytesRead);

                    // Add the incoming data to the RxBuffer 

                    for (var i = 0; i < numBytesRead; i++)
                    {
                        RxBuffer.Add(readData[i]);
                        RxIndex++;
                    }

                    if (RxIndex > Common.MESSAGE_OVERHEAD)
                    {
                        TxByteCountOffset = 2; // The LSB and MSB byte for the transmit byte count is located at buffer address starting at 2 
                        TxByteCount = rxBuffer[TxByteCountOffset] + (rxBuffer[TxByteCountOffset + 1] << 8);  // Add the LSB and MSB byte counts together to create a 16 bit count
                    }
                    if (echo)
                    {
                        // Wait until we get all of echoed transmit bytes and the first 4 bytes of the recieved message 
                        if (RxIndex > TxByteCount + 4) // If the current index is past the Tx message and we have access to the count LSB and MSB in the Rx message
                        {
                            int RxByteCountOffset = TxByteCount + 2; // The LSB and MSB byte for the receive byte count is located at buffer address starting at TxByteCount + 2
                            int RxByteCount = rxBuffer[RxByteCountOffset] + (rxBuffer[RxByteCountOffset + 1] << 8);  // Add the Rx LSB and MSB byte counts together to create a 16 bit count
                            RxByteCountExpected = TxByteCount + RxByteCount; // Add the Tx and Rx byte counts together for the total number of bytes to expect from the com port 

                            // If we have recieved all of the expected bytes
                            if (RxBuffer.Count >= RxByteCountExpected)
                            {
                                // Remove the echo portion of the received message and save to rxNoEcho 
                                int indexOffset = RxBuffer[2] + RxBuffer[3]; // The message starts at this index location
                                for (int i = messageLength; i < RxBuffer.Count; i++)    // Data = count minus one so we dont include the checksum value.
                                {
                                    RxNoEcho.Add(RxBuffer[i]);
                                }

                                // Add the data portion only the rxData list which can be used later by a parser
                                indexOffset = (RxBuffer[2] * 2) - 1; // The data starts at this index location

                                if (indexOffset <= 0)
                                    return false;

                                // Add the data to the RxData list which can be used later by the parser
                                for (var i = indexOffset; i < RxBuffer.Count - 1; i++) // Data = count minus one so we dont include the checksum value.
                                    RxData.Add(RxBuffer[i]);

                                myFtdiDevice.Purge(1); // Clean up the com port buffer
                                flagRxDataReady = true;

                                myFtdiDevice.Purge(2); // Clean up the com port buffer
                                RxIndex = 0; // And reset the index for the next batch of data

                                if (dataToWrite[(int)Common.COM_PROTOCOL.MSG_CMD] != Common.CMD_MEMORY_RDWR)
                                {
                                    NT.Log(Wolverine_Field_Applications.MainForm.LogMsgType0.Outgoing, "TX: " + BitConverter.ToString(message), 1);
                                    NT.Log(Wolverine_Field_Applications.MainForm.LogMsgType0.Outgoing, "RX: " + BitConverter.ToString(rxNoEcho.ToArray()) + '\n', 1);
                                }

                                return true;
                            }
                        }
                    }
                    else
                    {
                        // Wait until we get all of echoed transmit bytes and the first 4 bytes of the recieved message 
                        if (RxIndex > Common.MESSAGE_OVERHEAD) // If the current index is past the Tx message and we have access to the count LSB and MSB in the Rx message
                        {
                            int RxByteCountOffset = 2; // The LSB and MSB byte for the receive byte count is located at buffer address starting at TxByteCount + 2
                            int RxByteCount = rxBuffer[RxByteCountOffset] + (rxBuffer[RxByteCountOffset + 1] << 8);  // Add the Rx LSB and MSB byte counts together to create a 16 bit count
                            RxByteCountExpected = RxByteCount; // Add the Tx and Rx byte counts together for the total number of bytes to expect from the com port 

                            // If we have recieved all of the expected bytes
                            if (RxBuffer.Count >= RxByteCountExpected)
                            {
                                //// Remove the echo portion of the received message and save to rxNoEcho 
                                //int indexOffset = RxBuffer[2] + RxBuffer[3]; // The message starts at this index location
                                for (int i = 0; i < RxBuffer.Count; i++)    // Data = count minus one so we dont include the checksum value.
                                {
                                    RxNoEcho.Add(RxBuffer[i]);
                                }

                                //// Add the data portion only the rxData list which can be used later by a parser
                                //indexOffset = (RxBuffer[2] * 2) - 1; // The data starts at this index location

                                //if (indexOffset <= 0)
                                //    return false;

                                // Add the data to the RxData list which can be used later by the parser
                                for (int i = Common.MESSAGE_OVERHEAD; i < RxBuffer.Count - 1; i++) // Data = count minus one so we dont include the checksum value.
                                    RxData.Add(RxBuffer[i]);

                                myFtdiDevice.Purge(1); // Clean up the com port buffer
                                flagRxDataReady = true;

                                myFtdiDevice.Purge(2); // Clean up the com port buffer
                                RxIndex = 0; // And reset the index for the next batch of data

                                if (dataToWrite[(int)Common.COM_PROTOCOL.MSG_CMD] != Common.CMD_MEMORY_RDWR)
                                {
                                    NT.Log(Wolverine_Field_Applications.MainForm.LogMsgType0.Outgoing, "TX: " + BitConverter.ToString(message), 1);
                                    NT.Log(Wolverine_Field_Applications.MainForm.LogMsgType0.Outgoing, "RX: " + BitConverter.ToString(rxNoEcho.ToArray()) + '\n', 1);
                                }

                                return true;
                            }
                        }
                    }

                    counter++;
                    await Task.Delay(1);
                } while (RxBuffer.Count < RxByteCountExpected);

                return false;
            }
            catch
            {
                //FTDI_Text_Box.AppendText("Error, exception : " + ex.ToString());
                return false;
            }
        }

        public async Task<bool> FTDI_SEND(byte[] message, FTD2XX_NET.FTDI myFtdiDevice, FTD2XX_NET.FTDI.FT_STATUS ftStatus, Wolverine_Field_Applications.MainForm NT)
        {
            try
            {
                RxData.Clear(); //Clear RxData Buffer
                RxBuffer.Clear(); //Clear RxBuffer
                RxNoEcho.Clear(); //Clear RxNoEcho
                UInt32 numBytesAvailable = 0;

                // Perform msg write from FTDI text box
                // Write bytes data to the device

                byte[] dataToWrite = message;

                messageLength = dataToWrite.Count();

                if (dataToWrite.Count() == 0)
                {
                    //FTDI_Text_Box.AppendText("Error, Nothing to Send! \n");
                    return false;
                }

                ftStatus = myFtdiDevice.GetRxBytesAvailable(ref numBytesAvailable);
                if (numBytesAvailable != 0)
                {
                    myFtdiDevice.Purge(1);
                    myFtdiDevice.Purge(2);
                }

                UInt32 numBytesWritten = 0;
                // Note that the Write method is overloaded, so can write string or byte array data
                ftStatus = myFtdiDevice.Write(dataToWrite, dataToWrite.Length, ref numBytesWritten);

                if (dataToWrite[(int)Common.COM_PROTOCOL.MSG_CMD] != Common.CMD_MEMORY_RDWR)

                    // Are we transmitting a broadcast command? If so do not wait for a reply message 
                    if (dataToWrite[0] == Common.BROADCAST)
                    {
                        await Task.Delay(100);
                        NT.Log(Wolverine_Field_Applications.MainForm.LogMsgType0.Outgoing, "RX:\n", 1);
                        flagRxDataReady = true;
                        return true;
                    }

                if (dataToWrite[(int)Common.COM_PROTOCOL.MSG_CMD] == Common.CMD_MODE_RDWR)
                {
                    await Task.Delay(100);
                }

                // Check the amount of data available to read
                // In this case we know how much data we are expecting, 
                // so wait until we have all of the bytes we have sent.
                RxIndex = 0;
                // Now that we have the amount of data we want available, read it
                byte[] readData = new byte[4096];
                var RxByteCountExpected = 999;
                UInt32 numBytesRead = 0;
                int TxByteCountOffset = 2; // Get the Tx byte count LSB index
                int TxByteCount = Common.MESSAGE_OVERHEAD;
                int counter = 0, timeout = timeoutMs;

                // Obtain the number of bytes waiting in the port's buffer
                var bytes = numBytesAvailable;

                do
                {
                    if (counter > timeout)
                    {
                        NT.Log(Wolverine_Field_Applications.MainForm.LogMsgType0.Outgoing, "TX: " + BitConverter.ToString(message), 1);
                        NT.Log(Wolverine_Field_Applications.MainForm.LogMsgType0.Outgoing, "RX:\n", 1);
                        return false;
                    }

                    ftStatus = myFtdiDevice.GetRxBytesAvailable(ref numBytesAvailable);

                    // Note that the Read method is overloaded, so can read string or byte array data
                    ftStatus = myFtdiDevice.Read(readData, numBytesAvailable, ref numBytesRead);

                    // Add the incoming data to the RxBuffer 

                    for (var i = 0; i < numBytesRead; i++)
                    {
                        RxBuffer.Add(readData[i]);
                        RxIndex++;
                    }

                    if (RxIndex > Common.MESSAGE_OVERHEAD)
                    {
                        TxByteCountOffset = 2; // The LSB and MSB byte for the transmit byte count is located at buffer address starting at 2 
                        TxByteCount = rxBuffer[TxByteCountOffset] + (rxBuffer[TxByteCountOffset + 1] << 8);  // Add the LSB and MSB byte counts together to create a 16 bit count
                    }

                    // Wait until we get all of echoed transmit bytes and the first 4 bytes of the recieved message 
                    if (RxIndex > TxByteCount + 4) // If the current index is past the Tx message and we have access to the count LSB and MSB in the Rx message
                    {
                        int RxByteCountOffset = TxByteCount + 2; // The LSB and MSB byte for the receive byte count is located at buffer address starting at TxByteCount + 2
                        int RxByteCount = rxBuffer[RxByteCountOffset] + (rxBuffer[RxByteCountOffset + 1] << 8);  // Add the Rx LSB and MSB byte counts together to create a 16 bit count
                        RxByteCountExpected = TxByteCount + RxByteCount; // Add the Tx and Rx byte counts together for the total number of bytes to expect from the com port 

                        // If we have recieved all of the expected bytes
                        if (RxBuffer.Count >= RxByteCountExpected)
                        {
                            // Remove the echo portion of the received message and save to rxNoEcho 
                            int indexOffset = RxBuffer[2] + RxBuffer[3]; // The message starts at this index location
                            for (int i = messageLength; i < RxBuffer.Count; i++)    // Data = count minus one so we dont include the checksum value.
                            {
                                RxNoEcho.Add(RxBuffer[i]);
                            }

                            // Add the data portion only the rxData list which can be used later by a parser
                            indexOffset = (RxBuffer[2] * 2) - 1; // The data starts at this index location

                            if (indexOffset <= 0)
                                return false;

                            // Add the data to the RxData list which can be used later by the parser
                            for (var i = indexOffset; i < RxBuffer.Count - 1; i++) // Data = count minus one so we dont include the checksum value.
                                RxData.Add(RxBuffer[i]);

                            myFtdiDevice.Purge(1); // Clean up the com port buffer
                            flagRxDataReady = true;

                            myFtdiDevice.Purge(2); // Clean up the com port buffer
                            RxIndex = 0; // And reset the index for the next batch of data

                            if (dataToWrite[(int)Common.COM_PROTOCOL.MSG_CMD] != Common.CMD_MEMORY_RDWR)
                            {
                                NT.Log(Wolverine_Field_Applications.MainForm.LogMsgType0.Outgoing, "TX: " + BitConverter.ToString(message), 1);
                                NT.Log(Wolverine_Field_Applications.MainForm.LogMsgType0.Outgoing, "RX: " + BitConverter.ToString(rxNoEcho.ToArray()) + '\n', 1);
                            }

                            return true;
                        }
                    }

                    counter++;
                    await Task.Delay(1);
                } while (RxBuffer.Count < RxByteCountExpected);

                return false;
            }
            catch
            {
                //FTDI_Text_Box.AppendText("Error, exception : " + ex.ToString());
                return false;
            }
        }

        public async Task<bool> sendCommand(byte[] message, SerialPort serialPort)
        {
            _serialPort = serialPort;
            _serialPort.DiscardInBuffer();
            _serialPort.DiscardOutBuffer();
            _serialPort.BaseStream.Flush();

            #region DataDLStream
            if ((message[(int)Common.COM_PROTOCOL.MSG_TO] == Common.NODE_NDMASTER) && (message[(int)Common.COM_PROTOCOL.MSG_CMD] == Common.CMD_ACTION))
            {
                // Clear some things before sending the message
                streamBuffer.Clear();
                RxIndex = 0;
                rxBuffer.Clear();
                rxNoEcho.Clear();
                rxData.Clear();
                flagrxDataReady = false;
                flagTimeout = false;
                flagTransmit = true;

                // Send the TX message
                _serialPort.Write(message, 0, message.Length);
                _serialPort.BaudRate = Common.BAUD_ND_DOWNLOAD;
                _serialPort.DataReceived += new SerialDataReceivedEventHandler(receivedDLStream);

                while (!flagrxDataReady)
                {
                    if (flagTimeout == true)    // If we are still waiting on the data ready flag and character timeout occurs, stop waiting
                    {
                        _serialPort.DataReceived -= new SerialDataReceivedEventHandler(receivedDLStream);

                        _serialPort.DiscardOutBuffer();
                        _serialPort.DiscardInBuffer();
                        _serialPort.BaseStream.Flush();

                        msgReceiveTimer.Stop();
                        msgReceiveTimer.Reset();
                        asyncTimer.Stop();
                        asyncTimer.Enabled = false;
                        flagTransmit = false;

                        serialPort.BaudRate = Common.BAUD_ND_APPLICATION;
                        return true;
                    }
                    else if (msgReceiveTimer.Elapsed.Milliseconds >= timeoutMs)
                    {
                        msgReceiveTimer.Stop();
                        flagTimeout = true;
                    }
                    else if (msgReceiveTimer.Elapsed.Milliseconds == 0)
                    {
                        msgReceiveTimer.Start();
                    }
                }

                _serialPort.DataReceived -= new SerialDataReceivedEventHandler(receivedDLStream);

                msgReceiveTimer.Stop();
                msgReceiveTimer.Reset();

                flagTransmit = false;
                flagTimeout = false;

                serialPort.BaudRate = Common.BAUD_ND_APPLICATION;
                return false;
            }
            #endregion

            //if (message[(int)Common.COM_PROTOCOL.MSG_CMD] == Common.CMD_MEMORY_RDWR)
            //{
            //    // When data is recieved through the port, call this method
            //    _serialPort.DataReceived += new SerialDataReceivedEventHandler(receivedMemPageData);
            //}
            //else
            //{
            //    // When data is recieved through the port, call this method
            //    _serialPort.DataReceived += new SerialDataReceivedEventHandler(receivedData);
            //}

            _serialPort.DataReceived += new SerialDataReceivedEventHandler(receivedData);
            messageLength = message.Length;

            // Clear some things before sending the message
            RxIndex = 0;
            rxBuffer.Clear();
            rxNoEcho.Clear();
            rxData.Clear();
            flagrxDataReady = false;
            flagTimeout = false;
            flagTransmit = true;

            // Send the TX message
            _serialPort.Write(message, 0, message.Length);
            //await Task.Delay(1);

            // Are we transmitting a broadcast command? If so do not wait for a reply message 
            if (message[(int)Common.COM_PROTOCOL.MSG_TO] == Common.BROADCAST)
            {
                flagrxDataReady = true;
                flagTransmit = false;

                _serialPort.DiscardOutBuffer();
                _serialPort.DiscardInBuffer();
                _serialPort.BaseStream.Flush();
                _serialPort.DataReceived -= new SerialDataReceivedEventHandler(receivedData);
                asyncTimer.Elapsed -= new System.Timers.ElapsedEventHandler(asyncTimerEvent);
                await Task.Delay(100);
                return true;
            }
            else if (message[(int)Common.COM_PROTOCOL.MSG_CMD] == Common.CMD_MODE_RDWR)
            {
                await Task.Delay(100);
            }

            int counter = 0;

            while (!flagrxDataReady)
            {
                await Task.Delay(1);
                counter++;
                if (counter > timeoutMs)
                {
                    if (message[(int)Common.COM_PROTOCOL.MSG_CMD] == Common.CMD_MEMORY_RDWR)
                    {
                        // When data is recieved through the port, call this method
                        _serialPort.DataReceived -= new SerialDataReceivedEventHandler(receivedMemPageData);
                    }
                    else
                    {
                        // When data is recieved through the port, call this method
                        _serialPort.DataReceived -= new SerialDataReceivedEventHandler(receivedData);
                    }
                    asyncTimer.Elapsed -= new System.Timers.ElapsedEventHandler(asyncTimerEvent);

                    _serialPort.DiscardOutBuffer();
                    _serialPort.DiscardInBuffer();
                    _serialPort.BaseStream.Flush();

                    msgReceiveTimer.Stop();
                    msgReceiveTimer.Reset();

                    asyncTimer.Stop();
                    asyncTimer.Enabled = false;

                    flagTransmit = false;
                    flagTimeout = false;
                    return false;
                }
            }
            msgReceiveTimer.Stop();
            msgReceiveTimer.Reset();
            asyncTimer.Stop();
            asyncTimer.Enabled = false;
            return true;
        }

        public bool sendCommandSynchronous(byte[] message, SerialPort serialPort)
        {
            _serialPort = serialPort;
            _serialPort.DiscardInBuffer();
            _serialPort.DiscardOutBuffer();
            _serialPort.BaseStream.Flush();

            // When data is recieved through the port, call this method
            _serialPort.DataReceived += new SerialDataReceivedEventHandler(receivedData);
            messageLength = message.Length;

            // Clear some things before sending the message
            RxIndex = 0;
            rxBuffer.Clear();
            rxNoEcho.Clear();
            rxData.Clear();
            flagrxDataReady = false;
            flagTimeout = false;

            // Send the TX message
            _serialPort.Write(message, 0, message.Length);
            Thread.Sleep(15);

            // Are we transmitting a broadcast command? If so do not wait for a reply message 
            if (message[0] == Common.BROADCAST)
            {
                flagrxDataReady = true;
                return true;
            }

            asyncTimer.Enabled = true;

            // Wait for the RX data ready flag to indicate we have received a reply message
            while (!flagrxDataReady)
            {
                if (flagTimeout == true)    // If we are still waiting on the data ready flag and time has expired, stop waiting
                {
                    flagTimeout = false;
                    _serialPort.DiscardOutBuffer();
                    _serialPort.DiscardInBuffer();
                    asyncTimer.Enabled = false;
                    flagTransmit = false;
                    return false;
                }
            }
            asyncTimer.Enabled = false;
            flagTransmit = true;
            return true;
        }

        public void receivedData(object sender, SerialDataReceivedEventArgs e)
        {
            if (!flagTransmit)
            {
                return;
            }

            int TxByteCountOffset = 2; // Get the Tx byte count LSB index
            int TxByteCount = Common.MESSAGE_OVERHEAD;

            try
            {
                //We've Received bytes, stop the 5mS timer
                asyncTimer.Stop();

                // Obtain the number of bytes waiting in the port's buffer
                int bytes = _serialPort.BytesToRead;

                // Create a local byte array buffer to hold the incoming data
                buffer = new byte[bytes];

                // Read the data from the port and store it in our local buffer
                _serialPort.Read(buffer, 0, bytes);

                // Add the incomming data to the rxBuffer 
                for (int i = 0; i < bytes; i++)
                {
                    rxBuffer.Add(buffer[i]);
                    RxIndex++;
                }

                if (RxIndex > Common.MESSAGE_OVERHEAD)
                {
                    TxByteCountOffset = 2; // The LSB and MSB byte for the transmit byte count is located at buffer address starting at 2 
                    TxByteCount = rxBuffer[TxByteCountOffset] + (rxBuffer[TxByteCountOffset + 1] << 8);  // Add the LSB and MSB byte counts together to create a 16 bit count
                }

                // Wait until we get all of echoed transmit bytes and the first 4 bytes of the recieved message 
                if (RxIndex > TxByteCount + 4) // If the current index is past the Tx message and we have access to the count LSB and MSB in the Rx message
                {
                    int RxByteCountOffset = TxByteCount + 2; // The LSB and MSB byte for the receive byte count is located at buffer address starting at TxByteCount + 2
                    int RxByteCount = rxBuffer[RxByteCountOffset] + (rxBuffer[RxByteCountOffset + 1] << 8);  // Add the Rx LSB and MSB byte counts together to create a 16 bit count
                    int RxByteCountExpected = TxByteCount + RxByteCount; // Add the Tx and Rx byte counts together for the total number of bytes to expect from the com port 

                    // If we have recieved all of the expected bytes
                    if (rxBuffer.Count >= RxByteCountExpected)
                    {

                        // Remove the echo portion of the received message and save to rxNoEcho 
                        int indexOffset = rxBuffer[2] + rxBuffer[3]; // The message starts at this index location
                        for (int i = messageLength; i < rxBuffer.Count; i++)    // Data = count minus one so we dont include the checksum value.
                        {
                            rxNoEcho.Add(rxBuffer[i]);
                        }

                        // Add the data portion only the rxData list which can be used later by a parser
                        indexOffset = (rxBuffer[2] * 2) - 1; // The data starts at this index location

                        if (indexOffset <= 0)
                            return;

                        for (int i = indexOffset; i < rxBuffer.Count - 1; i++)    // Data = count minus one so we dont include the checksum value.
                        {
                            rxData.Add(rxBuffer[i]);
                        }

                        _serialPort.DataReceived -= new SerialDataReceivedEventHandler(receivedData);

                        _serialPort.DiscardInBuffer();
                        _serialPort.DiscardOutBuffer();
                        _serialPort.BaseStream.Flush();

                        RxIndex = 0; // And reset the index for the next batch of data
                        flagrxDataReady = true;
                        flagTransmit = false;
                        return;
                    }
                }

                //We're done processing the received bytes, restart 5ms Timer
                msgReceiveTimer.Reset();
                msgReceiveTimer.Start();
                asyncTimer.Start();
            }
            catch (Exception ex)
            {
                ErrorDisplay ED = new ErrorDisplay();
                ED.StartPosition = FormStartPosition.CenterScreen;
                ED.setMessage(ex.Message);
                ED.ShowDialog();

                _serialPort.DataReceived -= new SerialDataReceivedEventHandler(receivedData);
                return;
            }
        }

        public void receivedMemPageData(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                // Create a local byte array buffer to hold the incoming data
                buffer = new byte[_serialPort.BytesToRead];

                // Read the data from the port and store it in our local buffer
                _serialPort.Read(buffer, 0, _serialPort.BytesToRead);

                rxBuffer.AddRange(buffer);

                if (rxBuffer.Count >= (270 + messageLength))
                {
                    rxNoEcho.AddRange(rxBuffer.Skip(messageLength));
                    _serialPort.DataReceived -= new SerialDataReceivedEventHandler(receivedMemPageData);
                    _serialPort.DiscardInBuffer();
                    _serialPort.DiscardOutBuffer();
                    _serialPort.BaseStream.Flush();

                    RxIndex = 0; // And reset the index for the next batch of data
                    flagrxDataReady = true;
                    flagTransmit = false;
                    return;
                }

                return;
            }
            catch
            {
                //SerialPort.DataReceived -= new SerialDataReceivedEventHandler(receivedMemPageData);
                return;
            }
        }

        public async void receivedDataFullDuplex(object sender, SerialDataReceivedEventArgs e)
        {
            if (!_serialPort.IsOpen)
            {
                return;
            }

            if (!flagTransmit)
            {
                return;
            }

            int TxByteCountOffset = 2; // Get the Tx byte count LSB index
            int TxByteCount = Common.MESSAGE_OVERHEAD;

            try
            {
                asyncTimer.Stop();
                await Task.Delay(5);
                // Obtain the number of bytes waiting in the port's buffer
                int bytes = _serialPort.BytesToRead;

                // Create a local byte array buffer to hold the incoming data
                byte[] buffer = new byte[bytes];

                // Read the data from the port and store it in our local buffer
                _serialPort.Read(buffer, 0, bytes);

                // Add the incomming data to the rxBuffer 
                for (int i = 0; i < bytes; i++)
                {
                    rxBuffer.Add(buffer[i]);
                    RxIndex++;
                }

                if (bytes > Common.MESSAGE_OVERHEAD)
                {
                    TxByteCountOffset = 2; // The LSB and MSB byte for the transmit byte count is located at buffer address starting at 2 
                    TxByteCount = rxBuffer[TxByteCountOffset] + (rxBuffer[TxByteCountOffset + 1] << 8);  // Add the LSB and MSB byte counts together to create a 16 bit count
                }

                // Wait until we get all of echoed transmit bytes and the first 4 bytes of the recieved message 
                if (RxIndex == TxByteCount) // If the current index is past the Tx message and we have access to the count LSB and MSB in the Rx message
                {

                    for (int i = 0; i < rxBuffer.Count; i++)    // Data = count minus one so we dont include the checksum value.
                    {
                        //rxData.Add(rxBuffer[i]);
                        rxNoEcho.Add(rxBuffer[i]);
                    }

                    for (int i = Common.MESSAGE_OVERHEAD; i < rxBuffer.Count - 1; i++)
                    {
                        rxData.Add(rxBuffer[i]);
                    }

                    _serialPort.DiscardInBuffer();
                    _serialPort.DiscardOutBuffer();
                    _serialPort.BaseStream.Flush();
                    RxIndex = 0; // And reset the index for the next batch of data
                    flagrxDataReady = true;
                    flagTransmit = false;
                    _serialPort.DataReceived -= new SerialDataReceivedEventHandler(receivedDataFullDuplex);
                    return;
                }
                asyncTimer.Start();
            }
            catch
            {
                //ErrorDisplay ED = new ErrorDisplay();
                //ED.StartPosition = FormStartPosition.CenterScreen;
                //ED.Show();

                //ED.setMessage(ex.Message);
                _serialPort.DiscardInBuffer();
                _serialPort.DiscardOutBuffer();
                _serialPort.BaseStream.Flush();
                RxIndex = 0; // And reset the index for the next batch of data
                flagrxDataReady = false;
                flagTransmit = false;
                _serialPort.DataReceived -= new SerialDataReceivedEventHandler(receivedDataFullDuplex);
                return;
            }
        }

        private static void asyncTimerEvent(Object source, System.Timers.ElapsedEventArgs e)
        {
            flagTimeout = true;
        }

        public void receivedDLStream(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                // Obtain the number of bytes waiting in the port's buffer
                int bytes = _serialPort.BytesToRead;

                // Create a local byte array buffer to hold the incoming data
                byte[] buffer = new byte[bytes];

                // Read the data from the port and store it in our local buffer
                _serialPort.Read(buffer, 0, bytes);

                streamBuffer.AddRange(buffer);

                //for (int i = 0; i < bytes; i++)
                //{
                //    streamBuffer.Add(buffer[i]);
                //    //if (streamBuffer.Count > 4096)
                //    //    await Task.Delay(1);
                //}

                msgReceiveTimer.Reset();
                msgReceiveTimer.Start();
                return;
            }
            catch
            {
                //SerialPort.DataReceived -= new SerialDataReceivedEventHandler(receivedDataStream);
                return;
            }
        }
    }
}
