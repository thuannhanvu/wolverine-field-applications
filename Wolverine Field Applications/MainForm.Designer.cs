﻿
namespace Wolverine_Field_Applications
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.mainTabControl = new System.Windows.Forms.TabControl();
            this.batCalcTab = new System.Windows.Forms.TabPage();
            this.batteryCalcTabControl = new System.Windows.Forms.TabControl();
            this.mainTab = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.printButton = new System.Windows.Forms.Button();
            this.loadConfigButton = new System.Windows.Forms.Button();
            this.clearButton = new System.Windows.Forms.Button();
            this.processButton = new System.Windows.Forms.Button();
            this.pulserSettingGroupbox = new System.Windows.Forms.GroupBox();
            this.setROPButton = new System.Windows.Forms.Button();
            this.setBatteryButton = new System.Windows.Forms.Button();
            this.setPwButton = new System.Windows.Forms.Button();
            this.totalLifeTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.numberOfBatteriesTextBox = new System.Windows.Forms.MaskedTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.resistivitySampleRateComboBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.ropTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tdTmpComboBox = new System.Windows.Forms.ComboBox();
            this.smartSolenoidCheckBox = new System.Windows.Forms.CheckBox();
            this.pulseWidthTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.telemetryGroupBox = new System.Windows.Forms.GroupBox();
            this.distanceBetweenDataRotateBox = new System.Windows.Forms.TextBox();
            this.joulesPerPulseRotateBox = new System.Windows.Forms.TextBox();
            this.pulsesPerHourRotateBox = new System.Windows.Forms.TextBox();
            this.numberOfPulsesRotateBox = new System.Windows.Forms.TextBox();
            this.bitRateRotateBox = new System.Windows.Forms.TextBox();
            this.rotateComboBox = new System.Windows.Forms.ComboBox();
            this.toolfaceComboBox = new System.Windows.Forms.ComboBox();
            this.surveyComboBox = new System.Windows.Forms.ComboBox();
            this.timeSequenceRotateBox = new System.Windows.Forms.TextBox();
            this.seventeenBitRotateItem = new System.Windows.Forms.TextBox();
            this.sixteenBitRotateItem = new System.Windows.Forms.TextBox();
            this.fifteenBitRotateItem = new System.Windows.Forms.TextBox();
            this.fourteenBitRotateItem = new System.Windows.Forms.TextBox();
            this.thirteenBitRotateItem = new System.Windows.Forms.TextBox();
            this.twelveBitRotateItem = new System.Windows.Forms.TextBox();
            this.elevenBitRotateItem = new System.Windows.Forms.TextBox();
            this.tenBitRotateItem = new System.Windows.Forms.TextBox();
            this.nineBitRotateItem = new System.Windows.Forms.TextBox();
            this.eightBitRotateItem = new System.Windows.Forms.TextBox();
            this.sevenBitRotateItem = new System.Windows.Forms.TextBox();
            this.sixBitRotateItem = new System.Windows.Forms.TextBox();
            this.fiveBitRotateItem = new System.Windows.Forms.TextBox();
            this.fourBitRotateItem = new System.Windows.Forms.TextBox();
            this.threeBitRotateItem = new System.Windows.Forms.TextBox();
            this.twoBitRotateItem = new System.Windows.Forms.TextBox();
            this.oneBitRotateItem = new System.Windows.Forms.TextBox();
            this.distanceBetweenDataToolfaceBox = new System.Windows.Forms.TextBox();
            this.joulesPerPulseToolfaceBox = new System.Windows.Forms.TextBox();
            this.pulsesPerHourToolfaceBox = new System.Windows.Forms.TextBox();
            this.numberOfPulsesToolfaceBox = new System.Windows.Forms.TextBox();
            this.numberOfPulsesSurveyBox = new System.Windows.Forms.TextBox();
            this.bitRateToolfaceBox = new System.Windows.Forms.TextBox();
            this.bitRateSurveyBox = new System.Windows.Forms.TextBox();
            this.timeSequenceToolfaceBox = new System.Windows.Forms.TextBox();
            this.timeSequenceSurveyBox = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.seventeenBitToolfaceItem = new System.Windows.Forms.TextBox();
            this.sixteenBitToolfaceItem = new System.Windows.Forms.TextBox();
            this.fifteenBitToolfaceItem = new System.Windows.Forms.TextBox();
            this.fourteenBitToolfaceItem = new System.Windows.Forms.TextBox();
            this.thirteenBitToolfaceItem = new System.Windows.Forms.TextBox();
            this.twelveBitToolfaceItem = new System.Windows.Forms.TextBox();
            this.elevenBitToolfaceItem = new System.Windows.Forms.TextBox();
            this.tenBitToolfaceItem = new System.Windows.Forms.TextBox();
            this.nineBitToolfaceItem = new System.Windows.Forms.TextBox();
            this.eightBitToolfaceItem = new System.Windows.Forms.TextBox();
            this.sevenBitToolfaceItem = new System.Windows.Forms.TextBox();
            this.sixBitToolfaceItem = new System.Windows.Forms.TextBox();
            this.fiveBitToolfaceItem = new System.Windows.Forms.TextBox();
            this.fourBitToolfaceItem = new System.Windows.Forms.TextBox();
            this.threeBitToolfaceItem = new System.Windows.Forms.TextBox();
            this.twoBitToolfaceItem = new System.Windows.Forms.TextBox();
            this.oneBitToolfaceItem = new System.Windows.Forms.TextBox();
            this.seventeenBitSurveyItem = new System.Windows.Forms.TextBox();
            this.sixteenBitSurveyItem = new System.Windows.Forms.TextBox();
            this.fifteenBitSurveyItem = new System.Windows.Forms.TextBox();
            this.fourteenBitSurveyItem = new System.Windows.Forms.TextBox();
            this.thirteenBitSurveyItem = new System.Windows.Forms.TextBox();
            this.twelveBitSurveyItem = new System.Windows.Forms.TextBox();
            this.elevenBitSurveyItem = new System.Windows.Forms.TextBox();
            this.tenBitSurveyItem = new System.Windows.Forms.TextBox();
            this.nineBitSurveyItem = new System.Windows.Forms.TextBox();
            this.eightBitSurveyItem = new System.Windows.Forms.TextBox();
            this.sevenBitSurveyItem = new System.Windows.Forms.TextBox();
            this.sixBitSurveyItem = new System.Windows.Forms.TextBox();
            this.fiveBitSurveyItem = new System.Windows.Forms.TextBox();
            this.fourBitSurveyItem = new System.Windows.Forms.TextBox();
            this.threeBitSurveyItem = new System.Windows.Forms.TextBox();
            this.twoBitSurveyItem = new System.Windows.Forms.TextBox();
            this.oneBitSurveyItem = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.energyConsumptionGroupBox = new System.Windows.Forms.GroupBox();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.toolGroupBox = new System.Windows.Forms.GroupBox();
            this.apsWpr = new System.Windows.Forms.CheckBox();
            this.wolverineLndcCheckbox = new System.Windows.Forms.CheckBox();
            this.wolverineMicrohopCheckbox = new System.Windows.Forms.CheckBox();
            this.rmsPressureToolCheckbox = new System.Windows.Forms.CheckBox();
            this.rmsResistivtyCheckbox = new System.Windows.Forms.CheckBox();
            this.wolverineTranslator = new System.Windows.Forms.CheckBox();
            this.gamma = new System.Windows.Forms.CheckBox();
            this.wolverineMasterWDirectional = new System.Windows.Forms.CheckBox();
            this.wolverineTmpSmartSolenoid = new System.Windows.Forms.CheckBox();
            this.powerConsumptionTab = new System.Windows.Forms.TabPage();
            this.toolTable = new System.Windows.Forms.DataGridView();
            this.toolColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.inStringColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.currentColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.resistivityPowerConsumptionTable = new System.Windows.Forms.DataGridView();
            this.powerConsumptionTable = new System.Windows.Forms.DataGridView();
            this.pulserDataTab = new System.Windows.Forms.TabPage();
            this.tnpTdOptionTable = new System.Windows.Forms.DataGridView();
            this.pulserConsumptionTable = new System.Windows.Forms.DataGridView();
            this.slotWidth200Table = new System.Windows.Forms.DataGridView();
            this.slotWidth0213Table = new System.Windows.Forms.DataGridView();
            this.slotWidth160Table = new System.Windows.Forms.DataGridView();
            this.orificeCalcTab = new System.Windows.Forms.TabPage();
            this.orificeCalcTabControl = new System.Windows.Forms.TabControl();
            this.orificeMainTab = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.pumpEfficiencyTextbox = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.mudWeightTextbox = new System.Windows.Forms.TextBox();
            this.orificeMudWeightUnitsBox = new System.Windows.Forms.ComboBox();
            this.label62 = new System.Windows.Forms.Label();
            this.orificeFlowRateUnitsBox = new System.Windows.Forms.ComboBox();
            this.orificeToolSizeBox = new System.Windows.Forms.ComboBox();
            this.label63 = new System.Windows.Forms.Label();
            this.orificeFlowRateTextbox = new System.Windows.Forms.TextBox();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.pressureAtSurfaceUnitComboBox = new System.Windows.Forms.ComboBox();
            this.depthForPressureBox = new System.Windows.Forms.TextBox();
            this.pressureAtSurfaceBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.depthTdComboBox = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.pressureAtSourceBox = new System.Windows.Forms.TextBox();
            this.pressureAtSourceUnitComboBox = new System.Windows.Forms.ComboBox();
            this.setButton = new System.Windows.Forms.Button();
            this.defaultButton = new System.Windows.Forms.Button();
            this.orificeAdminGroupBox = new System.Windows.Forms.GroupBox();
            this.modelTypicalTextbox = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.modelOrificesTextbox = new System.Windows.Forms.TextBox();
            this.roundTextBox = new System.Windows.Forms.TextBox();
            this.orificeTablesTab = new System.Windows.Forms.TabPage();
            this.atmGroupBox = new System.Windows.Forms.GroupBox();
            this.pulseAmplitudeAtmTable = new System.Windows.Forms.DataGridView();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.pulserSpecTable = new System.Windows.Forms.DataGridView();
            this.psiGroupBox = new System.Windows.Forms.GroupBox();
            this.pulseAmplitudePSITable = new System.Windows.Forms.DataGridView();
            this.rssCalcTab = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label61 = new System.Windows.Forms.Label();
            this.motorBypassPercentBox = new System.Windows.Forms.TextBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.fourThreeQuarterToolSizeBox = new System.Windows.Forms.CheckBox();
            this.rssResetButton = new System.Windows.Forms.Button();
            this.rssCalculateButton = new System.Windows.Forms.Button();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.tfaUnits = new System.Windows.Forms.TextBox();
            this.nozzleTFA = new System.Windows.Forms.TextBox();
            this.label59 = new System.Windows.Forms.Label();
            this.nozzle8Units = new System.Windows.Forms.TextBox();
            this.nozzle8Size = new System.Windows.Forms.TextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.nozzle7Units = new System.Windows.Forms.TextBox();
            this.nozzle7Size = new System.Windows.Forms.TextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.nozzle6Units = new System.Windows.Forms.TextBox();
            this.nozzle6Size = new System.Windows.Forms.TextBox();
            this.label56 = new System.Windows.Forms.Label();
            this.nozzle5Units = new System.Windows.Forms.TextBox();
            this.nozzle5Size = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.nozzle4Units = new System.Windows.Forms.TextBox();
            this.nozzle4Size = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.nozzle3Units = new System.Windows.Forms.TextBox();
            this.nozzle3Size = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.nozzle2Units = new System.Windows.Forms.TextBox();
            this.nozzle1Units = new System.Windows.Forms.ComboBox();
            this.nozzle2Size = new System.Windows.Forms.TextBox();
            this.nozzle1Size = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.pinRestrictTFAUnitsBox = new System.Windows.Forms.TextBox();
            this.pinRestrictNozzleUnit = new System.Windows.Forms.ComboBox();
            this.pinRestrictTFABox = new System.Windows.Forms.TextBox();
            this.pinRestrictNozzleSize = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.pressureDropUnits = new System.Windows.Forms.ComboBox();
            this.pressureDrop3Box = new System.Windows.Forms.TextBox();
            this.flowRateUnitBox = new System.Windows.Forms.ComboBox();
            this.mudWeightUnitsBox = new System.Windows.Forms.ComboBox();
            this.pressureDrop2Box = new System.Windows.Forms.TextBox();
            this.flowRateSection3 = new System.Windows.Forms.TextBox();
            this.flowRateSection2 = new System.Windows.Forms.TextBox();
            this.pressureDrop1Box = new System.Windows.Forms.TextBox();
            this.flowRateSection1 = new System.Windows.Forms.TextBox();
            this.mudWeightSection3 = new System.Windows.Forms.TextBox();
            this.label60 = new System.Windows.Forms.Label();
            this.mudWeightSection2 = new System.Windows.Forms.TextBox();
            this.mudWeightSection1 = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.sensorCalTab = new System.Windows.Forms.TabPage();
            this.readGammaButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.mwdBit15 = new System.Windows.Forms.Label();
            this.mwdBit14 = new System.Windows.Forms.Label();
            this.mwdBit13 = new System.Windows.Forms.Label();
            this.mwdBit12 = new System.Windows.Forms.Label();
            this.mwdBit11 = new System.Windows.Forms.Label();
            this.mwdBit10 = new System.Windows.Forms.Label();
            this.mwdBit9 = new System.Windows.Forms.Label();
            this.mwdBit8 = new System.Windows.Forms.Label();
            this.mwdBit7 = new System.Windows.Forms.Label();
            this.mwdBit6 = new System.Windows.Forms.Label();
            this.mwdBit5 = new System.Windows.Forms.Label();
            this.mwdBit4 = new System.Windows.Forms.Label();
            this.mwdBit3 = new System.Windows.Forms.Label();
            this.mwdBit2 = new System.Windows.Forms.Label();
            this.mwdBit0 = new System.Windows.Forms.Label();
            this.mwdBit1 = new System.Windows.Forms.Label();
            this.timerTextBox = new System.Windows.Forms.RichTextBox();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.ReceivingBox = new System.Windows.Forms.RichTextBox();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.dataBox = new System.Windows.Forms.RichTextBox();
            this.rmsBoxCheckBox = new System.Windows.Forms.CheckBox();
            this.qbusCheckBox = new System.Windows.Forms.CheckBox();
            this.alt_open_btn = new System.Windows.Forms.Button();
            this.label64 = new System.Windows.Forms.Label();
            this.comport = new System.Windows.Forms.ComboBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.gammaCalTab = new System.Windows.Forms.TabPage();
            this.configGammaButton = new System.Windows.Forms.Button();
            this.woftGammaCheckBox = new System.Windows.Forms.CheckBox();
            this.digitalGammaCheckbox = new System.Windows.Forms.CheckBox();
            this.gammaCalPanel = new System.Windows.Forms.Panel();
            this.serialNumberBox = new System.Windows.Forms.RichTextBox();
            this.label76 = new System.Windows.Forms.Label();
            this.apiCalibratorLabel = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.firstFiveMinGammaCpsBox = new System.Windows.Forms.RichTextBox();
            this.label74 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.netCalibratorCpsTextBox = new System.Windows.Forms.RichTextBox();
            this.gammaScalingFactorBox = new System.Windows.Forms.RichTextBox();
            this.label73 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.avgCalibratorCpsBox = new System.Windows.Forms.RichTextBox();
            this.backgroundCpsBox = new System.Windows.Forms.RichTextBox();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.calibratorLastFiveMinBox = new System.Windows.Forms.RichTextBox();
            this.saveButton = new System.Windows.Forms.Button();
            this.calibratorLastFiveMinButton = new System.Windows.Forms.Button();
            this.startBackGroundCpsButton = new System.Windows.Forms.Button();
            this.startCalFirst5MinButton = new System.Windows.Forms.Button();
            this.calculateGammaFactorButton = new System.Windows.Forms.Button();
            this.novDmCalTab = new System.Windows.Forms.TabPage();
            this.dmDataButton = new System.Windows.Forms.Button();
            this.rollTestButton = new System.Windows.Forms.Button();
            this.readKTableButton = new System.Windows.Forms.Button();
            this.takeSurveyButton = new System.Windows.Forms.Button();
            this.readEepromButton = new System.Windows.Forms.Button();
            this.connectButton = new System.Windows.Forms.Button();
            this.readDataButton = new System.Windows.Forms.Button();
            this.readPowerButton = new System.Windows.Forms.Button();
            this.runButton = new System.Windows.Forms.Button();
            this.bhaLengthBuilderTab = new System.Windows.Forms.TabPage();
            this.toolSizeComboBox = new System.Windows.Forms.ComboBox();
            this.label84 = new System.Windows.Forms.Label();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.mwdNmdcCalculateButton = new System.Windows.Forms.Button();
            this.calculateNmdcLengthBox = new System.Windows.Forms.TextBox();
            this.label107 = new System.Windows.Forms.Label();
            this.probeNotes20 = new System.Windows.Forms.TextBox();
            this.probeNotes19 = new System.Windows.Forms.TextBox();
            this.probeNotes18 = new System.Windows.Forms.TextBox();
            this.probeNotes17 = new System.Windows.Forms.TextBox();
            this.probeNotes16 = new System.Windows.Forms.TextBox();
            this.probeNotes15 = new System.Windows.Forms.TextBox();
            this.probeNotes14 = new System.Windows.Forms.TextBox();
            this.probeNotes13 = new System.Windows.Forms.TextBox();
            this.probeNotes12 = new System.Windows.Forms.TextBox();
            this.probeNotes11 = new System.Windows.Forms.TextBox();
            this.probeNotes10 = new System.Windows.Forms.TextBox();
            this.probeNotes9 = new System.Windows.Forms.TextBox();
            this.probeNotes8 = new System.Windows.Forms.TextBox();
            this.probeNotes7 = new System.Windows.Forms.TextBox();
            this.probeNotes6 = new System.Windows.Forms.TextBox();
            this.probeNotes5 = new System.Windows.Forms.TextBox();
            this.probeNotes4 = new System.Windows.Forms.TextBox();
            this.probeNotes3 = new System.Windows.Forms.TextBox();
            this.probeNotes2 = new System.Windows.Forms.TextBox();
            this.probeNotes1 = new System.Windows.Forms.TextBox();
            this.probeUnits20 = new System.Windows.Forms.ComboBox();
            this.probeUnits19 = new System.Windows.Forms.ComboBox();
            this.probeUnits18 = new System.Windows.Forms.ComboBox();
            this.probeUnits17 = new System.Windows.Forms.ComboBox();
            this.probeUnits16 = new System.Windows.Forms.ComboBox();
            this.probeUnits15 = new System.Windows.Forms.ComboBox();
            this.probeUnits14 = new System.Windows.Forms.ComboBox();
            this.probeUnits13 = new System.Windows.Forms.ComboBox();
            this.probeUnits12 = new System.Windows.Forms.ComboBox();
            this.probeUnits11 = new System.Windows.Forms.ComboBox();
            this.probeUnits10 = new System.Windows.Forms.ComboBox();
            this.probeUnits9 = new System.Windows.Forms.ComboBox();
            this.probeUnits8 = new System.Windows.Forms.ComboBox();
            this.probeUnits7 = new System.Windows.Forms.ComboBox();
            this.probeUnits6 = new System.Windows.Forms.ComboBox();
            this.probeUnits5 = new System.Windows.Forms.ComboBox();
            this.probeUnits4 = new System.Windows.Forms.ComboBox();
            this.probeUnits3 = new System.Windows.Forms.ComboBox();
            this.probeUnits2 = new System.Windows.Forms.ComboBox();
            this.probeUnits1 = new System.Windows.Forms.ComboBox();
            this.probeLength20 = new System.Windows.Forms.TextBox();
            this.probeLength19 = new System.Windows.Forms.TextBox();
            this.probeLength18 = new System.Windows.Forms.TextBox();
            this.probeLength17 = new System.Windows.Forms.TextBox();
            this.probeLength16 = new System.Windows.Forms.TextBox();
            this.probeLength15 = new System.Windows.Forms.TextBox();
            this.probeLength14 = new System.Windows.Forms.TextBox();
            this.probeLength13 = new System.Windows.Forms.TextBox();
            this.probeLength12 = new System.Windows.Forms.TextBox();
            this.probeLength11 = new System.Windows.Forms.TextBox();
            this.probeLength10 = new System.Windows.Forms.TextBox();
            this.probeLength9 = new System.Windows.Forms.TextBox();
            this.probeLength8 = new System.Windows.Forms.TextBox();
            this.probeLength7 = new System.Windows.Forms.TextBox();
            this.probeLength6 = new System.Windows.Forms.TextBox();
            this.probeLength5 = new System.Windows.Forms.TextBox();
            this.probeLength4 = new System.Windows.Forms.TextBox();
            this.probeLength3 = new System.Windows.Forms.TextBox();
            this.probeLength2 = new System.Windows.Forms.TextBox();
            this.probeLength1 = new System.Windows.Forms.TextBox();
            this.probeCombo20 = new System.Windows.Forms.ComboBox();
            this.probeCombo19 = new System.Windows.Forms.ComboBox();
            this.probeCombo18 = new System.Windows.Forms.ComboBox();
            this.probeCombo17 = new System.Windows.Forms.ComboBox();
            this.probeCombo16 = new System.Windows.Forms.ComboBox();
            this.probeCombo15 = new System.Windows.Forms.ComboBox();
            this.probeCombo14 = new System.Windows.Forms.ComboBox();
            this.probeCombo13 = new System.Windows.Forms.ComboBox();
            this.probeCombo12 = new System.Windows.Forms.ComboBox();
            this.probeCombo11 = new System.Windows.Forms.ComboBox();
            this.probeCombo10 = new System.Windows.Forms.ComboBox();
            this.probeCombo9 = new System.Windows.Forms.ComboBox();
            this.probeCombo8 = new System.Windows.Forms.ComboBox();
            this.probeCombo7 = new System.Windows.Forms.ComboBox();
            this.probeCombo6 = new System.Windows.Forms.ComboBox();
            this.probeCombo5 = new System.Windows.Forms.ComboBox();
            this.probeCombo4 = new System.Windows.Forms.ComboBox();
            this.probeCombo3 = new System.Windows.Forms.ComboBox();
            this.probeCombo2 = new System.Windows.Forms.ComboBox();
            this.probeCombo1 = new System.Windows.Forms.ComboBox();
            this.collarNotes20 = new System.Windows.Forms.TextBox();
            this.collarNotes19 = new System.Windows.Forms.TextBox();
            this.collarNotes18 = new System.Windows.Forms.TextBox();
            this.collarNotes17 = new System.Windows.Forms.TextBox();
            this.collarNotes16 = new System.Windows.Forms.TextBox();
            this.collarNotes15 = new System.Windows.Forms.TextBox();
            this.collarNotes14 = new System.Windows.Forms.TextBox();
            this.collarNotes13 = new System.Windows.Forms.TextBox();
            this.collarNotes12 = new System.Windows.Forms.TextBox();
            this.collarNotes11 = new System.Windows.Forms.TextBox();
            this.collarNotes10 = new System.Windows.Forms.TextBox();
            this.collarNotes9 = new System.Windows.Forms.TextBox();
            this.collarNotes8 = new System.Windows.Forms.TextBox();
            this.collarNotes7 = new System.Windows.Forms.TextBox();
            this.collarNotes6 = new System.Windows.Forms.TextBox();
            this.collarNotes5 = new System.Windows.Forms.TextBox();
            this.collarNotes4 = new System.Windows.Forms.TextBox();
            this.collarNotes3 = new System.Windows.Forms.TextBox();
            this.collarNotes2 = new System.Windows.Forms.TextBox();
            this.collarNotes1 = new System.Windows.Forms.TextBox();
            this.collarUnits20 = new System.Windows.Forms.ComboBox();
            this.collarUnits19 = new System.Windows.Forms.ComboBox();
            this.collarUnits18 = new System.Windows.Forms.ComboBox();
            this.collarUnits17 = new System.Windows.Forms.ComboBox();
            this.collarUnits16 = new System.Windows.Forms.ComboBox();
            this.collarUnits15 = new System.Windows.Forms.ComboBox();
            this.collarUnits14 = new System.Windows.Forms.ComboBox();
            this.collarUnits13 = new System.Windows.Forms.ComboBox();
            this.collarUnits12 = new System.Windows.Forms.ComboBox();
            this.collarUnits11 = new System.Windows.Forms.ComboBox();
            this.collarUnits10 = new System.Windows.Forms.ComboBox();
            this.collarUnits9 = new System.Windows.Forms.ComboBox();
            this.collarUnits8 = new System.Windows.Forms.ComboBox();
            this.collarUnits7 = new System.Windows.Forms.ComboBox();
            this.collarUnits6 = new System.Windows.Forms.ComboBox();
            this.collarUnits5 = new System.Windows.Forms.ComboBox();
            this.collarUnits4 = new System.Windows.Forms.ComboBox();
            this.collarUnits3 = new System.Windows.Forms.ComboBox();
            this.collarUnits2 = new System.Windows.Forms.ComboBox();
            this.collarUnits1 = new System.Windows.Forms.ComboBox();
            this.collarLength20 = new System.Windows.Forms.TextBox();
            this.collarLength19 = new System.Windows.Forms.TextBox();
            this.collarLength18 = new System.Windows.Forms.TextBox();
            this.collarLength17 = new System.Windows.Forms.TextBox();
            this.collarLength16 = new System.Windows.Forms.TextBox();
            this.collarLength15 = new System.Windows.Forms.TextBox();
            this.collarLength14 = new System.Windows.Forms.TextBox();
            this.collarLength13 = new System.Windows.Forms.TextBox();
            this.collarLength12 = new System.Windows.Forms.TextBox();
            this.collarLength11 = new System.Windows.Forms.TextBox();
            this.collarLength10 = new System.Windows.Forms.TextBox();
            this.collarLength9 = new System.Windows.Forms.TextBox();
            this.collarLength8 = new System.Windows.Forms.TextBox();
            this.collarLength7 = new System.Windows.Forms.TextBox();
            this.collarLength6 = new System.Windows.Forms.TextBox();
            this.collarLength5 = new System.Windows.Forms.TextBox();
            this.collarLength4 = new System.Windows.Forms.TextBox();
            this.collarLength3 = new System.Windows.Forms.TextBox();
            this.collarLength2 = new System.Windows.Forms.TextBox();
            this.collarLength1 = new System.Windows.Forms.TextBox();
            this.label106 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.bhaCombo20 = new System.Windows.Forms.ComboBox();
            this.bhaCombo19 = new System.Windows.Forms.ComboBox();
            this.bhaCombo18 = new System.Windows.Forms.ComboBox();
            this.bhaCombo17 = new System.Windows.Forms.ComboBox();
            this.bhaCombo16 = new System.Windows.Forms.ComboBox();
            this.bhaCombo15 = new System.Windows.Forms.ComboBox();
            this.bhaCombo14 = new System.Windows.Forms.ComboBox();
            this.bhaCombo13 = new System.Windows.Forms.ComboBox();
            this.bhaCombo12 = new System.Windows.Forms.ComboBox();
            this.bhaCombo11 = new System.Windows.Forms.ComboBox();
            this.bhaCombo10 = new System.Windows.Forms.ComboBox();
            this.bhaCombo9 = new System.Windows.Forms.ComboBox();
            this.bhaCombo8 = new System.Windows.Forms.ComboBox();
            this.bhaCombo7 = new System.Windows.Forms.ComboBox();
            this.bhaCombo6 = new System.Windows.Forms.ComboBox();
            this.bhaCombo5 = new System.Windows.Forms.ComboBox();
            this.bhaCombo4 = new System.Windows.Forms.ComboBox();
            this.bhaCombo3 = new System.Windows.Forms.ComboBox();
            this.bhaCombo2 = new System.Windows.Forms.ComboBox();
            this.bhaCombo1 = new System.Windows.Forms.ComboBox();
            this.label104 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.mainPanel = new System.Windows.Forms.Panel();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.comRefreshTimer = new System.Windows.Forms.Timer(this.components);
            this.gammaTimer = new System.Windows.Forms.Timer(this.components);
            this.StopwatchTimer = new System.Windows.Forms.Timer(this.components);
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.bhaLengthCalculateButton = new System.Windows.Forms.Button();
            this.bhaLengthBox = new System.Windows.Forms.TextBox();
            this.label108 = new System.Windows.Forms.Label();
            this.reloadTablesButton = new System.Windows.Forms.Button();
            this.mainTabControl.SuspendLayout();
            this.batCalcTab.SuspendLayout();
            this.batteryCalcTabControl.SuspendLayout();
            this.mainTab.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.pulserSettingGroupbox.SuspendLayout();
            this.telemetryGroupBox.SuspendLayout();
            this.energyConsumptionGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.toolGroupBox.SuspendLayout();
            this.powerConsumptionTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.toolTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resistivityPowerConsumptionTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.powerConsumptionTable)).BeginInit();
            this.pulserDataTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tnpTdOptionTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pulserConsumptionTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.slotWidth200Table)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.slotWidth0213Table)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.slotWidth160Table)).BeginInit();
            this.orificeCalcTab.SuspendLayout();
            this.orificeCalcTabControl.SuspendLayout();
            this.orificeMainTab.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.orificeAdminGroupBox.SuspendLayout();
            this.orificeTablesTab.SuspendLayout();
            this.atmGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pulseAmplitudeAtmTable)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pulserSpecTable)).BeginInit();
            this.psiGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pulseAmplitudePSITable)).BeginInit();
            this.rssCalcTab.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.sensorCalTab.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.gammaCalTab.SuspendLayout();
            this.gammaCalPanel.SuspendLayout();
            this.novDmCalTab.SuspendLayout();
            this.bhaLengthBuilderTab.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.mainPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainTabControl
            // 
            this.mainTabControl.Controls.Add(this.batCalcTab);
            this.mainTabControl.Controls.Add(this.orificeCalcTab);
            this.mainTabControl.Controls.Add(this.rssCalcTab);
            this.mainTabControl.Controls.Add(this.sensorCalTab);
            this.mainTabControl.Controls.Add(this.bhaLengthBuilderTab);
            this.mainTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainTabControl.Location = new System.Drawing.Point(0, 0);
            this.mainTabControl.Name = "mainTabControl";
            this.mainTabControl.SelectedIndex = 0;
            this.mainTabControl.Size = new System.Drawing.Size(1904, 1041);
            this.mainTabControl.TabIndex = 0;
            // 
            // batCalcTab
            // 
            this.batCalcTab.Controls.Add(this.batteryCalcTabControl);
            this.batCalcTab.Location = new System.Drawing.Point(4, 22);
            this.batCalcTab.Name = "batCalcTab";
            this.batCalcTab.Padding = new System.Windows.Forms.Padding(3);
            this.batCalcTab.Size = new System.Drawing.Size(1896, 1015);
            this.batCalcTab.TabIndex = 0;
            this.batCalcTab.Text = "BATTERY CALCULATOR";
            this.batCalcTab.UseVisualStyleBackColor = true;
            // 
            // batteryCalcTabControl
            // 
            this.batteryCalcTabControl.Controls.Add(this.mainTab);
            this.batteryCalcTabControl.Controls.Add(this.powerConsumptionTab);
            this.batteryCalcTabControl.Controls.Add(this.pulserDataTab);
            this.batteryCalcTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.batteryCalcTabControl.Location = new System.Drawing.Point(3, 3);
            this.batteryCalcTabControl.Name = "batteryCalcTabControl";
            this.batteryCalcTabControl.SelectedIndex = 0;
            this.batteryCalcTabControl.Size = new System.Drawing.Size(1890, 1009);
            this.batteryCalcTabControl.TabIndex = 8;
            // 
            // mainTab
            // 
            this.mainTab.Controls.Add(this.groupBox2);
            this.mainTab.Controls.Add(this.pulserSettingGroupbox);
            this.mainTab.Controls.Add(this.telemetryGroupBox);
            this.mainTab.Controls.Add(this.energyConsumptionGroupBox);
            this.mainTab.Controls.Add(this.toolGroupBox);
            this.mainTab.Location = new System.Drawing.Point(4, 22);
            this.mainTab.Name = "mainTab";
            this.mainTab.Padding = new System.Windows.Forms.Padding(3);
            this.mainTab.Size = new System.Drawing.Size(1882, 983);
            this.mainTab.TabIndex = 0;
            this.mainTab.Text = "MAIN";
            this.mainTab.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox2.Controls.Add(this.printButton);
            this.groupBox2.Controls.Add(this.loadConfigButton);
            this.groupBox2.Controls.Add(this.clearButton);
            this.groupBox2.Controls.Add(this.processButton);
            this.groupBox2.Location = new System.Drawing.Point(1486, 835);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(362, 136);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Processing";
            // 
            // printButton
            // 
            this.printButton.Location = new System.Drawing.Point(151, 95);
            this.printButton.Name = "printButton";
            this.printButton.Size = new System.Drawing.Size(75, 23);
            this.printButton.TabIndex = 102;
            this.printButton.Text = "Print";
            this.printButton.UseVisualStyleBackColor = true;
            this.printButton.Click += new System.EventHandler(this.printButton_Click);
            // 
            // loadConfigButton
            // 
            this.loadConfigButton.Location = new System.Drawing.Point(250, 52);
            this.loadConfigButton.Name = "loadConfigButton";
            this.loadConfigButton.Size = new System.Drawing.Size(75, 23);
            this.loadConfigButton.TabIndex = 2;
            this.loadConfigButton.Text = "Load";
            this.loadConfigButton.UseVisualStyleBackColor = true;
            this.loadConfigButton.Click += new System.EventHandler(this.loadConfigButton_Click);
            // 
            // clearButton
            // 
            this.clearButton.Location = new System.Drawing.Point(151, 52);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(75, 23);
            this.clearButton.TabIndex = 1;
            this.clearButton.Text = "Clear";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // processButton
            // 
            this.processButton.Location = new System.Drawing.Point(47, 52);
            this.processButton.Name = "processButton";
            this.processButton.Size = new System.Drawing.Size(75, 23);
            this.processButton.TabIndex = 0;
            this.processButton.Text = "Calculate";
            this.processButton.UseVisualStyleBackColor = true;
            this.processButton.Click += new System.EventHandler(this.processButton_Click);
            // 
            // pulserSettingGroupbox
            // 
            this.pulserSettingGroupbox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pulserSettingGroupbox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pulserSettingGroupbox.Controls.Add(this.setROPButton);
            this.pulserSettingGroupbox.Controls.Add(this.setBatteryButton);
            this.pulserSettingGroupbox.Controls.Add(this.setPwButton);
            this.pulserSettingGroupbox.Controls.Add(this.totalLifeTextBox);
            this.pulserSettingGroupbox.Controls.Add(this.label6);
            this.pulserSettingGroupbox.Controls.Add(this.numberOfBatteriesTextBox);
            this.pulserSettingGroupbox.Controls.Add(this.label5);
            this.pulserSettingGroupbox.Controls.Add(this.resistivitySampleRateComboBox);
            this.pulserSettingGroupbox.Controls.Add(this.label4);
            this.pulserSettingGroupbox.Controls.Add(this.ropTextBox);
            this.pulserSettingGroupbox.Controls.Add(this.label3);
            this.pulserSettingGroupbox.Controls.Add(this.label2);
            this.pulserSettingGroupbox.Controls.Add(this.tdTmpComboBox);
            this.pulserSettingGroupbox.Controls.Add(this.smartSolenoidCheckBox);
            this.pulserSettingGroupbox.Controls.Add(this.pulseWidthTextBox);
            this.pulserSettingGroupbox.Controls.Add(this.label1);
            this.pulserSettingGroupbox.Location = new System.Drawing.Point(1077, 734);
            this.pulserSettingGroupbox.Name = "pulserSettingGroupbox";
            this.pulserSettingGroupbox.Size = new System.Drawing.Size(333, 237);
            this.pulserSettingGroupbox.TabIndex = 1;
            this.pulserSettingGroupbox.TabStop = false;
            this.pulserSettingGroupbox.Text = "Settings";
            // 
            // setROPButton
            // 
            this.setROPButton.Location = new System.Drawing.Point(234, 100);
            this.setROPButton.Name = "setROPButton";
            this.setROPButton.Size = new System.Drawing.Size(75, 23);
            this.setROPButton.TabIndex = 102;
            this.setROPButton.Text = "Set";
            this.setROPButton.UseVisualStyleBackColor = true;
            this.setROPButton.Click += new System.EventHandler(this.setROPButton_Click);
            // 
            // setBatteryButton
            // 
            this.setBatteryButton.Location = new System.Drawing.Point(234, 153);
            this.setBatteryButton.Name = "setBatteryButton";
            this.setBatteryButton.Size = new System.Drawing.Size(75, 23);
            this.setBatteryButton.TabIndex = 14;
            this.setBatteryButton.Text = "Set";
            this.setBatteryButton.UseVisualStyleBackColor = true;
            this.setBatteryButton.Click += new System.EventHandler(this.setBatteryButton_Click);
            // 
            // setPwButton
            // 
            this.setPwButton.Location = new System.Drawing.Point(234, 47);
            this.setPwButton.Name = "setPwButton";
            this.setPwButton.Size = new System.Drawing.Size(75, 23);
            this.setPwButton.TabIndex = 13;
            this.setPwButton.Text = "Set";
            this.setPwButton.UseVisualStyleBackColor = true;
            this.setPwButton.Click += new System.EventHandler(this.setPwButton_Click);
            // 
            // totalLifeTextBox
            // 
            this.totalLifeTextBox.Location = new System.Drawing.Point(131, 181);
            this.totalLifeTextBox.Name = "totalLifeTextBox";
            this.totalLifeTextBox.Size = new System.Drawing.Size(81, 20);
            this.totalLifeTextBox.TabIndex = 12;
            this.totalLifeTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(37, 184);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(88, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Total Life (Hours)";
            // 
            // numberOfBatteriesTextBox
            // 
            this.numberOfBatteriesTextBox.Location = new System.Drawing.Point(131, 155);
            this.numberOfBatteriesTextBox.Name = "numberOfBatteriesTextBox";
            this.numberOfBatteriesTextBox.Size = new System.Drawing.Size(81, 20);
            this.numberOfBatteriesTextBox.TabIndex = 10;
            this.numberOfBatteriesTextBox.Text = "4";
            this.numberOfBatteriesTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numberOfBatteriesTextBox.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.numberOfBatteriesTextBox_MaskInputRejected);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 158);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Number of Batteries";
            // 
            // resistivitySampleRateComboBox
            // 
            this.resistivitySampleRateComboBox.FormattingEnabled = true;
            this.resistivitySampleRateComboBox.Items.AddRange(new object[] {
            "30 Sec",
            "20 Sec"});
            this.resistivitySampleRateComboBox.Location = new System.Drawing.Point(131, 128);
            this.resistivitySampleRateComboBox.Name = "resistivitySampleRateComboBox";
            this.resistivitySampleRateComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.resistivitySampleRateComboBox.Size = new System.Drawing.Size(81, 21);
            this.resistivitySampleRateComboBox.TabIndex = 8;
            this.resistivitySampleRateComboBox.SelectedIndexChanged += new System.EventHandler(this.resistivitySampleRateComboBox_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 131);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(118, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Resistivity Sample Rate";
            // 
            // ropTextBox
            // 
            this.ropTextBox.Location = new System.Drawing.Point(131, 102);
            this.ropTextBox.Name = "ropTextBox";
            this.ropTextBox.Size = new System.Drawing.Size(81, 20);
            this.ropTextBox.TabIndex = 6;
            this.ropTextBox.Text = "100";
            this.ropTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 105);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(109, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "ROP (Distance/Hour)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(74, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "TD, TNP";
            // 
            // tdTmpComboBox
            // 
            this.tdTmpComboBox.FormattingEnabled = true;
            this.tdTmpComboBox.Items.AddRange(new object[] {
            "1.50, 0.50",
            "1.00, 0.67",
            "1.50, 0.63"});
            this.tdTmpComboBox.Location = new System.Drawing.Point(131, 75);
            this.tdTmpComboBox.Name = "tdTmpComboBox";
            this.tdTmpComboBox.Size = new System.Drawing.Size(81, 21);
            this.tdTmpComboBox.TabIndex = 1;
            this.tdTmpComboBox.SelectedIndexChanged += new System.EventHandler(this.tdTmpComboBox_SelectedIndexChanged);
            // 
            // smartSolenoidCheckBox
            // 
            this.smartSolenoidCheckBox.AutoSize = true;
            this.smartSolenoidCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartSolenoidCheckBox.Checked = true;
            this.smartSolenoidCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.smartSolenoidCheckBox.Location = new System.Drawing.Point(224, 77);
            this.smartSolenoidCheckBox.Name = "smartSolenoidCheckBox";
            this.smartSolenoidCheckBox.Size = new System.Drawing.Size(97, 17);
            this.smartSolenoidCheckBox.TabIndex = 2;
            this.smartSolenoidCheckBox.Text = "Smart Solenoid";
            this.smartSolenoidCheckBox.UseVisualStyleBackColor = true;
            this.smartSolenoidCheckBox.CheckedChanged += new System.EventHandler(this.smartSolenoidCheckBox_CheckedChanged);
            // 
            // pulseWidthTextBox
            // 
            this.pulseWidthTextBox.Location = new System.Drawing.Point(131, 49);
            this.pulseWidthTextBox.Name = "pulseWidthTextBox";
            this.pulseWidthTextBox.Size = new System.Drawing.Size(81, 20);
            this.pulseWidthTextBox.TabIndex = 1;
            this.pulseWidthTextBox.Text = "0.32";
            this.pulseWidthTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(61, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Pulse Width";
            // 
            // telemetryGroupBox
            // 
            this.telemetryGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.telemetryGroupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.telemetryGroupBox.Controls.Add(this.distanceBetweenDataRotateBox);
            this.telemetryGroupBox.Controls.Add(this.joulesPerPulseRotateBox);
            this.telemetryGroupBox.Controls.Add(this.pulsesPerHourRotateBox);
            this.telemetryGroupBox.Controls.Add(this.numberOfPulsesRotateBox);
            this.telemetryGroupBox.Controls.Add(this.bitRateRotateBox);
            this.telemetryGroupBox.Controls.Add(this.rotateComboBox);
            this.telemetryGroupBox.Controls.Add(this.toolfaceComboBox);
            this.telemetryGroupBox.Controls.Add(this.surveyComboBox);
            this.telemetryGroupBox.Controls.Add(this.timeSequenceRotateBox);
            this.telemetryGroupBox.Controls.Add(this.seventeenBitRotateItem);
            this.telemetryGroupBox.Controls.Add(this.sixteenBitRotateItem);
            this.telemetryGroupBox.Controls.Add(this.fifteenBitRotateItem);
            this.telemetryGroupBox.Controls.Add(this.fourteenBitRotateItem);
            this.telemetryGroupBox.Controls.Add(this.thirteenBitRotateItem);
            this.telemetryGroupBox.Controls.Add(this.twelveBitRotateItem);
            this.telemetryGroupBox.Controls.Add(this.elevenBitRotateItem);
            this.telemetryGroupBox.Controls.Add(this.tenBitRotateItem);
            this.telemetryGroupBox.Controls.Add(this.nineBitRotateItem);
            this.telemetryGroupBox.Controls.Add(this.eightBitRotateItem);
            this.telemetryGroupBox.Controls.Add(this.sevenBitRotateItem);
            this.telemetryGroupBox.Controls.Add(this.sixBitRotateItem);
            this.telemetryGroupBox.Controls.Add(this.fiveBitRotateItem);
            this.telemetryGroupBox.Controls.Add(this.fourBitRotateItem);
            this.telemetryGroupBox.Controls.Add(this.threeBitRotateItem);
            this.telemetryGroupBox.Controls.Add(this.twoBitRotateItem);
            this.telemetryGroupBox.Controls.Add(this.oneBitRotateItem);
            this.telemetryGroupBox.Controls.Add(this.distanceBetweenDataToolfaceBox);
            this.telemetryGroupBox.Controls.Add(this.joulesPerPulseToolfaceBox);
            this.telemetryGroupBox.Controls.Add(this.pulsesPerHourToolfaceBox);
            this.telemetryGroupBox.Controls.Add(this.numberOfPulsesToolfaceBox);
            this.telemetryGroupBox.Controls.Add(this.numberOfPulsesSurveyBox);
            this.telemetryGroupBox.Controls.Add(this.bitRateToolfaceBox);
            this.telemetryGroupBox.Controls.Add(this.bitRateSurveyBox);
            this.telemetryGroupBox.Controls.Add(this.timeSequenceToolfaceBox);
            this.telemetryGroupBox.Controls.Add(this.timeSequenceSurveyBox);
            this.telemetryGroupBox.Controls.Add(this.label32);
            this.telemetryGroupBox.Controls.Add(this.label31);
            this.telemetryGroupBox.Controls.Add(this.label30);
            this.telemetryGroupBox.Controls.Add(this.label29);
            this.telemetryGroupBox.Controls.Add(this.label28);
            this.telemetryGroupBox.Controls.Add(this.label27);
            this.telemetryGroupBox.Controls.Add(this.seventeenBitToolfaceItem);
            this.telemetryGroupBox.Controls.Add(this.sixteenBitToolfaceItem);
            this.telemetryGroupBox.Controls.Add(this.fifteenBitToolfaceItem);
            this.telemetryGroupBox.Controls.Add(this.fourteenBitToolfaceItem);
            this.telemetryGroupBox.Controls.Add(this.thirteenBitToolfaceItem);
            this.telemetryGroupBox.Controls.Add(this.twelveBitToolfaceItem);
            this.telemetryGroupBox.Controls.Add(this.elevenBitToolfaceItem);
            this.telemetryGroupBox.Controls.Add(this.tenBitToolfaceItem);
            this.telemetryGroupBox.Controls.Add(this.nineBitToolfaceItem);
            this.telemetryGroupBox.Controls.Add(this.eightBitToolfaceItem);
            this.telemetryGroupBox.Controls.Add(this.sevenBitToolfaceItem);
            this.telemetryGroupBox.Controls.Add(this.sixBitToolfaceItem);
            this.telemetryGroupBox.Controls.Add(this.fiveBitToolfaceItem);
            this.telemetryGroupBox.Controls.Add(this.fourBitToolfaceItem);
            this.telemetryGroupBox.Controls.Add(this.threeBitToolfaceItem);
            this.telemetryGroupBox.Controls.Add(this.twoBitToolfaceItem);
            this.telemetryGroupBox.Controls.Add(this.oneBitToolfaceItem);
            this.telemetryGroupBox.Controls.Add(this.seventeenBitSurveyItem);
            this.telemetryGroupBox.Controls.Add(this.sixteenBitSurveyItem);
            this.telemetryGroupBox.Controls.Add(this.fifteenBitSurveyItem);
            this.telemetryGroupBox.Controls.Add(this.fourteenBitSurveyItem);
            this.telemetryGroupBox.Controls.Add(this.thirteenBitSurveyItem);
            this.telemetryGroupBox.Controls.Add(this.twelveBitSurveyItem);
            this.telemetryGroupBox.Controls.Add(this.elevenBitSurveyItem);
            this.telemetryGroupBox.Controls.Add(this.tenBitSurveyItem);
            this.telemetryGroupBox.Controls.Add(this.nineBitSurveyItem);
            this.telemetryGroupBox.Controls.Add(this.eightBitSurveyItem);
            this.telemetryGroupBox.Controls.Add(this.sevenBitSurveyItem);
            this.telemetryGroupBox.Controls.Add(this.sixBitSurveyItem);
            this.telemetryGroupBox.Controls.Add(this.fiveBitSurveyItem);
            this.telemetryGroupBox.Controls.Add(this.fourBitSurveyItem);
            this.telemetryGroupBox.Controls.Add(this.threeBitSurveyItem);
            this.telemetryGroupBox.Controls.Add(this.twoBitSurveyItem);
            this.telemetryGroupBox.Controls.Add(this.oneBitSurveyItem);
            this.telemetryGroupBox.Controls.Add(this.label26);
            this.telemetryGroupBox.Controls.Add(this.label25);
            this.telemetryGroupBox.Controls.Add(this.label24);
            this.telemetryGroupBox.Controls.Add(this.label23);
            this.telemetryGroupBox.Controls.Add(this.label7);
            this.telemetryGroupBox.Controls.Add(this.label10);
            this.telemetryGroupBox.Controls.Add(this.label16);
            this.telemetryGroupBox.Controls.Add(this.label22);
            this.telemetryGroupBox.Controls.Add(this.label17);
            this.telemetryGroupBox.Controls.Add(this.label11);
            this.telemetryGroupBox.Controls.Add(this.label15);
            this.telemetryGroupBox.Controls.Add(this.label21);
            this.telemetryGroupBox.Controls.Add(this.label18);
            this.telemetryGroupBox.Controls.Add(this.label12);
            this.telemetryGroupBox.Controls.Add(this.label14);
            this.telemetryGroupBox.Controls.Add(this.label20);
            this.telemetryGroupBox.Controls.Add(this.label19);
            this.telemetryGroupBox.Controls.Add(this.label13);
            this.telemetryGroupBox.Location = new System.Drawing.Point(1415, 6);
            this.telemetryGroupBox.Name = "telemetryGroupBox";
            this.telemetryGroupBox.Size = new System.Drawing.Size(460, 824);
            this.telemetryGroupBox.TabIndex = 15;
            this.telemetryGroupBox.TabStop = false;
            this.telemetryGroupBox.Text = "Telemetry";
            // 
            // distanceBetweenDataRotateBox
            // 
            this.distanceBetweenDataRotateBox.Location = new System.Drawing.Point(362, 711);
            this.distanceBetweenDataRotateBox.Name = "distanceBetweenDataRotateBox";
            this.distanceBetweenDataRotateBox.Size = new System.Drawing.Size(75, 20);
            this.distanceBetweenDataRotateBox.TabIndex = 128;
            this.distanceBetweenDataRotateBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // joulesPerPulseRotateBox
            // 
            this.joulesPerPulseRotateBox.Location = new System.Drawing.Point(362, 683);
            this.joulesPerPulseRotateBox.Name = "joulesPerPulseRotateBox";
            this.joulesPerPulseRotateBox.Size = new System.Drawing.Size(75, 20);
            this.joulesPerPulseRotateBox.TabIndex = 127;
            this.joulesPerPulseRotateBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pulsesPerHourRotateBox
            // 
            this.pulsesPerHourRotateBox.Location = new System.Drawing.Point(362, 651);
            this.pulsesPerHourRotateBox.Name = "pulsesPerHourRotateBox";
            this.pulsesPerHourRotateBox.Size = new System.Drawing.Size(75, 20);
            this.pulsesPerHourRotateBox.TabIndex = 126;
            this.pulsesPerHourRotateBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numberOfPulsesRotateBox
            // 
            this.numberOfPulsesRotateBox.Location = new System.Drawing.Point(362, 623);
            this.numberOfPulsesRotateBox.Name = "numberOfPulsesRotateBox";
            this.numberOfPulsesRotateBox.Size = new System.Drawing.Size(75, 20);
            this.numberOfPulsesRotateBox.TabIndex = 125;
            this.numberOfPulsesRotateBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // bitRateRotateBox
            // 
            this.bitRateRotateBox.Location = new System.Drawing.Point(362, 594);
            this.bitRateRotateBox.Name = "bitRateRotateBox";
            this.bitRateRotateBox.Size = new System.Drawing.Size(75, 20);
            this.bitRateRotateBox.TabIndex = 124;
            this.bitRateRotateBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // rotateComboBox
            // 
            this.rotateComboBox.AutoCompleteCustomSource.AddRange(new string[] {
            "SSEQ0",
            "SSEQ1",
            "SSEQ2",
            "SSEQ3"});
            this.rotateComboBox.FormattingEnabled = true;
            this.rotateComboBox.Items.AddRange(new object[] {
            "RSEQ1",
            "RSEQ2",
            "RSEQ3",
            "RSEQ4"});
            this.rotateComboBox.Location = new System.Drawing.Point(369, 8);
            this.rotateComboBox.Name = "rotateComboBox";
            this.rotateComboBox.Size = new System.Drawing.Size(59, 21);
            this.rotateComboBox.TabIndex = 123;
            this.rotateComboBox.Text = "RSEQ1";
            this.rotateComboBox.SelectedIndexChanged += new System.EventHandler(this.rotateComboBox_SelectedIndexChanged);
            // 
            // toolfaceComboBox
            // 
            this.toolfaceComboBox.AutoCompleteCustomSource.AddRange(new string[] {
            "SSEQ0",
            "SSEQ1",
            "SSEQ2",
            "SSEQ3"});
            this.toolfaceComboBox.FormattingEnabled = true;
            this.toolfaceComboBox.Items.AddRange(new object[] {
            "TFSEQ1",
            "TFSEQ2",
            "TFSEQ3",
            "TFSEQ4"});
            this.toolfaceComboBox.Location = new System.Drawing.Point(268, 8);
            this.toolfaceComboBox.Name = "toolfaceComboBox";
            this.toolfaceComboBox.Size = new System.Drawing.Size(69, 21);
            this.toolfaceComboBox.TabIndex = 122;
            this.toolfaceComboBox.Text = "TFSEQ1";
            this.toolfaceComboBox.SelectedIndexChanged += new System.EventHandler(this.toolfaceComboBox_SelectedIndexChanged);
            // 
            // surveyComboBox
            // 
            this.surveyComboBox.AutoCompleteCustomSource.AddRange(new string[] {
            "SSEQ0",
            "SSEQ1",
            "SSEQ2",
            "SSEQ3"});
            this.surveyComboBox.FormattingEnabled = true;
            this.surveyComboBox.Items.AddRange(new object[] {
            "SSEQ1",
            "SSEQ2",
            "SSEQ3",
            "SSEQ4"});
            this.surveyComboBox.Location = new System.Drawing.Point(178, 8);
            this.surveyComboBox.Name = "surveyComboBox";
            this.surveyComboBox.Size = new System.Drawing.Size(59, 21);
            this.surveyComboBox.TabIndex = 121;
            this.surveyComboBox.Text = "SSEQ1";
            this.surveyComboBox.SelectedIndexChanged += new System.EventHandler(this.surveyComboBox_SelectedIndexChanged);
            // 
            // timeSequenceRotateBox
            // 
            this.timeSequenceRotateBox.Location = new System.Drawing.Point(362, 560);
            this.timeSequenceRotateBox.Name = "timeSequenceRotateBox";
            this.timeSequenceRotateBox.Size = new System.Drawing.Size(75, 20);
            this.timeSequenceRotateBox.TabIndex = 120;
            this.timeSequenceRotateBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // seventeenBitRotateItem
            // 
            this.seventeenBitRotateItem.Location = new System.Drawing.Point(362, 529);
            this.seventeenBitRotateItem.Name = "seventeenBitRotateItem";
            this.seventeenBitRotateItem.Size = new System.Drawing.Size(75, 20);
            this.seventeenBitRotateItem.TabIndex = 119;
            this.seventeenBitRotateItem.Text = "0";
            this.seventeenBitRotateItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // sixteenBitRotateItem
            // 
            this.sixteenBitRotateItem.Location = new System.Drawing.Point(362, 495);
            this.sixteenBitRotateItem.Name = "sixteenBitRotateItem";
            this.sixteenBitRotateItem.Size = new System.Drawing.Size(75, 20);
            this.sixteenBitRotateItem.TabIndex = 118;
            this.sixteenBitRotateItem.Text = "0";
            this.sixteenBitRotateItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // fifteenBitRotateItem
            // 
            this.fifteenBitRotateItem.Location = new System.Drawing.Point(362, 462);
            this.fifteenBitRotateItem.Name = "fifteenBitRotateItem";
            this.fifteenBitRotateItem.Size = new System.Drawing.Size(75, 20);
            this.fifteenBitRotateItem.TabIndex = 117;
            this.fifteenBitRotateItem.Text = "0";
            this.fifteenBitRotateItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // fourteenBitRotateItem
            // 
            this.fourteenBitRotateItem.Location = new System.Drawing.Point(362, 430);
            this.fourteenBitRotateItem.Name = "fourteenBitRotateItem";
            this.fourteenBitRotateItem.Size = new System.Drawing.Size(75, 20);
            this.fourteenBitRotateItem.TabIndex = 116;
            this.fourteenBitRotateItem.Text = "0";
            this.fourteenBitRotateItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // thirteenBitRotateItem
            // 
            this.thirteenBitRotateItem.Location = new System.Drawing.Point(362, 400);
            this.thirteenBitRotateItem.Name = "thirteenBitRotateItem";
            this.thirteenBitRotateItem.Size = new System.Drawing.Size(75, 20);
            this.thirteenBitRotateItem.TabIndex = 115;
            this.thirteenBitRotateItem.Text = "0";
            this.thirteenBitRotateItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // twelveBitRotateItem
            // 
            this.twelveBitRotateItem.Location = new System.Drawing.Point(362, 368);
            this.twelveBitRotateItem.Name = "twelveBitRotateItem";
            this.twelveBitRotateItem.Size = new System.Drawing.Size(75, 20);
            this.twelveBitRotateItem.TabIndex = 114;
            this.twelveBitRotateItem.Text = "0";
            this.twelveBitRotateItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // elevenBitRotateItem
            // 
            this.elevenBitRotateItem.Location = new System.Drawing.Point(362, 337);
            this.elevenBitRotateItem.Name = "elevenBitRotateItem";
            this.elevenBitRotateItem.Size = new System.Drawing.Size(75, 20);
            this.elevenBitRotateItem.TabIndex = 113;
            this.elevenBitRotateItem.Text = "0";
            this.elevenBitRotateItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tenBitRotateItem
            // 
            this.tenBitRotateItem.Location = new System.Drawing.Point(362, 307);
            this.tenBitRotateItem.Name = "tenBitRotateItem";
            this.tenBitRotateItem.Size = new System.Drawing.Size(75, 20);
            this.tenBitRotateItem.TabIndex = 112;
            this.tenBitRotateItem.Text = "0";
            this.tenBitRotateItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // nineBitRotateItem
            // 
            this.nineBitRotateItem.Location = new System.Drawing.Point(362, 277);
            this.nineBitRotateItem.Name = "nineBitRotateItem";
            this.nineBitRotateItem.Size = new System.Drawing.Size(75, 20);
            this.nineBitRotateItem.TabIndex = 111;
            this.nineBitRotateItem.Text = "0";
            this.nineBitRotateItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // eightBitRotateItem
            // 
            this.eightBitRotateItem.Location = new System.Drawing.Point(362, 250);
            this.eightBitRotateItem.Name = "eightBitRotateItem";
            this.eightBitRotateItem.Size = new System.Drawing.Size(75, 20);
            this.eightBitRotateItem.TabIndex = 110;
            this.eightBitRotateItem.Text = "0";
            this.eightBitRotateItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // sevenBitRotateItem
            // 
            this.sevenBitRotateItem.Location = new System.Drawing.Point(362, 218);
            this.sevenBitRotateItem.Name = "sevenBitRotateItem";
            this.sevenBitRotateItem.Size = new System.Drawing.Size(75, 20);
            this.sevenBitRotateItem.TabIndex = 109;
            this.sevenBitRotateItem.Text = "0";
            this.sevenBitRotateItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // sixBitRotateItem
            // 
            this.sixBitRotateItem.Location = new System.Drawing.Point(362, 187);
            this.sixBitRotateItem.Name = "sixBitRotateItem";
            this.sixBitRotateItem.Size = new System.Drawing.Size(75, 20);
            this.sixBitRotateItem.TabIndex = 108;
            this.sixBitRotateItem.Text = "0";
            this.sixBitRotateItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // fiveBitRotateItem
            // 
            this.fiveBitRotateItem.Location = new System.Drawing.Point(362, 156);
            this.fiveBitRotateItem.Name = "fiveBitRotateItem";
            this.fiveBitRotateItem.Size = new System.Drawing.Size(75, 20);
            this.fiveBitRotateItem.TabIndex = 107;
            this.fiveBitRotateItem.Text = "0";
            this.fiveBitRotateItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // fourBitRotateItem
            // 
            this.fourBitRotateItem.Location = new System.Drawing.Point(362, 127);
            this.fourBitRotateItem.Name = "fourBitRotateItem";
            this.fourBitRotateItem.Size = new System.Drawing.Size(75, 20);
            this.fourBitRotateItem.TabIndex = 106;
            this.fourBitRotateItem.Text = "0";
            this.fourBitRotateItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // threeBitRotateItem
            // 
            this.threeBitRotateItem.Location = new System.Drawing.Point(362, 94);
            this.threeBitRotateItem.Name = "threeBitRotateItem";
            this.threeBitRotateItem.Size = new System.Drawing.Size(75, 20);
            this.threeBitRotateItem.TabIndex = 105;
            this.threeBitRotateItem.Text = "0";
            this.threeBitRotateItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // twoBitRotateItem
            // 
            this.twoBitRotateItem.Location = new System.Drawing.Point(362, 67);
            this.twoBitRotateItem.Name = "twoBitRotateItem";
            this.twoBitRotateItem.Size = new System.Drawing.Size(75, 20);
            this.twoBitRotateItem.TabIndex = 104;
            this.twoBitRotateItem.Text = "0";
            this.twoBitRotateItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // oneBitRotateItem
            // 
            this.oneBitRotateItem.Location = new System.Drawing.Point(362, 35);
            this.oneBitRotateItem.Name = "oneBitRotateItem";
            this.oneBitRotateItem.Size = new System.Drawing.Size(75, 20);
            this.oneBitRotateItem.TabIndex = 103;
            this.oneBitRotateItem.Text = "0";
            this.oneBitRotateItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // distanceBetweenDataToolfaceBox
            // 
            this.distanceBetweenDataToolfaceBox.Location = new System.Drawing.Point(264, 711);
            this.distanceBetweenDataToolfaceBox.Name = "distanceBetweenDataToolfaceBox";
            this.distanceBetweenDataToolfaceBox.Size = new System.Drawing.Size(75, 20);
            this.distanceBetweenDataToolfaceBox.TabIndex = 101;
            this.distanceBetweenDataToolfaceBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // joulesPerPulseToolfaceBox
            // 
            this.joulesPerPulseToolfaceBox.Location = new System.Drawing.Point(264, 683);
            this.joulesPerPulseToolfaceBox.Name = "joulesPerPulseToolfaceBox";
            this.joulesPerPulseToolfaceBox.Size = new System.Drawing.Size(75, 20);
            this.joulesPerPulseToolfaceBox.TabIndex = 100;
            this.joulesPerPulseToolfaceBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pulsesPerHourToolfaceBox
            // 
            this.pulsesPerHourToolfaceBox.Location = new System.Drawing.Point(264, 651);
            this.pulsesPerHourToolfaceBox.Name = "pulsesPerHourToolfaceBox";
            this.pulsesPerHourToolfaceBox.Size = new System.Drawing.Size(75, 20);
            this.pulsesPerHourToolfaceBox.TabIndex = 99;
            this.pulsesPerHourToolfaceBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numberOfPulsesToolfaceBox
            // 
            this.numberOfPulsesToolfaceBox.Location = new System.Drawing.Point(264, 623);
            this.numberOfPulsesToolfaceBox.Name = "numberOfPulsesToolfaceBox";
            this.numberOfPulsesToolfaceBox.Size = new System.Drawing.Size(75, 20);
            this.numberOfPulsesToolfaceBox.TabIndex = 98;
            this.numberOfPulsesToolfaceBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numberOfPulsesSurveyBox
            // 
            this.numberOfPulsesSurveyBox.Location = new System.Drawing.Point(170, 623);
            this.numberOfPulsesSurveyBox.Name = "numberOfPulsesSurveyBox";
            this.numberOfPulsesSurveyBox.Size = new System.Drawing.Size(75, 20);
            this.numberOfPulsesSurveyBox.TabIndex = 97;
            this.numberOfPulsesSurveyBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // bitRateToolfaceBox
            // 
            this.bitRateToolfaceBox.Location = new System.Drawing.Point(264, 594);
            this.bitRateToolfaceBox.Name = "bitRateToolfaceBox";
            this.bitRateToolfaceBox.Size = new System.Drawing.Size(75, 20);
            this.bitRateToolfaceBox.TabIndex = 96;
            this.bitRateToolfaceBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // bitRateSurveyBox
            // 
            this.bitRateSurveyBox.Location = new System.Drawing.Point(170, 594);
            this.bitRateSurveyBox.Name = "bitRateSurveyBox";
            this.bitRateSurveyBox.Size = new System.Drawing.Size(75, 20);
            this.bitRateSurveyBox.TabIndex = 95;
            this.bitRateSurveyBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // timeSequenceToolfaceBox
            // 
            this.timeSequenceToolfaceBox.Location = new System.Drawing.Point(264, 560);
            this.timeSequenceToolfaceBox.Name = "timeSequenceToolfaceBox";
            this.timeSequenceToolfaceBox.Size = new System.Drawing.Size(75, 20);
            this.timeSequenceToolfaceBox.TabIndex = 94;
            this.timeSequenceToolfaceBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // timeSequenceSurveyBox
            // 
            this.timeSequenceSurveyBox.Location = new System.Drawing.Point(170, 560);
            this.timeSequenceSurveyBox.Name = "timeSequenceSurveyBox";
            this.timeSequenceSurveyBox.Size = new System.Drawing.Size(75, 20);
            this.timeSequenceSurveyBox.TabIndex = 93;
            this.timeSequenceSurveyBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(31, 714);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(120, 13);
            this.label32.TabIndex = 92;
            this.label32.Text = "Distance Between Data";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(74, 686);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(77, 13);
            this.label31.TabIndex = 91;
            this.label31.Text = "Joules / Pulse ";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(79, 654);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(72, 13);
            this.label30.TabIndex = 90;
            this.label30.Text = "Pulses / Hour";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(91, 626);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(60, 13);
            this.label29.TabIndex = 89;
            this.label29.Text = "# of Pulses";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(35, 597);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(116, 13);
            this.label28.TabIndex = 88;
            this.label28.Text = "Bit Rate (bits / second)";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(13, 563);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(139, 13);
            this.label27.TabIndex = 87;
            this.label27.Text = "Time / Sequence (seconds)";
            // 
            // seventeenBitToolfaceItem
            // 
            this.seventeenBitToolfaceItem.Location = new System.Drawing.Point(264, 529);
            this.seventeenBitToolfaceItem.Name = "seventeenBitToolfaceItem";
            this.seventeenBitToolfaceItem.Size = new System.Drawing.Size(75, 20);
            this.seventeenBitToolfaceItem.TabIndex = 86;
            this.seventeenBitToolfaceItem.Text = "0";
            this.seventeenBitToolfaceItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // sixteenBitToolfaceItem
            // 
            this.sixteenBitToolfaceItem.Location = new System.Drawing.Point(264, 495);
            this.sixteenBitToolfaceItem.Name = "sixteenBitToolfaceItem";
            this.sixteenBitToolfaceItem.Size = new System.Drawing.Size(75, 20);
            this.sixteenBitToolfaceItem.TabIndex = 85;
            this.sixteenBitToolfaceItem.Text = "0";
            this.sixteenBitToolfaceItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // fifteenBitToolfaceItem
            // 
            this.fifteenBitToolfaceItem.Location = new System.Drawing.Point(264, 462);
            this.fifteenBitToolfaceItem.Name = "fifteenBitToolfaceItem";
            this.fifteenBitToolfaceItem.Size = new System.Drawing.Size(75, 20);
            this.fifteenBitToolfaceItem.TabIndex = 84;
            this.fifteenBitToolfaceItem.Text = "0";
            this.fifteenBitToolfaceItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // fourteenBitToolfaceItem
            // 
            this.fourteenBitToolfaceItem.Location = new System.Drawing.Point(264, 430);
            this.fourteenBitToolfaceItem.Name = "fourteenBitToolfaceItem";
            this.fourteenBitToolfaceItem.Size = new System.Drawing.Size(75, 20);
            this.fourteenBitToolfaceItem.TabIndex = 83;
            this.fourteenBitToolfaceItem.Text = "0";
            this.fourteenBitToolfaceItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // thirteenBitToolfaceItem
            // 
            this.thirteenBitToolfaceItem.Location = new System.Drawing.Point(264, 400);
            this.thirteenBitToolfaceItem.Name = "thirteenBitToolfaceItem";
            this.thirteenBitToolfaceItem.Size = new System.Drawing.Size(75, 20);
            this.thirteenBitToolfaceItem.TabIndex = 82;
            this.thirteenBitToolfaceItem.Text = "0";
            this.thirteenBitToolfaceItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // twelveBitToolfaceItem
            // 
            this.twelveBitToolfaceItem.Location = new System.Drawing.Point(264, 368);
            this.twelveBitToolfaceItem.Name = "twelveBitToolfaceItem";
            this.twelveBitToolfaceItem.Size = new System.Drawing.Size(75, 20);
            this.twelveBitToolfaceItem.TabIndex = 81;
            this.twelveBitToolfaceItem.Text = "0";
            this.twelveBitToolfaceItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // elevenBitToolfaceItem
            // 
            this.elevenBitToolfaceItem.Location = new System.Drawing.Point(264, 337);
            this.elevenBitToolfaceItem.Name = "elevenBitToolfaceItem";
            this.elevenBitToolfaceItem.Size = new System.Drawing.Size(75, 20);
            this.elevenBitToolfaceItem.TabIndex = 80;
            this.elevenBitToolfaceItem.Text = "0";
            this.elevenBitToolfaceItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tenBitToolfaceItem
            // 
            this.tenBitToolfaceItem.Location = new System.Drawing.Point(264, 307);
            this.tenBitToolfaceItem.Name = "tenBitToolfaceItem";
            this.tenBitToolfaceItem.Size = new System.Drawing.Size(75, 20);
            this.tenBitToolfaceItem.TabIndex = 79;
            this.tenBitToolfaceItem.Text = "0";
            this.tenBitToolfaceItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // nineBitToolfaceItem
            // 
            this.nineBitToolfaceItem.Location = new System.Drawing.Point(264, 277);
            this.nineBitToolfaceItem.Name = "nineBitToolfaceItem";
            this.nineBitToolfaceItem.Size = new System.Drawing.Size(75, 20);
            this.nineBitToolfaceItem.TabIndex = 78;
            this.nineBitToolfaceItem.Text = "0";
            this.nineBitToolfaceItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // eightBitToolfaceItem
            // 
            this.eightBitToolfaceItem.Location = new System.Drawing.Point(264, 250);
            this.eightBitToolfaceItem.Name = "eightBitToolfaceItem";
            this.eightBitToolfaceItem.Size = new System.Drawing.Size(75, 20);
            this.eightBitToolfaceItem.TabIndex = 77;
            this.eightBitToolfaceItem.Text = "0";
            this.eightBitToolfaceItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // sevenBitToolfaceItem
            // 
            this.sevenBitToolfaceItem.Location = new System.Drawing.Point(264, 218);
            this.sevenBitToolfaceItem.Name = "sevenBitToolfaceItem";
            this.sevenBitToolfaceItem.Size = new System.Drawing.Size(75, 20);
            this.sevenBitToolfaceItem.TabIndex = 76;
            this.sevenBitToolfaceItem.Text = "0";
            this.sevenBitToolfaceItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // sixBitToolfaceItem
            // 
            this.sixBitToolfaceItem.Location = new System.Drawing.Point(264, 187);
            this.sixBitToolfaceItem.Name = "sixBitToolfaceItem";
            this.sixBitToolfaceItem.Size = new System.Drawing.Size(75, 20);
            this.sixBitToolfaceItem.TabIndex = 75;
            this.sixBitToolfaceItem.Text = "0";
            this.sixBitToolfaceItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // fiveBitToolfaceItem
            // 
            this.fiveBitToolfaceItem.Location = new System.Drawing.Point(264, 156);
            this.fiveBitToolfaceItem.Name = "fiveBitToolfaceItem";
            this.fiveBitToolfaceItem.Size = new System.Drawing.Size(75, 20);
            this.fiveBitToolfaceItem.TabIndex = 74;
            this.fiveBitToolfaceItem.Text = "0";
            this.fiveBitToolfaceItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // fourBitToolfaceItem
            // 
            this.fourBitToolfaceItem.Location = new System.Drawing.Point(264, 127);
            this.fourBitToolfaceItem.Name = "fourBitToolfaceItem";
            this.fourBitToolfaceItem.Size = new System.Drawing.Size(75, 20);
            this.fourBitToolfaceItem.TabIndex = 73;
            this.fourBitToolfaceItem.Text = "0";
            this.fourBitToolfaceItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // threeBitToolfaceItem
            // 
            this.threeBitToolfaceItem.Location = new System.Drawing.Point(264, 94);
            this.threeBitToolfaceItem.Name = "threeBitToolfaceItem";
            this.threeBitToolfaceItem.Size = new System.Drawing.Size(75, 20);
            this.threeBitToolfaceItem.TabIndex = 72;
            this.threeBitToolfaceItem.Text = "0";
            this.threeBitToolfaceItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // twoBitToolfaceItem
            // 
            this.twoBitToolfaceItem.Location = new System.Drawing.Point(264, 67);
            this.twoBitToolfaceItem.Name = "twoBitToolfaceItem";
            this.twoBitToolfaceItem.Size = new System.Drawing.Size(75, 20);
            this.twoBitToolfaceItem.TabIndex = 71;
            this.twoBitToolfaceItem.Text = "0";
            this.twoBitToolfaceItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // oneBitToolfaceItem
            // 
            this.oneBitToolfaceItem.Location = new System.Drawing.Point(264, 35);
            this.oneBitToolfaceItem.Name = "oneBitToolfaceItem";
            this.oneBitToolfaceItem.Size = new System.Drawing.Size(75, 20);
            this.oneBitToolfaceItem.TabIndex = 70;
            this.oneBitToolfaceItem.Text = "0";
            this.oneBitToolfaceItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // seventeenBitSurveyItem
            // 
            this.seventeenBitSurveyItem.Location = new System.Drawing.Point(170, 529);
            this.seventeenBitSurveyItem.Name = "seventeenBitSurveyItem";
            this.seventeenBitSurveyItem.Size = new System.Drawing.Size(75, 20);
            this.seventeenBitSurveyItem.TabIndex = 69;
            this.seventeenBitSurveyItem.Text = "0";
            this.seventeenBitSurveyItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // sixteenBitSurveyItem
            // 
            this.sixteenBitSurveyItem.Location = new System.Drawing.Point(170, 495);
            this.sixteenBitSurveyItem.Name = "sixteenBitSurveyItem";
            this.sixteenBitSurveyItem.Size = new System.Drawing.Size(75, 20);
            this.sixteenBitSurveyItem.TabIndex = 68;
            this.sixteenBitSurveyItem.Text = "0";
            this.sixteenBitSurveyItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // fifteenBitSurveyItem
            // 
            this.fifteenBitSurveyItem.Location = new System.Drawing.Point(170, 462);
            this.fifteenBitSurveyItem.Name = "fifteenBitSurveyItem";
            this.fifteenBitSurveyItem.Size = new System.Drawing.Size(75, 20);
            this.fifteenBitSurveyItem.TabIndex = 67;
            this.fifteenBitSurveyItem.Text = "0";
            this.fifteenBitSurveyItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // fourteenBitSurveyItem
            // 
            this.fourteenBitSurveyItem.Location = new System.Drawing.Point(170, 430);
            this.fourteenBitSurveyItem.Name = "fourteenBitSurveyItem";
            this.fourteenBitSurveyItem.Size = new System.Drawing.Size(75, 20);
            this.fourteenBitSurveyItem.TabIndex = 66;
            this.fourteenBitSurveyItem.Text = "0";
            this.fourteenBitSurveyItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // thirteenBitSurveyItem
            // 
            this.thirteenBitSurveyItem.Location = new System.Drawing.Point(170, 400);
            this.thirteenBitSurveyItem.Name = "thirteenBitSurveyItem";
            this.thirteenBitSurveyItem.Size = new System.Drawing.Size(75, 20);
            this.thirteenBitSurveyItem.TabIndex = 65;
            this.thirteenBitSurveyItem.Text = "0";
            this.thirteenBitSurveyItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // twelveBitSurveyItem
            // 
            this.twelveBitSurveyItem.Location = new System.Drawing.Point(170, 368);
            this.twelveBitSurveyItem.Name = "twelveBitSurveyItem";
            this.twelveBitSurveyItem.Size = new System.Drawing.Size(75, 20);
            this.twelveBitSurveyItem.TabIndex = 64;
            this.twelveBitSurveyItem.Text = "0";
            this.twelveBitSurveyItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // elevenBitSurveyItem
            // 
            this.elevenBitSurveyItem.Location = new System.Drawing.Point(170, 337);
            this.elevenBitSurveyItem.Name = "elevenBitSurveyItem";
            this.elevenBitSurveyItem.Size = new System.Drawing.Size(75, 20);
            this.elevenBitSurveyItem.TabIndex = 63;
            this.elevenBitSurveyItem.Text = "0";
            this.elevenBitSurveyItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tenBitSurveyItem
            // 
            this.tenBitSurveyItem.Location = new System.Drawing.Point(170, 307);
            this.tenBitSurveyItem.Name = "tenBitSurveyItem";
            this.tenBitSurveyItem.Size = new System.Drawing.Size(75, 20);
            this.tenBitSurveyItem.TabIndex = 62;
            this.tenBitSurveyItem.Text = "0";
            this.tenBitSurveyItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // nineBitSurveyItem
            // 
            this.nineBitSurveyItem.Location = new System.Drawing.Point(170, 277);
            this.nineBitSurveyItem.Name = "nineBitSurveyItem";
            this.nineBitSurveyItem.Size = new System.Drawing.Size(75, 20);
            this.nineBitSurveyItem.TabIndex = 61;
            this.nineBitSurveyItem.Text = "0";
            this.nineBitSurveyItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // eightBitSurveyItem
            // 
            this.eightBitSurveyItem.Location = new System.Drawing.Point(170, 250);
            this.eightBitSurveyItem.Name = "eightBitSurveyItem";
            this.eightBitSurveyItem.Size = new System.Drawing.Size(75, 20);
            this.eightBitSurveyItem.TabIndex = 60;
            this.eightBitSurveyItem.Text = "0";
            this.eightBitSurveyItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // sevenBitSurveyItem
            // 
            this.sevenBitSurveyItem.Location = new System.Drawing.Point(170, 217);
            this.sevenBitSurveyItem.Name = "sevenBitSurveyItem";
            this.sevenBitSurveyItem.Size = new System.Drawing.Size(75, 20);
            this.sevenBitSurveyItem.TabIndex = 59;
            this.sevenBitSurveyItem.Text = "0";
            this.sevenBitSurveyItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // sixBitSurveyItem
            // 
            this.sixBitSurveyItem.Location = new System.Drawing.Point(170, 187);
            this.sixBitSurveyItem.Name = "sixBitSurveyItem";
            this.sixBitSurveyItem.Size = new System.Drawing.Size(75, 20);
            this.sixBitSurveyItem.TabIndex = 58;
            this.sixBitSurveyItem.Text = "0";
            this.sixBitSurveyItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // fiveBitSurveyItem
            // 
            this.fiveBitSurveyItem.Location = new System.Drawing.Point(170, 155);
            this.fiveBitSurveyItem.Name = "fiveBitSurveyItem";
            this.fiveBitSurveyItem.Size = new System.Drawing.Size(75, 20);
            this.fiveBitSurveyItem.TabIndex = 57;
            this.fiveBitSurveyItem.Text = "0";
            this.fiveBitSurveyItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // fourBitSurveyItem
            // 
            this.fourBitSurveyItem.Location = new System.Drawing.Point(170, 127);
            this.fourBitSurveyItem.Name = "fourBitSurveyItem";
            this.fourBitSurveyItem.Size = new System.Drawing.Size(75, 20);
            this.fourBitSurveyItem.TabIndex = 56;
            this.fourBitSurveyItem.Text = "0";
            this.fourBitSurveyItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // threeBitSurveyItem
            // 
            this.threeBitSurveyItem.Location = new System.Drawing.Point(170, 95);
            this.threeBitSurveyItem.Name = "threeBitSurveyItem";
            this.threeBitSurveyItem.Size = new System.Drawing.Size(75, 20);
            this.threeBitSurveyItem.TabIndex = 55;
            this.threeBitSurveyItem.Text = "0";
            this.threeBitSurveyItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // twoBitSurveyItem
            // 
            this.twoBitSurveyItem.Location = new System.Drawing.Point(170, 67);
            this.twoBitSurveyItem.Name = "twoBitSurveyItem";
            this.twoBitSurveyItem.Size = new System.Drawing.Size(75, 20);
            this.twoBitSurveyItem.TabIndex = 54;
            this.twoBitSurveyItem.Text = "0";
            this.twoBitSurveyItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // oneBitSurveyItem
            // 
            this.oneBitSurveyItem.Location = new System.Drawing.Point(170, 35);
            this.oneBitSurveyItem.Name = "oneBitSurveyItem";
            this.oneBitSurveyItem.Size = new System.Drawing.Size(75, 20);
            this.oneBitSurveyItem.TabIndex = 53;
            this.oneBitSurveyItem.Text = "0";
            this.oneBitSurveyItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(84, 16);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(56, 13);
            this.label26.TabIndex = 52;
            this.label26.Text = "Sequence";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(67, 536);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(84, 13);
            this.label25.TabIndex = 34;
            this.label25.Text = "# of 17 Bit Items";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(67, 502);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(84, 13);
            this.label24.TabIndex = 33;
            this.label24.Text = "# of 16 Bit Items";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(67, 469);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(84, 13);
            this.label23.TabIndex = 32;
            this.label23.Text = "# of 15 Bit Items";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(67, 437);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 13);
            this.label7.TabIndex = 31;
            this.label7.Text = "# of 14 Bit Items";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(73, 42);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(78, 13);
            this.label10.TabIndex = 18;
            this.label10.Text = "# of 1 Bit Items";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(73, 224);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(78, 13);
            this.label16.TabIndex = 24;
            this.label16.Text = "# of 7 Bit Items";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(67, 407);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(84, 13);
            this.label22.TabIndex = 30;
            this.label22.Text = "# of 13 Bit Items";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(73, 257);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(78, 13);
            this.label17.TabIndex = 25;
            this.label17.Text = "# of 8 Bit Items";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(73, 74);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(78, 13);
            this.label11.TabIndex = 19;
            this.label11.Text = "# of 2 Bit Items";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(73, 194);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(78, 13);
            this.label15.TabIndex = 23;
            this.label15.Text = "# of 6 Bit Items";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(67, 375);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(84, 13);
            this.label21.TabIndex = 29;
            this.label21.Text = "# of 12 Bit Items";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(73, 284);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(78, 13);
            this.label18.TabIndex = 26;
            this.label18.Text = "# of 9 Bit Items";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(73, 104);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(78, 13);
            this.label12.TabIndex = 20;
            this.label12.Text = "# of 3 Bit Items";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(73, 164);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(78, 13);
            this.label14.TabIndex = 22;
            this.label14.Text = "# of 5 Bit Items";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(67, 344);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(84, 13);
            this.label20.TabIndex = 28;
            this.label20.Text = "# of 11 Bit Items";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(67, 314);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(84, 13);
            this.label19.TabIndex = 27;
            this.label19.Text = "# of 10 Bit Items";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(73, 134);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(78, 13);
            this.label13.TabIndex = 21;
            this.label13.Text = "# of 4 Bit Items";
            // 
            // energyConsumptionGroupBox
            // 
            this.energyConsumptionGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.energyConsumptionGroupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.energyConsumptionGroupBox.Controls.Add(this.chart1);
            this.energyConsumptionGroupBox.Location = new System.Drawing.Point(6, 6);
            this.energyConsumptionGroupBox.Name = "energyConsumptionGroupBox";
            this.energyConsumptionGroupBox.Size = new System.Drawing.Size(1403, 727);
            this.energyConsumptionGroupBox.TabIndex = 14;
            this.energyConsumptionGroupBox.TabStop = false;
            this.energyConsumptionGroupBox.Text = "Energy Consumption";
            // 
            // chart1
            // 
            chartArea3.Area3DStyle.Inclination = 40;
            chartArea3.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea3);
            this.chart1.Dock = System.Windows.Forms.DockStyle.Fill;
            legend3.Name = "Legend1";
            this.chart1.Legends.Add(legend3);
            this.chart1.Location = new System.Drawing.Point(3, 16);
            this.chart1.Name = "chart1";
            series3.ChartArea = "ChartArea1";
            series3.Legend = "Legend1";
            series3.Name = "Series1";
            this.chart1.Series.Add(series3);
            this.chart1.Size = new System.Drawing.Size(1397, 708);
            this.chart1.TabIndex = 13;
            this.chart1.Text = "chart1";
            // 
            // toolGroupBox
            // 
            this.toolGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.toolGroupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.toolGroupBox.Controls.Add(this.apsWpr);
            this.toolGroupBox.Controls.Add(this.wolverineLndcCheckbox);
            this.toolGroupBox.Controls.Add(this.wolverineMicrohopCheckbox);
            this.toolGroupBox.Controls.Add(this.rmsPressureToolCheckbox);
            this.toolGroupBox.Controls.Add(this.rmsResistivtyCheckbox);
            this.toolGroupBox.Controls.Add(this.wolverineTranslator);
            this.toolGroupBox.Controls.Add(this.gamma);
            this.toolGroupBox.Controls.Add(this.wolverineMasterWDirectional);
            this.toolGroupBox.Controls.Add(this.wolverineTmpSmartSolenoid);
            this.toolGroupBox.Location = new System.Drawing.Point(694, 736);
            this.toolGroupBox.Name = "toolGroupBox";
            this.toolGroupBox.Size = new System.Drawing.Size(369, 236);
            this.toolGroupBox.TabIndex = 0;
            this.toolGroupBox.TabStop = false;
            this.toolGroupBox.Text = "Tool";
            // 
            // apsWpr
            // 
            this.apsWpr.AutoSize = true;
            this.apsWpr.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.apsWpr.Checked = true;
            this.apsWpr.CheckState = System.Windows.Forms.CheckState.Checked;
            this.apsWpr.Location = new System.Drawing.Point(244, 34);
            this.apsWpr.Name = "apsWpr";
            this.apsWpr.Size = new System.Drawing.Size(76, 17);
            this.apsWpr.TabIndex = 8;
            this.apsWpr.Text = "APS WPR";
            this.apsWpr.UseVisualStyleBackColor = true;
            this.apsWpr.CheckedChanged += new System.EventHandler(this.apsWpr_CheckedChanged);
            // 
            // wolverineLndcCheckbox
            // 
            this.wolverineLndcCheckbox.AutoSize = true;
            this.wolverineLndcCheckbox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.wolverineLndcCheckbox.Checked = true;
            this.wolverineLndcCheckbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.wolverineLndcCheckbox.Location = new System.Drawing.Point(112, 195);
            this.wolverineLndcCheckbox.Name = "wolverineLndcCheckbox";
            this.wolverineLndcCheckbox.Size = new System.Drawing.Size(106, 17);
            this.wolverineLndcCheckbox.TabIndex = 7;
            this.wolverineLndcCheckbox.Text = "Wolverine LNDC";
            this.wolverineLndcCheckbox.UseVisualStyleBackColor = true;
            this.wolverineLndcCheckbox.CheckedChanged += new System.EventHandler(this.wolverineLndcCheckbox_CheckedChanged);
            // 
            // wolverineMicrohopCheckbox
            // 
            this.wolverineMicrohopCheckbox.AutoSize = true;
            this.wolverineMicrohopCheckbox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.wolverineMicrohopCheckbox.Checked = true;
            this.wolverineMicrohopCheckbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.wolverineMicrohopCheckbox.Location = new System.Drawing.Point(97, 172);
            this.wolverineMicrohopCheckbox.Name = "wolverineMicrohopCheckbox";
            this.wolverineMicrohopCheckbox.Size = new System.Drawing.Size(121, 17);
            this.wolverineMicrohopCheckbox.TabIndex = 6;
            this.wolverineMicrohopCheckbox.Text = "Wolverine Microhop";
            this.wolverineMicrohopCheckbox.UseVisualStyleBackColor = true;
            this.wolverineMicrohopCheckbox.CheckedChanged += new System.EventHandler(this.wolverineMicrohopCheckbox_CheckedChanged);
            // 
            // rmsPressureToolCheckbox
            // 
            this.rmsPressureToolCheckbox.AutoSize = true;
            this.rmsPressureToolCheckbox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.rmsPressureToolCheckbox.Checked = true;
            this.rmsPressureToolCheckbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.rmsPressureToolCheckbox.Location = new System.Drawing.Point(100, 149);
            this.rmsPressureToolCheckbox.Name = "rmsPressureToolCheckbox";
            this.rmsPressureToolCheckbox.Size = new System.Drawing.Size(118, 17);
            this.rmsPressureToolCheckbox.TabIndex = 5;
            this.rmsPressureToolCheckbox.Text = "RMS Pressure Tool";
            this.rmsPressureToolCheckbox.UseVisualStyleBackColor = true;
            this.rmsPressureToolCheckbox.CheckedChanged += new System.EventHandler(this.rmsPressureToolCheckbox_CheckedChanged);
            // 
            // rmsResistivtyCheckbox
            // 
            this.rmsResistivtyCheckbox.AutoSize = true;
            this.rmsResistivtyCheckbox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.rmsResistivtyCheckbox.Checked = true;
            this.rmsResistivtyCheckbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.rmsResistivtyCheckbox.Location = new System.Drawing.Point(118, 126);
            this.rmsResistivtyCheckbox.Name = "rmsResistivtyCheckbox";
            this.rmsResistivtyCheckbox.Size = new System.Drawing.Size(100, 17);
            this.rmsResistivtyCheckbox.TabIndex = 4;
            this.rmsResistivtyCheckbox.Text = "RMS Resistivity";
            this.rmsResistivtyCheckbox.UseVisualStyleBackColor = true;
            this.rmsResistivtyCheckbox.CheckedChanged += new System.EventHandler(this.rmsResistivtyCheckbox_CheckedChanged);
            // 
            // wolverineTranslator
            // 
            this.wolverineTranslator.AutoSize = true;
            this.wolverineTranslator.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.wolverineTranslator.Checked = true;
            this.wolverineTranslator.CheckState = System.Windows.Forms.CheckState.Checked;
            this.wolverineTranslator.Location = new System.Drawing.Point(94, 103);
            this.wolverineTranslator.Name = "wolverineTranslator";
            this.wolverineTranslator.Size = new System.Drawing.Size(124, 17);
            this.wolverineTranslator.TabIndex = 3;
            this.wolverineTranslator.Text = "Wolverine Translator";
            this.wolverineTranslator.UseVisualStyleBackColor = true;
            this.wolverineTranslator.CheckedChanged += new System.EventHandler(this.wolverineTranslator_CheckedChanged);
            // 
            // gamma
            // 
            this.gamma.AutoSize = true;
            this.gamma.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.gamma.Checked = true;
            this.gamma.CheckState = System.Windows.Forms.CheckState.Checked;
            this.gamma.Location = new System.Drawing.Point(156, 80);
            this.gamma.Name = "gamma";
            this.gamma.Size = new System.Drawing.Size(62, 17);
            this.gamma.TabIndex = 2;
            this.gamma.Text = "Gamma";
            this.gamma.UseVisualStyleBackColor = true;
            this.gamma.CheckedChanged += new System.EventHandler(this.gamma_CheckedChanged);
            // 
            // wolverineMasterWDirectional
            // 
            this.wolverineMasterWDirectional.AutoSize = true;
            this.wolverineMasterWDirectional.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.wolverineMasterWDirectional.Checked = true;
            this.wolverineMasterWDirectional.CheckState = System.Windows.Forms.CheckState.Checked;
            this.wolverineMasterWDirectional.Location = new System.Drawing.Point(40, 57);
            this.wolverineMasterWDirectional.Name = "wolverineMasterWDirectional";
            this.wolverineMasterWDirectional.Size = new System.Drawing.Size(178, 17);
            this.wolverineMasterWDirectional.TabIndex = 1;
            this.wolverineMasterWDirectional.Text = "Wolverine Master w/ Directional";
            this.wolverineMasterWDirectional.UseVisualStyleBackColor = true;
            this.wolverineMasterWDirectional.CheckedChanged += new System.EventHandler(this.wolverineMasterWNovDirectional_CheckedChanged);
            // 
            // wolverineTmpSmartSolenoid
            // 
            this.wolverineTmpSmartSolenoid.AutoSize = true;
            this.wolverineTmpSmartSolenoid.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.wolverineTmpSmartSolenoid.Checked = true;
            this.wolverineTmpSmartSolenoid.CheckState = System.Windows.Forms.CheckState.Checked;
            this.wolverineTmpSmartSolenoid.Location = new System.Drawing.Point(118, 34);
            this.wolverineTmpSmartSolenoid.Name = "wolverineTmpSmartSolenoid";
            this.wolverineTmpSmartSolenoid.Size = new System.Drawing.Size(100, 17);
            this.wolverineTmpSmartSolenoid.TabIndex = 0;
            this.wolverineTmpSmartSolenoid.Text = "Wolverine TMP";
            this.wolverineTmpSmartSolenoid.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.wolverineTmpSmartSolenoid.UseVisualStyleBackColor = true;
            this.wolverineTmpSmartSolenoid.CheckedChanged += new System.EventHandler(this.wolverineTmpSmartSolenoid_CheckedChanged);
            // 
            // powerConsumptionTab
            // 
            this.powerConsumptionTab.Controls.Add(this.toolTable);
            this.powerConsumptionTab.Controls.Add(this.resistivityPowerConsumptionTable);
            this.powerConsumptionTab.Controls.Add(this.powerConsumptionTable);
            this.powerConsumptionTab.Location = new System.Drawing.Point(4, 22);
            this.powerConsumptionTab.Name = "powerConsumptionTab";
            this.powerConsumptionTab.Padding = new System.Windows.Forms.Padding(3);
            this.powerConsumptionTab.Size = new System.Drawing.Size(1882, 983);
            this.powerConsumptionTab.TabIndex = 1;
            this.powerConsumptionTab.Text = "POWER CONSUMPTION";
            this.powerConsumptionTab.UseVisualStyleBackColor = true;
            // 
            // toolTable
            // 
            this.toolTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.toolTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.toolColumn,
            this.inStringColumn,
            this.currentColumn});
            this.toolTable.Location = new System.Drawing.Point(285, 121);
            this.toolTable.Name = "toolTable";
            this.toolTable.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.toolTable.RowHeadersWidth = 62;
            this.toolTable.Size = new System.Drawing.Size(343, 176);
            this.toolTable.TabIndex = 7;
            // 
            // toolColumn
            // 
            this.toolColumn.HeaderText = "Tool";
            this.toolColumn.MinimumWidth = 8;
            this.toolColumn.Name = "toolColumn";
            this.toolColumn.ReadOnly = true;
            this.toolColumn.Width = 150;
            // 
            // inStringColumn
            // 
            this.inStringColumn.HeaderText = "In String?";
            this.inStringColumn.MinimumWidth = 8;
            this.inStringColumn.Name = "inStringColumn";
            this.inStringColumn.ReadOnly = true;
            this.inStringColumn.Width = 150;
            // 
            // currentColumn
            // 
            this.currentColumn.HeaderText = "Current (mA)";
            this.currentColumn.MinimumWidth = 8;
            this.currentColumn.Name = "currentColumn";
            this.currentColumn.ReadOnly = true;
            this.currentColumn.Width = 150;
            // 
            // resistivityPowerConsumptionTable
            // 
            this.resistivityPowerConsumptionTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.resistivityPowerConsumptionTable.Location = new System.Drawing.Point(285, 16);
            this.resistivityPowerConsumptionTable.Name = "resistivityPowerConsumptionTable";
            this.resistivityPowerConsumptionTable.RowHeadersWidth = 62;
            this.resistivityPowerConsumptionTable.Size = new System.Drawing.Size(944, 87);
            this.resistivityPowerConsumptionTable.TabIndex = 1;
            // 
            // powerConsumptionTable
            // 
            this.powerConsumptionTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.powerConsumptionTable.Location = new System.Drawing.Point(18, 16);
            this.powerConsumptionTable.Name = "powerConsumptionTable";
            this.powerConsumptionTable.RowHeadersWidth = 62;
            this.powerConsumptionTable.Size = new System.Drawing.Size(243, 244);
            this.powerConsumptionTable.TabIndex = 0;
            // 
            // pulserDataTab
            // 
            this.pulserDataTab.Controls.Add(this.tnpTdOptionTable);
            this.pulserDataTab.Controls.Add(this.pulserConsumptionTable);
            this.pulserDataTab.Controls.Add(this.slotWidth200Table);
            this.pulserDataTab.Controls.Add(this.slotWidth0213Table);
            this.pulserDataTab.Controls.Add(this.slotWidth160Table);
            this.pulserDataTab.Location = new System.Drawing.Point(4, 22);
            this.pulserDataTab.Name = "pulserDataTab";
            this.pulserDataTab.Size = new System.Drawing.Size(1882, 983);
            this.pulserDataTab.TabIndex = 2;
            this.pulserDataTab.Text = "PULSER DATA";
            this.pulserDataTab.UseVisualStyleBackColor = true;
            // 
            // tnpTdOptionTable
            // 
            this.tnpTdOptionTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tnpTdOptionTable.Location = new System.Drawing.Point(1027, 478);
            this.tnpTdOptionTable.Name = "tnpTdOptionTable";
            this.tnpTdOptionTable.RowHeadersWidth = 62;
            this.tnpTdOptionTable.Size = new System.Drawing.Size(475, 59);
            this.tnpTdOptionTable.TabIndex = 4;
            // 
            // pulserConsumptionTable
            // 
            this.pulserConsumptionTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.pulserConsumptionTable.Location = new System.Drawing.Point(26, 478);
            this.pulserConsumptionTable.Name = "pulserConsumptionTable";
            this.pulserConsumptionTable.RowHeadersWidth = 62;
            this.pulserConsumptionTable.Size = new System.Drawing.Size(978, 59);
            this.pulserConsumptionTable.TabIndex = 3;
            // 
            // slotWidth200Table
            // 
            this.slotWidth200Table.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.slotWidth200Table.Location = new System.Drawing.Point(1027, 23);
            this.slotWidth200Table.Name = "slotWidth200Table";
            this.slotWidth200Table.RowHeadersWidth = 62;
            this.slotWidth200Table.Size = new System.Drawing.Size(475, 428);
            this.slotWidth200Table.TabIndex = 2;
            // 
            // slotWidth0213Table
            // 
            this.slotWidth0213Table.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.slotWidth0213Table.Location = new System.Drawing.Point(529, 23);
            this.slotWidth0213Table.Name = "slotWidth0213Table";
            this.slotWidth0213Table.RowHeadersWidth = 62;
            this.slotWidth0213Table.Size = new System.Drawing.Size(475, 428);
            this.slotWidth0213Table.TabIndex = 1;
            // 
            // slotWidth160Table
            // 
            this.slotWidth160Table.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.slotWidth160Table.Location = new System.Drawing.Point(26, 23);
            this.slotWidth160Table.Name = "slotWidth160Table";
            this.slotWidth160Table.RowHeadersWidth = 62;
            this.slotWidth160Table.Size = new System.Drawing.Size(475, 428);
            this.slotWidth160Table.TabIndex = 0;
            // 
            // orificeCalcTab
            // 
            this.orificeCalcTab.Controls.Add(this.orificeCalcTabControl);
            this.orificeCalcTab.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.orificeCalcTab.Location = new System.Drawing.Point(4, 22);
            this.orificeCalcTab.Name = "orificeCalcTab";
            this.orificeCalcTab.Size = new System.Drawing.Size(1896, 1015);
            this.orificeCalcTab.TabIndex = 1;
            this.orificeCalcTab.Text = "ORIFICE CALCULATOR";
            this.orificeCalcTab.UseVisualStyleBackColor = true;
            // 
            // orificeCalcTabControl
            // 
            this.orificeCalcTabControl.Controls.Add(this.orificeMainTab);
            this.orificeCalcTabControl.Controls.Add(this.orificeTablesTab);
            this.orificeCalcTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.orificeCalcTabControl.Location = new System.Drawing.Point(0, 0);
            this.orificeCalcTabControl.Name = "orificeCalcTabControl";
            this.orificeCalcTabControl.SelectedIndex = 0;
            this.orificeCalcTabControl.Size = new System.Drawing.Size(1896, 1015);
            this.orificeCalcTabControl.TabIndex = 13;
            // 
            // orificeMainTab
            // 
            this.orificeMainTab.Controls.Add(this.groupBox6);
            this.orificeMainTab.Controls.Add(this.groupBox4);
            this.orificeMainTab.Controls.Add(this.orificeAdminGroupBox);
            this.orificeMainTab.Location = new System.Drawing.Point(4, 22);
            this.orificeMainTab.Name = "orificeMainTab";
            this.orificeMainTab.Padding = new System.Windows.Forms.Padding(3);
            this.orificeMainTab.Size = new System.Drawing.Size(1888, 989);
            this.orificeMainTab.TabIndex = 0;
            this.orificeMainTab.Text = "MAIN";
            this.orificeMainTab.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox6.CausesValidation = false;
            this.groupBox6.Controls.Add(this.chart);
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.groupBox6.Location = new System.Drawing.Point(6, 6);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(1876, 716);
            this.groupBox6.TabIndex = 12;
            this.groupBox6.TabStop = false;
            // 
            // chart
            // 
            chartArea4.Name = "ChartArea1";
            this.chart.ChartAreas.Add(chartArea4);
            this.chart.Dock = System.Windows.Forms.DockStyle.Fill;
            legend4.Name = "Legend1";
            this.chart.Legends.Add(legend4);
            this.chart.Location = new System.Drawing.Point(3, 22);
            this.chart.Name = "chart";
            series4.ChartArea = "ChartArea1";
            series4.Legend = "Legend1";
            series4.Name = "Series1";
            this.chart.Series.Add(series4);
            this.chart.Size = new System.Drawing.Size(1870, 691);
            this.chart.TabIndex = 10;
            this.chart.Text = "chart2";
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.groupBox4.Controls.Add(this.groupBox14);
            this.groupBox4.Controls.Add(this.groupBox15);
            this.groupBox4.Controls.Add(this.setButton);
            this.groupBox4.Controls.Add(this.defaultButton);
            this.groupBox4.Location = new System.Drawing.Point(563, 734);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(643, 222);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Settings";
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.pumpEfficiencyTextbox);
            this.groupBox14.Controls.Add(this.label33);
            this.groupBox14.Controls.Add(this.label65);
            this.groupBox14.Controls.Add(this.label36);
            this.groupBox14.Controls.Add(this.mudWeightTextbox);
            this.groupBox14.Controls.Add(this.orificeMudWeightUnitsBox);
            this.groupBox14.Controls.Add(this.label62);
            this.groupBox14.Controls.Add(this.orificeFlowRateUnitsBox);
            this.groupBox14.Controls.Add(this.orificeToolSizeBox);
            this.groupBox14.Controls.Add(this.label63);
            this.groupBox14.Controls.Add(this.orificeFlowRateTextbox);
            this.groupBox14.Location = new System.Drawing.Point(15, 18);
            this.groupBox14.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox14.Size = new System.Drawing.Size(297, 139);
            this.groupBox14.TabIndex = 27;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "ORIFICE CALCULATOR";
            // 
            // pumpEfficiencyTextbox
            // 
            this.pumpEfficiencyTextbox.Location = new System.Drawing.Point(104, 18);
            this.pumpEfficiencyTextbox.Name = "pumpEfficiencyTextbox";
            this.pumpEfficiencyTextbox.Size = new System.Drawing.Size(100, 20);
            this.pumpEfficiencyTextbox.TabIndex = 5;
            this.pumpEfficiencyTextbox.Text = "1";
            this.pumpEfficiencyTextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(17, 21);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(83, 13);
            this.label33.TabIndex = 0;
            this.label33.Text = "Pump Efficiency";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(43, 113);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(55, 13);
            this.label65.TabIndex = 1;
            this.label65.Text = "Flow Rate";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(35, 81);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(65, 13);
            this.label36.TabIndex = 3;
            this.label36.Text = "Mud Weight";
            // 
            // mudWeightTextbox
            // 
            this.mudWeightTextbox.Location = new System.Drawing.Point(105, 78);
            this.mudWeightTextbox.Name = "mudWeightTextbox";
            this.mudWeightTextbox.Size = new System.Drawing.Size(100, 20);
            this.mudWeightTextbox.TabIndex = 8;
            this.mudWeightTextbox.Text = "9";
            this.mudWeightTextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // orificeMudWeightUnitsBox
            // 
            this.orificeMudWeightUnitsBox.AutoCompleteCustomSource.AddRange(new string[] {
            "PPG",
            "SG",
            "KG/M^3",
            "LB/FT^3",
            "G/CM^3"});
            this.orificeMudWeightUnitsBox.FormattingEnabled = true;
            this.orificeMudWeightUnitsBox.Items.AddRange(new object[] {
            "PPG",
            "SG",
            "KG/M^3",
            "LB/FT^3",
            "G/CM^3"});
            this.orificeMudWeightUnitsBox.Location = new System.Drawing.Point(211, 77);
            this.orificeMudWeightUnitsBox.Name = "orificeMudWeightUnitsBox";
            this.orificeMudWeightUnitsBox.Size = new System.Drawing.Size(75, 21);
            this.orificeMudWeightUnitsBox.TabIndex = 12;
            this.orificeMudWeightUnitsBox.Text = "PPG";
            this.orificeMudWeightUnitsBox.SelectedIndexChanged += new System.EventHandler(this.orificeMudWeightUnitsBox_SelectedIndexChanged);
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(48, 51);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(51, 13);
            this.label62.TabIndex = 14;
            this.label62.Text = "Tool Size";
            // 
            // orificeFlowRateUnitsBox
            // 
            this.orificeFlowRateUnitsBox.AutoCompleteCustomSource.AddRange(new string[] {
            "PPG",
            "SG",
            "KG/M^3",
            "LB/FT^3",
            "G/CM^3"});
            this.orificeFlowRateUnitsBox.FormattingEnabled = true;
            this.orificeFlowRateUnitsBox.Items.AddRange(new object[] {
            "GPM",
            "LPS"});
            this.orificeFlowRateUnitsBox.Location = new System.Drawing.Point(210, 110);
            this.orificeFlowRateUnitsBox.Name = "orificeFlowRateUnitsBox";
            this.orificeFlowRateUnitsBox.Size = new System.Drawing.Size(75, 21);
            this.orificeFlowRateUnitsBox.TabIndex = 13;
            this.orificeFlowRateUnitsBox.Text = "GPM";
            this.orificeFlowRateUnitsBox.SelectedIndexChanged += new System.EventHandler(this.orificeFlowRateUnitsBox_SelectedIndexChanged);
            // 
            // orificeToolSizeBox
            // 
            this.orificeToolSizeBox.AutoCompleteCustomSource.AddRange(new string[] {
            "4.75",
            "6.75",
            "8.00"});
            this.orificeToolSizeBox.FormattingEnabled = true;
            this.orificeToolSizeBox.Items.AddRange(new object[] {
            "4.75",
            "6.75",
            "8.00"});
            this.orificeToolSizeBox.Location = new System.Drawing.Point(104, 48);
            this.orificeToolSizeBox.Name = "orificeToolSizeBox";
            this.orificeToolSizeBox.Size = new System.Drawing.Size(101, 21);
            this.orificeToolSizeBox.TabIndex = 15;
            this.orificeToolSizeBox.Text = "4.75";
            this.orificeToolSizeBox.SelectedIndexChanged += new System.EventHandler(this.orificeToolSizeBox_SelectedIndexChanged);
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(211, 51);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(47, 13);
            this.label63.TabIndex = 16;
            this.label63.Text = "INCHES";
            // 
            // orificeFlowRateTextbox
            // 
            this.orificeFlowRateTextbox.Location = new System.Drawing.Point(105, 110);
            this.orificeFlowRateTextbox.Name = "orificeFlowRateTextbox";
            this.orificeFlowRateTextbox.Size = new System.Drawing.Size(99, 20);
            this.orificeFlowRateTextbox.TabIndex = 17;
            this.orificeFlowRateTextbox.Text = "250";
            this.orificeFlowRateTextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.pressureAtSurfaceUnitComboBox);
            this.groupBox15.Controls.Add(this.depthForPressureBox);
            this.groupBox15.Controls.Add(this.pressureAtSurfaceBox);
            this.groupBox15.Controls.Add(this.label8);
            this.groupBox15.Controls.Add(this.label75);
            this.groupBox15.Controls.Add(this.depthTdComboBox);
            this.groupBox15.Controls.Add(this.label9);
            this.groupBox15.Controls.Add(this.pressureAtSourceBox);
            this.groupBox15.Controls.Add(this.pressureAtSourceUnitComboBox);
            this.groupBox15.Location = new System.Drawing.Point(325, 18);
            this.groupBox15.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox15.Size = new System.Drawing.Size(305, 139);
            this.groupBox15.TabIndex = 28;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "SURFACE PULSE AMPLITUDE";
            // 
            // pressureAtSurfaceUnitComboBox
            // 
            this.pressureAtSurfaceUnitComboBox.AutoCompleteCustomSource.AddRange(new string[] {
            "PPG",
            "SG",
            "KG/M^3",
            "LB/FT^3",
            "G/CM^3"});
            this.pressureAtSurfaceUnitComboBox.FormattingEnabled = true;
            this.pressureAtSurfaceUnitComboBox.Items.AddRange(new object[] {
            "PSI"});
            this.pressureAtSurfaceUnitComboBox.Location = new System.Drawing.Point(218, 91);
            this.pressureAtSurfaceUnitComboBox.Name = "pressureAtSurfaceUnitComboBox";
            this.pressureAtSurfaceUnitComboBox.Size = new System.Drawing.Size(75, 21);
            this.pressureAtSurfaceUnitComboBox.TabIndex = 26;
            this.pressureAtSurfaceUnitComboBox.Text = "PSI";
            // 
            // depthForPressureBox
            // 
            this.depthForPressureBox.Location = new System.Drawing.Point(113, 35);
            this.depthForPressureBox.Name = "depthForPressureBox";
            this.depthForPressureBox.Size = new System.Drawing.Size(99, 20);
            this.depthForPressureBox.TabIndex = 19;
            this.depthForPressureBox.Text = "5000";
            this.depthForPressureBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pressureAtSurfaceBox
            // 
            this.pressureAtSurfaceBox.Location = new System.Drawing.Point(113, 92);
            this.pressureAtSurfaceBox.Name = "pressureAtSurfaceBox";
            this.pressureAtSurfaceBox.Size = new System.Drawing.Size(99, 20);
            this.pressureAtSurfaceBox.TabIndex = 25;
            this.pressureAtSurfaceBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(50, 38);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "Depth - TD";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(7, 95);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(101, 13);
            this.label75.TabIndex = 24;
            this.label75.Text = "Pressure At Surface";
            // 
            // depthTdComboBox
            // 
            this.depthTdComboBox.AutoCompleteCustomSource.AddRange(new string[] {
            "PPG",
            "SG",
            "KG/M^3",
            "LB/FT^3",
            "G/CM^3"});
            this.depthTdComboBox.FormattingEnabled = true;
            this.depthTdComboBox.Items.AddRange(new object[] {
            "FEET",
            "METERS"});
            this.depthTdComboBox.Location = new System.Drawing.Point(217, 34);
            this.depthTdComboBox.Name = "depthTdComboBox";
            this.depthTdComboBox.Size = new System.Drawing.Size(75, 21);
            this.depthTdComboBox.TabIndex = 20;
            this.depthTdComboBox.Text = "FEET";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 68);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(98, 13);
            this.label9.TabIndex = 21;
            this.label9.Text = "Pressure At Source";
            // 
            // pressureAtSourceBox
            // 
            this.pressureAtSourceBox.Location = new System.Drawing.Point(113, 65);
            this.pressureAtSourceBox.Name = "pressureAtSourceBox";
            this.pressureAtSourceBox.Size = new System.Drawing.Size(99, 20);
            this.pressureAtSourceBox.TabIndex = 22;
            this.pressureAtSourceBox.Text = "350";
            this.pressureAtSourceBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pressureAtSourceUnitComboBox
            // 
            this.pressureAtSourceUnitComboBox.AutoCompleteCustomSource.AddRange(new string[] {
            "PPG",
            "SG",
            "KG/M^3",
            "LB/FT^3",
            "G/CM^3"});
            this.pressureAtSourceUnitComboBox.FormattingEnabled = true;
            this.pressureAtSourceUnitComboBox.Items.AddRange(new object[] {
            "PSI"});
            this.pressureAtSourceUnitComboBox.Location = new System.Drawing.Point(217, 64);
            this.pressureAtSourceUnitComboBox.Name = "pressureAtSourceUnitComboBox";
            this.pressureAtSourceUnitComboBox.Size = new System.Drawing.Size(75, 21);
            this.pressureAtSourceUnitComboBox.TabIndex = 23;
            this.pressureAtSourceUnitComboBox.Text = "PSI";
            // 
            // setButton
            // 
            this.setButton.Location = new System.Drawing.Point(229, 175);
            this.setButton.Name = "setButton";
            this.setButton.Size = new System.Drawing.Size(83, 23);
            this.setButton.TabIndex = 10;
            this.setButton.Text = "CALCULATE";
            this.setButton.UseVisualStyleBackColor = true;
            this.setButton.Click += new System.EventHandler(this.setButton_Click);
            // 
            // defaultButton
            // 
            this.defaultButton.Location = new System.Drawing.Point(325, 175);
            this.defaultButton.Name = "defaultButton";
            this.defaultButton.Size = new System.Drawing.Size(83, 23);
            this.defaultButton.TabIndex = 11;
            this.defaultButton.Text = "DEFAULTS";
            this.defaultButton.UseVisualStyleBackColor = true;
            this.defaultButton.Click += new System.EventHandler(this.defaultButton_Click);
            // 
            // orificeAdminGroupBox
            // 
            this.orificeAdminGroupBox.Controls.Add(this.modelTypicalTextbox);
            this.orificeAdminGroupBox.Controls.Add(this.label34);
            this.orificeAdminGroupBox.Controls.Add(this.label35);
            this.orificeAdminGroupBox.Controls.Add(this.label37);
            this.orificeAdminGroupBox.Controls.Add(this.modelOrificesTextbox);
            this.orificeAdminGroupBox.Controls.Add(this.roundTextBox);
            this.orificeAdminGroupBox.Location = new System.Drawing.Point(1084, 660);
            this.orificeAdminGroupBox.Name = "orificeAdminGroupBox";
            this.orificeAdminGroupBox.Size = new System.Drawing.Size(200, 101);
            this.orificeAdminGroupBox.TabIndex = 13;
            this.orificeAdminGroupBox.TabStop = false;
            this.orificeAdminGroupBox.Text = "Admin";
            // 
            // modelTypicalTextbox
            // 
            this.modelTypicalTextbox.Location = new System.Drawing.Point(91, 21);
            this.modelTypicalTextbox.Name = "modelTypicalTextbox";
            this.modelTypicalTextbox.Size = new System.Drawing.Size(100, 20);
            this.modelTypicalTextbox.TabIndex = 6;
            this.modelTypicalTextbox.Text = "12034";
            this.modelTypicalTextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(12, 24);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(73, 13);
            this.label34.TabIndex = 1;
            this.label34.Text = "Model Typical";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(12, 50);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(74, 13);
            this.label35.TabIndex = 2;
            this.label35.Text = "Model Orifices";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(45, 76);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(39, 13);
            this.label37.TabIndex = 4;
            this.label37.Text = "Round";
            // 
            // modelOrificesTextbox
            // 
            this.modelOrificesTextbox.Location = new System.Drawing.Point(91, 47);
            this.modelOrificesTextbox.Name = "modelOrificesTextbox";
            this.modelOrificesTextbox.Size = new System.Drawing.Size(100, 20);
            this.modelOrificesTextbox.TabIndex = 7;
            this.modelOrificesTextbox.Text = "16000";
            this.modelOrificesTextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // roundTextBox
            // 
            this.roundTextBox.Location = new System.Drawing.Point(91, 73);
            this.roundTextBox.Name = "roundTextBox";
            this.roundTextBox.Size = new System.Drawing.Size(100, 20);
            this.roundTextBox.TabIndex = 9;
            this.roundTextBox.Text = "1";
            this.roundTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // orificeTablesTab
            // 
            this.orificeTablesTab.Controls.Add(this.atmGroupBox);
            this.orificeTablesTab.Controls.Add(this.groupBox5);
            this.orificeTablesTab.Controls.Add(this.psiGroupBox);
            this.orificeTablesTab.Location = new System.Drawing.Point(4, 22);
            this.orificeTablesTab.Name = "orificeTablesTab";
            this.orificeTablesTab.Padding = new System.Windows.Forms.Padding(3);
            this.orificeTablesTab.Size = new System.Drawing.Size(1888, 989);
            this.orificeTablesTab.TabIndex = 1;
            this.orificeTablesTab.Text = "TABLES";
            this.orificeTablesTab.UseVisualStyleBackColor = true;
            // 
            // atmGroupBox
            // 
            this.atmGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.atmGroupBox.Controls.Add(this.pulseAmplitudeAtmTable);
            this.atmGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.atmGroupBox.Location = new System.Drawing.Point(9, 477);
            this.atmGroupBox.Name = "atmGroupBox";
            this.atmGroupBox.Size = new System.Drawing.Size(714, 504);
            this.atmGroupBox.TabIndex = 2;
            this.atmGroupBox.TabStop = false;
            this.atmGroupBox.Text = "Pulse Amplitude at Tool (ATM), MW = X.X SG";
            // 
            // pulseAmplitudeAtmTable
            // 
            this.pulseAmplitudeAtmTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.pulseAmplitudeAtmTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pulseAmplitudeAtmTable.Location = new System.Drawing.Point(3, 19);
            this.pulseAmplitudeAtmTable.Name = "pulseAmplitudeAtmTable";
            this.pulseAmplitudeAtmTable.RowHeadersWidth = 62;
            this.pulseAmplitudeAtmTable.Size = new System.Drawing.Size(708, 482);
            this.pulseAmplitudeAtmTable.TabIndex = 0;
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox5.Controls.Add(this.pulserSpecTable);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.groupBox5.Location = new System.Drawing.Point(735, 106);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(758, 123);
            this.groupBox5.TabIndex = 11;
            this.groupBox5.TabStop = false;
            // 
            // pulserSpecTable
            // 
            this.pulserSpecTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.pulserSpecTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pulserSpecTable.Location = new System.Drawing.Point(3, 19);
            this.pulserSpecTable.Name = "pulserSpecTable";
            this.pulserSpecTable.RowHeadersWidth = 62;
            this.pulserSpecTable.Size = new System.Drawing.Size(752, 101);
            this.pulserSpecTable.TabIndex = 1;
            // 
            // psiGroupBox
            // 
            this.psiGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.psiGroupBox.Controls.Add(this.pulseAmplitudePSITable);
            this.psiGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.psiGroupBox.Location = new System.Drawing.Point(6, 6);
            this.psiGroupBox.Name = "psiGroupBox";
            this.psiGroupBox.Size = new System.Drawing.Size(714, 530);
            this.psiGroupBox.TabIndex = 1;
            this.psiGroupBox.TabStop = false;
            this.psiGroupBox.Text = "Pulse Amplitude at Tool (psi), MW = X.X PPG";
            // 
            // pulseAmplitudePSITable
            // 
            this.pulseAmplitudePSITable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.pulseAmplitudePSITable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pulseAmplitudePSITable.Location = new System.Drawing.Point(3, 19);
            this.pulseAmplitudePSITable.Name = "pulseAmplitudePSITable";
            this.pulseAmplitudePSITable.RowHeadersWidth = 62;
            this.pulseAmplitudePSITable.Size = new System.Drawing.Size(708, 508);
            this.pulseAmplitudePSITable.TabIndex = 0;
            // 
            // rssCalcTab
            // 
            this.rssCalcTab.Controls.Add(this.groupBox1);
            this.rssCalcTab.Controls.Add(this.groupBox10);
            this.rssCalcTab.Controls.Add(this.groupBox9);
            this.rssCalcTab.Controls.Add(this.groupBox8);
            this.rssCalcTab.Controls.Add(this.groupBox7);
            this.rssCalcTab.Controls.Add(this.groupBox3);
            this.rssCalcTab.Location = new System.Drawing.Point(4, 22);
            this.rssCalcTab.Name = "rssCalcTab";
            this.rssCalcTab.Size = new System.Drawing.Size(1896, 1015);
            this.rssCalcTab.TabIndex = 2;
            this.rssCalcTab.Text = "RSS CALCULATOR";
            this.rssCalcTab.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label61);
            this.groupBox1.Controls.Add(this.motorBypassPercentBox);
            this.groupBox1.Location = new System.Drawing.Point(691, 188);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(271, 174);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "MOTOR BYPASS";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(48, 77);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(66, 13);
            this.label61.TabIndex = 1;
            this.label61.Text = "BYPASS (%)";
            // 
            // motorBypassPercentBox
            // 
            this.motorBypassPercentBox.Location = new System.Drawing.Point(114, 74);
            this.motorBypassPercentBox.Name = "motorBypassPercentBox";
            this.motorBypassPercentBox.Size = new System.Drawing.Size(100, 20);
            this.motorBypassPercentBox.TabIndex = 0;
            this.motorBypassPercentBox.Text = "0";
            this.motorBypassPercentBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.motorBypassPercentBox.TextChanged += new System.EventHandler(this.motorBypassPercentBox_TextChanged);
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.richTextBox1);
            this.groupBox10.Location = new System.Drawing.Point(682, 368);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(280, 179);
            this.groupBox10.TabIndex = 10;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "INSTRUCTIONS";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox1.Location = new System.Drawing.Point(3, 16);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(274, 160);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = resources.GetString("richTextBox1.Text");
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.fourThreeQuarterToolSizeBox);
            this.groupBox9.Controls.Add(this.rssResetButton);
            this.groupBox9.Controls.Add(this.rssCalculateButton);
            this.groupBox9.Location = new System.Drawing.Point(691, 3);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(280, 174);
            this.groupBox9.TabIndex = 42;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "PROCESSING";
            // 
            // fourThreeQuarterToolSizeBox
            // 
            this.fourThreeQuarterToolSizeBox.AutoSize = true;
            this.fourThreeQuarterToolSizeBox.Checked = true;
            this.fourThreeQuarterToolSizeBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.fourThreeQuarterToolSizeBox.Location = new System.Drawing.Point(91, 125);
            this.fourThreeQuarterToolSizeBox.Name = "fourThreeQuarterToolSizeBox";
            this.fourThreeQuarterToolSizeBox.Size = new System.Drawing.Size(99, 17);
            this.fourThreeQuarterToolSizeBox.TabIndex = 2;
            this.fourThreeQuarterToolSizeBox.Text = "4 3/4 Tool Size";
            this.fourThreeQuarterToolSizeBox.UseVisualStyleBackColor = true;
            this.fourThreeQuarterToolSizeBox.CheckedChanged += new System.EventHandler(this.solenoidPressureCompBox_CheckedChanged);
            // 
            // rssResetButton
            // 
            this.rssResetButton.Location = new System.Drawing.Point(157, 81);
            this.rssResetButton.Name = "rssResetButton";
            this.rssResetButton.Size = new System.Drawing.Size(75, 23);
            this.rssResetButton.TabIndex = 1;
            this.rssResetButton.Text = "RESET";
            this.rssResetButton.UseVisualStyleBackColor = true;
            this.rssResetButton.Click += new System.EventHandler(this.rssResetButton_Click);
            // 
            // rssCalculateButton
            // 
            this.rssCalculateButton.Location = new System.Drawing.Point(41, 81);
            this.rssCalculateButton.Name = "rssCalculateButton";
            this.rssCalculateButton.Size = new System.Drawing.Size(81, 23);
            this.rssCalculateButton.TabIndex = 0;
            this.rssCalculateButton.Text = "CALCULATE";
            this.rssCalculateButton.UseVisualStyleBackColor = true;
            this.rssCalculateButton.Click += new System.EventHandler(this.rssCalculateButton_Click);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.tfaUnits);
            this.groupBox8.Controls.Add(this.nozzleTFA);
            this.groupBox8.Controls.Add(this.label59);
            this.groupBox8.Controls.Add(this.nozzle8Units);
            this.groupBox8.Controls.Add(this.nozzle8Size);
            this.groupBox8.Controls.Add(this.label58);
            this.groupBox8.Controls.Add(this.nozzle7Units);
            this.groupBox8.Controls.Add(this.nozzle7Size);
            this.groupBox8.Controls.Add(this.label57);
            this.groupBox8.Controls.Add(this.nozzle6Units);
            this.groupBox8.Controls.Add(this.nozzle6Size);
            this.groupBox8.Controls.Add(this.label56);
            this.groupBox8.Controls.Add(this.nozzle5Units);
            this.groupBox8.Controls.Add(this.nozzle5Size);
            this.groupBox8.Controls.Add(this.label55);
            this.groupBox8.Controls.Add(this.nozzle4Units);
            this.groupBox8.Controls.Add(this.nozzle4Size);
            this.groupBox8.Controls.Add(this.label54);
            this.groupBox8.Controls.Add(this.nozzle3Units);
            this.groupBox8.Controls.Add(this.nozzle3Size);
            this.groupBox8.Controls.Add(this.label46);
            this.groupBox8.Controls.Add(this.nozzle2Units);
            this.groupBox8.Controls.Add(this.nozzle1Units);
            this.groupBox8.Controls.Add(this.nozzle2Size);
            this.groupBox8.Controls.Add(this.nozzle1Size);
            this.groupBox8.Controls.Add(this.label45);
            this.groupBox8.Controls.Add(this.label50);
            this.groupBox8.Controls.Add(this.label51);
            this.groupBox8.Controls.Add(this.label52);
            this.groupBox8.Controls.Add(this.label53);
            this.groupBox8.Location = new System.Drawing.Point(8, 368);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(668, 461);
            this.groupBox8.TabIndex = 15;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "BIT NOZZLES";
            // 
            // tfaUnits
            // 
            this.tfaUnits.Location = new System.Drawing.Point(381, 413);
            this.tfaUnits.Name = "tfaUnits";
            this.tfaUnits.Size = new System.Drawing.Size(100, 20);
            this.tfaUnits.TabIndex = 36;
            this.tfaUnits.Text = "IN^2";
            this.tfaUnits.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // nozzleTFA
            // 
            this.nozzleTFA.Location = new System.Drawing.Point(258, 413);
            this.nozzleTFA.Name = "nozzleTFA";
            this.nozzleTFA.Size = new System.Drawing.Size(100, 20);
            this.nozzleTFA.TabIndex = 34;
            this.nozzleTFA.Text = "9";
            this.nozzleTFA.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(187, 413);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(27, 13);
            this.label59.TabIndex = 32;
            this.label59.Text = "TFA";
            // 
            // nozzle8Units
            // 
            this.nozzle8Units.Location = new System.Drawing.Point(381, 369);
            this.nozzle8Units.Name = "nozzle8Units";
            this.nozzle8Units.Size = new System.Drawing.Size(100, 20);
            this.nozzle8Units.TabIndex = 31;
            this.nozzle8Units.Text = "1/32 in";
            // 
            // nozzle8Size
            // 
            this.nozzle8Size.Location = new System.Drawing.Point(258, 369);
            this.nozzle8Size.Name = "nozzle8Size";
            this.nozzle8Size.Size = new System.Drawing.Size(100, 20);
            this.nozzle8Size.TabIndex = 30;
            this.nozzle8Size.Text = "0";
            this.nozzle8Size.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(155, 372);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(59, 13);
            this.label58.TabIndex = 29;
            this.label58.Text = "NOZZLE 8";
            // 
            // nozzle7Units
            // 
            this.nozzle7Units.Location = new System.Drawing.Point(381, 328);
            this.nozzle7Units.Name = "nozzle7Units";
            this.nozzle7Units.Size = new System.Drawing.Size(100, 20);
            this.nozzle7Units.TabIndex = 28;
            this.nozzle7Units.Text = "1/32 in";
            // 
            // nozzle7Size
            // 
            this.nozzle7Size.Location = new System.Drawing.Point(258, 328);
            this.nozzle7Size.Name = "nozzle7Size";
            this.nozzle7Size.Size = new System.Drawing.Size(100, 20);
            this.nozzle7Size.TabIndex = 27;
            this.nozzle7Size.Text = "0";
            this.nozzle7Size.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(155, 331);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(59, 13);
            this.label57.TabIndex = 26;
            this.label57.Text = "NOZZLE 7";
            // 
            // nozzle6Units
            // 
            this.nozzle6Units.Location = new System.Drawing.Point(381, 288);
            this.nozzle6Units.Name = "nozzle6Units";
            this.nozzle6Units.Size = new System.Drawing.Size(100, 20);
            this.nozzle6Units.TabIndex = 25;
            this.nozzle6Units.Text = "1/32 in";
            // 
            // nozzle6Size
            // 
            this.nozzle6Size.Location = new System.Drawing.Point(258, 288);
            this.nozzle6Size.Name = "nozzle6Size";
            this.nozzle6Size.Size = new System.Drawing.Size(100, 20);
            this.nozzle6Size.TabIndex = 24;
            this.nozzle6Size.Text = "9";
            this.nozzle6Size.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(155, 291);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(59, 13);
            this.label56.TabIndex = 23;
            this.label56.Text = "NOZZLE 6";
            // 
            // nozzle5Units
            // 
            this.nozzle5Units.Location = new System.Drawing.Point(381, 245);
            this.nozzle5Units.Name = "nozzle5Units";
            this.nozzle5Units.Size = new System.Drawing.Size(100, 20);
            this.nozzle5Units.TabIndex = 22;
            this.nozzle5Units.Text = "1/32 in";
            // 
            // nozzle5Size
            // 
            this.nozzle5Size.Location = new System.Drawing.Point(258, 245);
            this.nozzle5Size.Name = "nozzle5Size";
            this.nozzle5Size.Size = new System.Drawing.Size(100, 20);
            this.nozzle5Size.TabIndex = 21;
            this.nozzle5Size.Text = "9";
            this.nozzle5Size.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(155, 248);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(59, 13);
            this.label55.TabIndex = 20;
            this.label55.Text = "NOZZLE 5";
            // 
            // nozzle4Units
            // 
            this.nozzle4Units.Location = new System.Drawing.Point(381, 201);
            this.nozzle4Units.Name = "nozzle4Units";
            this.nozzle4Units.Size = new System.Drawing.Size(100, 20);
            this.nozzle4Units.TabIndex = 19;
            this.nozzle4Units.Text = "1/32 in";
            // 
            // nozzle4Size
            // 
            this.nozzle4Size.Location = new System.Drawing.Point(258, 201);
            this.nozzle4Size.Name = "nozzle4Size";
            this.nozzle4Size.Size = new System.Drawing.Size(100, 20);
            this.nozzle4Size.TabIndex = 18;
            this.nozzle4Size.Text = "9";
            this.nozzle4Size.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(155, 204);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(59, 13);
            this.label54.TabIndex = 17;
            this.label54.Text = "NOZZLE 4";
            // 
            // nozzle3Units
            // 
            this.nozzle3Units.Location = new System.Drawing.Point(381, 159);
            this.nozzle3Units.Name = "nozzle3Units";
            this.nozzle3Units.Size = new System.Drawing.Size(100, 20);
            this.nozzle3Units.TabIndex = 16;
            this.nozzle3Units.Text = "1/32 in";
            // 
            // nozzle3Size
            // 
            this.nozzle3Size.Location = new System.Drawing.Point(258, 159);
            this.nozzle3Size.Name = "nozzle3Size";
            this.nozzle3Size.Size = new System.Drawing.Size(100, 20);
            this.nozzle3Size.TabIndex = 15;
            this.nozzle3Size.Text = "8";
            this.nozzle3Size.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(155, 162);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(59, 13);
            this.label46.TabIndex = 14;
            this.label46.Text = "NOZZLE 3";
            // 
            // nozzle2Units
            // 
            this.nozzle2Units.Location = new System.Drawing.Point(381, 115);
            this.nozzle2Units.Name = "nozzle2Units";
            this.nozzle2Units.Size = new System.Drawing.Size(100, 20);
            this.nozzle2Units.TabIndex = 13;
            this.nozzle2Units.Text = "1/32 in";
            // 
            // nozzle1Units
            // 
            this.nozzle1Units.AutoCompleteCustomSource.AddRange(new string[] {
            "PPG",
            "SG",
            "KG/M^3",
            "LB/FT^3",
            "G/CM^3"});
            this.nozzle1Units.FormattingEnabled = true;
            this.nozzle1Units.Items.AddRange(new object[] {
            "1/32 in",
            "MM"});
            this.nozzle1Units.Location = new System.Drawing.Point(381, 68);
            this.nozzle1Units.Name = "nozzle1Units";
            this.nozzle1Units.Size = new System.Drawing.Size(121, 21);
            this.nozzle1Units.TabIndex = 12;
            this.nozzle1Units.Text = "1/32 in";
            this.nozzle1Units.SelectedIndexChanged += new System.EventHandler(this.nozzle1Units_SelectedIndexChanged);
            // 
            // nozzle2Size
            // 
            this.nozzle2Size.Location = new System.Drawing.Point(258, 115);
            this.nozzle2Size.Name = "nozzle2Size";
            this.nozzle2Size.Size = new System.Drawing.Size(100, 20);
            this.nozzle2Size.TabIndex = 10;
            this.nozzle2Size.Text = "8";
            this.nozzle2Size.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // nozzle1Size
            // 
            this.nozzle1Size.Location = new System.Drawing.Point(258, 68);
            this.nozzle1Size.Name = "nozzle1Size";
            this.nozzle1Size.Size = new System.Drawing.Size(100, 20);
            this.nozzle1Size.TabIndex = 7;
            this.nozzle1Size.Text = "8";
            this.nozzle1Size.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(419, 36);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(40, 13);
            this.label45.TabIndex = 5;
            this.label45.Text = "UNITS";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(270, 35);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(77, 13);
            this.label50.TabIndex = 3;
            this.label50.Text = "NOZZLE SIZE";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(158, 36);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(60, 13);
            this.label51.TabIndex = 2;
            this.label51.Text = "NOZZLE #";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(155, 118);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(59, 13);
            this.label52.TabIndex = 1;
            this.label52.Text = "NOZZLE 2";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(158, 75);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(59, 13);
            this.label53.TabIndex = 0;
            this.label53.Text = "NOZZLE 1";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.pinRestrictTFAUnitsBox);
            this.groupBox7.Controls.Add(this.pinRestrictNozzleUnit);
            this.groupBox7.Controls.Add(this.pinRestrictTFABox);
            this.groupBox7.Controls.Add(this.pinRestrictNozzleSize);
            this.groupBox7.Controls.Add(this.label44);
            this.groupBox7.Controls.Add(this.label47);
            this.groupBox7.Controls.Add(this.label48);
            this.groupBox7.Controls.Add(this.label49);
            this.groupBox7.Location = new System.Drawing.Point(8, 188);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(668, 174);
            this.groupBox7.TabIndex = 14;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "PIN RESTRICTOR";
            // 
            // pinRestrictTFAUnitsBox
            // 
            this.pinRestrictTFAUnitsBox.Location = new System.Drawing.Point(264, 115);
            this.pinRestrictTFAUnitsBox.Name = "pinRestrictTFAUnitsBox";
            this.pinRestrictTFAUnitsBox.Size = new System.Drawing.Size(121, 20);
            this.pinRestrictTFAUnitsBox.TabIndex = 13;
            this.pinRestrictTFAUnitsBox.Text = "IN^2";
            // 
            // pinRestrictNozzleUnit
            // 
            this.pinRestrictNozzleUnit.AutoCompleteCustomSource.AddRange(new string[] {
            "PPG",
            "SG",
            "KG/M^3",
            "LB/FT^3",
            "G/CM^3"});
            this.pinRestrictNozzleUnit.FormattingEnabled = true;
            this.pinRestrictNozzleUnit.Items.AddRange(new object[] {
            "1/32 in",
            "MM"});
            this.pinRestrictNozzleUnit.Location = new System.Drawing.Point(264, 67);
            this.pinRestrictNozzleUnit.Name = "pinRestrictNozzleUnit";
            this.pinRestrictNozzleUnit.Size = new System.Drawing.Size(121, 21);
            this.pinRestrictNozzleUnit.TabIndex = 12;
            this.pinRestrictNozzleUnit.Text = "1/32 in";
            this.pinRestrictNozzleUnit.SelectedIndexChanged += new System.EventHandler(this.pinRestrictNozzleUnit_SelectedIndexChanged);
            // 
            // pinRestrictTFABox
            // 
            this.pinRestrictTFABox.Location = new System.Drawing.Point(136, 115);
            this.pinRestrictTFABox.Name = "pinRestrictTFABox";
            this.pinRestrictTFABox.Size = new System.Drawing.Size(100, 20);
            this.pinRestrictTFABox.TabIndex = 9;
            this.pinRestrictTFABox.Text = "0.000";
            this.pinRestrictTFABox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pinRestrictNozzleSize
            // 
            this.pinRestrictNozzleSize.Location = new System.Drawing.Point(136, 68);
            this.pinRestrictNozzleSize.Name = "pinRestrictNozzleSize";
            this.pinRestrictNozzleSize.Size = new System.Drawing.Size(100, 20);
            this.pinRestrictNozzleSize.TabIndex = 6;
            this.pinRestrictNozzleSize.Text = "0";
            this.pinRestrictNozzleSize.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.pinRestrictNozzleSize.TextChanged += new System.EventHandler(this.pinRestrictNozzleSize_TextChanged);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(302, 35);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(40, 13);
            this.label44.TabIndex = 5;
            this.label44.Text = "UNITS";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(154, 35);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(77, 13);
            this.label47.TabIndex = 2;
            this.label47.Text = "NOZZLE SIZE";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(77, 118);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(27, 13);
            this.label48.TabIndex = 1;
            this.label48.Text = "TFA";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(44, 70);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(77, 13);
            this.label49.TabIndex = 0;
            this.label49.Text = "NOZZLE SIZE";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.pressureDropUnits);
            this.groupBox3.Controls.Add(this.pressureDrop3Box);
            this.groupBox3.Controls.Add(this.flowRateUnitBox);
            this.groupBox3.Controls.Add(this.mudWeightUnitsBox);
            this.groupBox3.Controls.Add(this.pressureDrop2Box);
            this.groupBox3.Controls.Add(this.flowRateSection3);
            this.groupBox3.Controls.Add(this.flowRateSection2);
            this.groupBox3.Controls.Add(this.pressureDrop1Box);
            this.groupBox3.Controls.Add(this.flowRateSection1);
            this.groupBox3.Controls.Add(this.mudWeightSection3);
            this.groupBox3.Controls.Add(this.label60);
            this.groupBox3.Controls.Add(this.mudWeightSection2);
            this.groupBox3.Controls.Add(this.mudWeightSection1);
            this.groupBox3.Controls.Add(this.label43);
            this.groupBox3.Controls.Add(this.label42);
            this.groupBox3.Controls.Add(this.label41);
            this.groupBox3.Controls.Add(this.label40);
            this.groupBox3.Controls.Add(this.label39);
            this.groupBox3.Controls.Add(this.label38);
            this.groupBox3.Location = new System.Drawing.Point(8, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(668, 179);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "ENVIRONMENTAL PARAMETERS";
            // 
            // pressureDropUnits
            // 
            this.pressureDropUnits.AutoCompleteCustomSource.AddRange(new string[] {
            "PPG",
            "SG",
            "KG/M^3",
            "LB/FT^3",
            "G/CM^3"});
            this.pressureDropUnits.FormattingEnabled = true;
            this.pressureDropUnits.Items.AddRange(new object[] {
            "PSI",
            "ATM",
            "BAR",
            "kPa"});
            this.pressureDropUnits.Location = new System.Drawing.Point(510, 144);
            this.pressureDropUnits.Name = "pressureDropUnits";
            this.pressureDropUnits.Size = new System.Drawing.Size(121, 21);
            this.pressureDropUnits.TabIndex = 41;
            this.pressureDropUnits.Text = "ATM";
            this.pressureDropUnits.SelectedIndexChanged += new System.EventHandler(this.pressureDropUnits_SelectedIndexChanged);
            // 
            // pressureDrop3Box
            // 
            this.pressureDrop3Box.Location = new System.Drawing.Point(381, 145);
            this.pressureDrop3Box.Name = "pressureDrop3Box";
            this.pressureDrop3Box.Size = new System.Drawing.Size(100, 20);
            this.pressureDrop3Box.TabIndex = 40;
            this.pressureDrop3Box.Text = "24";
            this.pressureDrop3Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // flowRateUnitBox
            // 
            this.flowRateUnitBox.AutoCompleteCustomSource.AddRange(new string[] {
            "PPG",
            "SG",
            "KG/M^3",
            "LB/FT^3",
            "G/CM^3"});
            this.flowRateUnitBox.FormattingEnabled = true;
            this.flowRateUnitBox.Items.AddRange(new object[] {
            "GPM",
            "LPS",
            "LPM"});
            this.flowRateUnitBox.Location = new System.Drawing.Point(510, 101);
            this.flowRateUnitBox.Name = "flowRateUnitBox";
            this.flowRateUnitBox.Size = new System.Drawing.Size(121, 21);
            this.flowRateUnitBox.TabIndex = 13;
            this.flowRateUnitBox.Text = "LPS";
            this.flowRateUnitBox.SelectedIndexChanged += new System.EventHandler(this.flowRateUnitBox_SelectedIndexChanged);
            // 
            // mudWeightUnitsBox
            // 
            this.mudWeightUnitsBox.AutoCompleteCustomSource.AddRange(new string[] {
            "PPG",
            "SG",
            "KG/M^3",
            "LB/FT^3",
            "G/CM^3"});
            this.mudWeightUnitsBox.FormattingEnabled = true;
            this.mudWeightUnitsBox.Items.AddRange(new object[] {
            "PPG",
            "SG",
            "KG/M^3",
            "LB/FT^3",
            "G/CM^3"});
            this.mudWeightUnitsBox.Location = new System.Drawing.Point(510, 54);
            this.mudWeightUnitsBox.Name = "mudWeightUnitsBox";
            this.mudWeightUnitsBox.Size = new System.Drawing.Size(121, 21);
            this.mudWeightUnitsBox.TabIndex = 12;
            this.mudWeightUnitsBox.Text = "G/CM^3";
            this.mudWeightUnitsBox.SelectedIndexChanged += new System.EventHandler(this.mudWeightUnitsBox_SelectedIndexChanged);
            // 
            // pressureDrop2Box
            // 
            this.pressureDrop2Box.Location = new System.Drawing.Point(258, 145);
            this.pressureDrop2Box.Name = "pressureDrop2Box";
            this.pressureDrop2Box.Size = new System.Drawing.Size(100, 20);
            this.pressureDrop2Box.TabIndex = 39;
            this.pressureDrop2Box.Text = "24";
            this.pressureDrop2Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // flowRateSection3
            // 
            this.flowRateSection3.Location = new System.Drawing.Point(381, 102);
            this.flowRateSection3.Name = "flowRateSection3";
            this.flowRateSection3.Size = new System.Drawing.Size(100, 20);
            this.flowRateSection3.TabIndex = 11;
            this.flowRateSection3.Text = "14";
            this.flowRateSection3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // flowRateSection2
            // 
            this.flowRateSection2.Location = new System.Drawing.Point(258, 102);
            this.flowRateSection2.Name = "flowRateSection2";
            this.flowRateSection2.Size = new System.Drawing.Size(100, 20);
            this.flowRateSection2.TabIndex = 10;
            this.flowRateSection2.Text = "14";
            this.flowRateSection2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pressureDrop1Box
            // 
            this.pressureDrop1Box.Location = new System.Drawing.Point(136, 145);
            this.pressureDrop1Box.Name = "pressureDrop1Box";
            this.pressureDrop1Box.Size = new System.Drawing.Size(100, 20);
            this.pressureDrop1Box.TabIndex = 38;
            this.pressureDrop1Box.Text = "21";
            this.pressureDrop1Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // flowRateSection1
            // 
            this.flowRateSection1.Location = new System.Drawing.Point(136, 102);
            this.flowRateSection1.Name = "flowRateSection1";
            this.flowRateSection1.Size = new System.Drawing.Size(100, 20);
            this.flowRateSection1.TabIndex = 9;
            this.flowRateSection1.Text = "13";
            this.flowRateSection1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // mudWeightSection3
            // 
            this.mudWeightSection3.Location = new System.Drawing.Point(381, 55);
            this.mudWeightSection3.Name = "mudWeightSection3";
            this.mudWeightSection3.Size = new System.Drawing.Size(100, 20);
            this.mudWeightSection3.TabIndex = 8;
            this.mudWeightSection3.Text = "1.3";
            this.mudWeightSection3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(16, 148);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(100, 13);
            this.label60.TabIndex = 37;
            this.label60.Text = "PRESSURE DROP";
            // 
            // mudWeightSection2
            // 
            this.mudWeightSection2.Location = new System.Drawing.Point(258, 55);
            this.mudWeightSection2.Name = "mudWeightSection2";
            this.mudWeightSection2.Size = new System.Drawing.Size(100, 20);
            this.mudWeightSection2.TabIndex = 7;
            this.mudWeightSection2.Text = "1.3";
            this.mudWeightSection2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // mudWeightSection1
            // 
            this.mudWeightSection1.Location = new System.Drawing.Point(136, 55);
            this.mudWeightSection1.Name = "mudWeightSection1";
            this.mudWeightSection1.Size = new System.Drawing.Size(100, 20);
            this.mudWeightSection1.TabIndex = 6;
            this.mudWeightSection1.Text = "1.3";
            this.mudWeightSection1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(548, 22);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(40, 13);
            this.label43.TabIndex = 5;
            this.label43.Text = "UNITS";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(402, 22);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(63, 13);
            this.label42.TabIndex = 4;
            this.label42.Text = "SECTION 3";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(279, 22);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(63, 13);
            this.label41.TabIndex = 3;
            this.label41.Text = "SECTION 2";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(154, 22);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(63, 13);
            this.label40.TabIndex = 2;
            this.label40.Text = "SECTION 1";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(34, 105);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(70, 13);
            this.label39.TabIndex = 1;
            this.label39.Text = "FLOW RATE";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(25, 58);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(79, 13);
            this.label38.TabIndex = 0;
            this.label38.Text = "MUD WEIGHT";
            // 
            // sensorCalTab
            // 
            this.sensorCalTab.Controls.Add(this.readGammaButton);
            this.sensorCalTab.Controls.Add(this.tableLayoutPanel2);
            this.sensorCalTab.Controls.Add(this.timerTextBox);
            this.sensorCalTab.Controls.Add(this.groupBox12);
            this.sensorCalTab.Controls.Add(this.groupBox11);
            this.sensorCalTab.Controls.Add(this.rmsBoxCheckBox);
            this.sensorCalTab.Controls.Add(this.qbusCheckBox);
            this.sensorCalTab.Controls.Add(this.alt_open_btn);
            this.sensorCalTab.Controls.Add(this.label64);
            this.sensorCalTab.Controls.Add(this.comport);
            this.sensorCalTab.Controls.Add(this.tabControl1);
            this.sensorCalTab.Controls.Add(this.connectButton);
            this.sensorCalTab.Controls.Add(this.readDataButton);
            this.sensorCalTab.Controls.Add(this.readPowerButton);
            this.sensorCalTab.Controls.Add(this.runButton);
            this.sensorCalTab.Location = new System.Drawing.Point(4, 22);
            this.sensorCalTab.Name = "sensorCalTab";
            this.sensorCalTab.Size = new System.Drawing.Size(1896, 1015);
            this.sensorCalTab.TabIndex = 3;
            this.sensorCalTab.Text = "SENSOR CALIBRATION";
            this.sensorCalTab.UseVisualStyleBackColor = true;
            // 
            // readGammaButton
            // 
            this.readGammaButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.readGammaButton.Location = new System.Drawing.Point(699, 498);
            this.readGammaButton.Name = "readGammaButton";
            this.readGammaButton.Size = new System.Drawing.Size(136, 86);
            this.readGammaButton.TabIndex = 42;
            this.readGammaButton.Text = "READ GAMMA";
            this.readGammaButton.UseVisualStyleBackColor = true;
            this.readGammaButton.Click += new System.EventHandler(this.readGammaButton_Click);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.InsetDouble;
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.Controls.Add(this.mwdBit15, 3, 3);
            this.tableLayoutPanel2.Controls.Add(this.mwdBit14, 2, 3);
            this.tableLayoutPanel2.Controls.Add(this.mwdBit13, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.mwdBit12, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.mwdBit11, 3, 2);
            this.tableLayoutPanel2.Controls.Add(this.mwdBit10, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.mwdBit9, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.mwdBit8, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.mwdBit7, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.mwdBit6, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.mwdBit5, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.mwdBit4, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.mwdBit3, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.mwdBit2, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.mwdBit0, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.mwdBit1, 0, 0);
            this.tableLayoutPanel2.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(1005, 696);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(412, 176);
            this.tableLayoutPanel2.TabIndex = 14;
            // 
            // mwdBit15
            // 
            this.mwdBit15.AutoSize = true;
            this.mwdBit15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mwdBit15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.mwdBit15.Location = new System.Drawing.Point(312, 132);
            this.mwdBit15.Name = "mwdBit15";
            this.mwdBit15.Size = new System.Drawing.Size(94, 41);
            this.mwdBit15.TabIndex = 15;
            this.mwdBit15.Text = "BIT15";
            this.mwdBit15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // mwdBit14
            // 
            this.mwdBit14.AutoSize = true;
            this.mwdBit14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mwdBit14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.mwdBit14.Location = new System.Drawing.Point(210, 132);
            this.mwdBit14.Name = "mwdBit14";
            this.mwdBit14.Size = new System.Drawing.Size(93, 41);
            this.mwdBit14.TabIndex = 14;
            this.mwdBit14.Text = "BIT14";
            this.mwdBit14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // mwdBit13
            // 
            this.mwdBit13.AutoSize = true;
            this.mwdBit13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mwdBit13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.mwdBit13.Location = new System.Drawing.Point(108, 132);
            this.mwdBit13.Name = "mwdBit13";
            this.mwdBit13.Size = new System.Drawing.Size(93, 41);
            this.mwdBit13.TabIndex = 13;
            this.mwdBit13.Text = "BIT13";
            this.mwdBit13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // mwdBit12
            // 
            this.mwdBit12.AutoSize = true;
            this.mwdBit12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mwdBit12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.mwdBit12.Location = new System.Drawing.Point(6, 132);
            this.mwdBit12.Name = "mwdBit12";
            this.mwdBit12.Size = new System.Drawing.Size(93, 41);
            this.mwdBit12.TabIndex = 12;
            this.mwdBit12.Text = "BIT12";
            this.mwdBit12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // mwdBit11
            // 
            this.mwdBit11.AutoSize = true;
            this.mwdBit11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mwdBit11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.mwdBit11.Location = new System.Drawing.Point(312, 89);
            this.mwdBit11.Name = "mwdBit11";
            this.mwdBit11.Size = new System.Drawing.Size(94, 40);
            this.mwdBit11.TabIndex = 11;
            this.mwdBit11.Text = "BIT11";
            this.mwdBit11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // mwdBit10
            // 
            this.mwdBit10.AutoSize = true;
            this.mwdBit10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mwdBit10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.mwdBit10.Location = new System.Drawing.Point(210, 89);
            this.mwdBit10.Name = "mwdBit10";
            this.mwdBit10.Size = new System.Drawing.Size(93, 40);
            this.mwdBit10.TabIndex = 10;
            this.mwdBit10.Text = "BIT10";
            this.mwdBit10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // mwdBit9
            // 
            this.mwdBit9.AutoSize = true;
            this.mwdBit9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mwdBit9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.mwdBit9.Location = new System.Drawing.Point(108, 89);
            this.mwdBit9.Name = "mwdBit9";
            this.mwdBit9.Size = new System.Drawing.Size(93, 40);
            this.mwdBit9.TabIndex = 9;
            this.mwdBit9.Text = "BIT9";
            this.mwdBit9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // mwdBit8
            // 
            this.mwdBit8.AutoSize = true;
            this.mwdBit8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mwdBit8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.mwdBit8.Location = new System.Drawing.Point(6, 89);
            this.mwdBit8.Name = "mwdBit8";
            this.mwdBit8.Size = new System.Drawing.Size(93, 40);
            this.mwdBit8.TabIndex = 8;
            this.mwdBit8.Text = "BIT8";
            this.mwdBit8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // mwdBit7
            // 
            this.mwdBit7.AutoSize = true;
            this.mwdBit7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mwdBit7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.mwdBit7.Location = new System.Drawing.Point(312, 46);
            this.mwdBit7.Name = "mwdBit7";
            this.mwdBit7.Size = new System.Drawing.Size(94, 40);
            this.mwdBit7.TabIndex = 7;
            this.mwdBit7.Text = "BIT7";
            this.mwdBit7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // mwdBit6
            // 
            this.mwdBit6.AutoSize = true;
            this.mwdBit6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mwdBit6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.mwdBit6.Location = new System.Drawing.Point(210, 46);
            this.mwdBit6.Name = "mwdBit6";
            this.mwdBit6.Size = new System.Drawing.Size(93, 40);
            this.mwdBit6.TabIndex = 6;
            this.mwdBit6.Text = "BIT6";
            this.mwdBit6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // mwdBit5
            // 
            this.mwdBit5.AutoSize = true;
            this.mwdBit5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mwdBit5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.mwdBit5.Location = new System.Drawing.Point(108, 46);
            this.mwdBit5.Name = "mwdBit5";
            this.mwdBit5.Size = new System.Drawing.Size(93, 40);
            this.mwdBit5.TabIndex = 5;
            this.mwdBit5.Text = "BIT5";
            this.mwdBit5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // mwdBit4
            // 
            this.mwdBit4.AutoSize = true;
            this.mwdBit4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mwdBit4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F);
            this.mwdBit4.Location = new System.Drawing.Point(6, 46);
            this.mwdBit4.Name = "mwdBit4";
            this.mwdBit4.Size = new System.Drawing.Size(93, 40);
            this.mwdBit4.TabIndex = 4;
            this.mwdBit4.Text = "BIT4";
            this.mwdBit4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // mwdBit3
            // 
            this.mwdBit3.AutoSize = true;
            this.mwdBit3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mwdBit3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.mwdBit3.Location = new System.Drawing.Point(312, 3);
            this.mwdBit3.Name = "mwdBit3";
            this.mwdBit3.Size = new System.Drawing.Size(94, 40);
            this.mwdBit3.TabIndex = 3;
            this.mwdBit3.Text = "BIT3";
            this.mwdBit3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // mwdBit2
            // 
            this.mwdBit2.AutoSize = true;
            this.mwdBit2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mwdBit2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.mwdBit2.Location = new System.Drawing.Point(210, 3);
            this.mwdBit2.Name = "mwdBit2";
            this.mwdBit2.Size = new System.Drawing.Size(93, 40);
            this.mwdBit2.TabIndex = 2;
            this.mwdBit2.Text = "BIT2";
            this.mwdBit2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // mwdBit0
            // 
            this.mwdBit0.AutoSize = true;
            this.mwdBit0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mwdBit0.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.mwdBit0.Location = new System.Drawing.Point(6, 3);
            this.mwdBit0.Name = "mwdBit0";
            this.mwdBit0.Size = new System.Drawing.Size(93, 40);
            this.mwdBit0.TabIndex = 1;
            this.mwdBit0.Text = "BIT0";
            this.mwdBit0.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // mwdBit1
            // 
            this.mwdBit1.AutoSize = true;
            this.mwdBit1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mwdBit1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.mwdBit1.Location = new System.Drawing.Point(108, 3);
            this.mwdBit1.Name = "mwdBit1";
            this.mwdBit1.Size = new System.Drawing.Size(93, 40);
            this.mwdBit1.TabIndex = 0;
            this.mwdBit1.Text = "BIT1";
            this.mwdBit1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // timerTextBox
            // 
            this.timerTextBox.BackColor = System.Drawing.Color.Black;
            this.timerTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 24.25F);
            this.timerTextBox.ForeColor = System.Drawing.Color.Yellow;
            this.timerTextBox.Location = new System.Drawing.Point(664, 25);
            this.timerTextBox.Name = "timerTextBox";
            this.timerTextBox.Size = new System.Drawing.Size(207, 86);
            this.timerTextBox.TabIndex = 41;
            this.timerTextBox.Text = "";
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.ReceivingBox);
            this.groupBox12.Location = new System.Drawing.Point(915, 478);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(605, 178);
            this.groupBox12.TabIndex = 13;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "TX/RX";
            // 
            // ReceivingBox
            // 
            this.ReceivingBox.BackColor = System.Drawing.Color.Black;
            this.ReceivingBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ReceivingBox.ForeColor = System.Drawing.Color.Yellow;
            this.ReceivingBox.Location = new System.Drawing.Point(3, 16);
            this.ReceivingBox.Name = "ReceivingBox";
            this.ReceivingBox.Size = new System.Drawing.Size(599, 159);
            this.ReceivingBox.TabIndex = 3;
            this.ReceivingBox.Text = "";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.dataBox);
            this.groupBox11.Location = new System.Drawing.Point(915, 50);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(608, 422);
            this.groupBox11.TabIndex = 6;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "DATA";
            // 
            // dataBox
            // 
            this.dataBox.BackColor = System.Drawing.Color.Black;
            this.dataBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataBox.ForeColor = System.Drawing.Color.Yellow;
            this.dataBox.Location = new System.Drawing.Point(3, 16);
            this.dataBox.Name = "dataBox";
            this.dataBox.Size = new System.Drawing.Size(602, 403);
            this.dataBox.TabIndex = 1;
            this.dataBox.Text = "";
            // 
            // rmsBoxCheckBox
            // 
            this.rmsBoxCheckBox.AutoSize = true;
            this.rmsBoxCheckBox.Location = new System.Drawing.Point(1320, 18);
            this.rmsBoxCheckBox.Name = "rmsBoxCheckBox";
            this.rmsBoxCheckBox.Size = new System.Drawing.Size(98, 17);
            this.rmsBoxCheckBox.TabIndex = 5;
            this.rmsBoxCheckBox.Text = "FULL DUPLEX";
            this.rmsBoxCheckBox.UseVisualStyleBackColor = true;
            this.rmsBoxCheckBox.CheckedChanged += new System.EventHandler(this.rmsBoxCheckBox_CheckedChanged);
            // 
            // qbusCheckBox
            // 
            this.qbusCheckBox.AutoSize = true;
            this.qbusCheckBox.Location = new System.Drawing.Point(1252, 18);
            this.qbusCheckBox.Name = "qbusCheckBox";
            this.qbusCheckBox.Size = new System.Drawing.Size(56, 17);
            this.qbusCheckBox.TabIndex = 4;
            this.qbusCheckBox.Text = "QBUS";
            this.qbusCheckBox.UseVisualStyleBackColor = true;
            this.qbusCheckBox.CheckedChanged += new System.EventHandler(this.qbusCheckBox_CheckedChanged);
            // 
            // alt_open_btn
            // 
            this.alt_open_btn.Location = new System.Drawing.Point(1152, 14);
            this.alt_open_btn.Name = "alt_open_btn";
            this.alt_open_btn.Size = new System.Drawing.Size(75, 23);
            this.alt_open_btn.TabIndex = 3;
            this.alt_open_btn.Text = "OPEN";
            this.alt_open_btn.UseVisualStyleBackColor = true;
            this.alt_open_btn.Click += new System.EventHandler(this.alt_open_btn_Click);
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(982, 17);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(37, 13);
            this.label64.TabIndex = 2;
            this.label64.Text = "PORT";
            // 
            // comport
            // 
            this.comport.FormattingEnabled = true;
            this.comport.Location = new System.Drawing.Point(1025, 14);
            this.comport.Name = "comport";
            this.comport.Size = new System.Drawing.Size(121, 21);
            this.comport.TabIndex = 0;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.gammaCalTab);
            this.tabControl1.Controls.Add(this.novDmCalTab);
            this.tabControl1.Location = new System.Drawing.Point(8, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(615, 924);
            this.tabControl1.TabIndex = 0;
            // 
            // gammaCalTab
            // 
            this.gammaCalTab.Controls.Add(this.configGammaButton);
            this.gammaCalTab.Controls.Add(this.woftGammaCheckBox);
            this.gammaCalTab.Controls.Add(this.digitalGammaCheckbox);
            this.gammaCalTab.Controls.Add(this.gammaCalPanel);
            this.gammaCalTab.Controls.Add(this.saveButton);
            this.gammaCalTab.Controls.Add(this.calibratorLastFiveMinButton);
            this.gammaCalTab.Controls.Add(this.startBackGroundCpsButton);
            this.gammaCalTab.Controls.Add(this.startCalFirst5MinButton);
            this.gammaCalTab.Controls.Add(this.calculateGammaFactorButton);
            this.gammaCalTab.Location = new System.Drawing.Point(4, 22);
            this.gammaCalTab.Name = "gammaCalTab";
            this.gammaCalTab.Padding = new System.Windows.Forms.Padding(3);
            this.gammaCalTab.Size = new System.Drawing.Size(607, 898);
            this.gammaCalTab.TabIndex = 0;
            this.gammaCalTab.Text = "GAMMA";
            this.gammaCalTab.UseVisualStyleBackColor = true;
            // 
            // configGammaButton
            // 
            this.configGammaButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.configGammaButton.Location = new System.Drawing.Point(464, 790);
            this.configGammaButton.Name = "configGammaButton";
            this.configGammaButton.Size = new System.Drawing.Size(130, 60);
            this.configGammaButton.TabIndex = 46;
            this.configGammaButton.Text = "CONFIG";
            this.configGammaButton.UseVisualStyleBackColor = true;
            this.configGammaButton.Click += new System.EventHandler(this.configGammaButton_Click);
            // 
            // woftGammaCheckBox
            // 
            this.woftGammaCheckBox.AutoSize = true;
            this.woftGammaCheckBox.Checked = true;
            this.woftGammaCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.woftGammaCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.woftGammaCheckBox.Location = new System.Drawing.Point(462, 12);
            this.woftGammaCheckBox.Name = "woftGammaCheckBox";
            this.woftGammaCheckBox.Size = new System.Drawing.Size(114, 56);
            this.woftGammaCheckBox.TabIndex = 45;
            this.woftGammaCheckBox.Text = "WOFT \r\nGAMMA";
            this.woftGammaCheckBox.UseVisualStyleBackColor = true;
            this.woftGammaCheckBox.CheckedChanged += new System.EventHandler(this.woftGammaCheckBox_CheckedChanged);
            // 
            // digitalGammaCheckbox
            // 
            this.digitalGammaCheckbox.AutoSize = true;
            this.digitalGammaCheckbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.digitalGammaCheckbox.Location = new System.Drawing.Point(462, 90);
            this.digitalGammaCheckbox.Name = "digitalGammaCheckbox";
            this.digitalGammaCheckbox.Size = new System.Drawing.Size(121, 56);
            this.digitalGammaCheckbox.TabIndex = 44;
            this.digitalGammaCheckbox.Text = "DIGITAL \r\nGAMMA";
            this.digitalGammaCheckbox.UseVisualStyleBackColor = true;
            this.digitalGammaCheckbox.CheckedChanged += new System.EventHandler(this.digitalGammaCheckbox_CheckedChanged);
            // 
            // gammaCalPanel
            // 
            this.gammaCalPanel.Controls.Add(this.serialNumberBox);
            this.gammaCalPanel.Controls.Add(this.label76);
            this.gammaCalPanel.Controls.Add(this.apiCalibratorLabel);
            this.gammaCalPanel.Controls.Add(this.label66);
            this.gammaCalPanel.Controls.Add(this.firstFiveMinGammaCpsBox);
            this.gammaCalPanel.Controls.Add(this.label74);
            this.gammaCalPanel.Controls.Add(this.label67);
            this.gammaCalPanel.Controls.Add(this.netCalibratorCpsTextBox);
            this.gammaCalPanel.Controls.Add(this.gammaScalingFactorBox);
            this.gammaCalPanel.Controls.Add(this.label73);
            this.gammaCalPanel.Controls.Add(this.label69);
            this.gammaCalPanel.Controls.Add(this.avgCalibratorCpsBox);
            this.gammaCalPanel.Controls.Add(this.backgroundCpsBox);
            this.gammaCalPanel.Controls.Add(this.label70);
            this.gammaCalPanel.Controls.Add(this.label71);
            this.gammaCalPanel.Controls.Add(this.label68);
            this.gammaCalPanel.Controls.Add(this.label72);
            this.gammaCalPanel.Controls.Add(this.calibratorLastFiveMinBox);
            this.gammaCalPanel.Location = new System.Drawing.Point(16, 11);
            this.gammaCalPanel.Name = "gammaCalPanel";
            this.gammaCalPanel.Size = new System.Drawing.Size(434, 870);
            this.gammaCalPanel.TabIndex = 43;
            // 
            // serialNumberBox
            // 
            this.serialNumberBox.BackColor = System.Drawing.Color.Black;
            this.serialNumberBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.serialNumberBox.ForeColor = System.Drawing.Color.Yellow;
            this.serialNumberBox.Location = new System.Drawing.Point(169, 63);
            this.serialNumberBox.Name = "serialNumberBox";
            this.serialNumberBox.Size = new System.Drawing.Size(250, 35);
            this.serialNumberBox.TabIndex = 43;
            this.serialNumberBox.Text = "";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label76.Location = new System.Drawing.Point(14, 66);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(149, 26);
            this.label76.TabIndex = 42;
            this.label76.Text = "MODULE SN:";
            // 
            // apiCalibratorLabel
            // 
            this.apiCalibratorLabel.AutoSize = true;
            this.apiCalibratorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.apiCalibratorLabel.Location = new System.Drawing.Point(24, 14);
            this.apiCalibratorLabel.Name = "apiCalibratorLabel";
            this.apiCalibratorLabel.Size = new System.Drawing.Size(363, 26);
            this.apiCalibratorLabel.TabIndex = 41;
            this.apiCalibratorLabel.Text = "CALIBRATOR API VALUE: 224 API";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label66.Location = new System.Drawing.Point(14, 120);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(394, 26);
            this.label66.TabIndex = 21;
            this.label66.Text = "CALIBRATOR CPS FIRST 5 MINUTES";
            // 
            // firstFiveMinGammaCpsBox
            // 
            this.firstFiveMinGammaCpsBox.BackColor = System.Drawing.Color.Black;
            this.firstFiveMinGammaCpsBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 24.25F);
            this.firstFiveMinGammaCpsBox.ForeColor = System.Drawing.Color.Yellow;
            this.firstFiveMinGammaCpsBox.Location = new System.Drawing.Point(60, 163);
            this.firstFiveMinGammaCpsBox.Name = "firstFiveMinGammaCpsBox";
            this.firstFiveMinGammaCpsBox.Size = new System.Drawing.Size(200, 60);
            this.firstFiveMinGammaCpsBox.TabIndex = 20;
            this.firstFiveMinGammaCpsBox.Text = "";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label74.Location = new System.Drawing.Point(33, 591);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(253, 26);
            this.label74.TabIndex = 40;
            this.label74.Text = "NET CALIBRATOR CPS";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label67.Location = new System.Drawing.Point(266, 174);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(58, 26);
            this.label67.TabIndex = 22;
            this.label67.Text = "CPS";
            // 
            // netCalibratorCpsTextBox
            // 
            this.netCalibratorCpsTextBox.BackColor = System.Drawing.Color.Black;
            this.netCalibratorCpsTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 24.25F);
            this.netCalibratorCpsTextBox.ForeColor = System.Drawing.Color.Yellow;
            this.netCalibratorCpsTextBox.Location = new System.Drawing.Point(59, 620);
            this.netCalibratorCpsTextBox.Name = "netCalibratorCpsTextBox";
            this.netCalibratorCpsTextBox.Size = new System.Drawing.Size(200, 60);
            this.netCalibratorCpsTextBox.TabIndex = 39;
            this.netCalibratorCpsTextBox.Text = "";
            // 
            // gammaScalingFactorBox
            // 
            this.gammaScalingFactorBox.BackColor = System.Drawing.Color.Black;
            this.gammaScalingFactorBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 24.25F);
            this.gammaScalingFactorBox.ForeColor = System.Drawing.Color.Yellow;
            this.gammaScalingFactorBox.Location = new System.Drawing.Point(59, 733);
            this.gammaScalingFactorBox.Name = "gammaScalingFactorBox";
            this.gammaScalingFactorBox.Size = new System.Drawing.Size(200, 60);
            this.gammaScalingFactorBox.TabIndex = 25;
            this.gammaScalingFactorBox.Text = "";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label73.Location = new System.Drawing.Point(33, 477);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(263, 26);
            this.label73.TabIndex = 38;
            this.label73.Text = "AVG. CALIBRATOR CPS";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label69.Location = new System.Drawing.Point(24, 704);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(319, 26);
            this.label69.TabIndex = 26;
            this.label69.Text = "PROBE CAL RATIO (API/CPS)";
            // 
            // avgCalibratorCpsBox
            // 
            this.avgCalibratorCpsBox.BackColor = System.Drawing.Color.Black;
            this.avgCalibratorCpsBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 24.25F);
            this.avgCalibratorCpsBox.ForeColor = System.Drawing.Color.Yellow;
            this.avgCalibratorCpsBox.Location = new System.Drawing.Point(59, 506);
            this.avgCalibratorCpsBox.Name = "avgCalibratorCpsBox";
            this.avgCalibratorCpsBox.Size = new System.Drawing.Size(200, 60);
            this.avgCalibratorCpsBox.TabIndex = 37;
            this.avgCalibratorCpsBox.Text = "";
            // 
            // backgroundCpsBox
            // 
            this.backgroundCpsBox.BackColor = System.Drawing.Color.Black;
            this.backgroundCpsBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 24.25F);
            this.backgroundCpsBox.ForeColor = System.Drawing.Color.Yellow;
            this.backgroundCpsBox.Location = new System.Drawing.Point(59, 279);
            this.backgroundCpsBox.Name = "backgroundCpsBox";
            this.backgroundCpsBox.Size = new System.Drawing.Size(200, 60);
            this.backgroundCpsBox.TabIndex = 29;
            this.backgroundCpsBox.Text = "";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label70.Location = new System.Drawing.Point(54, 246);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(223, 26);
            this.label70.TabIndex = 30;
            this.label70.Text = "BACKGROUND CPS";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label71.Location = new System.Drawing.Point(265, 422);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(58, 26);
            this.label71.TabIndex = 35;
            this.label71.Text = "CPS";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label68.Location = new System.Drawing.Point(265, 293);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(58, 26);
            this.label68.TabIndex = 31;
            this.label68.Text = "CPS";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label72.Location = new System.Drawing.Point(33, 372);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(386, 26);
            this.label72.TabIndex = 34;
            this.label72.Text = "CALIBRATOR CPS LAST 5 MINUTES";
            // 
            // calibratorLastFiveMinBox
            // 
            this.calibratorLastFiveMinBox.BackColor = System.Drawing.Color.Black;
            this.calibratorLastFiveMinBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 24.25F);
            this.calibratorLastFiveMinBox.ForeColor = System.Drawing.Color.Yellow;
            this.calibratorLastFiveMinBox.Location = new System.Drawing.Point(59, 401);
            this.calibratorLastFiveMinBox.Name = "calibratorLastFiveMinBox";
            this.calibratorLastFiveMinBox.Size = new System.Drawing.Size(200, 60);
            this.calibratorLastFiveMinBox.TabIndex = 33;
            this.calibratorLastFiveMinBox.Text = "";
            // 
            // saveButton
            // 
            this.saveButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.saveButton.Location = new System.Drawing.Point(464, 723);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(130, 60);
            this.saveButton.TabIndex = 42;
            this.saveButton.Text = "SAVE";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // calibratorLastFiveMinButton
            // 
            this.calibratorLastFiveMinButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.calibratorLastFiveMinButton.Location = new System.Drawing.Point(462, 412);
            this.calibratorLastFiveMinButton.Name = "calibratorLastFiveMinButton";
            this.calibratorLastFiveMinButton.Size = new System.Drawing.Size(130, 60);
            this.calibratorLastFiveMinButton.TabIndex = 36;
            this.calibratorLastFiveMinButton.Text = "START";
            this.calibratorLastFiveMinButton.UseVisualStyleBackColor = true;
            this.calibratorLastFiveMinButton.Click += new System.EventHandler(this.calibratorLastFiveMinButton_Click);
            // 
            // startBackGroundCpsButton
            // 
            this.startBackGroundCpsButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.startBackGroundCpsButton.Location = new System.Drawing.Point(462, 290);
            this.startBackGroundCpsButton.Name = "startBackGroundCpsButton";
            this.startBackGroundCpsButton.Size = new System.Drawing.Size(130, 60);
            this.startBackGroundCpsButton.TabIndex = 32;
            this.startBackGroundCpsButton.Text = "START";
            this.startBackGroundCpsButton.UseVisualStyleBackColor = true;
            this.startBackGroundCpsButton.Click += new System.EventHandler(this.startBackGroundCpsButton_Click);
            // 
            // startCalFirst5MinButton
            // 
            this.startCalFirst5MinButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.startCalFirst5MinButton.Location = new System.Drawing.Point(462, 174);
            this.startCalFirst5MinButton.Name = "startCalFirst5MinButton";
            this.startCalFirst5MinButton.Size = new System.Drawing.Size(130, 60);
            this.startCalFirst5MinButton.TabIndex = 28;
            this.startCalFirst5MinButton.Text = "START";
            this.startCalFirst5MinButton.UseVisualStyleBackColor = true;
            this.startCalFirst5MinButton.Click += new System.EventHandler(this.startCalFirst5MinButton_Click);
            // 
            // calculateGammaFactorButton
            // 
            this.calculateGammaFactorButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.calculateGammaFactorButton.Location = new System.Drawing.Point(454, 654);
            this.calculateGammaFactorButton.Name = "calculateGammaFactorButton";
            this.calculateGammaFactorButton.Size = new System.Drawing.Size(150, 60);
            this.calculateGammaFactorButton.TabIndex = 27;
            this.calculateGammaFactorButton.Text = "CALCULATE";
            this.calculateGammaFactorButton.UseVisualStyleBackColor = true;
            this.calculateGammaFactorButton.Click += new System.EventHandler(this.applyGammFactorButton_Click);
            // 
            // novDmCalTab
            // 
            this.novDmCalTab.Controls.Add(this.dmDataButton);
            this.novDmCalTab.Controls.Add(this.rollTestButton);
            this.novDmCalTab.Controls.Add(this.readKTableButton);
            this.novDmCalTab.Controls.Add(this.takeSurveyButton);
            this.novDmCalTab.Controls.Add(this.readEepromButton);
            this.novDmCalTab.Location = new System.Drawing.Point(4, 22);
            this.novDmCalTab.Name = "novDmCalTab";
            this.novDmCalTab.Padding = new System.Windows.Forms.Padding(3);
            this.novDmCalTab.Size = new System.Drawing.Size(607, 898);
            this.novDmCalTab.TabIndex = 1;
            this.novDmCalTab.Text = "NOV DIRECTIONAL MODULE";
            this.novDmCalTab.UseVisualStyleBackColor = true;
            // 
            // dmDataButton
            // 
            this.dmDataButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.dmDataButton.Location = new System.Drawing.Point(217, 126);
            this.dmDataButton.Name = "dmDataButton";
            this.dmDataButton.Size = new System.Drawing.Size(130, 60);
            this.dmDataButton.TabIndex = 33;
            this.dmDataButton.Text = "DM DATA";
            this.dmDataButton.UseVisualStyleBackColor = true;
            this.dmDataButton.Click += new System.EventHandler(this.dmDataButton_Click);
            // 
            // rollTestButton
            // 
            this.rollTestButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.rollTestButton.Location = new System.Drawing.Point(58, 126);
            this.rollTestButton.Name = "rollTestButton";
            this.rollTestButton.Size = new System.Drawing.Size(130, 60);
            this.rollTestButton.TabIndex = 32;
            this.rollTestButton.Text = "ROLL TEST";
            this.rollTestButton.UseVisualStyleBackColor = true;
            this.rollTestButton.Click += new System.EventHandler(this.rollTestButton_Click);
            // 
            // readKTableButton
            // 
            this.readKTableButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.readKTableButton.Location = new System.Drawing.Point(374, 41);
            this.readKTableButton.Name = "readKTableButton";
            this.readKTableButton.Size = new System.Drawing.Size(130, 60);
            this.readKTableButton.TabIndex = 31;
            this.readKTableButton.Text = "K TABLE";
            this.readKTableButton.UseVisualStyleBackColor = true;
            this.readKTableButton.Click += new System.EventHandler(this.readKTableButton_Click);
            // 
            // takeSurveyButton
            // 
            this.takeSurveyButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.takeSurveyButton.Location = new System.Drawing.Point(217, 41);
            this.takeSurveyButton.Name = "takeSurveyButton";
            this.takeSurveyButton.Size = new System.Drawing.Size(130, 60);
            this.takeSurveyButton.TabIndex = 30;
            this.takeSurveyButton.Text = "TAKE SURVEY";
            this.takeSurveyButton.UseVisualStyleBackColor = true;
            this.takeSurveyButton.Click += new System.EventHandler(this.takeSurveyButton_Click);
            // 
            // readEepromButton
            // 
            this.readEepromButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.readEepromButton.Location = new System.Drawing.Point(58, 41);
            this.readEepromButton.Name = "readEepromButton";
            this.readEepromButton.Size = new System.Drawing.Size(130, 60);
            this.readEepromButton.TabIndex = 29;
            this.readEepromButton.Text = "EEPROM RD";
            this.readEepromButton.UseVisualStyleBackColor = true;
            this.readEepromButton.Click += new System.EventHandler(this.readEepromButton_Click);
            // 
            // connectButton
            // 
            this.connectButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.connectButton.Location = new System.Drawing.Point(699, 145);
            this.connectButton.Name = "connectButton";
            this.connectButton.Size = new System.Drawing.Size(136, 86);
            this.connectButton.TabIndex = 18;
            this.connectButton.Text = "CONNECT";
            this.connectButton.UseVisualStyleBackColor = true;
            this.connectButton.Click += new System.EventHandler(this.connectButton_Click);
            // 
            // readDataButton
            // 
            this.readDataButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.readDataButton.Location = new System.Drawing.Point(699, 378);
            this.readDataButton.Name = "readDataButton";
            this.readDataButton.Size = new System.Drawing.Size(136, 86);
            this.readDataButton.TabIndex = 16;
            this.readDataButton.Text = "READ MWD";
            this.readDataButton.UseVisualStyleBackColor = true;
            this.readDataButton.Click += new System.EventHandler(this.readDataButton_Click);
            // 
            // readPowerButton
            // 
            this.readPowerButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.readPowerButton.Location = new System.Drawing.Point(699, 627);
            this.readPowerButton.Name = "readPowerButton";
            this.readPowerButton.Size = new System.Drawing.Size(136, 86);
            this.readPowerButton.TabIndex = 17;
            this.readPowerButton.Text = "READ POWER";
            this.readPowerButton.UseVisualStyleBackColor = true;
            this.readPowerButton.Click += new System.EventHandler(this.readPowerButton_Click);
            // 
            // runButton
            // 
            this.runButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.runButton.Location = new System.Drawing.Point(699, 261);
            this.runButton.Name = "runButton";
            this.runButton.Size = new System.Drawing.Size(136, 86);
            this.runButton.TabIndex = 19;
            this.runButton.Text = "RUN";
            this.runButton.UseVisualStyleBackColor = true;
            this.runButton.Click += new System.EventHandler(this.runButton_Click);
            // 
            // bhaLengthBuilderTab
            // 
            this.bhaLengthBuilderTab.Controls.Add(this.reloadTablesButton);
            this.bhaLengthBuilderTab.Controls.Add(this.toolSizeComboBox);
            this.bhaLengthBuilderTab.Controls.Add(this.label84);
            this.bhaLengthBuilderTab.Controls.Add(this.groupBox13);
            this.bhaLengthBuilderTab.Location = new System.Drawing.Point(4, 22);
            this.bhaLengthBuilderTab.Name = "bhaLengthBuilderTab";
            this.bhaLengthBuilderTab.Size = new System.Drawing.Size(1896, 1015);
            this.bhaLengthBuilderTab.TabIndex = 4;
            this.bhaLengthBuilderTab.Text = "BHA LENGTH BUILDER";
            this.bhaLengthBuilderTab.UseVisualStyleBackColor = true;
            // 
            // toolSizeComboBox
            // 
            this.toolSizeComboBox.FormattingEnabled = true;
            this.toolSizeComboBox.Items.AddRange(new object[] {
            "8\"",
            "6 3/4\"",
            "4 3/4\""});
            this.toolSizeComboBox.Location = new System.Drawing.Point(487, 14);
            this.toolSizeComboBox.Name = "toolSizeComboBox";
            this.toolSizeComboBox.Size = new System.Drawing.Size(99, 21);
            this.toolSizeComboBox.TabIndex = 10;
            this.toolSizeComboBox.Text = "8\"";
            this.toolSizeComboBox.SelectedIndexChanged += new System.EventHandler(this.toolSizeComboBox_SelectedIndexChanged);
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(418, 17);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(63, 13);
            this.label84.TabIndex = 9;
            this.label84.Text = "TOOL SIZE";
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.bhaLengthCalculateButton);
            this.groupBox13.Controls.Add(this.bhaLengthBox);
            this.groupBox13.Controls.Add(this.label108);
            this.groupBox13.Controls.Add(this.mwdNmdcCalculateButton);
            this.groupBox13.Controls.Add(this.calculateNmdcLengthBox);
            this.groupBox13.Controls.Add(this.label107);
            this.groupBox13.Controls.Add(this.probeNotes20);
            this.groupBox13.Controls.Add(this.probeNotes19);
            this.groupBox13.Controls.Add(this.probeNotes18);
            this.groupBox13.Controls.Add(this.probeNotes17);
            this.groupBox13.Controls.Add(this.probeNotes16);
            this.groupBox13.Controls.Add(this.probeNotes15);
            this.groupBox13.Controls.Add(this.probeNotes14);
            this.groupBox13.Controls.Add(this.probeNotes13);
            this.groupBox13.Controls.Add(this.probeNotes12);
            this.groupBox13.Controls.Add(this.probeNotes11);
            this.groupBox13.Controls.Add(this.probeNotes10);
            this.groupBox13.Controls.Add(this.probeNotes9);
            this.groupBox13.Controls.Add(this.probeNotes8);
            this.groupBox13.Controls.Add(this.probeNotes7);
            this.groupBox13.Controls.Add(this.probeNotes6);
            this.groupBox13.Controls.Add(this.probeNotes5);
            this.groupBox13.Controls.Add(this.probeNotes4);
            this.groupBox13.Controls.Add(this.probeNotes3);
            this.groupBox13.Controls.Add(this.probeNotes2);
            this.groupBox13.Controls.Add(this.probeNotes1);
            this.groupBox13.Controls.Add(this.probeUnits20);
            this.groupBox13.Controls.Add(this.probeUnits19);
            this.groupBox13.Controls.Add(this.probeUnits18);
            this.groupBox13.Controls.Add(this.probeUnits17);
            this.groupBox13.Controls.Add(this.probeUnits16);
            this.groupBox13.Controls.Add(this.probeUnits15);
            this.groupBox13.Controls.Add(this.probeUnits14);
            this.groupBox13.Controls.Add(this.probeUnits13);
            this.groupBox13.Controls.Add(this.probeUnits12);
            this.groupBox13.Controls.Add(this.probeUnits11);
            this.groupBox13.Controls.Add(this.probeUnits10);
            this.groupBox13.Controls.Add(this.probeUnits9);
            this.groupBox13.Controls.Add(this.probeUnits8);
            this.groupBox13.Controls.Add(this.probeUnits7);
            this.groupBox13.Controls.Add(this.probeUnits6);
            this.groupBox13.Controls.Add(this.probeUnits5);
            this.groupBox13.Controls.Add(this.probeUnits4);
            this.groupBox13.Controls.Add(this.probeUnits3);
            this.groupBox13.Controls.Add(this.probeUnits2);
            this.groupBox13.Controls.Add(this.probeUnits1);
            this.groupBox13.Controls.Add(this.probeLength20);
            this.groupBox13.Controls.Add(this.probeLength19);
            this.groupBox13.Controls.Add(this.probeLength18);
            this.groupBox13.Controls.Add(this.probeLength17);
            this.groupBox13.Controls.Add(this.probeLength16);
            this.groupBox13.Controls.Add(this.probeLength15);
            this.groupBox13.Controls.Add(this.probeLength14);
            this.groupBox13.Controls.Add(this.probeLength13);
            this.groupBox13.Controls.Add(this.probeLength12);
            this.groupBox13.Controls.Add(this.probeLength11);
            this.groupBox13.Controls.Add(this.probeLength10);
            this.groupBox13.Controls.Add(this.probeLength9);
            this.groupBox13.Controls.Add(this.probeLength8);
            this.groupBox13.Controls.Add(this.probeLength7);
            this.groupBox13.Controls.Add(this.probeLength6);
            this.groupBox13.Controls.Add(this.probeLength5);
            this.groupBox13.Controls.Add(this.probeLength4);
            this.groupBox13.Controls.Add(this.probeLength3);
            this.groupBox13.Controls.Add(this.probeLength2);
            this.groupBox13.Controls.Add(this.probeLength1);
            this.groupBox13.Controls.Add(this.probeCombo20);
            this.groupBox13.Controls.Add(this.probeCombo19);
            this.groupBox13.Controls.Add(this.probeCombo18);
            this.groupBox13.Controls.Add(this.probeCombo17);
            this.groupBox13.Controls.Add(this.probeCombo16);
            this.groupBox13.Controls.Add(this.probeCombo15);
            this.groupBox13.Controls.Add(this.probeCombo14);
            this.groupBox13.Controls.Add(this.probeCombo13);
            this.groupBox13.Controls.Add(this.probeCombo12);
            this.groupBox13.Controls.Add(this.probeCombo11);
            this.groupBox13.Controls.Add(this.probeCombo10);
            this.groupBox13.Controls.Add(this.probeCombo9);
            this.groupBox13.Controls.Add(this.probeCombo8);
            this.groupBox13.Controls.Add(this.probeCombo7);
            this.groupBox13.Controls.Add(this.probeCombo6);
            this.groupBox13.Controls.Add(this.probeCombo5);
            this.groupBox13.Controls.Add(this.probeCombo4);
            this.groupBox13.Controls.Add(this.probeCombo3);
            this.groupBox13.Controls.Add(this.probeCombo2);
            this.groupBox13.Controls.Add(this.probeCombo1);
            this.groupBox13.Controls.Add(this.collarNotes20);
            this.groupBox13.Controls.Add(this.collarNotes19);
            this.groupBox13.Controls.Add(this.collarNotes18);
            this.groupBox13.Controls.Add(this.collarNotes17);
            this.groupBox13.Controls.Add(this.collarNotes16);
            this.groupBox13.Controls.Add(this.collarNotes15);
            this.groupBox13.Controls.Add(this.collarNotes14);
            this.groupBox13.Controls.Add(this.collarNotes13);
            this.groupBox13.Controls.Add(this.collarNotes12);
            this.groupBox13.Controls.Add(this.collarNotes11);
            this.groupBox13.Controls.Add(this.collarNotes10);
            this.groupBox13.Controls.Add(this.collarNotes9);
            this.groupBox13.Controls.Add(this.collarNotes8);
            this.groupBox13.Controls.Add(this.collarNotes7);
            this.groupBox13.Controls.Add(this.collarNotes6);
            this.groupBox13.Controls.Add(this.collarNotes5);
            this.groupBox13.Controls.Add(this.collarNotes4);
            this.groupBox13.Controls.Add(this.collarNotes3);
            this.groupBox13.Controls.Add(this.collarNotes2);
            this.groupBox13.Controls.Add(this.collarNotes1);
            this.groupBox13.Controls.Add(this.collarUnits20);
            this.groupBox13.Controls.Add(this.collarUnits19);
            this.groupBox13.Controls.Add(this.collarUnits18);
            this.groupBox13.Controls.Add(this.collarUnits17);
            this.groupBox13.Controls.Add(this.collarUnits16);
            this.groupBox13.Controls.Add(this.collarUnits15);
            this.groupBox13.Controls.Add(this.collarUnits14);
            this.groupBox13.Controls.Add(this.collarUnits13);
            this.groupBox13.Controls.Add(this.collarUnits12);
            this.groupBox13.Controls.Add(this.collarUnits11);
            this.groupBox13.Controls.Add(this.collarUnits10);
            this.groupBox13.Controls.Add(this.collarUnits9);
            this.groupBox13.Controls.Add(this.collarUnits8);
            this.groupBox13.Controls.Add(this.collarUnits7);
            this.groupBox13.Controls.Add(this.collarUnits6);
            this.groupBox13.Controls.Add(this.collarUnits5);
            this.groupBox13.Controls.Add(this.collarUnits4);
            this.groupBox13.Controls.Add(this.collarUnits3);
            this.groupBox13.Controls.Add(this.collarUnits2);
            this.groupBox13.Controls.Add(this.collarUnits1);
            this.groupBox13.Controls.Add(this.collarLength20);
            this.groupBox13.Controls.Add(this.collarLength19);
            this.groupBox13.Controls.Add(this.collarLength18);
            this.groupBox13.Controls.Add(this.collarLength17);
            this.groupBox13.Controls.Add(this.collarLength16);
            this.groupBox13.Controls.Add(this.collarLength15);
            this.groupBox13.Controls.Add(this.collarLength14);
            this.groupBox13.Controls.Add(this.collarLength13);
            this.groupBox13.Controls.Add(this.collarLength12);
            this.groupBox13.Controls.Add(this.collarLength11);
            this.groupBox13.Controls.Add(this.collarLength10);
            this.groupBox13.Controls.Add(this.collarLength9);
            this.groupBox13.Controls.Add(this.collarLength8);
            this.groupBox13.Controls.Add(this.collarLength7);
            this.groupBox13.Controls.Add(this.collarLength6);
            this.groupBox13.Controls.Add(this.collarLength5);
            this.groupBox13.Controls.Add(this.collarLength4);
            this.groupBox13.Controls.Add(this.collarLength3);
            this.groupBox13.Controls.Add(this.collarLength2);
            this.groupBox13.Controls.Add(this.collarLength1);
            this.groupBox13.Controls.Add(this.label106);
            this.groupBox13.Controls.Add(this.label105);
            this.groupBox13.Controls.Add(this.bhaCombo20);
            this.groupBox13.Controls.Add(this.bhaCombo19);
            this.groupBox13.Controls.Add(this.bhaCombo18);
            this.groupBox13.Controls.Add(this.bhaCombo17);
            this.groupBox13.Controls.Add(this.bhaCombo16);
            this.groupBox13.Controls.Add(this.bhaCombo15);
            this.groupBox13.Controls.Add(this.bhaCombo14);
            this.groupBox13.Controls.Add(this.bhaCombo13);
            this.groupBox13.Controls.Add(this.bhaCombo12);
            this.groupBox13.Controls.Add(this.bhaCombo11);
            this.groupBox13.Controls.Add(this.bhaCombo10);
            this.groupBox13.Controls.Add(this.bhaCombo9);
            this.groupBox13.Controls.Add(this.bhaCombo8);
            this.groupBox13.Controls.Add(this.bhaCombo7);
            this.groupBox13.Controls.Add(this.bhaCombo6);
            this.groupBox13.Controls.Add(this.bhaCombo5);
            this.groupBox13.Controls.Add(this.bhaCombo4);
            this.groupBox13.Controls.Add(this.bhaCombo3);
            this.groupBox13.Controls.Add(this.bhaCombo2);
            this.groupBox13.Controls.Add(this.bhaCombo1);
            this.groupBox13.Controls.Add(this.label104);
            this.groupBox13.Controls.Add(this.label103);
            this.groupBox13.Controls.Add(this.label102);
            this.groupBox13.Controls.Add(this.label101);
            this.groupBox13.Controls.Add(this.label100);
            this.groupBox13.Controls.Add(this.label99);
            this.groupBox13.Controls.Add(this.label98);
            this.groupBox13.Controls.Add(this.label97);
            this.groupBox13.Controls.Add(this.label96);
            this.groupBox13.Controls.Add(this.label95);
            this.groupBox13.Controls.Add(this.label94);
            this.groupBox13.Controls.Add(this.label93);
            this.groupBox13.Controls.Add(this.label92);
            this.groupBox13.Controls.Add(this.label91);
            this.groupBox13.Controls.Add(this.label90);
            this.groupBox13.Controls.Add(this.label89);
            this.groupBox13.Controls.Add(this.label88);
            this.groupBox13.Controls.Add(this.label87);
            this.groupBox13.Controls.Add(this.label86);
            this.groupBox13.Controls.Add(this.label85);
            this.groupBox13.Controls.Add(this.label77);
            this.groupBox13.Controls.Add(this.label78);
            this.groupBox13.Controls.Add(this.label83);
            this.groupBox13.Controls.Add(this.label79);
            this.groupBox13.Controls.Add(this.label82);
            this.groupBox13.Controls.Add(this.label80);
            this.groupBox13.Controls.Add(this.label81);
            this.groupBox13.Location = new System.Drawing.Point(8, 36);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(1437, 656);
            this.groupBox13.TabIndex = 8;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "LENGTH BUILDER";
            // 
            // mwdNmdcCalculateButton
            // 
            this.mwdNmdcCalculateButton.Location = new System.Drawing.Point(1142, 607);
            this.mwdNmdcCalculateButton.Name = "mwdNmdcCalculateButton";
            this.mwdNmdcCalculateButton.Size = new System.Drawing.Size(81, 23);
            this.mwdNmdcCalculateButton.TabIndex = 191;
            this.mwdNmdcCalculateButton.Text = "CALCULATE";
            this.mwdNmdcCalculateButton.UseVisualStyleBackColor = true;
            this.mwdNmdcCalculateButton.Click += new System.EventHandler(this.mwdNmdcCalculateButton_Click);
            // 
            // calculateNmdcLengthBox
            // 
            this.calculateNmdcLengthBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.calculateNmdcLengthBox.Location = new System.Drawing.Point(1036, 601);
            this.calculateNmdcLengthBox.Name = "calculateNmdcLengthBox";
            this.calculateNmdcLengthBox.Size = new System.Drawing.Size(100, 32);
            this.calculateNmdcLengthBox.TabIndex = 190;
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label107.Location = new System.Drawing.Point(797, 607);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(233, 26);
            this.label107.TabIndex = 189;
            this.label107.Text = "MWD NMDC LENGTH";
            // 
            // probeNotes20
            // 
            this.probeNotes20.Location = new System.Drawing.Point(1218, 562);
            this.probeNotes20.Name = "probeNotes20";
            this.probeNotes20.Size = new System.Drawing.Size(166, 20);
            this.probeNotes20.TabIndex = 188;
            // 
            // probeNotes19
            // 
            this.probeNotes19.Location = new System.Drawing.Point(1218, 535);
            this.probeNotes19.Name = "probeNotes19";
            this.probeNotes19.Size = new System.Drawing.Size(166, 20);
            this.probeNotes19.TabIndex = 187;
            // 
            // probeNotes18
            // 
            this.probeNotes18.Location = new System.Drawing.Point(1218, 508);
            this.probeNotes18.Name = "probeNotes18";
            this.probeNotes18.Size = new System.Drawing.Size(166, 20);
            this.probeNotes18.TabIndex = 186;
            // 
            // probeNotes17
            // 
            this.probeNotes17.Location = new System.Drawing.Point(1218, 482);
            this.probeNotes17.Name = "probeNotes17";
            this.probeNotes17.Size = new System.Drawing.Size(166, 20);
            this.probeNotes17.TabIndex = 185;
            // 
            // probeNotes16
            // 
            this.probeNotes16.Location = new System.Drawing.Point(1218, 454);
            this.probeNotes16.Name = "probeNotes16";
            this.probeNotes16.Size = new System.Drawing.Size(166, 20);
            this.probeNotes16.TabIndex = 184;
            // 
            // probeNotes15
            // 
            this.probeNotes15.Location = new System.Drawing.Point(1218, 428);
            this.probeNotes15.Name = "probeNotes15";
            this.probeNotes15.Size = new System.Drawing.Size(166, 20);
            this.probeNotes15.TabIndex = 183;
            // 
            // probeNotes14
            // 
            this.probeNotes14.Location = new System.Drawing.Point(1218, 401);
            this.probeNotes14.Name = "probeNotes14";
            this.probeNotes14.Size = new System.Drawing.Size(166, 20);
            this.probeNotes14.TabIndex = 182;
            // 
            // probeNotes13
            // 
            this.probeNotes13.Location = new System.Drawing.Point(1218, 372);
            this.probeNotes13.Name = "probeNotes13";
            this.probeNotes13.Size = new System.Drawing.Size(166, 20);
            this.probeNotes13.TabIndex = 181;
            // 
            // probeNotes12
            // 
            this.probeNotes12.Location = new System.Drawing.Point(1218, 345);
            this.probeNotes12.Name = "probeNotes12";
            this.probeNotes12.Size = new System.Drawing.Size(166, 20);
            this.probeNotes12.TabIndex = 180;
            // 
            // probeNotes11
            // 
            this.probeNotes11.Location = new System.Drawing.Point(1218, 318);
            this.probeNotes11.Name = "probeNotes11";
            this.probeNotes11.Size = new System.Drawing.Size(166, 20);
            this.probeNotes11.TabIndex = 179;
            // 
            // probeNotes10
            // 
            this.probeNotes10.Location = new System.Drawing.Point(1218, 291);
            this.probeNotes10.Name = "probeNotes10";
            this.probeNotes10.Size = new System.Drawing.Size(166, 20);
            this.probeNotes10.TabIndex = 178;
            // 
            // probeNotes9
            // 
            this.probeNotes9.Location = new System.Drawing.Point(1218, 263);
            this.probeNotes9.Name = "probeNotes9";
            this.probeNotes9.Size = new System.Drawing.Size(166, 20);
            this.probeNotes9.TabIndex = 177;
            // 
            // probeNotes8
            // 
            this.probeNotes8.Location = new System.Drawing.Point(1218, 235);
            this.probeNotes8.Name = "probeNotes8";
            this.probeNotes8.Size = new System.Drawing.Size(166, 20);
            this.probeNotes8.TabIndex = 176;
            // 
            // probeNotes7
            // 
            this.probeNotes7.Location = new System.Drawing.Point(1218, 208);
            this.probeNotes7.Name = "probeNotes7";
            this.probeNotes7.Size = new System.Drawing.Size(166, 20);
            this.probeNotes7.TabIndex = 175;
            // 
            // probeNotes6
            // 
            this.probeNotes6.Location = new System.Drawing.Point(1218, 182);
            this.probeNotes6.Name = "probeNotes6";
            this.probeNotes6.Size = new System.Drawing.Size(166, 20);
            this.probeNotes6.TabIndex = 174;
            // 
            // probeNotes5
            // 
            this.probeNotes5.Location = new System.Drawing.Point(1218, 154);
            this.probeNotes5.Name = "probeNotes5";
            this.probeNotes5.Size = new System.Drawing.Size(166, 20);
            this.probeNotes5.TabIndex = 173;
            // 
            // probeNotes4
            // 
            this.probeNotes4.Location = new System.Drawing.Point(1218, 128);
            this.probeNotes4.Name = "probeNotes4";
            this.probeNotes4.Size = new System.Drawing.Size(166, 20);
            this.probeNotes4.TabIndex = 172;
            // 
            // probeNotes3
            // 
            this.probeNotes3.Location = new System.Drawing.Point(1218, 101);
            this.probeNotes3.Name = "probeNotes3";
            this.probeNotes3.Size = new System.Drawing.Size(166, 20);
            this.probeNotes3.TabIndex = 171;
            // 
            // probeNotes2
            // 
            this.probeNotes2.Location = new System.Drawing.Point(1218, 74);
            this.probeNotes2.Name = "probeNotes2";
            this.probeNotes2.Size = new System.Drawing.Size(166, 20);
            this.probeNotes2.TabIndex = 170;
            // 
            // probeNotes1
            // 
            this.probeNotes1.Location = new System.Drawing.Point(1218, 47);
            this.probeNotes1.Name = "probeNotes1";
            this.probeNotes1.Size = new System.Drawing.Size(166, 20);
            this.probeNotes1.TabIndex = 169;
            // 
            // probeUnits20
            // 
            this.probeUnits20.FormattingEnabled = true;
            this.probeUnits20.Location = new System.Drawing.Point(1069, 562);
            this.probeUnits20.Name = "probeUnits20";
            this.probeUnits20.Size = new System.Drawing.Size(110, 21);
            this.probeUnits20.TabIndex = 168;
            // 
            // probeUnits19
            // 
            this.probeUnits19.FormattingEnabled = true;
            this.probeUnits19.Location = new System.Drawing.Point(1069, 535);
            this.probeUnits19.Name = "probeUnits19";
            this.probeUnits19.Size = new System.Drawing.Size(110, 21);
            this.probeUnits19.TabIndex = 167;
            // 
            // probeUnits18
            // 
            this.probeUnits18.FormattingEnabled = true;
            this.probeUnits18.Location = new System.Drawing.Point(1069, 508);
            this.probeUnits18.Name = "probeUnits18";
            this.probeUnits18.Size = new System.Drawing.Size(110, 21);
            this.probeUnits18.TabIndex = 166;
            // 
            // probeUnits17
            // 
            this.probeUnits17.FormattingEnabled = true;
            this.probeUnits17.Location = new System.Drawing.Point(1069, 481);
            this.probeUnits17.Name = "probeUnits17";
            this.probeUnits17.Size = new System.Drawing.Size(110, 21);
            this.probeUnits17.TabIndex = 165;
            // 
            // probeUnits16
            // 
            this.probeUnits16.FormattingEnabled = true;
            this.probeUnits16.Location = new System.Drawing.Point(1069, 454);
            this.probeUnits16.Name = "probeUnits16";
            this.probeUnits16.Size = new System.Drawing.Size(110, 21);
            this.probeUnits16.TabIndex = 164;
            // 
            // probeUnits15
            // 
            this.probeUnits15.FormattingEnabled = true;
            this.probeUnits15.Location = new System.Drawing.Point(1069, 427);
            this.probeUnits15.Name = "probeUnits15";
            this.probeUnits15.Size = new System.Drawing.Size(110, 21);
            this.probeUnits15.TabIndex = 163;
            // 
            // probeUnits14
            // 
            this.probeUnits14.FormattingEnabled = true;
            this.probeUnits14.Location = new System.Drawing.Point(1069, 400);
            this.probeUnits14.Name = "probeUnits14";
            this.probeUnits14.Size = new System.Drawing.Size(110, 21);
            this.probeUnits14.TabIndex = 162;
            // 
            // probeUnits13
            // 
            this.probeUnits13.FormattingEnabled = true;
            this.probeUnits13.Location = new System.Drawing.Point(1069, 371);
            this.probeUnits13.Name = "probeUnits13";
            this.probeUnits13.Size = new System.Drawing.Size(110, 21);
            this.probeUnits13.TabIndex = 161;
            // 
            // probeUnits12
            // 
            this.probeUnits12.FormattingEnabled = true;
            this.probeUnits12.Location = new System.Drawing.Point(1069, 344);
            this.probeUnits12.Name = "probeUnits12";
            this.probeUnits12.Size = new System.Drawing.Size(110, 21);
            this.probeUnits12.TabIndex = 160;
            // 
            // probeUnits11
            // 
            this.probeUnits11.FormattingEnabled = true;
            this.probeUnits11.Location = new System.Drawing.Point(1069, 317);
            this.probeUnits11.Name = "probeUnits11";
            this.probeUnits11.Size = new System.Drawing.Size(110, 21);
            this.probeUnits11.TabIndex = 159;
            // 
            // probeUnits10
            // 
            this.probeUnits10.FormattingEnabled = true;
            this.probeUnits10.Location = new System.Drawing.Point(1069, 290);
            this.probeUnits10.Name = "probeUnits10";
            this.probeUnits10.Size = new System.Drawing.Size(110, 21);
            this.probeUnits10.TabIndex = 158;
            // 
            // probeUnits9
            // 
            this.probeUnits9.FormattingEnabled = true;
            this.probeUnits9.Location = new System.Drawing.Point(1069, 262);
            this.probeUnits9.Name = "probeUnits9";
            this.probeUnits9.Size = new System.Drawing.Size(110, 21);
            this.probeUnits9.TabIndex = 157;
            // 
            // probeUnits8
            // 
            this.probeUnits8.FormattingEnabled = true;
            this.probeUnits8.Location = new System.Drawing.Point(1069, 235);
            this.probeUnits8.Name = "probeUnits8";
            this.probeUnits8.Size = new System.Drawing.Size(110, 21);
            this.probeUnits8.TabIndex = 156;
            // 
            // probeUnits7
            // 
            this.probeUnits7.FormattingEnabled = true;
            this.probeUnits7.Location = new System.Drawing.Point(1069, 208);
            this.probeUnits7.Name = "probeUnits7";
            this.probeUnits7.Size = new System.Drawing.Size(110, 21);
            this.probeUnits7.TabIndex = 155;
            // 
            // probeUnits6
            // 
            this.probeUnits6.FormattingEnabled = true;
            this.probeUnits6.Location = new System.Drawing.Point(1069, 181);
            this.probeUnits6.Name = "probeUnits6";
            this.probeUnits6.Size = new System.Drawing.Size(110, 21);
            this.probeUnits6.TabIndex = 154;
            // 
            // probeUnits5
            // 
            this.probeUnits5.FormattingEnabled = true;
            this.probeUnits5.Location = new System.Drawing.Point(1069, 154);
            this.probeUnits5.Name = "probeUnits5";
            this.probeUnits5.Size = new System.Drawing.Size(110, 21);
            this.probeUnits5.TabIndex = 153;
            // 
            // probeUnits4
            // 
            this.probeUnits4.FormattingEnabled = true;
            this.probeUnits4.Location = new System.Drawing.Point(1069, 127);
            this.probeUnits4.Name = "probeUnits4";
            this.probeUnits4.Size = new System.Drawing.Size(110, 21);
            this.probeUnits4.TabIndex = 152;
            // 
            // probeUnits3
            // 
            this.probeUnits3.FormattingEnabled = true;
            this.probeUnits3.Location = new System.Drawing.Point(1069, 100);
            this.probeUnits3.Name = "probeUnits3";
            this.probeUnits3.Size = new System.Drawing.Size(110, 21);
            this.probeUnits3.TabIndex = 151;
            // 
            // probeUnits2
            // 
            this.probeUnits2.FormattingEnabled = true;
            this.probeUnits2.Location = new System.Drawing.Point(1069, 73);
            this.probeUnits2.Name = "probeUnits2";
            this.probeUnits2.Size = new System.Drawing.Size(110, 21);
            this.probeUnits2.TabIndex = 150;
            // 
            // probeUnits1
            // 
            this.probeUnits1.FormattingEnabled = true;
            this.probeUnits1.Location = new System.Drawing.Point(1069, 46);
            this.probeUnits1.Name = "probeUnits1";
            this.probeUnits1.Size = new System.Drawing.Size(110, 21);
            this.probeUnits1.TabIndex = 149;
            // 
            // probeLength20
            // 
            this.probeLength20.Location = new System.Drawing.Point(930, 561);
            this.probeLength20.Name = "probeLength20";
            this.probeLength20.Size = new System.Drawing.Size(100, 20);
            this.probeLength20.TabIndex = 148;
            // 
            // probeLength19
            // 
            this.probeLength19.Location = new System.Drawing.Point(930, 534);
            this.probeLength19.Name = "probeLength19";
            this.probeLength19.Size = new System.Drawing.Size(100, 20);
            this.probeLength19.TabIndex = 147;
            // 
            // probeLength18
            // 
            this.probeLength18.Location = new System.Drawing.Point(930, 507);
            this.probeLength18.Name = "probeLength18";
            this.probeLength18.Size = new System.Drawing.Size(100, 20);
            this.probeLength18.TabIndex = 146;
            // 
            // probeLength17
            // 
            this.probeLength17.Location = new System.Drawing.Point(930, 481);
            this.probeLength17.Name = "probeLength17";
            this.probeLength17.Size = new System.Drawing.Size(100, 20);
            this.probeLength17.TabIndex = 145;
            // 
            // probeLength16
            // 
            this.probeLength16.Location = new System.Drawing.Point(930, 453);
            this.probeLength16.Name = "probeLength16";
            this.probeLength16.Size = new System.Drawing.Size(100, 20);
            this.probeLength16.TabIndex = 144;
            // 
            // probeLength15
            // 
            this.probeLength15.Location = new System.Drawing.Point(930, 427);
            this.probeLength15.Name = "probeLength15";
            this.probeLength15.Size = new System.Drawing.Size(100, 20);
            this.probeLength15.TabIndex = 143;
            // 
            // probeLength14
            // 
            this.probeLength14.Location = new System.Drawing.Point(930, 400);
            this.probeLength14.Name = "probeLength14";
            this.probeLength14.Size = new System.Drawing.Size(100, 20);
            this.probeLength14.TabIndex = 142;
            // 
            // probeLength13
            // 
            this.probeLength13.Location = new System.Drawing.Point(930, 371);
            this.probeLength13.Name = "probeLength13";
            this.probeLength13.Size = new System.Drawing.Size(100, 20);
            this.probeLength13.TabIndex = 141;
            // 
            // probeLength12
            // 
            this.probeLength12.Location = new System.Drawing.Point(930, 344);
            this.probeLength12.Name = "probeLength12";
            this.probeLength12.Size = new System.Drawing.Size(100, 20);
            this.probeLength12.TabIndex = 140;
            // 
            // probeLength11
            // 
            this.probeLength11.Location = new System.Drawing.Point(930, 317);
            this.probeLength11.Name = "probeLength11";
            this.probeLength11.Size = new System.Drawing.Size(100, 20);
            this.probeLength11.TabIndex = 139;
            // 
            // probeLength10
            // 
            this.probeLength10.Location = new System.Drawing.Point(930, 290);
            this.probeLength10.Name = "probeLength10";
            this.probeLength10.Size = new System.Drawing.Size(100, 20);
            this.probeLength10.TabIndex = 138;
            // 
            // probeLength9
            // 
            this.probeLength9.Location = new System.Drawing.Point(930, 262);
            this.probeLength9.Name = "probeLength9";
            this.probeLength9.Size = new System.Drawing.Size(100, 20);
            this.probeLength9.TabIndex = 137;
            // 
            // probeLength8
            // 
            this.probeLength8.Location = new System.Drawing.Point(930, 234);
            this.probeLength8.Name = "probeLength8";
            this.probeLength8.Size = new System.Drawing.Size(100, 20);
            this.probeLength8.TabIndex = 136;
            // 
            // probeLength7
            // 
            this.probeLength7.Location = new System.Drawing.Point(930, 207);
            this.probeLength7.Name = "probeLength7";
            this.probeLength7.Size = new System.Drawing.Size(100, 20);
            this.probeLength7.TabIndex = 135;
            // 
            // probeLength6
            // 
            this.probeLength6.Location = new System.Drawing.Point(930, 181);
            this.probeLength6.Name = "probeLength6";
            this.probeLength6.Size = new System.Drawing.Size(100, 20);
            this.probeLength6.TabIndex = 134;
            // 
            // probeLength5
            // 
            this.probeLength5.Location = new System.Drawing.Point(930, 153);
            this.probeLength5.Name = "probeLength5";
            this.probeLength5.Size = new System.Drawing.Size(100, 20);
            this.probeLength5.TabIndex = 133;
            // 
            // probeLength4
            // 
            this.probeLength4.Location = new System.Drawing.Point(930, 127);
            this.probeLength4.Name = "probeLength4";
            this.probeLength4.Size = new System.Drawing.Size(100, 20);
            this.probeLength4.TabIndex = 132;
            // 
            // probeLength3
            // 
            this.probeLength3.Location = new System.Drawing.Point(930, 100);
            this.probeLength3.Name = "probeLength3";
            this.probeLength3.Size = new System.Drawing.Size(100, 20);
            this.probeLength3.TabIndex = 131;
            // 
            // probeLength2
            // 
            this.probeLength2.Location = new System.Drawing.Point(930, 73);
            this.probeLength2.Name = "probeLength2";
            this.probeLength2.Size = new System.Drawing.Size(100, 20);
            this.probeLength2.TabIndex = 130;
            // 
            // probeLength1
            // 
            this.probeLength1.Location = new System.Drawing.Point(930, 46);
            this.probeLength1.Name = "probeLength1";
            this.probeLength1.Size = new System.Drawing.Size(100, 20);
            this.probeLength1.TabIndex = 129;
            // 
            // probeCombo20
            // 
            this.probeCombo20.FormattingEnabled = true;
            this.probeCombo20.Location = new System.Drawing.Point(723, 561);
            this.probeCombo20.Name = "probeCombo20";
            this.probeCombo20.Size = new System.Drawing.Size(187, 21);
            this.probeCombo20.TabIndex = 128;
            this.probeCombo20.SelectedIndexChanged += new System.EventHandler(this.probeCombo20_SelectedIndexChanged);
            // 
            // probeCombo19
            // 
            this.probeCombo19.FormattingEnabled = true;
            this.probeCombo19.Location = new System.Drawing.Point(723, 534);
            this.probeCombo19.Name = "probeCombo19";
            this.probeCombo19.Size = new System.Drawing.Size(187, 21);
            this.probeCombo19.TabIndex = 127;
            this.probeCombo19.SelectedIndexChanged += new System.EventHandler(this.probeCombo19_SelectedIndexChanged);
            // 
            // probeCombo18
            // 
            this.probeCombo18.FormattingEnabled = true;
            this.probeCombo18.Location = new System.Drawing.Point(723, 507);
            this.probeCombo18.Name = "probeCombo18";
            this.probeCombo18.Size = new System.Drawing.Size(187, 21);
            this.probeCombo18.TabIndex = 126;
            this.probeCombo18.SelectedIndexChanged += new System.EventHandler(this.probeCombo18_SelectedIndexChanged);
            // 
            // probeCombo17
            // 
            this.probeCombo17.FormattingEnabled = true;
            this.probeCombo17.Location = new System.Drawing.Point(723, 481);
            this.probeCombo17.Name = "probeCombo17";
            this.probeCombo17.Size = new System.Drawing.Size(187, 21);
            this.probeCombo17.TabIndex = 125;
            this.probeCombo17.SelectedIndexChanged += new System.EventHandler(this.probeCombo17_SelectedIndexChanged);
            // 
            // probeCombo16
            // 
            this.probeCombo16.FormattingEnabled = true;
            this.probeCombo16.Location = new System.Drawing.Point(723, 453);
            this.probeCombo16.Name = "probeCombo16";
            this.probeCombo16.Size = new System.Drawing.Size(187, 21);
            this.probeCombo16.TabIndex = 124;
            this.probeCombo16.SelectedIndexChanged += new System.EventHandler(this.probeCombo16_SelectedIndexChanged);
            // 
            // probeCombo15
            // 
            this.probeCombo15.FormattingEnabled = true;
            this.probeCombo15.Location = new System.Drawing.Point(723, 426);
            this.probeCombo15.Name = "probeCombo15";
            this.probeCombo15.Size = new System.Drawing.Size(187, 21);
            this.probeCombo15.TabIndex = 123;
            this.probeCombo15.SelectedIndexChanged += new System.EventHandler(this.probeCombo15_SelectedIndexChanged);
            // 
            // probeCombo14
            // 
            this.probeCombo14.FormattingEnabled = true;
            this.probeCombo14.Location = new System.Drawing.Point(723, 399);
            this.probeCombo14.Name = "probeCombo14";
            this.probeCombo14.Size = new System.Drawing.Size(187, 21);
            this.probeCombo14.TabIndex = 122;
            this.probeCombo14.SelectedIndexChanged += new System.EventHandler(this.probeCombo14_SelectedIndexChanged);
            // 
            // probeCombo13
            // 
            this.probeCombo13.FormattingEnabled = true;
            this.probeCombo13.Location = new System.Drawing.Point(723, 370);
            this.probeCombo13.Name = "probeCombo13";
            this.probeCombo13.Size = new System.Drawing.Size(187, 21);
            this.probeCombo13.TabIndex = 121;
            this.probeCombo13.SelectedIndexChanged += new System.EventHandler(this.probeCombo13_SelectedIndexChanged);
            // 
            // probeCombo12
            // 
            this.probeCombo12.FormattingEnabled = true;
            this.probeCombo12.Location = new System.Drawing.Point(723, 343);
            this.probeCombo12.Name = "probeCombo12";
            this.probeCombo12.Size = new System.Drawing.Size(187, 21);
            this.probeCombo12.TabIndex = 120;
            this.probeCombo12.SelectedIndexChanged += new System.EventHandler(this.probeCombo12_SelectedIndexChanged);
            // 
            // probeCombo11
            // 
            this.probeCombo11.FormattingEnabled = true;
            this.probeCombo11.Location = new System.Drawing.Point(723, 316);
            this.probeCombo11.Name = "probeCombo11";
            this.probeCombo11.Size = new System.Drawing.Size(187, 21);
            this.probeCombo11.TabIndex = 119;
            this.probeCombo11.SelectedIndexChanged += new System.EventHandler(this.probeCombo11_SelectedIndexChanged);
            // 
            // probeCombo10
            // 
            this.probeCombo10.FormattingEnabled = true;
            this.probeCombo10.Location = new System.Drawing.Point(723, 289);
            this.probeCombo10.Name = "probeCombo10";
            this.probeCombo10.Size = new System.Drawing.Size(187, 21);
            this.probeCombo10.TabIndex = 118;
            this.probeCombo10.SelectedIndexChanged += new System.EventHandler(this.probeCombo10_SelectedIndexChanged);
            // 
            // probeCombo9
            // 
            this.probeCombo9.FormattingEnabled = true;
            this.probeCombo9.Location = new System.Drawing.Point(723, 261);
            this.probeCombo9.Name = "probeCombo9";
            this.probeCombo9.Size = new System.Drawing.Size(187, 21);
            this.probeCombo9.TabIndex = 117;
            this.probeCombo9.SelectedIndexChanged += new System.EventHandler(this.probeCombo9_SelectedIndexChanged);
            // 
            // probeCombo8
            // 
            this.probeCombo8.FormattingEnabled = true;
            this.probeCombo8.Location = new System.Drawing.Point(723, 234);
            this.probeCombo8.Name = "probeCombo8";
            this.probeCombo8.Size = new System.Drawing.Size(187, 21);
            this.probeCombo8.TabIndex = 116;
            this.probeCombo8.SelectedIndexChanged += new System.EventHandler(this.probeCombo8_SelectedIndexChanged);
            // 
            // probeCombo7
            // 
            this.probeCombo7.FormattingEnabled = true;
            this.probeCombo7.Location = new System.Drawing.Point(723, 207);
            this.probeCombo7.Name = "probeCombo7";
            this.probeCombo7.Size = new System.Drawing.Size(187, 21);
            this.probeCombo7.TabIndex = 115;
            this.probeCombo7.SelectedIndexChanged += new System.EventHandler(this.probeCombo7_SelectedIndexChanged);
            // 
            // probeCombo6
            // 
            this.probeCombo6.FormattingEnabled = true;
            this.probeCombo6.Location = new System.Drawing.Point(723, 180);
            this.probeCombo6.Name = "probeCombo6";
            this.probeCombo6.Size = new System.Drawing.Size(187, 21);
            this.probeCombo6.TabIndex = 114;
            this.probeCombo6.SelectedIndexChanged += new System.EventHandler(this.probeCombo6_SelectedIndexChanged);
            // 
            // probeCombo5
            // 
            this.probeCombo5.FormattingEnabled = true;
            this.probeCombo5.Location = new System.Drawing.Point(723, 153);
            this.probeCombo5.Name = "probeCombo5";
            this.probeCombo5.Size = new System.Drawing.Size(187, 21);
            this.probeCombo5.TabIndex = 113;
            this.probeCombo5.SelectedIndexChanged += new System.EventHandler(this.probeCombo5_SelectedIndexChanged);
            // 
            // probeCombo4
            // 
            this.probeCombo4.FormattingEnabled = true;
            this.probeCombo4.Location = new System.Drawing.Point(723, 126);
            this.probeCombo4.Name = "probeCombo4";
            this.probeCombo4.Size = new System.Drawing.Size(187, 21);
            this.probeCombo4.TabIndex = 112;
            this.probeCombo4.SelectedIndexChanged += new System.EventHandler(this.probeCombo4_SelectedIndexChanged);
            // 
            // probeCombo3
            // 
            this.probeCombo3.FormattingEnabled = true;
            this.probeCombo3.Location = new System.Drawing.Point(723, 99);
            this.probeCombo3.Name = "probeCombo3";
            this.probeCombo3.Size = new System.Drawing.Size(187, 21);
            this.probeCombo3.TabIndex = 111;
            this.probeCombo3.SelectedIndexChanged += new System.EventHandler(this.probeCombo3_SelectedIndexChanged);
            // 
            // probeCombo2
            // 
            this.probeCombo2.FormattingEnabled = true;
            this.probeCombo2.Location = new System.Drawing.Point(723, 72);
            this.probeCombo2.Name = "probeCombo2";
            this.probeCombo2.Size = new System.Drawing.Size(187, 21);
            this.probeCombo2.TabIndex = 110;
            this.probeCombo2.SelectedIndexChanged += new System.EventHandler(this.probeCombo2_SelectedIndexChanged);
            // 
            // probeCombo1
            // 
            this.probeCombo1.FormattingEnabled = true;
            this.probeCombo1.Location = new System.Drawing.Point(723, 45);
            this.probeCombo1.Name = "probeCombo1";
            this.probeCombo1.Size = new System.Drawing.Size(187, 21);
            this.probeCombo1.TabIndex = 109;
            this.probeCombo1.SelectedIndexChanged += new System.EventHandler(this.probeCombo1_SelectedIndexChanged);
            // 
            // collarNotes20
            // 
            this.collarNotes20.Location = new System.Drawing.Point(538, 560);
            this.collarNotes20.Name = "collarNotes20";
            this.collarNotes20.Size = new System.Drawing.Size(154, 20);
            this.collarNotes20.TabIndex = 108;
            // 
            // collarNotes19
            // 
            this.collarNotes19.Location = new System.Drawing.Point(538, 533);
            this.collarNotes19.Name = "collarNotes19";
            this.collarNotes19.Size = new System.Drawing.Size(154, 20);
            this.collarNotes19.TabIndex = 107;
            // 
            // collarNotes18
            // 
            this.collarNotes18.Location = new System.Drawing.Point(538, 506);
            this.collarNotes18.Name = "collarNotes18";
            this.collarNotes18.Size = new System.Drawing.Size(154, 20);
            this.collarNotes18.TabIndex = 106;
            // 
            // collarNotes17
            // 
            this.collarNotes17.Location = new System.Drawing.Point(538, 480);
            this.collarNotes17.Name = "collarNotes17";
            this.collarNotes17.Size = new System.Drawing.Size(154, 20);
            this.collarNotes17.TabIndex = 105;
            // 
            // collarNotes16
            // 
            this.collarNotes16.Location = new System.Drawing.Point(538, 452);
            this.collarNotes16.Name = "collarNotes16";
            this.collarNotes16.Size = new System.Drawing.Size(154, 20);
            this.collarNotes16.TabIndex = 104;
            // 
            // collarNotes15
            // 
            this.collarNotes15.Location = new System.Drawing.Point(538, 426);
            this.collarNotes15.Name = "collarNotes15";
            this.collarNotes15.Size = new System.Drawing.Size(154, 20);
            this.collarNotes15.TabIndex = 103;
            // 
            // collarNotes14
            // 
            this.collarNotes14.Location = new System.Drawing.Point(538, 399);
            this.collarNotes14.Name = "collarNotes14";
            this.collarNotes14.Size = new System.Drawing.Size(154, 20);
            this.collarNotes14.TabIndex = 102;
            // 
            // collarNotes13
            // 
            this.collarNotes13.Location = new System.Drawing.Point(538, 370);
            this.collarNotes13.Name = "collarNotes13";
            this.collarNotes13.Size = new System.Drawing.Size(154, 20);
            this.collarNotes13.TabIndex = 101;
            // 
            // collarNotes12
            // 
            this.collarNotes12.Location = new System.Drawing.Point(538, 343);
            this.collarNotes12.Name = "collarNotes12";
            this.collarNotes12.Size = new System.Drawing.Size(154, 20);
            this.collarNotes12.TabIndex = 100;
            // 
            // collarNotes11
            // 
            this.collarNotes11.Location = new System.Drawing.Point(538, 316);
            this.collarNotes11.Name = "collarNotes11";
            this.collarNotes11.Size = new System.Drawing.Size(154, 20);
            this.collarNotes11.TabIndex = 99;
            // 
            // collarNotes10
            // 
            this.collarNotes10.Location = new System.Drawing.Point(538, 289);
            this.collarNotes10.Name = "collarNotes10";
            this.collarNotes10.Size = new System.Drawing.Size(154, 20);
            this.collarNotes10.TabIndex = 98;
            // 
            // collarNotes9
            // 
            this.collarNotes9.Location = new System.Drawing.Point(538, 261);
            this.collarNotes9.Name = "collarNotes9";
            this.collarNotes9.Size = new System.Drawing.Size(154, 20);
            this.collarNotes9.TabIndex = 97;
            // 
            // collarNotes8
            // 
            this.collarNotes8.Location = new System.Drawing.Point(538, 233);
            this.collarNotes8.Name = "collarNotes8";
            this.collarNotes8.Size = new System.Drawing.Size(154, 20);
            this.collarNotes8.TabIndex = 96;
            // 
            // collarNotes7
            // 
            this.collarNotes7.Location = new System.Drawing.Point(538, 206);
            this.collarNotes7.Name = "collarNotes7";
            this.collarNotes7.Size = new System.Drawing.Size(154, 20);
            this.collarNotes7.TabIndex = 95;
            // 
            // collarNotes6
            // 
            this.collarNotes6.Location = new System.Drawing.Point(538, 180);
            this.collarNotes6.Name = "collarNotes6";
            this.collarNotes6.Size = new System.Drawing.Size(154, 20);
            this.collarNotes6.TabIndex = 94;
            // 
            // collarNotes5
            // 
            this.collarNotes5.Location = new System.Drawing.Point(538, 152);
            this.collarNotes5.Name = "collarNotes5";
            this.collarNotes5.Size = new System.Drawing.Size(154, 20);
            this.collarNotes5.TabIndex = 93;
            // 
            // collarNotes4
            // 
            this.collarNotes4.Location = new System.Drawing.Point(538, 126);
            this.collarNotes4.Name = "collarNotes4";
            this.collarNotes4.Size = new System.Drawing.Size(154, 20);
            this.collarNotes4.TabIndex = 92;
            // 
            // collarNotes3
            // 
            this.collarNotes3.Location = new System.Drawing.Point(538, 99);
            this.collarNotes3.Name = "collarNotes3";
            this.collarNotes3.Size = new System.Drawing.Size(154, 20);
            this.collarNotes3.TabIndex = 91;
            // 
            // collarNotes2
            // 
            this.collarNotes2.Location = new System.Drawing.Point(538, 72);
            this.collarNotes2.Name = "collarNotes2";
            this.collarNotes2.Size = new System.Drawing.Size(154, 20);
            this.collarNotes2.TabIndex = 90;
            // 
            // collarNotes1
            // 
            this.collarNotes1.Location = new System.Drawing.Point(538, 45);
            this.collarNotes1.Name = "collarNotes1";
            this.collarNotes1.Size = new System.Drawing.Size(154, 20);
            this.collarNotes1.TabIndex = 89;
            // 
            // collarUnits20
            // 
            this.collarUnits20.FormattingEnabled = true;
            this.collarUnits20.Location = new System.Drawing.Point(399, 558);
            this.collarUnits20.Name = "collarUnits20";
            this.collarUnits20.Size = new System.Drawing.Size(110, 21);
            this.collarUnits20.TabIndex = 88;
            // 
            // collarUnits19
            // 
            this.collarUnits19.FormattingEnabled = true;
            this.collarUnits19.Location = new System.Drawing.Point(399, 531);
            this.collarUnits19.Name = "collarUnits19";
            this.collarUnits19.Size = new System.Drawing.Size(110, 21);
            this.collarUnits19.TabIndex = 87;
            // 
            // collarUnits18
            // 
            this.collarUnits18.FormattingEnabled = true;
            this.collarUnits18.Location = new System.Drawing.Point(399, 504);
            this.collarUnits18.Name = "collarUnits18";
            this.collarUnits18.Size = new System.Drawing.Size(110, 21);
            this.collarUnits18.TabIndex = 86;
            // 
            // collarUnits17
            // 
            this.collarUnits17.FormattingEnabled = true;
            this.collarUnits17.Location = new System.Drawing.Point(399, 477);
            this.collarUnits17.Name = "collarUnits17";
            this.collarUnits17.Size = new System.Drawing.Size(110, 21);
            this.collarUnits17.TabIndex = 85;
            // 
            // collarUnits16
            // 
            this.collarUnits16.FormattingEnabled = true;
            this.collarUnits16.Location = new System.Drawing.Point(399, 450);
            this.collarUnits16.Name = "collarUnits16";
            this.collarUnits16.Size = new System.Drawing.Size(110, 21);
            this.collarUnits16.TabIndex = 84;
            // 
            // collarUnits15
            // 
            this.collarUnits15.FormattingEnabled = true;
            this.collarUnits15.Location = new System.Drawing.Point(399, 423);
            this.collarUnits15.Name = "collarUnits15";
            this.collarUnits15.Size = new System.Drawing.Size(110, 21);
            this.collarUnits15.TabIndex = 83;
            // 
            // collarUnits14
            // 
            this.collarUnits14.FormattingEnabled = true;
            this.collarUnits14.Location = new System.Drawing.Point(399, 396);
            this.collarUnits14.Name = "collarUnits14";
            this.collarUnits14.Size = new System.Drawing.Size(110, 21);
            this.collarUnits14.TabIndex = 82;
            // 
            // collarUnits13
            // 
            this.collarUnits13.FormattingEnabled = true;
            this.collarUnits13.Location = new System.Drawing.Point(399, 367);
            this.collarUnits13.Name = "collarUnits13";
            this.collarUnits13.Size = new System.Drawing.Size(110, 21);
            this.collarUnits13.TabIndex = 81;
            // 
            // collarUnits12
            // 
            this.collarUnits12.FormattingEnabled = true;
            this.collarUnits12.Location = new System.Drawing.Point(399, 340);
            this.collarUnits12.Name = "collarUnits12";
            this.collarUnits12.Size = new System.Drawing.Size(110, 21);
            this.collarUnits12.TabIndex = 80;
            // 
            // collarUnits11
            // 
            this.collarUnits11.FormattingEnabled = true;
            this.collarUnits11.Location = new System.Drawing.Point(399, 313);
            this.collarUnits11.Name = "collarUnits11";
            this.collarUnits11.Size = new System.Drawing.Size(110, 21);
            this.collarUnits11.TabIndex = 79;
            // 
            // collarUnits10
            // 
            this.collarUnits10.FormattingEnabled = true;
            this.collarUnits10.Location = new System.Drawing.Point(399, 286);
            this.collarUnits10.Name = "collarUnits10";
            this.collarUnits10.Size = new System.Drawing.Size(110, 21);
            this.collarUnits10.TabIndex = 78;
            // 
            // collarUnits9
            // 
            this.collarUnits9.FormattingEnabled = true;
            this.collarUnits9.Location = new System.Drawing.Point(399, 258);
            this.collarUnits9.Name = "collarUnits9";
            this.collarUnits9.Size = new System.Drawing.Size(110, 21);
            this.collarUnits9.TabIndex = 77;
            // 
            // collarUnits8
            // 
            this.collarUnits8.FormattingEnabled = true;
            this.collarUnits8.Location = new System.Drawing.Point(399, 231);
            this.collarUnits8.Name = "collarUnits8";
            this.collarUnits8.Size = new System.Drawing.Size(110, 21);
            this.collarUnits8.TabIndex = 76;
            // 
            // collarUnits7
            // 
            this.collarUnits7.FormattingEnabled = true;
            this.collarUnits7.Location = new System.Drawing.Point(399, 204);
            this.collarUnits7.Name = "collarUnits7";
            this.collarUnits7.Size = new System.Drawing.Size(110, 21);
            this.collarUnits7.TabIndex = 75;
            // 
            // collarUnits6
            // 
            this.collarUnits6.FormattingEnabled = true;
            this.collarUnits6.Location = new System.Drawing.Point(399, 177);
            this.collarUnits6.Name = "collarUnits6";
            this.collarUnits6.Size = new System.Drawing.Size(110, 21);
            this.collarUnits6.TabIndex = 74;
            // 
            // collarUnits5
            // 
            this.collarUnits5.FormattingEnabled = true;
            this.collarUnits5.Location = new System.Drawing.Point(399, 150);
            this.collarUnits5.Name = "collarUnits5";
            this.collarUnits5.Size = new System.Drawing.Size(110, 21);
            this.collarUnits5.TabIndex = 73;
            // 
            // collarUnits4
            // 
            this.collarUnits4.FormattingEnabled = true;
            this.collarUnits4.Location = new System.Drawing.Point(399, 123);
            this.collarUnits4.Name = "collarUnits4";
            this.collarUnits4.Size = new System.Drawing.Size(110, 21);
            this.collarUnits4.TabIndex = 72;
            // 
            // collarUnits3
            // 
            this.collarUnits3.FormattingEnabled = true;
            this.collarUnits3.Location = new System.Drawing.Point(399, 96);
            this.collarUnits3.Name = "collarUnits3";
            this.collarUnits3.Size = new System.Drawing.Size(110, 21);
            this.collarUnits3.TabIndex = 71;
            // 
            // collarUnits2
            // 
            this.collarUnits2.FormattingEnabled = true;
            this.collarUnits2.Location = new System.Drawing.Point(399, 69);
            this.collarUnits2.Name = "collarUnits2";
            this.collarUnits2.Size = new System.Drawing.Size(110, 21);
            this.collarUnits2.TabIndex = 70;
            // 
            // collarUnits1
            // 
            this.collarUnits1.FormattingEnabled = true;
            this.collarUnits1.Location = new System.Drawing.Point(399, 42);
            this.collarUnits1.Name = "collarUnits1";
            this.collarUnits1.Size = new System.Drawing.Size(110, 21);
            this.collarUnits1.TabIndex = 69;
            // 
            // collarLength20
            // 
            this.collarLength20.Location = new System.Drawing.Point(277, 558);
            this.collarLength20.Name = "collarLength20";
            this.collarLength20.Size = new System.Drawing.Size(100, 20);
            this.collarLength20.TabIndex = 68;
            // 
            // collarLength19
            // 
            this.collarLength19.Location = new System.Drawing.Point(277, 531);
            this.collarLength19.Name = "collarLength19";
            this.collarLength19.Size = new System.Drawing.Size(100, 20);
            this.collarLength19.TabIndex = 67;
            // 
            // collarLength18
            // 
            this.collarLength18.Location = new System.Drawing.Point(277, 504);
            this.collarLength18.Name = "collarLength18";
            this.collarLength18.Size = new System.Drawing.Size(100, 20);
            this.collarLength18.TabIndex = 66;
            // 
            // collarLength17
            // 
            this.collarLength17.Location = new System.Drawing.Point(277, 478);
            this.collarLength17.Name = "collarLength17";
            this.collarLength17.Size = new System.Drawing.Size(100, 20);
            this.collarLength17.TabIndex = 65;
            // 
            // collarLength16
            // 
            this.collarLength16.Location = new System.Drawing.Point(277, 450);
            this.collarLength16.Name = "collarLength16";
            this.collarLength16.Size = new System.Drawing.Size(100, 20);
            this.collarLength16.TabIndex = 64;
            // 
            // collarLength15
            // 
            this.collarLength15.Location = new System.Drawing.Point(277, 424);
            this.collarLength15.Name = "collarLength15";
            this.collarLength15.Size = new System.Drawing.Size(100, 20);
            this.collarLength15.TabIndex = 63;
            // 
            // collarLength14
            // 
            this.collarLength14.Location = new System.Drawing.Point(277, 397);
            this.collarLength14.Name = "collarLength14";
            this.collarLength14.Size = new System.Drawing.Size(100, 20);
            this.collarLength14.TabIndex = 62;
            // 
            // collarLength13
            // 
            this.collarLength13.Location = new System.Drawing.Point(277, 368);
            this.collarLength13.Name = "collarLength13";
            this.collarLength13.Size = new System.Drawing.Size(100, 20);
            this.collarLength13.TabIndex = 61;
            // 
            // collarLength12
            // 
            this.collarLength12.Location = new System.Drawing.Point(277, 341);
            this.collarLength12.Name = "collarLength12";
            this.collarLength12.Size = new System.Drawing.Size(100, 20);
            this.collarLength12.TabIndex = 60;
            // 
            // collarLength11
            // 
            this.collarLength11.Location = new System.Drawing.Point(277, 314);
            this.collarLength11.Name = "collarLength11";
            this.collarLength11.Size = new System.Drawing.Size(100, 20);
            this.collarLength11.TabIndex = 59;
            // 
            // collarLength10
            // 
            this.collarLength10.Location = new System.Drawing.Point(277, 287);
            this.collarLength10.Name = "collarLength10";
            this.collarLength10.Size = new System.Drawing.Size(100, 20);
            this.collarLength10.TabIndex = 58;
            // 
            // collarLength9
            // 
            this.collarLength9.Location = new System.Drawing.Point(277, 259);
            this.collarLength9.Name = "collarLength9";
            this.collarLength9.Size = new System.Drawing.Size(100, 20);
            this.collarLength9.TabIndex = 57;
            // 
            // collarLength8
            // 
            this.collarLength8.Location = new System.Drawing.Point(277, 231);
            this.collarLength8.Name = "collarLength8";
            this.collarLength8.Size = new System.Drawing.Size(100, 20);
            this.collarLength8.TabIndex = 56;
            // 
            // collarLength7
            // 
            this.collarLength7.Location = new System.Drawing.Point(277, 204);
            this.collarLength7.Name = "collarLength7";
            this.collarLength7.Size = new System.Drawing.Size(100, 20);
            this.collarLength7.TabIndex = 55;
            // 
            // collarLength6
            // 
            this.collarLength6.Location = new System.Drawing.Point(277, 178);
            this.collarLength6.Name = "collarLength6";
            this.collarLength6.Size = new System.Drawing.Size(100, 20);
            this.collarLength6.TabIndex = 54;
            // 
            // collarLength5
            // 
            this.collarLength5.Location = new System.Drawing.Point(277, 150);
            this.collarLength5.Name = "collarLength5";
            this.collarLength5.Size = new System.Drawing.Size(100, 20);
            this.collarLength5.TabIndex = 53;
            // 
            // collarLength4
            // 
            this.collarLength4.Location = new System.Drawing.Point(277, 124);
            this.collarLength4.Name = "collarLength4";
            this.collarLength4.Size = new System.Drawing.Size(100, 20);
            this.collarLength4.TabIndex = 52;
            // 
            // collarLength3
            // 
            this.collarLength3.Location = new System.Drawing.Point(277, 97);
            this.collarLength3.Name = "collarLength3";
            this.collarLength3.Size = new System.Drawing.Size(100, 20);
            this.collarLength3.TabIndex = 51;
            // 
            // collarLength2
            // 
            this.collarLength2.Location = new System.Drawing.Point(277, 70);
            this.collarLength2.Name = "collarLength2";
            this.collarLength2.Size = new System.Drawing.Size(100, 20);
            this.collarLength2.TabIndex = 50;
            // 
            // collarLength1
            // 
            this.collarLength1.Location = new System.Drawing.Point(277, 43);
            this.collarLength1.Name = "collarLength1";
            this.collarLength1.Size = new System.Drawing.Size(100, 20);
            this.collarLength1.TabIndex = 49;
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label106.Location = new System.Drawing.Point(1238, 24);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(109, 13);
            this.label106.TabIndex = 48;
            this.label106.Text = "NOTES FOR PROBE";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label105.Location = new System.Drawing.Point(1096, 24);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(40, 13);
            this.label105.TabIndex = 47;
            this.label105.Text = "UNITS";
            // 
            // bhaCombo20
            // 
            this.bhaCombo20.FormattingEnabled = true;
            this.bhaCombo20.Location = new System.Drawing.Point(67, 558);
            this.bhaCombo20.Name = "bhaCombo20";
            this.bhaCombo20.Size = new System.Drawing.Size(191, 21);
            this.bhaCombo20.TabIndex = 46;
            this.bhaCombo20.SelectedIndexChanged += new System.EventHandler(this.bhaCombo20_SelectedIndexChanged);
            // 
            // bhaCombo19
            // 
            this.bhaCombo19.FormattingEnabled = true;
            this.bhaCombo19.Location = new System.Drawing.Point(67, 531);
            this.bhaCombo19.Name = "bhaCombo19";
            this.bhaCombo19.Size = new System.Drawing.Size(191, 21);
            this.bhaCombo19.TabIndex = 45;
            this.bhaCombo19.SelectedIndexChanged += new System.EventHandler(this.bhaCombo19_SelectedIndexChanged);
            // 
            // bhaCombo18
            // 
            this.bhaCombo18.FormattingEnabled = true;
            this.bhaCombo18.Location = new System.Drawing.Point(67, 504);
            this.bhaCombo18.Name = "bhaCombo18";
            this.bhaCombo18.Size = new System.Drawing.Size(191, 21);
            this.bhaCombo18.TabIndex = 44;
            this.bhaCombo18.SelectedIndexChanged += new System.EventHandler(this.bhaCombo18_SelectedIndexChanged);
            // 
            // bhaCombo17
            // 
            this.bhaCombo17.FormattingEnabled = true;
            this.bhaCombo17.Location = new System.Drawing.Point(67, 477);
            this.bhaCombo17.Name = "bhaCombo17";
            this.bhaCombo17.Size = new System.Drawing.Size(191, 21);
            this.bhaCombo17.TabIndex = 43;
            this.bhaCombo17.SelectedIndexChanged += new System.EventHandler(this.bhaCombo17_SelectedIndexChanged);
            // 
            // bhaCombo16
            // 
            this.bhaCombo16.FormattingEnabled = true;
            this.bhaCombo16.Location = new System.Drawing.Point(67, 450);
            this.bhaCombo16.Name = "bhaCombo16";
            this.bhaCombo16.Size = new System.Drawing.Size(191, 21);
            this.bhaCombo16.TabIndex = 42;
            this.bhaCombo16.SelectedIndexChanged += new System.EventHandler(this.bhaCombo16_SelectedIndexChanged);
            // 
            // bhaCombo15
            // 
            this.bhaCombo15.FormattingEnabled = true;
            this.bhaCombo15.Location = new System.Drawing.Point(67, 423);
            this.bhaCombo15.Name = "bhaCombo15";
            this.bhaCombo15.Size = new System.Drawing.Size(191, 21);
            this.bhaCombo15.TabIndex = 41;
            this.bhaCombo15.SelectedIndexChanged += new System.EventHandler(this.bhaCombo15_SelectedIndexChanged);
            // 
            // bhaCombo14
            // 
            this.bhaCombo14.FormattingEnabled = true;
            this.bhaCombo14.Location = new System.Drawing.Point(67, 396);
            this.bhaCombo14.Name = "bhaCombo14";
            this.bhaCombo14.Size = new System.Drawing.Size(191, 21);
            this.bhaCombo14.TabIndex = 40;
            this.bhaCombo14.SelectedIndexChanged += new System.EventHandler(this.bhaCombo14_SelectedIndexChanged);
            // 
            // bhaCombo13
            // 
            this.bhaCombo13.FormattingEnabled = true;
            this.bhaCombo13.Location = new System.Drawing.Point(67, 367);
            this.bhaCombo13.Name = "bhaCombo13";
            this.bhaCombo13.Size = new System.Drawing.Size(191, 21);
            this.bhaCombo13.TabIndex = 39;
            this.bhaCombo13.SelectedIndexChanged += new System.EventHandler(this.bhaCombo13_SelectedIndexChanged);
            // 
            // bhaCombo12
            // 
            this.bhaCombo12.FormattingEnabled = true;
            this.bhaCombo12.Location = new System.Drawing.Point(67, 340);
            this.bhaCombo12.Name = "bhaCombo12";
            this.bhaCombo12.Size = new System.Drawing.Size(191, 21);
            this.bhaCombo12.TabIndex = 38;
            this.bhaCombo12.SelectedIndexChanged += new System.EventHandler(this.bhaCombo12_SelectedIndexChanged);
            // 
            // bhaCombo11
            // 
            this.bhaCombo11.FormattingEnabled = true;
            this.bhaCombo11.Location = new System.Drawing.Point(67, 313);
            this.bhaCombo11.Name = "bhaCombo11";
            this.bhaCombo11.Size = new System.Drawing.Size(191, 21);
            this.bhaCombo11.TabIndex = 37;
            this.bhaCombo11.SelectedIndexChanged += new System.EventHandler(this.bhaCombo11_SelectedIndexChanged);
            // 
            // bhaCombo10
            // 
            this.bhaCombo10.FormattingEnabled = true;
            this.bhaCombo10.Location = new System.Drawing.Point(67, 286);
            this.bhaCombo10.Name = "bhaCombo10";
            this.bhaCombo10.Size = new System.Drawing.Size(191, 21);
            this.bhaCombo10.TabIndex = 36;
            this.bhaCombo10.SelectedIndexChanged += new System.EventHandler(this.bhaCombo10_SelectedIndexChanged);
            // 
            // bhaCombo9
            // 
            this.bhaCombo9.FormattingEnabled = true;
            this.bhaCombo9.Location = new System.Drawing.Point(67, 258);
            this.bhaCombo9.Name = "bhaCombo9";
            this.bhaCombo9.Size = new System.Drawing.Size(191, 21);
            this.bhaCombo9.TabIndex = 35;
            this.bhaCombo9.SelectedIndexChanged += new System.EventHandler(this.bhaCombo9_SelectedIndexChanged);
            // 
            // bhaCombo8
            // 
            this.bhaCombo8.FormattingEnabled = true;
            this.bhaCombo8.Location = new System.Drawing.Point(67, 231);
            this.bhaCombo8.Name = "bhaCombo8";
            this.bhaCombo8.Size = new System.Drawing.Size(191, 21);
            this.bhaCombo8.TabIndex = 34;
            this.bhaCombo8.SelectedIndexChanged += new System.EventHandler(this.bhaCombo8_SelectedIndexChanged);
            // 
            // bhaCombo7
            // 
            this.bhaCombo7.FormattingEnabled = true;
            this.bhaCombo7.Location = new System.Drawing.Point(67, 204);
            this.bhaCombo7.Name = "bhaCombo7";
            this.bhaCombo7.Size = new System.Drawing.Size(191, 21);
            this.bhaCombo7.TabIndex = 33;
            this.bhaCombo7.SelectedIndexChanged += new System.EventHandler(this.bhaCombo7_SelectedIndexChanged);
            // 
            // bhaCombo6
            // 
            this.bhaCombo6.FormattingEnabled = true;
            this.bhaCombo6.Location = new System.Drawing.Point(67, 177);
            this.bhaCombo6.Name = "bhaCombo6";
            this.bhaCombo6.Size = new System.Drawing.Size(191, 21);
            this.bhaCombo6.TabIndex = 32;
            this.bhaCombo6.SelectedIndexChanged += new System.EventHandler(this.bhaCombo6_SelectedIndexChanged);
            // 
            // bhaCombo5
            // 
            this.bhaCombo5.FormattingEnabled = true;
            this.bhaCombo5.Location = new System.Drawing.Point(67, 150);
            this.bhaCombo5.Name = "bhaCombo5";
            this.bhaCombo5.Size = new System.Drawing.Size(191, 21);
            this.bhaCombo5.TabIndex = 31;
            this.bhaCombo5.SelectedIndexChanged += new System.EventHandler(this.bhaCombo5_SelectedIndexChanged);
            // 
            // bhaCombo4
            // 
            this.bhaCombo4.FormattingEnabled = true;
            this.bhaCombo4.Location = new System.Drawing.Point(67, 123);
            this.bhaCombo4.Name = "bhaCombo4";
            this.bhaCombo4.Size = new System.Drawing.Size(191, 21);
            this.bhaCombo4.TabIndex = 30;
            this.bhaCombo4.SelectedIndexChanged += new System.EventHandler(this.bhaCombo4_SelectedIndexChanged);
            // 
            // bhaCombo3
            // 
            this.bhaCombo3.FormattingEnabled = true;
            this.bhaCombo3.Location = new System.Drawing.Point(67, 96);
            this.bhaCombo3.Name = "bhaCombo3";
            this.bhaCombo3.Size = new System.Drawing.Size(191, 21);
            this.bhaCombo3.TabIndex = 29;
            this.bhaCombo3.SelectedIndexChanged += new System.EventHandler(this.bhaCombo3_SelectedIndexChanged);
            // 
            // bhaCombo2
            // 
            this.bhaCombo2.FormattingEnabled = true;
            this.bhaCombo2.Location = new System.Drawing.Point(67, 69);
            this.bhaCombo2.Name = "bhaCombo2";
            this.bhaCombo2.Size = new System.Drawing.Size(191, 21);
            this.bhaCombo2.TabIndex = 28;
            this.bhaCombo2.SelectedIndexChanged += new System.EventHandler(this.bhaCombo2_SelectedIndexChanged);
            // 
            // bhaCombo1
            // 
            this.bhaCombo1.FormattingEnabled = true;
            this.bhaCombo1.Location = new System.Drawing.Point(67, 42);
            this.bhaCombo1.Name = "bhaCombo1";
            this.bhaCombo1.Size = new System.Drawing.Size(191, 21);
            this.bhaCombo1.TabIndex = 27;
            this.bhaCombo1.SelectedIndexChanged += new System.EventHandler(this.bhaCombo1_SelectedIndexChanged);
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label104.Location = new System.Drawing.Point(29, 561);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(19, 13);
            this.label104.TabIndex = 26;
            this.label104.Text = "20";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label103.Location = new System.Drawing.Point(29, 534);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(19, 13);
            this.label103.TabIndex = 25;
            this.label103.Text = "19";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label102.Location = new System.Drawing.Point(29, 507);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(19, 13);
            this.label102.TabIndex = 24;
            this.label102.Text = "18";
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label101.Location = new System.Drawing.Point(29, 481);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(19, 13);
            this.label101.TabIndex = 23;
            this.label101.Text = "17";
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label100.Location = new System.Drawing.Point(29, 454);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(19, 13);
            this.label100.TabIndex = 22;
            this.label100.Text = "16";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label99.Location = new System.Drawing.Point(29, 426);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(19, 13);
            this.label99.TabIndex = 21;
            this.label99.Text = "15";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label98.Location = new System.Drawing.Point(29, 399);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(19, 13);
            this.label98.TabIndex = 20;
            this.label98.Text = "14";
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label97.Location = new System.Drawing.Point(29, 370);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(19, 13);
            this.label97.TabIndex = 19;
            this.label97.Text = "13";
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label96.Location = new System.Drawing.Point(29, 343);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(19, 13);
            this.label96.TabIndex = 18;
            this.label96.Text = "12";
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label95.Location = new System.Drawing.Point(29, 316);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(19, 13);
            this.label95.TabIndex = 17;
            this.label95.Text = "11";
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label94.Location = new System.Drawing.Point(29, 289);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(19, 13);
            this.label94.TabIndex = 16;
            this.label94.Text = "10";
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label93.Location = new System.Drawing.Point(32, 261);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(13, 13);
            this.label93.TabIndex = 15;
            this.label93.Text = "9";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label92.Location = new System.Drawing.Point(32, 234);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(13, 13);
            this.label92.TabIndex = 14;
            this.label92.Text = "8";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label91.Location = new System.Drawing.Point(32, 207);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(13, 13);
            this.label91.TabIndex = 13;
            this.label91.Text = "7";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label90.Location = new System.Drawing.Point(32, 180);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(13, 13);
            this.label90.TabIndex = 12;
            this.label90.Text = "6";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label89.Location = new System.Drawing.Point(32, 153);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(13, 13);
            this.label89.TabIndex = 11;
            this.label89.Text = "5";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label88.Location = new System.Drawing.Point(32, 126);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(13, 13);
            this.label88.TabIndex = 10;
            this.label88.Text = "4";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label87.Location = new System.Drawing.Point(32, 99);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(13, 13);
            this.label87.TabIndex = 9;
            this.label87.Text = "3";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label86.Location = new System.Drawing.Point(32, 72);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(13, 13);
            this.label86.TabIndex = 8;
            this.label86.Text = "2";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label85.Location = new System.Drawing.Point(32, 45);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(13, 13);
            this.label85.TabIndex = 7;
            this.label85.Text = "1";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label77.Location = new System.Drawing.Point(22, 24);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(43, 13);
            this.label77.TabIndex = 0;
            this.label77.Text = "ITEM #";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label78.Location = new System.Drawing.Point(138, 24);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(29, 13);
            this.label78.TabIndex = 1;
            this.label78.Text = "BHA";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label83.Location = new System.Drawing.Point(936, 24);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(91, 13);
            this.label83.TabIndex = 6;
            this.label83.Text = "PROBE LENGTH";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label79.Location = new System.Drawing.Point(281, 24);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(96, 13);
            this.label79.TabIndex = 2;
            this.label79.Text = "COLLAR LENGTH";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label82.Location = new System.Drawing.Point(720, 24);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(190, 13);
            this.label82.TabIndex = 5;
            this.label82.Text = "PROBES (TO CALC. NMDC LENGTH)";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label80.Location = new System.Drawing.Point(430, 24);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(40, 13);
            this.label80.TabIndex = 3;
            this.label80.Text = "UNITS";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label81.Location = new System.Drawing.Point(569, 24);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(94, 13);
            this.label81.TabIndex = 4;
            this.label81.Text = "NOTES FOR BHA";
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            // 
            // mainPanel
            // 
            this.mainPanel.Controls.Add(this.mainTabControl);
            this.mainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainPanel.Location = new System.Drawing.Point(0, 0);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(1904, 1041);
            this.mainPanel.TabIndex = 9;
            this.mainPanel.SizeChanged += new System.EventHandler(this.mainPanel_SizeChanged);
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(0, 0);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(200, 100);
            this.tabPage2.TabIndex = 0;
            this.tabPage2.Text = "tabPage2";
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // comRefreshTimer
            // 
            this.comRefreshTimer.Enabled = true;
            this.comRefreshTimer.Interval = 500;
            this.comRefreshTimer.Tick += new System.EventHandler(this.comRefreshTimer_Tick);
            // 
            // gammaTimer
            // 
            this.gammaTimer.Interval = 30000;
            this.gammaTimer.Tick += new System.EventHandler(this.gammaTimer_Tick);
            // 
            // StopwatchTimer
            // 
            this.StopwatchTimer.Enabled = true;
            this.StopwatchTimer.Interval = 1000;
            this.StopwatchTimer.Tick += new System.EventHandler(this.StopwatchTimer_Tick);
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.Visible = false;
            // 
            // bhaLengthCalculateButton
            // 
            this.bhaLengthCalculateButton.Location = new System.Drawing.Point(446, 607);
            this.bhaLengthCalculateButton.Name = "bhaLengthCalculateButton";
            this.bhaLengthCalculateButton.Size = new System.Drawing.Size(81, 23);
            this.bhaLengthCalculateButton.TabIndex = 194;
            this.bhaLengthCalculateButton.Text = "CALCULATE";
            this.bhaLengthCalculateButton.UseVisualStyleBackColor = true;
            this.bhaLengthCalculateButton.Click += new System.EventHandler(this.bhaLengthCalculateButton_Click);
            // 
            // bhaLengthBox
            // 
            this.bhaLengthBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.bhaLengthBox.Location = new System.Drawing.Point(340, 601);
            this.bhaLengthBox.Name = "bhaLengthBox";
            this.bhaLengthBox.Size = new System.Drawing.Size(100, 32);
            this.bhaLengthBox.TabIndex = 193;
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label108.Location = new System.Drawing.Point(182, 607);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(152, 26);
            this.label108.TabIndex = 192;
            this.label108.Text = "BHA LENGTH";
            // 
            // reloadTablesButton
            // 
            this.reloadTablesButton.Location = new System.Drawing.Point(625, 12);
            this.reloadTablesButton.Name = "reloadTablesButton";
            this.reloadTablesButton.Size = new System.Drawing.Size(124, 23);
            this.reloadTablesButton.TabIndex = 11;
            this.reloadTablesButton.Text = "RELOAD TABLES";
            this.reloadTablesButton.UseVisualStyleBackColor = true;
            this.reloadTablesButton.Click += new System.EventHandler(this.reloadTablesButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1904, 1041);
            this.Controls.Add(this.mainPanel);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "WOLVERINE FIELD APPLICATIONS";
            this.mainTabControl.ResumeLayout(false);
            this.batCalcTab.ResumeLayout(false);
            this.batteryCalcTabControl.ResumeLayout(false);
            this.mainTab.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.pulserSettingGroupbox.ResumeLayout(false);
            this.pulserSettingGroupbox.PerformLayout();
            this.telemetryGroupBox.ResumeLayout(false);
            this.telemetryGroupBox.PerformLayout();
            this.energyConsumptionGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.toolGroupBox.ResumeLayout(false);
            this.toolGroupBox.PerformLayout();
            this.powerConsumptionTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.toolTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resistivityPowerConsumptionTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.powerConsumptionTable)).EndInit();
            this.pulserDataTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tnpTdOptionTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pulserConsumptionTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.slotWidth200Table)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.slotWidth0213Table)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.slotWidth160Table)).EndInit();
            this.orificeCalcTab.ResumeLayout(false);
            this.orificeCalcTabControl.ResumeLayout(false);
            this.orificeMainTab.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.orificeAdminGroupBox.ResumeLayout(false);
            this.orificeAdminGroupBox.PerformLayout();
            this.orificeTablesTab.ResumeLayout(false);
            this.atmGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pulseAmplitudeAtmTable)).EndInit();
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pulserSpecTable)).EndInit();
            this.psiGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pulseAmplitudePSITable)).EndInit();
            this.rssCalcTab.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.sensorCalTab.ResumeLayout(false);
            this.sensorCalTab.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.gammaCalTab.ResumeLayout(false);
            this.gammaCalTab.PerformLayout();
            this.gammaCalPanel.ResumeLayout(false);
            this.gammaCalPanel.PerformLayout();
            this.novDmCalTab.ResumeLayout(false);
            this.bhaLengthBuilderTab.ResumeLayout(false);
            this.bhaLengthBuilderTab.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.mainPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl mainTabControl;
        private System.Windows.Forms.TabPage batCalcTab;
        private System.Windows.Forms.GroupBox toolGroupBox;
        private System.Windows.Forms.DataGridView toolTable;
        private System.Windows.Forms.CheckBox wolverineMicrohopCheckbox;
        private System.Windows.Forms.CheckBox rmsPressureToolCheckbox;
        private System.Windows.Forms.CheckBox rmsResistivtyCheckbox;
        private System.Windows.Forms.CheckBox wolverineTranslator;
        private System.Windows.Forms.CheckBox gamma;
        private System.Windows.Forms.CheckBox wolverineMasterWDirectional;
        private System.Windows.Forms.CheckBox wolverineTmpSmartSolenoid;
        private System.Windows.Forms.TabControl batteryCalcTabControl;
        private System.Windows.Forms.TabPage mainTab;
        private System.Windows.Forms.TabPage powerConsumptionTab;
        private System.Windows.Forms.DataGridViewTextBoxColumn toolColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn inStringColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn currentColumn;
        private System.Windows.Forms.TabPage pulserDataTab;
        private System.Windows.Forms.DataGridView slotWidth160Table;
        private System.Windows.Forms.DataGridView slotWidth200Table;
        private System.Windows.Forms.DataGridView slotWidth0213Table;
        private System.Windows.Forms.DataGridView powerConsumptionTable;
        private System.Windows.Forms.GroupBox pulserSettingGroupbox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox pulseWidthTextBox;
        private System.Windows.Forms.TextBox totalLifeTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.MaskedTextBox numberOfBatteriesTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox resistivitySampleRateComboBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox ropTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox tdTmpComboBox;
        private System.Windows.Forms.CheckBox smartSolenoidCheckBox;
        private System.Windows.Forms.GroupBox energyConsumptionGroupBox;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.DataGridView pulserConsumptionTable;
        private System.Windows.Forms.Button setPwButton;
        private System.Windows.Forms.DataGridView resistivityPowerConsumptionTable;
        private System.Windows.Forms.Button setBatteryButton;
        private System.Windows.Forms.GroupBox telemetryGroupBox;
        private System.Windows.Forms.DataGridView tnpTdOptionTable;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox seventeenBitToolfaceItem;
        private System.Windows.Forms.TextBox sixteenBitToolfaceItem;
        private System.Windows.Forms.TextBox fifteenBitToolfaceItem;
        private System.Windows.Forms.TextBox fourteenBitToolfaceItem;
        private System.Windows.Forms.TextBox thirteenBitToolfaceItem;
        private System.Windows.Forms.TextBox twelveBitToolfaceItem;
        private System.Windows.Forms.TextBox elevenBitToolfaceItem;
        private System.Windows.Forms.TextBox tenBitToolfaceItem;
        private System.Windows.Forms.TextBox nineBitToolfaceItem;
        private System.Windows.Forms.TextBox eightBitToolfaceItem;
        private System.Windows.Forms.TextBox sevenBitToolfaceItem;
        private System.Windows.Forms.TextBox sixBitToolfaceItem;
        private System.Windows.Forms.TextBox fiveBitToolfaceItem;
        private System.Windows.Forms.TextBox fourBitToolfaceItem;
        private System.Windows.Forms.TextBox threeBitToolfaceItem;
        private System.Windows.Forms.TextBox twoBitToolfaceItem;
        private System.Windows.Forms.TextBox oneBitToolfaceItem;
        private System.Windows.Forms.TextBox seventeenBitSurveyItem;
        private System.Windows.Forms.TextBox sixteenBitSurveyItem;
        private System.Windows.Forms.TextBox fifteenBitSurveyItem;
        private System.Windows.Forms.TextBox fourteenBitSurveyItem;
        private System.Windows.Forms.TextBox thirteenBitSurveyItem;
        private System.Windows.Forms.TextBox twelveBitSurveyItem;
        private System.Windows.Forms.TextBox elevenBitSurveyItem;
        private System.Windows.Forms.TextBox tenBitSurveyItem;
        private System.Windows.Forms.TextBox nineBitSurveyItem;
        private System.Windows.Forms.TextBox eightBitSurveyItem;
        private System.Windows.Forms.TextBox sevenBitSurveyItem;
        private System.Windows.Forms.TextBox sixBitSurveyItem;
        private System.Windows.Forms.TextBox fiveBitSurveyItem;
        private System.Windows.Forms.TextBox fourBitSurveyItem;
        private System.Windows.Forms.TextBox threeBitSurveyItem;
        private System.Windows.Forms.TextBox twoBitSurveyItem;
        private System.Windows.Forms.TextBox oneBitSurveyItem;
        private System.Windows.Forms.TextBox distanceBetweenDataToolfaceBox;
        private System.Windows.Forms.TextBox joulesPerPulseToolfaceBox;
        private System.Windows.Forms.TextBox pulsesPerHourToolfaceBox;
        private System.Windows.Forms.TextBox numberOfPulsesToolfaceBox;
        private System.Windows.Forms.TextBox numberOfPulsesSurveyBox;
        private System.Windows.Forms.TextBox bitRateToolfaceBox;
        private System.Windows.Forms.TextBox bitRateSurveyBox;
        private System.Windows.Forms.TextBox timeSequenceToolfaceBox;
        private System.Windows.Forms.TextBox timeSequenceSurveyBox;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button loadConfigButton;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.Button processButton;
        private System.Windows.Forms.Button setROPButton;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.TabPage orificeCalcTab;
        private System.Windows.Forms.GroupBox psiGroupBox;
        private System.Windows.Forms.DataGridView pulseAmplitudePSITable;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.DataGridView pulserSpecTable;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button defaultButton;
        private System.Windows.Forms.Button setButton;
        private System.Windows.Forms.TextBox roundTextBox;
        private System.Windows.Forms.TextBox mudWeightTextbox;
        private System.Windows.Forms.TextBox modelOrificesTextbox;
        private System.Windows.Forms.TextBox modelTypicalTextbox;
        private System.Windows.Forms.TextBox pumpEfficiencyTextbox;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.GroupBox atmGroupBox;
        private System.Windows.Forms.DataGridView pulseAmplitudeAtmTable;
        private System.Windows.Forms.Panel mainPanel;
        private System.Windows.Forms.TabPage rssCalcTab;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.TextBox nozzle8Units;
        private System.Windows.Forms.TextBox nozzle8Size;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.TextBox nozzle7Units;
        private System.Windows.Forms.TextBox nozzle7Size;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.TextBox nozzle6Units;
        private System.Windows.Forms.TextBox nozzle6Size;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.TextBox nozzle5Units;
        private System.Windows.Forms.TextBox nozzle5Size;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.TextBox nozzle4Units;
        private System.Windows.Forms.TextBox nozzle4Size;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox nozzle3Units;
        private System.Windows.Forms.TextBox nozzle3Size;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox nozzle2Units;
        private System.Windows.Forms.ComboBox nozzle1Units;
        private System.Windows.Forms.TextBox nozzle2Size;
        private System.Windows.Forms.TextBox nozzle1Size;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.ComboBox pinRestrictNozzleUnit;
        private System.Windows.Forms.TextBox pinRestrictTFABox;
        private System.Windows.Forms.TextBox pinRestrictNozzleSize;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox flowRateUnitBox;
        private System.Windows.Forms.ComboBox mudWeightUnitsBox;
        private System.Windows.Forms.TextBox flowRateSection3;
        private System.Windows.Forms.TextBox flowRateSection2;
        private System.Windows.Forms.TextBox flowRateSection1;
        private System.Windows.Forms.TextBox mudWeightSection3;
        private System.Windows.Forms.TextBox mudWeightSection2;
        private System.Windows.Forms.TextBox mudWeightSection1;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.ComboBox pressureDropUnits;
        private System.Windows.Forms.TextBox pressureDrop3Box;
        private System.Windows.Forms.TextBox pressureDrop2Box;
        private System.Windows.Forms.TextBox pressureDrop1Box;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.TextBox tfaUnits;
        private System.Windows.Forms.TextBox nozzleTFA;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Button rssCalculateButton;
        private System.Windows.Forms.Button rssResetButton;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.ComboBox orificeFlowRateUnitsBox;
        private System.Windows.Forms.ComboBox orificeMudWeightUnitsBox;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.GroupBox orificeAdminGroupBox;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.ComboBox orificeToolSizeBox;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.TabControl orificeCalcTabControl;
        private System.Windows.Forms.TabPage orificeMainTab;
        private System.Windows.Forms.TabPage orificeTablesTab;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox orificeFlowRateTextbox;
        private System.Windows.Forms.CheckBox fourThreeQuarterToolSizeBox;
        private System.Windows.Forms.TextBox pinRestrictTFAUnitsBox;
        private System.Windows.Forms.CheckBox wolverineLndcCheckbox;
        private System.Windows.Forms.Button printButton;
        private System.Windows.Forms.PrintDialog printDialog1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.TextBox motorBypassPercentBox;
        private System.Windows.Forms.TabPage sensorCalTab;
        private System.Windows.Forms.Button alt_open_btn;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.ComboBox comport;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage gammaCalTab;
        private System.Windows.Forms.TabPage novDmCalTab;
        private System.Windows.Forms.CheckBox rmsBoxCheckBox;
        private System.Windows.Forms.CheckBox qbusCheckBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label mwdBit15;
        private System.Windows.Forms.Label mwdBit14;
        private System.Windows.Forms.Label mwdBit13;
        private System.Windows.Forms.Label mwdBit12;
        private System.Windows.Forms.Label mwdBit11;
        private System.Windows.Forms.Label mwdBit10;
        private System.Windows.Forms.Label mwdBit9;
        private System.Windows.Forms.Label mwdBit8;
        private System.Windows.Forms.Label mwdBit7;
        private System.Windows.Forms.Label mwdBit6;
        private System.Windows.Forms.Label mwdBit5;
        private System.Windows.Forms.Label mwdBit4;
        private System.Windows.Forms.Label mwdBit3;
        private System.Windows.Forms.Label mwdBit2;
        private System.Windows.Forms.Label mwdBit0;
        private System.Windows.Forms.Label mwdBit1;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.RichTextBox ReceivingBox;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.RichTextBox dataBox;
        private System.Windows.Forms.Timer comRefreshTimer;
        private System.Windows.Forms.Button readDataButton;
        private System.Windows.Forms.Button readPowerButton;
        private System.Windows.Forms.Button connectButton;
        private System.Windows.Forms.Button runButton;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.RichTextBox firstFiveMinGammaCpsBox;
        private System.Windows.Forms.Timer gammaTimer;
        private System.Windows.Forms.Button calculateGammaFactorButton;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.RichTextBox gammaScalingFactorBox;
        private System.Windows.Forms.Button startBackGroundCpsButton;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.RichTextBox backgroundCpsBox;
        private System.Windows.Forms.Button startCalFirst5MinButton;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.RichTextBox netCalibratorCpsTextBox;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.RichTextBox avgCalibratorCpsBox;
        private System.Windows.Forms.Button calibratorLastFiveMinButton;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.RichTextBox calibratorLastFiveMinBox;
        private System.Windows.Forms.RichTextBox timerTextBox;
        private System.Windows.Forms.Timer StopwatchTimer;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.Panel gammaCalPanel;
        private System.Windows.Forms.RichTextBox serialNumberBox;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label apiCalibratorLabel;
        private System.Windows.Forms.Button readEepromButton;
        private System.Windows.Forms.Button takeSurveyButton;
        private System.Windows.Forms.Button readKTableButton;
        private System.Windows.Forms.Button rollTestButton;
        private System.Windows.Forms.Button dmDataButton;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.CheckBox digitalGammaCheckbox;
        private System.Windows.Forms.Button readGammaButton;
        private System.Windows.Forms.CheckBox woftGammaCheckBox;
        private System.Windows.Forms.Button configGammaButton;
        private System.Windows.Forms.CheckBox apsWpr;
        private System.Windows.Forms.TextBox timeSequenceRotateBox;
        private System.Windows.Forms.TextBox seventeenBitRotateItem;
        private System.Windows.Forms.TextBox sixteenBitRotateItem;
        private System.Windows.Forms.TextBox fifteenBitRotateItem;
        private System.Windows.Forms.TextBox fourteenBitRotateItem;
        private System.Windows.Forms.TextBox thirteenBitRotateItem;
        private System.Windows.Forms.TextBox twelveBitRotateItem;
        private System.Windows.Forms.TextBox elevenBitRotateItem;
        private System.Windows.Forms.TextBox tenBitRotateItem;
        private System.Windows.Forms.TextBox nineBitRotateItem;
        private System.Windows.Forms.TextBox eightBitRotateItem;
        private System.Windows.Forms.TextBox sevenBitRotateItem;
        private System.Windows.Forms.TextBox sixBitRotateItem;
        private System.Windows.Forms.TextBox fiveBitRotateItem;
        private System.Windows.Forms.TextBox fourBitRotateItem;
        private System.Windows.Forms.TextBox threeBitRotateItem;
        private System.Windows.Forms.TextBox twoBitRotateItem;
        private System.Windows.Forms.TextBox oneBitRotateItem;
        private System.Windows.Forms.ComboBox toolfaceComboBox;
        private System.Windows.Forms.ComboBox surveyComboBox;
        private System.Windows.Forms.TextBox distanceBetweenDataRotateBox;
        private System.Windows.Forms.TextBox joulesPerPulseRotateBox;
        private System.Windows.Forms.TextBox pulsesPerHourRotateBox;
        private System.Windows.Forms.TextBox numberOfPulsesRotateBox;
        private System.Windows.Forms.TextBox bitRateRotateBox;
        private System.Windows.Forms.ComboBox rotateComboBox;
        private System.Windows.Forms.TextBox depthForPressureBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox depthTdComboBox;
        private System.Windows.Forms.ComboBox pressureAtSurfaceUnitComboBox;
        private System.Windows.Forms.TextBox pressureAtSurfaceBox;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.ComboBox pressureAtSourceUnitComboBox;
        private System.Windows.Forms.TextBox pressureAtSourceBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.TabPage bhaLengthBuilderTab;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.ComboBox bhaCombo5;
        private System.Windows.Forms.ComboBox bhaCombo4;
        private System.Windows.Forms.ComboBox bhaCombo3;
        private System.Windows.Forms.ComboBox bhaCombo2;
        private System.Windows.Forms.ComboBox bhaCombo1;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.ComboBox bhaCombo20;
        private System.Windows.Forms.ComboBox bhaCombo19;
        private System.Windows.Forms.ComboBox bhaCombo18;
        private System.Windows.Forms.ComboBox bhaCombo17;
        private System.Windows.Forms.ComboBox bhaCombo16;
        private System.Windows.Forms.ComboBox bhaCombo15;
        private System.Windows.Forms.ComboBox bhaCombo14;
        private System.Windows.Forms.ComboBox bhaCombo13;
        private System.Windows.Forms.ComboBox bhaCombo12;
        private System.Windows.Forms.ComboBox bhaCombo11;
        private System.Windows.Forms.ComboBox bhaCombo10;
        private System.Windows.Forms.ComboBox bhaCombo9;
        private System.Windows.Forms.ComboBox bhaCombo8;
        private System.Windows.Forms.ComboBox bhaCombo7;
        private System.Windows.Forms.ComboBox bhaCombo6;
        private System.Windows.Forms.TextBox probeNotes20;
        private System.Windows.Forms.TextBox probeNotes19;
        private System.Windows.Forms.TextBox probeNotes18;
        private System.Windows.Forms.TextBox probeNotes17;
        private System.Windows.Forms.TextBox probeNotes16;
        private System.Windows.Forms.TextBox probeNotes15;
        private System.Windows.Forms.TextBox probeNotes14;
        private System.Windows.Forms.TextBox probeNotes13;
        private System.Windows.Forms.TextBox probeNotes12;
        private System.Windows.Forms.TextBox probeNotes11;
        private System.Windows.Forms.TextBox probeNotes10;
        private System.Windows.Forms.TextBox probeNotes9;
        private System.Windows.Forms.TextBox probeNotes8;
        private System.Windows.Forms.TextBox probeNotes7;
        private System.Windows.Forms.TextBox probeNotes6;
        private System.Windows.Forms.TextBox probeNotes5;
        private System.Windows.Forms.TextBox probeNotes4;
        private System.Windows.Forms.TextBox probeNotes3;
        private System.Windows.Forms.TextBox probeNotes2;
        private System.Windows.Forms.TextBox probeNotes1;
        private System.Windows.Forms.ComboBox probeUnits20;
        private System.Windows.Forms.ComboBox probeUnits19;
        private System.Windows.Forms.ComboBox probeUnits18;
        private System.Windows.Forms.ComboBox probeUnits17;
        private System.Windows.Forms.ComboBox probeUnits16;
        private System.Windows.Forms.ComboBox probeUnits15;
        private System.Windows.Forms.ComboBox probeUnits14;
        private System.Windows.Forms.ComboBox probeUnits13;
        private System.Windows.Forms.ComboBox probeUnits12;
        private System.Windows.Forms.ComboBox probeUnits11;
        private System.Windows.Forms.ComboBox probeUnits10;
        private System.Windows.Forms.ComboBox probeUnits9;
        private System.Windows.Forms.ComboBox probeUnits8;
        private System.Windows.Forms.ComboBox probeUnits7;
        private System.Windows.Forms.ComboBox probeUnits6;
        private System.Windows.Forms.ComboBox probeUnits5;
        private System.Windows.Forms.ComboBox probeUnits4;
        private System.Windows.Forms.ComboBox probeUnits3;
        private System.Windows.Forms.ComboBox probeUnits2;
        private System.Windows.Forms.ComboBox probeUnits1;
        private System.Windows.Forms.TextBox probeLength20;
        private System.Windows.Forms.TextBox probeLength19;
        private System.Windows.Forms.TextBox probeLength18;
        private System.Windows.Forms.TextBox probeLength17;
        private System.Windows.Forms.TextBox probeLength16;
        private System.Windows.Forms.TextBox probeLength15;
        private System.Windows.Forms.TextBox probeLength14;
        private System.Windows.Forms.TextBox probeLength13;
        private System.Windows.Forms.TextBox probeLength12;
        private System.Windows.Forms.TextBox probeLength11;
        private System.Windows.Forms.TextBox probeLength10;
        private System.Windows.Forms.TextBox probeLength9;
        private System.Windows.Forms.TextBox probeLength8;
        private System.Windows.Forms.TextBox probeLength7;
        private System.Windows.Forms.TextBox probeLength6;
        private System.Windows.Forms.TextBox probeLength5;
        private System.Windows.Forms.TextBox probeLength4;
        private System.Windows.Forms.TextBox probeLength3;
        private System.Windows.Forms.TextBox probeLength2;
        private System.Windows.Forms.TextBox probeLength1;
        private System.Windows.Forms.ComboBox probeCombo20;
        private System.Windows.Forms.ComboBox probeCombo19;
        private System.Windows.Forms.ComboBox probeCombo18;
        private System.Windows.Forms.ComboBox probeCombo17;
        private System.Windows.Forms.ComboBox probeCombo16;
        private System.Windows.Forms.ComboBox probeCombo15;
        private System.Windows.Forms.ComboBox probeCombo14;
        private System.Windows.Forms.ComboBox probeCombo13;
        private System.Windows.Forms.ComboBox probeCombo12;
        private System.Windows.Forms.ComboBox probeCombo11;
        private System.Windows.Forms.ComboBox probeCombo10;
        private System.Windows.Forms.ComboBox probeCombo9;
        private System.Windows.Forms.ComboBox probeCombo8;
        private System.Windows.Forms.ComboBox probeCombo7;
        private System.Windows.Forms.ComboBox probeCombo6;
        private System.Windows.Forms.ComboBox probeCombo5;
        private System.Windows.Forms.ComboBox probeCombo4;
        private System.Windows.Forms.ComboBox probeCombo3;
        private System.Windows.Forms.ComboBox probeCombo2;
        private System.Windows.Forms.ComboBox probeCombo1;
        private System.Windows.Forms.TextBox collarNotes20;
        private System.Windows.Forms.TextBox collarNotes19;
        private System.Windows.Forms.TextBox collarNotes18;
        private System.Windows.Forms.TextBox collarNotes17;
        private System.Windows.Forms.TextBox collarNotes16;
        private System.Windows.Forms.TextBox collarNotes15;
        private System.Windows.Forms.TextBox collarNotes14;
        private System.Windows.Forms.TextBox collarNotes13;
        private System.Windows.Forms.TextBox collarNotes12;
        private System.Windows.Forms.TextBox collarNotes11;
        private System.Windows.Forms.TextBox collarNotes10;
        private System.Windows.Forms.TextBox collarNotes9;
        private System.Windows.Forms.TextBox collarNotes8;
        private System.Windows.Forms.TextBox collarNotes7;
        private System.Windows.Forms.TextBox collarNotes6;
        private System.Windows.Forms.TextBox collarNotes5;
        private System.Windows.Forms.TextBox collarNotes4;
        private System.Windows.Forms.TextBox collarNotes3;
        private System.Windows.Forms.TextBox collarNotes2;
        private System.Windows.Forms.TextBox collarNotes1;
        private System.Windows.Forms.ComboBox collarUnits20;
        private System.Windows.Forms.ComboBox collarUnits19;
        private System.Windows.Forms.ComboBox collarUnits18;
        private System.Windows.Forms.ComboBox collarUnits17;
        private System.Windows.Forms.ComboBox collarUnits16;
        private System.Windows.Forms.ComboBox collarUnits15;
        private System.Windows.Forms.ComboBox collarUnits14;
        private System.Windows.Forms.ComboBox collarUnits13;
        private System.Windows.Forms.ComboBox collarUnits12;
        private System.Windows.Forms.ComboBox collarUnits11;
        private System.Windows.Forms.ComboBox collarUnits10;
        private System.Windows.Forms.ComboBox collarUnits9;
        private System.Windows.Forms.ComboBox collarUnits8;
        private System.Windows.Forms.ComboBox collarUnits7;
        private System.Windows.Forms.ComboBox collarUnits6;
        private System.Windows.Forms.ComboBox collarUnits5;
        private System.Windows.Forms.ComboBox collarUnits4;
        private System.Windows.Forms.ComboBox collarUnits3;
        private System.Windows.Forms.ComboBox collarUnits2;
        private System.Windows.Forms.ComboBox collarUnits1;
        private System.Windows.Forms.TextBox collarLength20;
        private System.Windows.Forms.TextBox collarLength19;
        private System.Windows.Forms.TextBox collarLength18;
        private System.Windows.Forms.TextBox collarLength17;
        private System.Windows.Forms.TextBox collarLength16;
        private System.Windows.Forms.TextBox collarLength15;
        private System.Windows.Forms.TextBox collarLength14;
        private System.Windows.Forms.TextBox collarLength13;
        private System.Windows.Forms.TextBox collarLength12;
        private System.Windows.Forms.TextBox collarLength11;
        private System.Windows.Forms.TextBox collarLength10;
        private System.Windows.Forms.TextBox collarLength9;
        private System.Windows.Forms.TextBox collarLength8;
        private System.Windows.Forms.TextBox collarLength7;
        private System.Windows.Forms.TextBox collarLength6;
        private System.Windows.Forms.TextBox collarLength5;
        private System.Windows.Forms.TextBox collarLength4;
        private System.Windows.Forms.TextBox collarLength3;
        private System.Windows.Forms.TextBox collarLength2;
        private System.Windows.Forms.TextBox collarLength1;
        private System.Windows.Forms.ComboBox toolSizeComboBox;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Button mwdNmdcCalculateButton;
        private System.Windows.Forms.TextBox calculateNmdcLengthBox;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Button bhaLengthCalculateButton;
        private System.Windows.Forms.TextBox bhaLengthBox;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.Button reloadTablesButton;
    }
}

