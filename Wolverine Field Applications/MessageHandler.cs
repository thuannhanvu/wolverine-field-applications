﻿//--------------------------------------|---------------------------------------
//                                PC RMS V1.0.0.0
//                                File: MessageHandler.cs
//------------------------------------------------------------------------------
// Author: Thuan Vu
// Created on April 14, 2021
//--------------------------------------|---------------------------------------
//  This drawing/document is copyright and the property of
//  Wolverine OilField Technologies LLC. and Remote Measurement Systems Ltd.
//  It must not be copied (in whole or in part),
//  used for manufacture or otherwise disclosed without prior written consent.
//  Any  copies of this drawing/document made by any method must also
//  include a copy of this legend. This document is supplied  without
//  liability for errors or omissions.
//
// (c) COPYRIGHT WOLVERINE OILFIELD TECHNOLOGIES LLC. AND
//               REMOTE MEASUREMENT SYSTEMS LIMITED 2021.
//-----------------------------------------------------------------------------

using System;
using System.Threading.Tasks;

namespace PC_PDD
{
    internal class MessageHandler
    {
        public bool checkSumVerify(byte[] buffer)
        {
            if (buffer.Length == 0)
                return false;

            var sum = new byte();
            var flag = new bool();
            for (var i = 0; i < buffer.Length - 1; i++)
                if (i < buffer.Length)
                    sum = (byte)(sum + buffer[i]);
            if (sum == buffer[buffer.Length - 1])
                flag = true;
            else
                flag = false;

            return flag;
        }

        public bool checkSumVerify(byte[] buffer, int expectedBytes)
        {
            if (buffer.Length == 0)
                return false;
            if (buffer.Length == expectedBytes)
            {

                var sum = new byte();
                var flag = new bool();
                for (var i = 0; i < buffer.Length - 1; i++)
                    if (i < buffer.Length)
                        sum = (byte)(sum + buffer[i]);
                if (sum == buffer[buffer.Length - 1])
                    flag = true;
                else
                    flag = false;

                return flag;
            }
            else
                return false;
        }

        public async Task<bool> checkSumVerifyAsync(byte[] buffer)
        {
            var sum = new byte();
            var flag = new bool();

            await Task.Run(() =>
            {
                for (var i = 0; i < buffer.Length - 1; i++)
                    if (i < buffer.Length)
                        sum = (byte)(sum + buffer[i]);
            });
            if (sum == buffer[buffer.Length - 1])
                flag = true;
            else
                flag = false;

            return flag;
        }

        public ushort calculateChecksum(byte[] buffer)
        {
            var sum = new ushort();
            for (UInt16 i = 0; i < buffer.Length - 2; i++)
            {
                sum += Convert.ToUInt16(Combine(buffer[++i], buffer[i - 1]));
            }

            return sum;
        }

        public ushort calculateKTableChecksum(byte[] buffer)
        {
            var sum = new ushort();
            for (UInt16 i = 0; i < 255; i++)
            {
                sum += buffer[i]; ;
            }
            return Convert.ToUInt16(sum & 0x00FF);
        }

        public int Combine(byte b1, byte b2)
        {
            int combined = b1 << 8 | b2;
            return combined;
        }
    }
}