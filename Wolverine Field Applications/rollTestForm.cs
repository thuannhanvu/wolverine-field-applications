﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PC_PDD;
using NodeCommon;
using System.Runtime.InteropServices;

namespace Wolverine_Field_Applications
{
    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct DirectionalSavedDataSet
    {
        public float GxG;
        public float GyG;
        public float GzG;
        public float BxGauss;
        public float ByGauss;
        public float BzGauss;
        public float AzimuthDegrees;
        public float InclinationDegrees;
        public float MagneticToolFaceDegrees;
        public float GravityToolFaceDegrees;
        public float TGF;
        public float TMF;
        public float TFO;
        public float Temperature;
    }

    public partial class rollTestForm : Form
    {
        TIB_Commands tibCMD = new TIB_Commands();
        MessageHandler MH = new MessageHandler();
        deserializeStruct conv = new deserializeStruct();
        CommandProcessor cp = new CommandProcessor();
        Wolverine_Field_Applications.MainForm localNT = new Wolverine_Field_Applications.MainForm();
        ToolComms localTC = new ToolComms();
        public FTD2XX_NET.FTDI localMyFtdiDevice = new FTD2XX_NET.FTDI();
        public FTD2XX_NET.FTDI.FT_STATUS localFtStatus = FTD2XX_NET.FTDI.FT_STATUS.FT_OK;
        int stateMachineState = 0;

        StructureVariables.MWD_MASTER_DATAGROUP_1_STRUCTURE mwdDG1 = new StructureVariables.MWD_MASTER_DATAGROUP_1_STRUCTURE();

        DirectionalSavedDataSet west45Data = new DirectionalSavedDataSet();
        DirectionalSavedDataSet west90Data = new DirectionalSavedDataSet();
        DirectionalSavedDataSet west135Data = new DirectionalSavedDataSet();
        DirectionalSavedDataSet west180Data = new DirectionalSavedDataSet();
        DirectionalSavedDataSet west225Data = new DirectionalSavedDataSet();
        DirectionalSavedDataSet west270Data = new DirectionalSavedDataSet();
        DirectionalSavedDataSet west315Data = new DirectionalSavedDataSet();
        DirectionalSavedDataSet west360Data = new DirectionalSavedDataSet();

        DirectionalSavedDataSet east45Data = new DirectionalSavedDataSet();
        DirectionalSavedDataSet east90Data = new DirectionalSavedDataSet();
        DirectionalSavedDataSet east135Data = new DirectionalSavedDataSet();
        DirectionalSavedDataSet east180Data = new DirectionalSavedDataSet();
        DirectionalSavedDataSet east225Data = new DirectionalSavedDataSet();
        DirectionalSavedDataSet east270Data = new DirectionalSavedDataSet();
        DirectionalSavedDataSet east315Data = new DirectionalSavedDataSet();
        DirectionalSavedDataSet east360Data = new DirectionalSavedDataSet();

        DirectionalSavedDataSet vertical0Data = new DirectionalSavedDataSet();
        DirectionalSavedDataSet vertical180Data = new DirectionalSavedDataSet();

        public rollTestForm(Wolverine_Field_Applications.MainForm nT, ToolComms tc, FTD2XX_NET.FTDI myFtdiDevice, FTD2XX_NET.FTDI.FT_STATUS ftStatus)
        {
            InitializeComponent();

            localNT = nT;
            localTC = tc;
            localMyFtdiDevice = myFtdiDevice;
            localFtStatus = ftStatus;

            FormClosing += Form_Closing;
        }

        private void Form_Closing(object sender, FormClosingEventArgs e)
        {
            westRoll45Test.Text = "START";
            westRoll90Test.Text = "START";
            westRoll135Test.Text = "START";
            westRoll180Test.Text = "START";
            westRoll225Test.Text = "START";
            westRoll270Test.Text = "START";
            westRoll315Test.Text = "START";
            westRoll360Test.Text = "START";

            eastRoll45Test.Text = "START";
            eastRoll90Test.Text = "START";
            eastRoll135Test.Text = "START";
            eastRoll180Test.Text = "START";
            eastRoll225Test.Text = "START";
            eastRoll270Test.Text = "START";
            eastRoll315Test.Text = "START";
            eastRoll360Test.Text = "START";

            verticalRoll45Test.Text = "START";
            verticalRoll180Test.Text = "START";
        }

        public void setDataGroup1(StructureVariables.MWD_MASTER_DATAGROUP_1_STRUCTURE dg1)
        {
            mwdDG1 = dg1;
        }

        public void progressStateMachine()
        {
            stateMachineState++;
        }

        public void resetStateMachine()
        {
            stateMachineState = 0;
        }

        public async void RunStateMachine()
        {
            switch(stateMachineState)
            {
                case 0:
                    {
                        statusBox.Text = "ROLL POSITION 1 OF 18: WEST ROLL 45°";

                        westGxg45Box.BackColor = Color.Empty;
                        westGyg45Box.BackColor = Color.Empty;
                        westGzg45Box.BackColor = Color.Empty;
                        westBxGauss45Box.BackColor = Color.Empty;
                        westByGauss45Box.BackColor = Color.Empty;
                        westBzGauss45Box.BackColor = Color.Empty;

                        await Task.Delay(500);

                        westGxg45Box.Text = String.Format("{0:0.0000}", mwdDG1.GxG);
                        westGyg45Box.Text = String.Format("{0:0.0000}", mwdDG1.GyG);
                        westGzg45Box.Text = String.Format("{0:0.0000}", mwdDG1.GzG);

                        westBxGauss45Box.Text = String.Format("{0:0.0000}", mwdDG1.BxGauss);
                        westByGauss45Box.Text = String.Format("{0:0.0000}", mwdDG1.ByGauss);
                        westBzGauss45Box.Text = String.Format("{0:0.0000}", mwdDG1.BzGauss);


                        westGxg45Box.BackColor = Color.Yellow;
                        westGyg45Box.BackColor = Color.Yellow;
                        westGzg45Box.BackColor = Color.Yellow;
                        westBxGauss45Box.BackColor = Color.Yellow;
                        westByGauss45Box.BackColor = Color.Yellow;
                        westBzGauss45Box.BackColor = Color.Yellow;

                        west45Data.GxG = mwdDG1.GxG;
                        west45Data.GyG = mwdDG1.GyG;
                        west45Data.GzG = mwdDG1.GzG;
                        west45Data.BxGauss = mwdDG1.BxGauss;
                        west45Data.ByGauss = mwdDG1.ByGauss;
                        west45Data.BzGauss = mwdDG1.BzGauss;
                        west45Data.AzimuthDegrees = mwdDG1.AzimuthDegrees;
                        west45Data.InclinationDegrees = mwdDG1.InclinationDegrees;
                        west45Data.GravityToolFaceDegrees = mwdDG1.GravityToolFaceDegrees;
                        west45Data.MagneticToolFaceDegrees = mwdDG1.MagneticToolFaceDegrees;
                        west45Data.TGF = mwdDG1.TotalGravityFieldG;
                        west45Data.TMF = mwdDG1.TotalMagneticFieldGauss;
                        west45Data.TFO = mwdDG1.ToolfaceOffsetDegrees;
                        west45Data.Temperature = mwdDG1.TemperatureCelsius;
                    }
                    break;
                case 1:
                    {
                        statusBox.Text = "ROLL POSITION 2 OF 18: WEST ROLL 90°";

                        westGxg90Box.BackColor = Color.Empty;
                        westGyg90Box.BackColor = Color.Empty;
                        westGzg90Box.BackColor = Color.Empty;
                        westBxGauss90Box.BackColor = Color.Empty;
                        westByGauss90Box.BackColor = Color.Empty;
                        westBzGauss90Box.BackColor = Color.Empty;

                        await Task.Delay(500);

                        westGxg90Box.Text = String.Format("{0:0.0000}", mwdDG1.GxG);
                        westGyg90Box.Text = String.Format("{0:0.0000}", mwdDG1.GyG);
                        westGzg90Box.Text = String.Format("{0:0.0000}", mwdDG1.GzG);

                        westBxGauss90Box.Text = String.Format("{0:0.0000}", mwdDG1.BxGauss);
                        westByGauss90Box.Text = String.Format("{0:0.0000}", mwdDG1.ByGauss);
                        westBzGauss90Box.Text = String.Format("{0:0.0000}", mwdDG1.BzGauss);


                        westGxg90Box.BackColor = Color.Yellow;
                        westGyg90Box.BackColor = Color.Yellow;
                        westGzg90Box.BackColor = Color.Yellow;
                        westBxGauss90Box.BackColor = Color.Yellow;
                        westByGauss90Box.BackColor = Color.Yellow;
                        westBzGauss90Box.BackColor = Color.Yellow;

                        west90Data.GxG = mwdDG1.GxG;
                        west90Data.GyG = mwdDG1.GyG;
                        west90Data.GzG = mwdDG1.GzG;
                        west90Data.BxGauss = mwdDG1.BxGauss;
                        west90Data.ByGauss = mwdDG1.ByGauss;
                        west90Data.BzGauss = mwdDG1.BzGauss;
                        west90Data.AzimuthDegrees = mwdDG1.AzimuthDegrees;
                        west90Data.InclinationDegrees = mwdDG1.InclinationDegrees;
                        west90Data.GravityToolFaceDegrees = mwdDG1.GravityToolFaceDegrees;
                        west90Data.MagneticToolFaceDegrees = mwdDG1.MagneticToolFaceDegrees;
                        west90Data.TGF = mwdDG1.TotalGravityFieldG;
                        west90Data.TMF = mwdDG1.TotalMagneticFieldGauss;
                        west90Data.TFO = mwdDG1.ToolfaceOffsetDegrees;
                        west90Data.Temperature = mwdDG1.TemperatureCelsius;
                    }
                    break;
                case 2:
                    {
                        statusBox.Text = "ROLL POSITION 3 OF 18: WEST ROLL 135°";

                        westGxg135Box.BackColor = Color.Empty;
                        westGyg135Box.BackColor = Color.Empty;
                        westGzg135Box.BackColor = Color.Empty;
                        westBxGauss135Box.BackColor = Color.Empty;
                        westByGauss135Box.BackColor = Color.Empty;
                        westBzGauss135Box.BackColor = Color.Empty;

                        await Task.Delay(500);

                        westGxg135Box.Text = String.Format("{0:0.0000}", mwdDG1.GxG);
                        westGyg135Box.Text = String.Format("{0:0.0000}", mwdDG1.GyG);
                        westGzg135Box.Text = String.Format("{0:0.0000}", mwdDG1.GzG);

                        westBxGauss135Box.Text = String.Format("{0:0.0000}", mwdDG1.BxGauss);
                        westByGauss135Box.Text = String.Format("{0:0.0000}", mwdDG1.ByGauss);
                        westBzGauss135Box.Text = String.Format("{0:0.0000}", mwdDG1.BzGauss);


                        westGxg135Box.BackColor = Color.Yellow;
                        westGyg135Box.BackColor = Color.Yellow;
                        westGzg135Box.BackColor = Color.Yellow;
                        westBxGauss135Box.BackColor = Color.Yellow;
                        westByGauss135Box.BackColor = Color.Yellow;
                        westBzGauss135Box.BackColor = Color.Yellow;

                        west135Data.GxG = mwdDG1.GxG;
                        west135Data.GyG = mwdDG1.GyG;
                        west135Data.GzG = mwdDG1.GzG;
                        west135Data.BxGauss = mwdDG1.BxGauss;
                        west135Data.ByGauss = mwdDG1.ByGauss;
                        west135Data.BzGauss = mwdDG1.BzGauss;
                        west135Data.AzimuthDegrees = mwdDG1.AzimuthDegrees;
                        west135Data.InclinationDegrees = mwdDG1.InclinationDegrees;
                        west135Data.GravityToolFaceDegrees = mwdDG1.GravityToolFaceDegrees;
                        west135Data.MagneticToolFaceDegrees = mwdDG1.MagneticToolFaceDegrees;
                        west135Data.TGF = mwdDG1.TotalGravityFieldG;
                        west135Data.TMF = mwdDG1.TotalMagneticFieldGauss;
                        west135Data.TFO = mwdDG1.ToolfaceOffsetDegrees;
                        west135Data.Temperature = mwdDG1.TemperatureCelsius;
                    }
                    break;
                case 3:
                    {
                        statusBox.Text = "ROLL POSITION 4 OF 18: WEST ROLL 180°";

                        westGxg180Box.BackColor = Color.Empty;
                        westGyg180Box.BackColor = Color.Empty;
                        westGzg180Box.BackColor = Color.Empty;
                        westBxGauss180Box.BackColor = Color.Empty;
                        westByGauss180Box.BackColor = Color.Empty;
                        westBzGauss180Box.BackColor = Color.Empty;

                        await Task.Delay(500);

                        westGxg180Box.Text = String.Format("{0:0.0000}", mwdDG1.GxG);
                        westGyg180Box.Text = String.Format("{0:0.0000}", mwdDG1.GyG);
                        westGzg180Box.Text = String.Format("{0:0.0000}", mwdDG1.GzG);

                        westBxGauss180Box.Text = String.Format("{0:0.0000}", mwdDG1.BxGauss);
                        westByGauss180Box.Text = String.Format("{0:0.0000}", mwdDG1.ByGauss);
                        westBzGauss180Box.Text = String.Format("{0:0.0000}", mwdDG1.BzGauss);


                        westGxg180Box.BackColor = Color.Yellow;
                        westGyg180Box.BackColor = Color.Yellow;
                        westGzg180Box.BackColor = Color.Yellow;
                        westBxGauss180Box.BackColor = Color.Yellow;
                        westByGauss180Box.BackColor = Color.Yellow;
                        westBzGauss180Box.BackColor = Color.Yellow;

                        west180Data.GxG = mwdDG1.GxG;
                        west180Data.GyG = mwdDG1.GyG;
                        west180Data.GzG = mwdDG1.GzG;
                        west180Data.BxGauss = mwdDG1.BxGauss;
                        west180Data.ByGauss = mwdDG1.ByGauss;
                        west180Data.BzGauss = mwdDG1.BzGauss;
                        west180Data.AzimuthDegrees = mwdDG1.AzimuthDegrees;
                        west180Data.InclinationDegrees = mwdDG1.InclinationDegrees;
                        west180Data.GravityToolFaceDegrees = mwdDG1.GravityToolFaceDegrees;
                        west180Data.MagneticToolFaceDegrees = mwdDG1.MagneticToolFaceDegrees;
                        west180Data.TGF = mwdDG1.TotalGravityFieldG;
                        west180Data.TMF = mwdDG1.TotalMagneticFieldGauss;
                        west180Data.TFO = mwdDG1.ToolfaceOffsetDegrees;
                        west180Data.Temperature = mwdDG1.TemperatureCelsius;
                    }
                    break;
                case 4:
                    {
                        statusBox.Text = "ROLL POSITION 5 OF 18: WEST ROLL 225°";

                        westGxg225Box.BackColor = Color.Empty;
                        westGyg225Box.BackColor = Color.Empty;
                        westGzg225Box.BackColor = Color.Empty;
                        westBxGauss225Box.BackColor = Color.Empty;
                        westByGauss225Box.BackColor = Color.Empty;
                        westBzGauss225Box.BackColor = Color.Empty;

                        await Task.Delay(500);

                        westGxg225Box.Text = String.Format("{0:0.0000}", mwdDG1.GxG);
                        westGyg225Box.Text = String.Format("{0:0.0000}", mwdDG1.GyG);
                        westGzg225Box.Text = String.Format("{0:0.0000}", mwdDG1.GzG);

                        westBxGauss225Box.Text = String.Format("{0:0.0000}", mwdDG1.BxGauss);
                        westByGauss225Box.Text = String.Format("{0:0.0000}", mwdDG1.ByGauss);
                        westBzGauss225Box.Text = String.Format("{0:0.0000}", mwdDG1.BzGauss);


                        westGxg225Box.BackColor = Color.Yellow;
                        westGyg225Box.BackColor = Color.Yellow;
                        westGzg225Box.BackColor = Color.Yellow;
                        westBxGauss225Box.BackColor = Color.Yellow;
                        westByGauss225Box.BackColor = Color.Yellow;
                        westBzGauss225Box.BackColor = Color.Yellow;

                        west225Data.GxG = mwdDG1.GxG;
                        west225Data.GyG = mwdDG1.GyG;
                        west225Data.GzG = mwdDG1.GzG;
                        west225Data.BxGauss = mwdDG1.BxGauss;
                        west225Data.ByGauss = mwdDG1.ByGauss;
                        west225Data.BzGauss = mwdDG1.BzGauss;
                        west225Data.AzimuthDegrees = mwdDG1.AzimuthDegrees;
                        west225Data.InclinationDegrees = mwdDG1.InclinationDegrees;
                        west225Data.GravityToolFaceDegrees = mwdDG1.GravityToolFaceDegrees;
                        west225Data.MagneticToolFaceDegrees = mwdDG1.MagneticToolFaceDegrees;
                        west225Data.TGF = mwdDG1.TotalGravityFieldG;
                        west225Data.TMF = mwdDG1.TotalMagneticFieldGauss;
                        west225Data.TFO = mwdDG1.ToolfaceOffsetDegrees;
                        west225Data.Temperature = mwdDG1.TemperatureCelsius;
                    }
                    break;
                case 5:
                    {
                        statusBox.Text = "ROLL POSITION 6 OF 18: WEST ROLL 270°";

                        westGxg270Box.BackColor = Color.Empty;
                        westGyg270Box.BackColor = Color.Empty;
                        westGzg270Box.BackColor = Color.Empty;
                        westBxGauss270Box.BackColor = Color.Empty;
                        westByGauss270Box.BackColor = Color.Empty;
                        westBzGauss270Box.BackColor = Color.Empty;

                        await Task.Delay(500);

                        westGxg270Box.Text = String.Format("{0:0.0000}", mwdDG1.GxG);
                        westGyg270Box.Text = String.Format("{0:0.0000}", mwdDG1.GyG);
                        westGzg270Box.Text = String.Format("{0:0.0000}", mwdDG1.GzG);

                        westBxGauss270Box.Text = String.Format("{0:0.0000}", mwdDG1.BxGauss);
                        westByGauss270Box.Text = String.Format("{0:0.0000}", mwdDG1.ByGauss);
                        westBzGauss270Box.Text = String.Format("{0:0.0000}", mwdDG1.BzGauss);


                        westGxg270Box.BackColor = Color.Yellow;
                        westGyg270Box.BackColor = Color.Yellow;
                        westGzg270Box.BackColor = Color.Yellow;
                        westBxGauss270Box.BackColor = Color.Yellow;
                        westByGauss270Box.BackColor = Color.Yellow;
                        westBzGauss270Box.BackColor = Color.Yellow;

                        west270Data.GxG = mwdDG1.GxG;
                        west270Data.GyG = mwdDG1.GyG;
                        west270Data.GzG = mwdDG1.GzG;
                        west270Data.BxGauss = mwdDG1.BxGauss;
                        west270Data.ByGauss = mwdDG1.ByGauss;
                        west270Data.BzGauss = mwdDG1.BzGauss;
                        west270Data.AzimuthDegrees = mwdDG1.AzimuthDegrees;
                        west270Data.InclinationDegrees = mwdDG1.InclinationDegrees;
                        west270Data.GravityToolFaceDegrees = mwdDG1.GravityToolFaceDegrees;
                        west270Data.MagneticToolFaceDegrees = mwdDG1.MagneticToolFaceDegrees;
                        west270Data.TGF = mwdDG1.TotalGravityFieldG;
                        west270Data.TMF = mwdDG1.TotalMagneticFieldGauss;
                        west270Data.TFO = mwdDG1.ToolfaceOffsetDegrees;
                        west270Data.Temperature = mwdDG1.TemperatureCelsius;
                    }
                    break;
                case 6:
                    {
                        statusBox.Text = "ROLL POSITION 7 OF 18: WEST ROLL 315°";

                        westGxg315Box.BackColor = Color.Empty;
                        westGyg315Box.BackColor = Color.Empty;
                        westGzg315Box.BackColor = Color.Empty;
                        westBxGauss315Box.BackColor = Color.Empty;
                        westByGauss315Box.BackColor = Color.Empty;
                        westBzGauss315Box.BackColor = Color.Empty;

                        await Task.Delay(500);

                        westGxg315Box.Text = String.Format("{0:0.0000}", mwdDG1.GxG);
                        westGyg315Box.Text = String.Format("{0:0.0000}", mwdDG1.GyG);
                        westGzg315Box.Text = String.Format("{0:0.0000}", mwdDG1.GzG);

                        westBxGauss315Box.Text = String.Format("{0:0.0000}", mwdDG1.BxGauss);
                        westByGauss315Box.Text = String.Format("{0:0.0000}", mwdDG1.ByGauss);
                        westBzGauss315Box.Text = String.Format("{0:0.0000}", mwdDG1.BzGauss);


                        westGxg315Box.BackColor = Color.Yellow;
                        westGyg315Box.BackColor = Color.Yellow;
                        westGzg315Box.BackColor = Color.Yellow;
                        westBxGauss315Box.BackColor = Color.Yellow;
                        westByGauss315Box.BackColor = Color.Yellow;
                        westBzGauss315Box.BackColor = Color.Yellow;

                        west315Data.GxG = mwdDG1.GxG;
                        west315Data.GyG = mwdDG1.GyG;
                        west315Data.GzG = mwdDG1.GzG;
                        west315Data.BxGauss = mwdDG1.BxGauss;
                        west315Data.ByGauss = mwdDG1.ByGauss;
                        west315Data.BzGauss = mwdDG1.BzGauss;
                        west315Data.AzimuthDegrees = mwdDG1.AzimuthDegrees;
                        west315Data.InclinationDegrees = mwdDG1.InclinationDegrees;
                        west315Data.GravityToolFaceDegrees = mwdDG1.GravityToolFaceDegrees;
                        west315Data.MagneticToolFaceDegrees = mwdDG1.MagneticToolFaceDegrees;
                        west315Data.TGF = mwdDG1.TotalGravityFieldG;
                        west315Data.TMF = mwdDG1.TotalMagneticFieldGauss;
                        west315Data.TFO = mwdDG1.ToolfaceOffsetDegrees;
                        west315Data.Temperature = mwdDG1.TemperatureCelsius;
                    }
                    break;
                case 7:
                    {
                        statusBox.Text = "ROLL POSITION 8 OF 18: WEST ROLL 360°";

                        westGxg360Box.BackColor = Color.Empty;
                        westGyg360Box.BackColor = Color.Empty;
                        westGzg360Box.BackColor = Color.Empty;
                        westBxGauss360Box.BackColor = Color.Empty;
                        westByGauss360Box.BackColor = Color.Empty;
                        westBzGauss360Box.BackColor = Color.Empty;

                        await Task.Delay(500);

                        westGxg360Box.Text = String.Format("{0:0.0000}", mwdDG1.GxG);
                        westGyg360Box.Text = String.Format("{0:0.0000}", mwdDG1.GyG);
                        westGzg360Box.Text = String.Format("{0:0.0000}", mwdDG1.GzG);

                        westBxGauss360Box.Text = String.Format("{0:0.0000}", mwdDG1.BxGauss);
                        westByGauss360Box.Text = String.Format("{0:0.0000}", mwdDG1.ByGauss);
                        westBzGauss360Box.Text = String.Format("{0:0.0000}", mwdDG1.BzGauss);


                        westGxg360Box.BackColor = Color.Yellow;
                        westGyg360Box.BackColor = Color.Yellow;
                        westGzg360Box.BackColor = Color.Yellow;
                        westBxGauss360Box.BackColor = Color.Yellow;
                        westByGauss360Box.BackColor = Color.Yellow;
                        westBzGauss360Box.BackColor = Color.Yellow;

                        west360Data.GxG = mwdDG1.GxG;
                        west360Data.GyG = mwdDG1.GyG;
                        west360Data.GzG = mwdDG1.GzG;
                        west360Data.BxGauss = mwdDG1.BxGauss;
                        west360Data.ByGauss = mwdDG1.ByGauss;
                        west360Data.BzGauss = mwdDG1.BzGauss;
                        west360Data.AzimuthDegrees = mwdDG1.AzimuthDegrees;
                        west360Data.InclinationDegrees = mwdDG1.InclinationDegrees;
                        west360Data.GravityToolFaceDegrees = mwdDG1.GravityToolFaceDegrees;
                        west360Data.MagneticToolFaceDegrees = mwdDG1.MagneticToolFaceDegrees;
                        west360Data.TGF = mwdDG1.TotalGravityFieldG;
                        west360Data.TMF = mwdDG1.TotalMagneticFieldGauss;
                        west360Data.TFO = mwdDG1.ToolfaceOffsetDegrees;
                        west360Data.Temperature = mwdDG1.TemperatureCelsius;
                    }
                    break;
                case 8:
                    {
                        statusBox.Text = "ROLL POSITION 9 OF 18: EAST ROLL 45°";

                        eastGxg45Box.BackColor = Color.Empty;
                        eastGyg45Box.BackColor = Color.Empty;
                        eastGzg45Box.BackColor = Color.Empty;
                        eastBxGauss45Box.BackColor = Color.Empty;
                        eastByGauss45Box.BackColor = Color.Empty;
                        eastBzGauss45Box.BackColor = Color.Empty;

                        await Task.Delay(500);

                        eastGxg45Box.Text = String.Format("{0:0.0000}", mwdDG1.GxG);
                        eastGyg45Box.Text = String.Format("{0:0.0000}", mwdDG1.GyG);
                        eastGzg45Box.Text = String.Format("{0:0.0000}", mwdDG1.GzG);

                        eastBxGauss45Box.Text = String.Format("{0:0.0000}", mwdDG1.BxGauss);
                        eastByGauss45Box.Text = String.Format("{0:0.0000}", mwdDG1.ByGauss);
                        eastBzGauss45Box.Text = String.Format("{0:0.0000}", mwdDG1.BzGauss);


                        eastGxg45Box.BackColor = Color.Yellow;
                        eastGyg45Box.BackColor = Color.Yellow;
                        eastGzg45Box.BackColor = Color.Yellow;
                        eastBxGauss45Box.BackColor = Color.Yellow;
                        eastByGauss45Box.BackColor = Color.Yellow;
                        eastBzGauss45Box.BackColor = Color.Yellow;

                        east45Data.GxG = mwdDG1.GxG;
                        east45Data.GyG = mwdDG1.GyG;
                        east45Data.GzG = mwdDG1.GzG;
                        east45Data.BxGauss = mwdDG1.BxGauss;
                        east45Data.ByGauss = mwdDG1.ByGauss;
                        east45Data.BzGauss = mwdDG1.BzGauss;
                        east45Data.AzimuthDegrees = mwdDG1.AzimuthDegrees;
                        east45Data.InclinationDegrees = mwdDG1.InclinationDegrees;
                        east45Data.GravityToolFaceDegrees = mwdDG1.GravityToolFaceDegrees;
                        east45Data.MagneticToolFaceDegrees = mwdDG1.MagneticToolFaceDegrees;
                        east45Data.TGF = mwdDG1.TotalGravityFieldG;
                        east45Data.TMF = mwdDG1.TotalMagneticFieldGauss;
                        east45Data.TFO = mwdDG1.ToolfaceOffsetDegrees;
                        east45Data.Temperature = mwdDG1.TemperatureCelsius;
                    }
                    break;
                case 9:
                    {
                        statusBox.Text = "ROLL POSITION 10 OF 18: EAST ROLL 90°";

                        eastGxg90Box.BackColor = Color.Empty;
                        eastGyg90Box.BackColor = Color.Empty;
                        eastGzg90Box.BackColor = Color.Empty;
                        eastBxGauss90Box.BackColor = Color.Empty;
                        eastByGauss90Box.BackColor = Color.Empty;
                        eastBzGauss90Box.BackColor = Color.Empty;

                        await Task.Delay(500);

                        eastGxg90Box.Text = String.Format("{0:0.0000}", mwdDG1.GxG);
                        eastGyg90Box.Text = String.Format("{0:0.0000}", mwdDG1.GyG);
                        eastGzg90Box.Text = String.Format("{0:0.0000}", mwdDG1.GzG);

                        eastBxGauss90Box.Text = String.Format("{0:0.0000}", mwdDG1.BxGauss);
                        eastByGauss90Box.Text = String.Format("{0:0.0000}", mwdDG1.ByGauss);
                        eastBzGauss90Box.Text = String.Format("{0:0.0000}", mwdDG1.BzGauss);


                        eastGxg90Box.BackColor = Color.Yellow;
                        eastGyg90Box.BackColor = Color.Yellow;
                        eastGzg90Box.BackColor = Color.Yellow;
                        eastBxGauss90Box.BackColor = Color.Yellow;
                        eastByGauss90Box.BackColor = Color.Yellow;
                        eastBzGauss90Box.BackColor = Color.Yellow;

                        east90Data.GxG = mwdDG1.GxG;
                        east90Data.GyG = mwdDG1.GyG;
                        east90Data.GzG = mwdDG1.GzG;
                        east90Data.BxGauss = mwdDG1.BxGauss;
                        east90Data.ByGauss = mwdDG1.ByGauss;
                        east90Data.BzGauss = mwdDG1.BzGauss;
                        east90Data.AzimuthDegrees = mwdDG1.AzimuthDegrees;
                        east90Data.InclinationDegrees = mwdDG1.InclinationDegrees;
                        east90Data.GravityToolFaceDegrees = mwdDG1.GravityToolFaceDegrees;
                        east90Data.MagneticToolFaceDegrees = mwdDG1.MagneticToolFaceDegrees;
                        east90Data.TGF = mwdDG1.TotalGravityFieldG;
                        east90Data.TMF = mwdDG1.TotalMagneticFieldGauss;
                        east90Data.TFO = mwdDG1.ToolfaceOffsetDegrees;
                        east90Data.Temperature = mwdDG1.TemperatureCelsius;
                    }
                    break;
                case 10:
                    {
                        statusBox.Text = "ROLL POSITION 11 OF 18: EAST ROLL 135°";

                        eastGxg135Box.BackColor = Color.Empty;
                        eastGyg135Box.BackColor = Color.Empty;
                        eastGzg135Box.BackColor = Color.Empty;
                        eastBxGauss135Box.BackColor = Color.Empty;
                        eastByGauss135Box.BackColor = Color.Empty;
                        eastBzGauss135Box.BackColor = Color.Empty;

                        await Task.Delay(500);

                        eastGxg135Box.Text = String.Format("{0:0.0000}", mwdDG1.GxG);
                        eastGyg135Box.Text = String.Format("{0:0.0000}", mwdDG1.GyG);
                        eastGzg135Box.Text = String.Format("{0:0.0000}", mwdDG1.GzG);

                        eastBxGauss135Box.Text = String.Format("{0:0.0000}", mwdDG1.BxGauss);
                        eastByGauss135Box.Text = String.Format("{0:0.0000}", mwdDG1.ByGauss);
                        eastBzGauss135Box.Text = String.Format("{0:0.0000}", mwdDG1.BzGauss);


                        eastGxg135Box.BackColor = Color.Yellow;
                        eastGyg135Box.BackColor = Color.Yellow;
                        eastGzg135Box.BackColor = Color.Yellow;
                        eastBxGauss135Box.BackColor = Color.Yellow;
                        eastByGauss135Box.BackColor = Color.Yellow;
                        eastBzGauss135Box.BackColor = Color.Yellow;

                        east135Data.GxG = mwdDG1.GxG;
                        east135Data.GyG = mwdDG1.GyG;
                        east135Data.GzG = mwdDG1.GzG;
                        east135Data.BxGauss = mwdDG1.BxGauss;
                        east135Data.ByGauss = mwdDG1.ByGauss;
                        east135Data.BzGauss = mwdDG1.BzGauss;
                        east135Data.AzimuthDegrees = mwdDG1.AzimuthDegrees;
                        east135Data.InclinationDegrees = mwdDG1.InclinationDegrees;
                        east135Data.GravityToolFaceDegrees = mwdDG1.GravityToolFaceDegrees;
                        east135Data.MagneticToolFaceDegrees = mwdDG1.MagneticToolFaceDegrees;
                        east135Data.TGF = mwdDG1.TotalGravityFieldG;
                        east135Data.TMF = mwdDG1.TotalMagneticFieldGauss;
                        east135Data.TFO = mwdDG1.ToolfaceOffsetDegrees;
                        east135Data.Temperature = mwdDG1.TemperatureCelsius;
                    }
                    break;
                case 11:
                    {
                        statusBox.Text = "ROLL POSITION 12 OF 18: EAST ROLL 180°";

                        eastGxg180Box.BackColor = Color.Empty;
                        eastGyg180Box.BackColor = Color.Empty;
                        eastGzg180Box.BackColor = Color.Empty;
                        eastBxGauss180Box.BackColor = Color.Empty;
                        eastByGauss180Box.BackColor = Color.Empty;
                        eastBzGauss180Box.BackColor = Color.Empty;

                        await Task.Delay(500);

                        eastGxg180Box.Text = String.Format("{0:0.0000}", mwdDG1.GxG);
                        eastGyg180Box.Text = String.Format("{0:0.0000}", mwdDG1.GyG);
                        eastGzg180Box.Text = String.Format("{0:0.0000}", mwdDG1.GzG);

                        eastBxGauss180Box.Text = String.Format("{0:0.0000}", mwdDG1.BxGauss);
                        eastByGauss180Box.Text = String.Format("{0:0.0000}", mwdDG1.ByGauss);
                        eastBzGauss180Box.Text = String.Format("{0:0.0000}", mwdDG1.BzGauss);


                        eastGxg180Box.BackColor = Color.Yellow;
                        eastGyg180Box.BackColor = Color.Yellow;
                        eastGzg180Box.BackColor = Color.Yellow;
                        eastBxGauss180Box.BackColor = Color.Yellow;
                        eastByGauss180Box.BackColor = Color.Yellow;
                        eastBzGauss180Box.BackColor = Color.Yellow;

                        east180Data.GxG = mwdDG1.GxG;
                        east180Data.GyG = mwdDG1.GyG;
                        east180Data.GzG = mwdDG1.GzG;
                        east180Data.BxGauss = mwdDG1.BxGauss;
                        east180Data.ByGauss = mwdDG1.ByGauss;
                        east180Data.BzGauss = mwdDG1.BzGauss;
                        east180Data.AzimuthDegrees = mwdDG1.AzimuthDegrees;
                        east180Data.InclinationDegrees = mwdDG1.InclinationDegrees;
                        east180Data.GravityToolFaceDegrees = mwdDG1.GravityToolFaceDegrees;
                        east180Data.MagneticToolFaceDegrees = mwdDG1.MagneticToolFaceDegrees;
                        east180Data.TGF = mwdDG1.TotalGravityFieldG;
                        east180Data.TMF = mwdDG1.TotalMagneticFieldGauss;
                        east180Data.TFO = mwdDG1.ToolfaceOffsetDegrees;
                        east180Data.Temperature = mwdDG1.TemperatureCelsius;
                    }
                    break;
                case 12:
                    {
                        statusBox.Text = "ROLL POSITION 13 OF 18: EAST ROLL 225°";

                        eastGxg225Box.BackColor = Color.Empty;
                        eastGyg225Box.BackColor = Color.Empty;
                        eastGzg225Box.BackColor = Color.Empty;
                        eastBxGauss225Box.BackColor = Color.Empty;
                        eastByGauss225Box.BackColor = Color.Empty;
                        eastBzGauss225Box.BackColor = Color.Empty;

                        await Task.Delay(500);

                        eastGxg225Box.Text = String.Format("{0:0.0000}", mwdDG1.GxG);
                        eastGyg225Box.Text = String.Format("{0:0.0000}", mwdDG1.GyG);
                        eastGzg225Box.Text = String.Format("{0:0.0000}", mwdDG1.GzG);

                        eastBxGauss225Box.Text = String.Format("{0:0.0000}", mwdDG1.BxGauss);
                        eastByGauss225Box.Text = String.Format("{0:0.0000}", mwdDG1.ByGauss);
                        eastBzGauss225Box.Text = String.Format("{0:0.0000}", mwdDG1.BzGauss);


                        eastGxg225Box.BackColor = Color.Yellow;
                        eastGyg225Box.BackColor = Color.Yellow;
                        eastGzg225Box.BackColor = Color.Yellow;
                        eastBxGauss225Box.BackColor = Color.Yellow;
                        eastByGauss225Box.BackColor = Color.Yellow;
                        eastBzGauss225Box.BackColor = Color.Yellow;

                        east225Data.GxG = mwdDG1.GxG;
                        east225Data.GyG = mwdDG1.GyG;
                        east225Data.GzG = mwdDG1.GzG;
                        east225Data.BxGauss = mwdDG1.BxGauss;
                        east225Data.ByGauss = mwdDG1.ByGauss;
                        east225Data.BzGauss = mwdDG1.BzGauss;
                        east225Data.AzimuthDegrees = mwdDG1.AzimuthDegrees;
                        east225Data.InclinationDegrees = mwdDG1.InclinationDegrees;
                        east225Data.GravityToolFaceDegrees = mwdDG1.GravityToolFaceDegrees;
                        east225Data.MagneticToolFaceDegrees = mwdDG1.MagneticToolFaceDegrees;
                        east225Data.TGF = mwdDG1.TotalGravityFieldG;
                        east225Data.TMF = mwdDG1.TotalMagneticFieldGauss;
                        east225Data.TFO = mwdDG1.ToolfaceOffsetDegrees;
                        east225Data.Temperature = mwdDG1.TemperatureCelsius;
                    }
                    break;
                case 13:
                    {
                        statusBox.Text = "ROLL POSITION 14 OF 18: EAST ROLL 270°";

                        eastGxg270Box.BackColor = Color.Empty;
                        eastGyg270Box.BackColor = Color.Empty;
                        eastGzg270Box.BackColor = Color.Empty;
                        eastBxGauss270Box.BackColor = Color.Empty;
                        eastByGauss270Box.BackColor = Color.Empty;
                        eastBzGauss270Box.BackColor = Color.Empty;

                        await Task.Delay(500);

                        eastGxg270Box.Text = String.Format("{0:0.0000}", mwdDG1.GxG);
                        eastGyg270Box.Text = String.Format("{0:0.0000}", mwdDG1.GyG);
                        eastGzg270Box.Text = String.Format("{0:0.0000}", mwdDG1.GzG);

                        eastBxGauss270Box.Text = String.Format("{0:0.0000}", mwdDG1.BxGauss);
                        eastByGauss270Box.Text = String.Format("{0:0.0000}", mwdDG1.ByGauss);
                        eastBzGauss270Box.Text = String.Format("{0:0.0000}", mwdDG1.BzGauss);


                        eastGxg270Box.BackColor = Color.Yellow;
                        eastGyg270Box.BackColor = Color.Yellow;
                        eastGzg270Box.BackColor = Color.Yellow;
                        eastBxGauss270Box.BackColor = Color.Yellow;
                        eastByGauss270Box.BackColor = Color.Yellow;
                        eastBzGauss270Box.BackColor = Color.Yellow;

                        east270Data.GxG = mwdDG1.GxG;
                        east270Data.GyG = mwdDG1.GyG;
                        east270Data.GzG = mwdDG1.GzG;
                        east270Data.BxGauss = mwdDG1.BxGauss;
                        east270Data.ByGauss = mwdDG1.ByGauss;
                        east270Data.BzGauss = mwdDG1.BzGauss;
                        east270Data.AzimuthDegrees = mwdDG1.AzimuthDegrees;
                        east270Data.InclinationDegrees = mwdDG1.InclinationDegrees;
                        east270Data.GravityToolFaceDegrees = mwdDG1.GravityToolFaceDegrees;
                        east270Data.MagneticToolFaceDegrees = mwdDG1.MagneticToolFaceDegrees;
                        east270Data.TGF = mwdDG1.TotalGravityFieldG;
                        east270Data.TMF = mwdDG1.TotalMagneticFieldGauss;
                        east270Data.TFO = mwdDG1.ToolfaceOffsetDegrees;
                        east270Data.Temperature = mwdDG1.TemperatureCelsius;
                    }
                    break;
                case 14:
                    {
                        statusBox.Text = "ROLL POSITION 15 OF 18: EAST ROLL 315°";

                        eastGxg315Box.BackColor = Color.Empty;
                        eastGyg315Box.BackColor = Color.Empty;
                        eastGzg315Box.BackColor = Color.Empty;
                        eastBxGauss315Box.BackColor = Color.Empty;
                        eastByGauss315Box.BackColor = Color.Empty;
                        eastBzGauss315Box.BackColor = Color.Empty;

                        await Task.Delay(500);

                        eastGxg315Box.Text = String.Format("{0:0.0000}", mwdDG1.GxG);
                        eastGyg315Box.Text = String.Format("{0:0.0000}", mwdDG1.GyG);
                        eastGzg315Box.Text = String.Format("{0:0.0000}", mwdDG1.GzG);

                        eastBxGauss315Box.Text = String.Format("{0:0.0000}", mwdDG1.BxGauss);
                        eastByGauss315Box.Text = String.Format("{0:0.0000}", mwdDG1.ByGauss);
                        eastBzGauss315Box.Text = String.Format("{0:0.0000}", mwdDG1.BzGauss);


                        eastGxg315Box.BackColor = Color.Yellow;
                        eastGyg315Box.BackColor = Color.Yellow;
                        eastGzg315Box.BackColor = Color.Yellow;
                        eastBxGauss315Box.BackColor = Color.Yellow;
                        eastByGauss315Box.BackColor = Color.Yellow;
                        eastBzGauss315Box.BackColor = Color.Yellow;

                        east315Data.GxG = mwdDG1.GxG;
                        east315Data.GyG = mwdDG1.GyG;
                        east315Data.GzG = mwdDG1.GzG;
                        east315Data.BxGauss = mwdDG1.BxGauss;
                        east315Data.ByGauss = mwdDG1.ByGauss;
                        east315Data.BzGauss = mwdDG1.BzGauss;
                        east315Data.AzimuthDegrees = mwdDG1.AzimuthDegrees;
                        east315Data.InclinationDegrees = mwdDG1.InclinationDegrees;
                        east315Data.GravityToolFaceDegrees = mwdDG1.GravityToolFaceDegrees;
                        east315Data.MagneticToolFaceDegrees = mwdDG1.MagneticToolFaceDegrees;
                        east315Data.TGF = mwdDG1.TotalGravityFieldG;
                        east315Data.TMF = mwdDG1.TotalMagneticFieldGauss;
                        east315Data.TFO = mwdDG1.ToolfaceOffsetDegrees;
                        east315Data.Temperature = mwdDG1.TemperatureCelsius;
                    }
                    break;
                case 15:
                    {
                        statusBox.Text = "ROLL POSITION 16 OF 18: EAST ROLL 360°";

                        eastGxg360Box.BackColor = Color.Empty;
                        eastGyg360Box.BackColor = Color.Empty;
                        eastGzg360Box.BackColor = Color.Empty;
                        eastBxGauss360Box.BackColor = Color.Empty;
                        eastByGauss360Box.BackColor = Color.Empty;
                        eastBzGauss360Box.BackColor = Color.Empty;

                        await Task.Delay(500);

                        eastGxg360Box.Text = String.Format("{0:0.0000}", mwdDG1.GxG);
                        eastGyg360Box.Text = String.Format("{0:0.0000}", mwdDG1.GyG);
                        eastGzg360Box.Text = String.Format("{0:0.0000}", mwdDG1.GzG);

                        eastBxGauss360Box.Text = String.Format("{0:0.0000}", mwdDG1.BxGauss);
                        eastByGauss360Box.Text = String.Format("{0:0.0000}", mwdDG1.ByGauss);
                        eastBzGauss360Box.Text = String.Format("{0:0.0000}", mwdDG1.BzGauss);


                        eastGxg360Box.BackColor = Color.Yellow;
                        eastGyg360Box.BackColor = Color.Yellow;
                        eastGzg360Box.BackColor = Color.Yellow;
                        eastBxGauss360Box.BackColor = Color.Yellow;
                        eastByGauss360Box.BackColor = Color.Yellow;
                        eastBzGauss360Box.BackColor = Color.Yellow;

                        east360Data.GxG = mwdDG1.GxG;
                        east360Data.GyG = mwdDG1.GyG;
                        east360Data.GzG = mwdDG1.GzG;
                        east360Data.BxGauss = mwdDG1.BxGauss;
                        east360Data.ByGauss = mwdDG1.ByGauss;
                        east360Data.BzGauss = mwdDG1.BzGauss;
                        east360Data.AzimuthDegrees = mwdDG1.AzimuthDegrees;
                        east360Data.InclinationDegrees = mwdDG1.InclinationDegrees;
                        east360Data.GravityToolFaceDegrees = mwdDG1.GravityToolFaceDegrees;
                        east360Data.MagneticToolFaceDegrees = mwdDG1.MagneticToolFaceDegrees;
                        east360Data.TGF = mwdDG1.TotalGravityFieldG;
                        east360Data.TMF = mwdDG1.TotalMagneticFieldGauss;
                        east360Data.TFO = mwdDG1.ToolfaceOffsetDegrees;
                        east360Data.Temperature = mwdDG1.TemperatureCelsius;
                    }
                    break;
                case 16:
                    {
                        statusBox.Text = "ROLL POSITION 17 OF 18: VERTICAL ROLL 0° INC";

                        verticalGxg0Box.BackColor = Color.Empty;
                        verticalGyg0Box.BackColor = Color.Empty;
                        verticalGzg0Box.BackColor = Color.Empty;
                        verticalBxGauss0Box.BackColor = Color.Empty;
                        verticalByGauss0Box.BackColor = Color.Empty;
                        verticalBzGauss0Box.BackColor = Color.Empty;

                        await Task.Delay(500);

                        verticalGxg0Box.Text = String.Format("{0:0.0000}", mwdDG1.GxG);
                        verticalGyg0Box.Text = String.Format("{0:0.0000}", mwdDG1.GyG);
                        verticalGzg0Box.Text = String.Format("{0:0.0000}", mwdDG1.GzG);

                        verticalBxGauss0Box.Text = String.Format("{0:0.0000}", mwdDG1.BxGauss);
                        verticalByGauss0Box.Text = String.Format("{0:0.0000}", mwdDG1.ByGauss);
                        verticalBzGauss0Box.Text = String.Format("{0:0.0000}", mwdDG1.BzGauss);


                        verticalGxg0Box.BackColor = Color.Yellow;
                        verticalGyg0Box.BackColor = Color.Yellow;
                        verticalGzg0Box.BackColor = Color.Yellow;
                        verticalBxGauss0Box.BackColor = Color.Yellow;
                        verticalByGauss0Box.BackColor = Color.Yellow;
                        verticalBzGauss0Box.BackColor = Color.Yellow;

                        vertical0Data.GxG = mwdDG1.GxG;
                        vertical0Data.GyG = mwdDG1.GyG;
                        vertical0Data.GzG = mwdDG1.GzG;
                        vertical0Data.BxGauss = mwdDG1.BxGauss;
                        vertical0Data.ByGauss = mwdDG1.ByGauss;
                        vertical0Data.BzGauss = mwdDG1.BzGauss;
                        vertical0Data.AzimuthDegrees = mwdDG1.AzimuthDegrees;
                        vertical0Data.InclinationDegrees = mwdDG1.InclinationDegrees;
                        vertical0Data.GravityToolFaceDegrees = mwdDG1.GravityToolFaceDegrees;
                        vertical0Data.MagneticToolFaceDegrees = mwdDG1.MagneticToolFaceDegrees;
                        vertical0Data.TGF = mwdDG1.TotalGravityFieldG;
                        vertical0Data.TMF = mwdDG1.TotalMagneticFieldGauss;
                        vertical0Data.TFO = mwdDG1.ToolfaceOffsetDegrees;
                        vertical0Data.Temperature = mwdDG1.TemperatureCelsius;
                    }
                    break;
                case 17:
                    {
                        statusBox.Text = "ROLL POSITION 18 OF 18: VERTICAL ROLL 180° INC";

                        verticalGxg180Box.BackColor = Color.Empty;
                        verticalGyg180Box.BackColor = Color.Empty;
                        verticalGzg180Box.BackColor = Color.Empty;
                        verticalBxGauss180Box.BackColor = Color.Empty;
                        verticalByGauss180Box.BackColor = Color.Empty;
                        verticalBzGauss180Box.BackColor = Color.Empty;

                        await Task.Delay(500);

                        verticalGxg180Box.Text = String.Format("{0:0.0000}", mwdDG1.GxG);
                        verticalGyg180Box.Text = String.Format("{0:0.0000}", mwdDG1.GyG);
                        verticalGzg180Box.Text = String.Format("{0:0.0000}", mwdDG1.GzG);

                        verticalBxGauss180Box.Text = String.Format("{0:0.0000}", mwdDG1.BxGauss);
                        verticalByGauss180Box.Text = String.Format("{0:0.0000}", mwdDG1.ByGauss);
                        verticalBzGauss180Box.Text = String.Format("{0:0.0000}", mwdDG1.BzGauss);


                        verticalGxg180Box.BackColor = Color.Yellow;
                        verticalGyg180Box.BackColor = Color.Yellow;
                        verticalGzg180Box.BackColor = Color.Yellow;
                        verticalBxGauss180Box.BackColor = Color.Yellow;
                        verticalByGauss180Box.BackColor = Color.Yellow;
                        verticalBzGauss180Box.BackColor = Color.Yellow;

                        vertical180Data.GxG = mwdDG1.GxG;
                        vertical180Data.GyG = mwdDG1.GyG;
                        vertical180Data.GzG = mwdDG1.GzG;
                        vertical180Data.BxGauss = mwdDG1.BxGauss;
                        vertical180Data.ByGauss = mwdDG1.ByGauss;
                        vertical180Data.BzGauss = mwdDG1.BzGauss;
                        vertical180Data.AzimuthDegrees = mwdDG1.AzimuthDegrees;
                        vertical180Data.InclinationDegrees = mwdDG1.InclinationDegrees;
                        vertical180Data.GravityToolFaceDegrees = mwdDG1.GravityToolFaceDegrees;
                        vertical180Data.MagneticToolFaceDegrees = mwdDG1.MagneticToolFaceDegrees;
                        vertical180Data.TGF = mwdDG1.TotalGravityFieldG;
                        vertical180Data.TMF = mwdDG1.TotalMagneticFieldGauss;
                        vertical180Data.TFO = mwdDG1.ToolfaceOffsetDegrees;
                        vertical180Data.Temperature = mwdDG1.TemperatureCelsius;
                    }
                    break;
            }
        }

        //private async void westRoll45Test_Click(object sender, EventArgs e)
        //{
        //    if (westRoll45Test.Text == "START")
        //    {
        //        westRoll45Test.Text = "STOP";

        //        while (westRoll45Test.Text == "STOP")
        //        {
        //            await Task.Delay(1000);
        //            mwdDG1 = await localNT.readDataGroup1();

        //            westGxg45Box.BackColor = Color.Empty;
        //            westGyg45Box.BackColor = Color.Empty;
        //            westGzg45Box.BackColor = Color.Empty;
        //            westBxGauss45Box.BackColor = Color.Empty;
        //            westByGauss45Box.BackColor = Color.Empty;
        //            westBzGauss45Box.BackColor = Color.Empty;

        //            westGxg45Box.Text = String.Format("{0:0.0000}", mwdDG1.GxG);
        //            westGyg45Box.Text = String.Format("{0:0.0000}", mwdDG1.GyG);
        //            westGzg45Box.Text = String.Format("{0:0.0000}", mwdDG1.GzG);

        //            westBxGauss45Box.Text = String.Format("{0:0.0000}", mwdDG1.BxGauss);
        //            westByGauss45Box.Text = String.Format("{0:0.0000}", mwdDG1.ByGauss);
        //            westBzGauss45Box.Text = String.Format("{0:0.0000}", mwdDG1.BzGauss);


        //            westGxg45Box.BackColor = Color.Yellow;
        //            westGyg45Box.BackColor = Color.Yellow;
        //            westGzg45Box.BackColor = Color.Yellow;
        //            westBxGauss45Box.BackColor = Color.Yellow;
        //            westByGauss45Box.BackColor = Color.Yellow;
        //            westBzGauss45Box.BackColor = Color.Yellow;
        //        }
        //    }
        //    else if(westRoll45Test.Text == "STOP")
        //    {
        //        westRoll45Test.Text = "START";

        //        west45Data.GxG = mwdDG1.GxG;
        //        west45Data.GyG = mwdDG1.GyG;
        //        west45Data.GzG = mwdDG1.GzG;
        //        west45Data.BxGauss = mwdDG1.BxGauss;
        //        west45Data.ByGauss = mwdDG1.ByGauss;
        //        west45Data.BzGauss = mwdDG1.BzGauss;
        //        west45Data.AzimuthDegrees = mwdDG1.AzimuthDegrees;
        //        west45Data.InclinationDegrees = mwdDG1.InclinationDegrees;
        //        west45Data.GravityToolFaceDegrees = mwdDG1.GravityToolFaceDegrees;
        //        west45Data.MagneticToolFaceDegrees = mwdDG1.MagneticToolFaceDegrees;
        //    }
        //}

        //private async void westRoll90Test_Click(object sender, EventArgs e)
        //{
        //    if (westRoll90Test.Text == "START")
        //    {
        //        westRoll90Test.Text = "STOP";

        //        while (westRoll90Test.Text == "STOP")
        //        {
        //            await Task.Delay(1000);
        //            mwdDG1 = await localNT.readDataGroup1();

        //            westGxg90Box.BackColor = Color.Empty;
        //            westGyg90Box.BackColor = Color.Empty;
        //            westGzg90Box.BackColor = Color.Empty;
        //            westBxGauss90Box.BackColor = Color.Empty;
        //            westByGauss90Box.BackColor = Color.Empty;
        //            westBzGauss90Box.BackColor = Color.Empty;

        //            westGxg90Box.Text = String.Format("{0:0.0000}", mwdDG1.GxG);
        //            westGyg90Box.Text = String.Format("{0:0.0000}", mwdDG1.GyG);
        //            westGzg90Box.Text = String.Format("{0:0.0000}", mwdDG1.GzG);

        //            westBxGauss90Box.Text = String.Format("{0:0.0000}", mwdDG1.BxGauss);
        //            westByGauss90Box.Text = String.Format("{0:0.0000}", mwdDG1.ByGauss);
        //            westBzGauss90Box.Text = String.Format("{0:0.0000}", mwdDG1.BzGauss);

        //            westGxg90Box.BackColor = Color.Yellow;
        //            westGyg90Box.BackColor = Color.Yellow;
        //            westGzg90Box.BackColor = Color.Yellow;
        //            westBxGauss90Box.BackColor = Color.Yellow;
        //            westByGauss90Box.BackColor = Color.Yellow;
        //            westBzGauss90Box.BackColor = Color.Yellow;
        //        }
        //    }
        //    else if (westRoll90Test.Text == "STOP")
        //    {
        //        westRoll90Test.Text = "START";

        //        west90Data.GxG = mwdDG1.GxG;
        //        west90Data.GyG = mwdDG1.GyG;
        //        west90Data.GzG = mwdDG1.GzG;
        //        west90Data.BxGauss = mwdDG1.BxGauss;
        //        west90Data.ByGauss = mwdDG1.ByGauss;
        //        west90Data.BzGauss = mwdDG1.BzGauss;
        //        west90Data.AzimuthDegrees = mwdDG1.AzimuthDegrees;
        //        west90Data.InclinationDegrees = mwdDG1.InclinationDegrees;
        //        west90Data.GravityToolFaceDegrees = mwdDG1.GravityToolFaceDegrees;
        //        west90Data.MagneticToolFaceDegrees = mwdDG1.MagneticToolFaceDegrees;
        //    }
        //}

        //private async void westRoll135Test_Click(object sender, EventArgs e)
        //{
        //    if (westRoll135Test.Text == "START")
        //    {
        //        westRoll135Test.Text = "STOP";

        //        while (westRoll135Test.Text == "STOP")
        //        {
        //            await Task.Delay(1000);
        //            mwdDG1 = await localNT.readDataGroup1();

        //            westGxg135Box.BackColor = Color.Empty;
        //            westGyg135Box.BackColor = Color.Empty;
        //            westGzg135Box.BackColor = Color.Empty;
        //            westBxGauss135Box.BackColor = Color.Empty;
        //            westByGauss135Box.BackColor = Color.Empty;
        //            westBzGauss135Box.BackColor = Color.Empty;

        //            westGxg135Box.Text = String.Format("{0:0.0000}", mwdDG1.GxG);
        //            westGyg135Box.Text = String.Format("{0:0.0000}", mwdDG1.GyG);
        //            westGzg135Box.Text = String.Format("{0:0.0000}", mwdDG1.GzG);

        //            westBxGauss135Box.Text = String.Format("{0:0.0000}", mwdDG1.BxGauss);
        //            westByGauss135Box.Text = String.Format("{0:0.0000}", mwdDG1.ByGauss);
        //            westBzGauss135Box.Text = String.Format("{0:0.0000}", mwdDG1.BzGauss);

        //            westGxg135Box.BackColor = Color.Yellow;
        //            westGyg135Box.BackColor = Color.Yellow;
        //            westGzg135Box.BackColor = Color.Yellow;
        //            westBxGauss135Box.BackColor = Color.Yellow;
        //            westByGauss135Box.BackColor = Color.Yellow;
        //            westBzGauss135Box.BackColor = Color.Yellow;
        //        }
        //    }
        //    else if (westRoll135Test.Text == "STOP")
        //    {
        //        westRoll135Test.Text = "START";

        //        west135Data.GxG = mwdDG1.GxG;
        //        west135Data.GyG = mwdDG1.GyG;
        //        west135Data.GzG = mwdDG1.GzG;
        //        west135Data.BxGauss = mwdDG1.BxGauss;
        //        west135Data.ByGauss = mwdDG1.ByGauss;
        //        west135Data.BzGauss = mwdDG1.BzGauss;
        //        west135Data.AzimuthDegrees = mwdDG1.AzimuthDegrees;
        //        west135Data.InclinationDegrees = mwdDG1.InclinationDegrees;
        //        west135Data.GravityToolFaceDegrees = mwdDG1.GravityToolFaceDegrees;
        //        west135Data.MagneticToolFaceDegrees = mwdDG1.MagneticToolFaceDegrees;
        //    }
        //}

        //private async void westRoll180Box_Click(object sender, EventArgs e)
        //{
        //    if (westRoll180Test.Text == "START")
        //    {
        //        westRoll180Test.Text = "STOP";

        //        while (westRoll180Test.Text == "STOP")
        //        {
        //            await Task.Delay(1000);
        //            mwdDG1 = await localNT.readDataGroup1();

        //            westGxg180Box.BackColor = Color.Empty;
        //            westGyg180Box.BackColor = Color.Empty;
        //            westGzg180Box.BackColor = Color.Empty;
        //            westBxGauss180Box.BackColor = Color.Empty;
        //            westByGauss180Box.BackColor = Color.Empty;
        //            westBzGauss180Box.BackColor = Color.Empty;

        //            westGxg180Box.Text = String.Format("{0:0.0000}", mwdDG1.GxG);
        //            westGyg180Box.Text = String.Format("{0:0.0000}", mwdDG1.GyG);
        //            westGzg180Box.Text = String.Format("{0:0.0000}", mwdDG1.GzG);

        //            westBxGauss180Box.Text = String.Format("{0:0.0000}", mwdDG1.BxGauss);
        //            westByGauss180Box.Text = String.Format("{0:0.0000}", mwdDG1.ByGauss);
        //            westBzGauss180Box.Text = String.Format("{0:0.0000}", mwdDG1.BzGauss);

        //            westGxg180Box.BackColor = Color.Yellow;
        //            westGyg180Box.BackColor = Color.Yellow;
        //            westGzg180Box.BackColor = Color.Yellow;
        //            westBxGauss180Box.BackColor = Color.Yellow;
        //            westByGauss180Box.BackColor = Color.Yellow;
        //            westBzGauss180Box.BackColor = Color.Yellow;
        //        }
        //    }
        //    else if (westRoll180Test.Text == "STOP")
        //    {
        //        westRoll180Test.Text = "START";

        //        west180Data.GxG = mwdDG1.GxG;
        //        west180Data.GyG = mwdDG1.GyG;
        //        west180Data.GzG = mwdDG1.GzG;
        //        west180Data.BxGauss = mwdDG1.BxGauss;
        //        west180Data.ByGauss = mwdDG1.ByGauss;
        //        west180Data.BzGauss = mwdDG1.BzGauss;
        //        west180Data.AzimuthDegrees = mwdDG1.AzimuthDegrees;
        //        west180Data.InclinationDegrees = mwdDG1.InclinationDegrees;
        //        west180Data.GravityToolFaceDegrees = mwdDG1.GravityToolFaceDegrees;
        //        west180Data.MagneticToolFaceDegrees = mwdDG1.MagneticToolFaceDegrees;
        //    }
        //}

        //private async void westRoll225Test_Click(object sender, EventArgs e)
        //{
        //    if (westRoll225Test.Text == "START")
        //    {
        //        westRoll225Test.Text = "STOP";

        //        while (westRoll225Test.Text == "STOP")
        //        {
        //            await Task.Delay(1000);
        //            mwdDG1 = await localNT.readDataGroup1();

        //            westGxg225Box.BackColor = Color.Empty;
        //            westGyg225Box.BackColor = Color.Empty;
        //            westGzg225Box.BackColor = Color.Empty;
        //            westBxGauss225Box.BackColor = Color.Empty;
        //            westByGauss225Box.BackColor = Color.Empty;
        //            westBzGauss225Box.BackColor = Color.Empty;

        //            westGxg225Box.Text = String.Format("{0:0.0000}", mwdDG1.GxG);
        //            westGyg225Box.Text = String.Format("{0:0.0000}", mwdDG1.GyG);
        //            westGzg225Box.Text = String.Format("{0:0.0000}", mwdDG1.GzG);

        //            westBxGauss225Box.Text = String.Format("{0:0.0000}", mwdDG1.BxGauss);
        //            westByGauss225Box.Text = String.Format("{0:0.0000}", mwdDG1.ByGauss);
        //            westBzGauss225Box.Text = String.Format("{0:0.0000}", mwdDG1.BzGauss);

        //            westGxg225Box.BackColor = Color.Yellow;
        //            westGyg225Box.BackColor = Color.Yellow;
        //            westGzg225Box.BackColor = Color.Yellow;
        //            westBxGauss225Box.BackColor = Color.Yellow;
        //            westByGauss225Box.BackColor = Color.Yellow;
        //            westBzGauss225Box.BackColor = Color.Yellow;
        //        }
        //    }
        //    else if (westRoll225Test.Text == "STOP")
        //    {
        //        westRoll225Test.Text = "START";

        //        west225Data.GxG = mwdDG1.GxG;
        //        west225Data.GyG = mwdDG1.GyG;
        //        west225Data.GzG = mwdDG1.GzG;
        //        west225Data.BxGauss = mwdDG1.BxGauss;
        //        west225Data.ByGauss = mwdDG1.ByGauss;
        //        west225Data.BzGauss = mwdDG1.BzGauss;
        //        west225Data.AzimuthDegrees = mwdDG1.AzimuthDegrees;
        //        west225Data.InclinationDegrees = mwdDG1.InclinationDegrees;
        //        west225Data.GravityToolFaceDegrees = mwdDG1.GravityToolFaceDegrees;
        //        west225Data.MagneticToolFaceDegrees = mwdDG1.MagneticToolFaceDegrees;
        //    }
        //}

        //private async void westRoll270Test_Click(object sender, EventArgs e)
        //{
        //    if (westRoll270Test.Text == "START")
        //    {
        //        westRoll270Test.Text = "STOP";

        //        while (westRoll270Test.Text == "STOP")
        //        {
        //            await Task.Delay(1000);
        //            mwdDG1 = await localNT.readDataGroup1();

        //            westGxg270Box.BackColor = Color.Empty;
        //            westGyg270Box.BackColor = Color.Empty;
        //            westGzg270Box.BackColor = Color.Empty;
        //            westBxGauss270Box.BackColor = Color.Empty;
        //            westByGauss270Box.BackColor = Color.Empty;
        //            westBzGauss270Box.BackColor = Color.Empty;

        //            westGxg270Box.Text = String.Format("{0:0.0000}", mwdDG1.GxG);
        //            westGyg270Box.Text = String.Format("{0:0.0000}", mwdDG1.GyG);
        //            westGzg270Box.Text = String.Format("{0:0.0000}", mwdDG1.GzG);

        //            westBxGauss270Box.Text = String.Format("{0:0.0000}", mwdDG1.BxGauss);
        //            westByGauss270Box.Text = String.Format("{0:0.0000}", mwdDG1.ByGauss);
        //            westBzGauss270Box.Text = String.Format("{0:0.0000}", mwdDG1.BzGauss);

        //            westGxg270Box.BackColor = Color.Yellow;
        //            westGyg270Box.BackColor = Color.Yellow;
        //            westGzg270Box.BackColor = Color.Yellow;
        //            westBxGauss270Box.BackColor = Color.Yellow;
        //            westByGauss270Box.BackColor = Color.Yellow;
        //            westBzGauss270Box.BackColor = Color.Yellow;
        //        }
        //    }
        //    else if (westRoll270Test.Text == "STOP")
        //    {
        //        westRoll270Test.Text = "START";

        //        west270Data.GxG = mwdDG1.GxG;
        //        west270Data.GyG = mwdDG1.GyG;
        //        west270Data.GzG = mwdDG1.GzG;
        //        west270Data.BxGauss = mwdDG1.BxGauss;
        //        west270Data.ByGauss = mwdDG1.ByGauss;
        //        west270Data.BzGauss = mwdDG1.BzGauss;
        //        west270Data.AzimuthDegrees = mwdDG1.AzimuthDegrees;
        //        west270Data.InclinationDegrees = mwdDG1.InclinationDegrees;
        //        west270Data.GravityToolFaceDegrees = mwdDG1.GravityToolFaceDegrees;
        //        west270Data.MagneticToolFaceDegrees = mwdDG1.MagneticToolFaceDegrees;
        //    }
        //}

        //private async void westRoll315Test_Click(object sender, EventArgs e)
        //{
        //    if (westRoll315Test.Text == "START")
        //    {
        //        westRoll315Test.Text = "STOP";

        //        while (westRoll315Test.Text == "STOP")
        //        {
        //            await Task.Delay(1000);
        //            mwdDG1 = await localNT.readDataGroup1();

        //            westGxg315Box.BackColor = Color.Empty;
        //            westGyg315Box.BackColor = Color.Empty;
        //            westGzg315Box.BackColor = Color.Empty;
        //            westBxGauss315Box.BackColor = Color.Empty;
        //            westByGauss315Box.BackColor = Color.Empty;
        //            westBzGauss315Box.BackColor = Color.Empty;

        //            westGxg315Box.Text = String.Format("{0:0.0000}", mwdDG1.GxG);
        //            westGyg315Box.Text = String.Format("{0:0.0000}", mwdDG1.GyG);
        //            westGzg315Box.Text = String.Format("{0:0.0000}", mwdDG1.GzG);

        //            westBxGauss315Box.Text = String.Format("{0:0.0000}", mwdDG1.BxGauss);
        //            westByGauss315Box.Text = String.Format("{0:0.0000}", mwdDG1.ByGauss);
        //            westBzGauss315Box.Text = String.Format("{0:0.0000}", mwdDG1.BzGauss);

        //            westGxg315Box.BackColor = Color.Yellow;
        //            westGyg315Box.BackColor = Color.Yellow;
        //            westGzg315Box.BackColor = Color.Yellow;
        //            westBxGauss315Box.BackColor = Color.Yellow;
        //            westByGauss315Box.BackColor = Color.Yellow;
        //            westBzGauss315Box.BackColor = Color.Yellow;
        //        }
        //    }
        //    else if (westRoll315Test.Text == "STOP")
        //    {
        //        westRoll315Test.Text = "START";

        //        west315Data.GxG = mwdDG1.GxG;
        //        west315Data.GyG = mwdDG1.GyG;
        //        west315Data.GzG = mwdDG1.GzG;
        //        west315Data.BxGauss = mwdDG1.BxGauss;
        //        west315Data.ByGauss = mwdDG1.ByGauss;
        //        west315Data.BzGauss = mwdDG1.BzGauss;
        //        west315Data.AzimuthDegrees = mwdDG1.AzimuthDegrees;
        //        west315Data.InclinationDegrees = mwdDG1.InclinationDegrees;
        //        west315Data.GravityToolFaceDegrees = mwdDG1.GravityToolFaceDegrees;
        //        west315Data.MagneticToolFaceDegrees = mwdDG1.MagneticToolFaceDegrees;
        //    }
        //}

        //private async void westRoll360Test_Click(object sender, EventArgs e)
        //{
        //    if (westRoll360Test.Text == "START")
        //    {
        //        westRoll360Test.Text = "STOP";

        //        while (westRoll360Test.Text == "STOP")
        //        {
        //            await Task.Delay(1000);
        //            mwdDG1 = await localNT.readDataGroup1();

        //            westGxg360Box.BackColor = Color.Empty;
        //            westGyg360Box.BackColor = Color.Empty;
        //            westGzg360Box.BackColor = Color.Empty;
        //            westBxGauss360Box.BackColor = Color.Empty;
        //            westByGauss360Box.BackColor = Color.Empty;
        //            westBzGauss360Box.BackColor = Color.Empty;

        //            westGxg360Box.Text = String.Format("{0:0.0000}", mwdDG1.GxG);
        //            westGyg360Box.Text = String.Format("{0:0.0000}", mwdDG1.GyG);
        //            westGzg360Box.Text = String.Format("{0:0.0000}", mwdDG1.GzG);

        //            westBxGauss360Box.Text = String.Format("{0:0.0000}", mwdDG1.BxGauss);
        //            westByGauss360Box.Text = String.Format("{0:0.0000}", mwdDG1.ByGauss);
        //            westBzGauss360Box.Text = String.Format("{0:0.0000}", mwdDG1.BzGauss);

        //            westGxg360Box.BackColor = Color.Yellow;
        //            westGyg360Box.BackColor = Color.Yellow;
        //            westGzg360Box.BackColor = Color.Yellow;
        //            westBxGauss360Box.BackColor = Color.Yellow;
        //            westByGauss360Box.BackColor = Color.Yellow;
        //            westBzGauss360Box.BackColor = Color.Yellow;
        //        }
        //    }
        //    else if (westRoll360Test.Text == "STOP")
        //    {
        //        westRoll360Test.Text = "START";

        //        west360Data.GxG = mwdDG1.GxG;
        //        west360Data.GyG = mwdDG1.GyG;
        //        west360Data.GzG = mwdDG1.GzG;
        //        west360Data.BxGauss = mwdDG1.BxGauss;
        //        west360Data.ByGauss = mwdDG1.ByGauss;
        //        west360Data.BzGauss = mwdDG1.BzGauss;
        //        west360Data.AzimuthDegrees = mwdDG1.AzimuthDegrees;
        //        west360Data.InclinationDegrees = mwdDG1.InclinationDegrees;
        //        west360Data.GravityToolFaceDegrees = mwdDG1.GravityToolFaceDegrees;
        //        west360Data.MagneticToolFaceDegrees = mwdDG1.MagneticToolFaceDegrees;
        //    }
        //}

        //private async void eastRoll45Test_Click(object sender, EventArgs e)
        //{
        //    if (eastRoll45Test.Text == "START")
        //    {
        //        eastRoll45Test.Text = "STOP";

        //        while (eastRoll45Test.Text == "STOP")
        //        {
        //            await Task.Delay(1000);
        //            mwdDG1 = await localNT.readDataGroup1();

        //            eastGxg45Box.BackColor = Color.Empty;
        //            eastGyg45Box.BackColor = Color.Empty;
        //            eastGzg45Box.BackColor = Color.Empty;
        //            eastBxGauss45Box.BackColor = Color.Empty;
        //            eastByGauss45Box.BackColor = Color.Empty;
        //            eastBzGauss45Box.BackColor = Color.Empty;

        //            eastGxg45Box.Text = String.Format("{0:0.0000}", mwdDG1.GxG);
        //            eastGyg45Box.Text = String.Format("{0:0.0000}", mwdDG1.GyG);
        //            eastGzg45Box.Text = String.Format("{0:0.0000}", mwdDG1.GzG);

        //            eastBxGauss45Box.Text = String.Format("{0:0.0000}", mwdDG1.BxGauss);
        //            eastByGauss45Box.Text = String.Format("{0:0.0000}", mwdDG1.ByGauss);
        //            eastBzGauss45Box.Text = String.Format("{0:0.0000}", mwdDG1.BzGauss);

        //            eastGxg45Box.BackColor = Color.Yellow;
        //            eastGyg45Box.BackColor = Color.Yellow;
        //            eastGzg45Box.BackColor = Color.Yellow;
        //            eastBxGauss45Box.BackColor = Color.Yellow;
        //            eastByGauss45Box.BackColor = Color.Yellow;
        //            eastBzGauss45Box.BackColor = Color.Yellow;
        //        }
        //    }
        //    else if (eastRoll45Test.Text == "STOP")
        //    {
        //        eastRoll45Test.Text = "START";

        //        east45Data.GxG = mwdDG1.GxG;
        //        east45Data.GyG = mwdDG1.GyG;
        //        east45Data.GzG = mwdDG1.GzG;
        //        east45Data.BxGauss = mwdDG1.BxGauss;
        //        east45Data.ByGauss = mwdDG1.ByGauss;
        //        east45Data.BzGauss = mwdDG1.BzGauss;
        //        east45Data.AzimuthDegrees = mwdDG1.AzimuthDegrees;
        //        east45Data.InclinationDegrees = mwdDG1.InclinationDegrees;
        //        east45Data.GravityToolFaceDegrees = mwdDG1.GravityToolFaceDegrees;
        //        east45Data.MagneticToolFaceDegrees = mwdDG1.MagneticToolFaceDegrees;
        //    }
        //}

        //private async void eastRoll90Test_Click(object sender, EventArgs e)
        //{
        //    if (eastRoll90Test.Text == "START")
        //    {
        //        eastRoll90Test.Text = "STOP";

        //        while (eastRoll90Test.Text == "STOP")
        //        {
        //            await Task.Delay(1000);
        //            mwdDG1 = await localNT.readDataGroup1();

        //            eastGxg90Box.BackColor = Color.Empty;
        //            eastGyg90Box.BackColor = Color.Empty;
        //            eastGzg90Box.BackColor = Color.Empty;
        //            eastBxGauss90Box.BackColor = Color.Empty;
        //            eastByGauss90Box.BackColor = Color.Empty;
        //            eastBzGauss90Box.BackColor = Color.Empty;

        //            eastGxg90Box.Text = String.Format("{0:0.0000}", mwdDG1.GxG);
        //            eastGyg90Box.Text = String.Format("{0:0.0000}", mwdDG1.GyG);
        //            eastGzg90Box.Text = String.Format("{0:0.0000}", mwdDG1.GzG);

        //            eastBxGauss90Box.Text = String.Format("{0:0.0000}", mwdDG1.BxGauss);
        //            eastByGauss90Box.Text = String.Format("{0:0.0000}", mwdDG1.ByGauss);
        //            eastBzGauss90Box.Text = String.Format("{0:0.0000}", mwdDG1.BzGauss);

        //            eastGxg90Box.BackColor = Color.Yellow;
        //            eastGyg90Box.BackColor = Color.Yellow;
        //            eastGzg90Box.BackColor = Color.Yellow;
        //            eastBxGauss90Box.BackColor = Color.Yellow;
        //            eastByGauss90Box.BackColor = Color.Yellow;
        //            eastBzGauss90Box.BackColor = Color.Yellow;
        //        }
        //    }
        //    else if (eastRoll90Test.Text == "STOP")
        //    {
        //        eastRoll90Test.Text = "START";

        //        east90Data.GxG = mwdDG1.GxG;
        //        east90Data.GyG = mwdDG1.GyG;
        //        east90Data.GzG = mwdDG1.GzG;
        //        east90Data.BxGauss = mwdDG1.BxGauss;
        //        east90Data.ByGauss = mwdDG1.ByGauss;
        //        east90Data.BzGauss = mwdDG1.BzGauss;
        //        east90Data.AzimuthDegrees = mwdDG1.AzimuthDegrees;
        //        east90Data.InclinationDegrees = mwdDG1.InclinationDegrees;
        //        east90Data.GravityToolFaceDegrees = mwdDG1.GravityToolFaceDegrees;
        //        east90Data.MagneticToolFaceDegrees = mwdDG1.MagneticToolFaceDegrees;
        //    }
        //}

        //private async void eastRoll135Test_Click(object sender, EventArgs e)
        //{
        //    if (eastRoll135Test.Text == "START")
        //    {
        //        eastRoll135Test.Text = "STOP";

        //        while (eastRoll135Test.Text == "STOP")
        //        {
        //            await Task.Delay(1000);
        //            mwdDG1 = await localNT.readDataGroup1();

        //            eastGxg135Box.BackColor = Color.Empty;
        //            eastGyg135Box.BackColor = Color.Empty;
        //            eastGzg135Box.BackColor = Color.Empty;
        //            eastBxGauss135Box.BackColor = Color.Empty;
        //            eastByGauss135Box.BackColor = Color.Empty;
        //            eastBzGauss135Box.BackColor = Color.Empty;

        //            eastGxg135Box.Text = String.Format("{0:0.0000}", mwdDG1.GxG);
        //            eastGyg135Box.Text = String.Format("{0:0.0000}", mwdDG1.GyG);
        //            eastGzg135Box.Text = String.Format("{0:0.0000}", mwdDG1.GzG);

        //            eastBxGauss135Box.Text = String.Format("{0:0.0000}", mwdDG1.BxGauss);
        //            eastByGauss135Box.Text = String.Format("{0:0.0000}", mwdDG1.ByGauss);
        //            eastBzGauss135Box.Text = String.Format("{0:0.0000}", mwdDG1.BzGauss);

        //            eastGxg135Box.BackColor = Color.Yellow;
        //            eastGyg135Box.BackColor = Color.Yellow;
        //            eastGzg135Box.BackColor = Color.Yellow;
        //            eastBxGauss135Box.BackColor = Color.Yellow;
        //            eastByGauss135Box.BackColor = Color.Yellow;
        //            eastBzGauss135Box.BackColor = Color.Yellow;
        //        }
        //    }
        //    else if (eastRoll135Test.Text == "STOP")
        //    {
        //        eastRoll135Test.Text = "START";

        //        east135Data.GxG = mwdDG1.GxG;
        //        east135Data.GyG = mwdDG1.GyG;
        //        east135Data.GzG = mwdDG1.GzG;
        //        east135Data.BxGauss = mwdDG1.BxGauss;
        //        east135Data.ByGauss = mwdDG1.ByGauss;
        //        east135Data.BzGauss = mwdDG1.BzGauss;
        //        east135Data.AzimuthDegrees = mwdDG1.AzimuthDegrees;
        //        east135Data.InclinationDegrees = mwdDG1.InclinationDegrees;
        //        east135Data.GravityToolFaceDegrees = mwdDG1.GravityToolFaceDegrees;
        //        east135Data.MagneticToolFaceDegrees = mwdDG1.MagneticToolFaceDegrees;
        //    }
        //}

        //private async void eastRoll180Test_Click(object sender, EventArgs e)
        //{
        //    if (eastRoll180Test.Text == "START")
        //    {
        //        eastRoll180Test.Text = "STOP";

        //        while (eastRoll180Test.Text == "STOP")
        //        {
        //            await Task.Delay(1000);
        //            mwdDG1 = await localNT.readDataGroup1();

        //            eastGxg180Box.BackColor = Color.Empty;
        //            eastGyg180Box.BackColor = Color.Empty;
        //            eastGzg180Box.BackColor = Color.Empty;
        //            eastBxGauss180Box.BackColor = Color.Empty;
        //            eastByGauss180Box.BackColor = Color.Empty;
        //            eastBzGauss180Box.BackColor = Color.Empty;

        //            eastGxg180Box.Text = String.Format("{0:0.0000}", mwdDG1.GxG);
        //            eastGyg180Box.Text = String.Format("{0:0.0000}", mwdDG1.GyG);
        //            eastGzg180Box.Text = String.Format("{0:0.0000}", mwdDG1.GzG);

        //            eastBxGauss180Box.Text = String.Format("{0:0.0000}", mwdDG1.BxGauss);
        //            eastByGauss180Box.Text = String.Format("{0:0.0000}", mwdDG1.ByGauss);
        //            eastBzGauss180Box.Text = String.Format("{0:0.0000}", mwdDG1.BzGauss);

        //            eastGxg180Box.BackColor = Color.Yellow;
        //            eastGyg180Box.BackColor = Color.Yellow;
        //            eastGzg180Box.BackColor = Color.Yellow;
        //            eastBxGauss180Box.BackColor = Color.Yellow;
        //            eastByGauss180Box.BackColor = Color.Yellow;
        //            eastBzGauss180Box.BackColor = Color.Yellow;
        //        }
        //    }
        //    else if (eastRoll180Test.Text == "STOP")
        //    {
        //        eastRoll180Test.Text = "START";

        //        east180Data.GxG = mwdDG1.GxG;
        //        east180Data.GyG = mwdDG1.GyG;
        //        east180Data.GzG = mwdDG1.GzG;
        //        east180Data.BxGauss = mwdDG1.BxGauss;
        //        east180Data.ByGauss = mwdDG1.ByGauss;
        //        east180Data.BzGauss = mwdDG1.BzGauss;
        //        east180Data.AzimuthDegrees = mwdDG1.AzimuthDegrees;
        //        east180Data.InclinationDegrees = mwdDG1.InclinationDegrees;
        //        east180Data.GravityToolFaceDegrees = mwdDG1.GravityToolFaceDegrees;
        //        east180Data.MagneticToolFaceDegrees = mwdDG1.MagneticToolFaceDegrees;
        //    }
        //}

        //private async void eastRoll225Test_Click(object sender, EventArgs e)
        //{
        //    if (eastRoll225Test.Text == "START")
        //    {
        //        eastRoll225Test.Text = "STOP";

        //        while (eastRoll225Test.Text == "STOP")
        //        {
        //            await Task.Delay(1000);
        //            mwdDG1 = await localNT.readDataGroup1();

        //            eastGxg225Box.BackColor = Color.Empty;
        //            eastGyg225Box.BackColor = Color.Empty;
        //            eastGzg225Box.BackColor = Color.Empty;
        //            eastBxGauss225Box.BackColor = Color.Empty;
        //            eastByGauss225Box.BackColor = Color.Empty;
        //            eastBzGauss225Box.BackColor = Color.Empty;

        //            eastGxg225Box.Text = String.Format("{0:0.0000}", mwdDG1.GxG);
        //            eastGyg225Box.Text = String.Format("{0:0.0000}", mwdDG1.GyG);
        //            eastGzg225Box.Text = String.Format("{0:0.0000}", mwdDG1.GzG);

        //            eastBxGauss225Box.Text = String.Format("{0:0.0000}", mwdDG1.BxGauss);
        //            eastByGauss225Box.Text = String.Format("{0:0.0000}", mwdDG1.ByGauss);
        //            eastBzGauss225Box.Text = String.Format("{0:0.0000}", mwdDG1.BzGauss);

        //            eastGxg225Box.BackColor = Color.Yellow;
        //            eastGyg225Box.BackColor = Color.Yellow;
        //            eastGzg225Box.BackColor = Color.Yellow;
        //            eastBxGauss225Box.BackColor = Color.Yellow;
        //            eastByGauss225Box.BackColor = Color.Yellow;
        //            eastBzGauss225Box.BackColor = Color.Yellow;
        //        }
        //    }
        //    else if (eastRoll225Test.Text == "STOP")
        //    {
        //        eastRoll225Test.Text = "START";

        //        east225Data.GxG = mwdDG1.GxG;
        //        east225Data.GyG = mwdDG1.GyG;
        //        east225Data.GzG = mwdDG1.GzG;
        //        east225Data.BxGauss = mwdDG1.BxGauss;
        //        east225Data.ByGauss = mwdDG1.ByGauss;
        //        east225Data.BzGauss = mwdDG1.BzGauss;
        //        east225Data.AzimuthDegrees = mwdDG1.AzimuthDegrees;
        //        east225Data.InclinationDegrees = mwdDG1.InclinationDegrees;
        //        east225Data.GravityToolFaceDegrees = mwdDG1.GravityToolFaceDegrees;
        //        east225Data.MagneticToolFaceDegrees = mwdDG1.MagneticToolFaceDegrees;
        //    }
        //}

        //private async void eastRoll270Test_Click(object sender, EventArgs e)
        //{
        //    if (eastRoll270Test.Text == "START")
        //    {
        //        eastRoll270Test.Text = "STOP";

        //        while (eastRoll270Test.Text == "STOP")
        //        {
        //            await Task.Delay(1000);
        //            mwdDG1 = await localNT.readDataGroup1();

        //            eastGxg270Box.BackColor = Color.Empty;
        //            eastGyg270Box.BackColor = Color.Empty;
        //            eastGzg270Box.BackColor = Color.Empty;
        //            eastBxGauss270Box.BackColor = Color.Empty;
        //            eastByGauss270Box.BackColor = Color.Empty;
        //            eastBzGauss270Box.BackColor = Color.Empty;

        //            eastGxg270Box.Text = String.Format("{0:0.0000}", mwdDG1.GxG);
        //            eastGyg270Box.Text = String.Format("{0:0.0000}", mwdDG1.GyG);
        //            eastGzg270Box.Text = String.Format("{0:0.0000}", mwdDG1.GzG);

        //            eastBxGauss270Box.Text = String.Format("{0:0.0000}", mwdDG1.BxGauss);
        //            eastByGauss270Box.Text = String.Format("{0:0.0000}", mwdDG1.ByGauss);
        //            eastBzGauss270Box.Text = String.Format("{0:0.0000}", mwdDG1.BzGauss);

        //            eastGxg270Box.BackColor = Color.Yellow;
        //            eastGyg270Box.BackColor = Color.Yellow;
        //            eastGzg270Box.BackColor = Color.Yellow;
        //            eastBxGauss270Box.BackColor = Color.Yellow;
        //            eastByGauss270Box.BackColor = Color.Yellow;
        //            eastBzGauss270Box.BackColor = Color.Yellow;
        //        }
        //    }
        //    else if (eastRoll270Test.Text == "STOP")
        //    {
        //        eastRoll270Test.Text = "START";

        //        east270Data.GxG = mwdDG1.GxG;
        //        east270Data.GyG = mwdDG1.GyG;
        //        east270Data.GzG = mwdDG1.GzG;
        //        east270Data.BxGauss = mwdDG1.BxGauss;
        //        east270Data.ByGauss = mwdDG1.ByGauss;
        //        east270Data.BzGauss = mwdDG1.BzGauss;
        //        east270Data.AzimuthDegrees = mwdDG1.AzimuthDegrees;
        //        east270Data.InclinationDegrees = mwdDG1.InclinationDegrees;
        //        east270Data.GravityToolFaceDegrees = mwdDG1.GravityToolFaceDegrees;
        //        east270Data.MagneticToolFaceDegrees = mwdDG1.MagneticToolFaceDegrees;
        //    }
        //}

        //private async void eastRoll315Test_Click(object sender, EventArgs e)
        //{
        //    if (eastRoll315Test.Text == "START")
        //    {
        //        eastRoll315Test.Text = "STOP";

        //        while (eastRoll315Test.Text == "STOP")
        //        {
        //            await Task.Delay(1000);
        //            mwdDG1 = await localNT.readDataGroup1();

        //            eastGxg315Box.BackColor = Color.Empty;
        //            eastGyg315Box.BackColor = Color.Empty;
        //            eastGzg315Box.BackColor = Color.Empty;
        //            eastBxGauss315Box.BackColor = Color.Empty;
        //            eastByGauss315Box.BackColor = Color.Empty;
        //            eastBzGauss315Box.BackColor = Color.Empty;

        //            eastGxg315Box.Text = String.Format("{0:0.0000}", mwdDG1.GxG);
        //            eastGyg315Box.Text = String.Format("{0:0.0000}", mwdDG1.GyG);
        //            eastGzg315Box.Text = String.Format("{0:0.0000}", mwdDG1.GzG);

        //            eastBxGauss315Box.Text = String.Format("{0:0.0000}", mwdDG1.BxGauss);
        //            eastByGauss315Box.Text = String.Format("{0:0.0000}", mwdDG1.ByGauss);
        //            eastBzGauss315Box.Text = String.Format("{0:0.0000}", mwdDG1.BzGauss);

        //            eastGxg315Box.BackColor = Color.Yellow;
        //            eastGyg315Box.BackColor = Color.Yellow;
        //            eastGzg315Box.BackColor = Color.Yellow;
        //            eastBxGauss315Box.BackColor = Color.Yellow;
        //            eastByGauss315Box.BackColor = Color.Yellow;
        //            eastBzGauss315Box.BackColor = Color.Yellow;
        //        }
        //    }
        //    else if (eastRoll315Test.Text == "STOP")
        //    {
        //        eastRoll315Test.Text = "START";

        //        east315Data.GxG = mwdDG1.GxG;
        //        east315Data.GyG = mwdDG1.GyG;
        //        east315Data.GzG = mwdDG1.GzG;
        //        east315Data.BxGauss = mwdDG1.BxGauss;
        //        east315Data.ByGauss = mwdDG1.ByGauss;
        //        east315Data.BzGauss = mwdDG1.BzGauss;
        //        east315Data.AzimuthDegrees = mwdDG1.AzimuthDegrees;
        //        east315Data.InclinationDegrees = mwdDG1.InclinationDegrees;
        //        east315Data.GravityToolFaceDegrees = mwdDG1.GravityToolFaceDegrees;
        //        east315Data.MagneticToolFaceDegrees = mwdDG1.MagneticToolFaceDegrees;
        //    }
        //}

        //private async void eastRoll360Test_Click(object sender, EventArgs e)
        //{
        //    if (eastRoll360Test.Text == "START")
        //    {
        //        eastRoll360Test.Text = "STOP";

        //        while (eastRoll360Test.Text == "STOP")
        //        {
        //            await Task.Delay(1000);
        //            mwdDG1 = await localNT.readDataGroup1();

        //            eastGxg360Box.BackColor = Color.Empty;
        //            eastGyg360Box.BackColor = Color.Empty;
        //            eastGzg360Box.BackColor = Color.Empty;
        //            eastBxGauss360Box.BackColor = Color.Empty;
        //            eastByGauss360Box.BackColor = Color.Empty;
        //            eastBzGauss360Box.BackColor = Color.Empty;

        //            eastGxg360Box.Text = String.Format("{0:0.0000}", mwdDG1.GxG);
        //            eastGyg360Box.Text = String.Format("{0:0.0000}", mwdDG1.GyG);
        //            eastGzg360Box.Text = String.Format("{0:0.0000}", mwdDG1.GzG);

        //            eastBxGauss360Box.Text = String.Format("{0:0.0000}", mwdDG1.BxGauss);
        //            eastByGauss360Box.Text = String.Format("{0:0.0000}", mwdDG1.ByGauss);
        //            eastBzGauss360Box.Text = String.Format("{0:0.0000}", mwdDG1.BzGauss);

        //            eastGxg360Box.BackColor = Color.Yellow;
        //            eastGyg360Box.BackColor = Color.Yellow;
        //            eastGzg360Box.BackColor = Color.Yellow;
        //            eastBxGauss360Box.BackColor = Color.Yellow;
        //            eastByGauss360Box.BackColor = Color.Yellow;
        //            eastBzGauss360Box.BackColor = Color.Yellow;
        //        }
        //    }
        //    else if (eastRoll360Test.Text == "STOP")
        //    {
        //        eastRoll360Test.Text = "START";

        //        east360Data.GxG = mwdDG1.GxG;
        //        east360Data.GyG = mwdDG1.GyG;
        //        east360Data.GzG = mwdDG1.GzG;
        //        east360Data.BxGauss = mwdDG1.BxGauss;
        //        east360Data.ByGauss = mwdDG1.ByGauss;
        //        east360Data.BzGauss = mwdDG1.BzGauss;
        //        east360Data.AzimuthDegrees = mwdDG1.AzimuthDegrees;
        //        east360Data.InclinationDegrees = mwdDG1.InclinationDegrees;
        //        east360Data.GravityToolFaceDegrees = mwdDG1.GravityToolFaceDegrees;
        //        east360Data.MagneticToolFaceDegrees = mwdDG1.MagneticToolFaceDegrees;
        //    }
        //}

        //private async void verticalRoll45Test_Click(object sender, EventArgs e)
        //{
        //    if (verticalRoll45Test.Text == "START")
        //    {
        //        verticalRoll45Test.Text = "STOP";

        //        while (verticalRoll45Test.Text == "STOP")
        //        {
        //            await Task.Delay(1000);
        //            mwdDG1 = await localNT.readDataGroup1();

        //            verticalGxg45Box.BackColor = Color.Empty;
        //            verticalGyg45Box.BackColor = Color.Empty;
        //            verticalGzg45Box.BackColor = Color.Empty;
        //            verticalBxGauss45Box.BackColor = Color.Empty;
        //            verticalByGauss45Box.BackColor = Color.Empty;
        //            verticalBzGauss45Box.BackColor = Color.Empty;

        //            verticalGxg45Box.Text = String.Format("{0:0.0000}", mwdDG1.GxG);
        //            verticalGyg45Box.Text = String.Format("{0:0.0000}", mwdDG1.GyG);
        //            verticalGzg45Box.Text = String.Format("{0:0.0000}", mwdDG1.GzG);

        //            verticalBxGauss45Box.Text = String.Format("{0:0.0000}", mwdDG1.BxGauss);
        //            verticalByGauss45Box.Text = String.Format("{0:0.0000}", mwdDG1.ByGauss);
        //            verticalBzGauss45Box.Text = String.Format("{0:0.0000}", mwdDG1.BzGauss);

        //            verticalGxg45Box.BackColor = Color.Yellow;
        //            verticalGyg45Box.BackColor = Color.Yellow;
        //            verticalGzg45Box.BackColor = Color.Yellow;
        //            verticalBxGauss45Box.BackColor = Color.Yellow;
        //            verticalByGauss45Box.BackColor = Color.Yellow;
        //            verticalBzGauss45Box.BackColor = Color.Yellow;
        //        }
        //    }
        //    else if (verticalRoll45Test.Text == "STOP")
        //    {
        //        verticalRoll45Test.Text = "START";

        //        vertical45Data.GxG = mwdDG1.GxG;
        //        vertical45Data.GyG = mwdDG1.GyG;
        //        vertical45Data.GzG = mwdDG1.GzG;
        //        vertical45Data.BxGauss = mwdDG1.BxGauss;
        //        vertical45Data.ByGauss = mwdDG1.ByGauss;
        //        vertical45Data.BzGauss = mwdDG1.BzGauss;
        //        vertical45Data.AzimuthDegrees = mwdDG1.AzimuthDegrees;
        //        vertical45Data.InclinationDegrees = mwdDG1.InclinationDegrees;
        //        vertical45Data.GravityToolFaceDegrees = mwdDG1.GravityToolFaceDegrees;
        //        vertical45Data.MagneticToolFaceDegrees = mwdDG1.MagneticToolFaceDegrees;
        //    }
        //}

        //private async void verticalRoll90Test_Click(object sender, EventArgs e)
        //{
        //    if (verticalRoll90Test.Text == "START")
        //    {
        //        verticalRoll90Test.Text = "STOP";

        //        while (verticalRoll90Test.Text == "STOP")
        //        {
        //            await Task.Delay(1000);
        //            mwdDG1 = await localNT.readDataGroup1();

        //            verticalGxg90Box.BackColor = Color.Empty;
        //            verticalGyg90Box.BackColor = Color.Empty;
        //            verticalGzg90Box.BackColor = Color.Empty;
        //            verticalBxGauss90Box.BackColor = Color.Empty;
        //            verticalByGauss90Box.BackColor = Color.Empty;
        //            verticalBzGauss90Box.BackColor = Color.Empty;

        //            verticalGxg90Box.Text = String.Format("{0:0.0000}", mwdDG1.GxG);
        //            verticalGyg90Box.Text = String.Format("{0:0.0000}", mwdDG1.GyG);
        //            verticalGzg90Box.Text = String.Format("{0:0.0000}", mwdDG1.GzG);

        //            verticalBxGauss90Box.Text = String.Format("{0:0.0000}", mwdDG1.BxGauss);
        //            verticalByGauss90Box.Text = String.Format("{0:0.0000}", mwdDG1.ByGauss);
        //            verticalBzGauss90Box.Text = String.Format("{0:0.0000}", mwdDG1.BzGauss);

        //            verticalGxg90Box.BackColor = Color.Yellow;
        //            verticalGyg90Box.BackColor = Color.Yellow;
        //            verticalGzg90Box.BackColor = Color.Yellow;
        //            verticalBxGauss90Box.BackColor = Color.Yellow;
        //            verticalByGauss90Box.BackColor = Color.Yellow;
        //            verticalBzGauss90Box.BackColor = Color.Yellow;
        //        }
        //    }
        //    else if (verticalRoll90Test.Text == "STOP")
        //    {
        //        verticalRoll90Test.Text = "START";

        //        vertical90Data.GxG = mwdDG1.GxG;
        //        vertical90Data.GyG = mwdDG1.GyG;
        //        vertical90Data.GzG = mwdDG1.GzG;
        //        vertical90Data.BxGauss = mwdDG1.BxGauss;
        //        vertical90Data.ByGauss = mwdDG1.ByGauss;
        //        vertical90Data.BzGauss = mwdDG1.BzGauss;
        //        vertical90Data.AzimuthDegrees = mwdDG1.AzimuthDegrees;
        //        vertical90Data.InclinationDegrees = mwdDG1.InclinationDegrees;
        //        vertical90Data.GravityToolFaceDegrees = mwdDG1.GravityToolFaceDegrees;
        //        vertical90Data.MagneticToolFaceDegrees = mwdDG1.MagneticToolFaceDegrees;
        //    }
        //}

        //private async void verticalRoll135Test_Click(object sender, EventArgs e)
        //{
        //    if (verticalRoll135Test.Text == "START")
        //    {
        //        verticalRoll135Test.Text = "STOP";

        //        while (verticalRoll135Test.Text == "STOP")
        //        {
        //            await Task.Delay(1000);
        //            mwdDG1 = await localNT.readDataGroup1();

        //            verticalGxg135Box.BackColor = Color.Empty;
        //            verticalGyg135Box.BackColor = Color.Empty;
        //            verticalGzg135Box.BackColor = Color.Empty;
        //            verticalBxGauss135Box.BackColor = Color.Empty;
        //            verticalByGauss135Box.BackColor = Color.Empty;
        //            verticalBzGauss135Box.BackColor = Color.Empty;

        //            verticalGxg135Box.Text = String.Format("{0:0.0000}", mwdDG1.GxG);
        //            verticalGyg135Box.Text = String.Format("{0:0.0000}", mwdDG1.GyG);
        //            verticalGzg135Box.Text = String.Format("{0:0.0000}", mwdDG1.GzG);

        //            verticalBxGauss135Box.Text = String.Format("{0:0.0000}", mwdDG1.BxGauss);
        //            verticalByGauss135Box.Text = String.Format("{0:0.0000}", mwdDG1.ByGauss);
        //            verticalBzGauss135Box.Text = String.Format("{0:0.0000}", mwdDG1.BzGauss);

        //            verticalGxg135Box.BackColor = Color.Yellow;
        //            verticalGyg135Box.BackColor = Color.Yellow;
        //            verticalGzg135Box.BackColor = Color.Yellow;
        //            verticalBxGauss135Box.BackColor = Color.Yellow;
        //            verticalByGauss135Box.BackColor = Color.Yellow;
        //            verticalBzGauss135Box.BackColor = Color.Yellow;
        //        }
        //    }
        //    else if (verticalRoll135Test.Text == "STOP")
        //    {
        //        verticalRoll135Test.Text = "START";

        //        vertical135Data.GxG = mwdDG1.GxG;
        //        vertical135Data.GyG = mwdDG1.GyG;
        //        vertical135Data.GzG = mwdDG1.GzG;
        //        vertical135Data.BxGauss = mwdDG1.BxGauss;
        //        vertical135Data.ByGauss = mwdDG1.ByGauss;
        //        vertical135Data.BzGauss = mwdDG1.BzGauss;
        //        vertical135Data.AzimuthDegrees = mwdDG1.AzimuthDegrees;
        //        vertical135Data.InclinationDegrees = mwdDG1.InclinationDegrees;
        //        vertical135Data.GravityToolFaceDegrees = mwdDG1.GravityToolFaceDegrees;
        //        vertical135Data.MagneticToolFaceDegrees = mwdDG1.MagneticToolFaceDegrees;
        //    }
        //}

        //private async void verticalRoll180Test_Click(object sender, EventArgs e)
        //{
        //    if(verticalRoll180Test.Text == "START")
        //    {
        //        verticalRoll180Test.Text = "STOP";

        //        while (verticalRoll180Test.Text == "STOP")
        //        {
        //            await Task.Delay(1000);
        //            mwdDG1 = await localNT.readDataGroup1();

        //            verticalGxg180Box.BackColor = Color.Empty;
        //            verticalGyg180Box.BackColor = Color.Empty;
        //            verticalGzg180Box.BackColor = Color.Empty;
        //            verticalBxGauss180Box.BackColor = Color.Empty;
        //            verticalByGauss180Box.BackColor = Color.Empty;
        //            verticalBzGauss180Box.BackColor = Color.Empty;

        //            verticalGxg180Box.Text = String.Format("{0:0.0000}", mwdDG1.GxG);
        //            verticalGyg180Box.Text = String.Format("{0:0.0000}", mwdDG1.GyG);
        //            verticalGzg180Box.Text = String.Format("{0:0.0000}", mwdDG1.GzG);

        //            verticalBxGauss180Box.Text = String.Format("{0:0.0000}", mwdDG1.BxGauss);
        //            verticalByGauss180Box.Text = String.Format("{0:0.0000}", mwdDG1.ByGauss);
        //            verticalBzGauss180Box.Text = String.Format("{0:0.0000}", mwdDG1.BzGauss);

        //            verticalGxg180Box.BackColor = Color.Yellow;
        //            verticalGyg180Box.BackColor = Color.Yellow;
        //            verticalGzg180Box.BackColor = Color.Yellow;
        //            verticalBxGauss180Box.BackColor = Color.Yellow;
        //            verticalByGauss180Box.BackColor = Color.Yellow;
        //            verticalBzGauss180Box.BackColor = Color.Yellow;
        //        }
        //    }
        //    else if (verticalRoll180Test.Text == "STOP")
        //    {
        //        verticalRoll180Test.Text = "START";

        //        vertical180Data.GxG = mwdDG1.GxG;
        //        vertical180Data.GyG = mwdDG1.GyG;
        //        vertical180Data.GzG = mwdDG1.GzG;
        //        vertical180Data.BxGauss = mwdDG1.BxGauss;
        //        vertical180Data.ByGauss = mwdDG1.ByGauss;
        //        vertical180Data.BzGauss = mwdDG1.BzGauss;
        //        vertical180Data.AzimuthDegrees = mwdDG1.AzimuthDegrees;
        //        vertical180Data.InclinationDegrees = mwdDG1.InclinationDegrees;
        //        vertical180Data.GravityToolFaceDegrees = mwdDG1.GravityToolFaceDegrees;
        //        vertical180Data.MagneticToolFaceDegrees = mwdDG1.MagneticToolFaceDegrees;
        //    }
        //}

        //private async void verticalRoll225Test_Click(object sender, EventArgs e)
        //{
        //    if (verticalRoll225Test.Text == "START")
        //    {
        //        verticalRoll225Test.Text = "STOP";

        //        while (verticalRoll225Test.Text == "STOP")
        //        {
        //            await Task.Delay(1000);
        //            mwdDG1 = await localNT.readDataGroup1();

        //            verticalGxg225Box.BackColor = Color.Empty;
        //            verticalGyg225Box.BackColor = Color.Empty;
        //            verticalGzg225Box.BackColor = Color.Empty;
        //            verticalBxGauss225Box.BackColor = Color.Empty;
        //            verticalByGauss225Box.BackColor = Color.Empty;
        //            verticalBzGauss225Box.BackColor = Color.Empty;

        //            verticalGxg225Box.Text = String.Format("{0:0.0000}", mwdDG1.GxG);
        //            verticalGyg225Box.Text = String.Format("{0:0.0000}", mwdDG1.GyG);
        //            verticalGzg225Box.Text = String.Format("{0:0.0000}", mwdDG1.GzG);

        //            verticalBxGauss225Box.Text = String.Format("{0:0.0000}", mwdDG1.BxGauss);
        //            verticalByGauss225Box.Text = String.Format("{0:0.0000}", mwdDG1.ByGauss);
        //            verticalBzGauss225Box.Text = String.Format("{0:0.0000}", mwdDG1.BzGauss);

        //            verticalGxg225Box.BackColor = Color.Yellow;
        //            verticalGyg225Box.BackColor = Color.Yellow;
        //            verticalGzg225Box.BackColor = Color.Yellow;
        //            verticalBxGauss225Box.BackColor = Color.Yellow;
        //            verticalByGauss225Box.BackColor = Color.Yellow;
        //            verticalBzGauss225Box.BackColor = Color.Yellow;
        //        }
        //    }
        //    else if (verticalRoll225Test.Text == "STOP")
        //    {
        //        verticalRoll225Test.Text = "START";

        //        vertical225Data.GxG = mwdDG1.GxG;
        //        vertical225Data.GyG = mwdDG1.GyG;
        //        vertical225Data.GzG = mwdDG1.GzG;
        //        vertical225Data.BxGauss = mwdDG1.BxGauss;
        //        vertical225Data.ByGauss = mwdDG1.ByGauss;
        //        vertical225Data.BzGauss = mwdDG1.BzGauss;
        //        vertical225Data.AzimuthDegrees = mwdDG1.AzimuthDegrees;
        //        vertical225Data.InclinationDegrees = mwdDG1.InclinationDegrees;
        //        vertical225Data.GravityToolFaceDegrees = mwdDG1.GravityToolFaceDegrees;
        //        vertical225Data.MagneticToolFaceDegrees = mwdDG1.MagneticToolFaceDegrees;
        //    }
        //}

        //private async void verticalRoll270Test_Click(object sender, EventArgs e)
        //{
        //    if (verticalRoll270Test.Text == "START")
        //    {
        //        verticalRoll270Test.Text = "STOP";

        //        while (verticalRoll270Test.Text == "STOP")
        //        {
        //            await Task.Delay(1000);
        //            mwdDG1 = await localNT.readDataGroup1();

        //            verticalGxg270Box.BackColor = Color.Empty;
        //            verticalGyg270Box.BackColor = Color.Empty;
        //            verticalGzg270Box.BackColor = Color.Empty;
        //            verticalBxGauss270Box.BackColor = Color.Empty;
        //            verticalByGauss270Box.BackColor = Color.Empty;
        //            verticalBzGauss270Box.BackColor = Color.Empty;

        //            verticalGxg270Box.Text = String.Format("{0:0.0000}", mwdDG1.GxG);
        //            verticalGyg270Box.Text = String.Format("{0:0.0000}", mwdDG1.GyG);
        //            verticalGzg270Box.Text = String.Format("{0:0.0000}", mwdDG1.GzG);

        //            verticalBxGauss270Box.Text = String.Format("{0:0.0000}", mwdDG1.BxGauss);
        //            verticalByGauss270Box.Text = String.Format("{0:0.0000}", mwdDG1.ByGauss);
        //            verticalBzGauss270Box.Text = String.Format("{0:0.0000}", mwdDG1.BzGauss);

        //            verticalGxg270Box.BackColor = Color.Yellow;
        //            verticalGyg270Box.BackColor = Color.Yellow;
        //            verticalGzg270Box.BackColor = Color.Yellow;
        //            verticalBxGauss270Box.BackColor = Color.Yellow;
        //            verticalByGauss270Box.BackColor = Color.Yellow;
        //            verticalBzGauss270Box.BackColor = Color.Yellow;
        //        }
        //    }
        //    else if (verticalRoll270Test.Text == "STOP")
        //    {
        //        verticalRoll270Test.Text = "START";

        //        vertical270Data.GxG = mwdDG1.GxG;
        //        vertical270Data.GyG = mwdDG1.GyG;
        //        vertical270Data.GzG = mwdDG1.GzG;
        //        vertical270Data.BxGauss = mwdDG1.BxGauss;
        //        vertical270Data.ByGauss = mwdDG1.ByGauss;
        //        vertical270Data.BzGauss = mwdDG1.BzGauss;
        //        vertical270Data.AzimuthDegrees = mwdDG1.AzimuthDegrees;
        //        vertical270Data.InclinationDegrees = mwdDG1.InclinationDegrees;
        //        vertical270Data.GravityToolFaceDegrees = mwdDG1.GravityToolFaceDegrees;
        //        vertical270Data.MagneticToolFaceDegrees = mwdDG1.MagneticToolFaceDegrees;
        //    }
        //}

        //private async void verticalRoll315Box_Click(object sender, EventArgs e)
        //{
        //    if (verticalRoll315Test.Text == "START")
        //    {
        //        verticalRoll315Test.Text = "STOP";

        //        while (verticalRoll315Test.Text == "STOP")
        //        {
        //            await Task.Delay(1000);
        //            mwdDG1 = await localNT.readDataGroup1();

        //            verticalGxg315Box.BackColor = Color.Empty;
        //            verticalGyg315Box.BackColor = Color.Empty;
        //            verticalGzg315Box.BackColor = Color.Empty;
        //            verticalBxGauss315Box.BackColor = Color.Empty;
        //            verticalByGauss315Box.BackColor = Color.Empty;
        //            verticalBzGauss315Box.BackColor = Color.Empty;

        //            verticalGxg315Box.Text = String.Format("{0:0.0000}", mwdDG1.GxG);
        //            verticalGyg315Box.Text = String.Format("{0:0.0000}", mwdDG1.GyG);
        //            verticalGzg315Box.Text = String.Format("{0:0.0000}", mwdDG1.GzG);

        //            verticalBxGauss315Box.Text = String.Format("{0:0.0000}", mwdDG1.BxGauss);
        //            verticalByGauss315Box.Text = String.Format("{0:0.0000}", mwdDG1.ByGauss);
        //            verticalBzGauss315Box.Text = String.Format("{0:0.0000}", mwdDG1.BzGauss);

        //            verticalGxg315Box.BackColor = Color.Yellow;
        //            verticalGyg315Box.BackColor = Color.Yellow;
        //            verticalGzg315Box.BackColor = Color.Yellow;
        //            verticalBxGauss315Box.BackColor = Color.Yellow;
        //            verticalByGauss315Box.BackColor = Color.Yellow;
        //            verticalBzGauss315Box.BackColor = Color.Yellow;
        //        }
        //    }
        //    else if (verticalRoll315Test.Text == "STOP")
        //    {
        //        verticalRoll315Test.Text = "START";

        //        vertical315Data.GxG = mwdDG1.GxG;
        //        vertical315Data.GyG = mwdDG1.GyG;
        //        vertical315Data.GzG = mwdDG1.GzG;
        //        vertical315Data.BxGauss = mwdDG1.BxGauss;
        //        vertical315Data.ByGauss = mwdDG1.ByGauss;
        //        vertical315Data.BzGauss = mwdDG1.BzGauss;
        //        vertical315Data.AzimuthDegrees = mwdDG1.AzimuthDegrees;
        //        vertical315Data.InclinationDegrees = mwdDG1.InclinationDegrees;
        //        vertical315Data.GravityToolFaceDegrees = mwdDG1.GravityToolFaceDegrees;
        //        vertical315Data.MagneticToolFaceDegrees = mwdDG1.MagneticToolFaceDegrees;
        //    }
        //}

        //private async void verticalRoll360Test_Click(object sender, EventArgs e)
        //{
        //    if (verticalRoll360Test.Text == "START")
        //    {
        //        verticalRoll360Test.Text = "STOP";

        //        while (verticalRoll360Test.Text == "STOP")
        //        {
        //            await Task.Delay(1000);
        //            mwdDG1 = await localNT.readDataGroup1();

        //            verticalGxg360Box.BackColor = Color.Empty;
        //            verticalGyg360Box.BackColor = Color.Empty;
        //            verticalGzg360Box.BackColor = Color.Empty;
        //            verticalBxGauss360Box.BackColor = Color.Empty;
        //            verticalByGauss360Box.BackColor = Color.Empty;
        //            verticalBzGauss360Box.BackColor = Color.Empty;

        //            verticalGxg360Box.Text = String.Format("{0:0.0000}", mwdDG1.GxG);
        //            verticalGyg360Box.Text = String.Format("{0:0.0000}", mwdDG1.GyG);
        //            verticalGzg360Box.Text = String.Format("{0:0.0000}", mwdDG1.GzG);

        //            verticalBxGauss360Box.Text = String.Format("{0:0.0000}", mwdDG1.BxGauss);
        //            verticalByGauss360Box.Text = String.Format("{0:0.0000}", mwdDG1.ByGauss);
        //            verticalBzGauss360Box.Text = String.Format("{0:0.0000}", mwdDG1.BzGauss);

        //            verticalGxg360Box.BackColor = Color.Yellow;
        //            verticalGyg360Box.BackColor = Color.Yellow;
        //            verticalGzg360Box.BackColor = Color.Yellow;
        //            verticalBxGauss360Box.BackColor = Color.Yellow;
        //            verticalByGauss360Box.BackColor = Color.Yellow;
        //            verticalBzGauss360Box.BackColor = Color.Yellow;
        //        }
        //    }
        //    else if (verticalRoll360Test.Text == "STOP")
        //    {
        //        verticalRoll360Test.Text = "START";

        //        vertical360Data.GxG = mwdDG1.GxG;
        //        vertical360Data.GyG = mwdDG1.GyG;
        //        vertical360Data.GzG = mwdDG1.GzG;
        //        vertical360Data.BxGauss = mwdDG1.BxGauss;
        //        vertical360Data.ByGauss = mwdDG1.ByGauss;
        //        vertical360Data.BzGauss = mwdDG1.BzGauss;
        //        vertical360Data.AzimuthDegrees = mwdDG1.AzimuthDegrees;
        //        vertical360Data.InclinationDegrees = mwdDG1.InclinationDegrees;
        //        vertical360Data.GravityToolFaceDegrees = mwdDG1.GravityToolFaceDegrees;
        //        vertical360Data.MagneticToolFaceDegrees = mwdDG1.MagneticToolFaceDegrees;
        //    }
        //}
    }
}
