﻿namespace PC_PDD
{
    partial class RollTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RollTest));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxAZM = new System.Windows.Forms.TextBox();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.textBoxINC = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxGTF = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxMTF = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxDIP = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxTMP = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxTGF = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxTMF = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxTFO = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxBx = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textBoxBy = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textBoxBz = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.textBoxGx = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.textBoxGy = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBoxGz = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.buttonGetTFO = new System.Windows.Forms.Button();
            this.buttonResetTFO = new System.Windows.Forms.Button();
            this.checkBoxApplyOffset = new System.Windows.Forms.CheckBox();
            this.checkBoxRollType = new System.Windows.Forms.CheckBox();
            this.buttonStart = new System.Windows.Forms.Button();
            this.buttonNext = new System.Windows.Forms.Button();
            this.timerRollData = new System.Windows.Forms.Timer(this.components);
            this.labelPassFail = new System.Windows.Forms.Label();
            this.buttonPrint = new System.Windows.Forms.Button();
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.aGauge = new System.Windows.Forms.AGauge();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.Navy;
            this.dataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.EnableHeadersVisualStyles = false;
            this.dataGridView1.Location = new System.Drawing.Point(197, 88);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(2);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(651, 386);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 26);
            this.label1.TabIndex = 1;
            this.label1.Text = "AZM";
            // 
            // textBoxAZM
            // 
            this.textBoxAZM.BackColor = System.Drawing.Color.Black;
            this.textBoxAZM.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAZM.ForeColor = System.Drawing.Color.Yellow;
            this.textBoxAZM.Location = new System.Drawing.Point(69, 40);
            this.textBoxAZM.Name = "textBoxAZM";
            this.textBoxAZM.Size = new System.Drawing.Size(100, 35);
            this.textBoxAZM.TabIndex = 2;
            this.textBoxAZM.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // statusStrip
            // 
            this.statusStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2});
            this.statusStrip.Location = new System.Drawing.Point(0, 641);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(1008, 24);
            this.statusStrip.TabIndex = 3;
            this.statusStrip.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.AutoSize = false;
            this.toolStripStatusLabel1.BackColor = System.Drawing.Color.Black;
            this.toolStripStatusLabel1.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.toolStripStatusLabel1.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.toolStripStatusLabel1.ForeColor = System.Drawing.Color.Yellow;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(150, 19);
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.AutoSize = false;
            this.toolStripStatusLabel2.BackColor = System.Drawing.Color.Black;
            this.toolStripStatusLabel2.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.toolStripStatusLabel2.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.toolStripStatusLabel2.ForeColor = System.Drawing.Color.Yellow;
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(200, 19);
            this.toolStripStatusLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxINC
            // 
            this.textBoxINC.BackColor = System.Drawing.Color.Black;
            this.textBoxINC.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxINC.ForeColor = System.Drawing.Color.Yellow;
            this.textBoxINC.Location = new System.Drawing.Point(69, 97);
            this.textBoxINC.Name = "textBoxINC";
            this.textBoxINC.Size = new System.Drawing.Size(100, 35);
            this.textBoxINC.TabIndex = 5;
            this.textBoxINC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 102);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 26);
            this.label2.TabIndex = 4;
            this.label2.Text = "INC";
            // 
            // textBoxGTF
            // 
            this.textBoxGTF.BackColor = System.Drawing.Color.Black;
            this.textBoxGTF.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxGTF.ForeColor = System.Drawing.Color.Yellow;
            this.textBoxGTF.Location = new System.Drawing.Point(69, 154);
            this.textBoxGTF.Name = "textBoxGTF";
            this.textBoxGTF.Size = new System.Drawing.Size(100, 35);
            this.textBoxGTF.TabIndex = 7;
            this.textBoxGTF.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 159);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 26);
            this.label3.TabIndex = 6;
            this.label3.Text = "GTF";
            // 
            // textBoxMTF
            // 
            this.textBoxMTF.BackColor = System.Drawing.Color.Black;
            this.textBoxMTF.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxMTF.ForeColor = System.Drawing.Color.Yellow;
            this.textBoxMTF.Location = new System.Drawing.Point(69, 211);
            this.textBoxMTF.Name = "textBoxMTF";
            this.textBoxMTF.Size = new System.Drawing.Size(100, 35);
            this.textBoxMTF.TabIndex = 9;
            this.textBoxMTF.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 216);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 26);
            this.label4.TabIndex = 8;
            this.label4.Text = "MTF";
            // 
            // textBoxDIP
            // 
            this.textBoxDIP.BackColor = System.Drawing.Color.Black;
            this.textBoxDIP.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxDIP.ForeColor = System.Drawing.Color.Yellow;
            this.textBoxDIP.Location = new System.Drawing.Point(69, 268);
            this.textBoxDIP.Name = "textBoxDIP";
            this.textBoxDIP.Size = new System.Drawing.Size(100, 35);
            this.textBoxDIP.TabIndex = 11;
            this.textBoxDIP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 273);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 26);
            this.label5.TabIndex = 10;
            this.label5.Text = "DIP";
            // 
            // textBoxTMP
            // 
            this.textBoxTMP.BackColor = System.Drawing.Color.Black;
            this.textBoxTMP.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxTMP.ForeColor = System.Drawing.Color.Yellow;
            this.textBoxTMP.Location = new System.Drawing.Point(69, 325);
            this.textBoxTMP.Name = "textBoxTMP";
            this.textBoxTMP.Size = new System.Drawing.Size(100, 35);
            this.textBoxTMP.TabIndex = 13;
            this.textBoxTMP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(12, 330);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 26);
            this.label6.TabIndex = 12;
            this.label6.Text = "TMP";
            // 
            // textBoxTGF
            // 
            this.textBoxTGF.BackColor = System.Drawing.Color.Black;
            this.textBoxTGF.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxTGF.ForeColor = System.Drawing.Color.Yellow;
            this.textBoxTGF.Location = new System.Drawing.Point(69, 382);
            this.textBoxTGF.Name = "textBoxTGF";
            this.textBoxTGF.Size = new System.Drawing.Size(100, 35);
            this.textBoxTGF.TabIndex = 15;
            this.textBoxTGF.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(12, 387);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 26);
            this.label7.TabIndex = 14;
            this.label7.Text = "TGF";
            // 
            // textBoxTMF
            // 
            this.textBoxTMF.BackColor = System.Drawing.Color.Black;
            this.textBoxTMF.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxTMF.ForeColor = System.Drawing.Color.Yellow;
            this.textBoxTMF.Location = new System.Drawing.Point(69, 439);
            this.textBoxTMF.Name = "textBoxTMF";
            this.textBoxTMF.Size = new System.Drawing.Size(100, 35);
            this.textBoxTMF.TabIndex = 17;
            this.textBoxTMF.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(12, 444);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(55, 26);
            this.label8.TabIndex = 16;
            this.label8.Text = "TMF";
            // 
            // textBoxTFO
            // 
            this.textBoxTFO.BackColor = System.Drawing.Color.Black;
            this.textBoxTFO.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxTFO.ForeColor = System.Drawing.Color.Yellow;
            this.textBoxTFO.Location = new System.Drawing.Point(69, 516);
            this.textBoxTFO.Name = "textBoxTFO";
            this.textBoxTFO.Size = new System.Drawing.Size(100, 35);
            this.textBoxTFO.TabIndex = 19;
            this.textBoxTFO.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(12, 519);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 26);
            this.label9.TabIndex = 18;
            this.label9.Text = "TFO";
            // 
            // textBoxBx
            // 
            this.textBoxBx.BackColor = System.Drawing.Color.Black;
            this.textBoxBx.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxBx.ForeColor = System.Drawing.Color.Yellow;
            this.textBoxBx.Location = new System.Drawing.Point(896, 40);
            this.textBoxBx.Name = "textBoxBx";
            this.textBoxBx.Size = new System.Drawing.Size(100, 35);
            this.textBoxBx.TabIndex = 21;
            this.textBoxBx.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(856, 46);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(38, 26);
            this.label10.TabIndex = 20;
            this.label10.Text = "Bx";
            // 
            // textBoxBy
            // 
            this.textBoxBy.BackColor = System.Drawing.Color.Black;
            this.textBoxBy.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxBy.ForeColor = System.Drawing.Color.Yellow;
            this.textBoxBy.Location = new System.Drawing.Point(896, 97);
            this.textBoxBy.Name = "textBoxBy";
            this.textBoxBy.Size = new System.Drawing.Size(100, 35);
            this.textBoxBy.TabIndex = 23;
            this.textBoxBy.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(856, 102);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(38, 26);
            this.label11.TabIndex = 22;
            this.label11.Text = "By";
            // 
            // textBoxBz
            // 
            this.textBoxBz.BackColor = System.Drawing.Color.Black;
            this.textBoxBz.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxBz.ForeColor = System.Drawing.Color.Yellow;
            this.textBoxBz.Location = new System.Drawing.Point(896, 154);
            this.textBoxBz.Name = "textBoxBz";
            this.textBoxBz.Size = new System.Drawing.Size(100, 35);
            this.textBoxBz.TabIndex = 25;
            this.textBoxBz.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(856, 158);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(38, 26);
            this.label12.TabIndex = 24;
            this.label12.Text = "Bz";
            // 
            // textBoxGx
            // 
            this.textBoxGx.BackColor = System.Drawing.Color.Black;
            this.textBoxGx.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxGx.ForeColor = System.Drawing.Color.Yellow;
            this.textBoxGx.Location = new System.Drawing.Point(896, 211);
            this.textBoxGx.Name = "textBoxGx";
            this.textBoxGx.Size = new System.Drawing.Size(100, 35);
            this.textBoxGx.TabIndex = 27;
            this.textBoxGx.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(853, 214);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(40, 26);
            this.label13.TabIndex = 26;
            this.label13.Text = "Gx";
            // 
            // textBoxGy
            // 
            this.textBoxGy.BackColor = System.Drawing.Color.Black;
            this.textBoxGy.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxGy.ForeColor = System.Drawing.Color.Yellow;
            this.textBoxGy.Location = new System.Drawing.Point(896, 268);
            this.textBoxGy.Name = "textBoxGy";
            this.textBoxGy.Size = new System.Drawing.Size(100, 35);
            this.textBoxGy.TabIndex = 29;
            this.textBoxGy.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(853, 270);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(40, 26);
            this.label14.TabIndex = 28;
            this.label14.Text = "Gy";
            // 
            // textBoxGz
            // 
            this.textBoxGz.BackColor = System.Drawing.Color.Black;
            this.textBoxGz.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxGz.ForeColor = System.Drawing.Color.Yellow;
            this.textBoxGz.Location = new System.Drawing.Point(896, 325);
            this.textBoxGz.Name = "textBoxGz";
            this.textBoxGz.Size = new System.Drawing.Size(100, 35);
            this.textBoxGz.TabIndex = 31;
            this.textBoxGz.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(853, 326);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(40, 26);
            this.label15.TabIndex = 30;
            this.label15.Text = "Gz";
            // 
            // buttonGetTFO
            // 
            this.buttonGetTFO.Location = new System.Drawing.Point(69, 557);
            this.buttonGetTFO.Name = "buttonGetTFO";
            this.buttonGetTFO.Size = new System.Drawing.Size(100, 24);
            this.buttonGetTFO.TabIndex = 32;
            this.buttonGetTFO.Text = "Get TFO";
            this.buttonGetTFO.UseVisualStyleBackColor = true;
            this.buttonGetTFO.Click += new System.EventHandler(this.ButtonGetTFO_Click);
            // 
            // buttonResetTFO
            // 
            this.buttonResetTFO.Location = new System.Drawing.Point(69, 587);
            this.buttonResetTFO.Name = "buttonResetTFO";
            this.buttonResetTFO.Size = new System.Drawing.Size(100, 24);
            this.buttonResetTFO.TabIndex = 33;
            this.buttonResetTFO.Text = "Reset TFO";
            this.buttonResetTFO.UseVisualStyleBackColor = true;
            this.buttonResetTFO.Click += new System.EventHandler(this.ButtonResetTFO_Click);
            // 
            // checkBoxApplyOffset
            // 
            this.checkBoxApplyOffset.AutoSize = true;
            this.checkBoxApplyOffset.Location = new System.Drawing.Point(868, 382);
            this.checkBoxApplyOffset.Name = "checkBoxApplyOffset";
            this.checkBoxApplyOffset.Size = new System.Drawing.Size(128, 17);
            this.checkBoxApplyOffset.TabIndex = 35;
            this.checkBoxApplyOffset.Text = "Apply Toolface Offset";
            this.checkBoxApplyOffset.UseVisualStyleBackColor = true;
            // 
            // checkBoxRollType
            // 
            this.checkBoxRollType.AutoSize = true;
            this.checkBoxRollType.Location = new System.Drawing.Point(868, 405);
            this.checkBoxRollType.Name = "checkBoxRollType";
            this.checkBoxRollType.Size = new System.Drawing.Size(86, 17);
            this.checkBoxRollType.TabIndex = 36;
            this.checkBoxRollType.Text = "4 - Point Roll";
            this.checkBoxRollType.UseVisualStyleBackColor = true;
            // 
            // buttonStart
            // 
            this.buttonStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonStart.Location = new System.Drawing.Point(876, 439);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(111, 35);
            this.buttonStart.TabIndex = 37;
            this.buttonStart.Text = "Start";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.ButtonStart_Click);
            // 
            // buttonNext
            // 
            this.buttonNext.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNext.Location = new System.Drawing.Point(876, 489);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(111, 35);
            this.buttonNext.TabIndex = 38;
            this.buttonNext.Text = "Next";
            this.buttonNext.UseVisualStyleBackColor = true;
            this.buttonNext.Click += new System.EventHandler(this.ButtonNext_Click);
            // 
            // timerRollData
            // 
            this.timerRollData.Interval = 1000;
            this.timerRollData.Tick += new System.EventHandler(this.TimerRollData_Tick);
            // 
            // labelPassFail
            // 
            this.labelPassFail.BackColor = System.Drawing.Color.LightGreen;
            this.labelPassFail.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPassFail.ForeColor = System.Drawing.Color.White;
            this.labelPassFail.Location = new System.Drawing.Point(880, 587);
            this.labelPassFail.Name = "labelPassFail";
            this.labelPassFail.Size = new System.Drawing.Size(107, 40);
            this.labelPassFail.TabIndex = 39;
            this.labelPassFail.Text = "Pass";
            this.labelPassFail.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonPrint
            // 
            this.buttonPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPrint.Location = new System.Drawing.Point(876, 539);
            this.buttonPrint.Name = "buttonPrint";
            this.buttonPrint.Size = new System.Drawing.Size(111, 35);
            this.buttonPrint.TabIndex = 41;
            this.buttonPrint.Text = "Print";
            this.buttonPrint.UseVisualStyleBackColor = true;
            this.buttonPrint.Click += new System.EventHandler(this.ButtonPrint_Click);
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.ShowIcon = false;
            this.printPreviewDialog1.Visible = false;
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.PrintDocument_PrintPage);
            // 
            // aGauge
            // 
            this.aGauge.BackColor = System.Drawing.Color.Black;
            this.aGauge.BaseArcColor = System.Drawing.Color.White;
            this.aGauge.BaseArcRadius = 240;
            this.aGauge.BaseArcStart = 270;
            this.aGauge.BaseArcSweep = 360;
            this.aGauge.BaseArcWidth = 2;
            this.aGauge.Center = new System.Drawing.Point(305, 300);
            this.aGauge.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.aGauge.Location = new System.Drawing.Point(225, 12);
            this.aGauge.MaxValue = 359F;
            this.aGauge.MinValue = 0F;
            this.aGauge.Name = "aGauge";
            this.aGauge.NeedleColor1 = System.Windows.Forms.AGaugeNeedleColor.Yellow;
            this.aGauge.NeedleColor2 = System.Drawing.Color.White;
            this.aGauge.NeedleRadius = 225;
            this.aGauge.NeedleType = System.Windows.Forms.NeedleType.Advance;
            this.aGauge.NeedleWidth = 5;
            this.aGauge.ScaleLinesInterColor = System.Drawing.Color.White;
            this.aGauge.ScaleLinesInterInnerRadius = 240;
            this.aGauge.ScaleLinesInterOuterRadius = 250;
            this.aGauge.ScaleLinesInterWidth = 1;
            this.aGauge.ScaleLinesMajorColor = System.Drawing.Color.Yellow;
            this.aGauge.ScaleLinesMajorInnerRadius = 230;
            this.aGauge.ScaleLinesMajorOuterRadius = 250;
            this.aGauge.ScaleLinesMajorStepValue = 45F;
            this.aGauge.ScaleLinesMajorWidth = 4;
            this.aGauge.ScaleLinesMinorColor = System.Drawing.Color.White;
            this.aGauge.ScaleLinesMinorInnerRadius = 240;
            this.aGauge.ScaleLinesMinorOuterRadius = 250;
            this.aGauge.ScaleLinesMinorTicks = 9;
            this.aGauge.ScaleLinesMinorWidth = 1;
            this.aGauge.ScaleNumbersColor = System.Drawing.Color.Yellow;
            this.aGauge.ScaleNumbersFormat = null;
            this.aGauge.ScaleNumbersRadius = 275;
            this.aGauge.ScaleNumbersRotation = 0;
            this.aGauge.ScaleNumbersStartScaleLine = 0;
            this.aGauge.ScaleNumbersStepScaleLines = 1;
            this.aGauge.Size = new System.Drawing.Size(600, 601);
            this.aGauge.TabIndex = 40;
            this.aGauge.Text = "aGauge1";
            this.aGauge.Value = 0F;
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // RollTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 665);
            this.Controls.Add(this.buttonPrint);
            this.Controls.Add(this.aGauge);
            this.Controls.Add(this.labelPassFail);
            this.Controls.Add(this.buttonNext);
            this.Controls.Add(this.buttonStart);
            this.Controls.Add(this.checkBoxRollType);
            this.Controls.Add(this.checkBoxApplyOffset);
            this.Controls.Add(this.buttonResetTFO);
            this.Controls.Add(this.buttonGetTFO);
            this.Controls.Add(this.textBoxGz);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.textBoxGy);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.textBoxGx);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.textBoxBz);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.textBoxBy);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.textBoxBx);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.textBoxTFO);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.textBoxTMF);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.textBoxTGF);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.textBoxTMP);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBoxDIP);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBoxMTF);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBoxGTF);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxINC);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.textBoxAZM);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RollTest";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RollTest";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.RollTest_FormClosing);
            this.Load += new System.EventHandler(this.RollTest_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxAZM;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.TextBox textBoxINC;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxGTF;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxMTF;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxDIP;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxTMP;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxTGF;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxTMF;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxTFO;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxBx;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBoxBy;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBoxBz;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBoxGx;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBoxGy;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBoxGz;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button buttonGetTFO;
        private System.Windows.Forms.Button buttonResetTFO;
        private System.Windows.Forms.CheckBox checkBoxApplyOffset;
        private System.Windows.Forms.CheckBox checkBoxRollType;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.Button buttonNext;
        private System.Windows.Forms.Timer timerRollData;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.Label labelPassFail;
        private System.Windows.Forms.AGauge aGauge;
        private System.Windows.Forms.Button buttonPrint;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.PrintDialog printDialog1;
    }
}