﻿using Microsoft.VisualBasic;
using NodeCommon;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows.Forms;
using PC_PDD;
using Wolverine_Field_Applications;

namespace PC_PDD
{
    public partial class RollTest : Form
    {
        DataSet dSet = new DataSet();
        CommandProcessor cp = new CommandProcessor();
        ToolComms tc;
        StructureVariables sV = new StructureVariables();
        StructureVariables.MWD_MASTER_CONFIGURATION_STRUCTURE cfg = new StructureVariables.MWD_MASTER_CONFIGURATION_STRUCTURE();   // Configuration structure
        Wolverine_Field_Applications.MainForm localNT = new Wolverine_Field_Applications.MainForm();
        SerialPort _serialPort;
        deserializeStruct conv = new deserializeStruct();
        MessageHandler MH = new MessageHandler();
        rollTestForm rtf;

        float toolFaceOffset = 0F;
        string operatorName;
        string serialNumber;
        int currentPosition = 0;
        bool rollTestComplete = false;
        bool failFlag = false;

        #region Variables
        // Roll test variables
        int[] position = new int[20];
        float[] AZI = new float[20];
        float[] INC = new float[20];
        float[] GTF = new float[20];
        float[] MTF = new float[20];
        float[] TGF = new float[20];
        float[] TMF = new float[20];
        float[] DIP = new float[20];
        float[] TMP = new float[20];

        float AZIAverage;
        float AZImax;
        float AZImin;
        float INCAverage;
        float INCmax;
        float INCmin;
        float TGFAverage;
        float TGFmax;
        float TGFmin;
        float TMFAverage;
        float TMFmax;
        float TMFmin;
        float DIPAverage;
        float DIPmax;
        float DIPmin;
        float TMPAverage;
        float TMPmax;
        float TMPmin;
        float AZISpread;
        float INCSpread;
        float TGFSpread;
        float TMFSpread;
        float DIPSpread;
        float TMPSpread;
        readonly float AZIlimit = .5F;
        readonly float INClimit = .1F;
        readonly float TGFlimit = .003F;
        readonly float TMFlimit = .01F;
        readonly float DIPlimit = .5F;
        readonly float TMPlimit = .5F;

        public FTD2XX_NET.FTDI localMyFtdiDevice = new FTD2XX_NET.FTDI();
        public FTD2XX_NET.FTDI.FT_STATUS localFtStatus = FTD2XX_NET.FTDI.FT_STATUS.FT_OK;
        StructureVariables.MWD_MASTER_TFO_STRUCTURE masterTFO = new StructureVariables.MWD_MASTER_TFO_STRUCTURE();
        #endregion

        public RollTest(SerialPort serialPort, ToolComms toolComms, string SerialNumber, Wolverine_Field_Applications.MainForm NT)
        {
            InitializeComponent();

            localNT = NT;
            serialNumber = SerialNumber;
            _serialPort = serialPort;
            tc = toolComms;
            setupDataGridView();
            this.FormClosing += new FormClosingEventHandler(Form_Closing);
        }

        public RollTest(FTD2XX_NET.FTDI myFtdiDevice, FTD2XX_NET.FTDI.FT_STATUS ftStatus, ToolComms toolComms, string SerialNumber, Wolverine_Field_Applications.MainForm NT)
        {
            InitializeComponent();

            localNT = NT;
            serialNumber = SerialNumber;
            localMyFtdiDevice = myFtdiDevice;
            localFtStatus = ftStatus;
            tc = toolComms;
            setupDataGridView();
            this.FormClosing += new FormClosingEventHandler(Form_Closing);
        }

        private void Form_Closing(object sender, EventArgs e)
        {
            timerRollData.Enabled = false;
            this.Dispose();
        }

        private async void RollTest_Load(object sender, EventArgs e)
        {
            this.ActiveControl = buttonStart;
            buttonNext.Enabled = false;
            labelPassFail.Visible = false;
            timerRollData.Enabled = false;

            #region Read Config
            // Read the tool configuration structure
            var message = cp.buildMessage(Common.NODE_MASTER, Common.CMD_NONVOLATILE_RDWR, Common.MWD_MASTER_NONVOLATILE_CONFIGURATION);

            if (!localMyFtdiDevice.IsOpen)
                return;

            if (await tc.FTDI_SEND(message, localMyFtdiDevice, localFtStatus, localNT))
            {
                if (MH.checkSumVerify(tc.rxNoEcho.ToArray(), Common.MESSAGE_OVERHEAD + Marshal.SizeOf<StructureVariables.MWD_MASTER_CONFIGURATION_STRUCTURE>() + 1))
                {
                    try
                    {
                        sV.MasterConfiguration = conv.FromByteArray<StructureVariables.MWD_MASTER_CONFIGURATION_STRUCTURE>(tc.rxData.ToArray());
                        ushort CheckSum = MH.calculateChecksum(conv.ToByteArray<StructureVariables.MWD_MASTER_CONFIGURATION_STRUCTURE>(sV.MasterConfiguration));
                    }
                    catch (Exception ex)
                    {
                        //ErrorDisplay ED = new ErrorDisplay();
                        //ED.StartPosition = FormStartPosition.CenterParent;
                        //ED.Show(this);
                        //ED.setMessage(ex.Message);
                    }
                }
                cfg = sV.MasterConfiguration;
            }
            else
            {
                toolStripStatusLabel1.Text = "Read Config Failed";
                MessageBox.Show("Read configuration Failed");
            }

            message = cp.buildMessage(Common.NODE_MASTER, Common.CMD_NONVOLATILE_RDWR, Common.MWD_MASTER_NONVOLATILE_TFO);

            if (!localMyFtdiDevice.IsOpen)
                return;

            if (await tc.FTDI_SEND(message, localMyFtdiDevice, localFtStatus, localNT))
            {
                // Update the cfg structure with the rxData
                await Task.Delay(100);
                if (MH.checkSumVerify(tc.rxNoEcho.ToArray(), Common.MESSAGE_OVERHEAD + Marshal.SizeOf<StructureVariables.MWD_MASTER_TFO_STRUCTURE>() + 1))
                {
                    try
                    {
                        masterTFO = conv.FromByteArray<StructureVariables.MWD_MASTER_TFO_STRUCTURE>(tc.rxData.ToArray());
                        toolFaceOffset = masterTFO.ToolfaceOffsetDegrees;
                    }
                    catch (Exception ex)
                    {
                        //ErrorDisplay ED = new ErrorDisplay();
                        //ED.StartPosition = FormStartPosition.CenterParent;
                        //ED.Show(this);
                        //ED.setMessage(ex.Message);
                    }
                }
            }
            #endregion

            #region Mode Write Roll Test
            // Set the tool to roll test mode
            message = cp.buildMessage(Common.NODE_MASTER, Common.CMD_MODE_RDWR, Common.MODE_ROLLTEST);

            if (!localMyFtdiDevice.IsOpen)
                return;

            if (await tc.FTDI_SEND(message, localMyFtdiDevice, localFtStatus, localNT))
            {
                toolStripStatusLabel1.Text = "Mode = ROLLTEST";
                timerRollData.Enabled = true;
                //timerDG1.Enabled = true;
            }
            else
            {
                MessageBox.Show("Set Mode RollTest Failed", "Error", MessageBoxButtons.OK);
                this.Dispose();
            }
            #endregion
        }

        private void RollTest_FormClosing(object sender, FormClosingEventArgs e)
        {
            timerRollData.Enabled = false;
            this.Dispose();
        }

        private async void TimerRollData_Tick(object sender, EventArgs e)
        {
            // Build the message to read MasterDatagroup1, send it to the tool, and then display the reply
            var message = cp.buildMessage(Common.NODE_MASTER, Common.CMD_DATAGROUP_RDWR, Common.DATAGROUP_1);

            if (!localMyFtdiDevice.IsOpen)
                return;

            if (await tc.FTDI_SEND(message, localMyFtdiDevice, localFtStatus, localNT))
            {
                //User Requesting DG1 Information of Master
                //if (MH.checkSumVerify(tc.rxNoEcho.ToArray(), Common.MESSAGE_OVERHEAD + Marshal.SizeOf<StructureVariables.MWD_MASTER_DATAGROUP_1_STRUCTURE>() + 1))
                if (MH.checkSumVerify(tc.rxNoEcho.ToArray()))
                {
                    //Log(global::NeutrinoTracker.Log.Outgoing, "CheckSum PASS", 2);

                    try
                    {
                        // Update the sys structure with the new rxData
                        sV.MasterDataGroup1 = conv.FromByteArray<StructureVariables.MWD_MASTER_DATAGROUP_1_STRUCTURE>(tc.rxData.ToArray());
                        if (rtf != null)
                        {
                            rtf.setDataGroup1(sV.MasterDataGroup1);
                            rtf.RunStateMachine();
                        }
                        updateDisplay();
                    }
                    catch
                    {
                        //Error Deserializing
                    }
                }
            }
            else
            {
                //timerRollData.Enabled = false;
                //updateDisplay();
            }
        }

        private void updateDisplay()
        {
            toolFaceOffset = masterTFO.ToolfaceOffsetDegrees;

            textBoxAZM.Text = String.Format("{0:0.0}", sV.MasterDataGroup1.AzimuthDegrees);
            textBoxINC.Text = String.Format("{0:0.0}", sV.MasterDataGroup1.InclinationDegrees);
            if (checkBoxApplyOffset.Checked)
            {
                var newToolface = sV.MasterDataGroup1.GravityToolFaceDegrees - toolFaceOffset;
                if (newToolface < 0) newToolface += 360;
                if (newToolface >= 360) newToolface -= 360;
                if (newToolface > 359.9) newToolface = 0;
                textBoxGTF.Text = String.Format("{0:0.0}", newToolface);
                aGauge.Value = newToolface;
                aGauge.GaugeRanges.Add(new AGaugeRange(Color.Navy, toolFaceOffset - 2, toolFaceOffset + 2, 150, 250));
            }
            else
            {
                aGauge.GaugeRanges.Add(new AGaugeRange(Color.Black, toolFaceOffset - 2, toolFaceOffset + 2, 150, 230));
                textBoxGTF.Text = String.Format("{0:0.0}", sV.MasterDataGroup1.GravityToolFaceDegrees);
                aGauge.Value = sV.MasterDataGroup1.GravityToolFaceDegrees;
            }
            textBoxMTF.Text = String.Format("{0:0.0}", sV.MasterDataGroup1.MagneticToolFaceDegrees);
            textBoxDIP.Text = String.Format("{0:0.0}", sV.MasterDataGroup1.MagneticDipDegrees);
            textBoxTMP.Text = String.Format("{0:0.00}", sV.MasterDataGroup1.TemperatureCelsius);
            textBoxTGF.Text = String.Format("{0:0.000}", sV.MasterDataGroup1.TotalGravityFieldG);
            textBoxTMF.Text = String.Format("{0:0.000}", sV.MasterDataGroup1.TotalMagneticFieldGauss);

            textBoxTFO.Text = String.Format("{0:0.0}", toolFaceOffset);

            textBoxBx.Text = String.Format("{0:0.000}", sV.MasterDataGroup1.BxGauss);
            textBoxBy.Text = String.Format("{0:0.000}", sV.MasterDataGroup1.ByGauss);
            textBoxBz.Text = String.Format("{0:0.000}", sV.MasterDataGroup1.BzGauss);
            textBoxGx.Text = String.Format("{0:0.000}", sV.MasterDataGroup1.GxG);
            textBoxGy.Text = String.Format("{0:0.000}", sV.MasterDataGroup1.GyG);
            textBoxGz.Text = String.Format("{0:0.000}", sV.MasterDataGroup1.GzG);

            toolStripStatusLabel2.Text = String.Format("Serial Number = {0}", serialNumber);
        }

        private void setupDataGridView()
        {
            this.Controls.Add(dataGridView1);

            dataGridView1.ColumnCount = 9;
            dataGridView1.Name = "dataGridView1";

            foreach (DataGridViewColumn column in dataGridView1.Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = Color.Black;
            dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor = Color.Yellow;
            dataGridView1.ColumnHeadersDefaultCellStyle.SelectionBackColor = Color.Black;
            dataGridView1.ColumnHeadersDefaultCellStyle.SelectionForeColor = Color.Yellow;
            dataGridView1.ColumnHeadersDefaultCellStyle.Font = new Font(dataGridView1.Font, FontStyle.Bold);
            dataGridView1.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 22F, GraphicsUnit.Pixel);


            dataGridView1.Columns[0].DefaultCellStyle.BackColor = Color.Black;
            dataGridView1.Columns[0].DefaultCellStyle.ForeColor = Color.Yellow;
            dataGridView1.Columns[0].DefaultCellStyle.SelectionBackColor = Color.Black;
            dataGridView1.Columns[0].DefaultCellStyle.SelectionForeColor = Color.Yellow;

            dataGridView1.DefaultCellStyle.Font = new Font("Arial", 12F, GraphicsUnit.Pixel);
            dataGridView1.DefaultCellStyle.Font = new Font(dataGridView1.Font, FontStyle.Bold);
            dataGridView1.DefaultCellStyle.BackColor = Color.Blue;
            dataGridView1.DefaultCellStyle.ForeColor = Color.White;
            dataGridView1.DefaultCellStyle.SelectionBackColor = Color.Blue;
            dataGridView1.DefaultCellStyle.SelectionForeColor = Color.White;

            dataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedCells;
            dataGridView1.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
            dataGridView1.CellBorderStyle = DataGridViewCellBorderStyle.Raised;
            dataGridView1.GridColor = Color.Black;
            dataGridView1.RowHeadersVisible = false;

            dataGridView1.Columns[0].Name = "Pos";
            dataGridView1.Columns[1].Name = "AZI";
            dataGridView1.Columns[2].Name = "INC";
            dataGridView1.Columns[3].Name = "GTF";
            dataGridView1.Columns[4].Name = "MTF";
            dataGridView1.Columns[5].Name = "TGF";
            dataGridView1.Columns[6].Name = "TMF";
            dataGridView1.Columns[7].Name = "DIP";
            dataGridView1.Columns[8].Name = "TEMP";

            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.MultiSelect = false;
            dataGridView1.ClearSelection();
        }

        private void populateDataGridView()
        {
            failFlag = false;
            labelPassFail.BackColor = Color.DarkGreen;
            labelPassFail.Text = "Pass";

            // Roll Values collected
            string[] row0 = { position[0].ToString(), AZI[0].ToString(), INC[0].ToString(), GTF[0].ToString(), MTF[0].ToString(), TGF[0].ToString(), TMF[0].ToString(), DIP[0].ToString(), TMP[0].ToString() };
            string[] row1 = { position[1].ToString(), AZI[1].ToString(), INC[1].ToString(), GTF[1].ToString(), MTF[1].ToString(), TGF[1].ToString(), TMF[1].ToString(), DIP[1].ToString(), TMP[1].ToString() };
            string[] row2 = { position[2].ToString(), AZI[2].ToString(), INC[2].ToString(), GTF[2].ToString(), MTF[2].ToString(), TGF[2].ToString(), TMF[2].ToString(), DIP[2].ToString(), TMP[2].ToString() };
            string[] row3 = { position[3].ToString(), AZI[3].ToString(), INC[3].ToString(), GTF[3].ToString(), MTF[3].ToString(), TGF[3].ToString(), TMF[3].ToString(), DIP[3].ToString(), TMP[3].ToString() };
            string[] row4 = { position[4].ToString(), AZI[4].ToString(), INC[4].ToString(), GTF[4].ToString(), MTF[4].ToString(), TGF[4].ToString(), TMF[4].ToString(), DIP[4].ToString(), TMP[4].ToString() };
            string[] row5 = { position[5].ToString(), AZI[5].ToString(), INC[5].ToString(), GTF[5].ToString(), MTF[5].ToString(), TGF[5].ToString(), TMF[5].ToString(), DIP[5].ToString(), TMP[5].ToString() };
            string[] row6 = { position[6].ToString(), AZI[6].ToString(), INC[6].ToString(), GTF[6].ToString(), MTF[6].ToString(), TGF[6].ToString(), TMF[6].ToString(), DIP[6].ToString(), TMP[6].ToString() };
            string[] row7 = { position[7].ToString(), AZI[7].ToString(), INC[7].ToString(), GTF[7].ToString(), MTF[7].ToString(), TGF[7].ToString(), TMF[7].ToString(), DIP[7].ToString(), TMP[7].ToString() };
            string[] row8 = { position[8].ToString(), AZI[8].ToString(), INC[8].ToString(), GTF[8].ToString(), MTF[8].ToString(), TGF[8].ToString(), TMF[8].ToString(), DIP[8].ToString(), TMP[8].ToString() };
            string[] row9 = { position[9].ToString(), AZI[9].ToString(), INC[9].ToString(), GTF[9].ToString(), MTF[9].ToString(), TGF[9].ToString(), TMF[9].ToString(), DIP[9].ToString(), TMP[9].ToString() };
            string[] row10 = { position[10].ToString(), AZI[10].ToString(), INC[10].ToString(), GTF[10].ToString(), MTF[10].ToString(), TGF[10].ToString(), TMF[10].ToString(), DIP[10].ToString(), TMP[10].ToString() };
            string[] row11 = { position[11].ToString(), AZI[11].ToString(), INC[11].ToString(), GTF[11].ToString(), MTF[11].ToString(), TGF[11].ToString(), TMF[11].ToString(), DIP[11].ToString(), TMP[11].ToString() };
            string[] row12 = { position[12].ToString(), AZI[12].ToString(), INC[12].ToString(), GTF[12].ToString(), MTF[12].ToString(), TGF[12].ToString(), TMF[12].ToString(), DIP[12].ToString(), TMP[12].ToString() };
            string[] row13 = { position[13].ToString(), AZI[13].ToString(), INC[13].ToString(), GTF[13].ToString(), MTF[13].ToString(), TGF[13].ToString(), TMF[13].ToString(), DIP[13].ToString(), TMP[13].ToString() };
            string[] row14 = { position[14].ToString(), AZI[14].ToString(), INC[14].ToString(), GTF[14].ToString(), MTF[14].ToString(), TGF[14].ToString(), TMF[14].ToString(), DIP[14].ToString(), TMP[14].ToString() };
            string[] row15 = { position[15].ToString(), AZI[15].ToString(), INC[15].ToString(), GTF[15].ToString(), MTF[15].ToString(), TGF[15].ToString(), TMF[15].ToString(), DIP[15].ToString(), TMP[15].ToString() };
            string[] row16 = { position[16].ToString(), AZI[16].ToString(), INC[16].ToString(), GTF[16].ToString(), MTF[16].ToString(), TGF[16].ToString(), TMF[16].ToString(), DIP[16].ToString(), TMP[16].ToString() };
            string[] row17 = { position[17].ToString(), AZI[17].ToString(), INC[17].ToString(), GTF[17].ToString(), MTF[17].ToString(), TGF[17].ToString(), TMF[17].ToString(), DIP[17].ToString(), TMP[17].ToString() };

            // Averages
            AZIAverage = 0;
            INCAverage = 0;
            TGFAverage = 0;
            TMFAverage = 0;
            DIPAverage = 0;
            TMPAverage = 0;
            int avgCount = 0;
            for (avgCount = 0; avgCount < currentPosition; avgCount++)
            {
                AZIAverage = AZIAverage + AZI[avgCount];
                if (AZI[avgCount] < 0) AZI[avgCount] += 360;
                INCAverage = INCAverage + INC[avgCount];
                TGFAverage = TGFAverage + TGF[avgCount];
                TMFAverage = TMFAverage + TMF[avgCount];
                DIPAverage = DIPAverage + DIP[avgCount];
                TMPAverage = TMPAverage + TMP[avgCount];
            }
            string[] row18 = { "Average",
                String.Format("{0:0.0}", AZIAverage/avgCount),
                String.Format("{0:0.0}", INCAverage/avgCount),
                "----",
                "----",
                String.Format("{0:0.000}", TGFAverage/avgCount),
                String.Format("{0:0.000}", TMFAverage/avgCount),
                String.Format("{0:0.0}", DIPAverage/avgCount),
                String.Format("{0:0.0}", TMPAverage/avgCount) };

            // Spreads 
            for (int i = 0; i < currentPosition; i++)
            {
                // AZI
                if (AZI[i] > 350) AZI[i] -= 360;    // Compensate AZI for values toggling between 0 & 359.999
                if (i == 1) AZImax = AZImin = AZI[i];
                if (AZImax < AZI[i]) AZImax = AZI[i];
                if (AZImin > AZI[i]) AZImin = AZI[i];
                AZISpread = AZImax - AZImin;
                // INC
                if (i == 1) INCmax = INCmin = INC[i];
                if (INCmax < INC[i]) INCmax = INC[i];
                if (INCmin > INC[i]) INCmin = INC[i];
                INCSpread = INCmax - INCmin;
                // TGF
                if (i == 1) TGFmax = TGFmin = TGF[i];
                if (TGFmax < TGF[i]) TGFmax = TGF[i];
                if (TGFmin > TGF[i]) TGFmin = TGF[i];
                TGFSpread = TGFmax - TGFmin;
                // TMF
                if (i == 1) TMFmax = TMFmin = TMF[i];
                if (TMFmax < TMF[i]) TMFmax = TMF[i];
                if (TMFmin > TMF[i]) TMFmin = TMF[i];
                TMFSpread = TMFmax - TMFmin;
                // DIP
                if (i == 1) DIPmax = DIPmin = DIP[i];
                if (DIPmax < DIP[i]) DIPmax = DIP[i];
                if (DIPmin > DIP[i]) DIPmin = DIP[i];
                DIPSpread = DIPmax - DIPmin;
                // TMP
                if (i == 1) TMPmax = TMPmin = TMP[i];
                if (TMPmax < TMP[i]) TMPmax = TMP[i];
                if (TMPmin > TMP[i]) TMPmin = TMP[i];
                TMPSpread = TMPmax - TMPmin;
            }
            string[] row19 = { "Spread",
                String.Format("{0:0.0}", AZISpread),
                String.Format("{0:0.0}", INCSpread),
                "----",
                "----",
                String.Format("{0:0.000}", TGFSpread),
                String.Format("{0:0.000}", TMFSpread),
                String.Format("{0:0.0}", DIPSpread),
                String.Format("{0:0.0}", TMPSpread) };

            string[] row20 = { "Limits", "0.5", "0.1", "----", "----", "0.003", "0.010", "0.5", "0.5" };

            dataGridView1.Rows.Add(row0);
            dataGridView1.Rows.Add(row1);
            dataGridView1.Rows.Add(row2);
            dataGridView1.Rows.Add(row3);
            dataGridView1.Rows.Add(row4);
            dataGridView1.Rows.Add(row5);
            dataGridView1.Rows.Add(row6);
            dataGridView1.Rows.Add(row7);
            dataGridView1.Rows.Add(row8);
            dataGridView1.Rows.Add(row9);
            dataGridView1.Rows.Add(row10);
            dataGridView1.Rows.Add(row11);
            dataGridView1.Rows.Add(row12);
            dataGridView1.Rows.Add(row13);
            dataGridView1.Rows.Add(row14);
            dataGridView1.Rows.Add(row15);
            dataGridView1.Rows.Add(row16);
            dataGridView1.Rows.Add(row17);
            dataGridView1.Rows.Add(row18);
            dataGridView1.Rows.Add(row19);
            dataGridView1.Rows.Add(row20);

            // Check pass fail condition and flag the failing value
            labelPassFail.Visible = true;
            labelPassFail.BackColor = Color.DarkGreen;
            if (AZISpread > AZIlimit)
            {
                dataGridView1.Rows[10].Cells[1].Style.BackColor = Color.Red;
                //failFlag = true;
            }
            if (INCSpread > INClimit)
            {
                dataGridView1.Rows[10].Cells[2].Style.BackColor = Color.Red;
                failFlag = true;
            }
            if (TGFSpread > TGFlimit)
            {
                dataGridView1.Rows[10].Cells[5].Style.BackColor = Color.Red;
                failFlag = true;
            }
            if (TMFSpread > TMFlimit)
            {
                dataGridView1.Rows[10].Cells[6].Style.BackColor = Color.Red;
                failFlag = true;
            }
            if (DIPSpread > DIPlimit)
            {
                dataGridView1.Rows[10].Cells[7].Style.BackColor = Color.Red;
                failFlag = true;
            }
            if (TMPSpread > TMPlimit)
            {
                dataGridView1.Rows[10].Cells[8].Style.BackColor = Color.Red;
                failFlag = true;
            }
            // Update the Pass Fail label
            if (failFlag)
            {
                labelPassFail.BackColor = Color.Red;
                labelPassFail.Text = "Fail";
            }

            // Adjust the gridview size to match the number of rows 
            var height = 35;
            foreach (DataGridViewRow dr in dataGridView1.Rows)
            {
                height += dr.Height;
            }
            dataGridView1.Height = height;

            buttonPrint.Enabled = true;
        }

        private void ButtonStart_Click(object sender, EventArgs e)
        {
            labelPassFail.Visible = false;
            buttonPrint.Enabled = false;

            if (buttonStart.Text == "Stop")
            {
                buttonStart.Text = "Start";
                buttonNext.Enabled = false;
                dataGridView1.Visible = false;
                dataGridView1.Rows.Clear();
                dataGridView1.Refresh();
                rollTestComplete = false;
                aGauge.Visible = true;
                aGauge.GaugeRanges.Add(new AGaugeRange(Color.Black, 0, 359, 230, 250));
                return;
            }
            else
            {
                timerRollData.Enabled = false;  // Stop the timer while we do this.
                operatorName = Interaction.InputBox("Enter the operator name to be used on the report", "Rolltest", "", -1, -1);
                buttonStart.Text = "Stop";
                buttonNext.Enabled = true;
                timerRollData.Interval = 1000;
                currentPosition = 0;
                labelPassFail.Visible = false;
                buttonPrint.Enabled = false;
                timerRollData.Enabled = true;

                rtf = new rollTestForm(localNT, tc, localMyFtdiDevice, localFtStatus);
                rtf.StartPosition = FormStartPosition.CenterParent;
                rtf.Show();
            }

        }

        private void ButtonNext_Click(object sender, EventArgs e)
        {
            rtf.progressStateMachine();

            if (checkBoxRollType.Checked)
            {
                switch (currentPosition)
                {
                    case 0:
                        {
                            position[0] = 0;
                            aGauge.GaugeRanges.Add(new AGaugeRange(Color.Red, 0, 90, 230, 250));
                        }
                        break;
                    case 1:
                        {
                            position[1] = 90;
                            aGauge.GaugeRanges.Add(new AGaugeRange(Color.DarkGreen, 0, 90, 230, 250));
                            aGauge.GaugeRanges.Add(new AGaugeRange(Color.Red, 90, 180, 230, 250));
                        }
                        break;
                    case 2:
                        {
                            position[2] = 180;
                            aGauge.GaugeRanges.Add(new AGaugeRange(Color.DarkGreen, 90, 180, 230, 250));
                            aGauge.GaugeRanges.Add(new AGaugeRange(Color.Red, 180, 270, 230, 250));
                        }
                        break;
                    case 3:
                        {
                            position[3] = 270;
                            aGauge.GaugeRanges.Add(new AGaugeRange(Color.DarkGreen, 180, 270, 230, 250));
                            aGauge.GaugeRanges.Add(new AGaugeRange(Color.Red, 270, 360, 230, 250));
                        }
                        break;
                    case 4:
                        {
                            position[4] = 360;
                            aGauge.GaugeRanges.Add(new AGaugeRange(Color.DarkGreen, 270, 360, 230, 250));
                            buttonNext.Enabled = false;
                            aGauge.Visible = false;
                            rollTestComplete = true;
                            //dataGridView1.Visible = true;
                        }
                        break;
                }
            }
            else
            {
                switch (currentPosition)
                {
                    //West Rolls
                    case 0:
                    {
                        position[0] = 0;
                        aGauge.GaugeRanges.Add(new AGaugeRange(Color.Red, 0, 45, 230, 250));
                    }
                        break;
                    case 1:
                    {
                        position[1] = 45;
                        aGauge.GaugeRanges.Add(new AGaugeRange(Color.DarkGreen, 0, 45, 230, 250));
                        aGauge.GaugeRanges.Add(new AGaugeRange(Color.Red, 45, 90, 230, 250));
                    }
                        break;
                    case 2:
                    {
                        position[2] = 90;
                        aGauge.GaugeRanges.Add(new AGaugeRange(Color.DarkGreen, 45, 90, 230, 250));
                        aGauge.GaugeRanges.Add(new AGaugeRange(Color.Red, 90, 135, 230, 250));
                    }
                        break;
                    case 3:
                    {
                        position[3] = 135;
                        aGauge.GaugeRanges.Add(new AGaugeRange(Color.DarkGreen, 90, 135, 230, 250));
                        aGauge.GaugeRanges.Add(new AGaugeRange(Color.Red, 135, 180, 230, 250));
                    }
                        break;
                    case 4:
                    {
                        position[4] = 180;
                        aGauge.GaugeRanges.Add(new AGaugeRange(Color.DarkGreen, 135, 180, 230, 250));
                        aGauge.GaugeRanges.Add(new AGaugeRange(Color.Red, 180, 225, 230, 250));
                    }
                        break;
                    case 5:
                    {
                        position[5] = 225;
                        aGauge.GaugeRanges.Add(new AGaugeRange(Color.DarkGreen, 180, 225, 230, 250));
                        aGauge.GaugeRanges.Add(new AGaugeRange(Color.Red, 225, 270, 230, 250));
                    }
                        break;
                    case 6:
                    {
                        position[6] = 270;
                        aGauge.GaugeRanges.Add(new AGaugeRange(Color.DarkGreen, 225, 270, 230, 250));
                        aGauge.GaugeRanges.Add(new AGaugeRange(Color.Red, 270, 315, 230, 250));
                    }
                        break;
                    case 7:
                    {
                        position[7] = 315;
                        aGauge.GaugeRanges.Add(new AGaugeRange(Color.DarkGreen, 270, 315, 230, 250));
                        aGauge.GaugeRanges.Add(new AGaugeRange(Color.Red, 315, 359, 230, 250));
                    }
                        break;
                    case 8:
                    {
                        aGauge.GaugeRanges.Add(new AGaugeRange(Color.DarkGreen, 315, 359, 230, 250));
                        position[8] = 360;
                        //dataGridView1.Visible = true;
                    }
                        break;
                    // East Rolls
                    case 9:
                    {
                        position[10] = 45;
                        aGauge.GaugeRanges.Add(new AGaugeRange(Color.DarkGreen, 0, 45, 230, 250));
                        aGauge.GaugeRanges.Add(new AGaugeRange(Color.Red, 45, 90, 230, 250));
                    }
                    break;
                    case 10:
                    {
                        position[11] = 90;
                        aGauge.GaugeRanges.Add(new AGaugeRange(Color.DarkGreen, 45, 90, 230, 250));
                        aGauge.GaugeRanges.Add(new AGaugeRange(Color.Red, 90, 135, 230, 250));
                    }
                    break;
                    case 11:
                    {
                        position[12] = 135;
                        aGauge.GaugeRanges.Add(new AGaugeRange(Color.DarkGreen, 90, 135, 230, 250));
                        aGauge.GaugeRanges.Add(new AGaugeRange(Color.Red, 135, 180, 230, 250));
                    }
                    break;
                    case 12:
                    {
                        position[13] = 180;
                        aGauge.GaugeRanges.Add(new AGaugeRange(Color.DarkGreen, 135, 180, 230, 250));
                        aGauge.GaugeRanges.Add(new AGaugeRange(Color.Red, 180, 225, 230, 250));
                    }
                    break;
                    case 13:
                    {
                        position[14] = 225;
                        aGauge.GaugeRanges.Add(new AGaugeRange(Color.DarkGreen, 180, 225, 230, 250));
                        aGauge.GaugeRanges.Add(new AGaugeRange(Color.Red, 225, 270, 230, 250));
                    }
                        break;
                    case 14:
                    {
                        position[15] = 270;
                        aGauge.GaugeRanges.Add(new AGaugeRange(Color.DarkGreen, 225, 270, 230, 250));
                        aGauge.GaugeRanges.Add(new AGaugeRange(Color.Red, 270, 315, 230, 250));
                    }
                    break;
                    case 15:
                    {
                        position[16] = 315;
                        aGauge.GaugeRanges.Add(new AGaugeRange(Color.DarkGreen, 270, 315, 230, 250));
                        aGauge.GaugeRanges.Add(new AGaugeRange(Color.Red, 315, 359, 230, 250));
                    }
                    break;
                    //Z Check
                    case 16:
                    {
                        position[18] = 0;
                        aGauge.GaugeRanges.Add(new AGaugeRange(Color.Red, 0, 45, 230, 250));
                    }
                    break;
                    case 17:
                    {
                        position[19] = 180;
                        aGauge.GaugeRanges.Add(new AGaugeRange(Color.Red, 0, 45, 230, 250));
                        buttonNext.Enabled = false;
                        aGauge.Visible = false;
                        rollTestComplete = true;
                    }
                    break;
                }
            }

            aGauge.Refresh();

            AZI[currentPosition] = Convert.ToSingle(textBoxAZM.Text);
            INC[currentPosition] = Convert.ToSingle(textBoxINC.Text);
            GTF[currentPosition] = Convert.ToSingle(textBoxGTF.Text);
            MTF[currentPosition] = Convert.ToSingle(textBoxMTF.Text);
            DIP[currentPosition] = Convert.ToSingle(textBoxDIP.Text);
            TMP[currentPosition] = Convert.ToSingle(textBoxTMP.Text);
            TGF[currentPosition] = Convert.ToSingle(textBoxTGF.Text);
            TMF[currentPosition] = Convert.ToSingle(textBoxTMF.Text);

            if (rollTestComplete) // Update the gridview values when all positions have been completed
            {
                dataGridView1.Visible = true;
                populateDataGridView();
            }

            currentPosition++;
            toolStripStatusLabel1.Text = "Roll Pos = " + currentPosition.ToString();
        }

        private void ButtonPrint_Click(object sender, EventArgs e)
        {
            timerRollData.Enabled = false;  // Stop the timer while we do this.

            printPreviewDialog1.Document = printDocument1;
            // Build filename
            string date = System.DateTime.Now.ToString();
            date = date.Replace(' ', '_').Replace('/', '-').Replace(':', '-');
            string file = "ROLL RESULTS_" + Common.serialNumber.Trim('\0') + "_" + date;
            string directory = @"C:\Wolverine\NeutrinoTracker\ROLL RESULTS\";

            // Save the file as .PDF and then diplay in the preview. 
            try
            {
                if (!Directory.Exists(directory))
                {
                    // Try to create the directory.
                    Directory.CreateDirectory(directory);
                }

                printDocument1.PrinterSettings = new PrinterSettings()
                {
                    // set the printer to 'Microsoft Print to PDF'
                    PrinterName = "Microsoft Print to PDF",
                    PrintToFile = true,
                    PrintFileName = Path.Combine(directory, file + ".pdf"),
                };
                printDocument1.Print();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ToString());
            }

            PrintPreviewDialog ppDialog = new PrintPreviewDialog();
            ppDialog.Document = printDocument1;
            ppDialog.Icon = System.Drawing.Icon.ExtractAssociatedIcon(@"C:\Wolverine\Projects\NeutrinoDB\NeutrinoDBTracker\Neutrino-2-Cell.ico");

            // The default print button in print preview doesnt bring up the printer options window. The following code changes that  
            ToolStripButton b = new ToolStripButton();
            b.Image = ((System.Windows.Forms.ToolStrip)(ppDialog.Controls[1])).ImageList.Images[0];
            b.DisplayStyle = ToolStripItemDisplayStyle.Image;
            b.Click += printPreview_PrintClick;
            ((ToolStrip)(ppDialog.Controls[1])).Items.RemoveAt(0);
            ((ToolStrip)(ppDialog.Controls[1])).Items.Insert(0, b);

            // Resize and position the preview screen  
            ppDialog.Left = 0;
            ppDialog.Top = 0;
            ppDialog.Width = 1024;
            ppDialog.Height = 704;
            ppDialog.FindForm().StartPosition = FormStartPosition.CenterScreen;

            // Show it!
            ppDialog.ShowDialog();

            timerRollData.Enabled = false;
        }

        /// <summary>
        /// This gets executed when you click on the preview screen printer icon in the toolbar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void printPreview_PrintClick(object sender, EventArgs e)
        {
            try
            {
                printDialog1.Document = printDocument1;
                if (printDialog1.ShowDialog() == DialogResult.OK)
                {
                    printDocument1.Print();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ToString());
            }
        }

        private void PrintDocument_PrintPage(object sender, PrintPageEventArgs e)
        {
            try
            {
                // Logo on top
                Bitmap logo = new Bitmap(@"C:\Wolverine\Projects\NeutrinoDB\NeutrinoDBTracker\WOFT.png");
                //            Bitmap logo = new Bitmap(@"NeutrinoDBPcMwd\Images\wolverine logo.bmp");
                //Assembly myAssembly = Assembly.GetExecutingAssembly();
                //Stream myStream = myAssembly.GetManifestResourceStream("NeutrinoDBPcMwd.Images.wolverine logo.bmp");
                //Bitmap logo = new Bitmap(myStream);

                logo = ResizeBitmap(logo, 300, 75);
                e.Graphics.DrawImage(logo, e.MarginBounds.Left + (e.MarginBounds.Width / 2) - (logo.Width / 2), 10);

                // Report Title
                string tabDataText = "Roll Test Report";
                Font printFont = new Font("Times New Roman", 25);
                using (var sf = new StringFormat())
                {
                    sf.LineAlignment = StringAlignment.Center;
                    sf.Alignment = StringAlignment.Center;
                    e.Graphics.DrawString(tabDataText, printFont, new SolidBrush(Color.Black), e.MarginBounds.Left + (e.MarginBounds.Width / 2), (logo.Height / 2) + 100, sf);
                }

                // Report info
                string infoText =
                    "Operator Name:    " + operatorName + Environment.NewLine +
                    "Serial Number:    " + serialNumber + Environment.NewLine +
                    "Date and Time:    " + System.DateTime.Now + Environment.NewLine +
                    "Test Pass/Fail:   " + labelPassFail.Text + Environment.NewLine;
                printFont = new Font("Times New Roman", 12);
                using (var sf = new StringFormat())
                {
                    sf.LineAlignment = StringAlignment.Near;
                    sf.Alignment = StringAlignment.Near;
                    e.Graphics.DrawString(infoText, printFont, new SolidBrush(Color.Black), e.MarginBounds.Left + (e.MarginBounds.Width / 4), (logo.Height / 2) + 150, sf);
                }

                // Create a Bitmap of the dataGridView 
                Bitmap bitmap = new Bitmap(this.dataGridView1.Width, this.dataGridView1.Height);
                dataGridView1.DrawToBitmap(bitmap, new Rectangle(0, 0, this.dataGridView1.Width, this.dataGridView1.Height));
                // Draws the dataGridView graphic
                e.Graphics.DrawImage(bitmap, (e.PageBounds.Width - bitmap.Width) / 2, 350);

            }
            catch (Exception)
            {
                MessageBox.Show("Print Document error", "Error");
            }
        }

        public Bitmap ResizeBitmap(Bitmap bmp, int width, int height)
        {
            Bitmap result = new Bitmap(width, height);
            using (Graphics g = Graphics.FromImage(result))
            {
                g.DrawImage(bmp, 0, 0, width, height);
            }

            return result;
        }

        private async void ButtonGetTFO_Click(object sender, EventArgs e)
        {
            List<byte> payload = new List<byte>();
            MessageHandler mh = new MessageHandler();
            ushort checksumCalc = new ushort();

            timerRollData.Enabled = false;  // Stop the timer while we do this.
            aGauge.GaugeRanges.Clear();

            toolFaceOffset = sV.MasterDataGroup1.GravityToolFaceDegrees;
            textBoxTFO.Text = String.Format("{0:0.0}", toolFaceOffset);

            // Read the tool configuration structure
            var message = cp.buildMessage(Common.NODE_MASTER, Common.CMD_NONVOLATILE_RDWR, Common.MWD_MASTER_NONVOLATILE_CONFIGURATION);

            if (!localMyFtdiDevice.IsOpen)
                return;

            if (await tc.FTDI_SEND(message, localMyFtdiDevice, localFtStatus, localNT))
            {
                // Update the cfg structure with the rxData
                sV.MasterConfiguration = conv.FromByteArray<StructureVariables.MWD_MASTER_CONFIGURATION_STRUCTURE>(tc.rxData.ToArray());
                cfg = sV.MasterConfiguration;
            }
            else
            {
                toolStripStatusLabel1.Text = "Read Config Failed";
                MessageBox.Show("Read configuration Failed");
            }

            masterTFO.ToolfaceOffsetDegrees = Convert.ToSingle(textBoxTFO.Text); // Update the TFO value 

            // Write the Config structure prameters
            masterTFO.CheckSum = 0;
            checksumCalc = mh.calculateChecksum(conv.ToByteArray<StructureVariables.MWD_MASTER_TFO_STRUCTURE>(masterTFO));
            cfg.CheckSum = checksumCalc;
            payload = conv.ToByteArray<StructureVariables.MWD_MASTER_TFO_STRUCTURE>(masterTFO).ToList();
            message = cp.buildMessage(Common.NODE_MASTER, Common.CMD_NONVOLATILE_RDWR, Common.MWD_MASTER_NONVOLATILE_TFO, payload);

            if (!localMyFtdiDevice.IsOpen)
                return;

            if (await tc.FTDI_SEND(message, localMyFtdiDevice, localFtStatus, localNT))
            {
                toolStripStatusLabel1.Text = "Write Configuration Ok";
                textBoxTFO.Text = String.Format("{0:0.0}", toolFaceOffset);
            }
            else
            {
                MessageBox.Show("Write configuration Failed");
            }
            timerRollData.Enabled = true;
        }

        private async void ButtonResetTFO_Click(object sender, EventArgs e)
        {
            List<byte> payload = new List<byte>();
            MessageHandler mh = new MessageHandler();
            ushort checksumCalc = new ushort();

            timerRollData.Enabled = false;  // Stop the timer while we do this.
            aGauge.GaugeRanges.Clear();

            toolFaceOffset = 0;
            textBoxTFO.Text = String.Format("{0:0.0}", toolFaceOffset);

            // Read the tool configuration structure
            var message = cp.buildMessage(Common.NODE_MASTER, Common.CMD_NONVOLATILE_RDWR, Common.MWD_MASTER_NONVOLATILE_CONFIGURATION);

            if (!localMyFtdiDevice.IsOpen)
                return;

            if (await tc.FTDI_SEND(message, localMyFtdiDevice, localFtStatus, localNT))
            {
                // Update the cfg structure with the rxData
                sV.MasterConfiguration = conv.FromByteArray<StructureVariables.MWD_MASTER_CONFIGURATION_STRUCTURE>(tc.rxData.ToArray());
                cfg = sV.MasterConfiguration;

                // TODO: Verify Checksum 
            }
            else
            {
                toolStripStatusLabel1.Text = "Read Config Failed";
                MessageBox.Show("Read configuration Failed");
                return;
            }

            masterTFO.ToolfaceOffsetDegrees = Convert.ToSingle(textBoxTFO.Text); // Update the TFO value 

            // Write the Config structure prameters
            masterTFO.CheckSum = 0;
            checksumCalc = mh.calculateChecksum(conv.ToByteArray<StructureVariables.MWD_MASTER_TFO_STRUCTURE>(masterTFO));
            masterTFO.CheckSum = checksumCalc;
            payload = conv.ToByteArray<StructureVariables.MWD_MASTER_TFO_STRUCTURE>(masterTFO).ToList();
            message = cp.buildMessage(Common.NODE_MASTER, Common.CMD_NONVOLATILE_RDWR, Common.MWD_MASTER_NONVOLATILE_TFO, payload);

            if (!localMyFtdiDevice.IsOpen)
                return;

            if (await tc.FTDI_SEND(message, localMyFtdiDevice, localFtStatus, localNT))
            {
                toolStripStatusLabel1.Text = "Write Configuration Ok";
                textBoxTFO.Text = String.Format("{0:0.0}", toolFaceOffset);
            }
            else
            {
                MessageBox.Show("Write configuration Failed");
            }

            timerRollData.Enabled = true;
        }
    }
}
