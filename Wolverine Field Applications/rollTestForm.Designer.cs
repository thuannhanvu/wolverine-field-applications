﻿namespace Wolverine_Field_Applications
{
    partial class rollTestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rollTestForm));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.westGxg45Box = new System.Windows.Forms.TextBox();
            this.westGyg45Box = new System.Windows.Forms.TextBox();
            this.westGzg45Box = new System.Windows.Forms.TextBox();
            this.westBxGauss45Box = new System.Windows.Forms.TextBox();
            this.westByGauss45Box = new System.Windows.Forms.TextBox();
            this.westBzGauss45Box = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.westBzGauss90Box = new System.Windows.Forms.TextBox();
            this.westByGauss90Box = new System.Windows.Forms.TextBox();
            this.westBxGauss90Box = new System.Windows.Forms.TextBox();
            this.westGzg90Box = new System.Windows.Forms.TextBox();
            this.westGyg90Box = new System.Windows.Forms.TextBox();
            this.westGxg90Box = new System.Windows.Forms.TextBox();
            this.westBzGauss135Box = new System.Windows.Forms.TextBox();
            this.westByGauss135Box = new System.Windows.Forms.TextBox();
            this.westBxGauss135Box = new System.Windows.Forms.TextBox();
            this.westGzg135Box = new System.Windows.Forms.TextBox();
            this.westGyg135Box = new System.Windows.Forms.TextBox();
            this.westGxg135Box = new System.Windows.Forms.TextBox();
            this.westBzGauss180Box = new System.Windows.Forms.TextBox();
            this.westByGauss180Box = new System.Windows.Forms.TextBox();
            this.westBxGauss180Box = new System.Windows.Forms.TextBox();
            this.westGzg180Box = new System.Windows.Forms.TextBox();
            this.westGyg180Box = new System.Windows.Forms.TextBox();
            this.westGxg180Box = new System.Windows.Forms.TextBox();
            this.westBzGauss225Box = new System.Windows.Forms.TextBox();
            this.westByGauss225Box = new System.Windows.Forms.TextBox();
            this.westBxGauss225Box = new System.Windows.Forms.TextBox();
            this.westGzg225Box = new System.Windows.Forms.TextBox();
            this.westGyg225Box = new System.Windows.Forms.TextBox();
            this.westGxg225Box = new System.Windows.Forms.TextBox();
            this.westBzGauss270Box = new System.Windows.Forms.TextBox();
            this.westByGauss270Box = new System.Windows.Forms.TextBox();
            this.westBxGauss270Box = new System.Windows.Forms.TextBox();
            this.westGzg270Box = new System.Windows.Forms.TextBox();
            this.westGyg270Box = new System.Windows.Forms.TextBox();
            this.westGxg270Box = new System.Windows.Forms.TextBox();
            this.westBzGauss315Box = new System.Windows.Forms.TextBox();
            this.westByGauss315Box = new System.Windows.Forms.TextBox();
            this.westBxGauss315Box = new System.Windows.Forms.TextBox();
            this.westGzg315Box = new System.Windows.Forms.TextBox();
            this.westGyg315Box = new System.Windows.Forms.TextBox();
            this.westGxg315Box = new System.Windows.Forms.TextBox();
            this.westBzGauss360Box = new System.Windows.Forms.TextBox();
            this.westByGauss360Box = new System.Windows.Forms.TextBox();
            this.westBxGauss360Box = new System.Windows.Forms.TextBox();
            this.westGzg360Box = new System.Windows.Forms.TextBox();
            this.westGyg360Box = new System.Windows.Forms.TextBox();
            this.westGxg360Box = new System.Windows.Forms.TextBox();
            this.eastBzGauss180Box = new System.Windows.Forms.TextBox();
            this.eastByGauss180Box = new System.Windows.Forms.TextBox();
            this.eastBxGauss180Box = new System.Windows.Forms.TextBox();
            this.eastGzg180Box = new System.Windows.Forms.TextBox();
            this.eastGyg180Box = new System.Windows.Forms.TextBox();
            this.eastGxg180Box = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.eastBzGauss45Box = new System.Windows.Forms.TextBox();
            this.eastByGauss45Box = new System.Windows.Forms.TextBox();
            this.eastBxGauss45Box = new System.Windows.Forms.TextBox();
            this.eastGzg45Box = new System.Windows.Forms.TextBox();
            this.eastGyg45Box = new System.Windows.Forms.TextBox();
            this.eastGxg45Box = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.verticalBzGauss180Box = new System.Windows.Forms.TextBox();
            this.verticalByGauss180Box = new System.Windows.Forms.TextBox();
            this.verticalBxGauss180Box = new System.Windows.Forms.TextBox();
            this.verticalGzg180Box = new System.Windows.Forms.TextBox();
            this.verticalGyg180Box = new System.Windows.Forms.TextBox();
            this.verticalGxg180Box = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.verticalBzGauss0Box = new System.Windows.Forms.TextBox();
            this.verticalByGauss0Box = new System.Windows.Forms.TextBox();
            this.verticalBxGauss0Box = new System.Windows.Forms.TextBox();
            this.verticalGzg0Box = new System.Windows.Forms.TextBox();
            this.verticalGyg0Box = new System.Windows.Forms.TextBox();
            this.verticalGxg0Box = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.eastRoll45Test = new System.Windows.Forms.Button();
            this.eastRoll180Test = new System.Windows.Forms.Button();
            this.verticalRoll45Test = new System.Windows.Forms.Button();
            this.verticalRoll180Test = new System.Windows.Forms.Button();
            this.westRoll45Test = new System.Windows.Forms.Button();
            this.westRoll90Test = new System.Windows.Forms.Button();
            this.westRoll135Test = new System.Windows.Forms.Button();
            this.westRoll180Test = new System.Windows.Forms.Button();
            this.westRoll225Test = new System.Windows.Forms.Button();
            this.westRoll270Test = new System.Windows.Forms.Button();
            this.westRoll315Test = new System.Windows.Forms.Button();
            this.westRoll360Test = new System.Windows.Forms.Button();
            this.statusBox = new System.Windows.Forms.RichTextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.eastGxg90Box = new System.Windows.Forms.TextBox();
            this.eastGyg90Box = new System.Windows.Forms.TextBox();
            this.eastGzg90Box = new System.Windows.Forms.TextBox();
            this.eastBxGauss90Box = new System.Windows.Forms.TextBox();
            this.eastByGauss90Box = new System.Windows.Forms.TextBox();
            this.eastBzGauss90Box = new System.Windows.Forms.TextBox();
            this.eastGxg135Box = new System.Windows.Forms.TextBox();
            this.eastGyg135Box = new System.Windows.Forms.TextBox();
            this.eastGzg135Box = new System.Windows.Forms.TextBox();
            this.eastBxGauss135Box = new System.Windows.Forms.TextBox();
            this.eastByGauss135Box = new System.Windows.Forms.TextBox();
            this.eastBzGauss135Box = new System.Windows.Forms.TextBox();
            this.eastRoll90Test = new System.Windows.Forms.Button();
            this.eastRoll135Test = new System.Windows.Forms.Button();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.eastGxg225Box = new System.Windows.Forms.TextBox();
            this.eastGyg225Box = new System.Windows.Forms.TextBox();
            this.eastGzg225Box = new System.Windows.Forms.TextBox();
            this.eastBxGauss225Box = new System.Windows.Forms.TextBox();
            this.eastByGauss225Box = new System.Windows.Forms.TextBox();
            this.eastBzGauss225Box = new System.Windows.Forms.TextBox();
            this.eastGxg270Box = new System.Windows.Forms.TextBox();
            this.eastGyg270Box = new System.Windows.Forms.TextBox();
            this.eastGzg270Box = new System.Windows.Forms.TextBox();
            this.eastBxGauss270Box = new System.Windows.Forms.TextBox();
            this.eastByGauss270Box = new System.Windows.Forms.TextBox();
            this.eastBzGauss270Box = new System.Windows.Forms.TextBox();
            this.eastGxg315Box = new System.Windows.Forms.TextBox();
            this.eastGyg315Box = new System.Windows.Forms.TextBox();
            this.eastGzg315Box = new System.Windows.Forms.TextBox();
            this.eastBxGauss315Box = new System.Windows.Forms.TextBox();
            this.eastByGauss315Box = new System.Windows.Forms.TextBox();
            this.eastBzGauss315Box = new System.Windows.Forms.TextBox();
            this.eastGxg360Box = new System.Windows.Forms.TextBox();
            this.eastGyg360Box = new System.Windows.Forms.TextBox();
            this.eastGzg360Box = new System.Windows.Forms.TextBox();
            this.eastBxGauss360Box = new System.Windows.Forms.TextBox();
            this.eastByGauss360Box = new System.Windows.Forms.TextBox();
            this.eastBzGauss360Box = new System.Windows.Forms.TextBox();
            this.eastRoll225Test = new System.Windows.Forms.Button();
            this.eastRoll270Test = new System.Windows.Forms.Button();
            this.eastRoll315Test = new System.Windows.Forms.Button();
            this.eastRoll360Test = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label1.Location = new System.Drawing.Point(57, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(138, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "WEST ROLL";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label2.Location = new System.Drawing.Point(63, 378);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(132, 26);
            this.label2.TabIndex = 1;
            this.label2.Text = "EAST ROLL";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label4.Location = new System.Drawing.Point(208, 75);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 26);
            this.label4.TabIndex = 3;
            this.label4.Text = "45";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label5.Location = new System.Drawing.Point(334, 75);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 26);
            this.label5.TabIndex = 4;
            this.label5.Text = "90";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label6.Location = new System.Drawing.Point(454, 75);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 26);
            this.label6.TabIndex = 5;
            this.label6.Text = "135";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label7.Location = new System.Drawing.Point(582, 75);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 26);
            this.label7.TabIndex = 6;
            this.label7.Text = "180";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label8.Location = new System.Drawing.Point(699, 75);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 26);
            this.label8.TabIndex = 7;
            this.label8.Text = "225";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label9.Location = new System.Drawing.Point(827, 75);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(48, 26);
            this.label9.TabIndex = 8;
            this.label9.Text = "270";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label10.Location = new System.Drawing.Point(953, 75);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(48, 26);
            this.label10.TabIndex = 9;
            this.label10.Text = "315";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label11.Location = new System.Drawing.Point(1074, 75);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(48, 26);
            this.label11.TabIndex = 10;
            this.label11.Text = "360";
            // 
            // westGxg45Box
            // 
            this.westGxg45Box.Location = new System.Drawing.Point(176, 115);
            this.westGxg45Box.Name = "westGxg45Box";
            this.westGxg45Box.Size = new System.Drawing.Size(100, 20);
            this.westGxg45Box.TabIndex = 29;
            // 
            // westGyg45Box
            // 
            this.westGyg45Box.Location = new System.Drawing.Point(176, 151);
            this.westGyg45Box.Name = "westGyg45Box";
            this.westGyg45Box.Size = new System.Drawing.Size(100, 20);
            this.westGyg45Box.TabIndex = 30;
            // 
            // westGzg45Box
            // 
            this.westGzg45Box.Location = new System.Drawing.Point(176, 188);
            this.westGzg45Box.Name = "westGzg45Box";
            this.westGzg45Box.Size = new System.Drawing.Size(100, 20);
            this.westGzg45Box.TabIndex = 31;
            // 
            // westBxGauss45Box
            // 
            this.westBxGauss45Box.Location = new System.Drawing.Point(176, 224);
            this.westBxGauss45Box.Name = "westBxGauss45Box";
            this.westBxGauss45Box.Size = new System.Drawing.Size(100, 20);
            this.westBxGauss45Box.TabIndex = 32;
            // 
            // westByGauss45Box
            // 
            this.westByGauss45Box.Location = new System.Drawing.Point(176, 261);
            this.westByGauss45Box.Name = "westByGauss45Box";
            this.westByGauss45Box.Size = new System.Drawing.Size(100, 20);
            this.westByGauss45Box.TabIndex = 33;
            // 
            // westBzGauss45Box
            // 
            this.westBzGauss45Box.Location = new System.Drawing.Point(176, 298);
            this.westBzGauss45Box.Name = "westBzGauss45Box";
            this.westBzGauss45Box.Size = new System.Drawing.Size(100, 20);
            this.westBzGauss45Box.TabIndex = 34;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label30.Location = new System.Drawing.Point(90, 108);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(57, 26);
            this.label30.TabIndex = 35;
            this.label30.Text = "GxG";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label31.Location = new System.Drawing.Point(90, 144);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(57, 26);
            this.label31.TabIndex = 36;
            this.label31.Text = "GyG";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label32.Location = new System.Drawing.Point(90, 181);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(57, 26);
            this.label32.TabIndex = 37;
            this.label32.Text = "GzG";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label33.Location = new System.Drawing.Point(69, 217);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(101, 26);
            this.label33.TabIndex = 38;
            this.label33.Text = "BxGauss";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label34.Location = new System.Drawing.Point(69, 254);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(101, 26);
            this.label34.TabIndex = 39;
            this.label34.Text = "ByGauss";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label35.Location = new System.Drawing.Point(69, 294);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(101, 26);
            this.label35.TabIndex = 40;
            this.label35.Text = "BzGauss";
            // 
            // westBzGauss90Box
            // 
            this.westBzGauss90Box.Location = new System.Drawing.Point(302, 298);
            this.westBzGauss90Box.Name = "westBzGauss90Box";
            this.westBzGauss90Box.Size = new System.Drawing.Size(100, 20);
            this.westBzGauss90Box.TabIndex = 48;
            // 
            // westByGauss90Box
            // 
            this.westByGauss90Box.Location = new System.Drawing.Point(302, 261);
            this.westByGauss90Box.Name = "westByGauss90Box";
            this.westByGauss90Box.Size = new System.Drawing.Size(100, 20);
            this.westByGauss90Box.TabIndex = 47;
            // 
            // westBxGauss90Box
            // 
            this.westBxGauss90Box.Location = new System.Drawing.Point(302, 224);
            this.westBxGauss90Box.Name = "westBxGauss90Box";
            this.westBxGauss90Box.Size = new System.Drawing.Size(100, 20);
            this.westBxGauss90Box.TabIndex = 46;
            // 
            // westGzg90Box
            // 
            this.westGzg90Box.Location = new System.Drawing.Point(302, 188);
            this.westGzg90Box.Name = "westGzg90Box";
            this.westGzg90Box.Size = new System.Drawing.Size(100, 20);
            this.westGzg90Box.TabIndex = 45;
            // 
            // westGyg90Box
            // 
            this.westGyg90Box.Location = new System.Drawing.Point(302, 151);
            this.westGyg90Box.Name = "westGyg90Box";
            this.westGyg90Box.Size = new System.Drawing.Size(100, 20);
            this.westGyg90Box.TabIndex = 44;
            // 
            // westGxg90Box
            // 
            this.westGxg90Box.Location = new System.Drawing.Point(302, 115);
            this.westGxg90Box.Name = "westGxg90Box";
            this.westGxg90Box.Size = new System.Drawing.Size(100, 20);
            this.westGxg90Box.TabIndex = 43;
            // 
            // westBzGauss135Box
            // 
            this.westBzGauss135Box.Location = new System.Drawing.Point(431, 298);
            this.westBzGauss135Box.Name = "westBzGauss135Box";
            this.westBzGauss135Box.Size = new System.Drawing.Size(100, 20);
            this.westBzGauss135Box.TabIndex = 54;
            // 
            // westByGauss135Box
            // 
            this.westByGauss135Box.Location = new System.Drawing.Point(431, 261);
            this.westByGauss135Box.Name = "westByGauss135Box";
            this.westByGauss135Box.Size = new System.Drawing.Size(100, 20);
            this.westByGauss135Box.TabIndex = 53;
            // 
            // westBxGauss135Box
            // 
            this.westBxGauss135Box.Location = new System.Drawing.Point(431, 224);
            this.westBxGauss135Box.Name = "westBxGauss135Box";
            this.westBxGauss135Box.Size = new System.Drawing.Size(100, 20);
            this.westBxGauss135Box.TabIndex = 52;
            // 
            // westGzg135Box
            // 
            this.westGzg135Box.Location = new System.Drawing.Point(431, 188);
            this.westGzg135Box.Name = "westGzg135Box";
            this.westGzg135Box.Size = new System.Drawing.Size(100, 20);
            this.westGzg135Box.TabIndex = 51;
            // 
            // westGyg135Box
            // 
            this.westGyg135Box.Location = new System.Drawing.Point(431, 151);
            this.westGyg135Box.Name = "westGyg135Box";
            this.westGyg135Box.Size = new System.Drawing.Size(100, 20);
            this.westGyg135Box.TabIndex = 50;
            // 
            // westGxg135Box
            // 
            this.westGxg135Box.Location = new System.Drawing.Point(431, 115);
            this.westGxg135Box.Name = "westGxg135Box";
            this.westGxg135Box.Size = new System.Drawing.Size(100, 20);
            this.westGxg135Box.TabIndex = 49;
            // 
            // westBzGauss180Box
            // 
            this.westBzGauss180Box.Location = new System.Drawing.Point(555, 298);
            this.westBzGauss180Box.Name = "westBzGauss180Box";
            this.westBzGauss180Box.Size = new System.Drawing.Size(100, 20);
            this.westBzGauss180Box.TabIndex = 60;
            // 
            // westByGauss180Box
            // 
            this.westByGauss180Box.Location = new System.Drawing.Point(555, 261);
            this.westByGauss180Box.Name = "westByGauss180Box";
            this.westByGauss180Box.Size = new System.Drawing.Size(100, 20);
            this.westByGauss180Box.TabIndex = 59;
            // 
            // westBxGauss180Box
            // 
            this.westBxGauss180Box.Location = new System.Drawing.Point(555, 224);
            this.westBxGauss180Box.Name = "westBxGauss180Box";
            this.westBxGauss180Box.Size = new System.Drawing.Size(100, 20);
            this.westBxGauss180Box.TabIndex = 58;
            // 
            // westGzg180Box
            // 
            this.westGzg180Box.Location = new System.Drawing.Point(555, 188);
            this.westGzg180Box.Name = "westGzg180Box";
            this.westGzg180Box.Size = new System.Drawing.Size(100, 20);
            this.westGzg180Box.TabIndex = 57;
            // 
            // westGyg180Box
            // 
            this.westGyg180Box.Location = new System.Drawing.Point(555, 151);
            this.westGyg180Box.Name = "westGyg180Box";
            this.westGyg180Box.Size = new System.Drawing.Size(100, 20);
            this.westGyg180Box.TabIndex = 56;
            // 
            // westGxg180Box
            // 
            this.westGxg180Box.Location = new System.Drawing.Point(555, 115);
            this.westGxg180Box.Name = "westGxg180Box";
            this.westGxg180Box.Size = new System.Drawing.Size(100, 20);
            this.westGxg180Box.TabIndex = 55;
            // 
            // westBzGauss225Box
            // 
            this.westBzGauss225Box.Location = new System.Drawing.Point(674, 298);
            this.westBzGauss225Box.Name = "westBzGauss225Box";
            this.westBzGauss225Box.Size = new System.Drawing.Size(100, 20);
            this.westBzGauss225Box.TabIndex = 66;
            // 
            // westByGauss225Box
            // 
            this.westByGauss225Box.Location = new System.Drawing.Point(674, 261);
            this.westByGauss225Box.Name = "westByGauss225Box";
            this.westByGauss225Box.Size = new System.Drawing.Size(100, 20);
            this.westByGauss225Box.TabIndex = 65;
            // 
            // westBxGauss225Box
            // 
            this.westBxGauss225Box.Location = new System.Drawing.Point(674, 224);
            this.westBxGauss225Box.Name = "westBxGauss225Box";
            this.westBxGauss225Box.Size = new System.Drawing.Size(100, 20);
            this.westBxGauss225Box.TabIndex = 64;
            // 
            // westGzg225Box
            // 
            this.westGzg225Box.Location = new System.Drawing.Point(674, 188);
            this.westGzg225Box.Name = "westGzg225Box";
            this.westGzg225Box.Size = new System.Drawing.Size(100, 20);
            this.westGzg225Box.TabIndex = 63;
            // 
            // westGyg225Box
            // 
            this.westGyg225Box.Location = new System.Drawing.Point(674, 151);
            this.westGyg225Box.Name = "westGyg225Box";
            this.westGyg225Box.Size = new System.Drawing.Size(100, 20);
            this.westGyg225Box.TabIndex = 62;
            // 
            // westGxg225Box
            // 
            this.westGxg225Box.Location = new System.Drawing.Point(674, 115);
            this.westGxg225Box.Name = "westGxg225Box";
            this.westGxg225Box.Size = new System.Drawing.Size(100, 20);
            this.westGxg225Box.TabIndex = 61;
            // 
            // westBzGauss270Box
            // 
            this.westBzGauss270Box.Location = new System.Drawing.Point(801, 298);
            this.westBzGauss270Box.Name = "westBzGauss270Box";
            this.westBzGauss270Box.Size = new System.Drawing.Size(100, 20);
            this.westBzGauss270Box.TabIndex = 72;
            // 
            // westByGauss270Box
            // 
            this.westByGauss270Box.Location = new System.Drawing.Point(801, 261);
            this.westByGauss270Box.Name = "westByGauss270Box";
            this.westByGauss270Box.Size = new System.Drawing.Size(100, 20);
            this.westByGauss270Box.TabIndex = 71;
            // 
            // westBxGauss270Box
            // 
            this.westBxGauss270Box.Location = new System.Drawing.Point(801, 224);
            this.westBxGauss270Box.Name = "westBxGauss270Box";
            this.westBxGauss270Box.Size = new System.Drawing.Size(100, 20);
            this.westBxGauss270Box.TabIndex = 70;
            // 
            // westGzg270Box
            // 
            this.westGzg270Box.Location = new System.Drawing.Point(801, 188);
            this.westGzg270Box.Name = "westGzg270Box";
            this.westGzg270Box.Size = new System.Drawing.Size(100, 20);
            this.westGzg270Box.TabIndex = 69;
            // 
            // westGyg270Box
            // 
            this.westGyg270Box.Location = new System.Drawing.Point(801, 151);
            this.westGyg270Box.Name = "westGyg270Box";
            this.westGyg270Box.Size = new System.Drawing.Size(100, 20);
            this.westGyg270Box.TabIndex = 68;
            // 
            // westGxg270Box
            // 
            this.westGxg270Box.Location = new System.Drawing.Point(801, 115);
            this.westGxg270Box.Name = "westGxg270Box";
            this.westGxg270Box.Size = new System.Drawing.Size(100, 20);
            this.westGxg270Box.TabIndex = 67;
            // 
            // westBzGauss315Box
            // 
            this.westBzGauss315Box.Location = new System.Drawing.Point(926, 298);
            this.westBzGauss315Box.Name = "westBzGauss315Box";
            this.westBzGauss315Box.Size = new System.Drawing.Size(100, 20);
            this.westBzGauss315Box.TabIndex = 78;
            // 
            // westByGauss315Box
            // 
            this.westByGauss315Box.Location = new System.Drawing.Point(926, 261);
            this.westByGauss315Box.Name = "westByGauss315Box";
            this.westByGauss315Box.Size = new System.Drawing.Size(100, 20);
            this.westByGauss315Box.TabIndex = 77;
            // 
            // westBxGauss315Box
            // 
            this.westBxGauss315Box.Location = new System.Drawing.Point(926, 224);
            this.westBxGauss315Box.Name = "westBxGauss315Box";
            this.westBxGauss315Box.Size = new System.Drawing.Size(100, 20);
            this.westBxGauss315Box.TabIndex = 76;
            // 
            // westGzg315Box
            // 
            this.westGzg315Box.Location = new System.Drawing.Point(926, 188);
            this.westGzg315Box.Name = "westGzg315Box";
            this.westGzg315Box.Size = new System.Drawing.Size(100, 20);
            this.westGzg315Box.TabIndex = 75;
            // 
            // westGyg315Box
            // 
            this.westGyg315Box.Location = new System.Drawing.Point(926, 151);
            this.westGyg315Box.Name = "westGyg315Box";
            this.westGyg315Box.Size = new System.Drawing.Size(100, 20);
            this.westGyg315Box.TabIndex = 74;
            // 
            // westGxg315Box
            // 
            this.westGxg315Box.Location = new System.Drawing.Point(926, 115);
            this.westGxg315Box.Name = "westGxg315Box";
            this.westGxg315Box.Size = new System.Drawing.Size(100, 20);
            this.westGxg315Box.TabIndex = 73;
            // 
            // westBzGauss360Box
            // 
            this.westBzGauss360Box.Location = new System.Drawing.Point(1054, 298);
            this.westBzGauss360Box.Name = "westBzGauss360Box";
            this.westBzGauss360Box.Size = new System.Drawing.Size(100, 20);
            this.westBzGauss360Box.TabIndex = 84;
            // 
            // westByGauss360Box
            // 
            this.westByGauss360Box.Location = new System.Drawing.Point(1054, 261);
            this.westByGauss360Box.Name = "westByGauss360Box";
            this.westByGauss360Box.Size = new System.Drawing.Size(100, 20);
            this.westByGauss360Box.TabIndex = 83;
            // 
            // westBxGauss360Box
            // 
            this.westBxGauss360Box.Location = new System.Drawing.Point(1054, 224);
            this.westBxGauss360Box.Name = "westBxGauss360Box";
            this.westBxGauss360Box.Size = new System.Drawing.Size(100, 20);
            this.westBxGauss360Box.TabIndex = 82;
            // 
            // westGzg360Box
            // 
            this.westGzg360Box.Location = new System.Drawing.Point(1054, 188);
            this.westGzg360Box.Name = "westGzg360Box";
            this.westGzg360Box.Size = new System.Drawing.Size(100, 20);
            this.westGzg360Box.TabIndex = 81;
            // 
            // westGyg360Box
            // 
            this.westGyg360Box.Location = new System.Drawing.Point(1054, 151);
            this.westGyg360Box.Name = "westGyg360Box";
            this.westGyg360Box.Size = new System.Drawing.Size(100, 20);
            this.westGyg360Box.TabIndex = 80;
            // 
            // westGxg360Box
            // 
            this.westGxg360Box.Location = new System.Drawing.Point(1054, 115);
            this.westGxg360Box.Name = "westGxg360Box";
            this.westGxg360Box.Size = new System.Drawing.Size(100, 20);
            this.westGxg360Box.TabIndex = 79;
            // 
            // eastBzGauss180Box
            // 
            this.eastBzGauss180Box.Location = new System.Drawing.Point(555, 601);
            this.eastBzGauss180Box.Name = "eastBzGauss180Box";
            this.eastBzGauss180Box.Size = new System.Drawing.Size(100, 20);
            this.eastBzGauss180Box.TabIndex = 122;
            // 
            // eastByGauss180Box
            // 
            this.eastByGauss180Box.Location = new System.Drawing.Point(555, 564);
            this.eastByGauss180Box.Name = "eastByGauss180Box";
            this.eastByGauss180Box.Size = new System.Drawing.Size(100, 20);
            this.eastByGauss180Box.TabIndex = 121;
            // 
            // eastBxGauss180Box
            // 
            this.eastBxGauss180Box.Location = new System.Drawing.Point(555, 527);
            this.eastBxGauss180Box.Name = "eastBxGauss180Box";
            this.eastBxGauss180Box.Size = new System.Drawing.Size(100, 20);
            this.eastBxGauss180Box.TabIndex = 120;
            // 
            // eastGzg180Box
            // 
            this.eastGzg180Box.Location = new System.Drawing.Point(555, 491);
            this.eastGzg180Box.Name = "eastGzg180Box";
            this.eastGzg180Box.Size = new System.Drawing.Size(100, 20);
            this.eastGzg180Box.TabIndex = 119;
            // 
            // eastGyg180Box
            // 
            this.eastGyg180Box.Location = new System.Drawing.Point(555, 454);
            this.eastGyg180Box.Name = "eastGyg180Box";
            this.eastGyg180Box.Size = new System.Drawing.Size(100, 20);
            this.eastGyg180Box.TabIndex = 118;
            // 
            // eastGxg180Box
            // 
            this.eastGxg180Box.Location = new System.Drawing.Point(555, 418);
            this.eastGxg180Box.Name = "eastGxg180Box";
            this.eastGxg180Box.Size = new System.Drawing.Size(100, 20);
            this.eastGxg180Box.TabIndex = 117;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label12.Location = new System.Drawing.Point(69, 597);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(101, 26);
            this.label12.TabIndex = 104;
            this.label12.Text = "BzGauss";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label13.Location = new System.Drawing.Point(69, 557);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(101, 26);
            this.label13.TabIndex = 103;
            this.label13.Text = "ByGauss";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label14.Location = new System.Drawing.Point(69, 520);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(101, 26);
            this.label14.TabIndex = 102;
            this.label14.Text = "BxGauss";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label15.Location = new System.Drawing.Point(90, 484);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(57, 26);
            this.label15.TabIndex = 101;
            this.label15.Text = "GzG";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label16.Location = new System.Drawing.Point(90, 447);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(57, 26);
            this.label16.TabIndex = 100;
            this.label16.Text = "GyG";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label17.Location = new System.Drawing.Point(90, 411);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(57, 26);
            this.label17.TabIndex = 99;
            this.label17.Text = "GxG";
            // 
            // eastBzGauss45Box
            // 
            this.eastBzGauss45Box.Location = new System.Drawing.Point(176, 601);
            this.eastBzGauss45Box.Name = "eastBzGauss45Box";
            this.eastBzGauss45Box.Size = new System.Drawing.Size(100, 20);
            this.eastBzGauss45Box.TabIndex = 98;
            // 
            // eastByGauss45Box
            // 
            this.eastByGauss45Box.Location = new System.Drawing.Point(176, 564);
            this.eastByGauss45Box.Name = "eastByGauss45Box";
            this.eastByGauss45Box.Size = new System.Drawing.Size(100, 20);
            this.eastByGauss45Box.TabIndex = 97;
            // 
            // eastBxGauss45Box
            // 
            this.eastBxGauss45Box.Location = new System.Drawing.Point(176, 527);
            this.eastBxGauss45Box.Name = "eastBxGauss45Box";
            this.eastBxGauss45Box.Size = new System.Drawing.Size(100, 20);
            this.eastBxGauss45Box.TabIndex = 96;
            // 
            // eastGzg45Box
            // 
            this.eastGzg45Box.Location = new System.Drawing.Point(176, 491);
            this.eastGzg45Box.Name = "eastGzg45Box";
            this.eastGzg45Box.Size = new System.Drawing.Size(100, 20);
            this.eastGzg45Box.TabIndex = 95;
            // 
            // eastGyg45Box
            // 
            this.eastGyg45Box.Location = new System.Drawing.Point(176, 454);
            this.eastGyg45Box.Name = "eastGyg45Box";
            this.eastGyg45Box.Size = new System.Drawing.Size(100, 20);
            this.eastGyg45Box.TabIndex = 94;
            // 
            // eastGxg45Box
            // 
            this.eastGxg45Box.Location = new System.Drawing.Point(176, 418);
            this.eastGxg45Box.Name = "eastGxg45Box";
            this.eastGxg45Box.Size = new System.Drawing.Size(100, 20);
            this.eastGxg45Box.TabIndex = 93;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label38.Location = new System.Drawing.Point(582, 378);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(48, 26);
            this.label38.TabIndex = 88;
            this.label38.Text = "180";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label41.Location = new System.Drawing.Point(208, 378);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(36, 26);
            this.label41.TabIndex = 85;
            this.label41.Text = "45";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label3.Location = new System.Drawing.Point(414, 699);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(125, 52);
            this.label3.TabIndex = 2;
            this.label3.Text = "VERTICAL \r\n   ROLL";
            // 
            // verticalBzGauss180Box
            // 
            this.verticalBzGauss180Box.Location = new System.Drawing.Point(672, 948);
            this.verticalBzGauss180Box.Name = "verticalBzGauss180Box";
            this.verticalBzGauss180Box.Size = new System.Drawing.Size(100, 20);
            this.verticalBzGauss180Box.TabIndex = 184;
            // 
            // verticalByGauss180Box
            // 
            this.verticalByGauss180Box.Location = new System.Drawing.Point(672, 911);
            this.verticalByGauss180Box.Name = "verticalByGauss180Box";
            this.verticalByGauss180Box.Size = new System.Drawing.Size(100, 20);
            this.verticalByGauss180Box.TabIndex = 183;
            // 
            // verticalBxGauss180Box
            // 
            this.verticalBxGauss180Box.Location = new System.Drawing.Point(672, 874);
            this.verticalBxGauss180Box.Name = "verticalBxGauss180Box";
            this.verticalBxGauss180Box.Size = new System.Drawing.Size(100, 20);
            this.verticalBxGauss180Box.TabIndex = 182;
            // 
            // verticalGzg180Box
            // 
            this.verticalGzg180Box.Location = new System.Drawing.Point(672, 838);
            this.verticalGzg180Box.Name = "verticalGzg180Box";
            this.verticalGzg180Box.Size = new System.Drawing.Size(100, 20);
            this.verticalGzg180Box.TabIndex = 181;
            // 
            // verticalGyg180Box
            // 
            this.verticalGyg180Box.Location = new System.Drawing.Point(672, 801);
            this.verticalGyg180Box.Name = "verticalGyg180Box";
            this.verticalGyg180Box.Size = new System.Drawing.Size(100, 20);
            this.verticalGyg180Box.TabIndex = 180;
            // 
            // verticalGxg180Box
            // 
            this.verticalGxg180Box.Location = new System.Drawing.Point(672, 765);
            this.verticalGxg180Box.Name = "verticalGxg180Box";
            this.verticalGxg180Box.Size = new System.Drawing.Size(100, 20);
            this.verticalGxg180Box.TabIndex = 179;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label20.Location = new System.Drawing.Point(426, 944);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(101, 26);
            this.label20.TabIndex = 166;
            this.label20.Text = "BzGauss";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label21.Location = new System.Drawing.Point(426, 904);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(101, 26);
            this.label21.TabIndex = 165;
            this.label21.Text = "ByGauss";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label22.Location = new System.Drawing.Point(426, 867);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(101, 26);
            this.label22.TabIndex = 164;
            this.label22.Text = "BxGauss";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label23.Location = new System.Drawing.Point(447, 831);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(57, 26);
            this.label23.TabIndex = 163;
            this.label23.Text = "GzG";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label24.Location = new System.Drawing.Point(447, 794);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(57, 26);
            this.label24.TabIndex = 162;
            this.label24.Text = "GyG";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label25.Location = new System.Drawing.Point(447, 758);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(57, 26);
            this.label25.TabIndex = 161;
            this.label25.Text = "GxG";
            // 
            // verticalBzGauss0Box
            // 
            this.verticalBzGauss0Box.Location = new System.Drawing.Point(533, 948);
            this.verticalBzGauss0Box.Name = "verticalBzGauss0Box";
            this.verticalBzGauss0Box.Size = new System.Drawing.Size(100, 20);
            this.verticalBzGauss0Box.TabIndex = 160;
            // 
            // verticalByGauss0Box
            // 
            this.verticalByGauss0Box.Location = new System.Drawing.Point(533, 911);
            this.verticalByGauss0Box.Name = "verticalByGauss0Box";
            this.verticalByGauss0Box.Size = new System.Drawing.Size(100, 20);
            this.verticalByGauss0Box.TabIndex = 159;
            // 
            // verticalBxGauss0Box
            // 
            this.verticalBxGauss0Box.Location = new System.Drawing.Point(533, 874);
            this.verticalBxGauss0Box.Name = "verticalBxGauss0Box";
            this.verticalBxGauss0Box.Size = new System.Drawing.Size(100, 20);
            this.verticalBxGauss0Box.TabIndex = 158;
            // 
            // verticalGzg0Box
            // 
            this.verticalGzg0Box.Location = new System.Drawing.Point(533, 838);
            this.verticalGzg0Box.Name = "verticalGzg0Box";
            this.verticalGzg0Box.Size = new System.Drawing.Size(100, 20);
            this.verticalGzg0Box.TabIndex = 157;
            // 
            // verticalGyg0Box
            // 
            this.verticalGyg0Box.Location = new System.Drawing.Point(533, 801);
            this.verticalGyg0Box.Name = "verticalGyg0Box";
            this.verticalGyg0Box.Size = new System.Drawing.Size(100, 20);
            this.verticalGyg0Box.TabIndex = 156;
            // 
            // verticalGxg0Box
            // 
            this.verticalGxg0Box.Location = new System.Drawing.Point(533, 765);
            this.verticalGxg0Box.Name = "verticalGxg0Box";
            this.verticalGxg0Box.Size = new System.Drawing.Size(100, 20);
            this.verticalGxg0Box.TabIndex = 155;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label44.Location = new System.Drawing.Point(699, 725);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(48, 26);
            this.label44.TabIndex = 150;
            this.label44.Text = "180";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label47.Location = new System.Drawing.Point(565, 725);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(24, 26);
            this.label47.TabIndex = 147;
            this.label47.Text = "0";
            // 
            // eastRoll45Test
            // 
            this.eastRoll45Test.Location = new System.Drawing.Point(188, 641);
            this.eastRoll45Test.Name = "eastRoll45Test";
            this.eastRoll45Test.Size = new System.Drawing.Size(75, 23);
            this.eastRoll45Test.TabIndex = 219;
            this.eastRoll45Test.Text = "START";
            this.eastRoll45Test.UseVisualStyleBackColor = true;
            // 
            // eastRoll180Test
            // 
            this.eastRoll180Test.Location = new System.Drawing.Point(568, 641);
            this.eastRoll180Test.Name = "eastRoll180Test";
            this.eastRoll180Test.Size = new System.Drawing.Size(75, 23);
            this.eastRoll180Test.TabIndex = 222;
            this.eastRoll180Test.Text = "START";
            this.eastRoll180Test.UseVisualStyleBackColor = true;
            // 
            // verticalRoll45Test
            // 
            this.verticalRoll45Test.Location = new System.Drawing.Point(545, 981);
            this.verticalRoll45Test.Name = "verticalRoll45Test";
            this.verticalRoll45Test.Size = new System.Drawing.Size(75, 23);
            this.verticalRoll45Test.TabIndex = 227;
            this.verticalRoll45Test.Text = "START";
            this.verticalRoll45Test.UseVisualStyleBackColor = true;
            // 
            // verticalRoll180Test
            // 
            this.verticalRoll180Test.Location = new System.Drawing.Point(685, 981);
            this.verticalRoll180Test.Name = "verticalRoll180Test";
            this.verticalRoll180Test.Size = new System.Drawing.Size(75, 23);
            this.verticalRoll180Test.TabIndex = 230;
            this.verticalRoll180Test.Text = "START";
            this.verticalRoll180Test.UseVisualStyleBackColor = true;
            // 
            // westRoll45Test
            // 
            this.westRoll45Test.Location = new System.Drawing.Point(188, 336);
            this.westRoll45Test.Name = "westRoll45Test";
            this.westRoll45Test.Size = new System.Drawing.Size(75, 23);
            this.westRoll45Test.TabIndex = 209;
            this.westRoll45Test.Text = "START";
            this.westRoll45Test.UseVisualStyleBackColor = true;
            // 
            // westRoll90Test
            // 
            this.westRoll90Test.Location = new System.Drawing.Point(314, 336);
            this.westRoll90Test.Name = "westRoll90Test";
            this.westRoll90Test.Size = new System.Drawing.Size(75, 23);
            this.westRoll90Test.TabIndex = 212;
            this.westRoll90Test.Text = "START";
            this.westRoll90Test.UseVisualStyleBackColor = true;
            // 
            // westRoll135Test
            // 
            this.westRoll135Test.Location = new System.Drawing.Point(443, 336);
            this.westRoll135Test.Name = "westRoll135Test";
            this.westRoll135Test.Size = new System.Drawing.Size(75, 23);
            this.westRoll135Test.TabIndex = 213;
            this.westRoll135Test.Text = "START";
            this.westRoll135Test.UseVisualStyleBackColor = true;
            // 
            // westRoll180Test
            // 
            this.westRoll180Test.Location = new System.Drawing.Point(568, 336);
            this.westRoll180Test.Name = "westRoll180Test";
            this.westRoll180Test.Size = new System.Drawing.Size(75, 23);
            this.westRoll180Test.TabIndex = 214;
            this.westRoll180Test.Text = "START";
            this.westRoll180Test.UseVisualStyleBackColor = true;
            // 
            // westRoll225Test
            // 
            this.westRoll225Test.Location = new System.Drawing.Point(685, 336);
            this.westRoll225Test.Name = "westRoll225Test";
            this.westRoll225Test.Size = new System.Drawing.Size(75, 23);
            this.westRoll225Test.TabIndex = 215;
            this.westRoll225Test.Text = "START";
            this.westRoll225Test.UseVisualStyleBackColor = true;
            // 
            // westRoll270Test
            // 
            this.westRoll270Test.Location = new System.Drawing.Point(813, 336);
            this.westRoll270Test.Name = "westRoll270Test";
            this.westRoll270Test.Size = new System.Drawing.Size(75, 23);
            this.westRoll270Test.TabIndex = 216;
            this.westRoll270Test.Text = "START";
            this.westRoll270Test.UseVisualStyleBackColor = true;
            // 
            // westRoll315Test
            // 
            this.westRoll315Test.Location = new System.Drawing.Point(937, 336);
            this.westRoll315Test.Name = "westRoll315Test";
            this.westRoll315Test.Size = new System.Drawing.Size(75, 23);
            this.westRoll315Test.TabIndex = 217;
            this.westRoll315Test.Text = "START";
            this.westRoll315Test.UseVisualStyleBackColor = true;
            // 
            // westRoll360Test
            // 
            this.westRoll360Test.Location = new System.Drawing.Point(1065, 336);
            this.westRoll360Test.Name = "westRoll360Test";
            this.westRoll360Test.Size = new System.Drawing.Size(75, 23);
            this.westRoll360Test.TabIndex = 218;
            this.westRoll360Test.Text = "START";
            this.westRoll360Test.UseVisualStyleBackColor = true;
            // 
            // statusBox
            // 
            this.statusBox.BackColor = System.Drawing.Color.Black;
            this.statusBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.statusBox.ForeColor = System.Drawing.Color.Yellow;
            this.statusBox.Location = new System.Drawing.Point(176, 12);
            this.statusBox.Name = "statusBox";
            this.statusBox.Size = new System.Drawing.Size(978, 49);
            this.statusBox.TabIndex = 235;
            this.statusBox.Text = "";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label40.Location = new System.Drawing.Point(334, 378);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(36, 26);
            this.label40.TabIndex = 86;
            this.label40.Text = "90";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label39.Location = new System.Drawing.Point(454, 378);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(48, 26);
            this.label39.TabIndex = 87;
            this.label39.Text = "135";
            // 
            // eastGxg90Box
            // 
            this.eastGxg90Box.Location = new System.Drawing.Point(302, 418);
            this.eastGxg90Box.Name = "eastGxg90Box";
            this.eastGxg90Box.Size = new System.Drawing.Size(100, 20);
            this.eastGxg90Box.TabIndex = 105;
            // 
            // eastGyg90Box
            // 
            this.eastGyg90Box.Location = new System.Drawing.Point(302, 454);
            this.eastGyg90Box.Name = "eastGyg90Box";
            this.eastGyg90Box.Size = new System.Drawing.Size(100, 20);
            this.eastGyg90Box.TabIndex = 106;
            // 
            // eastGzg90Box
            // 
            this.eastGzg90Box.Location = new System.Drawing.Point(302, 491);
            this.eastGzg90Box.Name = "eastGzg90Box";
            this.eastGzg90Box.Size = new System.Drawing.Size(100, 20);
            this.eastGzg90Box.TabIndex = 107;
            // 
            // eastBxGauss90Box
            // 
            this.eastBxGauss90Box.Location = new System.Drawing.Point(302, 527);
            this.eastBxGauss90Box.Name = "eastBxGauss90Box";
            this.eastBxGauss90Box.Size = new System.Drawing.Size(100, 20);
            this.eastBxGauss90Box.TabIndex = 108;
            // 
            // eastByGauss90Box
            // 
            this.eastByGauss90Box.Location = new System.Drawing.Point(302, 564);
            this.eastByGauss90Box.Name = "eastByGauss90Box";
            this.eastByGauss90Box.Size = new System.Drawing.Size(100, 20);
            this.eastByGauss90Box.TabIndex = 109;
            // 
            // eastBzGauss90Box
            // 
            this.eastBzGauss90Box.Location = new System.Drawing.Point(302, 601);
            this.eastBzGauss90Box.Name = "eastBzGauss90Box";
            this.eastBzGauss90Box.Size = new System.Drawing.Size(100, 20);
            this.eastBzGauss90Box.TabIndex = 110;
            // 
            // eastGxg135Box
            // 
            this.eastGxg135Box.Location = new System.Drawing.Point(431, 418);
            this.eastGxg135Box.Name = "eastGxg135Box";
            this.eastGxg135Box.Size = new System.Drawing.Size(100, 20);
            this.eastGxg135Box.TabIndex = 111;
            // 
            // eastGyg135Box
            // 
            this.eastGyg135Box.Location = new System.Drawing.Point(431, 454);
            this.eastGyg135Box.Name = "eastGyg135Box";
            this.eastGyg135Box.Size = new System.Drawing.Size(100, 20);
            this.eastGyg135Box.TabIndex = 112;
            // 
            // eastGzg135Box
            // 
            this.eastGzg135Box.Location = new System.Drawing.Point(431, 491);
            this.eastGzg135Box.Name = "eastGzg135Box";
            this.eastGzg135Box.Size = new System.Drawing.Size(100, 20);
            this.eastGzg135Box.TabIndex = 113;
            // 
            // eastBxGauss135Box
            // 
            this.eastBxGauss135Box.Location = new System.Drawing.Point(431, 527);
            this.eastBxGauss135Box.Name = "eastBxGauss135Box";
            this.eastBxGauss135Box.Size = new System.Drawing.Size(100, 20);
            this.eastBxGauss135Box.TabIndex = 114;
            // 
            // eastByGauss135Box
            // 
            this.eastByGauss135Box.Location = new System.Drawing.Point(431, 564);
            this.eastByGauss135Box.Name = "eastByGauss135Box";
            this.eastByGauss135Box.Size = new System.Drawing.Size(100, 20);
            this.eastByGauss135Box.TabIndex = 115;
            // 
            // eastBzGauss135Box
            // 
            this.eastBzGauss135Box.Location = new System.Drawing.Point(431, 601);
            this.eastBzGauss135Box.Name = "eastBzGauss135Box";
            this.eastBzGauss135Box.Size = new System.Drawing.Size(100, 20);
            this.eastBzGauss135Box.TabIndex = 116;
            // 
            // eastRoll90Test
            // 
            this.eastRoll90Test.Location = new System.Drawing.Point(314, 641);
            this.eastRoll90Test.Name = "eastRoll90Test";
            this.eastRoll90Test.Size = new System.Drawing.Size(75, 23);
            this.eastRoll90Test.TabIndex = 220;
            this.eastRoll90Test.Text = "START";
            this.eastRoll90Test.UseVisualStyleBackColor = true;
            // 
            // eastRoll135Test
            // 
            this.eastRoll135Test.Location = new System.Drawing.Point(443, 641);
            this.eastRoll135Test.Name = "eastRoll135Test";
            this.eastRoll135Test.Size = new System.Drawing.Size(75, 23);
            this.eastRoll135Test.TabIndex = 221;
            this.eastRoll135Test.Text = "START";
            this.eastRoll135Test.UseVisualStyleBackColor = true;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label37.Location = new System.Drawing.Point(699, 378);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(48, 26);
            this.label37.TabIndex = 89;
            this.label37.Text = "225";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label36.Location = new System.Drawing.Point(827, 378);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(48, 26);
            this.label36.TabIndex = 90;
            this.label36.Text = "270";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label19.Location = new System.Drawing.Point(953, 378);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(48, 26);
            this.label19.TabIndex = 91;
            this.label19.Text = "315";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.label18.Location = new System.Drawing.Point(1074, 378);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(48, 26);
            this.label18.TabIndex = 92;
            this.label18.Text = "360";
            // 
            // eastGxg225Box
            // 
            this.eastGxg225Box.Location = new System.Drawing.Point(674, 418);
            this.eastGxg225Box.Name = "eastGxg225Box";
            this.eastGxg225Box.Size = new System.Drawing.Size(100, 20);
            this.eastGxg225Box.TabIndex = 123;
            // 
            // eastGyg225Box
            // 
            this.eastGyg225Box.Location = new System.Drawing.Point(674, 454);
            this.eastGyg225Box.Name = "eastGyg225Box";
            this.eastGyg225Box.Size = new System.Drawing.Size(100, 20);
            this.eastGyg225Box.TabIndex = 124;
            // 
            // eastGzg225Box
            // 
            this.eastGzg225Box.Location = new System.Drawing.Point(674, 491);
            this.eastGzg225Box.Name = "eastGzg225Box";
            this.eastGzg225Box.Size = new System.Drawing.Size(100, 20);
            this.eastGzg225Box.TabIndex = 125;
            // 
            // eastBxGauss225Box
            // 
            this.eastBxGauss225Box.Location = new System.Drawing.Point(674, 527);
            this.eastBxGauss225Box.Name = "eastBxGauss225Box";
            this.eastBxGauss225Box.Size = new System.Drawing.Size(100, 20);
            this.eastBxGauss225Box.TabIndex = 126;
            // 
            // eastByGauss225Box
            // 
            this.eastByGauss225Box.Location = new System.Drawing.Point(674, 564);
            this.eastByGauss225Box.Name = "eastByGauss225Box";
            this.eastByGauss225Box.Size = new System.Drawing.Size(100, 20);
            this.eastByGauss225Box.TabIndex = 127;
            // 
            // eastBzGauss225Box
            // 
            this.eastBzGauss225Box.Location = new System.Drawing.Point(674, 601);
            this.eastBzGauss225Box.Name = "eastBzGauss225Box";
            this.eastBzGauss225Box.Size = new System.Drawing.Size(100, 20);
            this.eastBzGauss225Box.TabIndex = 128;
            // 
            // eastGxg270Box
            // 
            this.eastGxg270Box.Location = new System.Drawing.Point(801, 418);
            this.eastGxg270Box.Name = "eastGxg270Box";
            this.eastGxg270Box.Size = new System.Drawing.Size(100, 20);
            this.eastGxg270Box.TabIndex = 129;
            // 
            // eastGyg270Box
            // 
            this.eastGyg270Box.Location = new System.Drawing.Point(801, 454);
            this.eastGyg270Box.Name = "eastGyg270Box";
            this.eastGyg270Box.Size = new System.Drawing.Size(100, 20);
            this.eastGyg270Box.TabIndex = 130;
            // 
            // eastGzg270Box
            // 
            this.eastGzg270Box.Location = new System.Drawing.Point(801, 491);
            this.eastGzg270Box.Name = "eastGzg270Box";
            this.eastGzg270Box.Size = new System.Drawing.Size(100, 20);
            this.eastGzg270Box.TabIndex = 131;
            // 
            // eastBxGauss270Box
            // 
            this.eastBxGauss270Box.Location = new System.Drawing.Point(801, 527);
            this.eastBxGauss270Box.Name = "eastBxGauss270Box";
            this.eastBxGauss270Box.Size = new System.Drawing.Size(100, 20);
            this.eastBxGauss270Box.TabIndex = 132;
            // 
            // eastByGauss270Box
            // 
            this.eastByGauss270Box.Location = new System.Drawing.Point(801, 564);
            this.eastByGauss270Box.Name = "eastByGauss270Box";
            this.eastByGauss270Box.Size = new System.Drawing.Size(100, 20);
            this.eastByGauss270Box.TabIndex = 133;
            // 
            // eastBzGauss270Box
            // 
            this.eastBzGauss270Box.Location = new System.Drawing.Point(801, 601);
            this.eastBzGauss270Box.Name = "eastBzGauss270Box";
            this.eastBzGauss270Box.Size = new System.Drawing.Size(100, 20);
            this.eastBzGauss270Box.TabIndex = 134;
            // 
            // eastGxg315Box
            // 
            this.eastGxg315Box.Location = new System.Drawing.Point(926, 418);
            this.eastGxg315Box.Name = "eastGxg315Box";
            this.eastGxg315Box.Size = new System.Drawing.Size(100, 20);
            this.eastGxg315Box.TabIndex = 135;
            // 
            // eastGyg315Box
            // 
            this.eastGyg315Box.Location = new System.Drawing.Point(926, 454);
            this.eastGyg315Box.Name = "eastGyg315Box";
            this.eastGyg315Box.Size = new System.Drawing.Size(100, 20);
            this.eastGyg315Box.TabIndex = 136;
            // 
            // eastGzg315Box
            // 
            this.eastGzg315Box.Location = new System.Drawing.Point(926, 491);
            this.eastGzg315Box.Name = "eastGzg315Box";
            this.eastGzg315Box.Size = new System.Drawing.Size(100, 20);
            this.eastGzg315Box.TabIndex = 137;
            // 
            // eastBxGauss315Box
            // 
            this.eastBxGauss315Box.Location = new System.Drawing.Point(926, 527);
            this.eastBxGauss315Box.Name = "eastBxGauss315Box";
            this.eastBxGauss315Box.Size = new System.Drawing.Size(100, 20);
            this.eastBxGauss315Box.TabIndex = 138;
            // 
            // eastByGauss315Box
            // 
            this.eastByGauss315Box.Location = new System.Drawing.Point(926, 564);
            this.eastByGauss315Box.Name = "eastByGauss315Box";
            this.eastByGauss315Box.Size = new System.Drawing.Size(100, 20);
            this.eastByGauss315Box.TabIndex = 139;
            // 
            // eastBzGauss315Box
            // 
            this.eastBzGauss315Box.Location = new System.Drawing.Point(926, 601);
            this.eastBzGauss315Box.Name = "eastBzGauss315Box";
            this.eastBzGauss315Box.Size = new System.Drawing.Size(100, 20);
            this.eastBzGauss315Box.TabIndex = 140;
            // 
            // eastGxg360Box
            // 
            this.eastGxg360Box.Location = new System.Drawing.Point(1054, 418);
            this.eastGxg360Box.Name = "eastGxg360Box";
            this.eastGxg360Box.Size = new System.Drawing.Size(100, 20);
            this.eastGxg360Box.TabIndex = 141;
            // 
            // eastGyg360Box
            // 
            this.eastGyg360Box.Location = new System.Drawing.Point(1054, 454);
            this.eastGyg360Box.Name = "eastGyg360Box";
            this.eastGyg360Box.Size = new System.Drawing.Size(100, 20);
            this.eastGyg360Box.TabIndex = 142;
            // 
            // eastGzg360Box
            // 
            this.eastGzg360Box.Location = new System.Drawing.Point(1054, 491);
            this.eastGzg360Box.Name = "eastGzg360Box";
            this.eastGzg360Box.Size = new System.Drawing.Size(100, 20);
            this.eastGzg360Box.TabIndex = 143;
            // 
            // eastBxGauss360Box
            // 
            this.eastBxGauss360Box.Location = new System.Drawing.Point(1054, 527);
            this.eastBxGauss360Box.Name = "eastBxGauss360Box";
            this.eastBxGauss360Box.Size = new System.Drawing.Size(100, 20);
            this.eastBxGauss360Box.TabIndex = 144;
            // 
            // eastByGauss360Box
            // 
            this.eastByGauss360Box.Location = new System.Drawing.Point(1054, 564);
            this.eastByGauss360Box.Name = "eastByGauss360Box";
            this.eastByGauss360Box.Size = new System.Drawing.Size(100, 20);
            this.eastByGauss360Box.TabIndex = 145;
            // 
            // eastBzGauss360Box
            // 
            this.eastBzGauss360Box.Location = new System.Drawing.Point(1054, 601);
            this.eastBzGauss360Box.Name = "eastBzGauss360Box";
            this.eastBzGauss360Box.Size = new System.Drawing.Size(100, 20);
            this.eastBzGauss360Box.TabIndex = 146;
            // 
            // eastRoll225Test
            // 
            this.eastRoll225Test.Location = new System.Drawing.Point(685, 641);
            this.eastRoll225Test.Name = "eastRoll225Test";
            this.eastRoll225Test.Size = new System.Drawing.Size(75, 23);
            this.eastRoll225Test.TabIndex = 223;
            this.eastRoll225Test.Text = "START";
            this.eastRoll225Test.UseVisualStyleBackColor = true;
            // 
            // eastRoll270Test
            // 
            this.eastRoll270Test.Location = new System.Drawing.Point(813, 641);
            this.eastRoll270Test.Name = "eastRoll270Test";
            this.eastRoll270Test.Size = new System.Drawing.Size(75, 23);
            this.eastRoll270Test.TabIndex = 224;
            this.eastRoll270Test.Text = "START";
            this.eastRoll270Test.UseVisualStyleBackColor = true;
            // 
            // eastRoll315Test
            // 
            this.eastRoll315Test.Location = new System.Drawing.Point(937, 641);
            this.eastRoll315Test.Name = "eastRoll315Test";
            this.eastRoll315Test.Size = new System.Drawing.Size(75, 23);
            this.eastRoll315Test.TabIndex = 225;
            this.eastRoll315Test.Text = "START";
            this.eastRoll315Test.UseVisualStyleBackColor = true;
            // 
            // eastRoll360Test
            // 
            this.eastRoll360Test.Location = new System.Drawing.Point(1065, 641);
            this.eastRoll360Test.Name = "eastRoll360Test";
            this.eastRoll360Test.Size = new System.Drawing.Size(75, 23);
            this.eastRoll360Test.TabIndex = 226;
            this.eastRoll360Test.Text = "START";
            this.eastRoll360Test.UseVisualStyleBackColor = true;
            // 
            // rollTestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1230, 1058);
            this.Controls.Add(this.statusBox);
            this.Controls.Add(this.verticalRoll180Test);
            this.Controls.Add(this.verticalRoll45Test);
            this.Controls.Add(this.eastRoll360Test);
            this.Controls.Add(this.eastRoll315Test);
            this.Controls.Add(this.eastRoll270Test);
            this.Controls.Add(this.eastRoll225Test);
            this.Controls.Add(this.eastRoll180Test);
            this.Controls.Add(this.eastRoll135Test);
            this.Controls.Add(this.eastRoll90Test);
            this.Controls.Add(this.eastRoll45Test);
            this.Controls.Add(this.westRoll360Test);
            this.Controls.Add(this.westRoll315Test);
            this.Controls.Add(this.westRoll270Test);
            this.Controls.Add(this.westRoll225Test);
            this.Controls.Add(this.westRoll180Test);
            this.Controls.Add(this.westRoll135Test);
            this.Controls.Add(this.westRoll90Test);
            this.Controls.Add(this.westRoll45Test);
            this.Controls.Add(this.verticalBzGauss180Box);
            this.Controls.Add(this.verticalByGauss180Box);
            this.Controls.Add(this.verticalBxGauss180Box);
            this.Controls.Add(this.verticalGzg180Box);
            this.Controls.Add(this.verticalGyg180Box);
            this.Controls.Add(this.verticalGxg180Box);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.verticalBzGauss0Box);
            this.Controls.Add(this.verticalByGauss0Box);
            this.Controls.Add(this.verticalBxGauss0Box);
            this.Controls.Add(this.verticalGzg0Box);
            this.Controls.Add(this.verticalGyg0Box);
            this.Controls.Add(this.verticalGxg0Box);
            this.Controls.Add(this.label44);
            this.Controls.Add(this.label47);
            this.Controls.Add(this.eastBzGauss360Box);
            this.Controls.Add(this.eastByGauss360Box);
            this.Controls.Add(this.eastBxGauss360Box);
            this.Controls.Add(this.eastGzg360Box);
            this.Controls.Add(this.eastGyg360Box);
            this.Controls.Add(this.eastGxg360Box);
            this.Controls.Add(this.eastBzGauss315Box);
            this.Controls.Add(this.eastByGauss315Box);
            this.Controls.Add(this.eastBxGauss315Box);
            this.Controls.Add(this.eastGzg315Box);
            this.Controls.Add(this.eastGyg315Box);
            this.Controls.Add(this.eastGxg315Box);
            this.Controls.Add(this.eastBzGauss270Box);
            this.Controls.Add(this.eastByGauss270Box);
            this.Controls.Add(this.eastBxGauss270Box);
            this.Controls.Add(this.eastGzg270Box);
            this.Controls.Add(this.eastGyg270Box);
            this.Controls.Add(this.eastGxg270Box);
            this.Controls.Add(this.eastBzGauss225Box);
            this.Controls.Add(this.eastByGauss225Box);
            this.Controls.Add(this.eastBxGauss225Box);
            this.Controls.Add(this.eastGzg225Box);
            this.Controls.Add(this.eastGyg225Box);
            this.Controls.Add(this.eastGxg225Box);
            this.Controls.Add(this.eastBzGauss180Box);
            this.Controls.Add(this.eastByGauss180Box);
            this.Controls.Add(this.eastBxGauss180Box);
            this.Controls.Add(this.eastGzg180Box);
            this.Controls.Add(this.eastGyg180Box);
            this.Controls.Add(this.eastGxg180Box);
            this.Controls.Add(this.eastBzGauss135Box);
            this.Controls.Add(this.eastByGauss135Box);
            this.Controls.Add(this.eastBxGauss135Box);
            this.Controls.Add(this.eastGzg135Box);
            this.Controls.Add(this.eastGyg135Box);
            this.Controls.Add(this.eastGxg135Box);
            this.Controls.Add(this.eastBzGauss90Box);
            this.Controls.Add(this.eastByGauss90Box);
            this.Controls.Add(this.eastBxGauss90Box);
            this.Controls.Add(this.eastGzg90Box);
            this.Controls.Add(this.eastGyg90Box);
            this.Controls.Add(this.eastGxg90Box);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.eastBzGauss45Box);
            this.Controls.Add(this.eastByGauss45Box);
            this.Controls.Add(this.eastBxGauss45Box);
            this.Controls.Add(this.eastGzg45Box);
            this.Controls.Add(this.eastGyg45Box);
            this.Controls.Add(this.eastGxg45Box);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.label40);
            this.Controls.Add(this.label41);
            this.Controls.Add(this.westBzGauss360Box);
            this.Controls.Add(this.westByGauss360Box);
            this.Controls.Add(this.westBxGauss360Box);
            this.Controls.Add(this.westGzg360Box);
            this.Controls.Add(this.westGyg360Box);
            this.Controls.Add(this.westGxg360Box);
            this.Controls.Add(this.westBzGauss315Box);
            this.Controls.Add(this.westByGauss315Box);
            this.Controls.Add(this.westBxGauss315Box);
            this.Controls.Add(this.westGzg315Box);
            this.Controls.Add(this.westGyg315Box);
            this.Controls.Add(this.westGxg315Box);
            this.Controls.Add(this.westBzGauss270Box);
            this.Controls.Add(this.westByGauss270Box);
            this.Controls.Add(this.westBxGauss270Box);
            this.Controls.Add(this.westGzg270Box);
            this.Controls.Add(this.westGyg270Box);
            this.Controls.Add(this.westGxg270Box);
            this.Controls.Add(this.westBzGauss225Box);
            this.Controls.Add(this.westByGauss225Box);
            this.Controls.Add(this.westBxGauss225Box);
            this.Controls.Add(this.westGzg225Box);
            this.Controls.Add(this.westGyg225Box);
            this.Controls.Add(this.westGxg225Box);
            this.Controls.Add(this.westBzGauss180Box);
            this.Controls.Add(this.westByGauss180Box);
            this.Controls.Add(this.westBxGauss180Box);
            this.Controls.Add(this.westGzg180Box);
            this.Controls.Add(this.westGyg180Box);
            this.Controls.Add(this.westGxg180Box);
            this.Controls.Add(this.westBzGauss135Box);
            this.Controls.Add(this.westByGauss135Box);
            this.Controls.Add(this.westBxGauss135Box);
            this.Controls.Add(this.westGzg135Box);
            this.Controls.Add(this.westGyg135Box);
            this.Controls.Add(this.westGxg135Box);
            this.Controls.Add(this.westBzGauss90Box);
            this.Controls.Add(this.westByGauss90Box);
            this.Controls.Add(this.westBxGauss90Box);
            this.Controls.Add(this.westGzg90Box);
            this.Controls.Add(this.westGyg90Box);
            this.Controls.Add(this.westGxg90Box);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.westBzGauss45Box);
            this.Controls.Add(this.westByGauss45Box);
            this.Controls.Add(this.westBxGauss45Box);
            this.Controls.Add(this.westGzg45Box);
            this.Controls.Add(this.westGyg45Box);
            this.Controls.Add(this.westGxg45Box);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "rollTestForm";
            this.Text = "ROLL TEST";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox westGxg45Box;
        private System.Windows.Forms.TextBox westGyg45Box;
        private System.Windows.Forms.TextBox westGzg45Box;
        private System.Windows.Forms.TextBox westBxGauss45Box;
        private System.Windows.Forms.TextBox westByGauss45Box;
        private System.Windows.Forms.TextBox westBzGauss45Box;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox westBzGauss90Box;
        private System.Windows.Forms.TextBox westByGauss90Box;
        private System.Windows.Forms.TextBox westBxGauss90Box;
        private System.Windows.Forms.TextBox westGzg90Box;
        private System.Windows.Forms.TextBox westGyg90Box;
        private System.Windows.Forms.TextBox westGxg90Box;
        private System.Windows.Forms.TextBox westBzGauss135Box;
        private System.Windows.Forms.TextBox westByGauss135Box;
        private System.Windows.Forms.TextBox westBxGauss135Box;
        private System.Windows.Forms.TextBox westGzg135Box;
        private System.Windows.Forms.TextBox westGyg135Box;
        private System.Windows.Forms.TextBox westGxg135Box;
        private System.Windows.Forms.TextBox westBzGauss180Box;
        private System.Windows.Forms.TextBox westByGauss180Box;
        private System.Windows.Forms.TextBox westBxGauss180Box;
        private System.Windows.Forms.TextBox westGzg180Box;
        private System.Windows.Forms.TextBox westGyg180Box;
        private System.Windows.Forms.TextBox westGxg180Box;
        private System.Windows.Forms.TextBox westBzGauss225Box;
        private System.Windows.Forms.TextBox westByGauss225Box;
        private System.Windows.Forms.TextBox westBxGauss225Box;
        private System.Windows.Forms.TextBox westGzg225Box;
        private System.Windows.Forms.TextBox westGyg225Box;
        private System.Windows.Forms.TextBox westGxg225Box;
        private System.Windows.Forms.TextBox westBzGauss270Box;
        private System.Windows.Forms.TextBox westByGauss270Box;
        private System.Windows.Forms.TextBox westBxGauss270Box;
        private System.Windows.Forms.TextBox westGzg270Box;
        private System.Windows.Forms.TextBox westGyg270Box;
        private System.Windows.Forms.TextBox westGxg270Box;
        private System.Windows.Forms.TextBox westBzGauss315Box;
        private System.Windows.Forms.TextBox westByGauss315Box;
        private System.Windows.Forms.TextBox westBxGauss315Box;
        private System.Windows.Forms.TextBox westGzg315Box;
        private System.Windows.Forms.TextBox westGyg315Box;
        private System.Windows.Forms.TextBox westGxg315Box;
        private System.Windows.Forms.TextBox westBzGauss360Box;
        private System.Windows.Forms.TextBox westByGauss360Box;
        private System.Windows.Forms.TextBox westBxGauss360Box;
        private System.Windows.Forms.TextBox westGzg360Box;
        private System.Windows.Forms.TextBox westGyg360Box;
        private System.Windows.Forms.TextBox westGxg360Box;
        private System.Windows.Forms.TextBox eastBzGauss180Box;
        private System.Windows.Forms.TextBox eastByGauss180Box;
        private System.Windows.Forms.TextBox eastBxGauss180Box;
        private System.Windows.Forms.TextBox eastGzg180Box;
        private System.Windows.Forms.TextBox eastGyg180Box;
        private System.Windows.Forms.TextBox eastGxg180Box;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox eastBzGauss45Box;
        private System.Windows.Forms.TextBox eastByGauss45Box;
        private System.Windows.Forms.TextBox eastBxGauss45Box;
        private System.Windows.Forms.TextBox eastGzg45Box;
        private System.Windows.Forms.TextBox eastGyg45Box;
        private System.Windows.Forms.TextBox eastGxg45Box;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox verticalBzGauss180Box;
        private System.Windows.Forms.TextBox verticalByGauss180Box;
        private System.Windows.Forms.TextBox verticalBxGauss180Box;
        private System.Windows.Forms.TextBox verticalGzg180Box;
        private System.Windows.Forms.TextBox verticalGyg180Box;
        private System.Windows.Forms.TextBox verticalGxg180Box;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox verticalBzGauss0Box;
        private System.Windows.Forms.TextBox verticalByGauss0Box;
        private System.Windows.Forms.TextBox verticalBxGauss0Box;
        private System.Windows.Forms.TextBox verticalGzg0Box;
        private System.Windows.Forms.TextBox verticalGyg0Box;
        private System.Windows.Forms.TextBox verticalGxg0Box;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Button eastRoll45Test;
        private System.Windows.Forms.Button eastRoll180Test;
        private System.Windows.Forms.Button verticalRoll45Test;
        private System.Windows.Forms.Button verticalRoll180Test;
        private System.Windows.Forms.Button westRoll45Test;
        private System.Windows.Forms.Button westRoll90Test;
        private System.Windows.Forms.Button westRoll135Test;
        private System.Windows.Forms.Button westRoll180Test;
        private System.Windows.Forms.Button westRoll225Test;
        private System.Windows.Forms.Button westRoll270Test;
        private System.Windows.Forms.Button westRoll315Test;
        private System.Windows.Forms.Button westRoll360Test;
        private System.Windows.Forms.RichTextBox statusBox;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox eastGxg90Box;
        private System.Windows.Forms.TextBox eastGyg90Box;
        private System.Windows.Forms.TextBox eastGzg90Box;
        private System.Windows.Forms.TextBox eastBxGauss90Box;
        private System.Windows.Forms.TextBox eastByGauss90Box;
        private System.Windows.Forms.TextBox eastBzGauss90Box;
        private System.Windows.Forms.TextBox eastGxg135Box;
        private System.Windows.Forms.TextBox eastGyg135Box;
        private System.Windows.Forms.TextBox eastGzg135Box;
        private System.Windows.Forms.TextBox eastBxGauss135Box;
        private System.Windows.Forms.TextBox eastByGauss135Box;
        private System.Windows.Forms.TextBox eastBzGauss135Box;
        private System.Windows.Forms.Button eastRoll90Test;
        private System.Windows.Forms.Button eastRoll135Test;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox eastGxg225Box;
        private System.Windows.Forms.TextBox eastGyg225Box;
        private System.Windows.Forms.TextBox eastGzg225Box;
        private System.Windows.Forms.TextBox eastBxGauss225Box;
        private System.Windows.Forms.TextBox eastByGauss225Box;
        private System.Windows.Forms.TextBox eastBzGauss225Box;
        private System.Windows.Forms.TextBox eastGxg270Box;
        private System.Windows.Forms.TextBox eastGyg270Box;
        private System.Windows.Forms.TextBox eastGzg270Box;
        private System.Windows.Forms.TextBox eastBxGauss270Box;
        private System.Windows.Forms.TextBox eastByGauss270Box;
        private System.Windows.Forms.TextBox eastBzGauss270Box;
        private System.Windows.Forms.TextBox eastGxg315Box;
        private System.Windows.Forms.TextBox eastGyg315Box;
        private System.Windows.Forms.TextBox eastGzg315Box;
        private System.Windows.Forms.TextBox eastBxGauss315Box;
        private System.Windows.Forms.TextBox eastByGauss315Box;
        private System.Windows.Forms.TextBox eastBzGauss315Box;
        private System.Windows.Forms.TextBox eastGxg360Box;
        private System.Windows.Forms.TextBox eastGyg360Box;
        private System.Windows.Forms.TextBox eastGzg360Box;
        private System.Windows.Forms.TextBox eastBxGauss360Box;
        private System.Windows.Forms.TextBox eastByGauss360Box;
        private System.Windows.Forms.TextBox eastBzGauss360Box;
        private System.Windows.Forms.Button eastRoll225Test;
        private System.Windows.Forms.Button eastRoll270Test;
        private System.Windows.Forms.Button eastRoll315Test;
        private System.Windows.Forms.Button eastRoll360Test;
    }
}