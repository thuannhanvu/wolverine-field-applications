﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Wolverine_Field_Applications
{
    public partial class gammaConfigForm : Form
    {
        MainForm localMF = new MainForm();

        public gammaConfigForm(MainForm mF)
        {
            InitializeComponent();
            localMF = mF;
            try
            {
                calibratorApiBox.Text = localMF.getApiCalibrateValue().ToString();
            }catch (Exception ex) { }
        }

        private async void setCalibratorApiButton_Click(object sender, EventArgs e)
        {
            try
            {
                localMF.setCalibratorApi(Convert.ToSingle(calibratorApiBox.Text));
                localMF.saveConfig();
                localMF.checkForConfig();
                await Task.Delay(100);
                this.Dispose();
            }
            catch { }
        }
    }
}
