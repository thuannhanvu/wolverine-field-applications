﻿//--------------------------------------|---------------------------------------
//                                PC RMS V1.0.0.0
//                                File: ErrorDisplay.cs
//------------------------------------------------------------------------------
// Author: Thuan Vu
// Created on April 14, 2021
//--------------------------------------|---------------------------------------
//  This drawing/document is copyright and the property of
//  Wolverine OilField Technologies LLC. and Remote Measurement Systems Ltd.
//  It must not be copied (in whole or in part),
//  used for manufacture or otherwise disclosed without prior written consent.
//  Any  copies of this drawing/document made by any method must also
//  include a copy of this legend. This document is supplied  without
//  liability for errors or omissions.
//
// (c) COPYRIGHT WOLVERINE OILFIELD TECHNOLOGIES LLC. AND
//               REMOTE MEASUREMENT SYSTEMS LIMITED 2021.
//-----------------------------------------------------------------------------
using System;
using System.Windows.Forms;

namespace PC_PDD
{
    public partial class ErrorDisplay : Form
    {
        public ErrorDisplay()
        {
            InitializeComponent();
            this.MinimizeBox = false;
            this.MaximizeBox = false;
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
        }

        public void setMessage(string ex)
        {
            errorTextBox.Text = ex;
            errorTextBox.SelectAll();
            errorTextBox.SelectionAlignment = HorizontalAlignment.Center;
        }

        private void ConfirmButton_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}
