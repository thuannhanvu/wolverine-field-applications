﻿//--------------------------------------|---------------------------------------
//                                PC RMS V1.0.0.0
//                                File: StructureVariables.cs
//------------------------------------------------------------------------------
// Author: Thuan Vu
// Created on April 14, 2021
//--------------------------------------|---------------------------------------
//  This drawing/document is copyright and the property of
//  Wolverine OilField Technologies LLC. and Remote Measurement Systems Ltd.
//  It must not be copied (in whole or in part),
//  used for manufacture or otherwise disclosed without prior written consent.
//  Any  copies of this drawing/document made by any method must also
//  include a copy of this legend. This document is supplied  without
//  liability for errors or omissions.
//
// (c) COPYRIGHT WOLVERINE OILFIELD TECHNOLOGIES LLC. AND
//               REMOTE MEASUREMENT SYSTEMS LIMITED 2021.
//-----------------------------------------------------------------------------

using PC_PDD;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace NodeCommon     // Structures translated to C# from C++ headers
{
    public class StructureVariables
    {
        #region Tool Interface Box
        public TIBFirmwareID ticFwId { get; set; }

        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct TIBFirmwareID
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public sbyte[] STRING;
        }

        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct TIC_DATAGROUP1_STRUCTURE
        {
            public ushort NodeStatus;
            public ushort Mode;
            public UInt32 TimeStampSeconds;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            public byte[] TimeStampDate;

            public float StartupVoltageVolts;
            public float StartupCurrentMilliAmps;
            public float NormalVoltageVolts;
            public float NormalCurrentMilliAmps;
            public float ErrorStartupVoltageVolts;
            public float ErrorStartupCurrentMilliAmps;
            public float ErrorNormalVoltageVolts;
            public float ErrorNormalCurrentMilliAmps;
            public float FlowVolts;
            public float PulseVolts;

            public ushort StartupVoltageAdcCounts;
            public ushort StartupCurrentAdcCounts;
            public ushort NormalVoltageAdcCounts;
            public ushort NormalCurrentAdcCounts;
            public ushort VoltageAdcCounts;
            public ushort CurrentAdcCounts;
            public ushort TicSampleStatus;
            public ushort NumberOfSamples;
            public ushort GammaCountsPerSecond;
            public ushort ControlStatus;
            public ushort SavedPowerSwitchStatus;
            public ushort SavedPulseSwitchStatus;
            public ushort SwitchPowerStatus;
            public ushort SwitchPulseStatus;
            public ushort ErrorStatus;
            public ushort PulseRateOnTime;
            public ushort PulseRateOffTime;
            public ushort Uart1ComErrorCounts;
            public ushort Uart2ComErrorCounts;

            float TestVariable1;
            float TestVariable2;
            float TestVariable3;
            float TestVariable4;
        }
        #endregion

        #region PDD V1.02
        public SYSTEM_RECORD_STRUCTURE SystemRecordStructure { get; set; }
        public SERIAL_NUMBER_STRUCTURE SerialNumber { get; set; }

        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct PDD_SERVICE_RECORD_STRUCTURE
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
            public byte[] BoardSerialNumber;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
            public byte[] BoardRevision;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            public byte[] ManufactureTimeStamp;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            public byte[] BatteryInstalledTimeStamp;
  
            public UInt16 CheckSum;// Checksum of configuration
        }

        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct SERIAL_NUMBER_STRUCTURE
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
            public byte[] StringPCB;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
            public byte[] StringModule;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
            public byte[] StringTopLevel;

            public ushort CheckSum;
        }

        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct FIRMWARE_ID_STRUCTURE
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public sbyte[] STRING;
        }

        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct SYSTEM_RECORD_STRUCTURE
        {
            public UInt16 TotalOpHours;
            public Int16 MaxTemperatureCelsius;
            public UInt32 MemoryPageCount;
            public UInt16 MemoryPageSize;
            public UInt16 MemoryParagraphSize;
            public UInt32 MemoryPageWritePointer;
            public UInt16 CheckSum;
        }

        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct  PDD_CONFIGURATION_STRUCTURE
        {
            //RG DIFFERENT
            //float LoggingPeriodSeconds;
            //signed int32 PressureSamples;
            //unsigned int16 StaticDelaySeconds;

            //RG INSERT RMS CODE 
            public UInt16 ADCSampleTime;             // ADC sampling duration for pressure readings
            public UInt16 ADCSampleInterval;         // Gap between ADC sampling
            public UInt16 FlowOffTimeout;            // Max time Flow can be Off before switching on again
            public UInt16 FlowSampleInterval;        // Gap between Flow On/Off readings
            public float QBusAddress;           // PDD Tool node address on QBus
            public float VBatSF;                              // Battery Voltage scale factor
            public float IBatSF;                              // Battery Current scale factor
            public float BoardTemperatureScaleFactor;               // Board Temperature scale factor
            public float BoardTemperatureOffset;                    // Board Temperature offset

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public Byte[] GenericVariables;          // GV 0 - 7 values
            public UInt16 CheckSum;                    // Checksum of configuration
        }

        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct PDD_CALIBRATION_STRUCTURE
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5)]
            public float[] Correction0_PaCalDataK;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5)]
            public float[] Correction1_PaCalDataK;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5)]
            public float[] Correction2_PaCalDataK;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5)]
            public float[] Correction3_PaCalDataK;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5)]
            public float[] Correction4_PaCalDataK;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5)]
            public float[] Correction0_PbCalDataK;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5)]
            public float[] Correction1_PbCalDataK;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5)]
            public float[] Correction2_PbCalDataK;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5)]
            public float[] Correction3_PbCalDataK;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5)]
            public float[] Correction4_PbCalDataK;

            public UInt16 CheckSum; // Check sum of configuration
        }


        //------------------------------------------------------------------------------
        // Data structures

        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct PDD_DATAGROUP_1_STRUCTURE
        {
            public UInt16 NodeStatus;
            public UInt16 Mode;
            public UInt32 TimeStampSeconds;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            public byte[] TimeStampDate;
            public float CirculatingHours;

            public Int32 BoardTemperatureAdcCounts;
            public Int32 VBusVoltsAdcCounts;
            public Int32 VBusCurrentAdcCounts;

            //MCP3551 ADC Resistance to Temperature Sensor
            public Int32 RtTemperatureAdcCounts;
            public float Resistance;

            //MCP3551 ADC Resistance to Annular Pressure Sensor
            public Int32 PressureAnnularAdcCounts;
            public float PressureAnnularVolts;

            //MCP3551 ADC Resistance to Bore Pressure Sensor 
            public Int32 PressureBoreAdcCounts;
            public float PressureBoreVolts;

            //FLow
            public UInt16 NumFlowOffSampleIntervals;

            public float TestVariable1;
            public float TestVariable2;
            public float TestVariable3;
            public float TestVariable4;
        }

        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct PDD_DATAGROUP_2_STRUCTURE
        {
            // This datagroup is used for calibration procedures.
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5)]
            public float[] AnnularPressureTempCorrData;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5)]
            public float[] BorePressureTempCorrData;

            public float PressureAnnularStaticPsi0;
            public float PressureAnnularPsi0;
            public float PressureBorePsi0;
            public float AveragePressureAnnularPsi;
            public float AveragePressureBorePsi;
            public float PressureAnnularStaticVolts;
            public float AveragePressureAnnularVolts;
            public float AveragePressureBoreVolts;
            public float RtTemperatureCelsius;
            public float BoardTemperatureCelsius;
            public float VBusVolts;
            public float IBusCurrent;
        }

        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct PDD_DATAGROUP_3_STRUCTURE
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            public byte[] TimeStampDate;
            public UInt16 NodeStatus;

            public float PressureAnnularStaticPsi0;
            public float AveragePressureAnnularPsi;
            public float AveragePressureBorePsi;
        }
        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct PDD_DATAGROUP_4_STRUCTURE
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            public byte[] TimeStampDate;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            public byte[] RTC_TimeStampDate;
    
        }
        //------------------------------------------------------------------------------
        // Memory record structures

        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct PDD_MEMORY_RECORD_TIMESTAMP_STRUCTURE
        {
            // Header
            public UInt16 SyncHeader;
            public UInt16 RecordId;
            public UInt16 RecordLength;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            public byte[] TimeStamp;
            // Data
            // CheckSum
            public UInt16 CheckSum;
        }

        // Structure of data to be logged
        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct PDD_MEMORY_RECORD_PWR_DATA_LOGGING_STRUCTURE
        {
            // Header
            public UInt16 SyncHeader;
            public UInt16 RecordId;
            public UInt16 RecordLength;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            public byte[] TimeStamp;
            // Data
            public UInt16 boardTemperature;
	        public UInt32 vBat;
            public UInt16 Event;

            // CheckSum
            public UInt16 CheckSum;
            }

        // Alternative logging structure for PDD
        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct PDD_MEMORY_RECORD_DATA_PDD_LOGGING_STRUCTURE
            {
            // Header
            public UInt16 SyncHeader;
            public UInt16 RecordId;
            public UInt16 RecordLength;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            public byte[] TimeStamp;
            // Data
            public UInt16 NodeStatus;
            public UInt32 RecordNumber;
	        public float PressureAnnularStaticPsi0;
            public float AveragePressureAnnularPsi;
            public float AveragePressureBorePsi;
            public float PressureAnnularStaticVolts;
            public float AveragePressureAnnularVolts;
            public float AveragePressureBoreVolts;
            public float RtTemperatureCelsius;
            public float BoardTemperatureCelsius;
            public float VBusVolts;
            public float VBusCurrent;
            // CheckSum
            public UInt16 CheckSum;
        }
        #endregion

        #region MFPWR V2.02
        // Nonvolatile structures

        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct MFPWR_SERVICE_RECORD_STRUCTURE
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
            public byte[] BoardSerialNumber;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
            public byte[] BoardRevision;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            public byte[] ManufactureTimeStamp;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            public byte[] BatteryInstalledTimeStamp;

            public UInt16 CheckSum;                    // Checksum of configuration
        };

        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct MFPWR_CONFIGURATION_STRUCTURE
        {
            public float QBusAddress;

            public UInt16 TransmitterWarmUpTime;		    // Twut, milliSeconds, resolution 25mS
	        public UInt16 TransmitterSwitchDelay;		    // Tswt, milliSeconds, resolution 25mS
	        public UInt16 ReceiverMeasurementTime;		    // Rmt,  seconds, resolution 1 second
	        public UInt16 AutomaticAcquisitionUpdateTime;	// Aut,  seconds, resolution 1 second
	        public float BoardTemperatureOffset;            // Toff
	        public float BoardTemperatureScaleFactor;       // Tsf
            public float GVPhaseOffset;                     // GV Phase Offset
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public byte[] GenericVariables;                 // GV 0 - 7 values
            public UInt16 FlowControlMemoryStatus;
            public float GVArMultiplier;                   // GV AR Multiplier
            public UInt16 CheckSum;                        // Checksum of configuration
        };

        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct MFPWR_CALIBRATION_STRUCTURE
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5)]
            public float[] ZF2MAScaleK;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5)]
            public float[] ZF2MPScaleK;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5)]
            public float[] ZN2MAScaleK;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5)]
            public float[] ZN2MPScaleK;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5)]
            public float[] ZF4KAScaleK;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5)]
            public float[] ZF4KPScaleK;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5)]
            public float[] ZN4KAScaleK;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5)]
            public float[] ZN4KPScaleK;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 9)]
            public float[] AirHangOffset;
            public UInt16 CheckSum; // Check sum of Calibration
        };

//------------------------------------------------------------------------------
// Data structures

        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct MFPWR_DATAGROUP_1_STRUCTURE
        {
            public UInt16 NodeStatus;
            public UInt16 Mode;
            public UInt32 TimeStampSeconds;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            public byte[] TimeStampDate;
            public float CirculatingHours;

            public Int32 BoardTemperatureAdcCounts;
            public Int32 VBusVoltsAdcCounts;
            public UInt16 BoardTemperatureCelsius;
            public float TemperatureCelsius;
            public float VBusVolts;
            public UInt16 StateMachineState;

            public UInt16 DapSampleMode;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public byte[] DapResponseBuffer;    
            public UInt16 TxUpperDapUpperNumberOfSamples;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public UInt32[] TxUpperDapUpperAdcSamples;
            public UInt16 TxUpperDapLowerNumberOfSamples;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public UInt32[] TxUpperDapLowerAdcSamples;
    
    
            public UInt16 TxLowerDapUpperNumberOfSamples;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public UInt32[] TxLowerDapUpperAdcSamples;
            public UInt16 TxLowerDapLowerNumberOfSamples;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public UInt32[] TxLowerDapLowerAdcSamples;

            public float TestVariable1;
            public float TestVariable2;
            public float TestVariable3;
            public float TestVariable4;
        };

        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct MFPWR_DATAGROUP_2_STRUCTURE
        {
            // Raw values, RuPu, RuAu, RlPu, RlAu
            // Raw phase and amplitude Upper RX/Upper TX
            //Mode 0 - TX Near Upper 400KHz                 
            public float RxUpperTxUpperPhase;    
            public float RxUpperTxUpperAmplitude;
            public float RxLowerTxUpperPhase;    
            public float RxLowerTxUpperAmplitude;
            public float RxUpperTxLowerPhase;    
            public float RxUpperTxLowerAmplitude;
            public float RxLowerTxLowerPhase;    
            public float RxLowerTxLowerAmplitude;  

        }

        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct MFPWR_DATAGROUP_3_STRUCTURE
        {
            // Raw values, RuPu, RuAu, RlPu, RlAu
            // Raw phase and amplitude Upper RX/Upper TX
            //Mode 1 - TX Near Upper 2MHz                 
            public float RxUpperTxUpperPhase;    
            public float RxUpperTxUpperAmplitude;
            public float RxLowerTxUpperPhase;    
            public float RxLowerTxUpperAmplitude;
            public float RxUpperTxLowerPhase;    
            public float RxUpperTxLowerAmplitude;
            public float RxLowerTxLowerPhase;    
            public float RxLowerTxLowerAmplitude; 

        };

        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct MFPWR_DATAGROUP_4_STRUCTURE
        {
            // Raw values, RuPu, RuAu, RlPu, RlAu
            // Raw phase and amplitude Upper RX/Upper TX
            //Mode 2 - TX Far Upper 400KHz                 
            public float RxUpperTxUpperPhase;    
            public float RxUpperTxUpperAmplitude;
            public float RxLowerTxUpperPhase;    
            public float RxLowerTxUpperAmplitude;
            public float RxUpperTxLowerPhase;    
            public float RxUpperTxLowerAmplitude;
            public float RxLowerTxLowerPhase;    
            public float RxLowerTxLowerAmplitude; 

        };

        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct MFPWR_DATAGROUP_5_STRUCTURE
        {
            // Raw values, RuPu, RuAu, RlPu, RlAu
            // Raw phase and amplitude Upper RX/Upper TX
            //Mode 3 - TX Near Upper 2MHz                 
            public float RxUpperTxUpperPhase;   
            public float RxUpperTxUpperAmplitude;
            public float RxLowerTxUpperPhase;    
            public float RxLowerTxUpperAmplitude;
            public float RxUpperTxLowerPhase;   
            public float RxUpperTxLowerAmplitude;
            public float RxLowerTxLowerPhase;    
            public float RxLowerTxLowerAmplitude; 

        };

        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct MFPWR_DATAGROUP_6_STRUCTURE
        {
            // Intermediate values, IUAR, IUpH, ILAR, ILpH
            //Phase Shift from Upper and Lower
            //Amplitude Ratio from Upper and Lower
            //Mode 0 - TX Near Upper 400KHz
            public float AmplitudeTxUpper;
            public float PhaseShiftTxUpper;
            public float AmplitudeTxLower;
            public float PhaseShiftTxLower;

        };

        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct MFPWR_DATAGROUP_7_STRUCTURE
        {
            // Intermediate values, IUAR, IUpH, ILAR, ILpH
            //Phase Shift from Upper and Lower
            //Amplitude Ratio from Upper and Lower
            //Mode 1 - TX Near Upper 2MHz
            public float AmplitudeTxUpper;
            public float PhaseShiftTxUpper;
            public float AmplitudeTxLower;
            public float PhaseShiftTxLower;
        };

        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct MFPWR_DATAGROUP_8_STRUCTURE
        {
            // Intermediate values, IUAR, IUpH, ILAR, ILpH
            //Phase Shift from Upper and Lower
            //Amplitude Ratio from Upper and Lower
            //Mode 2 - TX Far Upper 400KHz
            public float AmplitudeTxUpper;
            public float PhaseShiftTxUpper;
            public float AmplitudeTxLower;
            public float PhaseShiftTxLower;
        };

        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct MFPWR_DATAGROUP_9_STRUCTURE
        {
            // Intermediate values, IUAR, IUpH, ILAR, ILpH
            //Phase Shift from Upper and Lower
            //Amplitude Ratio from Upper and Lower
            //Mode 3 - TX Far Upper 2MHz
            public float AmplitudeTxUpper;
            public float PhaseShiftTxUpper;
            public float AmplitudeTxLower;
            public float PhaseShiftTxLower;
        };

        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct MFPWR_DATAGROUP_10_STRUCTURE
        {
            // Converted res values, ReAr, RePh
            //Mode 0 - TX Near Upper 400KHz
            public float CompensatedPhase;
            public float CompensatedAmplitude;
        };

        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct MFPWR_DATAGROUP_11_STRUCTURE
        {
            // Converted res values, ReAr, RePh
            //Mode 1 - TX Near Upper 2MHz
            public float CompensatedPhase;
            public float CompensatedAmplitude;
        };

        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct MFPWR_DATAGROUP_12_STRUCTURE
        {
            // Converted res values, ReAr, RePh
            //Mode 2 - TX Far Upper 400KHz
            public float CompensatedPhase;
            public float CompensatedAmplitude;
        };

        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct MFPWR_DATAGROUP_13_STRUCTURE
        {
            // Converted res values, ReAr, RePh
            //Mode 3 - TX Far Upper 2MHz
            public float CompensatedPhase;
            public float CompensatedAmplitude;
        };

        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct MFPWR_DATAGROUP_14_STRUCTURE
        {
            public UInt32 ErrorCodeStatus;
            public UInt16 NodeStatus;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            public byte[] TimeStamp;

            // Converted res values, ReAr, RePh
            //Mode 0 - TX Near 400KHz
            public float CompensatedPhaseN4K;
            public float CompensatedAmplitudeN4K;
            // Converted res values, ReAr, RePh
            //Mode 1 - TX Near 2MHz
            public float CompensatedPhaseN2M;
            public float CompensatedAmplitudeN2M;
            // Converted res values, ReAr, RePh
            //Mode 2 - TX Far 400KHz
            public float CompensatedPhaseF4K;
            public float CompensatedAmplitudeF4K;
            // Converted res values, ReAr, RePh
            //Mode 3 - TX Far 2MHz
            public float CompensatedPhaseF2M;
            public float CompensatedAmplitudeF2M;
        }

        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct MFPWR_DATAGROUP_15_STRUCTURE
        {
            public UInt16 NodeStatus;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            public byte[] TimeStampDate;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            public byte[] RTC_TimeStampDate;
        }

        //------------------------------------------------------------------------------
        // Memory record structures

        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct MFPWR_MEMORY_RECORD_TIMESTAMP_STRUCTURE
        {
            // Header
            public UInt16 SyncHeader;
            public UInt16 RecordId;
            public UInt16 RecordLength;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            public byte[] TimeStamp;
            // Data

            // CheckSum
           public UInt16 CheckSum;
        };

        // Structure of data to be logged
        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct MFPWR_MEMORY_RECORD_PWR_DATA_LOGGING_STRUCTURE
        {
            // Header
            public UInt16 SyncHeader;
            public UInt16 RecordId;
            public UInt16 RecordLength;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            public byte[] TimeStamp;
            // Data
	        public UInt32 logAddress;


            public UInt16 boardTemperature;
            public UInt16 vBat;
            public UInt16 Event;

            // CheckSum
            public UInt16 CheckSum;
        };

        // Alternative logging structure for Mfpwr
        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct MFPWR_MEMORY_RECORD_DATA_MFPWR_LOGGING_STRUCTURE
        {
	        // Header
	        public UInt16 SyncHeader;
            public UInt16 RecordId;
            public UInt16 RecordLength;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            public byte[] TimeStamp;

            // Data
            public UInt16 NodeStatus;
            public UInt32 RecordNumber;

            //Near 400KHz
            // Raw phase and amplitude Upper RX/Upper TX
            public float Near400KHzRuPu;
            public float Near400KHzRuAu;
            public float Near400KHzRlPu;
            public float Near400KHzRlAu;
            // Raw phase and amplitude Upper RX/Lower TX
            public float Near400KHzRuPl;
            public float Near400KHzRuAl;
            public float Near400KHzRlPl;
            public float Near400KHzRlAl;

            //Near 2MHz
             // Raw phase and amplitude Upper RX/Upper TX
            public float Near2MHzRuPu;
            public float Near2MHzRuAu;
            public float Near2MHzRlPu;
            public float Near2MHzRlAu;
            // Raw phase and amplitude Upper RX/Lower TX
            public float Near2MHzRuPl;
            public float Near2MHzRuAl;
            public float Near2MHzRlPl;
            public float Near2MHzRlAl;

            //Far 400KHz
            // Raw phase and amplitude Upper RX/Upper TX
            public float Far400KHzRuPu;
            public float Far400KHzRuAu;
            public float Far400KHzRlPu;
            public float Far400KHzRlAu;
            // Raw phase and amplitude Upper RX/Lower TX
            public float Far400KHzRuPl;
            public float Far400KHzRuAl;
            public float Far400KHzRlPl;
            public float Far400KHzRlAl;

            //Far 2MHz
            // Raw phase and amplitude Upper RX/Upper TX
            public float Far2MHzRuPu;
            public float Far2MHzRuAu;
            public float Far2MHzRlPu;
            public float Far2MHzRlAu;
            // Raw phase and amplitude Upper RX/Lower TX
            public float Far2MHzRuPl;
            public float Far2MHzRuAl;
            public float Far2MHzRlPl;
            public float Far2MHzRlAl;

            //Phase Shift from Upper and Lower
            //Amplitude Ratio from Upper and Lower
            //Near 400KHz
            public float Near400KHzIuPH;
            public float Near400KHzIuAR;
            public float Near400KHzIlPH;
            public float Near400KHzIlAR;
            //Near 2MHz
            public float Near2MHzIuPH;
            public float Near2MHzIuAR;
            public float Near2MHzIlPH;
            public float Near2MHzIlAR;

            //Far 400KHz
            public float Far400KHzIuPH;
            public float Far400KHzIuAR;
            public float Far400KHzIlPH;
            public float Far400KHzIlAR;
            //Far 2MHz
            public float Far2MHzIuPH;
            public float Far2MHzIuAR;
            public float Far2MHzIlPH;
            public float Far2MHzIlAR;

            //Compensated Phase Shift
            public float Near400KHzRePH;
            public float Near2MHzRePH;
            public float Far400KHzRePH;
            public float Far2MHzRePH;
            //Compensated Amplitude Ratio
            public float Near400KHzReAR;
            public float Near2MHzReAR;
            public float Far400KHzReAR;
            public float Far2MHzReAR;

            public float TemperatureCelsius;
            public float VBusVolts;
            public UInt16 Event;
            // CheckSum
            public UInt16 CheckSum;
        };

        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct MFPWR_MEMORY_RECORD_DIAGNOSTICS_LOGGING_STRUCTURE
        {
            // Header
            public UInt16 SyncHeader;
            public UInt16 RecordId;
            public UInt16 RecordLength;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            public byte[] TimeStamp;

            // Data
            public Int32 BoardTemperatureAdcCounts;
            public float TemperatureCelsius;
            public float VBusVolts;
            public UInt16 NodeStatus;

            public UInt16 DapUpperNodeStatus;
            public UInt16 DapUpperStateMachineState;
            public UInt16 DapUpperInitialAdcEocTimeClockCycles;
            public UInt16 DapUpperAdcEocTimeClockCycles;
            public UInt16 DapUpperNumberOfSamples;

            public UInt16 DapLowerNodeStatus;
            public UInt16 DapLowerStateMachineState;
            public UInt16 DapLowerInitialAdcEocTimeClockCycles;
            public UInt16 DapLowerAdcEocTimeClockCycles;
            public UInt16 DapLowerNumberOfSamples;

            public UInt32 DapAcquisitionMilliSeconds;
            public UInt16 StateMachineState;
            public UInt16 DapSampleMode;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public byte[] AcqTXUpperDapUpperResponseBuffer;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public byte[] AcqTXUpperDapLowerResponseBuffer;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public byte[] AcqTXLowerDapUpperResponseBuffer;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public byte[] AcqTXLowerDapLowerResponseBuffer;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public byte[] GetTXUpperDapUpperResponseBuffer;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public byte[] GetTXUpperDapLowerResponseBuffer;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public byte[] GetTXLowerDapUpperResponseBuffer;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public byte[] GetTXLowerDapLowerResponseBuffer;
    
            public UInt16 TxUpperDapUpperNumberOfSamples;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public UInt32[] TxUpperDapUpperAdcSamples;
            public UInt16 TxUpperDapLowerNumberOfSamples;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public UInt32[] TxUpperDapLowerAdcSamples;

            public UInt16 TxLowerDapUpperNumberOfSamples;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public UInt32[] TxLowerDapUpperAdcSamples;
            public UInt16 TxLowerDapLowerNumberOfSamples;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public UInt32[] TxLowerDapLowerAdcSamples;

            // CheckSum
            public UInt16 CheckSum;
        }

        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct MFPWR_MEMORY_RECORD_RING_DOWN_LOGGING_STRUCTURE
        {
            // Header
            public UInt16 SyncHeader;
            public UInt16 RecordId;
            public UInt16 RecordLength;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            public byte[] TimeStamp;
            // Data
            public UInt16 NodeStatus;
            public UInt32 DapAcquisitionMilliSeconds;
            public UInt16 BoardTemperatureCelsius;
            public float VBusVolts;
            public UInt16 DapSampleMode;
            public UInt16 RingDownSampleSets;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 50)]
            public UInt32[] UpperDapAdcBuffer;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 50)]
            public UInt32[] LowerDapAdcBuffer;
            // CheckSum
            public UInt16 CheckSum;
        }

        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct MFPWR_MEMORY_RECORD_DIAGNOSTICS_MODE0_LOGGING_STRUCTURE
        {
            // Header
            public UInt16 SyncHeader;
            public UInt16 RecordId;
            public UInt16 RecordLength;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            public byte[] TimeStamp;

            // Data
            public Int32 BoardTemperatureAdcCounts;
            public float TemperatureCelsius;
            public float VBusVolts;
            public UInt16 NodeStatus;

            public UInt16 DapUpperNodeStatus;
            public UInt16 DapUpperStateMachineState;
            public UInt16 DapUpperInitialAdcEocTimeClockCycles;
            public UInt16 DapUpperAdcEocTimeClockCycles;
            public UInt16 DapUpperNumberOfSamples;

            public UInt16 DapLowerNodeStatus;
            public UInt16 DapLowerStateMachineState;
            public UInt16 DapLowerInitialAdcEocTimeClockCycles;
            public UInt16 DapLowerAdcEocTimeClockCycles;
            public UInt16 DapLowerNumberOfSamples;

            public UInt32 DapAcquisitionMilliSeconds;
            public UInt16 StateMachineState;
            public UInt16 DapSampleMode;

            public UInt16 TxUpperDapUpperNumberOfSamples;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public UInt32[] TxUpperDapUpperAdcSamples;
            public UInt16 TxUpperDapLowerNumberOfSamples;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public UInt32[] TxUpperDapLowerAdcSamples;

            public UInt16 TxLowerDapUpperNumberOfSamples;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public UInt32[] TxLowerDapUpperAdcSamples;
            public UInt16 TxLowerDapLowerNumberOfSamples;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public UInt32[] TxLowerDapLowerAdcSamples;

            // CheckSum
            public UInt16 CheckSum;
        }

        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct MFPWR_MEMORY_RECORD_DIAGNOSTICS_MODE1_LOGGING_STRUCTURE
        {
            // Header
            public UInt16 SyncHeader;
            public UInt16 RecordId;
            public UInt16 RecordLength;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            public byte[] TimeStamp;

            // Data
            public Int32 BoardTemperatureAdcCounts;
            public float TemperatureCelsius;
            public float VBusVolts;
            public UInt16 NodeStatus;

            public UInt16 DapUpperNodeStatus;
            public UInt16 DapUpperStateMachineState;
            public UInt16 DapUpperInitialAdcEocTimeClockCycles;
            public UInt16 DapUpperAdcEocTimeClockCycles;
            public UInt16 DapUpperNumberOfSamples;

            public UInt16 DapLowerNodeStatus;
            public UInt16 DapLowerStateMachineState;
            public UInt16 DapLowerInitialAdcEocTimeClockCycles;
            public UInt16 DapLowerAdcEocTimeClockCycles;
            public UInt16 DapLowerNumberOfSamples;

            public UInt32 DapAcquisitionMilliSeconds;
            public UInt16 StateMachineState;
            public UInt16 DapSampleMode;

            public UInt16 TxUpperDapUpperNumberOfSamples;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public UInt32[] TxUpperDapUpperAdcSamples;
            public UInt16 TxUpperDapLowerNumberOfSamples;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public UInt32[] TxUpperDapLowerAdcSamples;

            public UInt16 TxLowerDapUpperNumberOfSamples;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public UInt32[] TxLowerDapUpperAdcSamples;
            public UInt16 TxLowerDapLowerNumberOfSamples;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public UInt32[] TxLowerDapLowerAdcSamples;

            // CheckSum
            public UInt16 CheckSum;
        }

        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct MFPWR_MEMORY_RECORD_DIAGNOSTICS_MODE2_LOGGING_STRUCTURE
        {
            // Header
            public UInt16 SyncHeader;
            public UInt16 RecordId;
            public UInt16 RecordLength;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            public byte[] TimeStamp;

            // Data
            public Int32 BoardTemperatureAdcCounts;
            public float TemperatureCelsius;
            public float VBusVolts;
            public UInt16 NodeStatus;

            public UInt16 DapUpperNodeStatus;
            public UInt16 DapUpperStateMachineState;
            public UInt16 DapUpperInitialAdcEocTimeClockCycles;
            public UInt16 DapUpperAdcEocTimeClockCycles;
            public UInt16 DapUpperNumberOfSamples;

            public UInt16 DapLowerNodeStatus;
            public UInt16 DapLowerStateMachineState;
            public UInt16 DapLowerInitialAdcEocTimeClockCycles;
            public UInt16 DapLowerAdcEocTimeClockCycles;
            public UInt16 DapLowerNumberOfSamples;

            public UInt32 DapAcquisitionMilliSeconds;
            public UInt16 StateMachineState;
            public UInt16 DapSampleMode;

            public UInt16 TxUpperDapUpperNumberOfSamples;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public UInt32[] TxUpperDapUpperAdcSamples;
            public UInt16 TxUpperDapLowerNumberOfSamples;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public UInt32[] TxUpperDapLowerAdcSamples;

            public UInt16 TxLowerDapUpperNumberOfSamples;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public UInt32[] TxLowerDapUpperAdcSamples;
            public UInt16 TxLowerDapLowerNumberOfSamples;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public UInt32[] TxLowerDapLowerAdcSamples;

            // CheckSum
            public UInt16 CheckSum;
        }

        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct MFPWR_MEMORY_RECORD_DIAGNOSTICS_MODE3_LOGGING_STRUCTURE
        {
            // Header
            public UInt16 SyncHeader;
            public UInt16 RecordId;
            public UInt16 RecordLength;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            public byte[] TimeStamp;

            // Data
            public Int32 BoardTemperatureAdcCounts;
            public float TemperatureCelsius;
            public float VBusVolts;
            public UInt16 NodeStatus;

            public UInt16 DapUpperNodeStatus;
            public UInt16 DapUpperStateMachineState;
            public UInt16 DapUpperInitialAdcEocTimeClockCycles;
            public UInt16 DapUpperAdcEocTimeClockCycles;
            public UInt16 DapUpperNumberOfSamples;

            public UInt16 DapLowerNodeStatus;
            public UInt16 DapLowerStateMachineState;
            public UInt16 DapLowerInitialAdcEocTimeClockCycles;
            public UInt16 DapLowerAdcEocTimeClockCycles;
            public UInt16 DapLowerNumberOfSamples;

            public UInt32 DapAcquisitionMilliSeconds;
            public UInt16 StateMachineState;
            public UInt16 DapSampleMode;

            public UInt16 TxUpperDapUpperNumberOfSamples;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public UInt32[] TxUpperDapUpperAdcSamples;
            public UInt16 TxUpperDapLowerNumberOfSamples;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public UInt32[] TxUpperDapLowerAdcSamples;

            public UInt16 TxLowerDapUpperNumberOfSamples;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public UInt32[] TxLowerDapUpperAdcSamples;
            public UInt16 TxLowerDapLowerNumberOfSamples;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public UInt32[] TxLowerDapLowerAdcSamples;

            // CheckSum
            public UInt16 CheckSum;
        }
        #endregion

        #region MWD Master V10.55
        public MWD_MASTER_DATAGROUP_1_STRUCTURE MasterDataGroup1 { get; set; }
        public MWD_MASTER_CONFIGURATION_STRUCTURE MasterConfiguration { get; set; }

        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct MWD_MASTER_TFO_STRUCTURE
        {
            public float ToolfaceOffsetDegrees;
            public UInt16 CheckSum;
        }

        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct MWD_MASTER_DATAGROUP_1_STRUCTURE
        {
            public UInt16 NodeStatus;
            public UInt16 Mode;
            public UInt32 TimeStampSeconds;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            public byte[] TimeStampDate;
            public UInt32 MilliSecondCounter;
            public UInt16 StateMachineState;
            public UInt16 StateTimerMilliSeconds;
            public UInt16 FlagBat2On;
            public float ToolfaceOffsetDegrees;
            public UInt16 DownlinkState;
            public UInt16 DownlinkCommand;
            public UInt16 MptMode;
            public UInt16 PulseWidthIndex;
            public float PulseWidthSeconds;
            public float PulseWidthRatio;
            public float Tnp_mult;
            public float Td_mult;
            public float PulserDelayOnSeconds;
            public float PulserDelayOffSeconds;
            public float PulseWidthAdjustedSeconds;
            // Analog
            public float VBusVolts;
            public float TemperatureCelsius;
            public float VibxMilliVolts;
            public float VibyMilliVolts;
            public float VibzMilliVolts;
            // Circulation
            public UInt16 FlowState;
            public float CirculatingHours;
            public UInt32 TripDelayTimer;
            public UInt16 SurveyDelayTimerSeconds;
            public UInt16 MptTransmitDelayTimerSeconds;
            // MWD Data
            public UInt16 DirectionalSensorType;
            public float InclinationDegrees;
            public float CircInclinationDegrees;
            public float AzimuthDegrees;
            public float MagneticDipDegrees;
            public float TotalMagneticFieldGauss;
            public float TotalGravityFieldG;
            public float MagneticToolFaceDegrees;
            public float GravityToolFaceDegrees;
            public float GammaCountsPerSecond;
            public float GxG;
            public float GyG;
            public float GzG;
            public float BxGauss;
            public float ByGauss;
            public float BzGauss;
            // Test variables.
            public float TestVariable1;
            public float TestVariable2;
            public float TestVariable3;
            public float TestVariable4;

        }

        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct MWD_MASTER_CONFIGURATION_STRUCTURE
        {
            public UInt16 ToolSize;
            public UInt16 TripDelayMinutes;
            public UInt16 LoggingPeriodSeconds;
            public UInt16 SurveyDelaySeconds;
            public UInt16 TransmitDelaySeconds;
            public UInt16 DownlinkWindowSeconds;
            public UInt16 DownlinkDelayMinutes;
            public UInt16 DownlinkConfirmMinSeconds;
            public UInt16 DownlinkConfirmMaxSeconds;
            public UInt16 SyncFrameType;
            public UInt16 NumberOfSyncPulses;
            public UInt16 SurveyHeaderSize;
            public UInt16 ToolFaceHeaderSize;
            public UInt16 HeaderParityType;
            public UInt16 PulseWidthIndex;
            public float PulseWidthSeconds0;
            public float PulseWidthSeconds1;
            public float PulseWidthSeconds2;
            public float PulseWidthSeconds3;
            public float Tnp_mult0;
            public float Td_mult0;
            public float Tnp_mult1;
            public float Td_mult1;
            public float Tnp_mult2;
            public float Td_mult2;
            public float Tnp_mult3;
            public float Td_mult3;
            public float PulseWidthRatio0;
            public float PulseWidthRatio1;
            public float PulseWidthRatio2;
            public float PulseWidthRatio3;
            public UInt16 Options;
            public UInt16 uiStaticSurveySource;
            public UInt16 uiDownlinkType;
            public float InclinationThresholdDegrees;
            public float BatteryLowThresholdVolts;
            public float fGV0Range;
            public float fGV0Offset;
            public float fGV1Range;
            public float fGV1Offset;
            public float fGV2Range;
            public float fGV2Offset;
            public float fGV3Range;
            public float fGV3Offset;
            public float fGV4Range;
            public float fGV4Offset;
            public float fGV5Range;
            public float fGV5Offset;
            public float fGV6Range;
            public float fGV6Offset;
            public float fGV7Range;
            public float fGV7Offset;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public char[] cGV0Name;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public char[] cGV1Name;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public char[] cGV2Name;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public char[] cGV3Name;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public char[] cGV4Name;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public char[] cGV5Name;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public char[] cGV6Name;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public char[] cGV7Name;
            public UInt16 CheckSum;
        };

        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct MWD_MASTER_DATAGROUP_4_STRUCTURE
        {
            public UInt16 NumBytes;
            public UInt16 SerialNumber;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
            public char[] KtableModel;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
            public char[] HardwareModel;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
            public char[] CalDate;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
            public char[] CalTime;
            public char KtType;
            public char KtEEpromAdd;
            public char KtMode;
            public char ADCtype;
            public char BiaxTriax;
            public char Checksum;
            // Coefficients
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 33)]
            public float[] Kg;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 33)]
            public float[] Kh;

        };

        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct MWD_MASTER_DATAGROUP_5_STRUCTURE
        {
            // Roll test datagroup
            public UInt16 SequenceNumber;
            public UInt16 Mode;
            public float VBusVolts;
            public float TemperatureCelsius;
            // Uncorrected vectors, nominal units
            public float GxG0;
            public float GyG0;
            public float GzG0;
            public float BxGauss0;
            public float ByGauss0;
            public float BzGauss0;
            // Corrected vectors
            public float GxG;
            public float GyG;
            public float GzG;
            public float BxGauss;
            public float ByGauss;
            public float BzGauss;
        };
        #endregion

        #region Digital Gamma Update DG1 to V4.00
        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct GAMMA_DATAGROUP_1_STRUCTURE
        {
            public UInt16 NodeStatus;
            public UInt16 Mode;
            public UInt32 TimeStampSeconds;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            public byte[] TimeStampDate;
            public float BatBusVolts;
            public float Bat1Volts;
            public float Bat2Volts;
            public float TemperatureCelsius;
            public UInt16 FlowState;
            public float CirculatingHours;
            public UInt16 GammaAverageWindowSeconds;
            public float GammaCountsPerSecond;
            public float TestVariable1;
            public float TestVariable2;
            public float TestVariable3;
            public float TestVariable4;

        }

        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct GAMMA_DATAGROUP_2_STRUCTURE
        {
            public UInt16 NodeStatus;
            public UInt16 Mode;
            public UInt32 TimeStampSeconds;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            public byte[] TimeStampDate;
            public float BatBusVolts;
            public float Bat1Volts;
            public float Bat2Volts;
            public float TemperatureCelsius;
            public UInt16 FlowState;
            public float CirculatingHours;
            public float GammaCountsPerSecond;

        }
        #endregion

        #region UVT V0.53
        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct UVT_DATAGROUP_2_STRUCTURE
        {
            public UInt16 NodeStatus;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            public byte[] TimeStampDate;
            public UInt16 GammaAverageWindowSeconds;
            public float GammaCountsPerSecond;

        }

        #endregion
    }
}