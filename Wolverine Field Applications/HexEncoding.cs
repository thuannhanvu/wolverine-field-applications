﻿//--------------------------------------|---------------------------------------
//                                PC RMS V1.0.0.0
//                                File: HexEncoding.cs
//------------------------------------------------------------------------------
// Author: Thuan Vu
// Created on April 14, 2021
//--------------------------------------|---------------------------------------
//  This drawing/document is copyright and the property of
//  Wolverine OilField Technologies LLC. and Remote Measurement Systems Ltd.
//  It must not be copied (in whole or in part),
//  used for manufacture or otherwise disclosed without prior written consent.
//  Any  copies of this drawing/document made by any method must also
//  include a copy of this legend. This document is supplied  without
//  liability for errors or omissions.
//
// (c) COPYRIGHT WOLVERINE OILFIELD TECHNOLOGIES LLC. AND
//               REMOTE MEASUREMENT SYSTEMS LIMITED 2021.
//-----------------------------------------------------------------------------
using System;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PC_PDD
{
    internal class HexEncoding
    {
        public static byte[] GetBytes(bool value, bool littleEndian)
        {
            return ReverseAsNeeded(BitConverter.GetBytes(value), littleEndian);
        }

        public static byte[] GetBytes(char value, bool littleEndian)
        {
            return ReverseAsNeeded(BitConverter.GetBytes(value), littleEndian);
        }
        public static byte[] GetBytes(double value, bool littleEndian)
        {
            return ReverseAsNeeded(BitConverter.GetBytes(value), littleEndian);
        }
        public static byte[] GetBytes(float value, bool littleEndian)
        {
            return ReverseAsNeeded(BitConverter.GetBytes(value), littleEndian);
        }
        public static byte[] GetBytes(int value, bool littleEndian)
        {
            return ReverseAsNeeded(BitConverter.GetBytes(value), littleEndian);
        }
        public static byte[] GetBytes(long value, bool littleEndian)
        {
            return ReverseAsNeeded(BitConverter.GetBytes(value), littleEndian);
        }
        public static byte[] GetBytes(short value, bool littleEndian)
        {
            return ReverseAsNeeded(BitConverter.GetBytes(value), littleEndian);
        }
        public static byte[] GetBytes(uint value, bool littleEndian)
        {
            return ReverseAsNeeded(BitConverter.GetBytes(value), littleEndian);
        }
        public static byte[] GetBytes(ulong value, bool littleEndian)
        {
            return ReverseAsNeeded(BitConverter.GetBytes(value), littleEndian);
        }
        public static byte[] GetBytes(ushort value, bool littleEndian)
        {
            return ReverseAsNeeded(BitConverter.GetBytes(value), littleEndian);
        }
        private static byte[] ReverseAsNeeded(byte[] bytes, bool wantsLittleEndian)
        {
            if (wantsLittleEndian == BitConverter.IsLittleEndian)
                return bytes;
            else
                return (byte[])bytes.Reverse().ToArray();
        }

        public static string ByteArrayToString(byte[] ba)
        {
            var hex = new StringBuilder(ba.Length * 2);
            foreach (var b in ba)
                hex.AppendFormat("{0:X2} ", b);
            return hex.ToString();
        }

        public static int GetByteCount(string hexString)
        {
            var numHexChars = 0;
            char c;

            // remove all none A-F, 0-9, characters
            for (var i = 0; i < hexString.Length; i++)
            {
                c = hexString[i];
                if (IsHexDigit(c))
                    numHexChars++;
            }

            // if odd number of characters, discard last character
            if (numHexChars % 2 != 0) numHexChars--;

            return numHexChars / 2; // 2 characters per byte
        }

        /// <summary>
        ///     Creates a byte array from the hexadecimal string. Each two characters are combined
        ///     to create one byte. First two hexadecimal characters become first byte in returned array.
        ///     Non-hexadecimal characters are ignored.
        /// </summary>
        /// <param name="hexString">string to convert to byte array</param>
        /// <param name="discarded">number of characters in string ignored</param>
        /// <returns>byte array, in the same left-to-right order as the hexString</returns>
        public static async Task<byte[]> GetBytes(string hexString)
        {
            var newString = "";
            char c;
            // remove all none A-F, 0-9, characters

            await Task.Run(() =>
            {
                for (var i = 0; i < hexString.Length; i++)
                {
                    c = hexString[i];
                    if (IsHexDigit(c))
                        newString += c;
                }
            });

            // if odd number of characters, discard last character
            if (newString.Length % 2 != 0) newString = newString.Substring(0, newString.Length - 1);

            var byteLength = newString.Length / 2;
            var bytes = new byte[byteLength];
            string hex;
            var j = 0;

            await Task.Run(() =>
            {
                for (var i = 0; i < bytes.Length; i++)
                {
                    hex = new string(new[] { newString[j], newString[j + 1] });
                    bytes[i] = HexToByte(hex);
                    j = j + 2;
                }
            });

            return bytes;
        }

        public static byte[] GetBytesSynchronous(string hexString)
        {
            var newString = "";
            char c;
            // remove all none A-F, 0-9, characters

            for (var i = 0; i < hexString.Length; i++)
            {
                c = hexString[i];
                if (IsHexDigit(c))
                    newString += c;
            }

            // if odd number of characters, discard last character
            if (newString.Length % 2 != 0) newString = newString.Substring(0, newString.Length - 1);

            var byteLength = newString.Length / 2;
            var bytes = new byte[byteLength];
            string hex;
            var j = 0;

            for (var i = 0; i < bytes.Length; i++)
            {
                hex = new string(new[] { newString[j], newString[j + 1] });
                bytes[i] = HexToByte(hex);
                j = j + 2;
            }

            return bytes;
        }

        public static async Task<string> ToString(byte[] bytes)
        {
            var hexString = "";

            await Task.Run(() =>
            {
                for (var i = 0; i < bytes.Length; i++) hexString += bytes[i].ToString("X2") + " ";
            });

            return hexString;
        }

        /// <summary>
        ///     Determines if given string is in proper hexadecimal string format
        /// </summary>
        /// <param name="hexString"></param>
        /// <returns></returns>
        public static bool InHexFormat(string hexString)
        {
            var hexFormat = true;

            foreach (var digit in hexString)
                if (!IsHexDigit(digit))
                {
                    hexFormat = false;
                    break;
                }

            return hexFormat;
        }

        /// <summary>
        ///     Returns true is c is a hexadecimal digit (A-F, a-f, 0-9)
        /// </summary>
        /// <param name="c">Character to test</param>
        /// <returns>true if hex digit, false if not</returns>
        public static bool IsHexDigit(char c)
        {
            int numChar;
            var numA = Convert.ToInt32('A');
            var num1 = Convert.ToInt32('0');
            c = char.ToUpper(c);
            numChar = Convert.ToInt32(c);
            if (numChar >= numA && numChar < numA + 6)
                return true;
            if (numChar >= num1 && numChar < num1 + 10)
                return true;
            return false;
        }

        /// <summary>
        ///     Converts 1 or 2 character string into equivalant byte value
        /// </summary>
        /// <param name="hex">1 or 2 character string</param>
        /// <returns>byte</returns>
        public static byte HexToByte(string hex)
        {
            if (hex.Length > 2 || hex.Length <= 0)
                throw new ArgumentException("hex must be 1 or 2 characters in length");
            var newByte = byte.Parse(hex, NumberStyles.HexNumber);
            return newByte;
        }

        public string ConvertToString(sbyte[] arr)
        {
            unsafe
            {
                string returnStr;
                fixed (sbyte* fixedPtr = arr)
                {
                    returnStr = new string(fixedPtr);
                }

                return returnStr;
            }
        }

        public string StringToBinary(string data)
        {
            StringBuilder sb = new StringBuilder();

            foreach (char c in data.ToCharArray())
            {
                sb.Append(Convert.ToString(c, 2).PadLeft(8, '0'));
            }
            return sb.ToString();
        }

        public static byte[] StringToByteArrayFastest(string hex)
        {
            if (hex.Length % 2 == 1)
                throw new Exception("The binary key cannot have an odd number of digits");

            byte[] arr = new byte[hex.Length >> 1];

            for (int i = 0; i < hex.Length >> 1; ++i)
            {
                arr[i] = (byte)((GetHexVal(hex[i << 1]) << 4) + (GetHexVal(hex[(i << 1) + 1])));
            }

            return arr;
        }

        public static int GetHexVal(char hex)
        {
            int val = (int)hex;
            //For uppercase A-F letters:
            return val - (val < 58 ? 48 : 55);
            //For lowercase a-f letters:
            //return val - (val < 58 ? 48 : 87);
            //Or the two combined, but a bit slower:
            //return val - (val < 58 ? 48 : (val < 97 ? 55 : 87));
        }

        public ushort Combine(byte b1, byte b2)
        {
            ushort combined = (ushort)(b1 << 8 | b2);
            return combined;
        }
    }
}