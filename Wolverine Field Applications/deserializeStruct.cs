﻿//--------------------------------------|---------------------------------------
//                                PC RMS V1.0.0.0
//                                File: deserializeStruct.cs
//------------------------------------------------------------------------------
// Author: Thuan Vu
// Created on April 14, 2021
//--------------------------------------|---------------------------------------
//  This drawing/document is copyright and the property of
//  Wolverine OilField Technologies LLC. and Remote Measurement Systems Ltd.
//  It must not be copied (in whole or in part),
//  used for manufacture or otherwise disclosed without prior written consent.
//  Any  copies of this drawing/document made by any method must also
//  include a copy of this legend. This document is supplied  without
//  liability for errors or omissions.
//
// (c) COPYRIGHT WOLVERINE OILFIELD TECHNOLOGIES LLC. AND
//               REMOTE MEASUREMENT SYSTEMS LIMITED 2021.
//-----------------------------------------------------------------------------

using NodeCommon;
using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Binary;

namespace PC_PDD
{
    public class deserializeStruct
    {
        public object GetObjectFromBytes(byte[] buffer, Type objType)
        {
            object obj = null;
            if (buffer != null && buffer.Length > 0)
            {
                var ptrObj = IntPtr.Zero;
                try
                {
                    var objSize = Marshal.SizeOf(objType);
                    if (objSize > 0)
                    {
                        if (buffer.Length < objSize)
                            throw new Exception(
                                string.Format("Buffer smaller than needed for creation of object of type {0}",
                                    objType));
                        ptrObj = Marshal.AllocHGlobal(objSize);
                        if (ptrObj != IntPtr.Zero)
                        {
                            Marshal.Copy(buffer, 0, ptrObj, objSize);
                            obj = Marshal.PtrToStructure(ptrObj, objType);
                        }
                        else
                        {
                            throw new Exception(string.Format("Couldn't allocate memory to create object of type {0}",
                                objType));
                        }
                    }
                }
                finally
                {
                    if (ptrObj != IntPtr.Zero)
                        Marshal.FreeHGlobal(ptrObj);
                }
            }

            return obj;
        }

        public byte[] Serialize(object o)
        {
            var stream = new MemoryStream();
            var formatter = new BinaryFormatter();
            formatter.AssemblyFormat
                = FormatterAssemblyStyle.Simple;
            formatter.Serialize(stream, o);
            return stream.ToArray();
        }

        public object BinaryDeSerialize(byte[] bytes)
        {
            var stream = new MemoryStream(bytes);
            var formatter = new BinaryFormatter();
            formatter.AssemblyFormat
                = FormatterAssemblyStyle.Simple;
            formatter.Binder
                = new VersionConfigToNamespaceAssemblyObjectBinder();
            var obj = formatter.Deserialize(stream);
            return obj;
        }

        public static MemoryStream SerializeToStream(object o)
        {
            var stream = new MemoryStream();
            IFormatter formatter = new BinaryFormatter();
            formatter.Serialize(stream, o);
            return stream;
        }

        public static object DeserializeFromStream(MemoryStream stream)
        {
            IFormatter formatter = new BinaryFormatter();
            stream.Seek(0, SeekOrigin.Begin);
            var o = formatter.Deserialize(stream);
            return o;
        }

        public T FromByteArray<T>(byte[] bytes) where T : struct
        {
            var ptr = IntPtr.Zero;
            try
            {
                //Make sure that the Garbage Collector doesn't move our buffer 
                var handle = GCHandle.Alloc(bytes, GCHandleType.Pinned);
                var obj = Marshal.PtrToStructure(handle.AddrOfPinnedObject(), typeof(T));
                handle.Free();
                return (T)obj;
            }
            finally
            {
                if (ptr != IntPtr.Zero)
                    Marshal.FreeHGlobal(ptr);
            }
        }

        public byte[] ToByteArray<T>(T obj) where T : struct
        {
            var ptr = IntPtr.Zero;
            try
            {
                int size = Marshal.SizeOf(typeof(T));
                byte[] bytes = new byte[size];

                ptr = Marshal.AllocHGlobal(size);
                Marshal.StructureToPtr(obj, ptr, true);
                Marshal.Copy(ptr, bytes, 0, size);
                return bytes;
            }
            finally
            {
                if (ptr != IntPtr.Zero)
                    Marshal.FreeHGlobal(ptr);
            }
        }

        public static byte[] Serialize<T>(T data)
            where T : struct
        {
            var formatter = new BinaryFormatter();
            var stream = new MemoryStream();
            formatter.Serialize(stream, data);
            return stream.ToArray();
        }

        public static T Deserialize<T>(byte[] array)
            where T : struct
        {
            var stream = new MemoryStream(array);
            var formatter = new BinaryFormatter();
            return (T)formatter.Deserialize(stream);
        }

        public sealed class Marshaller<T> : IDisposable where T : struct
        {
            private readonly int _bufferSize;
            private readonly IntPtr _handle;

            public Marshaller()
            {
                _bufferSize = Marshal.SizeOf(typeof(T));
                _handle = Marshal.AllocHGlobal(_bufferSize);
            }

            public byte[] ToByteArray(T structure)
            {
                var byteArray = new byte[_bufferSize];

                Marshal.StructureToPtr(structure, _handle, true);
                Marshal.Copy(_handle, byteArray, 0, _bufferSize);

                return byteArray;
            }

            public T ToStructure(byte[] byteArray)
            {
                var packet = new T();

                Marshal.Copy(byteArray, 0, _handle, _bufferSize);

                return Marshal.PtrToStructure<T>(_handle);
            }


            #region IDisposable Support

            private bool disposedValue;

            ~Marshaller()
            {
                Dispose();
            }

            public void Dispose()
            {
                if (!disposedValue)
                {
                    Marshal.FreeHGlobal(_handle);

                    disposedValue = true;
                }

                GC.SuppressFinalize(this);
            }

            #endregion
        }

        internal sealed class VersionConfigToNamespaceAssemblyObjectBinder : SerializationBinder
        {
            public override Type BindToType(string assemblyName, string typeName)
            {
                Type typeToDeserialize = null;
                try
                {
                    var ToAssemblyName = assemblyName.Split(',')[0];
                    var Assemblies = AppDomain.CurrentDomain.GetAssemblies();
                    foreach (var ass in Assemblies)
                        if (ass.FullName.Split(',')[0] == ToAssemblyName)
                        {
                            typeToDeserialize = ass.GetType(typeName);
                            break;
                        }
                }
                catch (Exception exception)
                {
                    Console.Write(exception.ToString());
                }

                return typeToDeserialize;
            }
        }
    }
}