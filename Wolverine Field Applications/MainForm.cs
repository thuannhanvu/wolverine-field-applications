﻿using FTD2XX_NET;
using System;
using System.Collections.Generic;
using System.Deployment.Application;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Xml;
using NodeCommon;
using PC_PDD;
using System.IO.Ports;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Text;

namespace Wolverine_Field_Applications
{
    public partial class MainForm : Form
    {
        #region Battery Calculator Variables
        public Bitmap memoryImage;
        private System.Drawing.Printing.PrintDocument docToPrint =
        new System.Drawing.Printing.PrintDocument();
        double pulseWidth = 0.32;
        int ROP = 100;
        int pulseWidthIndex = 0;
        double totalCurrentDraw = 0;
        int numberOfBatteries = 4;
        double singleBatteryLife = 0;
        double totalLife = 0;
        int Ipeak = 0, DCR = 0, Battery = 0, PulseHour = 0, Standby = 0;
        double Ihold = 0, Tpeak = 0, IholdBat = 0, AsPulse = 0, AhPulse = 0, mA = 0, Total = 0,
            joulesMin30Sec = 0, watts30sec = 0, mA30Sec = 0, joulesMin20Sec = 0, watts20sec = 0,
            mA20Sec = 0, rmsResistivityConsumption = 0, rmsPressureConsumption = 0, timeSequenceSurvey = 0,
            timeSequenceToolface = 0, timeSequenceRotate =0, bitSecondSurvey = 0, bitSecondToolface = 0, bitSecondRotate = 0, numberOfPulsesSurvey = 0, 
            numberOfPulsesToolface = 0, numberOfPulsesRotate = 0, pulsesPerHour = 0, joulesPerPulse = 0, distanceBetweenData = 0;

        string 
            textBoxSurveySequence0 = "", textBoxSurveySequence1 = "", textBoxSurveySequence2 = "", textBoxSurveySequence3 = "",
            textBoxToolfaceSequence0 = "", textBoxToolfaceSequence1 = "", textBoxToolfaceSequence2 = "", textBoxToolfaceSequence3 = "",
            textBoxRotateSequence0 = "", textBoxRotateSequence1 = "", textBoxRotateSequence2 = "", textBoxRotateSequence3 = "";
        string resistivitySource = "";
        string pressureSource = "";

        int[] M05 = new int[] { 1, 1, 1, 2, 2, 3, 3, 3, 3, 4, 4, 4, 5, 5, 5, 5, 6 };
        int[] N05 = new int[] { 6, 8, 12, 15, 17, 20, 23, 25, 28, 31, 33, 36, 38, 41, 43, 46, 48 };
        int[] M067 = new int[] { 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4, 5, 5, 6, 6, 6, 6 };
        int[] N067 = new int[] { 4, 6, 9, 11, 13, 15, 17, 19, 21, 23, 25, 26, 28, 30, 32, 34, 36 };
        int[] M625 = new int[] { 1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 5, 5, 5, 5, 6, 6 };
        int[] N625 = new int[] { 5, 7, 11, 13, 15, 18, 20, 22, 25, 27, 29, 31, 33, 36, 38, 40, 42 };

        double TNP067 = 0;
        double TD067 = 0;

        double TNP05 = 0;
        double TD05 = 0;

        double TNP625 = 1.5;
        double TD625 = .625;

        int[] surveyItems =  new int[17], toolfaceItems = new int[17], rotateItems = new int[17];

        double[] sValue050, sValue067, sValue625, bitS050, bitS067, bitS625;
        #endregion

        #region Orifice Calculator Variables
        double pumpEfficiency = 1, modelTypical = 12034, modelOrifices = 16000, mudWeight = 9, round = 1, psiToAtm = 0.068046;
        int[] Code_475_PSI = new int[] { 2, 4, 6, 7, 0, 0, 0, 0, 0, 0, 0, 0 };
        int[] Code_675_PSI = new int[] { 0, 0, 0, 0, 8, 9, 10, 11, 12, 0, 0, 0 };
        int[] Code_8_PSI = new int[] { 0, 0, 0, 0, 0, 0, 0, 1, 3, 5, 7, 8 };
        double[] GPM = new double[] { 150.0, 200.0, 250.0, 300.0, 350.0, 400.0, 450.0, 500.0, 550.0, 600.0, 700.0, 800.0, 900.0, 1000.0};
        double[] LPSXaxis = new double[] { 9, 13, 16, 19, 22, 25, 28, 32, 35, 38, 44, 50, 57, 63 };
        double[] orifice188, orifice190, orifice192, orifice194, orifice196, orifice198, orifice200, orifice202, orifice204, orifice206, orifice208, orifice210, orifice212,
            orifice478, orifice483, orifice488, orifice493, orifice498, orifice503, orifice508, orifice513, orifice518, orifice523, orifice528, orifice533, orifice538;

        double[] orificeSizeInch = new double[] { 1.88, 1.90, 1.92, 1.94, 1.96, 1.98, 2.00, 2.02, 2.04, 2.06, 2.08, 2.10, 2.12 };

        double[] orificeSizeMM = new double[] { 47.8, 48.3, 48.8, 49.3, 49.8, 50.3, 50.8, 51.3, 51.8, 52.3, 52.8, 53.3, 53.8 };
        double[] areaClosed, areaOpen;

        double tubeStopOD = 1.25, poppetOD = 1.80;

        double[] GPM150, GPM200, GPM250, GPM300, GPM350, GPM400, GPM450, GPM500, GPM550, GPM600, GPM650, GPM700, GPM750,
            GPM800, GPM850, GPM900, GPM950, GPM1000;

        double orificeFlowRate = 0;

        int[] LS9, LS13, LS16, LS19, LS22, LS25, LS28, LS32, LS35, LS38, LS44, LS50, LS57, LS63;
        #endregion

        #region FTDI REFERENCES
        public FTD2XX_NET.FTDI.FT_DEVICE_INFO_NODE[] ftdiDeviceList;
        public FTD2XX_NET.FTDI myFtdiDevice = new FTD2XX_NET.FTDI();
        public FTD2XX_NET.FTDI.FT_STATUS ftStatus = FTD2XX_NET.FTDI.FT_STATUS.FT_OK;
        public UInt32 ftdiDeviceCount;
        public int FTDI_OPEN_FLAG = 0;

        string comstr = "";

        public string comConnected = "";
        public bool localEcho = true;

        public enum LogMsgType0
        {
            Incoming,
            Outgoing,
            Normal,
            Driving,
            Error
        }
        public enum LogMsgType1
        {
            Incoming,
            Outgoing,
            Normal,
            Driving,
            Error
        }
        public enum LogMsgType2
        {
            Incoming,
            Outgoing,
            Normal,
            Driving,
            Error
        }

        public enum MsgType
        {
            MSG_TO,
            MSG_FROM,
            MSG_LENGTH0,
            MSG_LENGTH1,
            MSG_CMD,
            MSG_DATA
        }

        CommandProcessor cp = new CommandProcessor();
        ToolComms tc = new ToolComms();
        MessageHandler MH = new MessageHandler();
        deserializeStruct conv = new deserializeStruct();

        TIB_Commands tibCMD = new TIB_Commands();

        StructureVariables.SERIAL_NUMBER_STRUCTURE sns = new StructureVariables.SERIAL_NUMBER_STRUCTURE();

        StructureVariables.FIRMWARE_ID_STRUCTURE fID = new StructureVariables.FIRMWARE_ID_STRUCTURE();

        StructureVariables.MWD_MASTER_DATAGROUP_1_STRUCTURE mwdDG1 = new StructureVariables.MWD_MASTER_DATAGROUP_1_STRUCTURE();
        StructureVariables.MWD_MASTER_DATAGROUP_4_STRUCTURE mwdDG4 = new StructureVariables.MWD_MASTER_DATAGROUP_4_STRUCTURE();
        StructureVariables.MWD_MASTER_DATAGROUP_5_STRUCTURE mwdDG5 = new StructureVariables.MWD_MASTER_DATAGROUP_5_STRUCTURE();
        StructureVariables.MWD_MASTER_CONFIGURATION_STRUCTURE mwdConfig = new StructureVariables.MWD_MASTER_CONFIGURATION_STRUCTURE();
        StructureVariables.UVT_DATAGROUP_2_STRUCTURE uvtDG2 = new StructureVariables.UVT_DATAGROUP_2_STRUCTURE();

        StructureVariables.GAMMA_DATAGROUP_1_STRUCTURE gammaDG1 = new StructureVariables.GAMMA_DATAGROUP_1_STRUCTURE();

        public float newToolFace = 0;
        StructureVariables sV = new StructureVariables();

        List<double> gammaCpsList = new List<double>();

        double gammaScalingFactor = 1;

        List<double> gammaFirstFiveMinutesCpsList = new List<double>();

        List<double> gammaBackgroundCpsList = new List<double>();
        List<double> gammaLastFiveMinutesCpsList = new List<double>();

        int fiveMinutesInSeconds = 300;

        int gammaAcqState = 0;

        int gammaAcqCounter = -1;

        double calibratorApiValue = 224;
        #endregion

        #region Print Panel
        #endregion

        #region BHA Length Calculator Variables
        List<string> collarLengthTable = new List<string>();
        List<string> probeLengthTable = new List<string>();

        float mwdNmdcLength = 0;
        float bhaLength = 0;
        #endregion

        private void surveyComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region survey sequence bit item parse logic
            string[] pieces;

            switch (surveyComboBox.Text)
            {
                case "SSEQ0":
                    pieces = textBoxSurveySequence0.Split(' ');
                    break;
                case "SSEQ1":
                    pieces = textBoxSurveySequence1.Split(' ');
                    break;
                case "SSEQ2":
                    pieces = textBoxSurveySequence2.Split(' ');
                    break;
                case "SSEQ3":
                    pieces = textBoxSurveySequence3.Split(' ');
                    break;
                default:
                    pieces = textBoxSurveySequence0.Split(' ');
                    break;
            }
            string tempItem = "", tempParity = "";
            int indexStart = 0, indexStop = 0;

            for (int i = 0; i < pieces.Count(); i++)
            {
                indexStart = pieces[i].IndexOf(":");
                tempItem = pieces[i].Substring(indexStart + 1);
                indexStop = tempItem.IndexOf(":");
                tempParity = tempItem.Substring(indexStop + 1);
                tempItem = pieces[i].Substring(indexStart + 1, indexStop);

                if (tempParity == "P")
                {
                    surveyItems[Convert.ToInt32(tempItem)] += 1;
                }
                else
                {
                    surveyItems[Convert.ToInt32(tempItem) - 1] += 1;
                }
            }
            #endregion

            for (int i = 0; i < surveyItems.Count(); i++)
            {
                switch (i)
                {
                    case 0:
                        oneBitSurveyItem.Text = String.Format("{0:0}", surveyItems[i]);
                        if (surveyItems[i] > 0)
                            oneBitSurveyItem.BackColor = Color.Yellow;
                        break;
                    case 1:
                        twoBitSurveyItem.Text = String.Format("{0:0}", surveyItems[i]);
                        if (surveyItems[i] > 0)
                            twoBitSurveyItem.BackColor = Color.Yellow;
                        break;
                    case 2:
                        threeBitSurveyItem.Text = String.Format("{0:0}", surveyItems[i]);
                        if (surveyItems[i] > 0)
                            threeBitSurveyItem.BackColor = Color.Yellow;
                        break;
                    case 3:
                        fourBitSurveyItem.Text = String.Format("{0:0}", surveyItems[i]);
                        if (surveyItems[i] > 0)
                            fourBitSurveyItem.BackColor = Color.Yellow;
                        break;
                    case 4:
                        fiveBitSurveyItem.Text = String.Format("{0:0}", surveyItems[i]);
                        if (surveyItems[i] > 0)
                            fiveBitSurveyItem.BackColor = Color.Yellow;
                        break;
                    case 5:
                        sixBitSurveyItem.Text = String.Format("{0:0}", surveyItems[i]);
                        if (surveyItems[i] > 0)
                            sixBitSurveyItem.BackColor = Color.Yellow;
                        break;
                    case 6:
                        sevenBitSurveyItem.Text = String.Format("{0:0}", surveyItems[i]);
                        if (surveyItems[i] > 0)
                            sevenBitSurveyItem.BackColor = Color.Yellow;
                        break;
                    case 7:
                        eightBitSurveyItem.Text = String.Format("{0:0}", surveyItems[i]);
                        if (surveyItems[i] > 0)
                            eightBitSurveyItem.BackColor = Color.Yellow;
                        break;
                    case 8:
                        nineBitSurveyItem.Text = String.Format("{0:0}", surveyItems[i]);
                        if (surveyItems[i] > 0)
                            nineBitSurveyItem.BackColor = Color.Yellow;
                        break;
                    case 9:
                        tenBitSurveyItem.Text = String.Format("{0:0}", surveyItems[i]);
                        if (surveyItems[i] > 0)
                            tenBitSurveyItem.BackColor = Color.Yellow;
                        break;
                    case 10:
                        elevenBitSurveyItem.Text = String.Format("{0:0}", surveyItems[i]);
                        if (surveyItems[i] > 0)
                            elevenBitSurveyItem.BackColor = Color.Yellow;
                        break;
                    case 11:
                        twelveBitSurveyItem.Text = String.Format("{0:0}", surveyItems[i]);
                        if (surveyItems[i] > 0)
                            twelveBitSurveyItem.BackColor = Color.Yellow;
                        break;
                    case 12:
                        thirteenBitSurveyItem.Text = String.Format("{0:0}", surveyItems[i]);
                        if (surveyItems[i] > 0)
                            thirteenBitSurveyItem.BackColor = Color.Yellow;
                        break;
                    case 13:
                        fourteenBitSurveyItem.Text = String.Format("{0:0}", surveyItems[i]);
                        if (surveyItems[i] > 0)
                            fourteenBitSurveyItem.BackColor = Color.Yellow;
                        break;
                    case 14:
                        fifteenBitSurveyItem.Text = String.Format("{0:0}", surveyItems[i]);
                        if (surveyItems[i] > 0)
                            fifteenBitSurveyItem.BackColor = Color.Yellow;
                        break;
                    case 15:
                        sixteenBitSurveyItem.Text = String.Format("{0:0}", surveyItems[i]);
                        if (surveyItems[i] > 0)
                            sixteenBitSurveyItem.BackColor = Color.Yellow;
                        break;
                    case 16:
                        seventeenBitSurveyItem.Text = String.Format("{0:0}", surveyItems[i]);
                        if (surveyItems[i] > 0)
                            seventeenBitSurveyItem.BackColor = Color.Yellow;
                        break;
                }
            }

            InitializeTelemetryBox();
        }

        private void toolfaceComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region toolface sequence bit item parse logic
            string[] pieces;

            switch (toolfaceComboBox.Text)
            {
                case "TFSEQ0":
                    pieces = textBoxToolfaceSequence0.Split(' ');
                    break;
                case "TFSEQ1":
                    pieces = textBoxToolfaceSequence1.Split(' ');
                    break;
                case "TFSEQ2":
                    pieces = textBoxToolfaceSequence2.Split(' ');
                    break;
                case "TFSEQ3":
                    pieces = textBoxToolfaceSequence3.Split(' ');
                    break;
                default:
                    pieces = textBoxToolfaceSequence0.Split(' ');
                    break;
            }
            string tempItem = "", tempParity = "";
            int indexStart = 0, indexStop = 0;

            for (int i = 0; i < pieces.Count(); i++)
            {
                indexStart = pieces[i].IndexOf(":");
                tempItem = pieces[i].Substring(indexStart + 1);
                indexStop = tempItem.IndexOf(":");
                tempParity = tempItem.Substring(indexStop + 1);
                tempItem = pieces[i].Substring(indexStart + 1, indexStop);

                if (tempParity == "P")
                {
                    toolfaceItems[Convert.ToInt32(tempItem)] += 1;
                }
                else
                {
                    toolfaceItems[Convert.ToInt32(tempItem) - 1] += 1;
                }
            }
            #endregion

            for (int i = 0; i < toolfaceItems.Count(); i++)
            {
                switch (i)
                {
                    case 0:
                        oneBitToolfaceItem.Text = String.Format("{0:0}", toolfaceItems[i]);
                        if (toolfaceItems[i] > 0)
                            oneBitToolfaceItem.BackColor = Color.Yellow;
                        break;
                    case 1:
                        twoBitToolfaceItem.Text = String.Format("{0:0}", toolfaceItems[i]);
                        if (toolfaceItems[i] > 0)
                            twoBitToolfaceItem.BackColor = Color.Yellow;
                        break;
                    case 2:
                        threeBitToolfaceItem.Text = String.Format("{0:0}", toolfaceItems[i]);
                        if (toolfaceItems[i] > 0)
                            threeBitToolfaceItem.BackColor = Color.Yellow;
                        break;
                    case 3:
                        fourBitToolfaceItem.Text = String.Format("{0:0}", toolfaceItems[i]);
                        if (toolfaceItems[i] > 0)
                            fourBitToolfaceItem.BackColor = Color.Yellow;
                        break;
                    case 4:
                        fiveBitToolfaceItem.Text = String.Format("{0:0}", toolfaceItems[i]);
                        if (toolfaceItems[i] > 0)
                            fiveBitToolfaceItem.BackColor = Color.Yellow;
                        break;
                    case 5:
                        sixBitToolfaceItem.Text = String.Format("{0:0}", toolfaceItems[i]);
                        if (toolfaceItems[i] > 0)
                            sixBitToolfaceItem.BackColor = Color.Yellow;
                        break;
                    case 6:
                        sevenBitToolfaceItem.Text = String.Format("{0:0}", toolfaceItems[i]);
                        if (toolfaceItems[i] > 0)
                            sevenBitToolfaceItem.BackColor = Color.Yellow;
                        break;
                    case 7:
                        eightBitToolfaceItem.Text = String.Format("{0:0}", toolfaceItems[i]);
                        if (toolfaceItems[i] > 0)
                            eightBitToolfaceItem.BackColor = Color.Yellow;
                        break;
                    case 8:
                        nineBitToolfaceItem.Text = String.Format("{0:0}", toolfaceItems[i]);
                        if (toolfaceItems[i] > 0)
                            nineBitToolfaceItem.BackColor = Color.Yellow;
                        break;
                    case 9:
                        tenBitToolfaceItem.Text = String.Format("{0:0}", toolfaceItems[i]);
                        if (toolfaceItems[i] > 0)
                            tenBitToolfaceItem.BackColor = Color.Yellow;
                        break;
                    case 10:
                        elevenBitToolfaceItem.Text = String.Format("{0:0}", toolfaceItems[i]);
                        if (toolfaceItems[i] > 0)
                            elevenBitToolfaceItem.BackColor = Color.Yellow;
                        break;
                    case 11:
                        twelveBitToolfaceItem.Text = String.Format("{0:0}", toolfaceItems[i]);
                        if (toolfaceItems[i] > 0)
                            twelveBitToolfaceItem.BackColor = Color.Yellow;
                        break;
                    case 12:
                        thirteenBitToolfaceItem.Text = String.Format("{0:0}", toolfaceItems[i]);
                        if (toolfaceItems[i] > 0)
                            thirteenBitToolfaceItem.BackColor = Color.Yellow;
                        break;
                    case 13:
                        fourteenBitToolfaceItem.Text = String.Format("{0:0}", toolfaceItems[i]);
                        if (toolfaceItems[i] > 0)
                            fourteenBitToolfaceItem.BackColor = Color.Yellow;
                        break;
                    case 14:
                        fifteenBitToolfaceItem.Text = String.Format("{0:0}", toolfaceItems[i]);
                        if (toolfaceItems[i] > 0)
                            fifteenBitToolfaceItem.BackColor = Color.Yellow;
                        break;
                    case 15:
                        sixteenBitToolfaceItem.Text = String.Format("{0:0}", toolfaceItems[i]);
                        if (toolfaceItems[i] > 0)
                            sixteenBitToolfaceItem.BackColor = Color.Yellow;
                        break;
                    case 16:
                        seventeenBitToolfaceItem.Text = String.Format("{0:0}", toolfaceItems[i]);
                        if (toolfaceItems[i] > 0)
                            seventeenBitToolfaceItem.BackColor = Color.Yellow;
                        break;
                }
            }

            InitializeTelemetryBox();
        }

        private void rotateComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region rotate sequence bit item parse logic
            string[] pieces;

            switch (toolfaceComboBox.Text)
            {
                case "RSEQ0":
                    pieces = textBoxRotateSequence0.Split(' ');
                    break;
                case "RSEQ1":
                    pieces = textBoxRotateSequence1.Split(' ');
                    break;
                case "RSEQ2":
                    pieces = textBoxRotateSequence2.Split(' ');
                    break;
                case "RSEQ3":
                    pieces = textBoxRotateSequence3.Split(' ');
                    break;
                default:
                    pieces = textBoxRotateSequence0.Split(' ');
                    break;
            }
            string tempItem = "", tempParity = "";
            int indexStart = 0, indexStop = 0;

            for (int i = 0; i < pieces.Count(); i++)
            {
                indexStart = pieces[i].IndexOf(":");
                tempItem = pieces[i].Substring(indexStart + 1);
                indexStop = tempItem.IndexOf(":");
                tempParity = tempItem.Substring(indexStop + 1);
                tempItem = pieces[i].Substring(indexStart + 1, indexStop);

                if (tempParity == "P")
                {
                    rotateItems[Convert.ToInt32(tempItem)] += 1;
                }
                else
                {
                    rotateItems[Convert.ToInt32(tempItem) - 1] += 1;
                }
            }
            #endregion

            for (int i = 0; i < toolfaceItems.Count(); i++)
            {
                switch (i)
                {
                    case 0:
                        oneBitRotateItem.Text = String.Format("{0:0}", rotateItems[i]);
                        if (rotateItems[i] > 0)
                            oneBitRotateItem.BackColor = Color.Yellow;
                        break;
                    case 1:
                        twoBitRotateItem.Text = String.Format("{0:0}", rotateItems[i]);
                        if (rotateItems[i] > 0)
                            twoBitRotateItem.BackColor = Color.Yellow;
                        break;
                    case 2:
                        threeBitRotateItem.Text = String.Format("{0:0}", rotateItems[i]);
                        if (toolfaceItems[i] > 0)
                            threeBitToolfaceItem.BackColor = Color.Yellow;
                        break;
                    case 3:
                        fourBitRotateItem.Text = String.Format("{0:0}", rotateItems[i]);
                        if (rotateItems[i] > 0)
                            fourBitRotateItem.BackColor = Color.Yellow;
                        break;
                    case 4:
                        fiveBitRotateItem.Text = String.Format("{0:0}", rotateItems[i]);
                        if (rotateItems[i] > 0)
                            fiveBitRotateItem.BackColor = Color.Yellow;
                        break;
                    case 5:
                        sixBitRotateItem.Text = String.Format("{0:0}", rotateItems[i]);
                        if (rotateItems[i] > 0)
                            sixBitRotateItem.BackColor = Color.Yellow;
                        break;
                    case 6:
                        sevenBitRotateItem.Text = String.Format("{0:0}", rotateItems[i]);
                        if (rotateItems[i] > 0)
                            sevenBitRotateItem.BackColor = Color.Yellow;
                        break;
                    case 7:
                        eightBitRotateItem.Text = String.Format("{0:0}", rotateItems[i]);
                        if (rotateItems[i] > 0)
                            eightBitToolfaceItem.BackColor = Color.Yellow;
                        break;
                    case 8:
                        nineBitRotateItem.Text = String.Format("{0:0}", rotateItems[i]);
                        if (rotateItems[i] > 0)
                            nineBitRotateItem.BackColor = Color.Yellow;
                        break;
                    case 9:
                        tenBitRotateItem.Text = String.Format("{0:0}", rotateItems[i]);
                        if (rotateItems[i] > 0)
                            tenBitRotateItem.BackColor = Color.Yellow;
                        break;
                    case 10:
                        elevenBitRotateItem.Text = String.Format("{0:0}", rotateItems[i]);
                        if (rotateItems[i] > 0)
                            elevenBitRotateItem.BackColor = Color.Yellow;
                        break;
                    case 11:
                        twelveBitRotateItem.Text = String.Format("{0:0}", rotateItems[i]);
                        if (rotateItems[i] > 0)
                            twelveBitRotateItem.BackColor = Color.Yellow;
                        break;
                    case 12:
                        thirteenBitRotateItem.Text = String.Format("{0:0}", rotateItems[i]);
                        if (rotateItems[i] > 0)
                            thirteenBitRotateItem.BackColor = Color.Yellow;
                        break;
                    case 13:
                        fourteenBitRotateItem.Text = String.Format("{0:0}", rotateItems[i]);
                        if (rotateItems[i] > 0)
                            fourteenBitRotateItem.BackColor = Color.Yellow;
                        break;
                    case 14:
                        fifteenBitRotateItem.Text = String.Format("{0:0}", rotateItems[i]);
                        if (rotateItems[i] > 0)
                            fifteenBitRotateItem.BackColor = Color.Yellow;
                        break;
                    case 15:
                        sixteenBitRotateItem.Text = String.Format("{0:0}", rotateItems[i]);
                        if (rotateItems[i] > 0)
                            sixteenBitRotateItem.BackColor = Color.Yellow;
                        break;
                    case 16:
                        seventeenBitRotateItem.Text = String.Format("{0:0}", rotateItems[i]);
                        if (rotateItems[i] > 0)
                            seventeenBitRotateItem.BackColor = Color.Yellow;
                        break;
                }
            }

            InitializeTelemetryBox();
        }

        private async void dmDataButton_Click(object sender, EventArgs e)
        {
            clearBoxes();

            #region Datagroup5 Read
            byte[] message = cp.buildMessage(Common.NODE_MASTER, Common.CMD_DATAGROUP_RDWR, Common.DATAGROUP_5);

            if (!myFtdiDevice.IsOpen)
            {
                Log(LogMsgType0.Outgoing, "Serial Port is Not Open!", 2);
                return;
            }

            if (await tc.FTDI_SEND(message, myFtdiDevice, ftStatus, this, localEcho))
            {
                //await Task.Delay(200);
            }
            else
            {
                return; // message failed
            }
            //if (MH.checkSumVerify(tc.rxNoEcho.ToArray(), Common.MESSAGE_OVERHEAD + Marshal.SizeOf(mwdDG1) + 1))
            if (MH.checkSumVerify(tc.rxNoEcho.ToArray()))
            {
                try
                {
                    mwdDG5 = conv.FromByteArray<StructureVariables.MWD_MASTER_DATAGROUP_5_STRUCTURE>(tc.rxData.ToArray());

                    Log(LogMsgType0.Outgoing, "Sequence Number: " + String.Format("{0:0.0000}", mwdDG5.SequenceNumber), 2);
                    switch (mwdDG5.Mode)
                    {
                        case Common.MODE_STARTUP:
                            Log(LogMsgType0.Outgoing, "Mode = STARTUP", 2);
                            break;
                        case Common.MODE_IDLE:
                            Log(LogMsgType0.Outgoing, "Mode = IDLE", 2);
                            break;
                        case Common.MODE_TRIP:
                            Log(LogMsgType0.Outgoing, "Mode = MODE_TRIP", 2);
                            break;
                        case Common.MODE_ACTIVE:
                            Log(LogMsgType0.Outgoing, "Mode = ACTIVE", 2);
                            break;
                        case Common.MODE_DOWNLOAD:
                            Log(LogMsgType0.Outgoing, "Mode = DOWNLOAD", 2);
                            break;
                        case Common.MODE_ROLLTEST:
                            Log(LogMsgType0.Outgoing, "Mode = ROLL TEST", 2);
                            break;
                        default:
                            Log(LogMsgType0.Outgoing, "Mode = UNKNOWN", 2);
                            break;
                    }
                    Log(LogMsgType0.Outgoing, "VBusVolts: " + String.Format("{0:0.0000}", mwdDG5.VBusVolts), 2);
                    Log(LogMsgType0.Outgoing, "Temperature Celsius: " + String.Format("{0:0.0000}", mwdDG5.TemperatureCelsius), 2);
                    Log(LogMsgType0.Outgoing, "\nUncorrected vectors, nominal units", 2);
                    Log(LogMsgType0.Outgoing, "GxG0: " + String.Format("{0:0.0000}", mwdDG5.GxG0), 2);
                    Log(LogMsgType0.Outgoing, "GyG0;: " + String.Format("{0:0.0000}", mwdDG5.GyG0), 2);
                    Log(LogMsgType0.Outgoing, "GzG0: " + String.Format("{0:0.0000}", mwdDG5.GzG0), 2);
                    Log(LogMsgType0.Outgoing, "BxGauss0: " + String.Format("{0:0.0000}", mwdDG5.BxGauss0), 2);
                    Log(LogMsgType0.Outgoing, "ByGauss0: " + String.Format("{0:0.0000}", mwdDG5.ByGauss0), 2);
                    Log(LogMsgType0.Outgoing, "BzGauss0: " + String.Format("{0:0.0000}", mwdDG5.BzGauss0), 2);
                    Log(LogMsgType0.Outgoing, "\nCorrected vectors", 2);
                    Log(LogMsgType0.Outgoing, "GxG: " + String.Format("{0:0.0000}", mwdDG5.GxG), 2);
                    Log(LogMsgType0.Outgoing, "GyG: " + String.Format("{0:0.0000}", mwdDG5.GyG), 2);
                    Log(LogMsgType0.Outgoing, "GzG: " + String.Format("{0:0.0000}", mwdDG5.GzG), 2);
                    Log(LogMsgType0.Outgoing, "BxGauss: " + String.Format("{0:0.0000}", mwdDG5.BxGauss), 2);
                    Log(LogMsgType0.Outgoing, "ByGauss: " + String.Format("{0:0.0000}", mwdDG5.ByGauss), 2);
                    Log(LogMsgType0.Outgoing, "BzGauss: " + String.Format("{0:0.0000}", mwdDG5.BzGauss), 2);
                }
                catch
                {

                }
            }
            #endregion
        }

        private async void rollTestButton_Click(object sender, EventArgs e)
        {
            clearBoxes();

            byte[] message = cp.buildMessage(Common.NODE_MASTER, Common.CMD_MODE_RDWR, Common.MODE_ROLLTEST);

            if (!myFtdiDevice.IsOpen)
            {
                Log(LogMsgType0.Outgoing, "Serial Port is Not Open!", 2);
                return;
            }

            if (await tc.FTDI_SEND(message, myFtdiDevice, ftStatus, this, localEcho))
            {
                //await Task.Delay(200);
            }
            else
            {
                return; // message failed
            }
            //if (MH.checkSumVerify(tc.rxNoEcho.ToArray(), Common.MESSAGE_OVERHEAD + Marshal.SizeOf(mwdDG1) + 1))
            if (MH.checkSumVerify(tc.rxNoEcho.ToArray()))
            {
                if (tc.rxNoEcho.ToArray()[5] == Common.ACK)
                {
                    Log(LogMsgType0.Outgoing, "MODE CHANGE: ROLL TEST", 2);

                    RollTest newRTF = new RollTest(myFtdiDevice, ftStatus, tc, "Test", this);
                    newRTF.StartPosition = FormStartPosition.CenterScreen;

                    newRTF.Show();
                }
                else
                    Log(LogMsgType0.Outgoing, "MODE CHANGE FAILED", 2);
            }
        }

        private async void readGammaButton_Click(object sender, EventArgs e)
        {
            clearBoxes();

            if (woftGammaCheckBox.Checked)
            {
                #region UVT Datagroup2 Read
                byte[] message = cp.buildMessage(Common.NODE_UVT, Common.CMD_DATAGROUP_RDWR, Common.DATAGROUP_2);

                if (!myFtdiDevice.IsOpen)
                {
                    return;
                }

                if (await tc.FTDI_SEND(message, myFtdiDevice, ftStatus, this, localEcho))
                {
                    //await Task.Delay(200);
                }
                else
                {
                    return; // message failed
                }
                if (MH.checkSumVerify(tc.rxNoEcho.ToArray(), Common.MESSAGE_OVERHEAD + Marshal.SizeOf(uvtDG2) + 1))
                {
                    try
                    {
                        uvtDG2 = conv.FromByteArray<StructureVariables.UVT_DATAGROUP_2_STRUCTURE>(tc.rxData.ToArray());

                        dataBox.Text = "NODE STATUS: " + String.Format("{0:X4}", uvtDG2.NodeStatus) + "\n" +
                            "DATETIME: " + uvtDG2.TimeStampDate[(int)Common.DATETIME.MONTH].ToString("D2") + "-" +
                                uvtDG2.TimeStampDate[(int)Common.DATETIME.DAY].ToString("D2") + "-" +
                                (uvtDG2.TimeStampDate[(int)Common.DATETIME.YEAR] + 2000) + " " +
                                uvtDG2.TimeStampDate[(int)Common.DATETIME.HOUR].ToString("D2") + ":" +
                                uvtDG2.TimeStampDate[(int)Common.DATETIME.MINUTE].ToString("D2") + ":" +
                                uvtDG2.TimeStampDate[(int)Common.DATETIME.SECOND].ToString("D2") + "," + "\n" +
                                "GAMMA AVERAGE WINDOW SECONDS: " + uvtDG2.GammaCountsPerSecond + "\n" +
                                "GAMMA CPS: " + String.Format("{0:0.000000}", uvtDG2.GammaCountsPerSecond);

                        updateAlerts();
                    }
                    catch
                    {
                    }
                }
                #endregion
            }
            else if (digitalGammaCheckbox.Checked)
            {
                #region Datagroup1 Read
                byte[] message = cp.buildMessage(Common.NODE_DIGITAL_GAMMA, Common.CMD_DATAGROUP_RDWR, Common.DATAGROUP_1);

                if (!myFtdiDevice.IsOpen)
                {
                    Log(LogMsgType0.Outgoing, "Serial Port is Not Open!", 2);
                    return;
                }

                if (await tc.FTDI_SEND(message, myFtdiDevice, ftStatus, this, localEcho))
                {
                    //await Task.Delay(200);
                }
                else
                {
                    return; // message failed
                }
                if (MH.checkSumVerify(tc.rxNoEcho.ToArray(), Common.MESSAGE_OVERHEAD + Marshal.SizeOf(gammaDG1) + 1))
                //if (MH.checkSumVerify(tc.rxNoEcho.ToArray()))
                {
                    try
                    {
                        gammaDG1 = conv.FromByteArray<StructureVariables.GAMMA_DATAGROUP_1_STRUCTURE>(tc.rxData.ToArray());

                        updateAlerts();

                        //Log(LogMsgType0.Outgoing, "\nDatagroup 1\nDateTime = " + gammaDG1.TimeStampDate[(int)Common.DATETIME.MONTH].ToString("D2") + "/" +
                        //                                            gammaDG1.TimeStampDate[(int)Common.DATETIME.DAY].ToString("D2") + "/" +
                        //                                            (gammaDG1.TimeStampDate[(int)Common.DATETIME.YEAR] + 2000) + " " +
                        //                                            gammaDG1.TimeStampDate[(int)Common.DATETIME.HOUR].ToString("D2") +
                        //                                            ":" + mwdDG1.TimeStampDate[(int)Common.DATETIME.MINUTE].ToString("D2") + ":" +
                        //                                            gammaDG1.TimeStampDate[(int)Common.DATETIME.SECOND].ToString("D2"), 2);

                        switch (gammaDG1.Mode)
                        {
                            case Common.MODE_STARTUP:
                                Log(LogMsgType0.Outgoing, "Mode = STARTUP", 2);
                                break;
                            case Common.MODE_IDLE:
                                Log(LogMsgType0.Outgoing, "Mode = IDLE", 2);
                                break;
                            case Common.MODE_TRIP:
                                Log(LogMsgType0.Outgoing, "Mode = MODE_TRIP", 2);
                                break;
                            case Common.MODE_ACTIVE:
                                Log(LogMsgType0.Outgoing, "Mode = ACTIVE", 2);
                                break;
                            case Common.MODE_DOWNLOAD:
                                Log(LogMsgType0.Outgoing, "Mode = DOWNLOAD", 2);
                                break;
                            case Common.MODE_CALIBRATION:
                                Log(LogMsgType0.Outgoing, "Mode = CALIBRATION", 2);
                                break;
                            default:
                                Log(LogMsgType0.Outgoing, "Mode = UNKNOWN", 2);
                                break;
                        }

                        Log(LogMsgType0.Outgoing, String.Format("TimeStampSeconds: {0:0}", gammaDG1.TimeStampSeconds), 2);
                        Log(LogMsgType0.Outgoing, String.Format("BatBusVolts: {0:0.0000}", gammaDG1.BatBusVolts), 2);
                        Log(LogMsgType0.Outgoing, String.Format("Bat1Volts: {0:0.0000}", gammaDG1.Bat1Volts), 2);
                        Log(LogMsgType0.Outgoing, String.Format("Bat2Volts: {0:0.0000}", gammaDG1.Bat2Volts), 2);
                        Log(LogMsgType0.Outgoing, String.Format("TemperatureCelsius: {0:0.0000}", gammaDG1.TemperatureCelsius), 2);
                        Log(LogMsgType0.Outgoing, String.Format("FlowState: {0:0.0000}", gammaDG1.FlowState), 2);
                        Log(LogMsgType0.Outgoing, String.Format("CirculatingHours: {0:0.0000}", gammaDG1.CirculatingHours), 2);
                        Log(LogMsgType0.Outgoing, String.Format("\nGammaCountsPerSecond: {0:0.0000}", gammaDG1.GammaCountsPerSecond), 2);

                        Log(LogMsgType0.Outgoing, String.Format("\nTestVariables1: {0:0.0000}", gammaDG1.TestVariable1), 2);
                        Log(LogMsgType0.Outgoing, String.Format("TestVariables2: {0:0.0000}", gammaDG1.TestVariable2), 2);
                        Log(LogMsgType0.Outgoing, String.Format("TestVariables3: {0:0.0000}", gammaDG1.TestVariable3), 2);
                        Log(LogMsgType0.Outgoing, String.Format("TestVariables4: {0:0.0000}", gammaDG1.TestVariable4), 2);

                        if (mwdDG1.InclinationDegrees > 5)
                            newToolFace = mwdDG1.GravityToolFaceDegrees;
                        else
                            newToolFace = mwdDG1.MagneticToolFaceDegrees;

                        timerTextBox.Text = String.Format("{0:0.0000}", newToolFace);
                    }
                    catch
                    {
                    }
                }
                #endregion
            }
        }

        private void woftGammaCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if(woftGammaCheckBox.Checked)
            {
                digitalGammaCheckbox.Checked = false;
            }
        }

        private void digitalGammaCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            if(digitalGammaCheckbox.Checked)
            {
                woftGammaCheckBox.Checked = false;
            }
        }

        private async void readKTableButton_Click(object sender, EventArgs e)
        {
            clearBoxes();

            #region Datagroup4 Read
            byte[] message = cp.buildMessage(Common.NODE_MASTER, Common.CMD_DATAGROUP_RDWR, Common.DATAGROUP_4);

            if (!myFtdiDevice.IsOpen)
            {
                Log(LogMsgType0.Outgoing, "Serial Port is Not Open!", 2);
                return;
            }

            if (await tc.FTDI_SEND(message, myFtdiDevice, ftStatus, this, localEcho))
            {
                //await Task.Delay(200);
            }
            else
            {
                return; // message failed
            }
            //if (MH.checkSumVerify(tc.rxNoEcho.ToArray(), Common.MESSAGE_OVERHEAD + Marshal.SizeOf(mwdDG1) + 1))
            if (MH.checkSumVerify(tc.rxNoEcho.ToArray(), Common.MESSAGE_OVERHEAD + Marshal.SizeOf(mwdDG4) + 1))
            {
                try
                {
                    mwdDG4 = conv.FromByteArray<StructureVariables.MWD_MASTER_DATAGROUP_4_STRUCTURE>(tc.rxData.ToArray());
                    string temp = new string (mwdDG4.KtableModel);
                    Log(LogMsgType0.Outgoing, "KtableModel: " + temp, 2);
                    temp = new string(mwdDG4.HardwareModel);
                    Log(LogMsgType0.Outgoing, "HardwareModel: " + temp, 2);
                    temp = new string(mwdDG4.CalDate);
                    Log(LogMsgType0.Outgoing, "CalDate: " + temp, 2);
                    temp = new string(mwdDG4.CalTime);
                    Log(LogMsgType0.Outgoing, "CalTime: " + temp, 2);
                    Log(LogMsgType0.Outgoing, "KtType: " + mwdDG4.KtType, 2);
                    Log(LogMsgType0.Outgoing, "KtEEpromAdd: " + mwdDG4.KtEEpromAdd, 2);
                    Log(LogMsgType0.Outgoing, "KtMode: " + mwdDG4.KtMode, 2);
                    Log(LogMsgType0.Outgoing, "ADCtype: " + mwdDG4.ADCtype, 2);
                    Log(LogMsgType0.Outgoing, "BiaxTriax: " + mwdDG4.BiaxTriax, 2);
                    Log(LogMsgType0.Outgoing, "Checksum: " + mwdDG4.Checksum, 2);
                }
                catch
                {
                }
            }
            #endregion
        }

        private void apsWpr_CheckedChanged(object sender, EventArgs e)
        {
            checkToolString();
        }

        private async void readEepromButton_Click(object sender, EventArgs e)
        {
            clearBoxes();

            byte[] message = cp.buildMessage(Common.NODE_MASTER, Common.CMD_ACTION, Common.MWD_MASTER_ACTION_READ_EEPROM, 0x01);
            if (!myFtdiDevice.IsOpen) // Is our serial port open?
                return;
            if (await tc.FTDI_SEND(message, myFtdiDevice, ftStatus, this, localEcho)) { }
            else
            {
                Log(LogMsgType0.Outgoing, "EEPROM READ FAILED", 2);
                return; // message failed
            }
            //User Requesting DataGroup1 from Master
            if (MH.checkSumVerify(tc.rxNoEcho.ToArray()))
            {
                if (tc.rxNoEcho.ToArray()[5] == Common.ACK)
                {
                    Log(LogMsgType0.Outgoing, "EEPROM READ SUCCESS", 2);
                }
            }
        }

        private void probeCombo1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] aux = probeLengthTable.ElementAt(probeCombo1.SelectedIndex).Split(',');
                probeUnits1.Items.Clear();
                probeUnits1.Items.Add("INCH");
                probeUnits1.SelectedIndex = 0;
                probeLength1.Text = aux[2];
                probeNotes1.Text = "";
                probeNotes1.Text = aux[3];
            }
            catch { }
        }

        private void probeCombo2_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] aux = probeLengthTable.ElementAt(probeCombo2.SelectedIndex).Split(',');
                probeUnits2.Items.Clear();
                probeUnits2.Items.Add("INCH");
                probeUnits2.SelectedIndex = 0;
                probeLength2.Text = aux[2];
                probeNotes2.Text = "";
                probeNotes2.Text = aux[3];
            }
            catch { }
        }

        private void probeCombo3_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] aux = probeLengthTable.ElementAt(probeCombo3.SelectedIndex).Split(',');
                probeUnits3.Items.Clear();
                probeUnits3.Items.Add("INCH");
                probeUnits3.SelectedIndex = 0;
                probeLength3.Text = aux[2];
                probeNotes3.Text = "";
                probeNotes3.Text = aux[3];
            }
            catch { }
        }

        private void probeCombo4_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] aux = probeLengthTable.ElementAt(probeCombo4.SelectedIndex).Split(',');
                probeUnits4.Items.Clear();
                probeUnits4.Items.Add("INCH");
                probeUnits4.SelectedIndex = 0;
                probeLength4.Text = aux[2];
                probeNotes4.Text = "";
                probeNotes4.Text = aux[3];
            }
            catch { }
        }

        private void probeCombo5_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] aux = probeLengthTable.ElementAt(probeCombo5.SelectedIndex).Split(',');
                probeUnits5.Items.Clear();
                probeUnits5.Items.Add("INCH");
                probeUnits5.SelectedIndex = 0;
                probeLength5.Text = aux[2];
                probeNotes5.Text = "";
                probeNotes5.Text = aux[3];
            }
            catch { }
        }

        private void probeCombo6_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] aux = probeLengthTable.ElementAt(probeCombo6.SelectedIndex).Split(',');
                probeUnits6.Items.Clear();
                probeUnits6.Items.Add("INCH");
                probeUnits6.SelectedIndex = 0;
                probeLength6.Text = aux[2];
                probeNotes6.Text = "";
                probeNotes6.Text = aux[3];
            }
            catch { }
        }

        private void probeCombo7_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] aux = probeLengthTable.ElementAt(probeCombo7.SelectedIndex).Split(',');
                probeUnits7.Items.Clear();
                probeUnits7.Items.Add("INCH");
                probeUnits7.SelectedIndex = 0;
                probeLength7.Text = aux[2];
                probeNotes7.Text = "";
                probeNotes7.Text = aux[3];
            }
            catch { }
        }

        private void probeCombo8_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] aux = probeLengthTable.ElementAt(probeCombo8.SelectedIndex).Split(',');
                probeUnits8.Items.Clear();
                probeUnits8.Items.Add("INCH");
                probeUnits8.SelectedIndex = 0;
                probeLength8.Text = aux[2];
                probeNotes8.Text = "";
                probeNotes8.Text = aux[3];
            }
            catch { }
        }

        private void probeCombo9_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] aux = probeLengthTable.ElementAt(probeCombo9.SelectedIndex).Split(',');
                probeUnits9.Items.Clear();
                probeUnits9.Items.Add("INCH");
                probeUnits9.SelectedIndex = 0;
                probeLength9.Text = aux[2];
                probeNotes9.Text = "";
                probeNotes9.Text = aux[3];
            }
            catch { }
        }

        private void probeCombo10_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] aux = probeLengthTable.ElementAt(probeCombo10.SelectedIndex).Split(',');
                probeUnits10.Items.Clear();
                probeUnits10.Items.Add("INCH");
                probeUnits10.SelectedIndex = 0;
                probeLength10.Text = aux[2];
                probeNotes10.Text = "";
                probeNotes10.Text = aux[3];
            }
            catch { }
        }

        private void probeCombo11_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] aux = probeLengthTable.ElementAt(probeCombo11.SelectedIndex).Split(',');
                probeUnits11.Items.Clear();
                probeUnits11.Items.Add("INCH");
                probeUnits11.SelectedIndex = 0;
                probeLength11.Text = aux[2];
                probeNotes11.Text = "";
                probeNotes11.Text = aux[3];
            }
            catch { }
        }

        private void probeCombo12_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] aux = probeLengthTable.ElementAt(probeCombo12.SelectedIndex).Split(',');
                probeUnits12.Items.Clear();
                probeUnits12.Items.Add("INCH");
                probeUnits12.SelectedIndex = 0;
                probeLength12.Text = aux[2];
                probeNotes12.Text = "";
                probeNotes12.Text = aux[3];
            }
            catch { }
        }

        private void probeCombo13_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] aux = probeLengthTable.ElementAt(probeCombo13.SelectedIndex).Split(',');
                probeUnits13.Items.Clear();
                probeUnits13.Items.Add("INCH");
                probeUnits13.SelectedIndex = 0;
                probeLength13.Text = aux[2];
                probeNotes13.Text = "";
                probeNotes13.Text = aux[3];
            }
            catch { }
        }

        private void probeCombo14_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] aux = probeLengthTable.ElementAt(probeCombo14.SelectedIndex).Split(',');
                probeUnits14.Items.Clear();
                probeUnits14.Items.Add("INCH");
                probeUnits14.SelectedIndex = 0;
                probeLength14.Text = aux[2];
                probeNotes14.Text = "";
                probeNotes14.Text = aux[3];
            }
            catch { }
        }

        private void probeCombo15_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] aux = probeLengthTable.ElementAt(probeCombo15.SelectedIndex).Split(',');
                probeUnits15.Items.Clear();
                probeUnits15.Items.Add("INCH");
                probeUnits15.SelectedIndex = 0;
                probeLength15.Text = aux[2];
                probeNotes15.Text = "";
                probeNotes15.Text = aux[3];
            }
            catch { }
        }

        private void probeCombo16_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] aux = probeLengthTable.ElementAt(probeCombo16.SelectedIndex).Split(',');
                probeUnits16.Items.Clear();
                probeUnits16.Items.Add("INCH");
                probeUnits16.SelectedIndex = 0;
                probeLength16.Text = aux[2];
                probeNotes16.Text = "";
                probeNotes16.Text = aux[3];
            }
            catch { }
        }

        private void probeCombo17_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] aux = probeLengthTable.ElementAt(probeCombo17.SelectedIndex).Split(',');
                probeUnits17.Items.Clear();
                probeUnits17.Items.Add("INCH");
                probeUnits17.SelectedIndex = 0;
                probeLength17.Text = aux[2];
                probeNotes17.Text = "";
                probeNotes17.Text = aux[3];
            }
            catch { }
        }

        private void probeCombo18_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] aux = probeLengthTable.ElementAt(probeCombo18.SelectedIndex).Split(',');
                probeUnits18.Items.Clear();
                probeUnits18.Items.Add("INCH");
                probeUnits18.SelectedIndex = 0;
                probeLength18.Text = aux[2];
                probeNotes18.Text = "";
                probeNotes18.Text = aux[3];
            }
            catch { }
        }

        private void probeCombo19_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] aux = probeLengthTable.ElementAt(probeCombo19.SelectedIndex).Split(',');
                probeUnits19.Items.Clear();
                probeUnits19.Items.Add("INCH");
                probeUnits19.SelectedIndex = 0;
                probeLength19.Text = aux[2];
                probeNotes19.Text = "";
                probeNotes19.Text = aux[3];
            }
            catch { }
        }

        private void probeCombo20_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] aux = probeLengthTable.ElementAt(probeCombo20.SelectedIndex).Split(',');
                probeUnits20.Items.Clear();
                probeUnits20.Items.Add("INCH");
                probeUnits20.SelectedIndex = 0;
                probeLength20.Text = aux[2];
                probeNotes20.Text = "";
                probeNotes20.Text = aux[3];
            }
            catch { }
        }

        private void calculateMwdNmdcLength()
        {
            try
            {
                calculateNmdcLengthBox.BackColor = Color.Empty;
                mwdNmdcLength = 0;
                mwdNmdcLength += Convert.ToSingle(probeLength1.Text);
                mwdNmdcLength += Convert.ToSingle(probeLength2.Text);
                mwdNmdcLength += Convert.ToSingle(probeLength3.Text);
                mwdNmdcLength += Convert.ToSingle(probeLength4.Text);
                mwdNmdcLength += Convert.ToSingle(probeLength5.Text);
                mwdNmdcLength += Convert.ToSingle(probeLength6.Text);
                mwdNmdcLength += Convert.ToSingle(probeLength7.Text);
                mwdNmdcLength += Convert.ToSingle(probeLength8.Text);
                mwdNmdcLength += Convert.ToSingle(probeLength9.Text);
                mwdNmdcLength += Convert.ToSingle(probeLength10.Text);
                mwdNmdcLength += Convert.ToSingle(probeLength11.Text);
                mwdNmdcLength += Convert.ToSingle(probeLength12.Text);
                mwdNmdcLength += Convert.ToSingle(probeLength13.Text);
                mwdNmdcLength += Convert.ToSingle(probeLength14.Text);
                mwdNmdcLength += Convert.ToSingle(probeLength15.Text);
                mwdNmdcLength += Convert.ToSingle(probeLength16.Text);
                mwdNmdcLength += Convert.ToSingle(probeLength17.Text);
                mwdNmdcLength += Convert.ToSingle(probeLength18.Text);
                mwdNmdcLength += Convert.ToSingle(probeLength19.Text);
                mwdNmdcLength += Convert.ToSingle(probeLength20.Text);
            }
            catch { }

            calculateNmdcLengthBox.Text = String.Format("{0:0.00}", mwdNmdcLength);
            calculateNmdcLengthBox.BackColor = Color.LimeGreen;
        }

        private void calculateBhaLength()
        {
            try
            {
                bhaLengthBox.BackColor = Color.Empty;
                bhaLength = 0;
                bhaLength += Convert.ToSingle(collarLength1.Text);
                bhaLength += Convert.ToSingle(collarLength2.Text);
                bhaLength += Convert.ToSingle(collarLength3.Text);
                bhaLength += Convert.ToSingle(collarLength4.Text);
                bhaLength += Convert.ToSingle(collarLength5.Text);
                bhaLength += Convert.ToSingle(collarLength6.Text);
                bhaLength += Convert.ToSingle(collarLength7.Text);
                bhaLength += Convert.ToSingle(collarLength8.Text);
                bhaLength += Convert.ToSingle(collarLength9.Text);
                bhaLength += Convert.ToSingle(collarLength10.Text);
                bhaLength += Convert.ToSingle(collarLength11.Text);
                bhaLength += Convert.ToSingle(collarLength12.Text);
                bhaLength += Convert.ToSingle(collarLength13.Text);
                bhaLength += Convert.ToSingle(collarLength14.Text);
                bhaLength += Convert.ToSingle(collarLength15.Text);
                bhaLength += Convert.ToSingle(collarLength16.Text);
                bhaLength += Convert.ToSingle(collarLength17.Text);
                bhaLength += Convert.ToSingle(collarLength18.Text);
                bhaLength += Convert.ToSingle(collarLength19.Text);
                bhaLength += Convert.ToSingle(collarLength20.Text);
            }
            catch { }

            bhaLengthBox.Text = String.Format("{0:0.00}", bhaLength);
            bhaLengthBox.BackColor = Color.LightBlue;
        }

        private void mwdNmdcCalculateButton_Click(object sender, EventArgs e)
        {
            calculateMwdNmdcLength();
        }

        private void bhaCombo1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] aux = collarLengthTable.ElementAt(bhaCombo1.SelectedIndex).Split(',');
                collarUnits1.Items.Clear();
                collarUnits1.Items.Add("INCH");
                collarUnits1.SelectedIndex = 0;
                if (bhaCombo1.Text.Contains("NMDC"))
                {
                    collarLength1.Text = String.Format("{0:0.00}", mwdNmdcLength);
                    collarLength1.BackColor = Color.Yellow;
                }
                else
                {
                    collarLength1.Text = aux[2];
                }
                collarNotes1.Text = "";
                collarNotes1.Text = aux[3];
            }
            catch { }
        }

        private void bhaCombo2_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] aux = collarLengthTable.ElementAt(bhaCombo2.SelectedIndex).Split(',');
                collarUnits2.Items.Clear();
                collarUnits2.Items.Add("INCH");
                collarUnits2.SelectedIndex = 0;
                if (bhaCombo2.Text.Contains("NMDC"))
                {
                    collarLength2.Text = String.Format("{0:0.00}", mwdNmdcLength);
                    collarLength2.BackColor = Color.Yellow;
                }
                else
                {
                    collarLength2.Text = aux[2];
                }
                collarNotes2.Text = "";
                collarNotes2.Text = aux[3];
            }
            catch { }
        }

        private void bhaCombo3_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] aux = collarLengthTable.ElementAt(bhaCombo3.SelectedIndex).Split(',');
                collarUnits3.Items.Clear();
                collarUnits3.Items.Add("INCH");
                collarUnits3.SelectedIndex = 0;
                if (bhaCombo3.Text.Contains("NMDC"))
                {
                    collarLength3.Text = String.Format("{0:0.00}", mwdNmdcLength);
                    collarLength3.BackColor = Color.Yellow;
                }
                else
                {
                    collarLength3.Text = aux[2];
                }
                collarNotes3.Text = "";
                collarNotes3.Text = aux[3];
            }
            catch { }
        }

        private void bhaCombo4_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] aux = collarLengthTable.ElementAt(bhaCombo4.SelectedIndex).Split(',');
                collarUnits4.Items.Clear();
                collarUnits4.Items.Add("INCH");
                collarUnits4.SelectedIndex = 0;
                if (bhaCombo4.Text.Contains("NMDC"))
                {
                    collarLength4.Text = String.Format("{0:0.00}", mwdNmdcLength);
                    collarLength4.BackColor = Color.Yellow;
                }
                else
                {
                    collarLength4.Text = aux[2];
                }
                collarNotes4.Text = "";
                collarNotes4.Text = aux[3];
            }
            catch { }
        }

        private void bhaCombo5_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] aux = collarLengthTable.ElementAt(bhaCombo5.SelectedIndex).Split(',');
                collarUnits5.Items.Clear();
                collarUnits5.Items.Add("INCH");
                collarUnits5.SelectedIndex = 0;
                if (bhaCombo5.Text.Contains("NMDC"))
                {
                    collarLength5.Text = String.Format("{0:0.00}", mwdNmdcLength);
                    collarLength5.BackColor = Color.Yellow;
                }
                else
                {
                    collarLength5.Text = aux[2];
                }
                collarNotes5.Text = "";
                collarNotes5.Text = aux[3];
            }
            catch { }
        }

        private void bhaCombo6_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] aux = collarLengthTable.ElementAt(bhaCombo6.SelectedIndex).Split(',');
                collarUnits6.Items.Clear();
                collarUnits6.Items.Add("INCH");
                collarUnits6.SelectedIndex = 0;
                if (bhaCombo6.Text.Contains("NMDC"))
                {
                    collarLength6.Text = String.Format("{0:0.00}", mwdNmdcLength);
                    collarLength6.BackColor = Color.Yellow;
                }
                else
                {
                    collarLength6.Text = aux[2];
                }
                collarNotes6.Text = "";
                collarNotes6.Text = aux[3];
            }
            catch { }
        }

        private void bhaCombo7_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] aux = collarLengthTable.ElementAt(bhaCombo7.SelectedIndex).Split(',');
                collarUnits7.Items.Clear();
                collarUnits7.Items.Add("INCH");
                collarUnits7.SelectedIndex = 0;
                if (bhaCombo7.Text.Contains("NMDC"))
                {
                    collarLength7.Text = String.Format("{0:0.00}", mwdNmdcLength);
                    collarLength7.BackColor = Color.Yellow;
                }
                else
                {
                    collarLength7.Text = aux[2];
                }
                collarNotes7.Text = "";
                collarNotes7.Text = aux[3];
            }
            catch { }
        }

        private void bhaCombo8_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] aux = collarLengthTable.ElementAt(bhaCombo8.SelectedIndex).Split(',');
                collarUnits8.Items.Clear();
                collarUnits8.Items.Add("INCH");
                collarUnits8.SelectedIndex = 0;
                if (bhaCombo8.Text.Contains("NMDC"))
                {
                    collarLength8.Text = String.Format("{0:0.00}", mwdNmdcLength);
                    collarLength8.BackColor = Color.Yellow;
                }
                else
                {
                    collarLength8.Text = aux[2];
                }
                collarNotes8.Text = "";
                collarNotes8.Text = aux[3];
            }
            catch { }
        }

        private void bhaCombo9_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] aux = collarLengthTable.ElementAt(bhaCombo9.SelectedIndex).Split(',');
                collarUnits9.Items.Clear();
                collarUnits9.Items.Add("INCH");
                collarUnits9.SelectedIndex = 0;
                if (bhaCombo9.Text.Contains("NMDC"))
                {
                    collarLength9.Text = String.Format("{0:0.00}", mwdNmdcLength);
                    collarLength9.BackColor = Color.Yellow;
                }
                else
                {
                    collarLength9.Text = aux[2];
                }
                collarNotes9.Text = "";
                collarNotes9.Text = aux[3];
            }
            catch { }
        }

        private void bhaCombo10_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] aux = collarLengthTable.ElementAt(bhaCombo10.SelectedIndex).Split(',');
                collarUnits10.Items.Clear();
                collarUnits10.Items.Add("INCH");
                collarUnits10.SelectedIndex = 0;
                if (bhaCombo10.Text.Contains("NMDC"))
                {
                    collarLength10.Text = String.Format("{0:0.00}", mwdNmdcLength);
                    collarLength10.BackColor = Color.Yellow;
                }
                else
                {
                    collarLength10.Text = aux[2];
                }
                collarNotes10.Text = "";
                collarNotes10.Text = aux[3];
            }
            catch { }
        }

        private void bhaCombo11_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] aux = collarLengthTable.ElementAt(bhaCombo11.SelectedIndex).Split(',');
                collarUnits11.Items.Clear();
                collarUnits11.Items.Add("INCH");
                collarUnits11.SelectedIndex = 0;
                if (bhaCombo11.Text.Contains("NMDC"))
                {
                    collarLength11.Text = String.Format("{0:0.00}", mwdNmdcLength);
                    collarLength11.BackColor = Color.Yellow;
                }
                else
                {
                    collarLength11.Text = aux[2];
                }
                collarNotes11.Text = "";
                collarNotes11.Text = aux[3];
            }
            catch { }
        }

        private void bhaCombo12_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] aux = collarLengthTable.ElementAt(bhaCombo12.SelectedIndex).Split(',');
                collarUnits12.Items.Clear();
                collarUnits12.Items.Add("INCH");
                collarUnits12.SelectedIndex = 0;
                if (bhaCombo12.Text.Contains("NMDC"))
                {
                    collarLength12.Text = String.Format("{0:0.00}", mwdNmdcLength);
                    collarLength12.BackColor = Color.Yellow;
                }
                else
                {
                    collarLength12.Text = aux[2];
                }
                collarNotes12.Text = "";
                collarNotes12.Text = aux[3];
            }
            catch { }
        }

        private void bhaCombo13_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] aux = collarLengthTable.ElementAt(bhaCombo13.SelectedIndex).Split(',');
                collarUnits13.Items.Clear();
                collarUnits13.Items.Add("INCH");
                collarUnits13.SelectedIndex = 0;
                if (bhaCombo13.Text.Contains("NMDC"))
                {
                    collarLength13.Text = String.Format("{0:0.00}", mwdNmdcLength);
                    collarLength13.BackColor = Color.Yellow;
                }
                else
                {
                    collarLength13.Text = aux[2];
                }
                collarNotes13.Text = "";
                collarNotes13.Text = aux[3];
            }
            catch { }
        }

        private void bhaCombo14_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] aux = collarLengthTable.ElementAt(bhaCombo14.SelectedIndex).Split(',');
                collarUnits14.Items.Clear();
                collarUnits14.Items.Add("INCH");
                collarUnits14.SelectedIndex = 0;
                if (bhaCombo14.Text.Contains("NMDC"))
                {
                    collarLength14.Text = String.Format("{0:0.00}", mwdNmdcLength);
                    collarLength14.BackColor = Color.Yellow;
                }
                else
                {
                    collarLength14.Text = aux[2];
                }
                collarNotes14.Text = "";
                collarNotes14.Text = aux[3];
            }
            catch { }
        }

        private void bhaCombo15_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] aux = collarLengthTable.ElementAt(bhaCombo15.SelectedIndex).Split(',');
                collarUnits15.Items.Clear();
                collarUnits15.Items.Add("INCH");
                collarUnits15.SelectedIndex = 0;
                if (bhaCombo15.Text.Contains("NMDC"))
                {
                    collarLength15.Text = String.Format("{0:0.00}", mwdNmdcLength);
                    collarLength15.BackColor = Color.Yellow;
                }
                else
                {
                    collarLength15.Text = aux[2];
                }
                collarNotes15.Text = "";
                collarNotes15.Text = aux[3];
            }
            catch { }
        }

        private void bhaCombo16_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] aux = collarLengthTable.ElementAt(bhaCombo16.SelectedIndex).Split(',');
                collarUnits16.Items.Clear();
                collarUnits16.Items.Add("INCH");
                collarUnits16.SelectedIndex = 0;
                if (bhaCombo16.Text.Contains("NMDC"))
                {
                    collarLength16.Text = String.Format("{0:0.00}", mwdNmdcLength);
                    collarLength16.BackColor = Color.Yellow;
                }
                else
                {
                    collarLength16.Text = aux[2];
                }
                collarNotes16.Text = "";
                collarNotes16.Text = aux[3];
            }
            catch { }
        }

        private void bhaCombo17_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] aux = collarLengthTable.ElementAt(bhaCombo17.SelectedIndex).Split(',');
                collarUnits17.Items.Clear();
                collarUnits17.Items.Add("INCH");
                collarUnits17.SelectedIndex = 0;
                if (bhaCombo17.Text.Contains("NMDC"))
                {
                    collarLength17.Text = String.Format("{0:0.00}", mwdNmdcLength);
                    collarLength17.BackColor = Color.Yellow;
                }
                else
                {
                    collarLength17.Text = aux[2];
                }
                collarNotes17.Text = "";
                collarNotes17.Text = aux[3];
            }
            catch { }
        }

        private void bhaCombo18_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] aux = collarLengthTable.ElementAt(bhaCombo18.SelectedIndex).Split(',');
                collarUnits18.Items.Clear();
                collarUnits18.Items.Add("INCH");
                collarUnits18.SelectedIndex = 0;
                if (bhaCombo18.Text.Contains("NMDC"))
                {
                    collarLength18.Text = String.Format("{0:0.00}", mwdNmdcLength);
                    collarLength18.BackColor = Color.Yellow;
                }
                else
                {
                    collarLength18.Text = aux[2];
                }
                collarNotes18.Text = "";
                collarNotes18.Text = aux[3];
            }
            catch { }
        }

        private void bhaCombo19_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] aux = collarLengthTable.ElementAt(bhaCombo19.SelectedIndex).Split(',');
                collarUnits19.Items.Clear();
                collarUnits19.Items.Add("INCH");
                collarUnits19.SelectedIndex = 0;
                if (bhaCombo19.Text.Contains("NMDC"))
                {
                    collarLength19.Text = String.Format("{0:0.00}", mwdNmdcLength);
                    collarLength19.BackColor = Color.Yellow;
                }
                else
                {
                    collarLength19.Text = aux[2];
                }
                collarNotes19.Text = "";
                collarNotes19.Text = aux[3];
            }
            catch { }
        }

        private void bhaCombo20_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] aux = collarLengthTable.ElementAt(bhaCombo20.SelectedIndex).Split(',');
                collarUnits20.Items.Clear();
                collarUnits20.Items.Add("INCH");
                collarUnits20.SelectedIndex = 0;
                if (bhaCombo20.Text.Contains("NMDC"))
                {
                    collarLength20.Text = String.Format("{0:0.00}", mwdNmdcLength);
                    collarLength20.BackColor = Color.Yellow;
                }
                else
                {
                    collarLength20.Text = aux[2];
                }
                collarNotes20.Text = "";
                collarNotes20.Text = aux[3];
            }
            catch { }
        }

        private void bhaLengthCalculateButton_Click(object sender, EventArgs e)
        {
            calculateBhaLength();
        }

        private void toolSizeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            runDefaultBhaLengthBuilder();
        }

        private void reloadTablesButton_Click(object sender, EventArgs e)
        {
            collarLengthTable = new List<string>();
            probeLengthTable = new List<string>();

            mwdNmdcLength = 0;
            bhaLength = 0;

            CheckDefaultCollarLengthTable();
            CheckDefaultProbeLengthTable();
            initializeProbeBoxes();
            initializeCollarBoxes();
            runDefaultBhaLengthBuilder();
        }

        private async void takeSurveyButton_Click(object sender, EventArgs e)
        {
            clearBoxes();

            byte[] message = cp.buildMessage(Common.NODE_MASTER, Common.CMD_ACTION, Common.MWD_MASTER_ACTION_TAKE_SURVEY, 0x01);
            if (!myFtdiDevice.IsOpen) // Is our serial port open?
                return;
            if (await tc.FTDI_SEND(message, myFtdiDevice, ftStatus, this, localEcho)) { }
            else
            {
                Log(LogMsgType0.Outgoing, "TAKE SURVEY FAILED", 2);
                return; // message failed
            }
            //User Requesting DataGroup1 from Master
            if (MH.checkSumVerify(tc.rxNoEcho.ToArray()))
            {
                if (tc.rxNoEcho.ToArray()[5] == Common.ACK)
                {
                    Log(LogMsgType0.Outgoing, "TAKE SURVEY PASSED", 2);
                }
            }
        }

        private void configGammaButton_Click(object sender, EventArgs e)
        {
            gammaConfigForm gammaConfig = new gammaConfigForm(this);
            gammaConfig.StartPosition = FormStartPosition.CenterScreen;
            gammaConfig.ShowDialog();
        }

        private void rmsBoxCheckBox_CheckedChanged(object sender, EventArgs e)
        {

            if (rmsBoxCheckBox.Checked == true)
            {
                localEcho = false;
            }
            else
            {
                localEcho = true;
            }

        }

        private async void qbusCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (qbusCheckBox.Checked)
            {
                if (await tibCMD.qBusEnabled(this, tc, myFtdiDevice, ftStatus))
                    Log(LogMsgType0.Outgoing, "TOOL INTERFACE BOX CONNECTED, USING QBUS", 2);
                else
                    Log(LogMsgType0.Outgoing, "LEGACY COM DEVICE CONNECTED", 2);
            }
            else
            {
                if (await tibCMD.qBusDisabled(this, tc, myFtdiDevice, ftStatus))
                    Log(LogMsgType0.Outgoing, "TOOL INTERFACE BOX CONNECTED, USING RS485", 2);
                else
                    Log(LogMsgType0.Outgoing, "LEGACY COM DEVICE CONNECTED", 2);
            }
        }

        public void setCalibratorApi(float calibratorApi)
        {
            calibratorApiValue = calibratorApi;
        }

        private void StopwatchTimer_Tick(object sender, EventArgs e)
        {
            if (gammaAcqState != 0 && gammaAcqState < 4)
            {
                fiveMinutesInSeconds--;

                TimeSpan remainingTime = TimeSpan.FromSeconds(fiveMinutesInSeconds);

                timerTextBox.Text = remainingTime.ToString(@"mm\:ss");

                if (fiveMinutesInSeconds < 0)
                {
                    clearBoxes();
                    Log(LogMsgType0.Outgoing, String.Format("DID NOT ACQUIRE ENOUGH SAMPLES... CONTINUING ACQUISITION", mwdDG1.TimeStampSeconds), 2);
                }
            }
            else
            {
                timerTextBox.Text = "";
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            DateTime current = DateTime.Now;

            PrintDocument doc = new PrintDocument();
            doc.PrintPage += this.doc_PrintPage;

            PrintDialog dlg = new PrintDialog();
            dlg.PrinterSettings.PrintToFile = true;
            if (serialNumberBox.Text != null)
                dlg.PrinterSettings.PrinterName = current.ToString("mm/dd/yyyy hh:mm:ss") + "SN- " + serialNumberBox.Text;
            else
                dlg.PrinterSettings.PrinterName = current.ToString("mm/dd/yyyy hh:mm:ss") + "_SN- UNKNOWN";
            dlg.Document = doc;
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                doc.Print();
            }
        }

        private void doc_PrintPage(object sender, PrintPageEventArgs e)
        {
            float x = e.MarginBounds.Left;
            float y = e.MarginBounds.Top;
            Bitmap bmp = new Bitmap(gammaCalPanel.Width, gammaCalPanel.Height);
            gammaCalPanel.DrawToBitmap(bmp, new Rectangle(0, 0, gammaCalPanel.Width, gammaCalPanel.Height));
            e.Graphics.DrawImage((Image)bmp, x, y);
        }

        private void startCalFirst5MinButton_Click(object sender, EventArgs e)
        {
            clearBoxes();

            if (startCalFirst5MinButton.Text == "START")
            {
                startCalFirst5MinButton.Text = "CANCEL";

                clearBoxes();
                gammaTimer.Stop();
                gammaTimer.Start();
                fiveMinutesInSeconds = 300;

                gammaAcqCounter = 0;
                gammaAcqState = 1;

                Log(LogMsgType0.Outgoing, String.Format("BEGIN FIRST 5 MINUTES ACQUISTION WITH SOURCE", mwdDG1.TimeStampSeconds), 2);
                startCalFirst5MinButton.BackColor = Color.Yellow;
            }
            else
            {
                startCalFirst5MinButton.Text = "START";
                startCalFirst5MinButton.BackColor = Color.Empty;

                gammaTimer.Stop();

                fiveMinutesInSeconds = 0;

                gammaAcqCounter = -1;
                gammaAcqState = 0;

                Log(LogMsgType0.Outgoing, String.Format("CANCEL FIRST 5 MINUTES ACQUISITION WITH SOURCE", mwdDG1.TimeStampSeconds), 2);
            }
        }

        private void startBackGroundCpsButton_Click(object sender, EventArgs e)
        {
            clearBoxes();

            if (startBackGroundCpsButton.Text == "START")
            {
                startBackGroundCpsButton.Text = "CANCEL";

                clearBoxes();
                gammaTimer.Stop();
                gammaTimer.Start();
                fiveMinutesInSeconds = 300;

                gammaAcqCounter = 0;
                gammaAcqState = 2;

                Log(LogMsgType0.Outgoing, String.Format("BEGIN 5 MINUTES ACQUISTION BACKGROUND COUNTS", mwdDG1.TimeStampSeconds), 2);
                startBackGroundCpsButton.BackColor = Color.Yellow;
            }
            else
            {
                startBackGroundCpsButton.Text = "START";
                startBackGroundCpsButton.BackColor = Color.Empty;

                fiveMinutesInSeconds = 0;
                gammaTimer.Stop();

                gammaAcqCounter = -1;
                gammaAcqState = 0;

                Log(LogMsgType0.Outgoing, String.Format("CANCEL 5 MINUTES ACQUISTION BACKGROUND COUNTS", mwdDG1.TimeStampSeconds), 2);
            }
        }

        private void calibratorLastFiveMinButton_Click(object sender, EventArgs e)
        {
            clearBoxes();

            if (calibratorLastFiveMinButton.Text == "START")
            {
                calibratorLastFiveMinButton.Text = "CANCEL";

                clearBoxes();
                gammaTimer.Stop();
                gammaTimer.Start();
                fiveMinutesInSeconds = 300;

                gammaAcqCounter = 0;
                gammaAcqState = 3;

                Log(LogMsgType0.Outgoing, String.Format("BEGIN LAST 5 MINUTES ACQUISTION WITH SOURCE", mwdDG1.TimeStampSeconds), 2);
                calibratorLastFiveMinButton.BackColor = Color.Yellow;
            }
            else
            {
                calibratorLastFiveMinButton.Text = "START";
                calibratorLastFiveMinButton.BackColor = Color.Empty;

                fiveMinutesInSeconds = 0;
                gammaTimer.Stop();

                gammaAcqCounter = -1;
                gammaAcqState = 0;

                Log(LogMsgType0.Outgoing, String.Format("CANCEL LAST 5 MINUTES ACQUISTION WITH SOURCE", mwdDG1.TimeStampSeconds), 2);
            }
        }

        private async void clearGammaListButton_Click(object sender, EventArgs e)
        {
            gammaCpsList.Clear();

            #region Datagroup1 Read
            byte[] message = cp.buildMessage(Common.NODE_MASTER, Common.CMD_DATAGROUP_RDWR, Common.DATAGROUP_1);

            if (!myFtdiDevice.IsOpen)
            {
                Log(LogMsgType0.Outgoing, "Serial Port is Not Open!", 2);
                return;
            }

            if (await tc.FTDI_SEND(message, myFtdiDevice, ftStatus, this, localEcho))
            {
                //await Task.Delay(200);
            }
            else
            {
                return; // message failed
            }
            //if (MH.checkSumVerify(tc.rxNoEcho.ToArray(), Common.MESSAGE_OVERHEAD + Marshal.SizeOf(mwdDG1) + 1))
            if (MH.checkSumVerify(tc.rxNoEcho.ToArray()))
            {
                try
                {
                    mwdDG1 = conv.FromByteArray<StructureVariables.MWD_MASTER_DATAGROUP_1_STRUCTURE>(tc.rxData.ToArray());

                    updateAlerts();
                }
                catch
                {
                }
            }
            #endregion

            gammaCpsList.Add(mwdDG1.GammaCountsPerSecond);

            if (gammaCpsList.Count > 10)
                gammaCpsList.RemoveAt(0);
            if (gammaScalingFactor != -1)
                firstFiveMinGammaCpsBox.Text = String.Format("{0:0.00000}", gammaCpsList.Average() * gammaScalingFactor);
            else
                firstFiveMinGammaCpsBox.Text = String.Format("{0:0.00000}", gammaCpsList.Average());
        }

        private void applyGammFactorButton_Click(object sender, EventArgs e)
        {
            try
            {
                avgCalibratorCpsBox.Text = String.Format("{0:0.0000}", (Convert.ToDouble(firstFiveMinGammaCpsBox.Text) + Convert.ToDouble(calibratorLastFiveMinBox.Text)) / 2);

                netCalibratorCpsTextBox.Text = String.Format("{0:0.0000}",
                    ((Convert.ToDouble(firstFiveMinGammaCpsBox.Text) + (Convert.ToDouble(calibratorLastFiveMinBox.Text) + double.Parse("1E-20", CultureInfo.InvariantCulture))) / 2) -
                    (0.55 * Convert.ToDouble(backgroundCpsBox.Text))
                    );

                gammaScalingFactorBox.Text = String.Format("{0:0.000}", (calibratorApiValue / Convert.ToDouble(netCalibratorCpsTextBox.Text)) *
                    (160.49432 + (Convert.ToDouble(netCalibratorCpsTextBox.Text) * 208 / calibratorApiValue) * 0.38110906 +
                    (-0.00060470845 * Math.Pow((Convert.ToDouble(netCalibratorCpsTextBox.Text) * 208 / calibratorApiValue), 2))) / 208);
            }
            catch
            {

            }
        }

        private async void runButton_Click(object sender, EventArgs e)
        {
            clearBoxes();

            #region Set Time
            //Set Time
            var TimeStamp = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
            var TimeStampArray = new byte[6];

            var year = Convert.ToInt32(TimeStamp.Substring(0, 4)) - 2000;
            var month = Convert.ToInt32(TimeStamp.Substring(5, 2));
            var day = Convert.ToInt32(TimeStamp.Substring(8, 2));
            var hour = Convert.ToInt32(TimeStamp.Substring(11, 2));
            var minute = Convert.ToInt32(TimeStamp.Substring(14, 2));
            var second = Convert.ToInt32(TimeStamp.Substring(17, 2));

            List<byte> timestamp = new List<byte>();

            timestamp.Add(Convert.ToByte(year));
            timestamp.Add(Convert.ToByte(month));
            timestamp.Add(Convert.ToByte(day));
            timestamp.Add(Convert.ToByte(hour));
            timestamp.Add(Convert.ToByte(minute));
            timestamp.Add(Convert.ToByte(second));

            byte[] message = cp.buildMessage(Common.NODE_MASTER, Common.CMD_TIME_RDWR, timestamp);

            if (!myFtdiDevice.IsOpen)
            {
                Log(LogMsgType0.Outgoing, "Serial Port is Not Open!", 2);
                return;
            }

            if (await tc.FTDI_SEND(message, myFtdiDevice, ftStatus, this, localEcho))
            {
                await Task.Delay(50);
            }
            else
            {
                return; // message failed
            }

            conv = new deserializeStruct(); // Class for marshalling structures

            //User wants to "RUN" Tool
            if (MH.checkSumVerify(tc.rxNoEcho.ToArray()))
            {
                // User Requesting TO RUN Tool

                //Log(global::NeutrinoTracker.Log.Outgoing, "CheckSum PASS", 2);
                if (tc.rxNoEcho.ToArray()[5] == Common.ACK)
                {
                    Log(LogMsgType0.Outgoing, "SET CLOCK SUCCESS", 2);
                }
                else
                {
                    Log(LogMsgType0.Outgoing, "SET CLOCK FAILED", 2);
                }
            }
            #endregion

            #region Write MODE ACTIVE
            //BroadCast Mode Active
            message = cp.writeMode(Common.NODE_MASTER, Common.MODE_ACTIVE);

            if (!myFtdiDevice.IsOpen)
            {
                Log(LogMsgType0.Outgoing, "Serial Port is Not Open!", 2);
                return;
            }

            if (await tc.FTDI_SEND(message, myFtdiDevice, ftStatus, this, localEcho)) { }
            else
            {
                return; // message failed
            }

            if (MH.checkSumVerify(tc.rxNoEcho.ToArray()))
            {
                Log(LogMsgType0.Outgoing, "MODE ACTIVE: PASS", 2);
                Log(LogMsgType0.Outgoing, "RUN: SUCCESS", 2);
            }
            else
            {
                Log(LogMsgType0.Outgoing, "MODE ACTIVE: FAILED", 2);
                Log(LogMsgType0.Outgoing, "RUN: FAILED", 2);
            }
            #endregion
        }

        private async void gammaTimer_Tick(object sender, EventArgs e)
        {
            if (gammaAcqCounter >= 9)
            {
                gammaAcqState = 4;
                startCalFirst5MinButton.BackColor = Color.Empty;
                startBackGroundCpsButton.BackColor  = Color.Empty;
                calibratorLastFiveMinButton.BackColor = Color.Empty;

                startCalFirst5MinButton.Text = "START";
                startBackGroundCpsButton.Text = "START";
                calibratorLastFiveMinButton.Text = "START";

                clearBoxes();
                Log(LogMsgType0.Outgoing, String.Format("ACQUISITION COMPLETE", mwdDG1.TimeStampSeconds), 2);

                return;
            }
            if (gammaAcqCounter == -1)
                return; 
                

            if (digitalGammaCheckbox.Checked == false && woftGammaCheckBox.Checked == false)
            {
                #region MWD Datagroup1 Read
                byte[] message = cp.buildMessage(Common.NODE_MASTER, Common.CMD_DATAGROUP_RDWR, Common.DATAGROUP_1);

                if (!myFtdiDevice.IsOpen)
                {
                    return;
                }

                if (await tc.FTDI_SEND(message, myFtdiDevice, ftStatus, this, localEcho))
                {
                    //await Task.Delay(200);
                }
                else
                {
                    return; // message failed
                }
                if (MH.checkSumVerify(tc.rxNoEcho.ToArray(), Common.MESSAGE_OVERHEAD + Marshal.SizeOf(mwdDG1) + 1))
                {
                    try
                    {
                        mwdDG1 = conv.FromByteArray<StructureVariables.MWD_MASTER_DATAGROUP_1_STRUCTURE>(tc.rxData.ToArray());

                        updateAlerts();
                    }
                    catch
                    {
                    }
                }
                #endregion
            }
            else if(woftGammaCheckBox.Checked)
            {
                #region UVT Datagroup2 Read
                byte[] message = cp.buildMessage(Common.NODE_UVT, Common.CMD_DATAGROUP_RDWR, Common.DATAGROUP_2);

                if (!myFtdiDevice.IsOpen)
                {
                    return;
                }

                if (await tc.FTDI_SEND(message, myFtdiDevice, ftStatus, this, localEcho))
                {
                    //await Task.Delay(200);
                }
                else
                {
                    return; // message failed
                }
                if (MH.checkSumVerify(tc.rxNoEcho.ToArray(), Common.MESSAGE_OVERHEAD + Marshal.SizeOf(uvtDG2) + 1))
                {
                    try
                    {
                        uvtDG2 = conv.FromByteArray<StructureVariables.UVT_DATAGROUP_2_STRUCTURE>(tc.rxData.ToArray());

                        updateAlerts();
                    }
                    catch
                    {
                    }
                }
                #endregion
            }
            else
            {
                #region Digital GAMMA Datagroup1 Read
                byte[] message = cp.buildMessage(Common.NODE_DIGITAL_GAMMA, Common.CMD_DATAGROUP_RDWR, Common.DATAGROUP_1);

                if (!myFtdiDevice.IsOpen)
                {
                    return;
                }

                if (await tc.FTDI_SEND(message, myFtdiDevice, ftStatus, this, localEcho))
                {
                    //await Task.Delay(200);
                }
                else
                {
                    return; // message failed
                }
                if (MH.checkSumVerify(tc.rxNoEcho.ToArray(), Common.MESSAGE_OVERHEAD + Marshal.SizeOf(gammaDG1) + 1))
                {
                    try
                    {
                        gammaDG1 = conv.FromByteArray<StructureVariables.GAMMA_DATAGROUP_1_STRUCTURE>(tc.rxData.ToArray());

                        updateAlerts();
                    }
                    catch
                    {
                    }
                }
                #endregion
            }

            switch (gammaAcqState)
            {
                case 1:
                    {
                        if (digitalGammaCheckbox.Checked == false && woftGammaCheckBox.Checked == false)
                        {
                            gammaFirstFiveMinutesCpsList.Add(mwdDG1.GammaCountsPerSecond);
                        }
                        else if (woftGammaCheckBox.Checked)
                        {
                            gammaFirstFiveMinutesCpsList.Add(uvtDG2.GammaCountsPerSecond);
                        }
                        else
                        {
                            gammaFirstFiveMinutesCpsList.Add(gammaDG1.GammaCountsPerSecond);
                        }

                        if (gammaFirstFiveMinutesCpsList.Count > 10)
                            gammaAcqState = 4;

                        firstFiveMinGammaCpsBox.Text = String.Format("{0:0.00000}", gammaFirstFiveMinutesCpsList.Average());

                        gammaAcqCounter++;
                    }
                    break;
                case 2:
                    {
                        if (digitalGammaCheckbox.Checked == false && woftGammaCheckBox.Checked == false)
                        {
                            gammaBackgroundCpsList.Add(mwdDG1.GammaCountsPerSecond);
                        }
                        else if (woftGammaCheckBox.Checked)
                        {
                            gammaBackgroundCpsList.Add(uvtDG2.GammaCountsPerSecond);
                        }
                        else
                        {
                            gammaBackgroundCpsList.Add(gammaDG1.GammaCountsPerSecond);
                        }

                        if (gammaBackgroundCpsList.Count > 10)
                            gammaAcqState = 4;

                        backgroundCpsBox.Text = String.Format("{0:0.00000}", gammaBackgroundCpsList.Average());

                        gammaAcqCounter++;
                    }
                    break;
                case 3:
                    {
                        if (digitalGammaCheckbox.Checked == false && woftGammaCheckBox.Checked == false)
                        {
                            gammaLastFiveMinutesCpsList.Add(mwdDG1.GammaCountsPerSecond);
                        }
                        else if (woftGammaCheckBox.Checked)
                        {
                            gammaLastFiveMinutesCpsList.Add(uvtDG2.GammaCountsPerSecond);
                        }
                        else
                        {
                            gammaLastFiveMinutesCpsList.Add(gammaDG1.GammaCountsPerSecond);
                        }

                        if (gammaLastFiveMinutesCpsList.Count > 10)
                            gammaAcqState = 4;

                        calibratorLastFiveMinBox.Text = String.Format("{0:0.00000}", gammaLastFiveMinutesCpsList.Average());

                        gammaAcqCounter++;
                    }
                    break;
            }
        }

        private async void readPowerButton_Click(object sender, EventArgs e)
        {
            clearBoxes();

            #region Read Power
            byte[] message = cp.buildMessage(Common.NODE_TIC, Common.CMD_DATAGROUP_RDWR, Common.DATAGROUP_1);
            StructureVariables.TIC_DATAGROUP1_STRUCTURE ticDG1 = new StructureVariables.TIC_DATAGROUP1_STRUCTURE();

            if (!myFtdiDevice.IsOpen)
                return;

            if (await tc.FTDI_SEND(message, myFtdiDevice, ftStatus, this, localEcho)) { }
            else
            {
                Log(LogMsgType0.Outgoing, "READ POWER: ERROR", 2);
                Log(LogMsgType0.Outgoing, "CONNECT: FAIL", 2);
            }
            //if (MH.checkSumVerify(tc.rxNoEcho.ToArray(), Common.MESSAGE_OVERHEAD + Marshal.SizeOf(ticDG1) + 1))
                if (MH.checkSumVerify(tc.rxNoEcho.ToArray()))
                {
                try
                {
                    ticDG1 = conv.FromByteArray<StructureVariables.TIC_DATAGROUP1_STRUCTURE>(tc.rxData.ToArray());// Save the incoming Datagroup 1 data via marshalling
                    Log(LogMsgType0.Outgoing, "SYSTEM VOLTAGE: " + String.Format("{0:0.00}", ticDG1.NormalVoltageVolts)
                        + " V\nSYSTEM CURRENT: " + String.Format("{0:0.00}", (float)ticDG1.NormalCurrentMilliAmps) + " mA", 2);
                }
                catch
                {
                    Log(LogMsgType0.Outgoing, "READ POWER: FAIL", 2);
                    Log(LogMsgType0.Outgoing, "CONNECT: FAIL", 2);
                }
            }
            #endregion
        }

        private async void connectButton_Click(object sender, EventArgs e)
        {
            try
            {
                clearBoxes();

                #region Write MODE IDLE
                //BroadCast Mode Active
                byte[] message = cp.writeMode(Common.NODE_MASTER, Common.MODE_IDLE);

                if (!myFtdiDevice.IsOpen)
                {
                    Log(LogMsgType0.Outgoing, "Serial Port is Not Open!", 2);
                    return;
                }

                if (await tc.FTDI_SEND(message, myFtdiDevice, ftStatus, this, localEcho)) { }
                else
                {
                    return; // message failed
                }
                #endregion

                if (MH.checkSumVerify(tc.rxNoEcho.ToArray()))
                {
                    Log(LogMsgType0.Outgoing, "MODE IDLE", 2);

                    #region Datagroup1 Read
                    message = cp.buildMessage(Common.NODE_MASTER, Common.CMD_DATAGROUP_RDWR, Common.DATAGROUP_1);

                    if (!myFtdiDevice.IsOpen)
                    {
                        Log(LogMsgType0.Outgoing, "Serial Port is Not Open!", 2);
                        return;
                    }

                    if (await tc.FTDI_SEND(message, myFtdiDevice, ftStatus, this, localEcho))
                    {
                        //await Task.Delay(200);
                    }
                    else
                    {
                        return; // message failed
                    }
                    if (MH.checkSumVerify(tc.rxNoEcho.ToArray(), Common.MESSAGE_OVERHEAD + Marshal.SizeOf(mwdDG1) + 1))
                    {
                        try
                        {
                            mwdDG1 = conv.FromByteArray<StructureVariables.MWD_MASTER_DATAGROUP_1_STRUCTURE>(tc.rxData.ToArray());

                            updateAlerts();

                        }
                        catch
                        {
                        }
                    }
                    #endregion

                    #region Set Time
                    //Set Time
                    DateTime temp = DateTime.Now;
                    var TimeStamp = temp.ToString("yyyy/MM/dd HH:mm:ss");
                    var TimeStampArray = new byte[6];

                    var year = Convert.ToInt32(TimeStamp.Substring(0, 4)) - 2000;
                    var month = Convert.ToInt32(TimeStamp.Substring(5, 2));
                    var day = Convert.ToInt32(TimeStamp.Substring(8, 2));
                    var hour = Convert.ToInt32(TimeStamp.Substring(11, 2));
                    var minute = Convert.ToInt32(TimeStamp.Substring(14, 2));
                    var second = Convert.ToInt32(TimeStamp.Substring(17, 2));

                    List<byte> timestamp = new List<byte>();

                    timestamp.Add(Convert.ToByte(year));
                    timestamp.Add(Convert.ToByte(month));
                    timestamp.Add(Convert.ToByte(day));
                    timestamp.Add(Convert.ToByte(hour));
                    timestamp.Add(Convert.ToByte(minute));
                    timestamp.Add(Convert.ToByte(second));

                    message = cp.buildMessage(Common.NODE_MASTER, Common.CMD_TIME_RDWR, timestamp);

                    if (!myFtdiDevice.IsOpen)
                    {
                        Log(LogMsgType0.Outgoing, "Serial Port is Not Open!", 2);
                        return;
                    }

                    if (await tc.FTDI_SEND(message, myFtdiDevice, ftStatus, this, localEcho)) { }
                    else
                    {
                        return; // message failed
                    }

                    conv = new deserializeStruct(); // Class for marshalling structures

                    //User wants to "RUN" Tool
                    if (MH.checkSumVerify(tc.rxNoEcho.ToArray()))
                    {
                        // User Requesting TO RUN Tool

                        //Log(global::NeutrinoTracker.Log.Outgoing, "CheckSum PASS", 2);
                        if (tc.rxNoEcho.ToArray()[5] == Common.ACK)
                        {
                            Log(LogMsgType0.Outgoing, "SET CLOCK SUCCESS", 2);
                        }
                    }
                    #endregion

                    #region Read Serial Number
                    message = cp.buildMessage(Common.NODE_MASTER, Common.CMD_SERIALNUMBER_RDWR);

                    if (!myFtdiDevice.IsOpen) // Is our serial port open?
                        return;

                    if (await tc.FTDI_SEND(message, myFtdiDevice, ftStatus, this, localEcho)) { }
                    else
                    {
                        Log(LogMsgType0.Outgoing, "READ SERIAL NUMBER: FAIL", 2);
                    }
                    if (MH.checkSumVerify(tc.rxNoEcho.ToArray(), Common.MESSAGE_OVERHEAD + Marshal.SizeOf(sns)))
                    {
                        try
                        {
                            if (localEcho)
                                sns = conv.FromByteArray<StructureVariables.SERIAL_NUMBER_STRUCTURE>(tc.rxData.ToArray());
                            else
                                sns = conv.FromByteArray<StructureVariables.SERIAL_NUMBER_STRUCTURE>(tc.rxNoEcho.Skip(5).ToArray());
                            Log(LogMsgType0.Outgoing, "READ SERIAL NUMBER: PASS", 2);
                        }
                        catch
                        {
                            Log(LogMsgType0.Outgoing, "READ SERIAL NUMBER: FAIL", 2);
                        }
                    }
                    #endregion

                    #region Read Firmware ID
                    message = cp.buildMessage(Common.NODE_MASTER, Common.CMD_FIRMWARE_ID_RD);

                    if (!myFtdiDevice.IsOpen)  // Is our serial port open?
                        return;
                    if (await tc.FTDI_SEND(message, myFtdiDevice, ftStatus, this, localEcho)) { }
                    else
                    {
                        Log(LogMsgType0.Outgoing, "READ FIRMWARE ID: FAIL", 2);
                    }
                    if (MH.checkSumVerify(tc.rxNoEcho.ToArray(), Common.MESSAGE_OVERHEAD + Marshal.SizeOf(fID)))
                    {
                        try
                        {
                            if (localEcho)
                                fID = conv.FromByteArray<StructureVariables.FIRMWARE_ID_STRUCTURE>(tc.rxData.ToArray());
                            else
                                fID = conv.FromByteArray<StructureVariables.FIRMWARE_ID_STRUCTURE>(tc.rxNoEcho.Skip(5).ToArray());
                            Log(LogMsgType0.Outgoing, "READ FIRMWARE ID: PASS", 2);
                        }
                        catch
                        {
                            Log(LogMsgType0.Outgoing, "READ FIRMWARE ID: FAIL", 2);
                        }
                    }
                    #endregion

                    #region Read Configuration
                    message = cp.buildMessage(Common.NODE_MASTER, Common.CMD_NONVOLATILE_RDWR, Common.MWD_MASTER_NONVOLATILE_CONFIGURATION);

                    if (!myFtdiDevice.IsOpen) // Is our serial port open?
                        return;

                    if (await tc.FTDI_SEND(message, myFtdiDevice, ftStatus, this, localEcho)) { }
                    else
                    {
                        Log(LogMsgType0.Outgoing, "READ CONFIGURATION: FAIL", 2);
                    }
                    if (MH.checkSumVerify(tc.rxNoEcho.ToArray(), Common.MESSAGE_OVERHEAD + Marshal.SizeOf(mwdConfig) + 1))
                    {
                        try
                        {
                            mwdConfig = conv.FromByteArray<StructureVariables.MWD_MASTER_CONFIGURATION_STRUCTURE>(tc.rxData.ToArray());
                            Log(LogMsgType0.Outgoing, "READ CONFIGURATION: PASS", 2);
                        }
                        catch
                        {
                            Log(LogMsgType0.Outgoing, "READ CONFIGURATION: FAIL", 2);
                        }
                    }
                    #endregion

                    #region Read System Record
                    message = cp.buildMessage(Common.NODE_MASTER, Common.CMD_SYSTEM_RDWR);

                    if (!myFtdiDevice.IsOpen) // Is our serial port open?
                        return;

                    if (await tc.FTDI_SEND(message, myFtdiDevice, ftStatus, this, localEcho)) { }
                    else
                    {
                        Log(LogMsgType0.Outgoing, "READ SYSTEM: FAIL", 2);
                        return; // message failed
                    }

                    //User Requesting System Record Information of Master
                    if (MH.checkSumVerify(tc.rxNoEcho.ToArray(), Common.MESSAGE_OVERHEAD + Marshal.SizeOf(sV.SystemRecordStructure) + 1))
                    {
                        try
                        {
                            if (localEcho)
                                sV.SystemRecordStructure = conv.FromByteArray<StructureVariables.SYSTEM_RECORD_STRUCTURE>
                                (tc.rxData.ToArray()); //deserialize buffer into structure
                            else
                                sV.SystemRecordStructure = conv.FromByteArray<StructureVariables.SYSTEM_RECORD_STRUCTURE>
                                (tc.rxNoEcho.Skip(5).ToArray()); //deserialize buffer into structure
                            Log(LogMsgType0.Outgoing, "READ SYSTEM: PASS", 2);
                        }
                        catch
                        {
                            Log(LogMsgType0.Outgoing, "MFPWR READ SYSTEM: FAIL", 2);
                        }
                    }
                    #endregion

                    #region Read Power
                    message = cp.buildMessage(Common.NODE_TIC, Common.CMD_DATAGROUP_RDWR, Common.DATAGROUP_1);

                    if (!myFtdiDevice.IsOpen)
                        return;

                    if (await tc.FTDI_SEND(message, myFtdiDevice, ftStatus, this, localEcho)) { }
                    else
                    {
                        Log(LogMsgType0.Outgoing, "MFPWR READ POWER: ERROR", 2);
                    }
                    if (MH.checkSumVerify(tc.rxNoEcho.ToArray()))
                    {
                        try
                        {
                            StructureVariables.TIC_DATAGROUP1_STRUCTURE ticDG1 = new StructureVariables.TIC_DATAGROUP1_STRUCTURE();
                            ticDG1 = conv.FromByteArray<StructureVariables.TIC_DATAGROUP1_STRUCTURE>(tc.rxData.ToArray());// Save the incoming Datagroup 1 data via marshalling
                            Log(LogMsgType0.Outgoing, "SYSTEM VOLTAGE: " + String.Format("{0:0.00}", ticDG1.NormalVoltageVolts)
                                + " V\nSYSTEM CURRENT: " + String.Format("{0:0.00}", (float)ticDG1.NormalCurrentMilliAmps) + " mA", 2);
                        }
                        catch
                        {
                            Log(LogMsgType0.Outgoing, "MFPWR READ POWER: FAIL", 2);
                            Log(LogMsgType0.Outgoing, "CONNECT: FAIL", 2);
                            return;
                        }
                    }
                    #endregion

                    Log(LogMsgType0.Outgoing, "CONNECT: PASS", 2);
                    char result;
                    var results = new char[16];
                    var i = 0;
                    foreach (var value in sns.StringModule)
                        try
                        {
                            result = Convert.ToChar(value);
                            results[i++] = result;
                        }
                        catch (OverflowException ex)
                        {
                            ErrorDisplay ED = new ErrorDisplay();
                            ED.StartPosition = FormStartPosition.CenterParent;
                            ED.setMessage(ex.Message);
                            ED.ShowDialog(this);
                        }

                    var s = new string(results);

                    Log(LogMsgType0.Outgoing, "\nSERIAL NUMBER: " + s, 2);
                    Log(LogMsgType0.Outgoing, "\nFIRMWARE ID: " + fID.STRING[0] + "." + fID.STRING[1].ToString("D2"), 2);
                    //Log(LogMsgType0.Outgoing, "MFPWR DAPA FIRMWARE ID: " + mfpwrDG16.STRING[0] + "." + mfpwrDG16.STRING[1].ToString("D2"), 2);
                    //Log(LogMsgType0.Outgoing, "MFPWR DAPB FIRMWARE ID: " + mfpwrDG17.STRING[0] + "." + mfpwrDG17.STRING[1].ToString("D2"), 2);

                    var MemoryDataPages = (long)(sV.SystemRecordStructure.MemoryPageWritePointer - 32);

                    Log(LogMsgType0.Outgoing, "MEMORY DATA: " + (int)(MemoryDataPages * sV.SystemRecordStructure.MemoryPageSize / 1024.0) + "KB", 2);

                    //Log(LogMsgType0.Outgoing, "\n\nSYSTEM CLOCK: " + mwdDG1.TimeStampDate[(int)Common.DATETIME.MONTH].ToString("D2") + "/" + mwdDG1.TimeStampDate[(int)Common.DATETIME.DAY].ToString("D2") + "/" + (mwdDG1.TimeStampDate[(int)Common.DATETIME.YEAR] + 2000) + " " +
                    //                   mwdDG1.TimeStampDate[(int)Common.DATETIME.HOUR].ToString("D2") + ":" + mwdDG1.TimeStampDate[(int)Common.DATETIME.MINUTE].ToString("D2") + ":" + mwdDG1.TimeStampDate[(int)Common.DATETIME.SECOND].ToString("D2"), 2);

                    Log(LogMsgType0.Outgoing, "PC SET CLOCK: " + temp.ToString("MM/dd/yyyy HH:mm:ss"), 2);

                    updateAlerts();
                }
            }
            catch
            {

            }
        }

        private async void readDataButton_Click(object sender, EventArgs e)
        {
            clearBoxes();

            #region Datagroup1 Read
            byte[] message = cp.buildMessage(Common.NODE_MASTER, Common.CMD_DATAGROUP_RDWR, Common.DATAGROUP_1);

            if (!myFtdiDevice.IsOpen)
            {
                Log(LogMsgType0.Outgoing, "Serial Port is Not Open!", 2);
                return;
            }

            if (await tc.FTDI_SEND(message, myFtdiDevice, ftStatus, this, localEcho))
            {
                //await Task.Delay(200);
            }
            else
            {
                return; // message failed
            }
            if (MH.checkSumVerify(tc.rxNoEcho.ToArray(), Common.MESSAGE_OVERHEAD + Marshal.SizeOf(mwdDG1) + 1))
            {
                try
                {
                    mwdDG1 = conv.FromByteArray<StructureVariables.MWD_MASTER_DATAGROUP_1_STRUCTURE>(tc.rxData.ToArray());

                    updateAlerts();

                    Log(LogMsgType0.Outgoing, "\nDatagroup 1\nDateTime = " + mwdDG1.TimeStampDate[(int)Common.DATETIME.MONTH].ToString("D2") + "/" +
                                                                mwdDG1.TimeStampDate[(int)Common.DATETIME.DAY].ToString("D2") + "/" +
                                                                (mwdDG1.TimeStampDate[(int)Common.DATETIME.YEAR] + 2000) + " " +
                                                                mwdDG1.TimeStampDate[(int)Common.DATETIME.HOUR].ToString("D2") +
                                                                ":" + mwdDG1.TimeStampDate[(int)Common.DATETIME.MINUTE].ToString("D2") + ":" +
                                                                mwdDG1.TimeStampDate[(int)Common.DATETIME.SECOND].ToString("D2"), 2);

                    switch (mwdDG1.Mode)
                    {
                        case Common.MODE_STARTUP:
                            Log(LogMsgType0.Outgoing, "Mode = STARTUP", 2);
                            break;
                        case Common.MODE_IDLE:
                            Log(LogMsgType0.Outgoing, "Mode = IDLE", 2);
                            break;
                        case Common.MODE_TRIP:
                            Log(LogMsgType0.Outgoing, "Mode = MODE_TRIP", 2);
                            break;
                        case Common.MODE_ACTIVE:
                            Log(LogMsgType0.Outgoing, "Mode = ACTIVE", 2);
                            break;
                        case Common.MODE_DOWNLOAD:
                            Log(LogMsgType0.Outgoing, "Mode = DOWNLOAD", 2);
                            break;
                        case Common.MODE_CALIBRATION:
                            Log(LogMsgType0.Outgoing, "Mode = CALIBRATION", 2);
                            break;
                        default:
                            Log(LogMsgType0.Outgoing, "Mode = UNKNOWN", 2);
                            break;
                    }

                    Log(LogMsgType0.Outgoing, String.Format("TimeStampSeconds: {0:0}", mwdDG1.TimeStampSeconds), 2);
                    Log(LogMsgType0.Outgoing, String.Format("MilliSecondCounter: {0:0}", mwdDG1.MilliSecondCounter), 2);
                    Log(LogMsgType0.Outgoing, String.Format("StateMachineState: {0:0}", mwdDG1.StateMachineState), 2);
                    Log(LogMsgType0.Outgoing, String.Format("StateTimerMilliSeconds: {0:0}", mwdDG1.StateTimerMilliSeconds), 2);
                    Log(LogMsgType0.Outgoing, String.Format("FlagBat2On: {0:0}", mwdDG1.FlagBat2On), 2);
                    Log(LogMsgType0.Outgoing, String.Format("ToolfaceOffsetDegrees: {0:0.0000}", mwdDG1.ToolfaceOffsetDegrees), 2);
                    Log(LogMsgType0.Outgoing, String.Format("DownlinkState: {0:0}", mwdDG1.DownlinkState), 2);
                    Log(LogMsgType0.Outgoing, String.Format("DownlinkCommand: {0:0}", mwdDG1.DownlinkCommand), 2);
                    Log(LogMsgType0.Outgoing, String.Format("MptMode: {0:0}", mwdDG1.MptMode), 2);
                    Log(LogMsgType0.Outgoing, String.Format("PulseWidthIndex: {0:0}", mwdDG1.PulseWidthIndex), 2);
                    Log(LogMsgType0.Outgoing, String.Format("PulseWidthSeconds: {0:0.0000}", mwdDG1.PulseWidthSeconds), 2);
                    Log(LogMsgType0.Outgoing, String.Format("PulseWidthRatio: {0:0.0000}", mwdDG1.PulseWidthRatio), 2);
                    Log(LogMsgType0.Outgoing, String.Format("Tnp_mult: {0:0.0000}", mwdDG1.Tnp_mult), 2);
                    Log(LogMsgType0.Outgoing, String.Format("Td_mult: {0:0.0000}", mwdDG1.Td_mult), 2);
                    Log(LogMsgType0.Outgoing, String.Format("PulserDelayOnSeconds: {0:0.0000}", mwdDG1.PulserDelayOnSeconds), 2);
                    Log(LogMsgType0.Outgoing, String.Format("PulserDelayOffSeconds: {0:0.0000}", mwdDG1.PulserDelayOffSeconds), 2);
                    Log(LogMsgType0.Outgoing, String.Format("PulseWidthAdjustedSeconds: {0:0.0000}", mwdDG1.PulseWidthAdjustedSeconds), 2);
                    Log(LogMsgType0.Outgoing, String.Format("PulseWidthAdjustedSeconds: {0:0.0000}", mwdDG1.PulseWidthAdjustedSeconds), 2);
                    Log(LogMsgType0.Outgoing, String.Format("VBusVolts: {0:0.00}", mwdDG1.VBusVolts), 2);
                    Log(LogMsgType0.Outgoing, String.Format("TemperatureCelsius: {0:0.00}C", mwdDG1.TemperatureCelsius), 2);
                    Log(LogMsgType0.Outgoing, String.Format("VibxMilliVolts: {0:0.00}", mwdDG1.VibxMilliVolts), 2);
                    Log(LogMsgType0.Outgoing, String.Format("VibyMilliVolts: {0:0.00}", mwdDG1.VibyMilliVolts), 2);
                    Log(LogMsgType0.Outgoing, String.Format("VibzMilliVolts: {0:0.00}", mwdDG1.VibzMilliVolts), 2);
                    Log(LogMsgType0.Outgoing, String.Format("FlowState: {0:0}", mwdDG1.FlowState), 2);
                    Log(LogMsgType0.Outgoing, String.Format("CirculatingHours: {0:0}", mwdDG1.CirculatingHours), 2);
                    Log(LogMsgType0.Outgoing, String.Format("TripDelayTimer: {0:0}", mwdDG1.TripDelayTimer), 2);
                    Log(LogMsgType0.Outgoing, String.Format("SurveyDelayTimerSeconds: {0:0}", mwdDG1.SurveyDelayTimerSeconds), 2);
                    Log(LogMsgType0.Outgoing, String.Format("MptTransmitDelayTimerSeconds: {0:0}", mwdDG1.MptTransmitDelayTimerSeconds), 2);
                    Log(LogMsgType0.Outgoing, String.Format("MptTransmitDelayTimerSeconds: {0:0}", mwdDG1.DirectionalSensorType), 2);
                    Log(LogMsgType0.Outgoing, String.Format("\nInclinationDegrees: {0:0.0000}", mwdDG1.InclinationDegrees), 2);
                    Log(LogMsgType0.Outgoing, String.Format("CircInclinationDegrees: {0:0.0000}", mwdDG1.CircInclinationDegrees), 2);
                    Log(LogMsgType0.Outgoing, String.Format("AzimuthDegrees: {0:0.0000}", mwdDG1.AzimuthDegrees), 2);
                    Log(LogMsgType0.Outgoing, String.Format("MagneticDipDegrees: {0:0.0000}", mwdDG1.MagneticDipDegrees), 2);
                    Log(LogMsgType0.Outgoing, String.Format("TotalMagneticFieldGauss: {0:0.0000}", mwdDG1.TotalMagneticFieldGauss), 2);
                    Log(LogMsgType0.Outgoing, String.Format("TotalGravityFieldG: {0:0.0000}", mwdDG1.TotalGravityFieldG), 2);
                    Log(LogMsgType0.Outgoing, String.Format("MagneticToolFaceDegrees: {0:0.0000}", mwdDG1.MagneticToolFaceDegrees), 2);
                    Log(LogMsgType0.Outgoing, String.Format("GravityToolFaceDegrees: {0:0.0000}", mwdDG1.GravityToolFaceDegrees), 2);
                    Log(LogMsgType0.Outgoing, String.Format("\nGammaCountsPerSecond: {0:0.0000}", mwdDG1.GammaCountsPerSecond), 2);
                    Log(LogMsgType0.Outgoing, String.Format("\nGxG: {0:0.0000}", mwdDG1.GxG), 2);
                    Log(LogMsgType0.Outgoing, String.Format("GyG: {0:0.0000}", mwdDG1.GyG), 2);
                    Log(LogMsgType0.Outgoing, String.Format("GzG: {0:0.0000}", mwdDG1.GzG), 2);
                    Log(LogMsgType0.Outgoing, String.Format("BxGauss: {0:0.0000}", mwdDG1.BxGauss), 2);
                    Log(LogMsgType0.Outgoing, String.Format("ByGauss: {0:0.0000}", mwdDG1.ByGauss), 2);
                    Log(LogMsgType0.Outgoing, String.Format("BzGauss: {0:0.0000}", mwdDG1.BzGauss), 2);

                    Log(LogMsgType0.Outgoing, String.Format("\nTestVariables1: {0:0.0000}", mwdDG1.TestVariable1), 2);
                    Log(LogMsgType0.Outgoing, String.Format("TestVariables2: {0:0.0000}", mwdDG1.TestVariable2), 2);
                    Log(LogMsgType0.Outgoing, String.Format("TestVariables3: {0:0.0000}", mwdDG1.TestVariable3), 2);
                    Log(LogMsgType0.Outgoing, String.Format("TestVariables4: {0:0.0000}", mwdDG1.TestVariable4), 2);

                    if (mwdDG1.InclinationDegrees > 5)
                        newToolFace = mwdDG1.GravityToolFaceDegrees;
                    else
                        newToolFace = mwdDG1.MagneticToolFaceDegrees;

                    timerTextBox.Text = String.Format("{0:0.0000}", newToolFace);
                }
                catch
                {
                }
            }
            #endregion
        }

        public async Task<StructureVariables.MWD_MASTER_DATAGROUP_1_STRUCTURE> readDataGroup1()
        {
            clearBoxes();

            #region Datagroup1 Read
            byte[] message = cp.buildMessage(Common.NODE_MASTER, Common.CMD_DATAGROUP_RDWR, Common.DATAGROUP_1);

            if (!myFtdiDevice.IsOpen)
            {
                Log(LogMsgType0.Outgoing, "Serial Port is Not Open!", 2);
                return mwdDG1;
            }

            if (await tc.FTDI_SEND(message, myFtdiDevice, ftStatus, this, localEcho))
            {
                //await Task.Delay(200);
            }
            else
            {
                return mwdDG1; // message failed
            }
            //if (MH.checkSumVerify(tc.rxNoEcho.ToArray(), Common.MESSAGE_OVERHEAD + Marshal.SizeOf(mwdDG1) + 1))
            if (MH.checkSumVerify(tc.rxNoEcho.ToArray()))
            {
                try
                {
                    mwdDG1 = conv.FromByteArray<StructureVariables.MWD_MASTER_DATAGROUP_1_STRUCTURE>(tc.rxData.ToArray());

                    updateAlerts();

                    if (mwdDG1.InclinationDegrees > 5)
                        newToolFace = mwdDG1.GravityToolFaceDegrees;
                    else
                        newToolFace = mwdDG1.MagneticToolFaceDegrees;

                    timerTextBox.Text = String.Format("{0:0.0000}", newToolFace);

                    return mwdDG1;
                }
                catch
                {
                    return mwdDG1;
                }
            }

            return mwdDG1;
            #endregion
        }

        private void comRefreshTimer_Tick(object sender, EventArgs e)
        {
            // checks to see if COM ports have been added or removed
            // since it is quite common now with USB-to-Serial adapters
            RefreshComPortList();
        }

        public void updateAlerts()
        {
            int result;

            if (woftGammaCheckBox.Checked)
            {
                //Change Status Bits
                mwdBit0.Text = "ACTIVE";
                mwdBit1.Text = "FLOW_ON";
                mwdBit2.Text = "VBUSX_ON";
                mwdBit3.Text = "APS_SYNC";
                mwdBit4.Text = "APS_INIT_ERR";
                mwdBit5.Text = "MEM_SCAN";
                mwdBit6.Text = "MEM_ERASE";
                mwdBit7.Text = "MEM_FULL";
                mwdBit8.Text = "TIME_NOINIT";
                mwdBit9.Text = "ROLLTEST";
                mwdBit10.Text = "BIT10";
                mwdBit11.Text = "JOB_RECORD_ERR";
                mwdBit12.Text = "SERIALNUMBER_ERR";
                mwdBit13.Text = "BIT13";
                mwdBit14.Text = "CONFIG";
                mwdBit15.Text = "SYSTEM";

                //Set Status Bit
                result = (int)uvtDG2.NodeStatus & 0x0001;
                //           if (((int)sV.MwdDataGroup1.NodeStatus & 0x0001) == 0x0001) labelBit1.BackColor = Color.LimeGreen;
                if (((int)uvtDG2.NodeStatus & 0x0001) == 0x0001) mwdBit0.BackColor = Color.LimeGreen;
                else mwdBit0.BackColor = Color.LightGray;
                if (((int)uvtDG2.NodeStatus & 0x0002) == 0x0002) mwdBit1.BackColor = Color.LimeGreen;
                else mwdBit1.BackColor = Color.LightGray;
                if (((int)uvtDG2.NodeStatus & 0x0004) == 0x0004) mwdBit2.BackColor = Color.LimeGreen;
                else mwdBit2.BackColor = Color.LightGray;
                if (((int)uvtDG2.NodeStatus & 0x0008) == 0x0008) mwdBit3.BackColor = Color.Yellow;
                else mwdBit3.BackColor = Color.LightGray;
                if (((int)uvtDG2.NodeStatus & 0x0010) == 0x0010) mwdBit4.BackColor = Color.Yellow;
                else mwdBit4.BackColor = Color.LightGray;
                if (((int)uvtDG2.NodeStatus & 0x0020) == 0x0020) mwdBit5.BackColor = Color.LimeGreen;
                else mwdBit5.BackColor = Color.LightGray;
                if (((int)uvtDG2.NodeStatus & 0x0040) == 0x0040) mwdBit6.BackColor = Color.LimeGreen;
                else mwdBit6.BackColor = Color.LightGray;
                if (((int)uvtDG2.NodeStatus & 0x0080) == 0x0080) mwdBit7.BackColor = Color.LimeGreen;
                else mwdBit7.BackColor = Color.LightGray;
                if (((int)uvtDG2.NodeStatus & 0x0100) == 0x0100) mwdBit8.BackColor = Color.Yellow;
                else mwdBit8.BackColor = Color.LimeGreen;
                if (((int)uvtDG2.NodeStatus & 0x0200) == 0x0200) mwdBit9.BackColor = Color.Yellow;
                else mwdBit9.BackColor = Color.LightGray;
                if (((int)uvtDG2.NodeStatus & 0x0400) == 0x0400) mwdBit10.BackColor = Color.Red;
                else mwdBit10.BackColor = Color.LightGray;
                if (((int)uvtDG2.NodeStatus & 0x0800) == 0x0800) mwdBit11.BackColor = Color.Red;
                else mwdBit11.BackColor = Color.LightGray;
                if (((int)uvtDG2.NodeStatus & 0x1000) == 0x1000) mwdBit12.BackColor = Color.Red;
                else mwdBit12.BackColor = Color.LightGray;
                if (((int)uvtDG2.NodeStatus & 0x2000) == 0x2000) mwdBit13.BackColor = Color.Red;
                else mwdBit13.BackColor = Color.LightGray;
                if (((int)uvtDG2.NodeStatus & 0x4000) == 0x4000) mwdBit14.BackColor = Color.Red;
                else mwdBit14.BackColor = Color.LightGray;
                if (((int)uvtDG2.NodeStatus & 0x8000) == 0x8000) mwdBit15.BackColor = Color.Red;
                else mwdBit15.BackColor = Color.LightGray;
            }
            else
            {
                //Change Status Bits
                mwdBit0.Text = "FLOW_ON";
                mwdBit1.Text = "BATTERY";
                mwdBit2.Text = "INVERT_FLOW";
                mwdBit3.Text = "ACTIVE";
                mwdBit4.Text = "MONITOR_ON";
                mwdBit5.Text = "MEM_SCAN";
                mwdBit6.Text = "MEM_ERASE";
                mwdBit7.Text = "MEM_FULL";
                mwdBit8.Text = "TIME_NOINIT";
                mwdBit9.Text = "MPT_TX";
                mwdBit10.Text = "TFO_ERR";
                mwdBit11.Text = "TELEMETRY_ERR";
                mwdBit12.Text = "SERIALNUMBER_ERR";
                mwdBit13.Text = "CAL_ERR";
                mwdBit14.Text = "CONFIG_ERR";
                mwdBit15.Text = "SYSTEMRECORD_ERR";

                //Set Status Bit
                result = (int)mwdDG1.NodeStatus & 0x0001;
                //           if (((int)sV.MwdDataGroup1.NodeStatus & 0x0001) == 0x0001) labelBit1.BackColor = Color.LimeGreen;
                if (((int)mwdDG1.NodeStatus & 0x0001) == 0x0001) mwdBit0.BackColor = Color.LimeGreen;
                else mwdBit0.BackColor = Color.LightGray;
                if (((int)mwdDG1.NodeStatus & 0x0002) == 0x0002) mwdBit1.BackColor = Color.LimeGreen;
                else mwdBit1.BackColor = Color.LightGray;
                if (((int)mwdDG1.NodeStatus & 0x0004) == 0x0004) mwdBit2.BackColor = Color.LimeGreen;
                else mwdBit2.BackColor = Color.LightGray;
                if (((int)mwdDG1.NodeStatus & 0x0008) == 0x0008) mwdBit3.BackColor = Color.Yellow;
                else mwdBit3.BackColor = Color.LimeGreen;
                if (((int)mwdDG1.NodeStatus & 0x0010) == 0x0010) mwdBit4.BackColor = Color.Yellow;
                else mwdBit4.BackColor = Color.LightGray;
                if (((int)mwdDG1.NodeStatus & 0x0020) == 0x0020) mwdBit5.BackColor = Color.LimeGreen;
                else mwdBit5.BackColor = Color.LightGray;
                if (((int)mwdDG1.NodeStatus & 0x0040) == 0x0040) mwdBit6.BackColor = Color.LimeGreen;
                else mwdBit6.BackColor = Color.LightGray;
                if (((int)mwdDG1.NodeStatus & 0x0080) == 0x0080) mwdBit7.BackColor = Color.LimeGreen;
                else mwdBit7.BackColor = Color.LightGray;
                if (((int)mwdDG1.NodeStatus & 0x0100) == 0x0100) mwdBit8.BackColor = Color.Yellow;
                else mwdBit8.BackColor = Color.LimeGreen;
                if (((int)mwdDG1.NodeStatus & 0x0200) == 0x0200) mwdBit9.BackColor = Color.Yellow;
                else mwdBit9.BackColor = Color.LightGray;
                if (((int)mwdDG1.NodeStatus & 0x0400) == 0x0400) mwdBit10.BackColor = Color.Red;
                else mwdBit10.BackColor = Color.LightGray;
                if (((int)mwdDG1.NodeStatus & 0x0800) == 0x0800) mwdBit11.BackColor = Color.Red;
                else mwdBit11.BackColor = Color.LightGray;
                if (((int)mwdDG1.NodeStatus & 0x1000) == 0x1000) mwdBit12.BackColor = Color.Red;
                else mwdBit12.BackColor = Color.LightGray;
                if (((int)mwdDG1.NodeStatus & 0x2000) == 0x2000) mwdBit13.BackColor = Color.Red;
                else mwdBit13.BackColor = Color.LightGray;
                if (((int)mwdDG1.NodeStatus & 0x4000) == 0x4000) mwdBit14.BackColor = Color.Red;
                else mwdBit14.BackColor = Color.LightGray;
                if (((int)mwdDG1.NodeStatus & 0x8000) == 0x8000) mwdBit15.BackColor = Color.Red;
                else mwdBit15.BackColor = Color.LightGray;
            }
            Application.DoEvents();
        }

        private void RefreshComPortList()
        {
            // Determine if the list of com port names has changed since last checked
            var selected = RefreshComPortList(comport.Items.Cast<string>(), comport.SelectedItem as string,
                myFtdiDevice.IsOpen);

            comport.Items.Clear();
            comport.Items.AddRange(OrderedPortNames());

            // If there was an update, then update the control showing the user the list of port names
            if (!string.IsNullOrEmpty(selected))
            {
                comport.Items.Clear();
                comport.Items.AddRange(OrderedPortNames());
                comport.SelectedItem = selected;
            }
            // A change in comport occured, if it was disconnected or switched, close the port and prompt user
            if ((selected != comConnected) && (comConnected != ""))
            {
                clearBoxes();
                tabControl1.SelectedTab = mainTab;
                alt_open_btn.Text = "OPEN";
                disableButons();
                comConnected = "";

                comport.Items.Clear();
                comport.ResetText();
                comport.Enabled = true;

                if (myFtdiDevice.IsOpen)
                {
                    myFtdiDevice.Close();
                    FTDI_OPEN_FLAG = 0;
                }
                Log(LogMsgType0.Normal, "Comport Error. Please Reconnect!\r", 2);

            }
        }

        private string RefreshComPortList(IEnumerable<string> PreviousPortNames, string CurrentSelection, bool PortOpen)
        {
            // Create a new return report to populate
            string selected = null;

            // Retrieve the list of ports currently mounted by the operating system (sorted by name)
            var ports = SerialPort.GetPortNames();

            // First determine if there was a change (any additions or removals)
            //bool updated = PreviousPortNames.Except(ports).Count() > 0 || ports.Except(PreviousPortNames).Count() > 0;

            // If there was a change, then select an appropriate default port
            if (!string.IsNullOrEmpty(ports.ToString()))
            {
                // Use the correctly ordered set of port names
                ports = OrderedPortNames();

                // Find newest port if one or more were added
                var newest = SerialPort.GetPortNames().Except(PreviousPortNames).OrderBy(a => a).LastOrDefault();

                // If the port was already open... (see logic notes and reasoning in Notes.txt)
                if (PortOpen)
                {
                    if (ports.Contains(CurrentSelection)) selected = CurrentSelection;
                    else if (!string.IsNullOrEmpty(newest)) selected = newest;
                    else selected = ports.LastOrDefault();
                }
                else
                {
                    if (!string.IsNullOrEmpty(newest)) selected = newest;
                    else if (ports.Contains(CurrentSelection)) selected = CurrentSelection;
                    else selected = ports.LastOrDefault();
                }
            }

            // If there was a change to the port list, return the recommended default selection
            return selected;
        }

        private string[] OrderedPortNames()
        {
            // Just a placeholder for a successful parsing of a string to an integer
            var num = 0;

            // Order the serial port names in numeric order (if possible)
            return SerialPort.GetPortNames()
                .OrderBy(a => a.Length > 3 && int.TryParse(a.Substring(3), out num) ? num : 0).ToArray();
        }

        public bool getLocalEchoStatus()
        {
            return localEcho;
        }

        private async void alt_open_btn_Click(object sender, EventArgs e)
        {
            clearBoxes();

            if (FTDI_OPEN_FLAG == 0)
            {
                int retry = 0;

                while (!myFtdiDevice.IsOpen)
                {
                    openFtdiComs();
                    if (retry < 3)
                        retry++;
                    else
                    {
                        Log(LogMsgType0.Outgoing, "COM PORT ERROR", 2);
                        return;
                    }
                }

                retry = 0;

                comport.Enabled = false;
                FTDI_OPEN_FLAG = 1;

                clearBoxes();

                if (!rmsBoxCheckBox.Checked)
                {

                    Log(LogMsgType0.Outgoing, "SCANNING COMS...", 2);

                    while (retry < 4)
                    {
                        switch (retry)
                        {
                            case 0:
                                myFtdiDevice.SetBaudRate(9600);
                                break;
                            case 1:
                                myFtdiDevice.SetBaudRate(115200);
                                break;
                            case 2:
                                myFtdiDevice.SetBaudRate(307200);
                                break;
                            case 3:
                                myFtdiDevice.SetBaudRate(333333);
                                break;
                            default:
                                myFtdiDevice.SetBaudRate(9600);
                                break;
                        }

                        retry++;

                        await Task.Delay(100);

                        if (qbusCheckBox.Checked)
                        {
                            if (await tibCMD.setBaud(this, 9600, tc, myFtdiDevice, ftStatus))
                            {
                                if (await tibCMD.qBusEnabled(this, tc, myFtdiDevice, ftStatus))
                                    Log(LogMsgType0.Outgoing, "TOOL INTERFACE BOX CONNECTED, USING QBUS", 2);
                                else
                                    Log(LogMsgType0.Outgoing, "LEGACY COM DEVICE CONNECTED", 2);

                                alt_open_btn.Text = "CLOSE";
                                myFtdiDevice.GetCOMPort(out comConnected);
                                setPortForwarding();
                                return;
                            }
                        }
                        else
                        {
                            if (await tibCMD.setBaud(this, 9600, tc, myFtdiDevice, ftStatus))
                            {
                                if (await tibCMD.qBusDisabled(this, tc, myFtdiDevice, ftStatus))
                                    Log(LogMsgType0.Outgoing, "TOOL INTERFACE BOX CONNECTED, USING RS485", 2);
                                else
                                    Log(LogMsgType0.Outgoing, "LEGACY COM DEVICE CONNECTED", 2);
                                alt_open_btn.Text = "CLOSE";
                                myFtdiDevice.GetCOMPort(out comConnected);
                                setPortForwarding();
                                return;
                            }
                        }
                    }
                    clearBoxes();
                }
                Log(LogMsgType0.Outgoing, "TOOL INTERFACE BOX NOT FOUND, USING LEGACY COMS", 2);
                alt_open_btn.Text = "CLOSE";
                setPortForwarding();
                return;
            }
            else if (FTDI_OPEN_FLAG == 1)
            {
                myFtdiDevice.GetCOMPort(out comstr);

                if (myFtdiDevice.IsOpen)
                {
                    ftStatus = myFtdiDevice.Close();

                    if (ftStatus != FTD2XX_NET.FTDI.FT_STATUS.FT_OK)
                    {
                        dataBox.AppendText("Failed to Close device (error " + ftStatus.ToString() + ")" + "\n");
                        return;
                    }
                    dataBox.AppendText("Close FTDI Device Found On " + comstr + "\n");
                    alt_open_btn.Text = "OPEN";
                }

                comport.Enabled = true;
                FTDI_OPEN_FLAG = 0;

                clearBoxes();
                alt_open_btn.Text = "OPEN";
                disableButons();
                //tabControl1.TabPages.Remove(pddControllerTab);
                //tabControl1.TabPages.Remove(mfpwrControllerTab);
                return;
            }
        }

        private async void setPortForwarding()
        {
            byte[] message = cp.buildMessage(Common.NODE_TIC, Common.CMD_ACTION, Common.TIC_ACTION_SET_PORT_FORWARDING, 0x01);
            if (!myFtdiDevice.IsOpen) // Is our serial port open?
                return;
            if (await tc.FTDI_SEND(message, myFtdiDevice, ftStatus, this, localEcho)) { }
            else
            {
                return; // message failed
            }
            //User Requesting DataGroup1 from Master
            if (MH.checkSumVerify(tc.rxNoEcho.ToArray()))
            {
                if (tc.rxNoEcho.ToArray()[5] == Common.ACK)
                {
                    Log(LogMsgType0.Outgoing, "TIC SET PORT FOWARDING MODE ON SUCCESS", 2);
                }
            }
        }

        private void disableButons()
        {
          
        }

        public void Log(LogMsgType0 msgtype, string msg, int Box)
        {
            switch (Box)
            {
                case 1:
                    {
                        ReceivingBox.Invoke(new EventHandler(delegate
                        {
                            ReceivingBox.SelectedText = string.Empty;
                            ReceivingBox.SelectionFont = new Font(ReceivingBox.SelectionFont, FontStyle.Bold);
                            ReceivingBox.SelectionColor = Color.Yellow;
                            ReceivingBox.AppendText(msg + "\n");
                            ReceivingBox.ScrollToCaret();
                        }));
                        break;
                    }
                case 2:
                    {
                        ReceivingBox.Invoke(new EventHandler(delegate
                        {
                            dataBox.SelectedText = string.Empty;
                            dataBox.SelectionFont = new Font(ReceivingBox.SelectionFont, FontStyle.Bold);
                            dataBox.SelectionColor = Color.Yellow;
                            dataBox.AppendText(msg + "\n");
                            dataBox.ScrollToCaret();
                        }));
                        break;
                    }
                case 3:
                    {
                        ReceivingBox.Invoke(new EventHandler(delegate
                        {
                            ReceivingBox.Text = string.Format(msg + "\n");
                            ReceivingBox.ScrollToCaret();
                        }));
                        break;
                    }
                case 4:
                    {
                        ReceivingBox.Invoke(new EventHandler(delegate
                        {
                            dataBox.Text = string.Format(msg + "\n");
                            dataBox.ScrollToCaret();
                        }));
                        break;
                    }
            }
            Application.DoEvents();
        }

        public void clearBoxes()
        {
            dataBox.Clear();
            ReceivingBox.Clear();
        }

        private bool openFtdiComs()
        {
            int counter = 0;

            GetVirtuCommPort();

            ftStatus = myFtdiDevice.OpenByIndex(Convert.ToUInt32(counter));

            myFtdiDevice.GetCOMPort(out comstr);

            while (comstr != comport.Text)
            {
                if (counter > ftdiDeviceCount)
                    break;
                else
                {
                    myFtdiDevice.Close();
                    ftStatus = myFtdiDevice.OpenByIndex(Convert.ToUInt32(counter));
                    myFtdiDevice.GetCOMPort(out comstr);
                }

                counter++;
            }

            if (ftStatus != FTDI.FT_STATUS.FT_OK)
            {
                return false;
            }

            Log(LogMsgType0.Outgoing, "Open FTDI Device Found On " + comstr, 2);
            //FTDI_COM_BOX.SelectedIndex = 0;

            // Set up device data parameters
            // Set Baud rate to 9600
            ftStatus = myFtdiDevice.SetBaudRate(Common.BAUD_APPLICATION);
            if (ftStatus != FTDI.FT_STATUS.FT_OK)
            {
                // Wait for a key press
                Log(LogMsgType0.Outgoing, "Failed to set Baud rate (error " + ftStatus.ToString() + ")", 2);
                return false;
            }

            // Set data characteristics - Data bits, Stop bits, Parity
            ftStatus = myFtdiDevice.SetDataCharacteristics(FTDI.FT_DATA_BITS.FT_BITS_8, FTDI.FT_STOP_BITS.FT_STOP_BITS_1, FTDI.FT_PARITY.FT_PARITY_NONE);
            if (ftStatus != FTDI.FT_STATUS.FT_OK)
            {
                Log(LogMsgType0.Outgoing, "Failed to set data characteristics (error " + ftStatus.ToString() + ")", 2);
                return false;
            }

            ////Set flow control - set RTS / CTS flow control
            //ftStatus = myFtdiDevice.SetFlowControl(FTDI.FT_FLOW_CONTROL.FT_FLOW_RTS_CTS, 0x11, 0x13);
            //if (ftStatus != FTDI.FT_STATUS.FT_OK)
            //{

            //    FTDI_Text_Box.AppendText("Failed to set flow control (error " + ftStatus.ToString() + ")");
            //    return;
            //}

            //Set read timeout to 50 mS, write timeout to infinite
            ftStatus = myFtdiDevice.SetTimeouts(50, 0);
            if (ftStatus != FTDI.FT_STATUS.FT_OK)
            {
                //Wait for a key press
                Log(LogMsgType0.Outgoing, "Failed to set timeouts (error " + ftStatus.ToString() + ")", 2);
                return false;
            }

            //Set read Latency 1 mS, write timeout to infinite
            ftStatus = myFtdiDevice.SetLatency(Convert.ToByte(1));
            if (ftStatus != FTDI.FT_STATUS.FT_OK)
            {
                //Wait for a key press
                Log(LogMsgType0.Outgoing, "Failed to set Latency (error " + ftStatus.ToString() + ")", 2);
                return false;
            }
            return true;
        }

        private void GetVirtuCommPort()
        {
            ftdiDeviceCount = 0;

            List<string> ports = new List<string>();

            myFtdiDevice.ResetPort();

            // Determine the number of FTDI devices connected to the machine
            ftStatus = myFtdiDevice.GetNumberOfDevices(ref ftdiDeviceCount);

            if (ftdiDeviceCount == 0)
            {
                //alt_open_btn.Text = "Search for FTDI";
                dataBox.AppendText("No FTDI device found!" + "\n");
                return;
            }

            // Allocate storage for device info list
            ftdiDeviceList = new FTD2XX_NET.FTDI.FT_DEVICE_INFO_NODE[ftdiDeviceCount];

            // Populate our device list
            ftStatus = myFtdiDevice.GetDeviceList(ftdiDeviceList);
        }

        private void motorBypassPercentBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                motorBypass = Convert.ToDouble(motorBypassPercentBox.Text) / 100.0;
            }
            catch
            {

            }
        }

        private void orificeToolSizeBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateChart();
        }

        #region RSS Calculator Variables
        double PPG = 1, SG = 8.33, KG_M3 = 0.0083, LB_FT3 = 7.48, G_CM3 = 8.33, RSS_GPM = 1, LPS = 15.85037248, LPM = 0.26417, motorBypass = 1;
        #endregion

        #region resize Form
        float firstWidth;
        float firstHeight;
        #endregion

        public MainForm()
        {
            InitializeComponent();
            if (ApplicationDeployment.IsNetworkDeployed)
            {
                this.Text = string.Format("PC WOLVERINE FIELD APPS - v{0}",
                    ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString(4));
            }

            //chart.EnableZoomAndPanControls();

            docToPrint.PrintPage += new PrintPageEventHandler(printPDF);

            //firstWidth = this.Size.Width;
            //firstHeight = this.Size.Height;

            #region Battery Calculator Init
            InitializeResistivityConsumptionTable();
            InitializePowerConsumptionTable();
            InitializeToolTable();
            InitializeTD05Table();
            InitializeTD067Table();
            InitializeTD625Table();
            InitializePulserSettings();
            InitializePulserConsumptionTable();
            initializeTnpTdTable();
            checkToolString();
            RunConsumptionCalcuation();
            #endregion

            #region Orifice Calculator Init
            InitializePoppetTubestopOpenClosed();
            IntializePulseAmplitudePsiTable();
            InitializePulseAmplitudeAtmTable();
            updateChart();
            #endregion

            #region RSS Calculator Init
            processRSSCalculator();
            #endregion

            toolTable.Visible = false;
            orificeAdminGroupBox.Visible = false;

            //Rectangle resolutionRect = System.Windows.Forms.Screen.FromControl(this).Bounds;
            //if (this.Width >= resolutionRect.Width || this.Height >= resolutionRect.Height)
            //{
            //    this.WindowState = FormWindowState.Maximized;
            //}

            //this.FormBorderStyle = FormBorderStyle.Sizable;
            //this.MaximizeBox = false;
            //this.MinimizeBox = false;

            //mainTabControl.TabPages.Remove(orificeCalcTab);

            this.AutoScrollMinSize = new System.Drawing.Size(1555, 1000);

            checkForConfig();

            CheckDefaultCollarLengthTable();

            CheckDefaultProbeLengthTable();

            initializeProbeBoxes();

            initializeCollarBoxes();

            runDefaultBhaLengthBuilder();

            //loadConfigButton.Enabled = false;

            //orificeFlowRateTextbox.Enabled = false;

            //tabControl1.TabPages.Remove(novDmCalTab);
        }

        public void checkForConfig()
        {
            string targetDirectory = @"C:\Wolverine\PC WOLVERINE FIELD APPS\";

            try //Check to see if the directory exists, if not create it
            {
                if (!Directory.Exists(targetDirectory))
                {
                    // Try to create the directory.
                    var di = Directory.CreateDirectory(targetDirectory);
                }
            }
            catch (IOException ioex)
            {
                Console.WriteLine(ioex.Message);
            }

            string[] files = Directory.GetFiles(targetDirectory, "CONFIG PC WOLVERINE FIELD APPS.csv", SearchOption.AllDirectories);

            Stream myStream;
            try
            {
                if (files.Count() > 0)
                {
                    using (myStream = File.Open(files[0], FileMode.Open))
                    {
                        // read the file Line by Line
                        var sr = new StreamReader(myStream);
                        string aux;
                        aux = sr.ReadLine();
                        string[] pieces = aux.Split(',');
                        calibratorApiValue = Convert.ToDouble(pieces[1]);
                        apiCalibratorLabel.Text = "CALIBRATOR API VALUE: " + calibratorApiValue.ToString() + " API";
                    }
                }
                else
                {
                    calibratorApiValue = 224;
                    saveConfig();
                }
            }
            catch { }
        }

        public void saveConfig()
        {
            string directory = @"C:\Wolverine\PC WOLVERINE FIELD APPS\";
            string fileName = directory + "CONFIG PC WOLVERINE FIELD APPS.csv";
            if (calibratorApiValue != null)
                using (var sw = new StreamWriter(fileName))
                {
                    sw.WriteLine(
                        "API CALIBRATOR VALUE: ," + calibratorApiValue.ToString() 
                        );
                }

            dataBox.Text = "CALIBRATOR API VALUE UPDATED SUCCESSFULLY";
        }

        public void CheckDefaultCollarLengthTable()
        {
            string targetDirectory = @"C:\Wolverine\PC WOLVERINE FIELD APPS\";

            try //Check to see if the directory exists, if not create it
            {
                if (!Directory.Exists(targetDirectory))
                {
                    // Try to create the directory.
                    var di = Directory.CreateDirectory(targetDirectory);
                }
            }
            catch (IOException ioex)
            {
                Console.WriteLine(ioex.Message);
            }

            string[] files = Directory.GetFiles(targetDirectory, "COLLAR LENGTH PC WOLVERINE FIELD APPS.csv", SearchOption.AllDirectories);

            Stream myStream;
            try
            {
                if (files.Count() > 0)
                {
                    using (myStream = File.Open(files[0], FileMode.Open))
                    {
                        // read the file Line by Line
                        var sr = new StreamReader(myStream);
                        string aux;
                        aux = sr.ReadLine(); // Header
                        string test = "";
                        while ((aux = sr.ReadLine()) != null)
                        {
                            test = aux.Replace(",\"", ",");
                            test = test.Replace("\",", ",");
                            test = test.Replace("\"\"", "\"");
                            test = test.Replace("\"\" ", "\"");
                            collarLengthTable.Add(test);
                        }
                    }
                }
                else
                {
                    GenerateDefaultCollarLengthTable();
                }
            }
            catch { }
        }

        public void GenerateDefaultCollarLengthTable()
        {
            string directory = @"C:\Wolverine\PC WOLVERINE FIELD APPS\";
            string fileName = directory + "COLLAR LENGTH PC WOLVERINE FIELD APPS.csv";
            int counter = 1;
            string collarLengthDefaultString = "";

            collarLengthDefaultString += counter.ToString() + ",8\" HOS (Pulser Sub),50.5\n";
            counter++;
            collarLengthDefaultString += counter.ToString() + ",6 3/4\" HOS (Pulser Sub),50.5\n";
            counter++;
            collarLengthDefaultString += counter.ToString() + ",4 3/4\" HOS (Pulser Sub),50.5\n";
            counter++;
            collarLengthDefaultString += counter.ToString() + ",8\" MWD NMDC,\n";
            counter++;
            collarLengthDefaultString += counter.ToString() + ",6 3/4\" MWD NMDC,\n";
            counter++;
            collarLengthDefaultString += counter.ToString() + ",4 3/4\" MWD NMDC,\n";
            counter++;
            collarLengthDefaultString += counter.ToString() + ",8\" MFPWR Top Sub,21.8\n";
            counter++;
            collarLengthDefaultString += counter.ToString() + ",6 3/4\" MFPWR Top Sub,21.8\n";
            counter++;
            collarLengthDefaultString += counter.ToString() + ",4 3/4\" MFPWR Top Sub,21.8\n";
            counter++;
            collarLengthDefaultString += counter.ToString() + ",8\" MFPWR Tool,144\n";
            counter++;
            collarLengthDefaultString += counter.ToString() + ",6 3/4\" MFPWR Tool,144.9\n";
            counter++;
            collarLengthDefaultString += counter.ToString() + ",4 3/4\" MFPWR Tool,144.9\n";
            counter++;
            collarLengthDefaultString += counter.ToString() + ",8\" MFPWR Bottom Sub (to fit PWD),12.4\n";
            counter++;
            collarLengthDefaultString += counter.ToString() + ",6 3/4\" MFPWR Bottom Sub (to fit PWD),12.4\n";
            counter++;
            collarLengthDefaultString += counter.ToString() + ",4 3/4\" MFPWR Bottom Sub (to fit PWD),15.9\n";
            counter++;
            collarLengthDefaultString += counter.ToString() + ",8\" MFPWR Bottom Sub,21.8\n";
            counter++;
            collarLengthDefaultString += counter.ToString() + ",6 3/4\" MFPWR Bottom Sub,21.8\n";
            counter++;
            collarLengthDefaultString += counter.ToString() + ",4 3/4\" MFPWR Bottom Sub,21.8\n";
            counter++;
            collarLengthDefaultString += counter.ToString() + ",8\" PWD Collar,37.5\n";
            counter++;
            collarLengthDefaultString += counter.ToString() + ",6 3/4\" PWD Collar,37.5\n";
            counter++;
            collarLengthDefaultString += counter.ToString() + ",4 3/4\" PWD Collar,34\n";
            counter++;
            collarLengthDefaultString += counter.ToString() + ",8\" RSS (3rd Party),\n";
            counter++;
            collarLengthDefaultString += counter.ToString() + ",6 3/4\" RSS (3rd Party),\n";
            counter++;
            collarLengthDefaultString += counter.ToString() + ",4 3/4\" RSS (3rd Party),\n";
            counter++;
            collarLengthDefaultString += counter.ToString() + ",Sub,\n";
            counter++;
            collarLengthDefaultString += counter.ToString() + ",Stabilizer,\n";
            counter++;

            using (var sw = new StreamWriter(fileName))
            {
                sw.WriteLine("Item #, Collars, Length[INCH], Notes\n" +
                    collarLengthDefaultString
                    );
            }

            Console.WriteLine("COLLAR LENGTH TABLE LOADED SUCCESSFULLY");

            CheckDefaultCollarLengthTable();
        }

        public void CheckDefaultProbeLengthTable()
        {
            string targetDirectory = @"C:\Wolverine\PC WOLVERINE FIELD APPS\";

            try //Check to see if the directory exists, if not create it
            {
                if (!Directory.Exists(targetDirectory))
                {
                    // Try to create the directory.
                    var di = Directory.CreateDirectory(targetDirectory);
                }
            }
            catch (IOException ioex)
            {
                Console.WriteLine(ioex.Message);
            }

            string[] files = Directory.GetFiles(targetDirectory, "PROBE LENGTH PC WOLVERINE FIELD APPS.csv", SearchOption.AllDirectories);

            Stream myStream;
            try
            {
                if (files.Count() > 0)
                {
                    using (myStream = File.Open(files[0], FileMode.Open))
                    {
                        // read the file Line by Line
                        // read the file Line by Line
                        var sr = new StreamReader(myStream);
                        string aux;
                        aux = sr.ReadLine(); // Header
                        string test = "";
                        while ((aux = sr.ReadLine()) != null)
                        {
                            test = aux.Replace(",\"", ",");
                            test = test.Replace("\",", ",");
                            test = test.Replace("\"\"", "\"");
                            probeLengthTable.Add(test);
                        }
                    }
                }
                else
                {
                    GenerateDefaultProbeLengthTable();
                }
            }
            catch { }
        }

        public void GenerateDefaultProbeLengthTable()
        {
            string directory = @"C:\Wolverine\PC WOLVERINE FIELD APPS\";
            string fileName = directory + "PROBE LENGTH PC WOLVERINE FIELD APPS.csv";

            int counter = 1;
            string collarLengthDefaultString = "";

            collarLengthDefaultString += counter.ToString() + ",8\" Pulser Stick-Up,12.3\n";
            counter++;
            collarLengthDefaultString += counter.ToString() + ",6 3/4\" Pulser Stick-Up,11.15\n";
            counter++;
            collarLengthDefaultString += counter.ToString() + ",4 3/4\" Pulser Stick-Up,11.15\n";
            counter++;
            collarLengthDefaultString += counter.ToString() + ",Battery,64.875\n";
            counter++;
            collarLengthDefaultString += counter.ToString() + ",DM (Master-Dir),51.15\n";
            counter++;
            collarLengthDefaultString += counter.ToString() + ",Gamma,40\n";
            counter++;
            collarLengthDefaultString += counter.ToString() + ",Female Stabber (Max Length), 43.125, 12\" spacer\n";
            counter++;
            collarLengthDefaultString += counter.ToString() + ",Female Stabber (Custom Length),, 7 or 8 9 10 11 spacer \n";
            counter++;
            collarLengthDefaultString += counter.ToString() + ",Female Stabber (Min Length),37.125, 6\" spacer \n";
            counter++;
            collarLengthDefaultString += counter.ToString() + ",Female Stabber Stick-Up,-6.1\n";
            counter++;
            collarLengthDefaultString += counter.ToString() + ",Male Stabber (Fixed Length), 27.5\n";
            counter++;
            collarLengthDefaultString += counter.ToString() + ",Male Stabber Stick-Up, -7.795\n";
            counter++;
            collarLengthDefaultString += counter.ToString() + ",Spacer Barrel Kit (Max Length), 16.507, 12\" + 4.507\"\n";
            counter++;
            collarLengthDefaultString += counter.ToString() + ",Spacer Barrel Kit (Custom Length), , (7 or 8 9 10 11)\" + 4.507\"\n";
            counter++;
            collarLengthDefaultString += counter.ToString() + ",Spacer Barrel Kit (Min Length), 10.507, 6\" + 4.507\"\n";
            counter++;
            collarLengthDefaultString += counter.ToString() + ",PWD Probe, 34.875\n";
            counter++;
            collarLengthDefaultString += counter.ToString() + ",FlowLine Adapter, 31.875\n";
            counter++;
            collarLengthDefaultString += counter.ToString() + ",FlowLine Adatper Stick-Up (Toward RSS), 23.1\n";
            counter++;
            collarLengthDefaultString += counter.ToString() + ",8\" PWD Stick-Up,6.42\n";
            counter++;
            collarLengthDefaultString += counter.ToString() + ",6 3/4\" PWD Stick-Up,6.42\n";
            counter++;
            collarLengthDefaultString += counter.ToString() + ",4 3/4\" PWD Stick-Up,9.92\n";
            counter++;
            collarLengthDefaultString += counter.ToString() + ",WOFT Adapter,\n";

            using (var sw = new StreamWriter(fileName))
            {
                sw.WriteLine("Item #, Probe Tools, Length[INCH], Notes\n" +
                    collarLengthDefaultString
                    ) ;
            }

            Console.WriteLine("PROBE LENGTH TABLE LOADED SUCCESSFULLY");

            CheckDefaultProbeLengthTable();
        }

        public double getApiCalibrateValue()
        {
            return calibratorApiValue;
        }

        public void runDefaultBhaLengthBuilder()
        {
            if(toolSizeComboBox.Text == "8\"")
            {
                for (int i = 0; i < probeLengthTable.Count(); i++)
                {
                    if (probeLengthTable.ElementAt(i).Contains("8\" Pulser Stick-Up"))
                    {
                        probeCombo1.SelectedIndex = i;
                    }
                }
                for (int i = 0; i < probeLengthTable.Count(); i++)
                {
                    if (probeLengthTable.ElementAt(i).Contains("Battery"))
                    {
                        probeCombo2.SelectedIndex = i;
                    }
                }
                for (int i = 0; i < probeLengthTable.Count(); i++)
                {
                    if (probeLengthTable.ElementAt(i).Contains("Battery"))
                    {
                        probeCombo3.SelectedIndex = i;
                    }
                }
                for (int i = 0; i < probeLengthTable.Count(); i++)
                {
                    if (probeLengthTable.ElementAt(i).Contains("Battery"))
                    {
                        probeCombo4.SelectedIndex = i;
                    }
                }
                for (int i = 0; i < probeLengthTable.Count(); i++)
                {
                    if (probeLengthTable.ElementAt(i).Contains("DM (Master-Dir)"))
                    {
                        probeCombo5.SelectedIndex = i;
                    }
                }
                for (int i = 0; i < probeLengthTable.Count(); i++)
                {
                    if (probeLengthTable.ElementAt(i).Contains("Gamma"))
                    {
                        probeCombo6.SelectedIndex = i;
                    }
                }
                for (int i = 0; i < probeLengthTable.Count(); i++)
                {
                    if (probeLengthTable.ElementAt(i).Contains("Female Stabber (Max Length)"))
                    {
                        probeCombo7.SelectedIndex = i;
                    }
                }
                for (int i = 0; i < probeLengthTable.Count(); i++)
                {
                    if (probeLengthTable.ElementAt(i).Contains("Female Stabber Stick-Up"))
                    {
                        probeCombo8.SelectedIndex = i;
                    }
                }

                calculateMwdNmdcLength();

                for (int i = 0; i < collarLengthTable.Count(); i++)
                {
                    if (collarLengthTable.ElementAt(i).Contains("8\" HOS (Pulser Sub)"))
                    {
                        bhaCombo1.SelectedIndex = i;
                    }
                }
                for (int i = 0; i < collarLengthTable.Count(); i++)
                {
                    if (collarLengthTable.ElementAt(i).Contains("8\" MWD NMDC"))
                    {
                        bhaCombo2.SelectedIndex = i;
                    }
                }
                for (int i = 0; i < collarLengthTable.Count(); i++)
                {
                    if (collarLengthTable.ElementAt(i).Contains("8\" MFPWR Top Sub"))
                    {
                        bhaCombo3.SelectedIndex = i;
                    }
                }
                for (int i = 0; i < collarLengthTable.Count(); i++)
                {
                    if (collarLengthTable.ElementAt(i).Contains("8\" MFPWR Tool"))
                    {
                        bhaCombo4.SelectedIndex = i;
                    }
                }
                for (int i = 0; i < collarLengthTable.Count(); i++)
                {
                    if (collarLengthTable.ElementAt(i).Contains("8\" MFPWR Bottom Sub (to fit PWD)"))
                    {
                        bhaCombo5.SelectedIndex = i;
                    }
                }
                for (int i = 0; i < collarLengthTable.Count(); i++)
                {
                    if (collarLengthTable.ElementAt(i).Contains("8\" PWD Collar"))
                    {
                        bhaCombo6.SelectedIndex = i;
                    }
                }

                calculateBhaLength();
            }
            if (toolSizeComboBox.Text == "6 3/4\"")
            {
                for (int i = 0; i < probeLengthTable.Count(); i++)
                {
                    if (probeLengthTable.ElementAt(i).Contains("6 3/4\" Pulser Stick-Up"))
                    {
                        probeCombo1.SelectedIndex = i;
                    }
                }
                for (int i = 0; i < probeLengthTable.Count(); i++)
                {
                    if (probeLengthTable.ElementAt(i).Contains("Battery"))
                    {
                        probeCombo2.SelectedIndex = i;
                    }
                }
                for (int i = 0; i < probeLengthTable.Count(); i++)
                {
                    if (probeLengthTable.ElementAt(i).Contains("Battery"))
                    {
                        probeCombo3.SelectedIndex = i;
                    }
                }
                for (int i = 0; i < probeLengthTable.Count(); i++)
                {
                    if (probeLengthTable.ElementAt(i).Contains("Battery"))
                    {
                        probeCombo4.SelectedIndex = i;
                    }
                }
                for (int i = 0; i < probeLengthTable.Count(); i++)
                {
                    if (probeLengthTable.ElementAt(i).Contains("DM (Master-Dir)"))
                    {
                        probeCombo5.SelectedIndex = i;
                    }
                }
                for (int i = 0; i < probeLengthTable.Count(); i++)
                {
                    if (probeLengthTable.ElementAt(i).Contains("Gamma"))
                    {
                        probeCombo6.SelectedIndex = i;
                    }
                }
                for (int i = 0; i < probeLengthTable.Count(); i++)
                {
                    if (probeLengthTable.ElementAt(i).Contains("Female Stabber (Max Length)"))
                    {
                        probeCombo7.SelectedIndex = i;
                    }
                }
                for (int i = 0; i < probeLengthTable.Count(); i++)
                {
                    if (probeLengthTable.ElementAt(i).Contains("Female Stabber Stick-Up"))
                    {
                        probeCombo8.SelectedIndex = i;
                    }
                }

                calculateMwdNmdcLength();

                for (int i = 0; i < collarLengthTable.Count(); i++)
                {
                    if (collarLengthTable.ElementAt(i).Contains("6 3/4\" HOS (Pulser Sub)"))
                    {
                        bhaCombo1.SelectedIndex = i;
                    }
                }
                for (int i = 0; i < collarLengthTable.Count(); i++)
                {
                    if (collarLengthTable.ElementAt(i).Contains("6 3/4\" MWD NMDC"))
                    {
                        bhaCombo2.SelectedIndex = i;
                    }
                }
                for (int i = 0; i < collarLengthTable.Count(); i++)
                {
                    if (collarLengthTable.ElementAt(i).Contains("6 3/4\" MFPWR Top Sub"))
                    {
                        bhaCombo3.SelectedIndex = i;
                    }
                }
                for (int i = 0; i < collarLengthTable.Count(); i++)
                {
                    if (collarLengthTable.ElementAt(i).Contains("6 3/4\" MFPWR Tool"))
                    {
                        bhaCombo4.SelectedIndex = i;
                    }
                }
                for (int i = 0; i < collarLengthTable.Count(); i++)
                {
                    if (collarLengthTable.ElementAt(i).Contains("6 3/4\" MFPWR Bottom Sub (to fit PWD)"))
                    {
                        bhaCombo5.SelectedIndex = i;
                    }
                }
                for (int i = 0; i < collarLengthTable.Count(); i++)
                {
                    if (collarLengthTable.ElementAt(i).Contains("6 3/4\" PWD Collar"))
                    {
                        bhaCombo6.SelectedIndex = i;
                    }
                }

                calculateBhaLength();
            }
            if (toolSizeComboBox.Text == "4 3/4\"")
            {
                for (int i = 0; i < probeLengthTable.Count(); i++)
                {
                    if (probeLengthTable.ElementAt(i).Contains("4 3/4\" Pulser Stick-Up"))
                    {
                        probeCombo1.SelectedIndex = i;
                    }
                }
                for (int i = 0; i < probeLengthTable.Count(); i++)
                {
                    if (probeLengthTable.ElementAt(i).Contains("Battery"))
                    {
                        probeCombo2.SelectedIndex = i;
                    }
                }
                for (int i = 0; i < probeLengthTable.Count(); i++)
                {
                    if (probeLengthTable.ElementAt(i).Contains("Battery"))
                    {
                        probeCombo3.SelectedIndex = i;
                    }
                }
                for (int i = 0; i < probeLengthTable.Count(); i++)
                {
                    if (probeLengthTable.ElementAt(i).Contains("Battery"))
                    {
                        probeCombo4.SelectedIndex = i;
                    }
                }
                for (int i = 0; i < probeLengthTable.Count(); i++)
                {
                    if (probeLengthTable.ElementAt(i).Contains("DM (Master-Dir)"))
                    {
                        probeCombo5.SelectedIndex = i;
                    }
                }
                for (int i = 0; i < probeLengthTable.Count(); i++)
                {
                    if (probeLengthTable.ElementAt(i).Contains("Gamma"))
                    {
                        probeCombo6.SelectedIndex = i;
                    }
                }
                for (int i = 0; i < probeLengthTable.Count(); i++)
                {
                    if (probeLengthTable.ElementAt(i).Contains("Female Stabber (Max Length)"))
                    {
                        probeCombo7.SelectedIndex = i;
                    }
                }
                for (int i = 0; i < probeLengthTable.Count(); i++)
                {
                    if (probeLengthTable.ElementAt(i).Contains("Female Stabber Stick-Up"))
                    {
                        probeCombo8.SelectedIndex = i;
                    }
                }

                calculateMwdNmdcLength();

                for (int i = 0; i < collarLengthTable.Count(); i++)
                {
                    if (collarLengthTable.ElementAt(i).Contains("4 3/4\" HOS (Pulser Sub)"))
                    {
                        bhaCombo1.SelectedIndex = i;
                    }
                }
                for (int i = 0; i < collarLengthTable.Count(); i++)
                {
                    if (collarLengthTable.ElementAt(i).Contains("4 3/4\" MWD NMDC"))
                    {
                        bhaCombo2.SelectedIndex = i;
                    }
                }
                for (int i = 0; i < collarLengthTable.Count(); i++)
                {
                    if (collarLengthTable.ElementAt(i).Contains("4 3/4\" MFPWR Top Sub"))
                    {
                        bhaCombo3.SelectedIndex = i;
                    }
                }
                for (int i = 0; i < collarLengthTable.Count(); i++)
                {
                    if (collarLengthTable.ElementAt(i).Contains("4 3/4\" MFPWR Tool"))
                    {
                        bhaCombo4.SelectedIndex = i;
                    }
                }
                for (int i = 0; i < collarLengthTable.Count(); i++)
                {
                    if (collarLengthTable.ElementAt(i).Contains("4 3/4\" MFPWR Bottom Sub (to fit PWD)"))
                    {
                        bhaCombo5.SelectedIndex = i;
                    }
                }
                for (int i = 0; i < collarLengthTable.Count(); i++)
                {
                    if (collarLengthTable.ElementAt(i).Contains("4 3/4\" PWD Collar"))
                    {
                        bhaCombo6.SelectedIndex = i;
                    }
                }

                calculateBhaLength();
            }
        }

        public void initializeProbeBoxes()
        {
            try
            {
                string[] pieces;
                for (int i = 0; i < probeLengthTable.Count(); i++)
                {
                    pieces = probeLengthTable.ElementAt(i).Split(',');
                    if (pieces.Count() > 0)
                    {
                        probeCombo1.Items.Add(pieces[1]);
                        probeCombo2.Items.Add(pieces[1]);
                        probeCombo3.Items.Add(pieces[1]);
                        probeCombo4.Items.Add(pieces[1]);
                        probeCombo5.Items.Add(pieces[1]);
                        probeCombo6.Items.Add(pieces[1]);
                        probeCombo7.Items.Add(pieces[1]);
                        probeCombo8.Items.Add(pieces[1]);
                        probeCombo9.Items.Add(pieces[1]);
                        probeCombo10.Items.Add(pieces[1]);
                        probeCombo11.Items.Add(pieces[1]);
                        probeCombo12.Items.Add(pieces[1]);
                        probeCombo13.Items.Add(pieces[1]);
                        probeCombo14.Items.Add(pieces[1]);
                        probeCombo15.Items.Add(pieces[1]);
                        probeCombo16.Items.Add(pieces[1]);
                        probeCombo17.Items.Add(pieces[1]);
                        probeCombo18.Items.Add(pieces[1]);
                        probeCombo19.Items.Add(pieces[1]);
                        probeCombo20.Items.Add(pieces[1]);
                    }
                }
                probeCombo1.Items.Add("");
                probeCombo2.Items.Add("");
                probeCombo3.Items.Add("");
                probeCombo4.Items.Add("");
                probeCombo5.Items.Add("");
                probeCombo6.Items.Add("");
                probeCombo7.Items.Add("");
                probeCombo8.Items.Add("");
                probeCombo9.Items.Add("");
                probeCombo10.Items.Add("");
                probeCombo11.Items.Add("");
                probeCombo12.Items.Add("");
                probeCombo13.Items.Add("");
                probeCombo14.Items.Add("");
                probeCombo15.Items.Add("");
                probeCombo16.Items.Add("");
                probeCombo17.Items.Add("");
                probeCombo18.Items.Add("");
                probeCombo19.Items.Add("");
                probeCombo20.Items.Add("");
            }
            catch (Exception e) { }
        }

        public void initializeCollarBoxes()
        {
            try
            {
                string[] pieces;
                for (int i = 0; i < collarLengthTable.Count(); i++)
                {
                    pieces = collarLengthTable.ElementAt(i).Split(',');
                    if (pieces.Count() > 0)
                    {
                        bhaCombo1.Items.Add(pieces[1]);
                        bhaCombo2.Items.Add(pieces[1]);
                        bhaCombo3.Items.Add(pieces[1]);
                        bhaCombo4.Items.Add(pieces[1]);
                        bhaCombo5.Items.Add(pieces[1]);
                        bhaCombo6.Items.Add(pieces[1]);
                        bhaCombo7.Items.Add(pieces[1]);
                        bhaCombo8.Items.Add(pieces[1]);
                        bhaCombo9.Items.Add(pieces[1]);
                        bhaCombo10.Items.Add(pieces[1]);
                        bhaCombo11.Items.Add(pieces[1]);
                        bhaCombo12.Items.Add(pieces[1]);
                        bhaCombo13.Items.Add(pieces[1]);
                        bhaCombo14.Items.Add(pieces[1]);
                        bhaCombo15.Items.Add(pieces[1]);
                        bhaCombo16.Items.Add(pieces[1]);
                        bhaCombo17.Items.Add(pieces[1]);
                        bhaCombo18.Items.Add(pieces[1]);
                        bhaCombo19.Items.Add(pieces[1]);
                        bhaCombo20.Items.Add(pieces[1]);
                    }
                }

                bhaCombo1.Items.Add("");
                bhaCombo2.Items.Add("");
                bhaCombo3.Items.Add("");
                bhaCombo4.Items.Add("");
                bhaCombo5.Items.Add("");
                bhaCombo6.Items.Add("");
                bhaCombo7.Items.Add("");
                bhaCombo8.Items.Add("");
                bhaCombo9.Items.Add("");
                bhaCombo10.Items.Add("");
                bhaCombo11.Items.Add("");
                bhaCombo12.Items.Add("");
                bhaCombo13.Items.Add("");
                bhaCombo14.Items.Add("");
                bhaCombo15.Items.Add("");
                bhaCombo16.Items.Add("");
                bhaCombo17.Items.Add("");
                bhaCombo18.Items.Add("");
                bhaCombo19.Items.Add("");
                bhaCombo20.Items.Add("");
            }
            catch (Exception ex) { }
        }

        private void pinRestrictNozzleSize_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (pinRestrictNozzleUnit.Text == "1/32 in")
                {
                    double pinRestrictorTFA = 0;

                    pinRestrictorTFA = Math.Pow(Convert.ToSingle(pinRestrictNozzleSize.Text) / 32.0, 2) * Math.PI / 4.0;

                    pinRestrictTFABox.Text = String.Format("{0:0.000}", pinRestrictorTFA);

                    pinRestrictTFAUnitsBox.Text = "IN^2";
                }
                else
                {
                    double pinRestrictorTFA = 0;

                    pinRestrictorTFA = Math.Pow((Convert.ToSingle(pinRestrictNozzleSize.Text) * 1.26) / 32.0, 2) * Math.PI / 4.0;

                    pinRestrictTFABox.Text = String.Format("{0:0.000}", pinRestrictorTFA * 645.16);

                    pinRestrictTFAUnitsBox.Text = "MM^2";
                }
            }
            catch
            {

            }
        }

        private void printButton_Click(object sender, EventArgs e)
        {

            Graphics myGraphics = this.CreateGraphics();
            Size s = this.Size;
            memoryImage = new Bitmap(s.Width, s.Height, myGraphics);
            Graphics memoryGraphics = Graphics.FromImage(memoryImage);
            memoryGraphics.CopyFromScreen(this.Location.X, this.Location.Y, 0, 0, s);


            // Allow the user to choose the page range he or she would
            // like to print.
            printDialog1.AllowSomePages = true;

            // Show the help button.
            printDialog1.ShowHelp = true;

            // Set the Document property to the PrintDocument for 
            // which the PrintPage Event has been handled. To display the
            // dialog, either this property or the PrinterSettings property 
            // must be set 
            printDialog1.Document = docToPrint;

            DialogResult result = printDialog1.ShowDialog();

            // If the result is OK then print the document.
            if (result == DialogResult.OK)
            {
                docToPrint.Print();
            }
        }

        private void printPDF(System.Object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
           e.Graphics.DrawImage(memoryImage, e.MarginBounds);
        }

        private void orificeFlowRateUnitsBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateChart();
        }

        private void orificeMudWeightUnitsBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            setButton.PerformClick();
        }

        private void solenoidPressureCompBox_CheckedChanged(object sender, EventArgs e)
        {
            processRSSCalculator();
        }

        private void rssCalculateButton_Click(object sender, EventArgs e)
        {
            processRSSCalculator();
        }

        private void pressureDropUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            processRSSCalculator();
        }

        private void nozzle1Units_SelectedIndexChanged(object sender, EventArgs e)
        {
            processRSSCalculator();
        }

        private void mudWeightUnitsBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            processRSSCalculator();
        }

        private void flowRateUnitBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            processRSSCalculator();
        }

        private void pinRestrictNozzleUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(pinRestrictNozzleUnit.Text == "1/32 in")
            {
                double pinRestrictorTFA = 0;

                pinRestrictorTFA = Math.Pow(Convert.ToSingle(pinRestrictNozzleSize.Text) / 32.0, 2) * Math.PI / 4.0;

                pinRestrictTFABox.Text = String.Format("{0:0.000}", pinRestrictorTFA);

                pinRestrictTFAUnitsBox.Text = "IN^2";
            }
            else
            {
                double pinRestrictorTFA = 0;

                pinRestrictorTFA = Math.Pow((Convert.ToSingle(pinRestrictNozzleSize.Text) * 1.26 )/ 32.0, 2) * Math.PI / 4.0 ;

                pinRestrictTFABox.Text = String.Format("{0:0.000}", pinRestrictorTFA * 645.16);

                pinRestrictTFAUnitsBox.Text = "MM^2";
            }

            processRSSCalculator();
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            processRSSCalculator();
        }

        private void rssResetButton_Click(object sender, EventArgs e)
        {
            mudWeightSection1.Text = "1.3";
            mudWeightSection2.Text = "1.3";
            mudWeightSection3.Text = "1.3";
            mudWeightUnitsBox.SelectedIndex = 4;

            flowRateSection1.Text = "13";
            flowRateSection2.Text = "14";
            flowRateSection3.Text = "14";
            flowRateUnitBox.SelectedIndex = 1;

            pinRestrictNozzleSize.Text = "0";
            pinRestrictNozzleUnit.Text = "1/32in";

            pinRestrictTFABox.Text = "0.000";
            pinRestrictTFAUnitsBox.Text = "IN^2";

            nozzle1Size.Text = "8";
            nozzle1Units.Text = "1/32in";
            nozzle2Size.Text = "8";
            nozzle3Size.Text = "8";
            nozzle4Size.Text = "9";
            nozzle5Size.Text = "9";
            nozzle6Size.Text = "9";
            nozzle7Size.Text = "0";
            nozzle8Size.Text = "0";

            processRSSCalculator();
        }

        private void processRSSCalculator()
        {
            double mudweight1 = 0, mudweight2 = 0, mudweight3 = 0;
            double flowRate1 = 0, flowRate2 = 0, flowRate3 = 0;
            double pinRestrictNozzle = 0, pinRestrictTFA = 0;
            double nozzleSize1 = 0, nozzleSize2 = 0, nozzleSize3 = 0, nozzleSize4 = 0, nozzleSize5 = 0,
                nozzleSize6 = 0, nozzleSize7 = 0, nozzleSize8 = 0;
            double TFA = 0, pressureDrop1 = 0, pressureDrop2 = 0, pressureDrop3 = 0;

            mudweight1 = Convert.ToDouble(mudWeightSection1.Text);
            mudweight2 = Convert.ToDouble(mudWeightSection2.Text);
            mudweight3 = Convert.ToDouble(mudWeightSection3.Text);

            flowRate1 = Convert.ToDouble(flowRateSection1.Text);
            flowRate2 = Convert.ToDouble(flowRateSection2.Text);
            flowRate3 = Convert.ToDouble(flowRateSection3.Text);

            pinRestrictNozzle = Convert.ToDouble(pinRestrictNozzleSize.Text);
            pinRestrictTFA = Convert.ToDouble(pinRestrictTFABox.Text);

            if(pinRestrictNozzleUnit.Text == "1/32 in")
                pinRestrictTFAUnitsBox.Text = "IN^2";
            else if(pinRestrictNozzleUnit.Text == "MM")
                pinRestrictTFAUnitsBox.Text = "MM^2";

            nozzleSize1 = Convert.ToDouble(nozzle1Size.Text);
            nozzleSize2 = Convert.ToDouble(nozzle2Size.Text);
            nozzleSize3 = Convert.ToDouble(nozzle3Size.Text);
            nozzleSize4 = Convert.ToDouble(nozzle4Size.Text);
            nozzleSize5 = Convert.ToDouble(nozzle5Size.Text);
            nozzleSize6 = Convert.ToDouble(nozzle6Size.Text);
            nozzleSize7 = Convert.ToDouble(nozzle7Size.Text);
            nozzleSize8 = Convert.ToDouble(nozzle8Size.Text);

            #region Convert Mud Weight to PPG
            if (mudWeightUnitsBox.Text == "LB/FT^3")
            {
                mudweight1 *= 7.48;
                mudweight2 *= 7.48;
                mudweight3 *= 7.48;
            }
            else if (mudWeightUnitsBox.Text == "KG/M^3")
            {
                mudweight1 *= .0083;
                mudweight2 *= .0083;
                mudweight3 *= .0083;
            }
            else if (mudWeightUnitsBox.Text == "SG")
            {
                mudweight1 *= 8.33;
                mudweight2 *= 8.33;
                mudweight3 *= 8.33;
            }
            else if (mudWeightUnitsBox.Text == "G/CM^3")
            {
                mudweight1 *= 8.33;
                mudweight2 *= 8.33;
                mudweight3 *= 8.33;
            }
            #endregion

            #region Convert Flow Rate to GPM
            if (flowRateUnitBox.Text == "LPS")
            {
                flowRate1 *= 15.85037248;
                flowRate2 *= 15.85037248;
                flowRate3 *= 15.85037248;
            }
            else if (flowRateUnitBox.Text == "LPM")
            {
                flowRate1 *= .26417;
                flowRate2 *= .26417;
                flowRate3 *= .26417;
            }
            //Calculator Motor Bypass
            flowRate1 = flowRate1 - (flowRate1 * motorBypass);
            flowRate2 = flowRate2 - (flowRate2 * motorBypass);
            flowRate3 = flowRate3 - (flowRate3 * motorBypass);
            #endregion

            #region Convert Nozzle Sizes to 1/32 inch, TFA to in^2
            if (nozzle1Units.Text == "MM")
            {
                pinRestrictNozzle *= 1.26;

                nozzleSize1 *= 1.26;
                nozzleSize2 *= 1.26;
                nozzleSize3 *= 1.26;
                nozzleSize4 *= 1.26;
                nozzleSize5 *= 1.26;
                nozzleSize6 *= 1.26;
                nozzleSize7 *= 1.26;
                nozzleSize8 *= 1.26;

                pinRestrictNozzleUnit.SelectedIndex = 1;
                nozzle1Units.SelectedIndex = 1;
                nozzle2Units.Text = "MM";
                nozzle3Units.Text = "MM";
                nozzle4Units.Text = "MM";
                nozzle5Units.Text = "MM";
                nozzle6Units.Text = "MM";
                nozzle7Units.Text = "MM";
                nozzle8Units.Text = "MM";
            }
            else
            {
                nozzle1Units.SelectedIndex = 0;
                nozzle2Units.Text = "1/32 in";
                nozzle3Units.Text = "1/32 in";
                nozzle4Units.Text = "1/32 in";
                nozzle5Units.Text = "1/32 in";
                nozzle6Units.Text = "1/32 in";
                nozzle7Units.Text = "1/32 in";
                nozzle8Units.Text = "1/32 in";
            }
            #endregion

            #region TFA Calculation
            TFA += (nozzleSize1 / 32.0) * (nozzleSize1 / 32.0);
            TFA += (nozzleSize2 / 32.0) * (nozzleSize2 / 32.0);
            TFA += (nozzleSize3 / 32.0) * (nozzleSize3 / 32.0);
            TFA += (nozzleSize4 / 32.0) * (nozzleSize4 / 32.0);
            TFA += (nozzleSize5 / 32.0) * (nozzleSize5 / 32.0);
            TFA += (nozzleSize6 / 32.0) * (nozzleSize6 / 32.0);
            TFA += (nozzleSize7 / 32.0) * (nozzleSize7 / 32.0);
            TFA += (nozzleSize8 / 32.0) * (nozzleSize8 / 32.0);
            TFA *= Math.PI / 4.0;

            if (nozzle1Units.Text == "1/32 in")
            {
                nozzleTFA.Text = String.Format("{0:0.000}", TFA);
                tfaUnits.Text = "IN^2";
            }
            else if (nozzle1Units.Text == "MM")
            {
                nozzleTFA.Text = String.Format("{0:0.000}", TFA * 645.16);
                tfaUnits.Text = "MM^2";
            }
            #endregion

            #region Pressure Drop Calculation
            if (pinRestrictTFA > 0)
                pressureDrop1 = (flowRate1 * flowRate1 * mudweight1) / (12034.0 * pinRestrictTFA * pinRestrictTFA * 0.95 * 0.95);
            if(TFA > 0)
                pressureDrop1 += (flowRate1 * flowRate1 * mudweight1) / (12034.0 * TFA * TFA * 0.95 * 0.95);

            if (pinRestrictTFA > 0)
                pressureDrop2 = (flowRate2 * flowRate2 * mudweight2) / (12034.0 * pinRestrictTFA * pinRestrictTFA * 0.95 * 0.95);
            if (TFA > 0)
                pressureDrop2 += (flowRate2 * flowRate2 * mudweight2) / (12034.0 * TFA * TFA * 0.95 * 0.95);

            if (pinRestrictTFA > 0)
                pressureDrop3 = (flowRate3 * flowRate3 * mudweight3) / (12034.0 * pinRestrictTFA * pinRestrictTFA * 0.95 * 0.95);
            if (TFA > 0)
                pressureDrop3 += (flowRate3 * flowRate3 * mudweight3) / (12034.0 * TFA * TFA * 0.95 * 0.95);

            if (pressureDropUnits.Text == "ATM")
            {
                pressureDrop1 *= 0.068046;
                pressureDrop2 *= 0.068046;
                pressureDrop3 *= 0.068046;
            }
            else if (pressureDropUnits.Text == "BAR")
            {
                pressureDrop1 *= 0.06895;
                pressureDrop2 *= 0.06895;
                pressureDrop3 *= 0.06895;
            }
            else if (pressureDropUnits.Text == "kPa")
            {
                pressureDrop1 *= 6.89476;
                pressureDrop2 *= 6.89476;
                pressureDrop3 *= 6.89476;
            }
            #endregion

            #region Pressure Drop Solenoid
            if (fourThreeQuarterToolSizeBox.Checked == true)
            {
                double C0 = 226.2269779, C1 = -2.235554462, C2 = 0.005879114;

                double solenoid1PressureDrop = 0, solenoid2PressureDrop = 0, solenoid3PressureDrop = 0;

                solenoid1PressureDrop += (mudweight1 / 8.34) * ((C2 * Math.Pow(flowRate1, 2)) + (C1 * flowRate1) + C0);

                solenoid2PressureDrop += (mudweight2 / 8.34) * ((C2 * Math.Pow(flowRate2, 2)) + (C1 * flowRate2) + C0);

                solenoid3PressureDrop += (mudweight3 / 8.34) * ((C2 * Math.Pow(flowRate3, 2)) + (C1 * flowRate3) + C0);

                if (pressureDropUnits.Text == "ATM")
                {
                    solenoid1PressureDrop *= 0.068046;
                    solenoid2PressureDrop *= 0.068046;
                    solenoid3PressureDrop *= 0.068046;
                }
                else if (pressureDropUnits.Text == "BAR")
                {
                    solenoid1PressureDrop *= 0.06895;
                    solenoid2PressureDrop *= 0.06895;
                    solenoid3PressureDrop *= 0.06895;
                }
                else if (pressureDropUnits.Text == "kPa")
                {
                    solenoid1PressureDrop *= 6.89476;
                    solenoid2PressureDrop *= 6.89476;
                    solenoid3PressureDrop *= 6.89476;
                }

                pressureDrop1 += solenoid1PressureDrop;
                pressureDrop2 += solenoid2PressureDrop;
                pressureDrop3 += solenoid3PressureDrop;
            }
            #endregion

            pressureDrop1Box.Text = String.Format("{0:0}", pressureDrop1);
            pressureDrop2Box.Text = String.Format("{0:0}", pressureDrop2);
            pressureDrop3Box.Text = String.Format("{0:0}", pressureDrop3);

            mudWeightSection1.BackColor = Color.Yellow;
            mudWeightSection2.BackColor = Color.Yellow;
            mudWeightSection3.BackColor = Color.Yellow;

            flowRateSection1.BackColor = Color.Yellow;
            flowRateSection2.BackColor = Color.Yellow;
            flowRateSection3.BackColor = Color.Yellow;

            pinRestrictNozzleSize.BackColor = Color.Yellow;
            pinRestrictTFABox.BackColor = Color.Yellow;

            nozzle1Size.BackColor = Color.Yellow;
            nozzle2Size.BackColor = Color.Yellow;
            nozzle3Size.BackColor = Color.Yellow;
            nozzle4Size.BackColor = Color.Yellow;
            nozzle5Size.BackColor = Color.Yellow;
            nozzle6Size.BackColor = Color.Yellow;
            nozzle7Size.BackColor = Color.Yellow;
            nozzle8Size.BackColor = Color.Yellow;

            nozzleTFA.BackColor = Color.Orange;

            if (pressureDropUnits.Text == "ATM")
            {
                if (fourThreeQuarterToolSizeBox.Checked)
                {
                    if (pressureDrop1 <= (400 * 0.068046) && pressureDrop1 >= (300 * 0.068046))
                        pressureDrop1Box.BackColor = Color.LightGreen;
                    else
                        pressureDrop1Box.BackColor = Color.Pink;

                    if (pressureDrop2 <= (400 * 0.068046) && pressureDrop2 >= (300 * 0.068046))
                        pressureDrop2Box.BackColor = Color.LightGreen;
                    else
                        pressureDrop2Box.BackColor = Color.Pink;

                    if (pressureDrop3 <= (400 * 0.068046) && pressureDrop3 >= (300 * 0.068046))
                        pressureDrop3Box.BackColor = Color.LightGreen;
                    else
                        pressureDrop3Box.BackColor = Color.Pink;

                    richTextBox1.Text = "Step 1: Choose desired units for each parameter.\n\n" +
                        "Step 2: Enter flow rate and mud weight for each section during run.\n\n" +
                        "Step 3: Modify bit nozzle and pin restrictor(if available) to obtain proper pressure drop of 20.4 - 27.2 ATM.";
                }
                else
                {
                    if (pressureDrop1 <= (600 * 0.068046) && pressureDrop1 >= (500 * 0.068046))
                        pressureDrop1Box.BackColor = Color.LightGreen;
                    else
                        pressureDrop1Box.BackColor = Color.Pink;

                    if (pressureDrop2 <= (600 * 0.068046) && pressureDrop2 >= (500 * 0.068046))
                        pressureDrop2Box.BackColor = Color.LightGreen;
                    else
                        pressureDrop2Box.BackColor = Color.Pink;

                    if (pressureDrop3 <= (600 * 0.068046) && pressureDrop3 >= (500 * 0.068046))
                        pressureDrop3Box.BackColor = Color.LightGreen;
                    else
                        pressureDrop3Box.BackColor = Color.Pink;

                    richTextBox1.Text = "Step 1: Choose desired units for each parameter.\n\n" +
                        "Step 2: Enter flow rate and mud weight for each section during run.\n\n" +
                        "Step 3: Modify bit nozzle and pin restrictor(if available) to obtain proper pressure drop of 34.0 - 40.8 ATM.";
                }
            }
            else if (pressureDropUnits.Text == "PSI")
            {
                if (fourThreeQuarterToolSizeBox.Checked)
                {
                    if (pressureDrop1 <= (400) && pressureDrop1 >= (300))
                        pressureDrop1Box.BackColor = Color.LightGreen;
                    else
                        pressureDrop1Box.BackColor = Color.Pink;

                    if (pressureDrop2 <= (400) && pressureDrop2 >= (300))
                        pressureDrop2Box.BackColor = Color.LightGreen;
                    else
                        pressureDrop2Box.BackColor = Color.Pink;

                    if (pressureDrop3 <= (400) && pressureDrop3 >= (300))
                        pressureDrop3Box.BackColor = Color.LightGreen;
                    else
                        pressureDrop3Box.BackColor = Color.Pink;

                    richTextBox1.Text = "Step 1: Choose desired units for each parameter.\n\n" +
                        "Step 2: Enter flow rate and mud weight for each section during run.\n\n" +
                        "Step 3: Modify bit nozzle and pin restrictor(if available) to obtain proper pressure drop of 300 - 400 PSI.";
                }
                else
                {
                    if (pressureDrop1 <= (600) && pressureDrop1 >= (500))
                        pressureDrop1Box.BackColor = Color.LightGreen;
                    else
                        pressureDrop1Box.BackColor = Color.Pink;

                    if (pressureDrop2 <= (600) && pressureDrop2 >= (500))
                        pressureDrop2Box.BackColor = Color.LightGreen;
                    else
                        pressureDrop2Box.BackColor = Color.Pink;

                    if (pressureDrop3 <= (600) && pressureDrop3 >= (500))
                        pressureDrop3Box.BackColor = Color.LightGreen;
                    else
                        pressureDrop3Box.BackColor = Color.Pink;

                    richTextBox1.Text = "Step 1: Choose desired units for each parameter.\n\n" +
                        "Step 2: Enter flow rate and mud weight for each section during run.\n\n" +
                        "Step 3: Modify bit nozzle and pin restrictor(if available) to obtain proper pressure drop of 500 - 600 PSI.";
                }
            }
            else if (pressureDropUnits.Text == "BAR")
            {
                if (fourThreeQuarterToolSizeBox.Checked)
                {
                    if (pressureDrop1 <= (400 * 0.06895) && pressureDrop1 >= (300 * 0.06895))
                        pressureDrop1Box.BackColor = Color.LightGreen;
                    else
                        pressureDrop1Box.BackColor = Color.Pink;

                    if (pressureDrop2 <= (400 * 0.06895) && pressureDrop2 >= (300 * 0.06895))
                        pressureDrop2Box.BackColor = Color.LightGreen;
                    else
                        pressureDrop2Box.BackColor = Color.Pink;

                    if (pressureDrop3 <= (400 * 0.06895) && pressureDrop3 >= (300 * 0.06895))
                        pressureDrop3Box.BackColor = Color.LightGreen;
                    else
                        pressureDrop3Box.BackColor = Color.Pink;

                    richTextBox1.Text = "Step 1: Choose desired units for each parameter.\n\n" +
                        "Step 2: Enter flow rate and mud weight for each section during run.\n\n" +
                        "Step 3: Modify bit nozzle and pin restrictor (if available) to obtain proper pressure drop of 20.7 - 27.6 BAR.";
                }
                else
                {
                    if (pressureDrop1 <= (600 * 0.06895) && pressureDrop1 >= (500 * 0.06895))
                        pressureDrop1Box.BackColor = Color.LightGreen;
                    else
                        pressureDrop1Box.BackColor = Color.Pink;

                    if (pressureDrop2 <= (600 * 0.06895) && pressureDrop2 >= (500 * 0.06895))
                        pressureDrop2Box.BackColor = Color.LightGreen;
                    else
                        pressureDrop2Box.BackColor = Color.Pink;

                    if (pressureDrop3 <= (600 * 0.06895) && pressureDrop3 >= (500 * 0.06895))
                        pressureDrop3Box.BackColor = Color.LightGreen;
                    else
                        pressureDrop3Box.BackColor = Color.Pink;

                    richTextBox1.Text = "Step 1: Choose desired units for each parameter.\n\n" +
                        "Step 2: Enter flow rate and mud weight for each section during run.\n\n" +
                        "Step 3: Modify bit nozzle and pin restrictor (if available) to obtain proper pressure drop of 34.5 - 41.4 BAR.";
                }
            }
            else if (pressureDropUnits.Text == "kPa")
            {
                if (fourThreeQuarterToolSizeBox.Checked)
                {
                    if (pressureDrop1 <= (400 * 6.89476) && pressureDrop1 >= (300 * 6.89476))
                        pressureDrop1Box.BackColor = Color.LightGreen;
                    else
                        pressureDrop1Box.BackColor = Color.Pink;

                    if (pressureDrop2 <= (400 * 6.89476) && pressureDrop2 >= (300 * 6.89476))
                        pressureDrop2Box.BackColor = Color.LightGreen;
                    else
                        pressureDrop2Box.BackColor = Color.Pink;

                    if (pressureDrop3 <= (400 * 6.89476) && pressureDrop3 >= (300 * 6.89476))
                        pressureDrop3Box.BackColor = Color.LightGreen;
                    else
                        pressureDrop3Box.BackColor = Color.Pink;

                    richTextBox1.Text = "Step 1: Choose desired units for each parameter.\n\n" +
                        "Step 2: Enter flow rate and mud weight for each section during run.\n\n" +
                        "Step 3: Modify bit nozzle and pin restrictor(if available) to obtain proper pressure drop of 2068.4 - 2757.9 kPa.";
                }
                else
                {
                    if (pressureDrop1 <= (600 * 6.89476) && pressureDrop1 >= (500 * 6.89476))
                        pressureDrop1Box.BackColor = Color.LightGreen;
                    else
                        pressureDrop1Box.BackColor = Color.Pink;

                    if (pressureDrop2 <= (600 * 6.89476) && pressureDrop2 >= (500 * 6.89476))
                        pressureDrop2Box.BackColor = Color.LightGreen;
                    else
                        pressureDrop2Box.BackColor = Color.Pink;

                    if (pressureDrop3 <= (600 * 6.89476) && pressureDrop3 >= (500 * 6.89476))
                        pressureDrop3Box.BackColor = Color.LightGreen;
                    else
                        pressureDrop3Box.BackColor = Color.Pink;

                    richTextBox1.Text = "Step 1: Choose desired units for each parameter.\n\n" +
                        "Step 2: Enter flow rate and mud weight for each section during run.\n\n" +
                        "Step 3: Modify bit nozzle and pin restrictor(if available) to obtain proper pressure drop of 3447.4 - 4136.9 kPa.";
                }
            }
        }

        private double pressureDropCalculator(double mudWeight, double flowRate, double area)
        {
            double CD = .95, pressure = 0;

            pressure = (mudWeight * Math.Pow(flowRate, 2)) / (12032.0 * Math.Pow(CD, 2) * Math.Pow(area, 2));

            return pressure;
        }

        private void mainPanel_SizeChanged(object sender, EventArgs e)
        {
            try
            {
                this.AutoScaleMode = AutoScaleMode.Dpi;

                // when the panel's size changes we should resize the child TableLayoutPanel and maintain it's size ratio
                Size panelSize = this.mainPanel.Size;
                int extent = Math.Min(panelSize.Width - this.mainTabControl.Left, panelSize.Height - this.mainTabControl.Top);
                int extent2 = Math.Max(panelSize.Width - this.mainTabControl.Left, panelSize.Height - this.mainTabControl.Top);
                this.mainPanel.Size = new Size(extent2, extent);
            }
            catch (Exception ex)
            {
            }
        }

        public void updateChart()
        {
            //chart.ChartAreas[0] = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            // a few semi-transparent colors
            int flowRate = Convert.ToInt32(orificeFlowRateTextbox.Text);

            // set up the chart area:  
            Axis ax = chart.ChartAreas[0].AxisX;
            ax.Minimum = 0;
            ax.Maximum = 360;
            ax.Interval = 30;


            chart.Series.Clear();
            chart.Titles.Clear();

            chart.ChartAreas[0].AxisX.LabelStyle.Format = "0";

            if (orificeFlowRateUnitsBox.Text == "GPM")
            {
                if (orificeToolSizeBox.Text == "4.75")
                {
                    chart.ChartAreas[0].AxisX.Maximum = 400;
                    chart.ChartAreas[0].AxisX.Minimum = 150;
                }
                else if (orificeToolSizeBox.Text == "6.75")
                {
                    chart.ChartAreas[0].AxisX.Maximum = 700;
                    chart.ChartAreas[0].AxisX.Minimum = 350;
                }
                else if (orificeToolSizeBox.Text == "8.00")
                {
                    chart.ChartAreas[0].AxisX.Maximum = 1000;
                    chart.ChartAreas[0].AxisX.Minimum = 600;
                }

                chart.ChartAreas[0].AxisX.Interval = 50;

                chart.ChartAreas[0].AxisY.LabelStyle.Format = "0";
                chart.ChartAreas[0].AxisY.Maximum = 600;
                chart.ChartAreas[0].AxisY.Minimum = 100;
                chart.ChartAreas[0].AxisY.Interval = 50;

                //chart.ChartAreas[0].AxisX.IntervalAutoMode = IntervalAutoMode.VariableCount;
                //chart.ChartAreas[0].AxisY.IntervalAutoMode = IntervalAutoMode.VariableCount;
                //chart.AlignDataPointsByAxisLabel();
                chart.ChartAreas[0].AxisX.MajorGrid.Enabled = false; //Disable gridlines for plot visibility
                chart.ChartAreas[0].AxisX.MinorGrid.Enabled = false;
                chart.ChartAreas[0].AxisY.MajorGrid.Enabled = false;
                chart.ChartAreas[0].AxisY.MinorGrid.Enabled = false;

                chart.ChartAreas[0].AxisX.Title = "Flow Rate (GPM)";
                chart.ChartAreas[0].AxisY.Title = "Pulse Amplitude";
                chart.Titles.Add("Pulse Amplitude at Tool (psi), MW = " + String.Format("{0:0.0}", mudWeight) + " PPG");

                if (orificeToolSizeBox.Text == "4.75")
                {
                    #region Orifice Code 2
                    chart.Series.Add("2");
                    chart.Series["2"].ChartType = SeriesChartType.FastLine;

                    for (int i = 0; i < orifice188.Count(); i++)
                    {
                        if (orifice188[i] != 0)
                            chart.Series["2"].Points.AddXY(GPM[i], orifice188[i]);
                    }
                    if (!verifySeriesPressurePulse(chart.Series["2"].Points.ToList(), flowRate))
                        chart.Series.Remove(chart.Series["2"]);
                    #endregion

                    #region Orifice Code 4
                    chart.Series.Add("4");
                    chart.Series["4"].ChartType = SeriesChartType.FastLine;

                    for (int i = 0; i < orifice190.Count(); i++)
                    {
                        if (orifice190[i] != 0)
                            chart.Series["4"].Points.AddXY(GPM[i], orifice190[i]);
                    }
                    if (!verifySeriesPressurePulse(chart.Series["4"].Points.ToList(), flowRate))
                        chart.Series.Remove(chart.Series["4"]);
                    #endregion

                    #region Orifice Code 6
                    chart.Series.Add("6");
                    chart.Series["6"].ChartType = SeriesChartType.FastLine;

                    for (int i = 0; i < orifice192.Count(); i++)
                    {
                        if (orifice192[i] != 0)
                            chart.Series["6"].Points.AddXY(GPM[i], orifice192[i]);
                    }
                    if (!verifySeriesPressurePulse(chart.Series["6"].Points.ToList(), flowRate))
                        chart.Series.Remove(chart.Series["6"]);
                    #endregion

                    #region Orifice Code 7
                    chart.Series.Add("7");
                    chart.Series["7"].ChartType = SeriesChartType.FastLine;

                    for (int i = 0; i < orifice194.Count(); i++)
                    {
                        if (orifice194[i] != 0)
                            chart.Series["7"].Points.AddXY(GPM[i], orifice194[i]);
                    }
                    if (!verifySeriesPressurePulse(chart.Series["7"].Points.ToList(), flowRate))
                        chart.Series.Remove(chart.Series["7"]);
                    #endregion

                    #region Orifice Code 8
                    chart.Series.Add("8");
                    chart.Series["8"].ChartType = SeriesChartType.FastLine;

                    for (int i = 0; i < orifice196.Count(); i++)
                    {
                        if (orifice196[i] != 0)
                            chart.Series["8"].Points.AddXY(GPM[i], orifice196[i]);
                    }
                    if (!verifySeriesPressurePulse(chart.Series["8"].Points.ToList(), flowRate))
                        chart.Series.Remove(chart.Series["8"]);
                    #endregion

                    if(chart.Series.Count == 0)
                    {
                        #region Orifice Code 2
                        chart.Series.Add("2");
                        chart.Series["2"].ChartType = SeriesChartType.FastLine;

                        for (int i = 0; i < orifice188.Count(); i++)
                        {
                            if (orifice188[i] != 0)
                                chart.Series["2"].Points.AddXY(GPM[i], orifice188[i]);
                        }
                        #endregion

                        #region Orifice Code 4
                        chart.Series.Add("4");
                        chart.Series["4"].ChartType = SeriesChartType.FastLine;

                        for (int i = 0; i < orifice190.Count(); i++)
                        {
                            if (orifice190[i] != 0)
                                chart.Series["4"].Points.AddXY(GPM[i], orifice190[i]);
                        }
                        #endregion

                        #region Orifice Code 6
                        chart.Series.Add("6");
                        chart.Series["6"].ChartType = SeriesChartType.FastLine;

                        for (int i = 0; i < orifice192.Count(); i++)
                        {
                            if (orifice192[i] != 0)
                                chart.Series["6"].Points.AddXY(GPM[i], orifice192[i]);
                        }
                        #endregion

                        #region Orifice Code 7
                        chart.Series.Add("7");
                        chart.Series["7"].ChartType = SeriesChartType.FastLine;

                        for (int i = 0; i < orifice194.Count(); i++)
                        {
                            if (orifice194[i] != 0)
                                chart.Series["7"].Points.AddXY(GPM[i], orifice194[i]);
                        }
                        #endregion

                        #region Orifice Code 8
                        chart.Series.Add("8");
                        chart.Series["8"].ChartType = SeriesChartType.FastLine;

                        for (int i = 0; i < orifice196.Count(); i++)
                        {
                            if (orifice196[i] != 0)
                                chart.Series["8"].Points.AddXY(GPM[i], orifice196[i]);
                        }
                        #endregion
                    }
                }

                if (orificeToolSizeBox.Text == "6.75")
                {
                    #region Orifice Code 7
                    chart.Series.Add("7");
                    chart.Series["7"].ChartType = SeriesChartType.FastLine;

                    for (int i = 0; i < orifice196.Count(); i++)
                    {
                        if (orifice196[i] != 0)
                            chart.Series["7"].Points.AddXY(GPM[i], orifice196[i]);
                    }
                    if (!verifySeriesPressurePulse(chart.Series["7"].Points.ToList(), flowRate))
                        chart.Series.Remove(chart.Series["7"]);
                    #endregion

                    #region Orifice Code 8
                    chart.Series.Add("8");
                    chart.Series["8"].ChartType = SeriesChartType.FastLine;

                    for (int i = 0; i < orifice198.Count(); i++)
                    {
                        if (orifice198[i] != 0)
                            chart.Series["8"].Points.AddXY(GPM[i], orifice198[i]);
                    }
                    if (!verifySeriesPressurePulse(chart.Series["8"].Points.ToList(), flowRate))
                        chart.Series.Remove(chart.Series["8"]);
                    #endregion

                    #region Orifice Code 9
                    chart.Series.Add("9");
                    chart.Series["9"].ChartType = SeriesChartType.FastLine;

                    for (int i = 0; i < orifice200.Count(); i++)
                    {
                        if (orifice200[i] != 0)
                            chart.Series["9"].Points.AddXY(GPM[i], orifice200[i]);
                    }
                    if (!verifySeriesPressurePulse(chart.Series["9"].Points.ToList(), flowRate))
                        chart.Series.Remove(chart.Series["9"]);
                    #endregion

                    #region Orifice Code 10
                    chart.Series.Add("10");
                    chart.Series["10"].ChartType = SeriesChartType.FastLine;

                    for (int i = 0; i < orifice202.Count(); i++)
                    {
                        if (orifice202[i] != 0)
                            chart.Series["10"].Points.AddXY(GPM[i], orifice202[i]);
                    }
                    if (!verifySeriesPressurePulse(chart.Series["10"].Points.ToList(), flowRate))
                        chart.Series.Remove(chart.Series["10"]);
                    #endregion

                    #region Orifice Code 11
                    chart.Series.Add("11");
                    chart.Series["11"].ChartType = SeriesChartType.FastLine;

                    for (int i = 0; i < orifice204.Count(); i++)
                    {
                        if (orifice204[i] != 0)
                            chart.Series["11"].Points.AddXY(GPM[i], orifice204[i]);
                    }
                    if (!verifySeriesPressurePulse(chart.Series["11"].Points.ToList(), flowRate))
                        chart.Series.Remove(chart.Series["11"]);
                    #endregion

                    #region Orifice Code 12
                    chart.Series.Add("12");
                    chart.Series["12"].ChartType = SeriesChartType.FastLine;

                    for (int i = 0; i < orifice204.Count(); i++)
                    {
                        if (orifice206[i] != 0)
                            chart.Series["12"].Points.AddXY(GPM[i], orifice206[i]);
                    }
                    if (!verifySeriesPressurePulse(chart.Series["12"].Points.ToList(), flowRate))
                        chart.Series.Remove(chart.Series["12"]);
                    #endregion

                    if(chart.Series.Count == 0)
                    {
                        #region Orifice Code 7
                        chart.Series.Add("7");
                        chart.Series["7"].ChartType = SeriesChartType.FastLine;

                        for (int i = 0; i < orifice196.Count(); i++)
                        {
                            if (orifice196[i] != 0)
                                chart.Series["7"].Points.AddXY(GPM[i], orifice196[i]);
                        }

                        #endregion

                        #region Orifice Code 8
                        chart.Series.Add("8");
                        chart.Series["8"].ChartType = SeriesChartType.FastLine;

                        for (int i = 0; i < orifice198.Count(); i++)
                        {
                            if (orifice198[i] != 0)
                                chart.Series["8"].Points.AddXY(GPM[i], orifice198[i]);
                        }

                        #endregion

                        #region Orifice Code 9
                        chart.Series.Add("9");
                        chart.Series["9"].ChartType = SeriesChartType.FastLine;

                        for (int i = 0; i < orifice200.Count(); i++)
                        {
                            if (orifice200[i] != 0)
                                chart.Series["9"].Points.AddXY(GPM[i], orifice200[i]);
                        }

                        #endregion

                        #region Orifice Code 10
                        chart.Series.Add("10");
                        chart.Series["10"].ChartType = SeriesChartType.FastLine;

                        for (int i = 0; i < orifice202.Count(); i++)
                        {
                            if (orifice202[i] != 0)
                                chart.Series["10"].Points.AddXY(GPM[i], orifice202[i]);
                        }

                        #endregion

                        #region Orifice Code 11
                        chart.Series.Add("11");
                        chart.Series["11"].ChartType = SeriesChartType.FastLine;

                        for (int i = 0; i < orifice204.Count(); i++)
                        {
                            if (orifice204[i] != 0)
                                chart.Series["11"].Points.AddXY(GPM[i], orifice204[i]);
                        }

                        #endregion

                        #region Orifice Code 12
                        chart.Series.Add("12");
                        chart.Series["12"].ChartType = SeriesChartType.FastLine;

                        for (int i = 0; i < orifice204.Count(); i++)
                        {
                            if (orifice206[i] != 0)
                                chart.Series["12"].Points.AddXY(GPM[i], orifice206[i]);
                        }

                        #endregion
                    }
                }

                if (orificeToolSizeBox.Text == "8.00")
                {
                    #region Orifice Code 1
                    chart.Series.Add("1");
                    chart.Series["1"].ChartType = SeriesChartType.FastLine;

                    for (int i = 0; i < orifice204.Count(); i++)
                    {
                        if (orifice204[i] != 0)
                            chart.Series["1"].Points.AddXY(GPM[i], orifice204[i]);
                    }
                    if (!verifySeriesPressurePulse(chart.Series["1"].Points.ToList(), flowRate))
                        chart.Series.Remove(chart.Series["1"]);
                    #endregion

                    #region Orifice Code 3
                    chart.Series.Add("3");
                    chart.Series["3"].ChartType = SeriesChartType.FastLine;

                    for (int i = 0; i < orifice204.Count(); i++)
                    {
                        if (orifice206[i] != 0)
                            chart.Series["3"].Points.AddXY(GPM[i], orifice206[i]);
                    }
                    if (!verifySeriesPressurePulse(chart.Series["3"].Points.ToList(), flowRate))
                        chart.Series.Remove(chart.Series["3"]);
                    #endregion

                    #region Orifice Code 5
                    chart.Series.Add("5");
                    chart.Series["5"].ChartType = SeriesChartType.FastLine;

                    for (int i = 0; i < orifice204.Count(); i++)
                    {
                        if (orifice208[i] != 0)
                            chart.Series["5"].Points.AddXY(GPM[i], orifice208[i]);
                    }
                    if (!verifySeriesPressurePulse(chart.Series["5"].Points.ToList(), flowRate))
                        chart.Series.Remove(chart.Series["5"]);
                    #endregion

                    #region Orifice Code 7
                    chart.Series.Add("7");
                    chart.Series["7"].ChartType = SeriesChartType.FastLine;

                    for (int i = 0; i < orifice210.Count(); i++)
                    {
                        if (orifice210[i] != 0)
                            chart.Series["7"].Points.AddXY(GPM[i], orifice210[i]);
                    }
                    if (!verifySeriesPressurePulse(chart.Series["7"].Points.ToList(), flowRate))
                        chart.Series.Remove(chart.Series["7"]);
                    #endregion

                    #region Orifice Code 8
                    chart.Series.Add("8");
                    chart.Series["8"].ChartType = SeriesChartType.FastLine;

                    for (int i = 0; i < orifice210.Count(); i++)
                    {
                        if (orifice212[i] != 0)
                            chart.Series["8"].Points.AddXY(GPM[i], orifice212[i]);
                    }
                    if (!verifySeriesPressurePulse(chart.Series["8"].Points.ToList(), flowRate))
                        chart.Series.Remove(chart.Series["8"]);
                    #endregion

                    if (chart.Series.Count == 0)
                    {
                        #region Orifice Code 1
                        chart.Series.Add("1");
                        chart.Series["1"].ChartType = SeriesChartType.FastLine;

                        for (int i = 0; i < orifice204.Count(); i++)
                        {
                            if (orifice204[i] != 0)
                                chart.Series["1"].Points.AddXY(GPM[i], orifice204[i]);
                        }

                        #endregion

                        #region Orifice Code 3
                        chart.Series.Add("3");
                        chart.Series["3"].ChartType = SeriesChartType.FastLine;

                        for (int i = 0; i < orifice204.Count(); i++)
                        {
                            if (orifice206[i] != 0)
                                chart.Series["3"].Points.AddXY(GPM[i], orifice206[i]);
                        }

                        #endregion

                        #region Orifice Code 5
                        chart.Series.Add("5");
                        chart.Series["5"].ChartType = SeriesChartType.FastLine;

                        for (int i = 0; i < orifice204.Count(); i++)
                        {
                            if (orifice208[i] != 0)
                                chart.Series["5"].Points.AddXY(GPM[i], orifice208[i]);
                        }

                        #endregion

                        #region Orifice Code 7
                        chart.Series.Add("7");
                        chart.Series["7"].ChartType = SeriesChartType.FastLine;

                        for (int i = 0; i < orifice210.Count(); i++)
                        {
                            if (orifice210[i] != 0)
                                chart.Series["7"].Points.AddXY(GPM[i], orifice210[i]);
                        }

                        #endregion

                        #region Orifice Code 8
                        chart.Series.Add("8");
                        chart.Series["8"].ChartType = SeriesChartType.FastLine;

                        for (int i = 0; i < orifice210.Count(); i++)
                        {
                            if (orifice212[i] != 0)
                                chart.Series["8"].Points.AddXY(GPM[i], orifice212[i]);
                        }

                        #endregion
                    }
                }

                #region Shade Chart
                // this is the width of the chart in values:
                double hrange = chart.ChartAreas[0].AxisY.Maximum - chart.ChartAreas[0].AxisY.Minimum;

                //Good Pulses
                StripLine sl = new StripLine();
                sl.Interval = hrange;         // no less than the range, so it won't repeat
                sl.StripWidth = 150;            // width
                sl.IntervalOffset = 150;  // x-position
                sl.BackColor = Color.LightGreen;
                chart.ChartAreas[0].AxisY.StripLines.Add(sl);

                //Okay Pulses
                sl = new StripLine();
                sl.Interval = hrange;         // no less than the range, so it won't repeat
                sl.StripWidth = 50;            // width
                sl.IntervalOffset = 100;  // x-position
                sl.BackColor = Color.PaleGoldenrod;
                chart.ChartAreas[0].AxisY.StripLines.Add(sl);

                sl = new StripLine();
                sl.Interval = hrange;         // no less than the range, so it won't repeat
                sl.StripWidth = 100;            // width
                sl.IntervalOffset = 300;  // x-position
                sl.BackColor = Color.PaleGoldenrod;
                chart.ChartAreas[0].AxisY.StripLines.Add(sl);

                //Bad Pulses
                sl = new StripLine();
                sl.Interval = hrange;         // no less than the range, so it won't repeat
                sl.StripWidth = 100;            // width
                sl.IntervalOffset = 0;  // x-position
                sl.BackColor = Color.Pink;
                chart.ChartAreas[0].AxisY.StripLines.Add(sl);

                sl = new StripLine();
                sl.Interval = hrange;         // no less than the range, so it won't repeat
                sl.StripWidth = 500;            // width
                sl.IntervalOffset = 400;  // x-position
                sl.BackColor = Color.Pink;
                chart.ChartAreas[0].AxisY.StripLines.Add(sl);
                #endregion
            }

            else if (orificeFlowRateUnitsBox.Text == "LPS")
            {
                if (orificeToolSizeBox.Text == "4.75")
                {
                    chart.ChartAreas[0].AxisX.Maximum = 25;
                    chart.ChartAreas[0].AxisX.Minimum = 9;
                }
                else if (orificeToolSizeBox.Text == "6.75")
                {
                    chart.ChartAreas[0].AxisX.Maximum = 44;
                    chart.ChartAreas[0].AxisX.Minimum = 22;
                }
                else if (orificeToolSizeBox.Text == "8.00")
                {
                    chart.ChartAreas[0].AxisX.Maximum = 62;
                    chart.ChartAreas[0].AxisX.Minimum = 36;
                }

                chart.ChartAreas[0].AxisX.Interval = 3;

                chart.ChartAreas[0].AxisY.LabelStyle.Format = "0";
                chart.ChartAreas[0].AxisY.Maximum = 42;
                chart.ChartAreas[0].AxisY.Minimum = 1;
                chart.ChartAreas[0].AxisY.Interval = 3;

                //chart.ChartAreas[0].AxisX.IntervalAutoMode = IntervalAutoMode.VariableCount;
                //chart.ChartAreas[0].AxisY.IntervalAutoMode = IntervalAutoMode.VariableCount;
                //chart.AlignDataPointsByAxisLabel();
                chart.ChartAreas[0].AxisX.MajorGrid.Enabled = false; //Disable gridlines for plot visibility
                chart.ChartAreas[0].AxisX.MinorGrid.Enabled = false;
                chart.ChartAreas[0].AxisY.MajorGrid.Enabled = false;
                chart.ChartAreas[0].AxisY.MinorGrid.Enabled = false;

                chart.ChartAreas[0].AxisX.Title = "Flow Rate (" + orificeFlowRateUnitsBox.Text + ")";
                chart.ChartAreas[0].AxisY.Title = "Pulse Amplitude";
                chart.Titles.Add("Pulse Amplitude at Tool (psi), MW = " + String.Format("{0:0.0}", mudWeight) + " PPG");

                if (orificeToolSizeBox.Text == "4.75")
                {
                    #region Orifice Code 2
                    chart.Series.Add("2");
                    chart.Series["2"].ChartType = SeriesChartType.FastLine;

                    for (int i = 0; i < orifice478.Count(); i++)
                    {
                        if (orifice188[i] != 0)
                            chart.Series["2"].Points.AddXY(LPSXaxis[i], orifice478[i]);
                    }
                    if (!verifySeriesPressurePulseLPS(chart.Series["2"].Points.ToList(), flowRate))
                        chart.Series.Remove(chart.Series["2"]);
                    #endregion

                    #region Orifice Code 4
                    chart.Series.Add("4");
                    chart.Series["4"].ChartType = SeriesChartType.FastLine;

                    for (int i = 0; i < orifice483.Count(); i++)
                    {
                        if (orifice483[i] != 0)
                            chart.Series["4"].Points.AddXY(LPSXaxis[i], orifice483[i]);
                    }
                    if (!verifySeriesPressurePulseLPS(chart.Series["4"].Points.ToList(), flowRate))
                        chart.Series.Remove(chart.Series["4"]);
                    #endregion

                    #region Orifice Code 6
                    chart.Series.Add("6");
                    chart.Series["6"].ChartType = SeriesChartType.FastLine;

                    for (int i = 0; i < orifice488.Count(); i++)
                    {
                        if (orifice488[i] != 0)
                            chart.Series["6"].Points.AddXY(LPSXaxis[i], orifice488[i]);
                    }
                    if (!verifySeriesPressurePulseLPS(chart.Series["6"].Points.ToList(), flowRate))
                        chart.Series.Remove(chart.Series["6"]);
                    #endregion

                    #region Orifice Code 7
                    chart.Series.Add("7");
                    chart.Series["7"].ChartType = SeriesChartType.FastLine;

                    for (int i = 0; i < orifice493.Count(); i++)
                    {
                        if (orifice493[i] != 0)
                            chart.Series["7"].Points.AddXY(LPSXaxis[i], orifice493[i]);
                    }
                    if (!verifySeriesPressurePulseLPS(chart.Series["7"].Points.ToList(), flowRate))
                        chart.Series.Remove(chart.Series["7"]);
                    #endregion

                    #region Orifice Code 8
                    chart.Series.Add("8");
                    chart.Series["8"].ChartType = SeriesChartType.FastLine;

                    for (int i = 0; i < orifice498.Count(); i++)
                    {
                        if (orifice498[i] != 0)
                            chart.Series["8"].Points.AddXY(LPSXaxis[i], orifice498[i]);
                    }
                    if (!verifySeriesPressurePulseLPS(chart.Series["8"].Points.ToList(), flowRate))
                        chart.Series.Remove(chart.Series["8"]);
                    #endregion

                    if (chart.Series.Count == 0)
                    {
                        #region Orifice Code 2
                        chart.Series.Add("2");
                        chart.Series["2"].ChartType = SeriesChartType.FastLine;

                        for (int i = 0; i < orifice478.Count(); i++)
                        {
                            if (orifice188[i] != 0)
                                chart.Series["2"].Points.AddXY(LPSXaxis[i], orifice478[i]);
                        }
                        #endregion

                        #region Orifice Code 4
                        chart.Series.Add("4");
                        chart.Series["4"].ChartType = SeriesChartType.FastLine;

                        for (int i = 0; i < orifice483.Count(); i++)
                        {
                            if (orifice483[i] != 0)
                                chart.Series["4"].Points.AddXY(LPSXaxis[i], orifice483[i]);
                        }
                        #endregion

                        #region Orifice Code 6
                        chart.Series.Add("6");
                        chart.Series["6"].ChartType = SeriesChartType.FastLine;

                        for (int i = 0; i < orifice488.Count(); i++)
                        {
                            if (orifice488[i] != 0)
                                chart.Series["6"].Points.AddXY(LPSXaxis[i], orifice488[i]);
                        }
                        #endregion

                        #region Orifice Code 7
                        chart.Series.Add("7");
                        chart.Series["7"].ChartType = SeriesChartType.FastLine;

                        for (int i = 0; i < orifice493.Count(); i++)
                        {
                            if (orifice493[i] != 0)
                                chart.Series["7"].Points.AddXY(LPSXaxis[i], orifice493[i]);
                        }
                        #endregion

                        #region Orifice Code 8
                        chart.Series.Add("8");
                        chart.Series["8"].ChartType = SeriesChartType.FastLine;

                        for (int i = 0; i < orifice498.Count(); i++)
                        {
                            if (orifice498[i] != 0)
                                chart.Series["8"].Points.AddXY(LPSXaxis[i], orifice498[i]);
                        }
                        #endregion
                    }
                }

                if (orificeToolSizeBox.Text == "6.75")
                {
                    #region Orifice Code 7
                    chart.Series.Add("7");
                    chart.Series["7"].ChartType = SeriesChartType.FastLine;

                    for (int i = 0; i < orifice498.Count(); i++)
                    {
                        if (orifice498[i] != 0)
                            chart.Series["7"].Points.AddXY(LPSXaxis[i], orifice498[i]);
                    }
                    if (!verifySeriesPressurePulseLPS(chart.Series["7"].Points.ToList(), flowRate))
                        chart.Series.Remove(chart.Series["7"]);
                    #endregion

                    #region Orifice Code 8
                    chart.Series.Add("8");
                    chart.Series["8"].ChartType = SeriesChartType.FastLine;

                    for (int i = 0; i < orifice503.Count(); i++)
                    {
                        if (orifice503[i] != 0)
                            chart.Series["8"].Points.AddXY(LPSXaxis[i], orifice503[i]);
                    }
                    if (!verifySeriesPressurePulseLPS(chart.Series["8"].Points.ToList(), flowRate))
                        chart.Series.Remove(chart.Series["8"]);
                    #endregion

                    #region Orifice Code 9
                    chart.Series.Add("9");
                    chart.Series["9"].ChartType = SeriesChartType.FastLine;

                    for (int i = 0; i < orifice508.Count(); i++)
                    {
                        if (orifice508[i] != 0)
                            chart.Series["9"].Points.AddXY(LPSXaxis[i], orifice508[i]);
                    }
                    if (!verifySeriesPressurePulseLPS(chart.Series["9"].Points.ToList(), flowRate))
                        chart.Series.Remove(chart.Series["9"]);
                    #endregion

                    #region Orifice Code 10
                    chart.Series.Add("10");
                    chart.Series["10"].ChartType = SeriesChartType.FastLine;

                    for (int i = 0; i < orifice513.Count(); i++)
                    {
                        if (orifice513[i] != 0)
                            chart.Series["10"].Points.AddXY(LPSXaxis[i], orifice513[i]);
                    }
                    if (!verifySeriesPressurePulseLPS(chart.Series["10"].Points.ToList(), flowRate))
                        chart.Series.Remove(chart.Series["10"]);
                    #endregion

                    #region Orifice Code 11
                    chart.Series.Add("11");
                    chart.Series["11"].ChartType = SeriesChartType.FastLine;

                    for (int i = 0; i < orifice518.Count(); i++)
                    {
                        if (orifice518[i] != 0)
                            chart.Series["11"].Points.AddXY(LPSXaxis[i], orifice518[i]);
                    }
                    if (!verifySeriesPressurePulseLPS(chart.Series["11"].Points.ToList(), flowRate))
                        chart.Series.Remove(chart.Series["11"]);
                    #endregion

                    #region Orifice Code 12
                    chart.Series.Add("12");
                    chart.Series["12"].ChartType = SeriesChartType.FastLine;

                    for (int i = 0; i < orifice523.Count(); i++)
                    {
                        if (orifice523[i] != 0)
                            chart.Series["12"].Points.AddXY(LPSXaxis[i], orifice523[i]);
                    }
                    if (!verifySeriesPressurePulseLPS(chart.Series["12"].Points.ToList(), flowRate))
                        chart.Series.Remove(chart.Series["12"]);
                    #endregion

                    if (chart.Series.Count == 0)
                    {
                        #region Orifice Code 7
                        chart.Series.Add("7");
                        chart.Series["7"].ChartType = SeriesChartType.FastLine;

                        for (int i = 0; i < orifice498.Count(); i++)
                        {
                            if (orifice498[i] != 0)
                                chart.Series["7"].Points.AddXY(LPSXaxis[i], orifice498[i]);
                        }

                        #endregion

                        #region Orifice Code 8
                        chart.Series.Add("8");
                        chart.Series["8"].ChartType = SeriesChartType.FastLine;

                        for (int i = 0; i < orifice503.Count(); i++)
                        {
                            if (orifice503[i] != 0)
                                chart.Series["8"].Points.AddXY(LPSXaxis[i], orifice503[i]);
                        }

                        #endregion

                        #region Orifice Code 9
                        chart.Series.Add("9");
                        chart.Series["9"].ChartType = SeriesChartType.FastLine;

                        for (int i = 0; i < orifice508.Count(); i++)
                        {
                            if (orifice508[i] != 0)
                                chart.Series["9"].Points.AddXY(LPSXaxis[i], orifice508[i]);
                        }

                        #endregion

                        #region Orifice Code 10
                        chart.Series.Add("10");
                        chart.Series["10"].ChartType = SeriesChartType.FastLine;

                        for (int i = 0; i < orifice513.Count(); i++)
                        {
                            if (orifice513[i] != 0)
                                chart.Series["10"].Points.AddXY(LPSXaxis[i], orifice513[i]);
                        }

                        #endregion

                        #region Orifice Code 11
                        chart.Series.Add("11");
                        chart.Series["11"].ChartType = SeriesChartType.FastLine;

                        for (int i = 0; i < orifice518.Count(); i++)
                        {
                            if (orifice518[i] != 0)
                                chart.Series["11"].Points.AddXY(LPSXaxis[i], orifice518[i]);
                        }

                        #endregion

                        #region Orifice Code 12
                        chart.Series.Add("12");
                        chart.Series["12"].ChartType = SeriesChartType.FastLine;

                        for (int i = 0; i < orifice523.Count(); i++)
                        {
                            if (orifice523[i] != 0)
                                chart.Series["12"].Points.AddXY(LPSXaxis[i], orifice523[i]);
                        }

                        #endregion
                    }
                }
                if (orificeToolSizeBox.Text == "8.00")
                {
                    #region Orifice Code 1
                    chart.Series.Add("1");
                    chart.Series["1"].ChartType = SeriesChartType.FastLine;

                    for (int i = 0; i < orifice518.Count(); i++)
                    {
                        if (orifice518[i] != 0)
                            chart.Series["1"].Points.AddXY(LPSXaxis[i], orifice518[i]);
                    }
                    if (!verifySeriesPressurePulseLPS(chart.Series["1"].Points.ToList(), flowRate))
                        chart.Series.Remove(chart.Series["1"]);
                    #endregion

                    #region Orifice Code 3
                    chart.Series.Add("3");
                    chart.Series["3"].ChartType = SeriesChartType.FastLine;

                    for (int i = 0; i < orifice523.Count(); i++)
                    {
                        if (orifice523[i] != 0)
                            chart.Series["3"].Points.AddXY(LPSXaxis[i], orifice523[i]);
                    }
                    if (!verifySeriesPressurePulseLPS(chart.Series["3"].Points.ToList(), flowRate))
                        chart.Series.Remove(chart.Series["3"]);
                    #endregion

                    #region Orifice Code 5
                    chart.Series.Add("5");
                    chart.Series["5"].ChartType = SeriesChartType.FastLine;

                    for (int i = 0; i < orifice528.Count(); i++)
                    {
                        if (orifice528[i] != 0)
                            chart.Series["5"].Points.AddXY(LPSXaxis[i], orifice528[i]);
                    }
                    if (!verifySeriesPressurePulseLPS(chart.Series["5"].Points.ToList(), flowRate))
                        chart.Series.Remove(chart.Series["5"]);
                    #endregion

                    #region Orifice Code 7
                    chart.Series.Add("7");
                    chart.Series["7"].ChartType = SeriesChartType.FastLine;

                    for (int i = 0; i < orifice533.Count(); i++)
                    {
                        if (orifice533[i] != 0)
                            chart.Series["7"].Points.AddXY(LPSXaxis[i], orifice533[i]);
                    }
                    if (!verifySeriesPressurePulseLPS(chart.Series["7"].Points.ToList(), flowRate))
                        chart.Series.Remove(chart.Series["7"]);
                    #endregion

                    #region Orifice Code 8
                    chart.Series.Add("8");
                    chart.Series["8"].ChartType = SeriesChartType.FastLine;

                    for (int i = 0; i < orifice538.Count(); i++)
                    {
                        if (orifice538[i] != 0)
                            chart.Series["8"].Points.AddXY(LPSXaxis[i], orifice538[i]);
                    }
                    if (!verifySeriesPressurePulseLPS(chart.Series["8"].Points.ToList(), flowRate))
                        chart.Series.Remove(chart.Series["8"]);
                    #endregion

                    if (chart.Series.Count == 0)
                    {
                        #region Orifice Code 1
                        chart.Series.Add("1");
                        chart.Series["1"].ChartType = SeriesChartType.FastLine;

                        for (int i = 0; i < orifice518.Count(); i++)
                        {
                            if (orifice518[i] != 0)
                                chart.Series["1"].Points.AddXY(LPSXaxis[i], orifice518[i]);
                        }

                        #endregion

                        #region Orifice Code 3
                        chart.Series.Add("3");
                        chart.Series["3"].ChartType = SeriesChartType.FastLine;

                        for (int i = 0; i < orifice523.Count(); i++)
                        {
                            if (orifice523[i] != 0)
                                chart.Series["3"].Points.AddXY(LPSXaxis[i], orifice523[i]);
                        }

                        #endregion

                        #region Orifice Code 5
                        chart.Series.Add("5");
                        chart.Series["5"].ChartType = SeriesChartType.FastLine;

                        for (int i = 0; i < orifice528.Count(); i++)
                        {
                            if (orifice528[i] != 0)
                                chart.Series["5"].Points.AddXY(LPSXaxis[i], orifice528[i]);
                        }

                        #endregion

                        #region Orifice Code 7
                        chart.Series.Add("7");
                        chart.Series["7"].ChartType = SeriesChartType.FastLine;

                        for (int i = 0; i < orifice533.Count(); i++)
                        {
                            if (orifice533[i] != 0)
                                chart.Series["7"].Points.AddXY(LPSXaxis[i], orifice533[i]);
                        }

                        #endregion

                        #region Orifice Code 8
                        chart.Series.Add("8");
                        chart.Series["8"].ChartType = SeriesChartType.FastLine;

                        for (int i = 0; i < orifice538.Count(); i++)
                        {
                            if (orifice538[i] != 0)
                                chart.Series["8"].Points.AddXY(LPSXaxis[i], orifice538[i]);
                        }

                        #endregion
                    }
                }

                #region Shade Chart
                double hrange = chart.ChartAreas[0].AxisY.Maximum - chart.ChartAreas[0].AxisY.Minimum;

                //Good Pulses
                StripLine sl = new StripLine();
                sl.Interval = hrange;         // no less than the range, so it won't repeat
                sl.StripWidth = 150 * psiToAtm; ;            // width
                sl.IntervalOffset = 150 * psiToAtm; ;  // x-position
                sl.BackColor = Color.LightGreen;
                chart.ChartAreas[0].AxisY.StripLines.Add(sl);

                //Okay Pulses
                sl = new StripLine();
                sl.Interval = hrange;         // no less than the range, so it won't repeat
                sl.StripWidth = 50 * psiToAtm; ;            // width
                sl.IntervalOffset = 100 * psiToAtm; ;  // x-position
                sl.BackColor = Color.PaleGoldenrod;
                chart.ChartAreas[0].AxisY.StripLines.Add(sl);

                sl = new StripLine();
                sl.Interval = hrange;         // no less than the range, so it won't repeat
                sl.StripWidth = 100 * psiToAtm; ;            // width
                sl.IntervalOffset = 300 * psiToAtm; ;  // x-position
                sl.BackColor = Color.PaleGoldenrod;
                chart.ChartAreas[0].AxisY.StripLines.Add(sl);

                //Bad Pulses
                sl = new StripLine();
                sl.Interval = hrange;         // no less than the range, so it won't repeat
                sl.StripWidth = 100 * psiToAtm; ;            // width
                sl.IntervalOffset = 0 * psiToAtm;    // x-position
                sl.BackColor = Color.Pink;
                chart.ChartAreas[0].AxisY.StripLines.Add(sl);

                sl = new StripLine();
                sl.Interval = hrange;         // no less than the range, so it won't repeat
                sl.StripWidth = 500 * psiToAtm; ;            // width
                sl.IntervalOffset = 400 * psiToAtm; ;  // x-position
                sl.BackColor = Color.Pink;
                chart.ChartAreas[0].AxisY.StripLines.Add(sl);
                #endregion
            }

            CalculateSourceToSurfacePressure();

            chart.ChartAreas[0].RecalculateAxesScale();

            chart.Invalidate();
        }

        private bool verifySeriesPressurePulse(List<DataPoint> seriesPointsList, int flowRate)
        {
            bool answer = false;

            for(int i = 0; i < seriesPointsList.Count; i++)
            {
                if(seriesPointsList[i].XValue >= flowRate - 25)
                {
                    if (seriesPointsList[i].XValue <= flowRate + 25)
                    {
                        if(seriesPointsList[i].YValues[0] >= 225 && seriesPointsList[i].YValues[0] <= 450)
                            answer = true;
                    }
                }
            }
            return answer;
        }

        private bool verifySeriesPressurePulseLPS(List<DataPoint> seriesPointsList, int flowRate)
        {
            bool answer = false;

            for (int i = 0; i < seriesPointsList.Count; i++)
            {
                if (seriesPointsList[i].XValue >= flowRate - 3)
                {
                    if (seriesPointsList[i].XValue <= flowRate + 3)
                    {
                        if (seriesPointsList[i].YValues[0] >= 10 && seriesPointsList[i].YValues[0] <= 25)
                            answer = true;
                    }
                }
            }
            return answer;
        }

        private void setButton_Click(object sender, EventArgs e)
        {
            pumpEfficiency = Convert.ToDouble(pumpEfficiencyTextbox.Text);
            modelTypical = Convert.ToDouble(modelTypicalTextbox.Text);
            modelOrifices = Convert.ToDouble(modelOrificesTextbox.Text);
            round = Convert.ToDouble(roundTextBox.Text);

            if (orificeMudWeightUnitsBox.Text == "PPG")
                mudWeight = Convert.ToSingle(mudWeightTextbox.Text) * 1;
            else if (orificeMudWeightUnitsBox.Text == "SG")
                mudWeight = Convert.ToSingle(mudWeightTextbox.Text) * 8.33;
            else if (orificeMudWeightUnitsBox.Text == "KG/M^3")
                mudWeight = Convert.ToSingle(mudWeightTextbox.Text) * 0.0083;
            else if (orificeMudWeightUnitsBox.Text == "LB/FT^3")
                mudWeight = Convert.ToSingle(mudWeightTextbox.Text) * 7.48;
            else if (orificeMudWeightUnitsBox.Text == "G/CM^3")
                mudWeight = Convert.ToSingle(mudWeightTextbox.Text) * 8.33;

            InitializePoppetTubestopOpenClosed();
            IntializePulseAmplitudePsiTable();
            InitializePulseAmplitudeAtmTable();
            updateChart();
        }

        private void CalculateSourceToSurfacePressure()
        {
            try
            {
                double depthTdMeters = 0;

                if (depthTdComboBox.Text == "METERS")
                {
                    depthTdMeters = Convert.ToDouble(depthForPressureBox.Text);
                }
                else
                {
                    depthTdMeters = Convert.ToDouble(depthForPressureBox.Text) * 0.3048;
                }
                double pressureAtSource = Convert.ToDouble(pressureAtSourceBox.Text);
                double pressureAtSurface = pressureAtSource * Math.Exp((-depthTdMeters) / 1524);
                pressureAtSurfaceBox.Text = String.Format("{0:0.000000}", pressureAtSurface);
                pressureAtSurfaceBox.BackColor = Color.Yellow;
            }
            catch(Exception ex)
            {

            }
        }

        private double calculateOrificeFlow(double mudweight, double GPM, double modelTypical, double CD, double A)
        {
            double orificeFlowRate = 0;

            CD = 1.153;

            orificeFlowRate = (mudWeight * Math.Pow(GPM, 2)) / (12032 * Math.Pow(CD, 2) * Math.Pow(A, 2));

            return orificeFlowRate;
        }

        private void defaultButton_Click(object sender, EventArgs e)
        {
            pumpEfficiencyTextbox.Text = "1";
            orificeToolSizeBox.Text = "4.75";
            modelTypicalTextbox.Text = "12034";
            modelOrificesTextbox.Text = " 16000";
            mudWeightTextbox.Text = "9";
            roundTextBox.Text = "1";
            orificeFlowRateTextbox.Text = "250";
            orificeMudWeightUnitsBox.Text = "PPG";
            orificeFlowRateUnitsBox.Text = "GPM";

            pumpEfficiency = Convert.ToInt32(pumpEfficiencyTextbox.Text);
            modelTypical = Convert.ToInt32(modelTypicalTextbox.Text);
            modelOrifices = Convert.ToInt32(modelOrificesTextbox.Text);
            mudWeight = Convert.ToInt32(mudWeightTextbox.Text);
            round = Convert.ToInt32(roundTextBox.Text);

            InitializePoppetTubestopOpenClosed();
            IntializePulseAmplitudePsiTable();
            InitializePulseAmplitudeAtmTable();
            updateChart();
        }

        public void IntializePulseAmplitudePsiTable()
        {
            psiGroupBox.Text = "Pulse Amplitude at Tool (psi), MW = " + String.Format("{0:0.0}", mudWeight) + " PPG";

            orifice188 = new double[14];
            orifice190 = new double[14];
            orifice192 = new double[14];
            orifice194 = new double[14];
            orifice196 = new double[14];
            orifice198 = new double[14];
            orifice200 = new double[14];
            orifice202 = new double[14];
            orifice204 = new double[14];
            orifice206 = new double[14];
            orifice208 = new double[14];
            orifice210 = new double[14];
            orifice212 = new double[14];

            pulseAmplitudePSITable.Rows.Clear();
            pulseAmplitudePSITable.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            pulseAmplitudePSITable.EnableHeadersVisualStyles = false;

            pulseAmplitudePSITable.RowHeadersVisible = false;
            pulseAmplitudePSITable.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;

            pulseAmplitudePSITable.ColumnCount = 14;

            string[] row;

            for (int i = 0; i < 9; i++)
            {
                pulseAmplitudePSITable.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            }

            pulseAmplitudePSITable.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            row = new string[] { "4 3/4 Code", "2", "4", "6", "7", "8" };
            pulseAmplitudePSITable.Rows.Add(row);
            pulseAmplitudePSITable.Rows[0].DefaultCellStyle.Font = new Font(pulseAmplitudePSITable.Font, FontStyle.Bold);

            pulseAmplitudePSITable.Rows[0].Cells[1].Style.BackColor = Color.Yellow;
            pulseAmplitudePSITable.Rows[0].Cells[2].Style.BackColor = Color.Yellow;
            pulseAmplitudePSITable.Rows[0].Cells[3].Style.BackColor = Color.Yellow;
            pulseAmplitudePSITable.Rows[0].Cells[4].Style.BackColor = Color.Yellow;

            row = new string[] { "6 3/4 Code", "", "", "", "", "7", "8", "9", "10", "11", "12" };
            pulseAmplitudePSITable.Rows.Add(row);
            pulseAmplitudePSITable.Rows[1].DefaultCellStyle.Font = new Font(pulseAmplitudePSITable.Font, FontStyle.Bold);

            pulseAmplitudePSITable.Rows[1].Cells[6].Style.BackColor = Color.Yellow;
            pulseAmplitudePSITable.Rows[1].Cells[8].Style.BackColor = Color.Yellow;
            pulseAmplitudePSITable.Rows[1].Cells[10].Style.BackColor = Color.Yellow;

            row = new string[] { "8 Code", "", "", "", "", "", "", "", "", "1", "3", "5", "7", "8" };
            pulseAmplitudePSITable.Rows.Add(row);
            pulseAmplitudePSITable.Rows[2].DefaultCellStyle.Font = new Font(pulseAmplitudePSITable.Font, FontStyle.Bold);

            pulseAmplitudePSITable.Rows[2].Cells[10].Style.BackColor = Color.Yellow;
            pulseAmplitudePSITable.Rows[2].Cells[12].Style.BackColor = Color.Yellow;
            pulseAmplitudePSITable.Rows[2].Cells[13].Style.BackColor = Color.Yellow;

            row = new string[] { "Orifice Size", "1.88", "1.90", "1.92", "1.94", "1.96", "1.98", "2.00", "2.02", "2.04", "2.06", "2.08", "2.10", "2.12" };
            pulseAmplitudePSITable.Rows.Add(row);
            pulseAmplitudePSITable.Rows[3].DefaultCellStyle.Font = new Font(pulseAmplitudePSITable.Font, FontStyle.Bold);

            #region 150 GPM
            double GPM = 150.0;

            GPM150 = new double[13];

            for (int i = 0; i < 13; i++)
            {
                GPM150[i] = roudUpMult5(Convert.ToInt32(round * ((((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight / (modelOrifices * areaClosed[i] * areaClosed[i])) -
                (((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * (areaOpen[i] * areaOpen[i])))) / round)));

                switch (i)
                {
                    case 0:
                        orifice188[0] = GPM150[i];
                        break;
                    case 1:
                        orifice190[0] = GPM150[i];
                        break;
                    case 2:
                        orifice192[0] = GPM150[i];
                        break;
                    case 3:
                        orifice194[0] = GPM150[i];
                        break;
                    case 4:
                        orifice196[0] = GPM150[i];
                        break;
                    case 5:
                        orifice198[0] = GPM150[i];
                        break;
                    case 6:
                        orifice200[0] = GPM150[i];
                        break;
                    case 7:
                        orifice202[0] = GPM150[i];
                        break;
                    case 8:
                        orifice204[0] = GPM150[i];
                        break;
                    case 9:
                        orifice206[0] = GPM150[i];
                        break;
                    case 10:
                        orifice208[0] = GPM150[i];
                        break;
                    case 11:
                        orifice210[0] = GPM150[i];
                        break;
                    case 12:
                        orifice212[0] = GPM150[i];
                        break;
                }
            }

            row = new string[14];
            row[0] = "150 GPM";

            for(int i = 0; i < 13; i++)
            {
                if (GPM150[i] >= 100 && GPM150[i] < 750)
                {
                    row[i + 1] = String.Format("{0:0}", GPM150[i]);
                }
                else
                    row[i + 1] = "";
            };
            pulseAmplitudePSITable.Rows.Add(row);

            for (int i = 0; i < 13; i++)
            {
                if (row[i + 1] != "")
                {
                    if (GPM150[i] >= 250 && GPM150[i] < 400)
                        pulseAmplitudePSITable.Rows[4].Cells[i + 1].Style.BackColor = Color.LightGreen;
                    else if (GPM150[i] >= 200 && GPM150[i] < 250)
                        pulseAmplitudePSITable.Rows[4].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (GPM150[i] >= 400 && GPM150[i] < 500)
                        pulseAmplitudePSITable.Rows[4].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (GPM150[i] >= 100 && GPM150[i] < 200)
                        pulseAmplitudePSITable.Rows[4].Cells[i + 1].Style.BackColor = Color.Pink;
                    else if (GPM150[i] >= 500)
                        pulseAmplitudePSITable.Rows[4].Cells[i + 1].Style.BackColor = Color.Pink;
                }
            }
            #endregion

            #region 200 GPM
            GPM = 200.0;

            GPM200 = new double[13];

            for (int i = 0; i < 13; i++)
            {
                GPM200[i] = roudUpMult5(Convert.ToInt32(round * (((((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * areaClosed[i] * areaClosed[i])) -
                (((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * (areaOpen[i] * areaOpen[i])))) / round)));


                switch (i)
                {
                    case 0:
                        orifice188[1] = GPM200[i];
                        break;
                    case 1:
                        orifice190[1] = GPM200[i];
                        break;
                    case 2:
                        orifice192[1] = GPM200[i];
                        break;
                    case 3:
                        orifice194[1] = GPM200[i];
                        break;
                    case 4:
                        orifice196[1] = GPM200[i];
                        break;
                    case 5:
                        orifice198[1] = GPM200[i];
                        break;
                    case 6:
                        orifice200[1] = GPM200[i];
                        break;
                    case 7:
                        orifice202[1] = GPM200[i];
                        break;
                    case 8:
                        orifice204[1] = GPM200[i];
                        break;
                    case 9:
                        orifice206[1] = GPM200[i];
                        break;
                    case 10:
                        orifice208[1] = GPM200[i];
                        break;
                    case 11:
                        orifice210[1] = GPM200[i];
                        break;
                    case 12:
                        orifice212[1] = GPM200[i];
                        break;
                }
            }

            row = new string[14];
            row[0] = "200 GPM";

            for (int i = 0; i < 13; i++)
            {
                if (GPM200[i] >= 100 && GPM200[i] < 750)
                {
                    row[i + 1] = String.Format("{0:0}", GPM200[i]);
                }
                else
                    row[i + 1] = "";
            };
            pulseAmplitudePSITable.Rows.Add(row);

            for (int i = 0; i < 13; i++)
            {
                if (row[i + 1] != "")
                {
                    if (GPM200[i] >= 250 && GPM200[i] < 400)
                        pulseAmplitudePSITable.Rows[5].Cells[i + 1].Style.BackColor = Color.LightGreen;
                    else if (GPM200[i] >= 200 && GPM200[i] < 250)
                        pulseAmplitudePSITable.Rows[5].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (GPM200[i] >= 400 && GPM200[i] < 500)
                        pulseAmplitudePSITable.Rows[5].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (GPM200[i] >= 100 && GPM200[i] < 200)
                        pulseAmplitudePSITable.Rows[5].Cells[i + 1].Style.BackColor = Color.Pink;
                    else if (GPM200[i] >= 500)
                        pulseAmplitudePSITable.Rows[5].Cells[i + 1].Style.BackColor = Color.Pink;
                }
            }
            #endregion

            #region 250 GPM
            GPM = 250.0;

            GPM250 = new double[13];

            for (int i = 0; i < 13; i++)
            {
                GPM250[i] = roudUpMult5(Convert.ToInt32(round * (((((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * areaClosed[i] * areaClosed[i])) -
                (((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * (areaOpen[i] * areaOpen[i])))) / round)));


                switch (i)
                {
                    case 0:
                        orifice188[2] = GPM250[i];
                        break;
                    case 1:
                        orifice190[2] = GPM250[i];
                        break;
                    case 2:
                        orifice192[2] = GPM250[i];
                        break;
                    case 3:
                        orifice194[2] = GPM250[i];
                        break;
                    case 4:
                        orifice196[2] = GPM250[i];
                        break;
                    case 5:
                        orifice198[2] = GPM250[i];
                        break;
                    case 6:
                        orifice200[2] = GPM250[i];
                        break;
                    case 7:
                        orifice202[2] = GPM250[i];
                        break;
                    case 8:
                        orifice204[2] = GPM250[i];
                        break;
                    case 9:
                        orifice206[2] = GPM250[i];
                        break;
                    case 10:
                        orifice208[2] = GPM250[i];
                        break;
                    case 11:
                        orifice210[2] = GPM250[i];
                        break;
                    case 12:
                        orifice212[2] = GPM250[i];
                        break;
                }
            }

            row = new string[14];
            row[0] = "250 GPM";

            for (int i = 0; i < 13; i++)
            {
                if (GPM250[i] >= 100 && GPM250[i] < 750)
                {
                    row[i + 1] = String.Format("{0:0}", GPM250[i]);
                }
                else
                    row[i + 1] = "";
            };
            pulseAmplitudePSITable.Rows.Add(row);

            for (int i = 0; i < 13; i++)
            {
                if (row[i + 1] != "")
                {
                    if (GPM250[i] >= 250 && GPM250[i] < 400)
                        pulseAmplitudePSITable.Rows[6].Cells[i + 1].Style.BackColor = Color.LightGreen;
                    else if (GPM250[i] >= 200 && GPM250[i] < 250)
                        pulseAmplitudePSITable.Rows[6].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (GPM250[i] >= 400 && GPM250[i] < 500)
                        pulseAmplitudePSITable.Rows[6].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (GPM250[i] >= 100 && GPM250[i] < 200)
                        pulseAmplitudePSITable.Rows[6].Cells[i + 1].Style.BackColor = Color.Pink;
                    else if (GPM250[i] >= 500)
                        pulseAmplitudePSITable.Rows[6].Cells[i + 1].Style.BackColor = Color.Pink;
                }
            }
            #endregion

            #region 300 GPM
            GPM = 300.0;

            GPM300 = new double[13];

            for (int i = 0; i < 13; i++)
            {
                GPM300[i] = roudUpMult5(Convert.ToInt32(round * (((((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * areaClosed[i] * areaClosed[i])) -
                (((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * (areaOpen[i] * areaOpen[i])))) / round)));


                switch (i)
                {
                    case 0:
                        orifice188[3] = GPM300[i];
                        break;
                    case 1:
                        orifice190[3] = GPM300[i];
                        break;
                    case 2:
                        orifice192[3] = GPM300[i];
                        break;
                    case 3:
                        orifice194[3] = GPM300[i];
                        break;
                    case 4:
                        orifice196[3] = GPM300[i];
                        break;
                    case 5:
                        orifice198[3] = GPM300[i];
                        break;
                    case 6:
                        orifice200[3] = GPM300[i];
                        break;
                    case 7:
                        orifice202[3] = GPM300[i];
                        break;
                    case 8:
                        orifice204[3] = GPM300[i];
                        break;
                    case 9:
                        orifice206[3] = GPM300[i];
                        break;
                    case 10:
                        orifice208[3] = GPM300[i];
                        break;
                    case 11:
                        orifice210[3] = GPM300[i];
                        break;
                    case 12:
                        orifice212[3] = GPM300[i];
                        break;
                }
            }

            row = new string[14];
            row[0] = "300 GPM";

            for (int i = 0; i < 13; i++)
            {
                if (GPM300[i] >= 100 && GPM300[i] < 750)
                {
                    row[i + 1] = String.Format("{0:0}", GPM300[i]);
                }
                else
                    row[i + 1] = "";
            };
            pulseAmplitudePSITable.Rows.Add(row);

            for (int i = 0; i < 13; i++)
            {
                if (row[i + 1] != "")
                {
                    if (GPM300[i] >= 250 && GPM300[i] < 400)
                        pulseAmplitudePSITable.Rows[7].Cells[i + 1].Style.BackColor = Color.LightGreen;
                    else if (GPM300[i] >= 200 && GPM300[i] < 250)
                        pulseAmplitudePSITable.Rows[7].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (GPM300[i] >= 400 && GPM300[i] < 500)
                        pulseAmplitudePSITable.Rows[7].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (GPM300[i] >= 100 && GPM300[i] < 200)
                        pulseAmplitudePSITable.Rows[7].Cells[i + 1].Style.BackColor = Color.Pink;
                    else if (GPM300[i] >= 500)
                        pulseAmplitudePSITable.Rows[7].Cells[i + 1].Style.BackColor = Color.Pink;
                }
            }
            #endregion

            #region 350 GPM
            GPM = 350.0;

            GPM350 = new double[13];

            for (int i = 0; i < 13; i++)
            {
                GPM350[i] = roudUpMult5(Convert.ToInt32(round * (((((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * areaClosed[i] * areaClosed[i])) -
                (((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * (areaOpen[i] * areaOpen[i])))) / round)));


                switch (i)
                {
                    case 0:
                        orifice188[4] = GPM350[i];
                        break;
                    case 1:
                        orifice190[4] = GPM350[i];
                        break;
                    case 2:
                        orifice192[4] = GPM350[i];
                        break;
                    case 3:
                        orifice194[4] = GPM350[i];
                        break;
                    case 4:
                        orifice196[4] = GPM350[i];
                        break;
                    case 5:
                        orifice198[4] = GPM350[i];
                        break;
                    case 6:
                        orifice200[4] = GPM350[i];
                        break;
                    case 7:
                        orifice202[4] = GPM350[i];
                        break;
                    case 8:
                        orifice204[4] = GPM350[i];
                        break;
                    case 9:
                        orifice206[4] = GPM350[i];
                        break;
                    case 10:
                        orifice208[4] = GPM350[i];
                        break;
                    case 11:
                        orifice210[4] = GPM350[i];
                        break;
                    case 12:
                        orifice212[4] = GPM350[i];
                        break;
                }
            }

            row = new string[14];
            row[0] = "350 GPM";

            for (int i = 0; i < 13; i++)
            {
                if (GPM350[i] >= 100 && GPM350[i] < 750)
                {
                    row[i + 1] = String.Format("{0:0}", GPM350[i]);
                }
                else
                    row[i + 1] = "";
            };
            pulseAmplitudePSITable.Rows.Add(row);

            for (int i = 0; i < 13; i++)
            {
                if (row[i + 1] != "")
                {
                    if (GPM350[i] >= 250 && GPM350[i] < 400)
                        pulseAmplitudePSITable.Rows[8].Cells[i + 1].Style.BackColor = Color.LightGreen;
                    else if (GPM350[i] >= 200 && GPM350[i] < 250)
                        pulseAmplitudePSITable.Rows[8].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (GPM350[i] >= 400 && GPM350[i] < 500)
                        pulseAmplitudePSITable.Rows[8].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (GPM350[i] >= 100 && GPM350[i] < 200)
                        pulseAmplitudePSITable.Rows[8].Cells[i + 1].Style.BackColor = Color.Pink;
                    else if (GPM350[i] >= 500)
                        pulseAmplitudePSITable.Rows[8].Cells[i + 1].Style.BackColor = Color.Pink;
                }
            }
            #endregion

            #region 400 GPM
            GPM = 400.0;

            GPM400 = new double[13];

            for (int i = 0; i < 13; i++)
            {
                GPM400[i] = roudUpMult5(Convert.ToInt32(round * (((((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * areaClosed[i] * areaClosed[i])) -
                (((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * (areaOpen[i] * areaOpen[i])))) / round)));

                switch (i)
                {
                    case 0:
                        orifice188[5] = GPM400[i];
                        break;
                    case 1:
                        orifice190[5] = GPM400[i];
                        break;
                    case 2:
                        orifice192[5] = GPM400[i];
                        break;
                    case 3:
                        orifice194[5] = GPM400[i];
                        break;
                    case 4:
                        orifice196[5] = GPM400[i];
                        break;
                    case 5:
                        orifice198[5] = GPM400[i];
                        break;
                    case 6:
                        orifice200[5] = GPM400[i];
                        break;
                    case 7:
                        orifice202[5] = GPM400[i];
                        break;
                    case 8:
                        orifice204[5] = GPM400[i];
                        break;
                    case 9:
                        orifice206[5] = GPM400[i];
                        break;
                    case 10:
                        orifice208[5] = GPM400[i];
                        break;
                    case 11:
                        orifice210[5] = GPM400[i];
                        break;
                    case 12:
                        orifice212[5] = GPM400[i];
                        break;
                }
            }

            row = new string[14];
            row[0] = "400 GPM";

            for (int i = 0; i < 13; i++)
            {
                if (GPM400[i] >= 100 && GPM400[i] < 750)
                {
                    row[i + 1] = String.Format("{0:0}", GPM400[i]);
                }
                else
                    row[i + 1] = "";
            }
            pulseAmplitudePSITable.Rows.Add(row);

            for (int i = 0; i < 13; i++)
            {
                if (row[i + 1] != "")
                {
                    if (GPM400[i] >= 250 && GPM400[i] < 400)
                        pulseAmplitudePSITable.Rows[9].Cells[i + 1].Style.BackColor = Color.LightGreen;
                    else if (GPM400[i] >= 200 && GPM400[i] < 250)
                        pulseAmplitudePSITable.Rows[9].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (GPM400[i] >= 400 && GPM400[i] < 500)
                        pulseAmplitudePSITable.Rows[9].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (GPM400[i] >= 100 && GPM400[i] < 200)
                        pulseAmplitudePSITable.Rows[9].Cells[i + 1].Style.BackColor = Color.Pink;
                    else if (GPM400[i] >= 500)
                        pulseAmplitudePSITable.Rows[9].Cells[i + 1].Style.BackColor = Color.Pink;
                }
            }
            #endregion

            #region 450 GPM
            GPM = 450.0;

            GPM450 = new double[13];

            for (int i = 0; i < 13; i++)
            {
                GPM450[i] = roudUpMult5(Convert.ToInt32(round * (((((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * areaClosed[i] * areaClosed[i])) -
                (((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * (areaOpen[i] * areaOpen[i])))) / round)));

                switch (i)
                {
                    case 0:
                        orifice188[6] = GPM450[i];
                        break;
                    case 1:
                        orifice190[6] = GPM450[i];
                        break;
                    case 2:
                        orifice192[6] = GPM450[i];
                        break;
                    case 3:
                        orifice194[6] = GPM450[i];
                        break;
                    case 4:
                        orifice196[6] = GPM450[i];
                        break;
                    case 5:
                        orifice198[6] = GPM450[i];
                        break;
                    case 6:
                        orifice200[6] = GPM450[i];
                        break;
                    case 7:
                        orifice202[6] = GPM450[i];
                        break;
                    case 8:
                        orifice204[6] = GPM450[i];
                        break;
                    case 9:
                        orifice206[6] = GPM450[i];
                        break;
                    case 10:
                        orifice208[6] = GPM450[i];
                        break;
                    case 11:
                        orifice210[6] = GPM450[i];
                        break;
                    case 12:
                        orifice212[6] = GPM450[i];
                        break;
                }
            }

            row = new string[14];
            row[0] = "450 GPM";

            for (int i = 0; i < 13; i++)
            {
                if (GPM450[i] >= 100 && GPM450[i] < 750)
                {
                    row[i + 1] = String.Format("{0:0}", GPM450[i]);
                }
                else
                    row[i + 1] = "";
            };
            pulseAmplitudePSITable.Rows.Add(row);

            for (int i = 0; i < 13; i++)
            {
                if (row[i + 1] != "")
                {
                    if (GPM450[i] >= 250 && GPM450[i] < 400)
                        pulseAmplitudePSITable.Rows[10].Cells[i + 1].Style.BackColor = Color.LightGreen;
                    else if (GPM450[i] >= 200 && GPM450[i] < 250)
                        pulseAmplitudePSITable.Rows[10].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (GPM450[i] >= 400 && GPM450[i] < 500)
                        pulseAmplitudePSITable.Rows[10].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (GPM450[i] >= 100 && GPM450[i] < 200)
                        pulseAmplitudePSITable.Rows[10].Cells[i + 1].Style.BackColor = Color.Pink;
                    else if (GPM450[i] >= 500)
                        pulseAmplitudePSITable.Rows[10].Cells[i + 1].Style.BackColor = Color.Pink;
                }
            }
            #endregion

            #region 500 GPM
            GPM = 500.0;

            GPM500 = new double[13];

            for (int i = 0; i < 13; i++)
            {
                GPM500[i] = roudUpMult5(Convert.ToInt32(round * (((((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * areaClosed[i] * areaClosed[i])) -
                (((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * (areaOpen[i] * areaOpen[i])))) / round)));

                switch (i)
                {
                    case 0:
                        orifice188[7] = GPM500[i];
                        break;
                    case 1:
                        orifice190[7] = GPM500[i];
                        break;
                    case 2:
                        orifice192[7] = GPM500[i];
                        break;
                    case 3:
                        orifice194[7] = GPM500[i];
                        break;
                    case 4:
                        orifice196[7] = GPM500[i];
                        break;
                    case 5:
                        orifice198[7] = GPM500[i];
                        break;
                    case 6:
                        orifice200[7] = GPM500[i];
                        break;
                    case 7:
                        orifice202[7] = GPM500[i];
                        break;
                    case 8:
                        orifice204[7] = GPM500[i];
                        break;
                    case 9:
                        orifice206[7] = GPM500[i];
                        break;
                    case 10:
                        orifice208[7] = GPM500[i];
                        break;
                    case 11:
                        orifice210[7] = GPM500[i];
                        break;
                    case 12:
                        orifice212[7] = GPM500[i];
                        break;
                }
            }

            row = new string[14];
            row[0] = "500 GPM";

            for (int i = 0; i < 13; i++)
            {
                if (GPM500[i] >= 100 && GPM500[i] < 750)
                {
                    row[i + 1] = String.Format("{0:0}", GPM500[i]);
                }
                else
                    row[i + 1] = "";
            };
            pulseAmplitudePSITable.Rows.Add(row);

            for (int i = 0; i < 13; i++)
            {
                if (row[i + 1] != "")
                {
                    if (GPM500[i] >= 250 && GPM500[i] < 400)
                        pulseAmplitudePSITable.Rows[11].Cells[i + 1].Style.BackColor = Color.LightGreen;
                    else if (GPM500[i] >= 200 && GPM500[i] < 250)
                        pulseAmplitudePSITable.Rows[11].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (GPM500[i] >= 400 && GPM500[i] < 500)
                        pulseAmplitudePSITable.Rows[11].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (GPM500[i] >= 100 && GPM500[i] < 200)
                        pulseAmplitudePSITable.Rows[11].Cells[i + 1].Style.BackColor = Color.Pink;
                    else if (GPM500[i] >= 500)
                        pulseAmplitudePSITable.Rows[11].Cells[i + 1].Style.BackColor = Color.Pink;
                }
            }
            #endregion

            #region 550 GPM
            GPM = 550.0;

            GPM550 = new double[13];

            for (int i = 0; i < 13; i++)
            {
                GPM550[i] = roudUpMult5(Convert.ToInt32(round * (((((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * areaClosed[i] * areaClosed[i])) -
                (((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * (areaOpen[i] * areaOpen[i])))) / round)));

                switch (i)
                {
                    case 0:
                        orifice188[8] = GPM550[i];
                        break;
                    case 1:
                        orifice190[8] = GPM550[i];
                        break;
                    case 2:
                        orifice192[8] = GPM550[i];
                        break;
                    case 3:
                        orifice194[8] = GPM550[i];
                        break;
                    case 4:
                        orifice196[8] = GPM550[i];
                        break;
                    case 5:
                        orifice198[8] = GPM550[i];
                        break;
                    case 6:
                        orifice200[8] = GPM550[i];
                        break;
                    case 7:
                        orifice202[8] = GPM550[i];
                        break;
                    case 8:
                        orifice204[8] = GPM550[i];
                        break;
                    case 9:
                        orifice206[8] = GPM550[i];
                        break;
                    case 10:
                        orifice208[8] = GPM550[i];
                        break;
                    case 11:
                        orifice210[8] = GPM550[i];
                        break;
                    case 12:
                        orifice212[8] = GPM550[i];
                        break;
                }
            }

            row = new string[14];
            row[0] = "550 GPM";

            for (int i = 0; i < 13; i++)
            {
                if (GPM550[i] >= 100 && GPM550[i] < 750)
                { 
                    row[i + 1] = String.Format("{0:0}", GPM550[i]);
                }
                else
                    row[i + 1] = "";
            };
            pulseAmplitudePSITable.Rows.Add(row);

            for (int i = 0; i < 13; i++)
            {
                if (row[i + 1] != "")
                {
                    if (GPM550[i] >= 250 && GPM550[i] < 400)
                        pulseAmplitudePSITable.Rows[12].Cells[i + 1].Style.BackColor = Color.LightGreen;
                    else if (GPM550[i] >= 200 && GPM550[i] < 250)
                        pulseAmplitudePSITable.Rows[12].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (GPM550[i] >= 400 && GPM550[i] < 500)
                        pulseAmplitudePSITable.Rows[12].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (GPM550[i] >= 100 && GPM550[i] < 200)
                        pulseAmplitudePSITable.Rows[12].Cells[i + 1].Style.BackColor = Color.Pink;
                    else if (GPM550[i] >= 500)
                        pulseAmplitudePSITable.Rows[12].Cells[i + 1].Style.BackColor = Color.Pink;
                }
            }
            #endregion

            #region 600 GPM
            GPM = 600.0;

            GPM600 = new double[13];

            for (int i = 0; i < 13; i++)
            {
                GPM600[i] = roudUpMult5(Convert.ToInt32(round * (((((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * areaClosed[i] * areaClosed[i])) -
                (((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * (areaOpen[i] * areaOpen[i])))) / round)));

                switch (i)
                {
                    case 0:
                        orifice188[9] = GPM600[i];
                        break;
                    case 1:
                        orifice190[9] = GPM600[i];
                        break;
                    case 2:
                        orifice192[9] = GPM600[i];
                        break;
                    case 3:
                        orifice194[9] = GPM600[i];
                        break;
                    case 4:
                        orifice196[9] = GPM600[i];
                        break;
                    case 5:
                        orifice198[9] = GPM600[i];
                        break;
                    case 6:
                        orifice200[9] = GPM600[i];
                        break;
                    case 7:
                        orifice202[9] = GPM600[i];
                        break;
                    case 8:
                        orifice204[9] = GPM600[i];
                        break;
                    case 9:
                        orifice206[9] = GPM600[i];
                        break;
                    case 10:
                        orifice208[9] = GPM600[i];
                        break;
                    case 11:
                        orifice210[9] = GPM600[i];
                        break;
                    case 12:
                        orifice212[9] = GPM600[i];
                        break;
                }
            }

            row = new string[14];
            row[0] = "600 GPM";

            for (int i = 0; i < 13; i++)
            {
                if (GPM600[i] >= 100 && GPM600[i] < 750)
                {
                    row[i + 1] = String.Format("{0:0}", GPM600[i]);
                }
                else
                    row[i + 1] = "";
            };
            pulseAmplitudePSITable.Rows.Add(row);

            for (int i = 0; i < 13; i++)
            {
                if (row[i + 1] != "")
                {
                    if (GPM600[i] >= 250 && GPM600[i] < 400)
                        pulseAmplitudePSITable.Rows[13].Cells[i + 1].Style.BackColor = Color.LightGreen;
                    else if (GPM600[i] >= 200 && GPM600[i] < 250)
                        pulseAmplitudePSITable.Rows[13].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (GPM600[i] >= 400 && GPM600[i] < 500)
                        pulseAmplitudePSITable.Rows[13].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (GPM600[i] >= 100 && GPM600[i] < 200)
                        pulseAmplitudePSITable.Rows[13].Cells[i + 1].Style.BackColor = Color.Pink;
                    else if (GPM600[i] >= 500)
                        pulseAmplitudePSITable.Rows[13].Cells[i + 1].Style.BackColor = Color.Pink;
                }
            }
            #endregion

            #region 700 GPM
            GPM = 700.0;

            GPM700 = new double[13];

            for (int i = 0; i < 13; i++)
            {
                GPM700[i] = roudUpMult5(Convert.ToInt32(round * (((((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * areaClosed[i] * areaClosed[i])) -
                (((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * (areaOpen[i] * areaOpen[i])))) / round)));

                switch (i)
                {
                    case 0:
                        orifice188[10] = GPM700[i];
                        break;
                    case 1:
                        orifice190[10] = GPM700[i];
                        break;
                    case 2:
                        orifice192[10] = GPM700[i];
                        break;
                    case 3:
                        orifice194[10] = GPM700[i];
                        break;
                    case 4:
                        orifice196[10] = GPM700[i];
                        break;
                    case 5:
                        orifice198[10] = GPM700[i];
                        break;
                    case 6:
                        orifice200[10] = GPM700[i];
                        break;
                    case 7:
                        orifice202[10] = GPM700[i];
                        break;
                    case 8:
                        orifice204[10] = GPM700[i];
                        break;
                    case 9:
                        orifice206[10] = GPM700[i];
                        break;
                    case 10:
                        orifice208[10] = GPM700[i];
                        break;
                    case 11:
                        orifice210[10] = GPM700[i];
                        break;
                    case 12:
                        orifice212[10] = GPM700[i];
                        break;
                }
            }

            row = new string[14];
            row[0] = "700 GPM";

            for (int i = 0; i < 13; i++)
            {
                if (GPM700[i] >= 100 && GPM700[i] < 750)
                {
                    row[i + 1] = String.Format("{0:0}", GPM700[i]);
                }
                else
                    row[i + 1] = "";
            };
            pulseAmplitudePSITable.Rows.Add(row);

            for (int i = 0; i < 13; i++)
            {
                if (row[i + 1] != "")
                {
                    if (GPM700[i] >= 250 && GPM700[i] < 400)
                        pulseAmplitudePSITable.Rows[14].Cells[i + 1].Style.BackColor = Color.LightGreen;
                    else if (GPM700[i] >= 200 && GPM700[i] < 250)
                        pulseAmplitudePSITable.Rows[14].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (GPM700[i] >= 400 && GPM700[i] < 500)
                        pulseAmplitudePSITable.Rows[14].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (GPM700[i] >= 100 && GPM700[i] < 200)
                        pulseAmplitudePSITable.Rows[14].Cells[i + 1].Style.BackColor = Color.Pink;
                    else if (GPM700[i] >= 500)
                        pulseAmplitudePSITable.Rows[14].Cells[i + 1].Style.BackColor = Color.Pink;
                }
            }
            #endregion

            #region 800 GPM
            GPM = 800.0;

            GPM800 = new double[13];

            for (int i = 0; i < 13; i++)
            {
                GPM800[i] = roudUpMult5(Convert.ToInt32(round * (((((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * areaClosed[i] * areaClosed[i])) -
                (((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * (areaOpen[i] * areaOpen[i])))) / round)));

                switch (i)
                {
                    case 0:
                        orifice188[11] = GPM800[i];
                        break;
                    case 1:
                        orifice190[11] = GPM800[i];
                        break;
                    case 2:
                        orifice192[11] = GPM800[i];
                        break;
                    case 3:
                        orifice194[11] = GPM800[i];
                        break;
                    case 4:
                        orifice196[11] = GPM800[i];
                        break;
                    case 5:
                        orifice198[11] = GPM800[i];
                        break;
                    case 6:
                        orifice200[11] = GPM800[i];
                        break;
                    case 7:
                        orifice202[11] = GPM800[i];
                        break;
                    case 8:
                        orifice204[11] = GPM800[i];
                        break;
                    case 9:
                        orifice206[11] = GPM800[i];
                        break;
                    case 10:
                        orifice208[11] = GPM800[i];
                        break;
                    case 11:
                        orifice210[11] = GPM800[i];
                        break;
                    case 12:
                        orifice212[11] = GPM800[i];
                        break;
                }
            }

            row = new string[14];
            row[0] = "800 GPM";

            for (int i = 0; i < 13; i++)
            {
                if (GPM800[i] >= 100 && GPM800[i] < 750)
                {
                    row[i + 1] = String.Format("{0:0}", GPM800[i]);
                }
                else
                    row[i + 1] = "";
            };
            pulseAmplitudePSITable.Rows.Add(row);

            for (int i = 0; i < 13; i++)
            {
                if (row[i + 1] != "")
                {
                    if (GPM800[i] >= 250 && GPM800[i] < 400)
                        pulseAmplitudePSITable.Rows[15].Cells[i + 1].Style.BackColor = Color.LightGreen;
                    else if (GPM800[i] >= 200 && GPM800[i] < 250)
                        pulseAmplitudePSITable.Rows[15].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (GPM800[i] >= 400 && GPM800[i] < 500)
                        pulseAmplitudePSITable.Rows[15].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (GPM800[i] >= 100 && GPM800[i] < 200)
                        pulseAmplitudePSITable.Rows[15].Cells[i + 1].Style.BackColor = Color.Pink;
                    else if (GPM800[i] >= 500)
                        pulseAmplitudePSITable.Rows[15].Cells[i + 1].Style.BackColor = Color.Pink;
                }
            }
            #endregion

            #region 900 GPM
            GPM = 900.0;

            GPM900 = new double[13];

            for (int i = 0; i < 13; i++)
            {
                GPM900[i] = roudUpMult5(Convert.ToInt32(round * (((((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * areaClosed[i] * areaClosed[i])) -
                (((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * (areaOpen[i] * areaOpen[i])))) / round)));

                switch (i)
                {
                    case 0:
                        orifice188[12] = GPM900[i];
                        break;
                    case 1:
                        orifice190[12] = GPM900[i];
                        break;
                    case 2:
                        orifice192[12] = GPM900[i];
                        break;
                    case 3:
                        orifice194[12] = GPM900[i];
                        break;
                    case 4:
                        orifice196[12] = GPM900[i];
                        break;
                    case 5:
                        orifice198[12] = GPM900[i];
                        break;
                    case 6:
                        orifice200[12] = GPM900[i];
                        break;
                    case 7:
                        orifice202[12] = GPM900[i];
                        break;
                    case 8:
                        orifice204[12] = GPM900[i];
                        break;
                    case 9:
                        orifice206[12] = GPM900[i];
                        break;
                    case 10:
                        orifice208[12] = GPM900[i];
                        break;
                    case 11:
                        orifice210[12] = GPM900[i];
                        break;
                    case 12:
                        orifice212[12] = GPM900[i];
                        break;
                }
            }

            row = new string[14];
            row[0] = "900 GPM";

            for (int i = 0; i < 13; i++)
            {
                if (GPM900[i] >= 100 && GPM900[i] < 750)
                {
                    row[i + 1] = String.Format("{0:0}", GPM900[i]);
                }
                else
                    row[i + 1] = "";
            };
            pulseAmplitudePSITable.Rows.Add(row);

            for (int i = 0; i < 13; i++)
            {
                if (row[i + 1] != "")
                {
                    if (GPM900[i] >= 250 && GPM900[i] < 400)
                        pulseAmplitudePSITable.Rows[16].Cells[i + 1].Style.BackColor = Color.LightGreen;
                    else if (GPM900[i] >= 200 && GPM900[i] < 250)
                        pulseAmplitudePSITable.Rows[16].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (GPM900[i] >= 400 && GPM900[i] < 500)
                        pulseAmplitudePSITable.Rows[16].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (GPM900[i] >= 100 && GPM900[i] < 200)
                        pulseAmplitudePSITable.Rows[16].Cells[i + 1].Style.BackColor = Color.Pink;
                    else if (GPM900[i] >= 500)
                        pulseAmplitudePSITable.Rows[16].Cells[i + 1].Style.BackColor = Color.Pink;
                }
            }
            #endregion

            #region 1000 GPM
            GPM = 1000.0;

            GPM1000 = new double[13];

            for (int i = 0; i < 13; i++)
            {
                GPM1000[i] = roudUpMult5(Convert.ToInt32(round * (((((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * areaClosed[i] * areaClosed[i])) -
                (((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * (areaOpen[i] * areaOpen[i])))) / round)));

                switch (i)
                {
                    case 0:
                        orifice188[13] = GPM1000[i];
                        break;
                    case 1:
                        orifice190[13] = GPM1000[i];
                        break;
                    case 2:
                        orifice192[13] = GPM1000[i];
                        break;
                    case 3:
                        orifice194[13] = GPM1000[i];
                        break;
                    case 4:
                        orifice196[13] = GPM1000[i];
                        break;
                    case 5:
                        orifice198[13] = GPM1000[i];
                        break;
                    case 6:
                        orifice200[13] = GPM1000[i];
                        break;
                    case 7:
                        orifice202[13] = GPM1000[i];
                        break;
                    case 8:
                        orifice204[13] = GPM1000[i];
                        break;
                    case 9:
                        orifice206[13] = GPM1000[i];
                        break;
                    case 10:
                        orifice208[13] = GPM1000[i];
                        break;
                    case 11:
                        orifice210[13] = GPM1000[i];
                        break;
                    case 12:
                        orifice212[13] = GPM1000[i];
                        break;
                }
            }

            row = new string[14];
            row[0] = "1000 GPM";

            for (int i = 0; i < 13; i++)
            {
                if (GPM1000[i] >= 100 && GPM1000[i] < 750)
                {
                    row[i + 1] = String.Format("{0:0}", GPM1000[i]);
                }
                else
                    row[i + 1] = "";
            };
            pulseAmplitudePSITable.Rows.Add(row);

            for (int i = 0; i < 13; i++)
            {
                if (row[i + 1] != "")
                {
                    if (GPM1000[i] >= 250 && GPM1000[i] < 400)
                        pulseAmplitudePSITable.Rows[17].Cells[i + 1].Style.BackColor = Color.LightGreen;
                    else if (GPM1000[i] >= 200 && GPM1000[i] < 250)
                        pulseAmplitudePSITable.Rows[17].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (GPM1000[i] >= 400 && GPM1000[i] < 500)
                        pulseAmplitudePSITable.Rows[17].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (GPM1000[i] >= 100 && GPM1000[i] < 200)
                        pulseAmplitudePSITable.Rows[17].Cells[i + 1].Style.BackColor = Color.Pink;
                    else if (GPM1000[i] >= 500)
                        pulseAmplitudePSITable.Rows[17].Cells[i + 1].Style.BackColor = Color.Pink;
                }
            }
            #endregion

            pulseAmplitudePSITable.Columns[0].DefaultCellStyle.Font = new Font(pulseAmplitudePSITable.Font, FontStyle.Bold);

            pulseAmplitudePSITable.AutoResizeColumns();
        }

        public void InitializePulseAmplitudeAtmTable()
        {
            orifice478 = new double[14];
            orifice483 = new double[14];
            orifice488 = new double[14];
            orifice493 = new double[14];
            orifice498 = new double[14];
            orifice503 = new double[14];
            orifice508 = new double[14];
            orifice513 = new double[14];
            orifice518 = new double[14];
            orifice523 = new double[14];
            orifice528 = new double[14];
            orifice533 = new double[14];
            orifice538 = new double[14];

            atmGroupBox.Text = "Pulse Amplitude at Tool (psi), MW = " + String.Format("{0:0.0}", mudWeight/8.34) + " SG";

            pulseAmplitudeAtmTable.Rows.Clear();
            pulseAmplitudeAtmTable.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            pulseAmplitudeAtmTable.EnableHeadersVisualStyles = false;

            pulseAmplitudeAtmTable.RowHeadersVisible = false;
            pulseAmplitudeAtmTable.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;

            pulseAmplitudeAtmTable.ColumnCount = 14;

            string[] row;

            for (int i = 0; i < 9; i++)
            {
                pulseAmplitudeAtmTable.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            }

            pulseAmplitudeAtmTable.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            row = new string[] { "4 3/4 Code", "2", "4", "6", "7", "8" };
            pulseAmplitudeAtmTable.Rows.Add(row);
            pulseAmplitudeAtmTable.Rows[0].DefaultCellStyle.Font = new Font(pulseAmplitudePSITable.Font, FontStyle.Bold);

            pulseAmplitudeAtmTable.Rows[0].Cells[1].Style.BackColor = Color.Yellow;
            pulseAmplitudeAtmTable.Rows[0].Cells[2].Style.BackColor = Color.Yellow;
            pulseAmplitudeAtmTable.Rows[0].Cells[3].Style.BackColor = Color.Yellow;
            pulseAmplitudeAtmTable.Rows[0].Cells[4].Style.BackColor = Color.Yellow;

            row = new string[] { "6 3/4 Code", "", "", "", "", "7", "8", "9", "10", "11", "12" };
            pulseAmplitudeAtmTable.Rows.Add(row);
            pulseAmplitudeAtmTable.Rows[1].DefaultCellStyle.Font = new Font(pulseAmplitudePSITable.Font, FontStyle.Bold);

            pulseAmplitudeAtmTable.Rows[1].Cells[6].Style.BackColor = Color.Yellow;
            pulseAmplitudeAtmTable.Rows[1].Cells[8].Style.BackColor = Color.Yellow;
            pulseAmplitudeAtmTable.Rows[1].Cells[10].Style.BackColor = Color.Yellow;

            row = new string[] { "8 Code", "", "", "", "", "", "", "", "", "1", "3", "5", "7", "8" };
            pulseAmplitudeAtmTable.Rows.Add(row);
            pulseAmplitudeAtmTable.Rows[2].DefaultCellStyle.Font = new Font(pulseAmplitudePSITable.Font, FontStyle.Bold);

            pulseAmplitudeAtmTable.Rows[2].Cells[10].Style.BackColor = Color.Yellow;
            pulseAmplitudeAtmTable.Rows[2].Cells[12].Style.BackColor = Color.Yellow;
            pulseAmplitudeAtmTable.Rows[2].Cells[13].Style.BackColor = Color.Yellow;

            row = new string[] { "Orifice Size", "47.8", "48.3", "48.8", "49.3", "49.8", "50.3", "50.8", "51.3", "51.8", "52.3", "52.8", "53.3", "53.8" };
            pulseAmplitudeAtmTable.Rows.Add(row);
            pulseAmplitudeAtmTable.Rows[3].DefaultCellStyle.Font = new Font(pulseAmplitudePSITable.Font, FontStyle.Bold);

            #region 9 L/S
            double GPM = 150.0;

            LS9 = new int[13];

            for (int i = 0; i < 13; i++)
            {
                LS9[i] = Convert.ToInt32(roudUpMult5(Convert.ToInt32(round * (((((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * areaClosed[i] * areaClosed[i])) -
                (((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * (areaOpen[i] * areaOpen[i])))) / round) * psiToAtm)));

                switch (i)
                {
                    case 0:
                        orifice478[0] = LS9[i];
                        break;
                    case 1:
                        orifice483[0] = LS9[i];
                        break;
                    case 2:
                        orifice488[0] = LS9[i];
                        break;
                    case 3:
                        orifice493[0] = LS9[i];
                        break;
                    case 4:
                        orifice498[0] = LS9[i];
                        break;
                    case 5:
                        orifice503[0] = LS9[i];
                        break;
                    case 6:
                        orifice508[0] = LS9[i];
                        break;
                    case 7:
                        orifice513[0] = LS9[i];
                        break;
                    case 8:
                        orifice518[0] = LS9[i];
                        break;
                    case 9:
                        orifice523[0] = LS9[i];
                        break;
                    case 10:
                        orifice528[0] = LS9[i];
                        break;
                    case 11:
                        orifice533[0] = LS9[i];
                        break;
                    case 12:
                        orifice538[0] = LS9[i];
                        break;
                }
            }

            row = new string[14];
            row[0] = "9 L/S";

            for (int i = 0; i < 13; i++)
            {
                if (LS9[i] >= (100.0 * psiToAtm) && LS9[i] < (750.0 * psiToAtm))
                    row[i + 1] = String.Format("{0:0}", LS9[i]);
                else
                    row[i + 1] = "";
            };
            pulseAmplitudeAtmTable.Rows.Add(row);

            for (int i = 0; i < 13; i++)
            {
                if (row[i + 1] != "")
                {
                    if (LS9[i] >= (250.0 * psiToAtm) && LS9[i] < (400.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[4].Cells[i + 1].Style.BackColor = Color.LightGreen;
                    else if (LS9[i] >= (200.0 * psiToAtm) && LS9[i] < (250.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[4].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (LS9[i] >= (400.0 *psiToAtm) && LS9[i] < (500.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[4].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (LS9[i] >= (100.0 * psiToAtm) && LS9[i] < (200.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[4].Cells[i + 1].Style.BackColor = Color.Pink;
                    else if (LS9[i] >= (500.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[4].Cells[i + 1].Style.BackColor = Color.Pink;
                }
            }
            #endregion

            #region 13 L/S
            GPM = 200.0;

            LS13 = new int[13];

            for (int i = 0; i < 13; i++)
            {
                LS13[i] = Convert.ToInt32(roudUpMult5(Convert.ToInt32(round * (((((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * areaClosed[i] * areaClosed[i])) -
                (((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * (areaOpen[i] * areaOpen[i])))) / round) * psiToAtm)));

                switch (i)
                {
                    case 0:
                        orifice478[1] = LS13[i];
                        break;
                    case 1:
                        orifice483[1] = LS13[i];
                        break;
                    case 2:
                        orifice488[1] = LS13[i];
                        break;
                    case 3:
                        orifice493[1] = LS13[i];
                        break;
                    case 4:
                        orifice498[1] = LS13[i];
                        break;
                    case 5:
                        orifice503[1] = LS13[i];
                        break;
                    case 6:
                        orifice508[1] = LS13[i];
                        break;
                    case 7:
                        orifice513[1] = LS13[i];
                        break;
                    case 8:
                        orifice518[1] = LS13[i];
                        break;
                    case 9:
                        orifice523[1] = LS13[i];
                        break;
                    case 10:
                        orifice528[1] = LS13[i];
                        break;
                    case 11:
                        orifice533[1] = LS13[i];
                        break;
                    case 12:
                        orifice538[1] = LS13[i];
                        break;
                }
            }

            row = new string[14];
            row[0] = "13 L/S";

            for (int i = 0; i < 13; i++)
            {
                if (LS13[i] >= (100.0 * psiToAtm) && LS13[i] < (750.0 * psiToAtm))
                    row[i + 1] = String.Format("{0:0}", LS13[i]);
                else
                    row[i + 1] = "";
            };
            pulseAmplitudeAtmTable.Rows.Add(row);

            for (int i = 0; i < 13; i++)
            {
                if (row[i + 1] != "")
                {
                    if (LS13[i] >= (250.0 * psiToAtm) && LS13[i] < (400.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[5].Cells[i + 1].Style.BackColor = Color.LightGreen;
                    else if (LS13[i] >= (200.0 * psiToAtm) && LS13[i] < (250.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[5].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (LS13[i] >= (400.0 * psiToAtm) && LS13[i] < (500.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[5].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (LS13[i] >= (100.0 * psiToAtm) && LS13[i] < (200.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[5].Cells[i + 1].Style.BackColor = Color.Pink;
                    else if (LS13[i] >= (500.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[5].Cells[i + 1].Style.BackColor = Color.Pink;
                }
            }
            #endregion

            #region 16 L/S
            GPM = 250.0;

            LS16 = new int[13];

            for (int i = 0; i < 13; i++)
            {
                LS16[i] = Convert.ToInt32(roudUpMult5(Convert.ToInt32(round * (((((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * areaClosed[i] * areaClosed[i])) -
                (((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * (areaOpen[i] * areaOpen[i])))) / round) * psiToAtm)));

                switch (i)
                {
                    case 0:
                        orifice478[2] = LS16[i];
                        break;
                    case 1:
                        orifice483[2] = LS16[i];
                        break;
                    case 2:
                        orifice488[2] = LS16[i];
                        break;
                    case 3:
                        orifice493[2] = LS16[i];
                        break;
                    case 4:
                        orifice498[2] = LS16[i];
                        break;
                    case 5:
                        orifice503[2] = LS16[i];
                        break;
                    case 6:
                        orifice508[2] = LS16[i];
                        break;
                    case 7:
                        orifice513[2] = LS16[i];
                        break;
                    case 8:
                        orifice518[2] = LS16[i];
                        break;
                    case 9:
                        orifice523[2] = LS16[i];
                        break;
                    case 10:
                        orifice528[2] = LS16[i];
                        break;
                    case 11:
                        orifice533[2] = LS16[i];
                        break;
                    case 12:
                        orifice538[2] = LS16[i];
                        break;
                }
            }

            row = new string[14];
            row[0] = "16 L/S";

            for (int i = 0; i < 13; i++)
            {
                if (LS16[i] >= (100.0 * psiToAtm) && LS16[i] < (750.0 * psiToAtm))
                    row[i + 1] = String.Format("{0:0}", LS16[i]);
                else
                    row[i + 1] = "";
            };
            pulseAmplitudeAtmTable.Rows.Add(row);

            for (int i = 0; i < 13; i++)
            {
                if (row[i + 1] != "")
                {
                    if (LS16[i] >= (250.0 * psiToAtm) && LS16[i] < (400.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[6].Cells[i + 1].Style.BackColor = Color.LightGreen;
                    else if (LS16[i] >= (200.0 * psiToAtm) && LS16[i] < (250.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[6].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (LS16[i] >= (400.0 * psiToAtm) && LS16[i] < (500.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[6].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (LS16[i] >= (100.0 * psiToAtm) && LS16[i] < (200.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[6].Cells[i + 1].Style.BackColor = Color.Pink;
                    else if (LS16[i] >= (500.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[6].Cells[i + 1].Style.BackColor = Color.Pink;
                }
            }
            #endregion

            #region 19 L/S
            GPM = 300.0;

            LS19 = new int[13];

            for (int i = 0; i < 13; i++)
            {
                LS19[i] = Convert.ToInt32(roudUpMult5(Convert.ToInt32(round * (((((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * areaClosed[i] * areaClosed[i])) -
                (((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * (areaOpen[i] * areaOpen[i])))) / round) * psiToAtm)));

                switch (i)
                {
                    case 0:
                        orifice478[3] = LS19[i];
                        break;
                    case 1:
                        orifice483[3] = LS19[i];
                        break;
                    case 2:
                        orifice488[3] = LS19[i];
                        break;
                    case 3:
                        orifice493[3] = LS19[i];
                        break;
                    case 4:
                        orifice498[3] = LS19[i];
                        break;
                    case 5:
                        orifice503[3] = LS19[i];
                        break;
                    case 6:
                        orifice508[3] = LS19[i];
                        break;
                    case 7:
                        orifice513[3] = LS19[i];
                        break;
                    case 8:
                        orifice518[3] = LS19[i];
                        break;
                    case 9:
                        orifice523[3] = LS19[i];
                        break;
                    case 10:
                        orifice528[3] = LS19[i];
                        break;
                    case 11:
                        orifice533[3] = LS19[i];
                        break;
                    case 12:
                        orifice538[3] = LS19[i];
                        break;
                }
            }

            row = new string[14];
            row[0] = "19 L/S";

            for (int i = 0; i < 13; i++)
            {
                if (LS19[i] >= (100.0 * psiToAtm) && LS19[i] < (750.0 * psiToAtm))
                    row[i + 1] = String.Format("{0:0}", LS19[i]);
                else
                    row[i + 1] = "";
            };
            pulseAmplitudeAtmTable.Rows.Add(row);

            for (int i = 0; i < 13; i++)
            {
                if (row[i + 1] != "")
                {
                    if (LS19[i] >= (250.0 * psiToAtm) && LS19[i] < (400.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[7].Cells[i + 1].Style.BackColor = Color.LightGreen;
                    else if (LS19[i] >= (200.0 * psiToAtm) && LS19[i] < (250.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[7].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (LS19[i] >= (400.0 * psiToAtm) && LS19[i] < (500.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[7].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (LS19[i] >= (100.0 * psiToAtm) && LS19[i] < (200.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[7].Cells[i + 1].Style.BackColor = Color.Pink;
                    else if (LS19[i] >= (500.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[7].Cells[i + 1].Style.BackColor = Color.Pink;
                }
            }
            #endregion

            #region 22 L/S
            GPM = 350.0;

            LS22 = new int[13];

            for (int i = 0; i < 13; i++)
            {
                LS22[i] = Convert.ToInt32(roudUpMult5(Convert.ToInt32(round * (((((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * areaClosed[i] * areaClosed[i])) -
                (((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * (areaOpen[i] * areaOpen[i])))) / round) * psiToAtm)));

                switch (i)
                {
                    case 0:
                        orifice478[4] = LS22[i];
                        break;
                    case 1:
                        orifice483[4] = LS22[i];
                        break;
                    case 2:
                        orifice488[4] = LS22[i]; 
                        break;
                    case 3:
                        orifice493[4] = LS22[i];
                        break;
                    case 4:
                        orifice498[4] = LS22[i];
                        break;
                    case 5:
                        orifice503[4] = LS22[i];
                        break;
                    case 6:
                        orifice508[4] = LS22[i];
                        break;
                    case 7:
                        orifice513[4] = LS22[i];
                        break;
                    case 8:
                        orifice518[4] = LS22[i];
                        break;
                    case 9:
                        orifice523[4] = LS22[i];
                        break;
                    case 10:
                        orifice528[4] = LS22[i];
                        break;
                    case 11:
                        orifice533[4] = LS22[i];
                        break;
                    case 12:
                        orifice538[4] = LS22[i];
                        break;
                }
            }

            row = new string[14];
            row[0] = "22 L/S";

            for (int i = 0; i < 13; i++)
            {
                if (LS22[i] >= (100.0 * psiToAtm) && LS22[i] < (750.0 * psiToAtm))
                    row[i + 1] = String.Format("{0:0}", LS22[i]);
                else
                    row[i + 1] = "";
            };
            pulseAmplitudeAtmTable.Rows.Add(row);

            for (int i = 0; i < 13; i++)
            {
                if (row[i + 1] != "")
                {
                    if (LS22[i] >= (250.0 * psiToAtm) && LS22[i] < (400.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[8].Cells[i + 1].Style.BackColor = Color.LightGreen;
                    else if (LS22[i] >= (200.0 * psiToAtm) && LS22[i] < (250.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[8].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (LS22[i] >= (400.0 * psiToAtm) && LS22[i] < (500.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[8].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (LS22[i] >= (100.0 * psiToAtm) && LS22[i] < (200.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[8].Cells[i + 1].Style.BackColor = Color.Pink;
                    else if (LS22[i] >= (500.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[8].Cells[i + 1].Style.BackColor = Color.Pink;
                }
            }
            #endregion

            #region 25 L/S
            GPM = 400.0;

            LS25 = new int[13];

            for (int i = 0; i < 13; i++)
            {
                LS25[i] = Convert.ToInt32(roudUpMult5(Convert.ToInt32(round * (((((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * areaClosed[i] * areaClosed[i])) -
                (((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * (areaOpen[i] * areaOpen[i])))) / round) * psiToAtm)));

                switch (i)
                {
                    case 0:
                        orifice478[5] = LS25[i];
                        break;
                    case 1:
                        orifice483[5] = LS25[i];
                        break;
                    case 2:
                        orifice488[5] = LS25[i];
                        break;
                    case 3:
                        orifice493[5] = LS25[i];
                        break;
                    case 4:
                        orifice498[5] = LS25[i];
                        break;
                    case 5:
                        orifice503[5] = LS25[i];
                        break;
                    case 6:
                        orifice508[5] = LS25[i];
                        break;
                    case 7:
                        orifice513[5] = LS25[i];
                        break;
                    case 8:
                        orifice518[5] = LS25[i];
                        break;
                    case 9:
                        orifice523[5] = LS25[i];
                        break;
                    case 10:
                        orifice528[5] = LS25[i];
                        break;
                    case 11:
                        orifice533[5] = LS25[i];
                        break;
                    case 12:
                        orifice538[5] = LS25[i];
                        break;
                }
            }

            row = new string[14];
            row[0] = "25 L/S";

            for (int i = 0; i < 13; i++)
            {
                if (LS25[i] >= (100.0 * psiToAtm) && LS25[i] < (750.0 * psiToAtm))
                    row[i + 1] = String.Format("{0:0}", LS25[i]);
                else
                    row[i + 1] = "";
            };
            pulseAmplitudeAtmTable.Rows.Add(row);

            for (int i = 0; i < 13; i++)
            {
                if (row[i + 1] != "")
                {
                    if (LS25[i] >= (250.0 * psiToAtm) && LS25[i] < (400.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[9].Cells[i + 1].Style.BackColor = Color.LightGreen;
                    else if (LS25[i] >= (200.0 * psiToAtm) && LS25[i] < (250.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[9].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (LS25[i] >= (400.0 * psiToAtm) && LS25[i] < (500.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[9].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (LS25[i] >= (100.0 * psiToAtm) && LS25[i] < (200.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[9].Cells[i + 1].Style.BackColor = Color.Pink;
                    else if (LS25[i] >= (500.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[9].Cells[i + 1].Style.BackColor = Color.Pink;
                }
            }
            #endregion

            #region 28 L/S
            GPM = 450.0;

            LS28 = new int[13];

            for (int i = 0; i < 13; i++)
            {
                LS28[i] = Convert.ToInt32(roudUpMult5(Convert.ToInt32(round * (((((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * areaClosed[i] * areaClosed[i])) -
                (((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * (areaOpen[i] * areaOpen[i])))) / round) * psiToAtm)));

                switch (i)
                {
                    case 0:
                        orifice478[6] = LS28[i];
                        break;
                    case 1:
                        orifice483[6] = LS28[i];
                        break;
                    case 2:
                        orifice488[6] = LS28[i];
                        break;
                    case 3:
                        orifice493[6] = LS28[i];
                        break;
                    case 4:
                        orifice498[6] = LS28[i];
                        break;
                    case 5:
                        orifice503[6] = LS28[i];
                        break;
                    case 6:
                        orifice508[6] = LS28[i];
                        break;
                    case 7:
                        orifice513[6] = LS28[i];
                        break;
                    case 8:
                        orifice518[6] = LS28[i];
                        break;
                    case 9:
                        orifice523[6] = LS28[i];
                        break;
                    case 10:
                        orifice528[6] = LS28[i];
                        break;
                    case 11:
                        orifice533[6] = LS28[i];
                        break;
                    case 12:
                        orifice538[6] = LS28[i];
                        break;
                }
            }

            row = new string[14];
            row[0] = "28 L/S";

            for (int i = 0; i < 13; i++)
            {
                if (LS28[i] >= (100.0 * psiToAtm) && LS28[i] < (750.0 * psiToAtm))
                    row[i + 1] = String.Format("{0:0}", LS28[i]);
                else
                    row[i + 1] = "";
            };
            pulseAmplitudeAtmTable.Rows.Add(row);

            for (int i = 0; i < 13; i++)
            {
                if (row[i + 1] != "")
                {
                    if (LS28[i] >= (250.0 * psiToAtm) && LS28[i] < (400.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[10].Cells[i + 1].Style.BackColor = Color.LightGreen;
                    else if (LS28[i] >= (200.0 * psiToAtm) && LS28[i] < (250.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[10].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (LS28[i] >= (400.0 * psiToAtm) && LS28[i] < (500.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[10].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (LS28[i] >= (100.0 * psiToAtm) && LS28[i] < (200.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[10].Cells[i + 1].Style.BackColor = Color.Pink;
                    else if (LS28[i] >= (500.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[10].Cells[i + 1].Style.BackColor = Color.Pink;
                }
            }
            #endregion

            #region 32 L/S
            GPM = 500.0;

            LS32 = new int[13];

            for (int i = 0; i < 13; i++)
            {
                LS32[i] = Convert.ToInt32(roudUpMult5(Convert.ToInt32(round * (((((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * areaClosed[i] * areaClosed[i])) -
                (((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * (areaOpen[i] * areaOpen[i])))) / round) * psiToAtm)));

                switch (i)
                {
                    case 0:
                        orifice478[7] = LS32[i];
                        break;
                    case 1:
                        orifice483[7] = LS32[i];
                        break;
                    case 2:
                        orifice488[7] = LS32[i];
                        break;
                    case 3:
                        orifice493[7] = LS32[i];
                        break;
                    case 4:
                        orifice498[7] = LS32[i];
                        break;
                    case 5:
                        orifice503[7] = LS32[i];
                        break;
                    case 6:
                        orifice508[7] = LS32[i];
                        break;
                    case 7:
                        orifice513[7] = LS32[i];
                        break;
                    case 8:
                        orifice518[7] = LS32[i];
                        break;
                    case 9:
                        orifice523[7] = LS32[i];
                        break;
                    case 10:
                        orifice528[7] = LS32[i];
                        break;
                    case 11:
                        orifice533[7] = LS32[i];
                        break;
                    case 12:
                        orifice538[7] = LS32[i];
                        break;
                }
            }

            row = new string[14];
            row[0] = "32 L/S";

            for (int i = 0; i < 13; i++)
            {
                if (LS32[i] >= (100.0 * psiToAtm) && LS32[i] < (750.0 * psiToAtm))
                    row[i + 1] = String.Format("{0:0}", LS32[i]);
                else
                    row[i + 1] = "";
            };
            pulseAmplitudeAtmTable.Rows.Add(row);

            for (int i = 0; i < 13; i++)
            {
                if (row[i + 1] != "")
                {
                    if (LS32[i] >= (250.0 * psiToAtm) && LS32[i] < (400.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[11].Cells[i + 1].Style.BackColor = Color.LightGreen;
                    else if (LS32[i] >= (200.0 * psiToAtm) && LS32[i] < (250.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[11].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (LS32[i] >= (400.0 * psiToAtm) && LS32[i] < (500.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[11].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (LS32[i] >= (100.0 * psiToAtm) && LS32[i] < (200.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[11].Cells[i + 1].Style.BackColor = Color.Pink;
                    else if (LS32[i] >= (500.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[11].Cells[i + 1].Style.BackColor = Color.Pink;
                }
            }
            #endregion

            #region 35 L/S
            GPM = 550.0;

            LS35 = new int[13];

            for (int i = 0; i < 13; i++)
            {
                LS35[i] = Convert.ToInt32(roudUpMult5(Convert.ToInt32(round * (((((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * areaClosed[i] * areaClosed[i])) -
                (((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * (areaOpen[i] * areaOpen[i])))) / round) * psiToAtm)));

                switch (i)
                {
                    case 0:
                        orifice478[8] = LS35[i];
                        break;
                    case 1:
                        orifice483[8] = LS35[i];
                        break;
                    case 2:
                        orifice488[8] = LS35[i];
                        break;
                    case 3:
                        orifice493[8] = LS35[i];
                        break;
                    case 4:
                        orifice498[8] = LS35[i];
                        break;
                    case 5:
                        orifice503[8] = LS35[i];
                        break;
                    case 6:
                        orifice508[8] = LS35[i];
                        break;
                    case 7:
                        orifice513[8] = LS35[i];
                        break;
                    case 8:
                        orifice518[8] = LS35[i];
                        break;
                    case 9:
                        orifice523[8] = LS35[i];
                        break;
                    case 10:
                        orifice528[8] = LS35[i];
                        break;
                    case 11:
                        orifice533[8] = LS35[i];
                        break;
                    case 12:
                        orifice538[8] = LS35[i];
                        break;
                }
            }

            row = new string[14];
            row[0] = "35 L/S";

            for (int i = 0; i < 13; i++)
            {
                if (LS35[i] >= (100.0 * psiToAtm) && LS35[i] < (750.0 * psiToAtm))
                    row[i + 1] = String.Format("{0:0}", LS35[i]);
                else
                    row[i + 1] = "";
            };
            pulseAmplitudeAtmTable.Rows.Add(row);

            for (int i = 0; i < 13; i++)
            {
                if (row[i + 1] != "")
                {
                    if (LS35[i] >= (250.0 * psiToAtm) && LS35[i] < (400.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[12].Cells[i + 1].Style.BackColor = Color.LightGreen;
                    else if (LS35[i] >= (200.0 * psiToAtm) && LS35[i] < (250.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[12].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (LS35[i] >= (400.0 * psiToAtm) && LS35[i] < (500.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[12].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (LS35[i] >= (100.0 * psiToAtm) && LS35[i] < (200.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[12].Cells[i + 1].Style.BackColor = Color.Pink;
                    else if (LS35[i] >= (500.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[12].Cells[i + 1].Style.BackColor = Color.Pink;
                }
            }
            #endregion

            #region 38 L/S
            GPM = 600.0;

            LS38 = new int[13];

            for (int i = 0; i < 13; i++)
            {
                LS38[i] = Convert.ToInt32(roudUpMult5(Convert.ToInt32(round * (((((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * areaClosed[i] * areaClosed[i])) -
                (((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * (areaOpen[i] * areaOpen[i])))) / round) * psiToAtm)));

                switch (i)
                {
                    case 0:
                        orifice478[9] = LS38[i];
                        break;
                    case 1:
                        orifice483[9] = LS38[i];
                        break;
                    case 2:
                        orifice488[9] = LS38[i];
                        break;
                    case 3:
                        orifice493[9] = LS38[i];
                        break;
                    case 4:
                        orifice498[9] = LS38[i];
                        break;
                    case 5:
                        orifice503[9] = LS38[i];
                        break;
                    case 6:
                        orifice508[9] = LS38[i];
                        break;
                    case 7:
                        orifice513[9] = LS38[i];
                        break;
                    case 8:
                        orifice518[9] = LS38[i];
                        break;
                    case 9:
                        orifice523[9] = LS38[i];
                        break;
                    case 10:
                        orifice528[9] = LS38[i];
                        break;
                    case 11:
                        orifice533[9] = LS38[i];
                        break;
                    case 12:
                        orifice538[9] = LS38[i];
                        break;
                }
            }

            row = new string[14];
            row[0] = "38 L/S";

            for (int i = 0; i < 13; i++)
            {
                if (LS38[i] >= (100.0 * psiToAtm) && LS38[i] < (750.0 * psiToAtm))
                    row[i + 1] = String.Format("{0:0}", LS38[i]);
                else
                    row[i + 1] = "";
            };
            pulseAmplitudeAtmTable.Rows.Add(row);

            for (int i = 0; i < 13; i++)
            {
                if (row[i + 1] != "")
                {
                    if (LS38[i] >= (250.0 * psiToAtm) && LS38[i] < (400.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[13].Cells[i + 1].Style.BackColor = Color.LightGreen;
                    else if (LS38[i] >= (200.0 * psiToAtm) && LS38[i] < (250.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[13].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (LS38[i] >= (400.0 * psiToAtm) && LS38[i] < (500.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[13].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (LS38[i] >= (100.0 * psiToAtm) && LS38[i] < (200.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[13].Cells[i + 1].Style.BackColor = Color.Pink;
                    else if (LS38[i] >= (500.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[13].Cells[i + 1].Style.BackColor = Color.Pink;
                }
            }
            #endregion

            #region 44 L/S
            GPM = 700.0;

            LS44 = new int[13];

            for (int i = 0; i < 13; i++)
            {
                LS44[i] = Convert.ToInt32(roudUpMult5(Convert.ToInt32(round * (((((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * areaClosed[i] * areaClosed[i])) -
                (((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * (areaOpen[i] * areaOpen[i])))) / round) * psiToAtm)));

                switch (i)
                {
                    case 0:
                        orifice478[10] = LS44[i];
                        break;
                    case 1:
                        orifice483[10] = LS44[i];
                        break;
                    case 2:
                        orifice488[10] = LS44[i];
                        break;
                    case 3:
                        orifice493[10] = LS44[i];
                        break;
                    case 4:
                        orifice498[10] = LS44[i];
                        break;
                    case 5:
                        orifice503[10] = LS44[i];
                        break;
                    case 6:
                        orifice508[10] = LS44[i];
                        break;
                    case 7:
                        orifice513[10] = LS44[i];
                        break;
                    case 8:
                        orifice518[10] = LS44[i];
                        break;
                    case 9:
                        orifice523[10] = LS44[i];
                        break;
                    case 10:
                        orifice528[10] = LS44[i];
                        break;
                    case 11:
                        orifice533[10] = LS44[i];
                        break;
                    case 12:
                        orifice538[10] = LS44[i];
                        break;
                }
            }

            row = new string[14];
            row[0] = "44 L/S";

            for (int i = 0; i < 13; i++)
            {
                if (LS44[i] >= (100.0 * psiToAtm) && LS44[i] < (750.0 * psiToAtm))
                    row[i + 1] = String.Format("{0:0}", LS44[i]);
                else
                    row[i + 1] = "";
            };
            pulseAmplitudeAtmTable.Rows.Add(row);

            for (int i = 0; i < 13; i++)
            {
                if (row[i + 1] != "")
                {
                    if (LS44[i] >= (250.0 * psiToAtm) && LS44[i] < (400.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[14].Cells[i + 1].Style.BackColor = Color.LightGreen;
                    else if (LS44[i] >= (200.0 * psiToAtm) && LS44[i] < (250.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[14].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (LS44[i] >= (400.0 * psiToAtm) && LS44[i] < (500.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[14].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (LS44[i] >= (100.0 * psiToAtm) && LS44[i] < (200.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[14].Cells[i + 1].Style.BackColor = Color.Pink;
                    else if (LS44[i] >= (500.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[14].Cells[i + 1].Style.BackColor = Color.Pink;
                }
            }
            #endregion

            #region 50 L/S
            GPM = 800.0;

            LS50 = new int[13];

            for (int i = 0; i < 13; i++)
            {
                LS50[i] = Convert.ToInt32(roudUpMult5(Convert.ToInt32(round * (((((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * areaClosed[i] * areaClosed[i])) -
                (((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * (areaOpen[i] * areaOpen[i])))) / round) * psiToAtm)));

                switch (i)
                {
                    case 0:
                        orifice478[11] = LS50[i];
                        break;
                    case 1:
                        orifice483[11] = LS50[i];
                        break;
                    case 2:
                        orifice488[11] = LS50[i];
                        break;
                    case 3:
                        orifice493[11] = LS50[i];
                        break;
                    case 4:
                        orifice498[11] = LS50[i];
                        break;
                    case 5:
                        orifice503[11] = LS50[i];
                        break;
                    case 6:
                        orifice508[11] = LS50[i];
                        break;
                    case 7:
                        orifice513[11] = LS50[i];
                        break;
                    case 8:
                        orifice518[11] = LS50[i];
                        break;
                    case 9:
                        orifice523[11] = LS50[i];
                        break;
                    case 10:
                        orifice528[11] = LS50[i];
                        break;
                    case 11:
                        orifice533[11] = LS50[i];
                        break;
                    case 12:
                        orifice538[11] = LS50[i];
                        break;
                }
            }

            row = new string[14];
            row[0] = "50 L/S";

            for (int i = 0; i < 13; i++)
            {
                if (LS50[i] >= (100.0 * psiToAtm) && LS50[i] < (750.0 * psiToAtm))
                    row[i + 1] = String.Format("{0:0}", LS50[i]);
                else
                    row[i + 1] = "";
            };
            pulseAmplitudeAtmTable.Rows.Add(row);

            for (int i = 0; i < 13; i++)
            {
                if (row[i + 1] != "")
                {
                    if (LS50[i] >= (250.0 * psiToAtm) && LS50[i] < (400.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[15].Cells[i + 1].Style.BackColor = Color.LightGreen;
                    else if (LS50[i] >= (200.0 * psiToAtm) && LS50[i] < (250.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[15].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (LS50[i] >= (400.0 * psiToAtm) && LS50[i] < (500.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[15].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (LS50[i] >= (100.0 * psiToAtm) && LS50[i] < (200.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[15].Cells[i + 1].Style.BackColor = Color.Pink;
                    else if (LS50[i] >= (500.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[15].Cells[i + 1].Style.BackColor = Color.Pink;
                }
            }
            #endregion

            #region 57 L/S
            GPM = 900.0;

            LS57 = new int[13];

            for (int i = 0; i < 13; i++)
            {
                LS57[i] = Convert.ToInt32(roudUpMult5(Convert.ToInt32(round * (((((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * areaClosed[i] * areaClosed[i])) -
                (((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * (areaOpen[i] * areaOpen[i])))) / round) * psiToAtm)));

                switch (i)
                {
                    case 0:
                        orifice478[12] = LS57[i];
                        break;
                    case 1:
                        orifice483[12] = LS57[i];
                        break;
                    case 2:
                        orifice488[12] = LS57[i];
                        break;
                    case 3:
                        orifice493[12] = LS57[i];
                        break;
                    case 4:
                        orifice498[12] = LS57[i];
                        break;
                    case 5:
                        orifice503[12] = LS57[i];
                        break;
                    case 6:
                        orifice508[12] = LS57[i];
                        break;
                    case 7:
                        orifice513[12] = LS57[i];
                        break;
                    case 8:
                        orifice518[12] = LS57[i];
                        break;
                    case 9:
                        orifice523[12] = LS57[i];
                        break;
                    case 10:
                        orifice528[12] = LS57[i];
                        break;
                    case 11:
                        orifice533[12] = LS57[i];
                        break;
                    case 12:
                        orifice538[12] = LS57[i];
                        break;
                }
            }

            row = new string[14];
            row[0] = "57 L/S";

            for (int i = 0; i < 13; i++)
            {
                if (LS57[i] >= (100.0 * psiToAtm) && LS57[i] < (750.0 * psiToAtm))
                    row[i + 1] = String.Format("{0:0}", LS57[i]);
                else
                    row[i + 1] = "";
            };
            pulseAmplitudeAtmTable.Rows.Add(row);

            for (int i = 0; i < 13; i++)
            {
                if (row[i + 1] != "")
                {
                    if (LS57[i] >= (250.0 * psiToAtm) && LS57[i] < (400.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[16].Cells[i + 1].Style.BackColor = Color.LightGreen;
                    else if (LS57[i] >= (200.0 * psiToAtm) && LS57[i] < (250.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[16].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (LS57[i] >= (400.0 * psiToAtm) && LS57[i] < (500.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[16].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (LS57[i] >= (100.0 * psiToAtm) && LS57[i] < (200.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[16].Cells[i + 1].Style.BackColor = Color.Pink;
                    else if (LS57[i] >= (500.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[16].Cells[i + 1].Style.BackColor = Color.Pink;
                }
            }
            #endregion

            #region 63 L/S
            GPM = 1000.0;

            LS63 = new int[13];

            for (int i = 0; i < 13; i++)
            {
                LS63[i] = Convert.ToInt32(roudUpMult5(Convert.ToInt32(round * (((((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * areaClosed[i] * areaClosed[i])) -
                (((GPM * pumpEfficiency) * (GPM * pumpEfficiency) * mudWeight) / (modelOrifices * (areaOpen[i] * areaOpen[i])))) / round) * psiToAtm)));

                switch (i)
                {
                    case 0:
                        orifice478[13] = LS63[i];
                        break;
                    case 1:
                        orifice483[13] = LS63[i];
                        break;
                    case 2:
                        orifice488[13] = LS63[i];
                        break;
                    case 3:
                        orifice493[13] = LS63[i];
                        break;
                    case 4:
                        orifice498[13] = LS63[i];
                        break;
                    case 5:
                        orifice503[13] = LS63[i];
                        break;
                    case 6:
                        orifice508[13] = LS63[i];
                        break;
                    case 7:
                        orifice513[13] = LS63[i];
                        break;
                    case 8:
                        orifice518[13] = LS63[i];
                        break;
                    case 9:
                        orifice523[13] = LS63[i];
                        break;
                    case 10:
                        orifice528[13] = LS63[i];
                        break;
                    case 11:
                        orifice533[13] = LS63[i];
                        break;
                    case 12:
                        orifice538[13] = LS63[i];
                        break;
                }
            }

            row = new string[14];
            row[0] = "63 L/S";

            for (int i = 0; i < 13; i++)
            {
                if (LS63[i] >= (100.0 * psiToAtm) && LS63[i] < (750.0 * psiToAtm))
                    row[i + 1] = String.Format("{0:0}", LS63[i]);
                else
                    row[i + 1] = "";
            };
            pulseAmplitudeAtmTable.Rows.Add(row);

            for (int i = 0; i < 13; i++)
            {
                if (row[i + 1] != "")
                {
                    if (LS63[i] >= (250.0 * psiToAtm) && LS63[i] < (400.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[17].Cells[i + 1].Style.BackColor = Color.LightGreen;
                    else if (LS63[i] >= (200.0 * psiToAtm) && LS63[i] < (250.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[17].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (LS63[i] >= (400.0 * psiToAtm) && LS63[i] < (500.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[17].Cells[i + 1].Style.BackColor = Color.PaleGoldenrod;
                    else if (LS63[i] >= (100.0 * psiToAtm) && LS63[i] < (200.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[17].Cells[i + 1].Style.BackColor = Color.Pink;
                    else if (LS63[i] >= (500.0 * psiToAtm))
                        pulseAmplitudeAtmTable.Rows[17].Cells[i + 1].Style.BackColor = Color.Pink;
                }
            }
            #endregion

            pulseAmplitudeAtmTable.Columns[0].DefaultCellStyle.Font = new Font(pulseAmplitudePSITable.Font, FontStyle.Bold);

            pulseAmplitudeAtmTable.AutoResizeColumns();
        }

        public int roudUpMult5(int num)
        {
            return Convert.ToInt32(Math.Round(num / round) * round);
        }

        public void InitializePoppetTubestopOpenClosed()
        {
            pulserSpecTable.Rows.Clear();
            pulserSpecTable.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            pulserSpecTable.EnableHeadersVisualStyles = false;

            pulserSpecTable.RowHeadersVisible = false;
            pulserSpecTable.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;

            pulserSpecTable.ColumnCount = 14;

            string[] row;

            for (int i = 0; i < 14; i++)
            {
                pulserSpecTable.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }

            pulserSpecTable.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            row = new string[] { "Poppet OD", "1.80" };
            pulserSpecTable.Rows.Add(row);

            row = new string[] { "Tubestop OD", "1.25" };
            pulserSpecTable.Rows.Add(row);

            areaOpen = new double[] {
                ((orificeSizeInch[0] * orificeSizeInch[0] - tubeStopOD* tubeStopOD) * Math.PI)/4.0,
                ((orificeSizeInch[1] * orificeSizeInch[1] - tubeStopOD* tubeStopOD) * Math.PI)/4.0,
                ((orificeSizeInch[2] * orificeSizeInch[2] - tubeStopOD* tubeStopOD) * Math.PI)/4.0,
                ((orificeSizeInch[3] * orificeSizeInch[3] - tubeStopOD* tubeStopOD) * Math.PI)/4.0,
                ((orificeSizeInch[4] * orificeSizeInch[4] - tubeStopOD* tubeStopOD) * Math.PI)/4.0,
                ((orificeSizeInch[5] * orificeSizeInch[5] - tubeStopOD* tubeStopOD) * Math.PI)/4.0,
                ((orificeSizeInch[6] * orificeSizeInch[6] - tubeStopOD* tubeStopOD) * Math.PI)/4.0,
                ((orificeSizeInch[7] * orificeSizeInch[7] - tubeStopOD* tubeStopOD) * Math.PI)/4.0,
                ((orificeSizeInch[8] * orificeSizeInch[8] - tubeStopOD* tubeStopOD) * Math.PI)/4.0,
                ((orificeSizeInch[9] * orificeSizeInch[9] - tubeStopOD* tubeStopOD) * Math.PI)/4.0,
                ((orificeSizeInch[10] * orificeSizeInch[10] - tubeStopOD* tubeStopOD) * Math.PI)/4.0,
                ((orificeSizeInch[11] * orificeSizeInch[11] - tubeStopOD* tubeStopOD) * Math.PI)/4.0,
                ((orificeSizeInch[12] * orificeSizeInch[12] - tubeStopOD* tubeStopOD) * Math.PI)/4.0
            };

            row = new string[] { "Area Open",
                String.Format("{0:0.00}", areaOpen[0]),
                String.Format("{0:0.00}", areaOpen[1]),
                String.Format("{0:0.00}", areaOpen[2]),
                String.Format("{0:0.00}", areaOpen[3]),
                String.Format("{0:0.00}", areaOpen[4]),
                String.Format("{0:0.00}", areaOpen[5]),
                String.Format("{0:0.00}", areaOpen[6]),
                String.Format("{0:0.00}", areaOpen[7]),
                String.Format("{0:0.00}", areaOpen[8]),
                String.Format("{0:0.00}", areaOpen[9]),
                String.Format("{0:0.00}", areaOpen[10]),
                String.Format("{0:0.00}", areaOpen[11]),
                String.Format("{0:0.00}", areaOpen[12])
            };
            pulserSpecTable.Rows.Add(row);

            areaClosed = new double[] {
                ((orificeSizeInch[0] * orificeSizeInch[0] - poppetOD* poppetOD) * Math.PI)/4.0,
                ((orificeSizeInch[1] * orificeSizeInch[1] - poppetOD* poppetOD) * Math.PI)/4.0,
                ((orificeSizeInch[2] * orificeSizeInch[2] - poppetOD* poppetOD) * Math.PI)/4.0,
                ((orificeSizeInch[3] * orificeSizeInch[3] - poppetOD* poppetOD) * Math.PI)/4.0,
                ((orificeSizeInch[4] * orificeSizeInch[4] - poppetOD* poppetOD) * Math.PI)/4.0,
                ((orificeSizeInch[5] * orificeSizeInch[5] - poppetOD* poppetOD) * Math.PI)/4.0,
                ((orificeSizeInch[6] * orificeSizeInch[6] - poppetOD* poppetOD) * Math.PI)/4.0,
                ((orificeSizeInch[7] * orificeSizeInch[7] - poppetOD* poppetOD) * Math.PI)/4.0,
                ((orificeSizeInch[8] * orificeSizeInch[8] - poppetOD* poppetOD) * Math.PI)/4.0,
                ((orificeSizeInch[9] * orificeSizeInch[9] - poppetOD* poppetOD) * Math.PI)/4.0,
                ((orificeSizeInch[10] * orificeSizeInch[10] - poppetOD* poppetOD) * Math.PI)/4.0,
                ((orificeSizeInch[11] * orificeSizeInch[11] - poppetOD* poppetOD) * Math.PI)/4.0,
                ((orificeSizeInch[12] * orificeSizeInch[12] - poppetOD* poppetOD) * Math.PI)/4.0
            };

            row = new string[] { "Area Closed",
                String.Format("{0:0.00}", areaClosed[0]),
                String.Format("{0:0.00}", areaClosed[1]),
                String.Format("{0:0.00}", areaClosed[2]),
                String.Format("{0:0.00}", areaClosed[3]),
                String.Format("{0:0.00}", areaClosed[4]),
                String.Format("{0:0.00}", areaClosed[5]),
                String.Format("{0:0.00}", areaClosed[6]),
                String.Format("{0:0.00}", areaClosed[7]),
                String.Format("{0:0.00}", areaClosed[8]),
                String.Format("{0:0.00}", areaClosed[9]),
                String.Format("{0:0.00}", areaClosed[10]),
                String.Format("{0:0.00}", areaClosed[11]),
                String.Format("{0:0.00}", areaClosed[12])
            };
            pulserSpecTable.Rows.Add(row);

            pulserSpecTable.AutoResizeColumns();
        }
        
        public void clearAllToolString()
        {
            wolverineTmpSmartSolenoid.Checked = false;
            wolverineMasterWDirectional.Checked = false;
            gamma.Checked = false;
            wolverineTranslator.Checked = false;
            rmsResistivtyCheckbox.Checked = false;
            rmsPressureToolCheckbox.Checked = false;
            wolverineMicrohopCheckbox.Checked = false;
            wolverineLndcCheckbox.Checked = false;
            apsWpr.Checked = false;
        }

        public void checkMnemonicForTools(string mnemonicString)
        {
            if (mnemonicString.Contains("GAMA"))
            { 
                gamma.Checked = true;
            }
            if (mnemonicString.Contains("GX"))
            {
                wolverineMasterWDirectional.Checked = true;
            }
            if (mnemonicString.Contains("RF") || mnemonicString.Contains("RN"))
            {
                rmsResistivtyCheckbox.Checked = true;
            }
            if (mnemonicString.Contains("PAS") || mnemonicString.Contains("PAC") || mnemonicString.Contains("PBC"))
            {
                rmsPressureToolCheckbox.Checked = true;
            }
            if (mnemonicString.Contains("RS") )
            {
                wolverineMicrohopCheckbox.Checked = true;
            }
            if (mnemonicString.Contains("RHO") || mnemonicString.Contains("SO") || mnemonicString.Contains("ND"))
            {
                wolverineLndcCheckbox.Checked = true;
            }
            //Can't have mudpulse without pulser
            wolverineTmpSmartSolenoid.Checked = true;
        }

        private void loadConfigButton_Click(object sender, EventArgs e)
        {
            clearButton.PerformClick();
            clearAllToolString();

            surveyItems = new int[17];
            toolfaceItems = new int[17];

            double[] pulseWidthTable = new double[4];
            int selectedPwIndex = 0;

            double tnp0 = 0, tnp1 = 0, tnp2 = 0, tnp3 = 0, 
                td0 = 0, td1 = 0, td2 = 0, td3 = 0;
            string tdTnp = "";

            //openFileDialog.InitialDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            openFileDialog.InitialDirectory = "c:\\Neutrino\\SavedConfig";
            openFileDialog.FileName = "";
            openFileDialog.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*";
            openFileDialog.FilterIndex = 1;
            openFileDialog.RestoreDirectory = true;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string Filename = openFileDialog.FileName;
                XmlTextReader reader = new XmlTextReader(Filename);
                while (reader.Read())
                {
                    try
                    {
                        if (reader.IsStartElement())
                        {
                            switch (reader.Name)
                            {
                                case "ResistivitySource":
                                    resistivitySource = reader.ReadString();
                                    break;
                                case "PressureSource":
                                    pressureSource = reader.ReadString();
                                    break;
                                case "SurveySequence0":
                                    textBoxSurveySequence0 = reader.ReadString();
                                    checkMnemonicForTools(textBoxSurveySequence0);
                                    break;
                                case "SurveySequence1":
                                    textBoxSurveySequence1 = reader.ReadString();
                                    checkMnemonicForTools(textBoxSurveySequence1);
                                    break;
                                case "SurveySequence2":
                                    textBoxSurveySequence2 = reader.ReadString();
                                    checkMnemonicForTools(textBoxSurveySequence2);
                                    break;
                                case "SurveySequence3":
                                    checkMnemonicForTools(textBoxSurveySequence3);
                                    textBoxSurveySequence3 = reader.ReadString();
                                    break;
                                case "ToolFaceSequence0":
                                    textBoxToolfaceSequence0 = reader.ReadString();
                                    checkMnemonicForTools(textBoxToolfaceSequence0);
                                    break;
                                case "ToolFaceSequence1":
                                    textBoxToolfaceSequence1 = reader.ReadString();
                                    checkMnemonicForTools(textBoxToolfaceSequence1);
                                    break;
                                case "ToolFaceSequence2":
                                    textBoxToolfaceSequence2 = reader.ReadString();
                                    checkMnemonicForTools(textBoxToolfaceSequence2);
                                    break;
                                case "ToolFaceSequence3":
                                    textBoxToolfaceSequence3 = reader.ReadString();
                                    checkMnemonicForTools(textBoxToolfaceSequence3);
                                    break;
                                case "RotateSequence0":
                                    textBoxRotateSequence0 = reader.ReadString();
                                    checkMnemonicForTools(textBoxRotateSequence0);
                                    break;
                                case "RotateSequence1":
                                    textBoxRotateSequence1 = reader.ReadString();
                                    checkMnemonicForTools(textBoxRotateSequence1);
                                    break;
                                case "RotateSequence2":
                                    textBoxRotateSequence2 = reader.ReadString();
                                    checkMnemonicForTools(textBoxRotateSequence2);
                                    break;
                                case "RotateSequence3":
                                    textBoxRotateSequence3 = reader.ReadString();
                                    checkMnemonicForTools(textBoxRotateSequence3);
                                    break;
                                case "TnpMult0":
                                    tnp0 = Convert.ToDouble(reader.ReadString());
                                    break;
                                case "TnpMult1":
                                    tnp1 = Convert.ToDouble(reader.ReadString());
                                    break;
                                case "TnpMult2":
                                    tnp2 = Convert.ToDouble(reader.ReadString());
                                    break;
                                case "TnpMult3":
                                    tnp3 = Convert.ToDouble(reader.ReadString());
                                    break;
                                case "TdMult0":
                                    td0 = Convert.ToDouble(reader.ReadString());
                                    break;
                                case "TdMult1":
                                    td1 = Convert.ToDouble(reader.ReadString());
                                    break;
                                case "TdMult2":
                                    td2 = Convert.ToDouble(reader.ReadString());
                                    break;
                                case "TdMult3":
                                    td3 = Convert.ToDouble(reader.ReadString());
                                    break;
                                case "PulseWidthSeconds0":
                                    pulseWidthTable[0] = Convert.ToDouble(reader.ReadString());
                                    break;
                                case "PulseWidthSeconds1":
                                    pulseWidthTable[1] = Convert.ToDouble(reader.ReadString());
                                    break;
                                case "PulseWidthSeconds2":
                                    pulseWidthTable[2] = Convert.ToDouble(reader.ReadString());
                                    break;
                                case "PulseWidthSeconds3":
                                    pulseWidthTable[3] = Convert.ToDouble(reader.ReadString());
                                    break;
                                case "m_nPulseWidthIndex":
                                    selectedPwIndex = Convert.ToInt32(reader.ReadString());
                                    break;

                            }
                        }
                    }
                    catch
                    {
                        MessageBox.Show("Configuration Import error");
                    }
                }
                if (resistivitySource == "1")
                {
                    if (rmsResistivtyCheckbox.Checked)
                    {
                        rmsResistivtyCheckbox.Checked = false;
                        resistivitySampleRateComboBox.SelectedIndex = 1;
                        resistivitySampleRateComboBox.BackColor = Color.Yellow;
                        apsWpr.Checked = true;
                    }
                }
                if (pressureSource == "1")
                {
                    if (rmsPressureToolCheckbox.Checked)
                    {
                        rmsPressureToolCheckbox.Checked = false;
                        apsWpr.Checked = true;
                    }
                }

                tdTnp = String.Format("{0:0.00}", tnp0) + ", " + String.Format("{0:0.00}", td0);

                #region survey sequence bit item parse logic
                string[] pieces;

                switch (surveyComboBox.Text)
                {
                    case "SSEQ0":
                        pieces = textBoxSurveySequence0.Split(' ');
                        break;
                    case "SSEQ1":
                        pieces = textBoxSurveySequence1.Split(' ');
                        break;
                    case "SSEQ2":
                        pieces = textBoxSurveySequence2.Split(' ');
                        break;
                    case "SSEQ3":
                        pieces = textBoxSurveySequence3.Split(' ');
                        break;
                    default:
                        pieces = textBoxSurveySequence0.Split(' ');
                        break;
                }
                string tempItem = "", tempParity = "";
                int indexStart = 0, indexStop = 0;

                for (int i = 0; i < pieces.Count(); i++)
                {
                    indexStart = pieces[i].IndexOf(":");
                    tempItem = pieces[i].Substring(indexStart + 1);
                    indexStop = tempItem.IndexOf(":");
                    tempParity = tempItem.Substring(indexStop + 1);
                    tempItem = pieces[i].Substring(indexStart + 1, indexStop);

                    if (tempParity == "P")
                    {
                        surveyItems[Convert.ToInt32(tempItem)] += 1;
                    }
                    else
                    {
                        surveyItems[Convert.ToInt32(tempItem) - 1] += 1;
                    }
                }
                #endregion

                #region toolface sequence bit item parse logic
                switch (toolfaceComboBox.Text)
                {
                    case "TFSEQ0":
                        pieces = textBoxToolfaceSequence0.Split(' ');
                        break;
                    case "TFSEQ1":
                        pieces = textBoxToolfaceSequence1.Split(' ');
                        break;
                    case "TFSEQ2":
                        pieces = textBoxToolfaceSequence2.Split(' ');
                        break;
                    case "TFSEQ3":
                        pieces = textBoxToolfaceSequence3.Split(' ');
                        break;
                    default:
                        pieces = textBoxToolfaceSequence0.Split(' ');
                        break;
                }
                tempItem = ""; tempParity = "";
                indexStart = 0; indexStop = 0;

                for (int i = 0; i < pieces.Count(); i++)
                {
                    indexStart = pieces[i].IndexOf(":");
                    tempItem = pieces[i].Substring(indexStart + 1);
                    indexStop = tempItem.IndexOf(":");
                    tempParity = tempItem.Substring(indexStop + 1);
                    tempItem = pieces[i].Substring(indexStart + 1, indexStop);

                    if (tempParity == "P")
                    {
                        toolfaceItems[Convert.ToInt32(tempItem)] += 1;
                    }
                    else
                    {
                        toolfaceItems[Convert.ToInt32(tempItem) - 1] += 1;
                    }
                }
                #endregion

                #region rotate sequence bit item parse logic
                switch (toolfaceComboBox.Text)
                {
                    case "RSEQ0":
                        pieces = textBoxRotateSequence0.Split(' ');
                        break;
                    case "RSEQ1":
                        pieces = textBoxRotateSequence1.Split(' ');
                        break;
                    case "RSEQ2":
                        pieces = textBoxRotateSequence2.Split(' ');
                        break;
                    case "RSEQ3":
                        pieces = textBoxRotateSequence3.Split(' ');
                        break;
                    default:
                        pieces = textBoxRotateSequence0.Split(' ');
                        break;
                }
                tempItem = ""; tempParity = "";
                indexStart = 0; indexStop = 0;

                for (int i = 0; i < pieces.Count(); i++)
                {
                    indexStart = pieces[i].IndexOf(":");
                    tempItem = pieces[i].Substring(indexStart + 1);
                    indexStop = tempItem.IndexOf(":");
                    tempParity = tempItem.Substring(indexStop + 1);
                    tempItem = pieces[i].Substring(indexStart + 1, indexStop);

                    if (tempParity == "P")
                    {
                        rotateItems[Convert.ToInt32(tempItem)] += 1;
                    }
                    else
                    {
                        rotateItems[Convert.ToInt32(tempItem) - 1] += 1;
                    }
                }
                #endregion

                for (int i = 0; i < surveyItems.Count(); i++)
                {
                    switch (i)
                    {
                        case 0:
                            oneBitSurveyItem.Text = String.Format("{0:0}", surveyItems[i]);
                            if (surveyItems[i] > 0)
                                oneBitSurveyItem.BackColor = Color.Yellow;
                            break;
                        case 1:
                            twoBitSurveyItem.Text = String.Format("{0:0}", surveyItems[i]);
                            if (surveyItems[i] > 0)
                                twoBitSurveyItem.BackColor = Color.Yellow;
                            break;
                        case 2:
                            threeBitSurveyItem.Text = String.Format("{0:0}", surveyItems[i]);
                            if (surveyItems[i] > 0)
                                threeBitSurveyItem.BackColor = Color.Yellow;
                            break;
                        case 3:
                            fourBitSurveyItem.Text = String.Format("{0:0}", surveyItems[i]);
                            if (surveyItems[i] > 0)
                                fourBitSurveyItem.BackColor = Color.Yellow;
                            break;
                        case 4:
                            fiveBitSurveyItem.Text = String.Format("{0:0}", surveyItems[i]);
                            if (surveyItems[i] > 0)
                                fiveBitSurveyItem.BackColor = Color.Yellow;
                            break;
                        case 5:
                            sixBitSurveyItem.Text = String.Format("{0:0}", surveyItems[i]);
                            if (surveyItems[i] > 0)
                                sixBitSurveyItem.BackColor = Color.Yellow;
                            break;
                        case 6:
                            sevenBitSurveyItem.Text = String.Format("{0:0}", surveyItems[i]);
                            if (surveyItems[i] > 0)
                                sevenBitSurveyItem.BackColor = Color.Yellow;
                            break;
                        case 7:
                            eightBitSurveyItem.Text = String.Format("{0:0}", surveyItems[i]);
                            if (surveyItems[i] > 0)
                                eightBitSurveyItem.BackColor = Color.Yellow;
                            break;
                        case 8:
                            nineBitSurveyItem.Text = String.Format("{0:0}", surveyItems[i]);
                            if (surveyItems[i] > 0)
                                nineBitSurveyItem.BackColor = Color.Yellow;
                            break;
                        case 9:
                            tenBitSurveyItem.Text = String.Format("{0:0}", surveyItems[i]);
                            if (surveyItems[i] > 0)
                                tenBitSurveyItem.BackColor = Color.Yellow;
                            break;
                        case 10:
                            elevenBitSurveyItem.Text = String.Format("{0:0}", surveyItems[i]);
                            if (surveyItems[i] > 0)
                                elevenBitSurveyItem.BackColor = Color.Yellow;
                            break;
                        case 11:
                            twelveBitSurveyItem.Text = String.Format("{0:0}", surveyItems[i]);
                            if (surveyItems[i] > 0)
                                twelveBitSurveyItem.BackColor = Color.Yellow;
                            break;
                        case 12:
                            thirteenBitSurveyItem.Text = String.Format("{0:0}", surveyItems[i]);
                            if (surveyItems[i] > 0)
                                thirteenBitSurveyItem.BackColor = Color.Yellow;
                            break;
                        case 13:
                            fourteenBitSurveyItem.Text = String.Format("{0:0}", surveyItems[i]);
                            if (surveyItems[i] > 0)
                                fourteenBitSurveyItem.BackColor = Color.Yellow;
                            break;
                        case 14:
                            fifteenBitSurveyItem.Text = String.Format("{0:0}", surveyItems[i]);
                            if (surveyItems[i] > 0)
                                fifteenBitSurveyItem.BackColor = Color.Yellow;
                            break;
                        case 15:
                            sixteenBitSurveyItem.Text = String.Format("{0:0}", surveyItems[i]);
                            if (surveyItems[i] > 0)
                                sixteenBitSurveyItem.BackColor = Color.Yellow;
                            break;
                        case 16:
                            seventeenBitSurveyItem.Text = String.Format("{0:0}", surveyItems[i]);
                            if (surveyItems[i] > 0)
                                seventeenBitSurveyItem.BackColor = Color.Yellow;
                            break;
                    }
                }

                for (int i = 0; i < toolfaceItems.Count(); i++)
                {
                    switch (i)
                    {
                        case 0:
                            oneBitToolfaceItem.Text = String.Format("{0:0}", toolfaceItems[i]);
                            if (toolfaceItems[i] > 0)
                                oneBitToolfaceItem.BackColor = Color.Yellow;
                            break;
                        case 1:
                            twoBitToolfaceItem.Text = String.Format("{0:0}", toolfaceItems[i]);
                            if (toolfaceItems[i] > 0)
                                twoBitToolfaceItem.BackColor = Color.Yellow;
                            break;
                        case 2:
                            threeBitToolfaceItem.Text = String.Format("{0:0}", toolfaceItems[i]);
                            if (toolfaceItems[i] > 0)
                                threeBitToolfaceItem.BackColor = Color.Yellow;
                            break;
                        case 3:
                            fourBitToolfaceItem.Text = String.Format("{0:0}", toolfaceItems[i]);
                            if (toolfaceItems[i] > 0)
                                fourBitToolfaceItem.BackColor = Color.Yellow;
                            break;
                        case 4:
                            fiveBitToolfaceItem.Text = String.Format("{0:0}", toolfaceItems[i]);
                            if (toolfaceItems[i] > 0)
                                fiveBitToolfaceItem.BackColor = Color.Yellow;
                            break;
                        case 5:
                            sixBitToolfaceItem.Text = String.Format("{0:0}", toolfaceItems[i]);
                            if (toolfaceItems[i] > 0)
                                sixBitToolfaceItem.BackColor = Color.Yellow;
                            break;
                        case 6:
                            sevenBitToolfaceItem.Text = String.Format("{0:0}", toolfaceItems[i]);
                            if (toolfaceItems[i] > 0)
                                sevenBitToolfaceItem.BackColor = Color.Yellow;
                            break;
                        case 7:
                            eightBitToolfaceItem.Text = String.Format("{0:0}", toolfaceItems[i]);
                            if (toolfaceItems[i] > 0)
                                eightBitToolfaceItem.BackColor = Color.Yellow;
                            break;
                        case 8:
                            nineBitToolfaceItem.Text = String.Format("{0:0}", toolfaceItems[i]);
                            if (toolfaceItems[i] > 0)
                                nineBitToolfaceItem.BackColor = Color.Yellow;
                            break;
                        case 9:
                            tenBitToolfaceItem.Text = String.Format("{0:0}", toolfaceItems[i]);
                            if (toolfaceItems[i] > 0)
                                tenBitToolfaceItem.BackColor = Color.Yellow;
                            break;
                        case 10:
                            elevenBitToolfaceItem.Text = String.Format("{0:0}", toolfaceItems[i]);
                            if (toolfaceItems[i] > 0)
                                elevenBitToolfaceItem.BackColor = Color.Yellow;
                            break;
                        case 11:
                            twelveBitToolfaceItem.Text = String.Format("{0:0}", toolfaceItems[i]);
                            if (toolfaceItems[i] > 0)
                                twelveBitToolfaceItem.BackColor = Color.Yellow;
                            break;
                        case 12:
                            thirteenBitToolfaceItem.Text = String.Format("{0:0}", toolfaceItems[i]);
                            if (toolfaceItems[i] > 0)
                                thirteenBitToolfaceItem.BackColor = Color.Yellow;
                            break;
                        case 13:
                            fourteenBitToolfaceItem.Text = String.Format("{0:0}", toolfaceItems[i]);
                            if (toolfaceItems[i] > 0)
                                fourteenBitToolfaceItem.BackColor = Color.Yellow;
                            break;
                        case 14:
                            fifteenBitToolfaceItem.Text = String.Format("{0:0}", toolfaceItems[i]);
                            if (toolfaceItems[i] > 0)
                                fifteenBitToolfaceItem.BackColor = Color.Yellow;
                            break;
                        case 15:
                            sixteenBitToolfaceItem.Text = String.Format("{0:0}", toolfaceItems[i]);
                            if (toolfaceItems[i] > 0)
                                sixteenBitToolfaceItem.BackColor = Color.Yellow;
                            break;
                        case 16:
                            seventeenBitToolfaceItem.Text = String.Format("{0:0}", toolfaceItems[i]);
                            if (toolfaceItems[i] > 0)
                                seventeenBitToolfaceItem.BackColor = Color.Yellow;
                            break;
                    }
                }

                for (int i = 0; i < toolfaceItems.Count(); i++)
                {
                    switch (i)
                    {
                        case 0:
                            oneBitRotateItem.Text = String.Format("{0:0}", rotateItems[i]);
                            if (rotateItems[i] > 0)
                                oneBitRotateItem.BackColor = Color.Yellow;
                            break;
                        case 1:
                            twoBitRotateItem.Text = String.Format("{0:0}", rotateItems[i]);
                            if (rotateItems[i] > 0)
                                twoBitRotateItem.BackColor = Color.Yellow;
                            break;
                        case 2:
                            threeBitRotateItem.Text = String.Format("{0:0}", rotateItems[i]);
                            if (toolfaceItems[i] > 0)
                                threeBitToolfaceItem.BackColor = Color.Yellow;
                            break;
                        case 3:
                            fourBitRotateItem.Text = String.Format("{0:0}", rotateItems[i]);
                            if (rotateItems[i] > 0)
                                fourBitRotateItem.BackColor = Color.Yellow;
                            break;
                        case 4:
                            fiveBitRotateItem.Text = String.Format("{0:0}", rotateItems[i]);
                            if (rotateItems[i] > 0)
                                fiveBitRotateItem.BackColor = Color.Yellow;
                            break;
                        case 5:
                            sixBitRotateItem.Text = String.Format("{0:0}", rotateItems[i]);
                            if (rotateItems[i] > 0)
                                sixBitRotateItem.BackColor = Color.Yellow;
                            break;
                        case 6:
                            sevenBitRotateItem.Text = String.Format("{0:0}", rotateItems[i]);
                            if (rotateItems[i] > 0)
                                sevenBitRotateItem.BackColor = Color.Yellow;
                            break;
                        case 7:
                            eightBitRotateItem.Text = String.Format("{0:0}", rotateItems[i]);
                            if (rotateItems[i] > 0)
                                eightBitToolfaceItem.BackColor = Color.Yellow;
                            break;
                        case 8:
                            nineBitRotateItem.Text = String.Format("{0:0}", rotateItems[i]);
                            if (rotateItems[i] > 0)
                                nineBitRotateItem.BackColor = Color.Yellow;
                            break;
                        case 9:
                            tenBitRotateItem.Text = String.Format("{0:0}", rotateItems[i]);
                            if (rotateItems[i] > 0)
                                tenBitRotateItem.BackColor = Color.Yellow;
                            break;
                        case 10:
                            elevenBitRotateItem.Text = String.Format("{0:0}", rotateItems[i]);
                            if (rotateItems[i] > 0)
                                elevenBitRotateItem.BackColor = Color.Yellow;
                            break;
                        case 11:
                            twelveBitRotateItem.Text = String.Format("{0:0}", rotateItems[i]);
                            if (rotateItems[i] > 0)
                                twelveBitRotateItem.BackColor = Color.Yellow;
                            break;
                        case 12:
                            thirteenBitRotateItem.Text = String.Format("{0:0}", rotateItems[i]);
                            if (rotateItems[i] > 0)
                                thirteenBitRotateItem.BackColor = Color.Yellow;
                            break;
                        case 13:
                            fourteenBitRotateItem.Text = String.Format("{0:0}", rotateItems[i]);
                            if (rotateItems[i] > 0)
                                fourteenBitRotateItem.BackColor = Color.Yellow;
                            break;
                        case 14:
                            fifteenBitRotateItem.Text = String.Format("{0:0}", rotateItems[i]);
                            if (rotateItems[i] > 0)
                                fifteenBitRotateItem.BackColor = Color.Yellow;
                            break;
                        case 15:
                            sixteenBitRotateItem.Text = String.Format("{0:0}", rotateItems[i]);
                            if (rotateItems[i] > 0)
                                sixteenBitRotateItem.BackColor = Color.Yellow;
                            break;
                        case 16:
                            seventeenBitRotateItem.Text = String.Format("{0:0}", rotateItems[i]);
                            if (rotateItems[i] > 0)
                                seventeenBitRotateItem.BackColor = Color.Yellow;
                            break;
                    }
                }

                tdTmpComboBox.SelectedIndex = tdTmpComboBox.Items.IndexOf(tdTnp);
                tdTmpComboBox.BackColor = Color.Yellow;

                pulseWidthTextBox.Text = String.Format("{0:0.00}", pulseWidthTable[selectedPwIndex]);
                pulseWidthTextBox.BackColor = Color.Yellow;

                processButton.PerformClick();

                surveyComboBox.SelectedIndex = selectedPwIndex;
                toolfaceComboBox.SelectedIndex = selectedPwIndex;
                rotateComboBox.SelectedIndex = selectedPwIndex;
            }
        }

        private void tdTmpComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            processButton.PerformClick();
        }

        private void setROPButton_Click(object sender, EventArgs e)
        {
            ROP = Convert.ToInt32(ropTextBox.Text);
            ropTextBox.BackColor = Color.Yellow;
            processButton.PerformClick();
        }

        public void InitializeTelemetryBox()
        {
            surveyItems = new int[] {Convert.ToInt32( oneBitSurveyItem.Text), Convert.ToInt32(twoBitSurveyItem.Text), Convert.ToInt32(threeBitSurveyItem.Text),
                Convert.ToInt32( fourBitSurveyItem.Text), Convert.ToInt32( fiveBitSurveyItem.Text), Convert.ToInt32( sixBitSurveyItem.Text), Convert.ToInt32( sevenBitSurveyItem.Text),
                Convert.ToInt32( eightBitSurveyItem.Text), Convert.ToInt32( nineBitSurveyItem.Text), Convert.ToInt32( tenBitSurveyItem.Text), Convert.ToInt32( elevenBitSurveyItem.Text),
                Convert.ToInt32( twelveBitSurveyItem.Text), Convert.ToInt32( thirteenBitSurveyItem.Text), Convert.ToInt32( fourteenBitSurveyItem.Text), Convert.ToInt32( fifteenBitSurveyItem.Text),
                Convert.ToInt32( sixteenBitSurveyItem.Text), Convert.ToInt32( seventeenBitSurveyItem.Text)
            };

            toolfaceItems = new int[] { Convert.ToInt32( oneBitToolfaceItem.Text), Convert.ToInt32( twoBitToolfaceItem.Text), Convert.ToInt32( threeBitToolfaceItem.Text),
                Convert.ToInt32( fourBitToolfaceItem.Text), Convert.ToInt32( fiveBitToolfaceItem.Text), Convert.ToInt32( sixBitToolfaceItem.Text), Convert.ToInt32( sevenBitToolfaceItem.Text),
                Convert.ToInt32( eightBitToolfaceItem.Text), Convert.ToInt32( nineBitToolfaceItem.Text), Convert.ToInt32( tenBitToolfaceItem.Text), Convert.ToInt32( elevenBitToolfaceItem.Text),
                Convert.ToInt32( twelveBitToolfaceItem.Text), Convert.ToInt32( thirteenBitToolfaceItem.Text), Convert.ToInt32( fourteenBitToolfaceItem.Text), Convert.ToInt32( fifteenBitToolfaceItem.Text),
                Convert.ToInt32( sixteenBitToolfaceItem.Text), Convert.ToInt32( oneBitToolfaceItem.Text)
            };

            rotateItems = new int[] { Convert.ToInt32( oneBitRotateItem.Text), Convert.ToInt32( twoBitRotateItem.Text), Convert.ToInt32( threeBitRotateItem.Text),
                Convert.ToInt32( fourBitRotateItem.Text), Convert.ToInt32( fiveBitRotateItem.Text), Convert.ToInt32( sixBitRotateItem.Text), Convert.ToInt32( sevenBitRotateItem.Text),
                Convert.ToInt32( eightBitRotateItem.Text), Convert.ToInt32( nineBitRotateItem.Text), Convert.ToInt32( tenBitRotateItem.Text), Convert.ToInt32( elevenBitRotateItem.Text),
                Convert.ToInt32( twelveBitRotateItem.Text), Convert.ToInt32( thirteenBitRotateItem.Text), Convert.ToInt32( fourteenBitRotateItem.Text), Convert.ToInt32( fifteenBitRotateItem.Text),
                Convert.ToInt32( sixteenBitRotateItem.Text), Convert.ToInt32( oneBitRotateItem.Text)
            };

            double total = 0;

            if (tdTmpComboBox.SelectedIndex == tdTmpComboBox.Items.IndexOf("1.50, 0.50"))
            {
                //Time / Sequence Survey
                for (int i = 0; i < 17; i++)
                {
                    total += surveyItems[i] * sValue050[i];
                }
                timeSequenceSurvey = total;
                timeSequenceSurveyBox.Text = String.Format("{0:0.0}", total);

                //Time/ Sequence Toolface
                total = 0;
                for(int i = 0; i< 17; i++)
                {
                    total += toolfaceItems[i] * sValue050[i];
                }
                timeSequenceToolface = total;
                timeSequenceToolfaceBox.Text = String.Format("{0:0.0}", total);

                //Time/ Sequence Rotate
                total = 0;
                for (int i = 0; i < 17; i++)
                {
                    total += rotateItems[i] * sValue050[i];
                }
                timeSequenceRotate = total;
                timeSequenceRotateBox.Text = String.Format("{0:0.0}", total);

                //Bits/ second Survey
                total = 0;
                for (int i = 0; i < 17; i++)
                {
                    total += surveyItems[i] * (i + 1);
                }
                bitSecondSurvey = total / timeSequenceSurvey;
                bitRateSurveyBox.Text = String.Format("{0:0.00}", total / timeSequenceSurvey);

                //Bits/ second Toolface
                total = 0;
                for (int i = 0; i < 17; i++)
                {
                    total += toolfaceItems[i] * (i + 1);
                }
                bitSecondToolface = total / timeSequenceToolface;
                bitRateToolfaceBox.Text = String.Format("{0:0.00}", total / timeSequenceToolface);

                // Bits / second Rotate
                total = 0;
                for (int i = 0; i < 17; i++)
                {
                    total += rotateItems[i] * (i + 1);
                }
                bitSecondRotate = total / timeSequenceRotate;
                bitRateRotateBox.Text = String.Format("{0:0.00}", total / timeSequenceRotate);

                //# of Pulses Survey
                total = 0;
                for (int i = 0; i < 17; i++)
                {
                    total += surveyItems[i] * M05[i];
                }

                numberOfPulsesSurvey = total;
                numberOfPulsesSurveyBox.Text = String.Format("{0:0}", numberOfPulsesSurvey);

                //# of Pulses Toolface
                total = 0;
                for (int i = 0; i < 17; i++)
                {
                    total += toolfaceItems[i] * M05[i];
                }

                numberOfPulsesToolface = total;
                numberOfPulsesToolfaceBox.Text = String.Format("{0:0}", numberOfPulsesToolface);

                 //# of Pulses Rotate
                total = 0;
                for (int i = 0; i < 17; i++)
                {
                    total += rotateItems[i] * M05[i];
                }

                numberOfPulsesRotate = total;
                numberOfPulsesRotateBox.Text = String.Format("{0:0}", numberOfPulsesRotate);
            }
            else if (tdTmpComboBox.SelectedIndex == tdTmpComboBox.Items.IndexOf("1.00, 0.67"))
            {
                //Time / Sequence Survey
                for (int i = 0; i < 17; i++)
                {
                    total += surveyItems[i] * sValue067[i];
                }
                timeSequenceSurvey = total;
                timeSequenceSurveyBox.Text = String.Format("{0:0.0}", total);

                //Time/ Sequence Toolface
                total = 0;
                for (int i = 0; i < 17; i++)
                {
                    total += toolfaceItems[i] * sValue067[i];
                }
                timeSequenceToolface = total;
                timeSequenceToolfaceBox.Text = String.Format("{0:0.0}", total);

                //Time/ Sequence Rotate
                total = 0;
                for (int i = 0; i < 17; i++)
                {
                    total += rotateItems[i] * sValue067[i];
                }
                timeSequenceRotate = total;
                timeSequenceRotateBox.Text = String.Format("{0:0.0}", total);

                //Bits/ second Survey
                total = 0;
                for (int i = 0; i < 17; i++)
                {
                    total += surveyItems[i] * (i + 1);
                }
                bitSecondSurvey = total / timeSequenceSurvey;
                bitRateSurveyBox.Text = String.Format("{0:0.00}", total / timeSequenceSurvey);

                //Bits/ second Toolface
                total = 0;
                for (int i = 0; i < 17; i++)
                {
                    total += toolfaceItems[i] * (i + 1);
                }
                bitSecondToolface = total / timeSequenceToolface;
                bitRateToolfaceBox.Text = String.Format("{0:0.00}", total / timeSequenceToolface);

                //Bits/ second Rotate
                total = 0;
                for (int i = 0; i < 17; i++)
                {
                    total += rotateItems[i] * (i + 1);
                }
                bitSecondRotate = total / timeSequenceRotate;
                bitRateRotateBox.Text = String.Format("{0:0.00}", total / timeSequenceRotate);

                //# of Pulses Survey
                total = 0;
                for (int i = 0; i < 17; i++)
                {
                    total += surveyItems[i] * M067[i];
                }

                numberOfPulsesSurvey = total;
                numberOfPulsesSurveyBox.Text = String.Format("{0:0}", numberOfPulsesSurvey);

                //# of Pulses Toolface
                total = 0;
                for (int i = 0; i < 17; i++)
                {
                    total += toolfaceItems[i] * M067[i];
                }

                numberOfPulsesToolface = total;
                numberOfPulsesToolfaceBox.Text = String.Format("{0:0}", numberOfPulsesToolface);

                //# of Pulses Rotate
                total = 0;
                for (int i = 0; i < 17; i++)
                {
                    total += rotateItems[i] * M067[i];
                }

                numberOfPulsesRotate = total;
                numberOfPulsesRotateBox.Text = String.Format("{0:0}", numberOfPulsesRotate);
            }
            else if(tdTmpComboBox.SelectedIndex == tdTmpComboBox.Items.IndexOf("1.50, 0.63"))
            {
                //Time / Sequence Survey
                for (int i = 0; i < 17; i++)
                {
                    total += surveyItems[i] * sValue625[i];
                }
                timeSequenceSurvey = total;
                timeSequenceSurveyBox.Text = String.Format("{0:0.0}", total);

                //Time/ Sequence Toolface
                total = 0;
                for (int i = 0; i < 17; i++)
                {
                    total += toolfaceItems[i] * sValue625[i];
                }
                timeSequenceToolface = total;
                timeSequenceToolfaceBox.Text = String.Format("{0:0.0}", total);

                //Time/ Sequence Rotate
                total = 0;
                for (int i = 0; i < 17; i++)
                {
                    total += rotateItems[i] * sValue625[i];
                }
                timeSequenceRotate = total;
                timeSequenceRotateBox.Text = String.Format("{0:0.0}", total);

                //Bits/ second Survey
                total = 0;
                for (int i = 0; i < 17; i++)
                {
                    total += surveyItems[i] * (i + 1);
                }
                bitSecondSurvey = total / timeSequenceSurvey;
                bitRateSurveyBox.Text = String.Format("{0:0.00}", total / timeSequenceSurvey);

                //Bits/ second Toolface
                total = 0;
                for (int i = 0; i < 17; i++)
                {
                    total += toolfaceItems[i] * (i + 1);
                }
                bitSecondToolface = total / timeSequenceToolface;
                bitRateToolfaceBox.Text = String.Format("{0:0.00}", total / timeSequenceToolface);

                //Bits/ second Rotate
                total = 0;
                for (int i = 0; i < 17; i++)
                {
                    total += rotateItems[i] * (i + 1);
                }
                bitSecondRotate = total / timeSequenceRotate;
                bitRateRotateBox.Text = String.Format("{0:0.00}", total / timeSequenceRotate);

                //# of Pulses Survey
                total = 0;
                for (int i = 0; i < 17; i++)
                {
                    total += surveyItems[i] * M625[i];
                }

                numberOfPulsesSurvey = total;
                numberOfPulsesSurveyBox.Text = String.Format("{0:0}", numberOfPulsesSurvey);

                //# of Pulses Toolface
                total = 0;
                for (int i = 0; i < 17; i++)
                {
                    total += toolfaceItems[i] * M625[i];
                }

                numberOfPulsesToolface = total;
                numberOfPulsesToolfaceBox.Text = String.Format("{0:0}", numberOfPulsesToolface);

                //# of Pulses Rotate
                total = 0;
                for (int i = 0; i < 17; i++)
                {
                    total += rotateItems[i] * M625[i];
                }

                numberOfPulsesRotate = total;
                numberOfPulsesRotateBox.Text = String.Format("{0:0}", numberOfPulsesRotate);
            }

            timeSequenceSurveyBox.BackColor = Color.Orange;
            timeSequenceToolfaceBox.BackColor = Color.Orange;
            timeSequenceRotateBox.BackColor = Color.Orange;
            bitRateSurveyBox.BackColor = Color.Orange;
            bitRateToolfaceBox.BackColor = Color.Orange;
            bitRateRotateBox.BackColor = Color.Orange;
            distanceBetweenDataToolfaceBox.BackColor = Color.Orange;
            distanceBetweenDataRotateBox.BackColor = Color.Orange;

            pulsesPerHour = numberOfPulsesToolface / timeSequenceToolface * 3600;
            pulsesPerHourToolfaceBox.Text = String.Format("{0:0}", pulsesPerHour);

            pulsesPerHour = numberOfPulsesRotate / timeSequenceRotate * 3600;
            pulsesPerHourRotateBox.Text = String.Format("{0:0}", pulsesPerHour);

            joulesPerPulse = AsPulse * 28;
            joulesPerPulseToolfaceBox.Text = String.Format("{0:0.0}", joulesPerPulse);

            joulesPerPulse = AsPulse * 28;
            joulesPerPulseRotateBox.Text = String.Format("{0:0.0}", joulesPerPulse);

            distanceBetweenData = (timeSequenceToolface / 3600) * ROP;
            distanceBetweenDataToolfaceBox.Text = String.Format("{0:0.00}", distanceBetweenData);

            distanceBetweenData = (timeSequenceRotate / 3600) * ROP;
            distanceBetweenDataRotateBox.Text = String.Format("{0:0.00}", distanceBetweenData);

            InitializePulserConsumptionTable();
        }

        public void initializeTnpTdTable()
        {
            tnpTdOptionTable.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            tnpTdOptionTable.EnableHeadersVisualStyles = false;

            tnpTdOptionTable.RowHeadersVisible = true;
            tnpTdOptionTable.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;

            tnpTdOptionTable.ColumnCount = 3;

            tnpTdOptionTable.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            tnpTdOptionTable.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            tnpTdOptionTable.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            tnpTdOptionTable.Columns[0].Name = "Option 1";
            tnpTdOptionTable.Columns[1].Name = "Option 2";
            tnpTdOptionTable.Columns[2].Name = "Option 3";

            string[] row = new string[] {String.Format("{0:0.00}", TNP05) + ", " + String.Format("{0:0.00}", TD05),
                String.Format("{0:0.00}", TNP067) + ", " + String.Format("{0:0.00}", TD067),
                String.Format("{0:0.00}", TNP625) + ", " + String.Format("{0:0.00}", TD625),
            };

            tnpTdOptionTable.Rows.Add(row);
        }

        public void updatePieChart()
        {
            //reset your chart series and legends
            chart1.Series.Clear();
            chart1.Legends.Clear();

            //Add a new Legend(if needed) and do some formating
            //chart1.Legends.Add("Tools");
            //chart1.Legends[0].LegendStyle = LegendStyle.Table;
            //chart1.Legends[0].Docking = Docking.Bottom;
            //chart1.Legends[0].Alignment = StringAlignment.Center;
            //chart1.Legends[0].Title = "Tools";
            //chart1.Legends[0].BorderColor = Color.Black;

            // Add a new chart-series
            string seriesname = "Energy Consumption";
            chart1.Series.Add(seriesname);
            ////set the chart-type to "Pie"
            chart1.Series[seriesname].ChartType = SeriesChartType.Pie;
            chart1.Series[seriesname]["PieLabelStyle"] = "outside";

            chart1.Series[seriesname].BorderColor = Color.Black;

            //Add some datapoints so the series. in this case you can pass the values to this method
            if (wolverineLndcCheckbox.Checked)
                chart1.Series[seriesname].Points.AddXY("Wolverine LNDC - " + '\n' + String.Format("{0:0}", 202 / totalCurrentDraw * 100) + "%", 202 / totalCurrentDraw);
            if (wolverineMicrohopCheckbox.Checked)
                chart1.Series[seriesname].Points.AddXY("Wolverine Microhop - " + '\n' + String.Format("{0:0}", 32 / totalCurrentDraw * 100) + "%", 32 / totalCurrentDraw);
            if (wolverineTmpSmartSolenoid.Checked)
            {
                if (smartSolenoidCheckBox.Checked)
                    chart1.Series[seriesname].Points.AddXY("Wolverine TMP w/ Smart Solenoid" + '\n' + String.Format("{0:0}", Total / totalCurrentDraw * 100) + "%", Total / totalCurrentDraw);
                else
                    chart1.Series[seriesname].Points.AddXY("Wolverine TMP" + '\n' + String.Format("{0:0}", Total / totalCurrentDraw * 100) + "%", Total / totalCurrentDraw);
            }
            if (wolverineMasterWDirectional.Checked)
                chart1.Series[seriesname].Points.AddXY("Wolverine Master w/ Directional" + '\n' + String.Format("{0:0}", 60 / totalCurrentDraw * 100) + "%", 60 / totalCurrentDraw);
            if(gamma.Checked)
                chart1.Series[seriesname].Points.AddXY("Gamma" + '\n' + String.Format("{0:0.00}", 21 / totalCurrentDraw * 100) + "%", 21 / totalCurrentDraw);
            if(wolverineTranslator.Checked)
                chart1.Series[seriesname].Points.AddXY("Wolverine Translator" + '\n' + String.Format("{0:0}", 14 / totalCurrentDraw * 100) + "%", 14 / totalCurrentDraw);
            if(rmsResistivtyCheckbox.Checked)
                chart1.Series[seriesname].Points.AddXY("RMS Resistivity" + '\n' + String.Format("{0:0}", rmsResistivityConsumption / totalCurrentDraw * 100) + "%", rmsResistivityConsumption / totalCurrentDraw);
            if(rmsPressureToolCheckbox.Checked)
                chart1.Series[seriesname].Points.AddXY("RMS Pressure Tool" + '\n' + String.Format("{0:0}",rmsPressureConsumption / totalCurrentDraw *100)  + "%", rmsPressureConsumption / totalCurrentDraw);
            if(apsWpr.Checked)
                chart1.Series[seriesname].Points.AddXY("APS WPR - " + '\n' + String.Format("{0:0}", 165 / totalCurrentDraw * 100) + "%",165 / totalCurrentDraw);

            chart1.Titles.Clear();
            Title title = new Title();
            title.Font = new Font("Arial", 14, FontStyle.Bold);
            title.Text = "Average Power Consumption = " + String.Format("{0:0}", totalCurrentDraw) + "mA\n" +
                "Total Number of Batteries = " + String.Format("{0:0}", numberOfBatteries) + '\n' +
                "Total Battery Life = " + String.Format("{0:0} Hours", totalLife);
            chart1.Titles.Add(title);
        }

        public void RunConsumptionCalcuation()
        {
            numberOfBatteries = Convert.ToInt32(numberOfBatteriesTextBox.Text);
            totalLife = numberOfBatteries * (singleBatteryLife / (totalCurrentDraw / 1000));
            totalLifeTextBox.Text = String.Format("{0:0}", totalLife);
        }

        public void InitializeToolTable()
        {
            toolTable.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            toolTable.EnableHeadersVisualStyles = false;

            toolTable.RowHeadersVisible = true;
            toolTable.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;

            toolTable.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            toolTable.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            toolTable.Columns[0].Name = "In String?";
            toolTable.Columns[1].Name = "Current(mA)";
        }

        private async void processButton_Click(object sender, EventArgs e)
        {
            InitializeTelemetryBox();
            await Task.Delay(1000);
            #region Reset Display
            //oneBitSurveyItem.Text = "0";
            //oneBitSurveyItem.BackColor = Color.Empty;
            //twoBitSurveyItem.Text = "0";
            //twoBitSurveyItem.BackColor = Color.Empty;
            //threeBitSurveyItem.Text = "0";
            //threeBitSurveyItem.BackColor = Color.Empty;
            //fourBitSurveyItem.Text = "0";
            //fourBitSurveyItem.BackColor = Color.Empty;
            //fiveBitSurveyItem.Text = "0";
            //fiveBitSurveyItem.BackColor = Color.Empty;
            //sixBitSurveyItem.Text = "0";
            //sixBitSurveyItem.BackColor = Color.Empty;
            //sevenBitSurveyItem.Text = "0";
            //sevenBitSurveyItem.BackColor = Color.Empty;
            //eightBitSurveyItem.Text = "0";
            //eightBitSurveyItem.BackColor = Color.Empty;
            //nineBitSurveyItem.Text = "0";
            //nineBitSurveyItem.BackColor = Color.Empty;
            //tenBitSurveyItem.Text = "0";
            //tenBitSurveyItem.BackColor = Color.Empty;
            //elevenBitSurveyItem.Text = "0";
            //elevenBitSurveyItem.BackColor = Color.Empty;
            //twelveBitSurveyItem.Text = "0";
            //twelveBitSurveyItem.BackColor = Color.Empty;
            //thirteenBitSurveyItem.Text = "0";
            //thirteenBitSurveyItem.BackColor = Color.Empty;
            //fourteenBitSurveyItem.Text = "0";
            //fourteenBitSurveyItem.BackColor = Color.Empty;
            //fifteenBitSurveyItem.Text = "0";
            //fifteenBitSurveyItem.BackColor = Color.Empty;
            //sixteenBitSurveyItem.Text = "0";
            //sixteenBitSurveyItem.BackColor = Color.Empty;
            //seventeenBitSurveyItem.Text = "0";
            //seventeenBitSurveyItem.BackColor = Color.Empty;

            //oneBitToolfaceItem.Text = "0";
            //oneBitToolfaceItem.BackColor = Color.Empty;
            //twoBitToolfaceItem.Text = "0";
            //twoBitToolfaceItem.BackColor = Color.Empty;
            //threeBitToolfaceItem.Text = "0";
            //threeBitToolfaceItem.BackColor = Color.Empty;
            //fourBitToolfaceItem.Text = "0";
            //fourBitToolfaceItem.BackColor = Color.Empty;
            //fiveBitToolfaceItem.Text = "0";
            //fiveBitToolfaceItem.BackColor = Color.Empty;
            //sixBitToolfaceItem.Text = "0";
            //sixBitToolfaceItem.BackColor = Color.Empty;
            //sevenBitToolfaceItem.Text = "0";
            //sevenBitToolfaceItem.BackColor = Color.Empty;
            //eightBitToolfaceItem.Text = "0";
            //eightBitToolfaceItem.BackColor = Color.Empty;
            //nineBitToolfaceItem.Text = "0";
            //nineBitToolfaceItem.BackColor = Color.Empty;
            //tenBitToolfaceItem.Text = "0";
            //tenBitToolfaceItem.BackColor = Color.Empty;
            //elevenBitToolfaceItem.Text = "0";
            //elevenBitToolfaceItem.BackColor = Color.Empty;
            //twelveBitToolfaceItem.Text = "0";
            //twelveBitToolfaceItem.BackColor = Color.Empty;
            //thirteenBitToolfaceItem.Text = "0";
            //thirteenBitToolfaceItem.BackColor = Color.Empty;
            //fourteenBitToolfaceItem.Text = "0";
            //fourteenBitToolfaceItem.BackColor = Color.Empty;
            //fifteenBitToolfaceItem.Text = "0";
            //fifteenBitToolfaceItem.BackColor = Color.Empty;
            //sixteenBitToolfaceItem.Text = "0";
            //sixteenBitToolfaceItem.BackColor = Color.Empty;
            //seventeenBitToolfaceItem.Text = "0";
            //seventeenBitToolfaceItem.BackColor = Color.Empty;

            //timeSequenceSurveyBox.BackColor = Color.Empty;
            //timeSequenceToolfaceBox.BackColor = Color.Empty;
            //bitRateSurveyBox.BackColor = Color.Empty;
            //bitRateToolfaceBox.BackColor = Color.Empty;
            //distanceBetweenDataBox.BackColor = Color.Empty;
            //tdTmpComboBox.BackColor = Color.Empty;
            //pulseWidthTextBox.BackColor = Color.Empty;
            #endregion
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            oneBitSurveyItem.Text = "0";
            oneBitSurveyItem.BackColor = Color.Empty;
            twoBitSurveyItem.Text = "0";
            twoBitSurveyItem.BackColor = Color.Empty;
            threeBitSurveyItem.Text = "0";
            threeBitSurveyItem.BackColor = Color.Empty;
            fourBitSurveyItem.Text = "0";
            fourBitSurveyItem.BackColor = Color.Empty;
            fiveBitSurveyItem.Text = "0";
            fiveBitSurveyItem.BackColor = Color.Empty;
            sixBitSurveyItem.Text = "0";
            sixBitSurveyItem.BackColor = Color.Empty;
            sevenBitSurveyItem.Text = "0";
            sevenBitSurveyItem.BackColor = Color.Empty;
            eightBitSurveyItem.Text = "0";
            eightBitSurveyItem.BackColor = Color.Empty;
            nineBitSurveyItem.Text = "0";
            nineBitSurveyItem.BackColor = Color.Empty;
            tenBitSurveyItem.Text = "0";
            tenBitSurveyItem.BackColor = Color.Empty;
            elevenBitSurveyItem.Text = "0";
            elevenBitSurveyItem.BackColor = Color.Empty;
            twelveBitSurveyItem.Text = "0";
            twelveBitSurveyItem.BackColor = Color.Empty;
            thirteenBitSurveyItem.Text = "0";
            thirteenBitSurveyItem.BackColor = Color.Empty;
            fourteenBitSurveyItem.Text = "0";
            fourteenBitSurveyItem.BackColor = Color.Empty;
            fifteenBitSurveyItem.Text = "0";
            fifteenBitSurveyItem.BackColor = Color.Empty;
            sixteenBitSurveyItem.Text = "0";
            sixteenBitSurveyItem.BackColor = Color.Empty;
            seventeenBitSurveyItem.Text = "0";
            seventeenBitSurveyItem.BackColor = Color.Empty;

            oneBitToolfaceItem.Text = "0";
            oneBitToolfaceItem.BackColor = Color.Empty;
            twoBitToolfaceItem.Text = "0";
            twoBitToolfaceItem.BackColor = Color.Empty;
            threeBitToolfaceItem.Text = "0";
            threeBitToolfaceItem.BackColor = Color.Empty;
            fourBitToolfaceItem.Text = "0";
            fourBitToolfaceItem.BackColor = Color.Empty;
            fiveBitToolfaceItem.Text = "0";
            fiveBitToolfaceItem.BackColor = Color.Empty;
            sixBitToolfaceItem.Text = "0";
            sixBitToolfaceItem.BackColor = Color.Empty;
            sevenBitToolfaceItem.Text = "0";
            sevenBitToolfaceItem.BackColor = Color.Empty;
            eightBitToolfaceItem.Text = "0";
            eightBitToolfaceItem.BackColor = Color.Empty;
            nineBitToolfaceItem.Text = "0";
            nineBitToolfaceItem.BackColor = Color.Empty;
            tenBitToolfaceItem.Text = "0";
            tenBitToolfaceItem.BackColor = Color.Empty;
            elevenBitToolfaceItem.Text = "0";
            elevenBitToolfaceItem.BackColor = Color.Empty;
            twelveBitToolfaceItem.Text = "0";
            twelveBitToolfaceItem.BackColor = Color.Empty;
            thirteenBitToolfaceItem.Text = "0";
            thirteenBitToolfaceItem.BackColor = Color.Empty;
            fourteenBitToolfaceItem.Text = "0";
            fourteenBitToolfaceItem.BackColor = Color.Empty;
            fifteenBitToolfaceItem.Text = "0";
            fifteenBitToolfaceItem.BackColor = Color.Empty;
            sixteenBitToolfaceItem.Text = "0";
            sixteenBitToolfaceItem.BackColor = Color.Empty;
            seventeenBitToolfaceItem.Text = "0";
            seventeenBitToolfaceItem.BackColor = Color.Empty;

            processButton.PerformClick();

            timeSequenceSurveyBox.BackColor = Color.Empty;
            timeSequenceToolfaceBox.BackColor = Color.Empty;
            bitRateSurveyBox.BackColor = Color.Empty;
            bitRateToolfaceBox.BackColor = Color.Empty;
            distanceBetweenDataToolfaceBox.BackColor = Color.Empty;
            tdTmpComboBox.BackColor = Color.Empty;
            pulseWidthTextBox.BackColor = Color.Empty;

            resistivitySampleRateComboBox.BackColor = Color.Empty;
            timeSequenceRotateBox.Text = "";
            timeSequenceRotateBox.BackColor = Color.Empty;
            bitRateRotateBox.Text = "";
            bitRateRotateBox.BackColor = Color.Empty;
            numberOfPulsesRotateBox.Text = "";
            numberOfPulsesRotateBox.BackColor = Color.Empty;
            pulsesPerHourRotateBox.Text = "";

        }

        public void InitializePulserConsumptionTable()
        {
            pulserConsumptionTable.Rows.Clear();

            pulserConsumptionTable.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            pulserConsumptionTable.EnableHeadersVisualStyles = false;

            pulserConsumptionTable.RowHeadersVisible = true;
            pulserConsumptionTable.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;

            pulserConsumptionTable.ColumnCount = 11;

            pulserConsumptionTable.Columns[0].Name = "Ipeak(A)";
            pulserConsumptionTable.Columns[1].Name = "Ihold(A)";
            pulserConsumptionTable.Columns[2].Name = "Tpeak(s)";
            pulserConsumptionTable.Columns[3].Name = "DCR(ohms)";
            pulserConsumptionTable.Columns[4].Name = "Battery(V)";
            pulserConsumptionTable.Columns[5].Name = "IholdBat(A)";
            pulserConsumptionTable.Columns[6].Name = "As/Pulse";
            pulserConsumptionTable.Columns[7].Name = "Ah/Pulse";
            pulserConsumptionTable.Columns[8].Name = "mA";
            pulserConsumptionTable.Columns[9].Name = "Standby(mA)";
            pulserConsumptionTable.Columns[10].Name = "Total";

            string[] row;

            for (int i = 0; i < 10; i++)
            {
                pulserConsumptionTable.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }

            if (smartSolenoidCheckBox.Checked)
            {
                Ipeak = 1;
                Tpeak = 0.05;
            }
            else
            {
                Ipeak = 2;
                Tpeak = 0.2;
            }

            Ihold = 0.5;
            DCR = 8;
            Battery = 28;
            IholdBat = (Ihold * Ihold) * DCR / Battery;

            if (pulseWidth > Tpeak)
            {
                AsPulse = (Tpeak * Ipeak) + ((pulseWidth - Tpeak) * IholdBat);
            }
            else
            {
                AsPulse = pulseWidth * Ipeak;
            }

            AhPulse = AsPulse / 3600;

            mA = AhPulse * pulsesPerHour * 1000;
            Standby = 18;
            Total = mA + Standby;

            row = new string[] { String.Format("{0:0.00}", Ipeak), String.Format("{0:0.00}", Ihold), String.Format("{0:0.00}",Tpeak),
                String.Format("{0:0.00}",DCR), String.Format("{0:0.00}",Battery), String.Format("{0:0.000000}",IholdBat), String.Format("{0:0.000000}",AsPulse),
                String.Format("{0:0.000000}",AhPulse), String.Format("{0:0.00}", mA), String.Format("{0:0.00}", Standby), String.Format("{0:0.00}",Total)};

            pulserConsumptionTable.Rows.Add(row);

            InitializePowerConsumptionTable();
        }

        private void setBatteryButton_Click(object sender, EventArgs e)
        {
            try
            {
                numberOfBatteries = Convert.ToInt32(numberOfBatteriesTextBox.Text);
            }
            catch
            {
                numberOfBatteriesTextBox.Text = "4";
            }

            checkToolString();
        }

        private void resistivitySampleRateComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(resistivitySampleRateComboBox.SelectedIndex == 0)
            {
                rmsResistivityConsumption = mA30Sec;
            }
            else
            {
                rmsResistivityConsumption = mA20Sec;
            }
            InitializePowerConsumptionTable();
            checkToolString();
            processButton.PerformClick();
        }

        public void InitializeTD05Table()
        {
            slotWidth160Table.Rows.Clear();
            slotWidth160Table.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            slotWidth160Table.EnableHeadersVisualStyles = false;

            slotWidth160Table.RowHeadersVisible = true;
            slotWidth160Table.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;

            slotWidth160Table.ColumnCount = 9;

            slotWidth160Table.Columns[0].Name = "Pulse Width";
            slotWidth160Table.Columns[1].Name = "TNP";
            slotWidth160Table.Columns[2].Name = "TD";
            slotWidth160Table.Columns[3].Name = "Slot Width";
            slotWidth160Table.Columns[4].Name = "Bits";
            slotWidth160Table.Columns[5].Name = "M";
            slotWidth160Table.Columns[6].Name = "N";
            slotWidth160Table.Columns[7].Name = "s/Value";
            slotWidth160Table.Columns[8].Name = "bit/s";

            string[] row;

            for (int i = 0; i < 9; i++)
            {
                slotWidth160Table.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }

            TNP05 = 1.5;
            TD05 = 0.5;
            double slotWidth = TD05 * pulseWidth;
            sValue050 = new double[17];
            bitS050 = new double[17];

            for (int i = 1; i <= 17; i++)
            {
                sValue050[i - 1] = N05[i - 1] * slotWidth;
                bitS050[i - 1] = i / (N05[i - 1] * slotWidth);
                row = new string[] {pulseWidth.ToString(), TNP05.ToString(), TD05.ToString(), slotWidth.ToString(),
                    i.ToString(), M05[i-1].ToString(), N05[i-1].ToString(),String.Format("{0:0.00}", N05[i-1] * slotWidth), String.Format("{0:0.00}",i/(N05[i-1]*slotWidth))};

                slotWidth160Table.Rows.Add(row);
            }
        }

        public void InitializeTD067Table()
        {
            slotWidth0213Table.Rows.Clear();
            slotWidth0213Table.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            slotWidth0213Table.EnableHeadersVisualStyles = false;

            slotWidth0213Table.RowHeadersVisible = true;
            slotWidth0213Table.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;

            slotWidth0213Table.ColumnCount = 9;

            slotWidth0213Table.Columns[0].Name = "Pulse Width";
            slotWidth0213Table.Columns[1].Name = "TNP";
            slotWidth0213Table.Columns[2].Name = "TD";
            slotWidth0213Table.Columns[3].Name = "Slot Width";
            slotWidth0213Table.Columns[4].Name = "Bits";
            slotWidth0213Table.Columns[5].Name = "M";
            slotWidth0213Table.Columns[6].Name = "N";
            slotWidth0213Table.Columns[7].Name = "s/Value";
            slotWidth0213Table.Columns[8].Name = "bit/s";

            string[] row;

            for (int i = 0; i < 9; i++)
            {
                slotWidth0213Table.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }

            TNP067 = 1;
            TD067 = 2.0/3.0;
            double slotWidth = TD067 * pulseWidth;
            sValue067 = new double[17];
            bitS067 = new double[17];

            for (int i = 1; i <= 17; i++)
            {
                sValue067[i - 1] = N067[i - 1] * slotWidth;
                bitS067[i - 1] = i / (N067[i - 1] * slotWidth);
                row = new string[] {pulseWidth.ToString(), TNP067.ToString(), String.Format("{0:0.00}", TD067), String.Format("{0:0.000}",slotWidth),
                    i.ToString(), M067[i-1].ToString(), N067[i-1].ToString(),String.Format("{0:0.00}", N067[i-1] * slotWidth), String.Format("{0:0.00}",i/(N067[i-1]*slotWidth))};

                slotWidth0213Table.Rows.Add(row);
            }
        }

        public void InitializeTD625Table()
        {
            slotWidth200Table.Rows.Clear();
            slotWidth200Table.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            slotWidth200Table.EnableHeadersVisualStyles = false;

            slotWidth200Table.RowHeadersVisible = true;
            slotWidth200Table.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;

            slotWidth200Table.ColumnCount = 9;

            slotWidth200Table.Columns[0].Name = "Pulse Width";
            slotWidth200Table.Columns[1].Name = "TNP";
            slotWidth200Table.Columns[2].Name = "TD";
            slotWidth200Table.Columns[3].Name = "Slot Width";
            slotWidth200Table.Columns[4].Name = "Bits";
            slotWidth200Table.Columns[5].Name = "M";
            slotWidth200Table.Columns[6].Name = "N";
            slotWidth200Table.Columns[7].Name = "s/Value";
            slotWidth200Table.Columns[8].Name = "bit/s";

            string[] row;

            for (int i = 0; i < 9; i++)
            {
                slotWidth200Table.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }

            TNP625 = 1.5;
            TD625 = .625;
            double slotWidth = TD625 * pulseWidth;
            sValue625 = new double[17];
            bitS625 = new double[17];

            for (int i = 1; i <= 17; i++)
            {
                sValue625[i - 1] = N625[i - 1] * slotWidth;
                bitS625[i - 1] = i / (N625[i - 1] * slotWidth);
                row = new string[] {pulseWidth.ToString(), TNP625.ToString(), TD625.ToString(), slotWidth.ToString(),
                    i.ToString(), M625[i-1].ToString(), N625[i-1].ToString(),String.Format("{0:0.00}", N625[i-1] * slotWidth), String.Format("{0:0.00}",i/(N625[i-1]*slotWidth))};

                slotWidth200Table.Rows.Add(row);
            }
        }

        public void InitializeResistivityConsumptionTable()
        {
            resistivityPowerConsumptionTable.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            resistivityPowerConsumptionTable.EnableHeadersVisualStyles = false;

            resistivityPowerConsumptionTable.RowHeadersVisible = true;
            resistivityPowerConsumptionTable.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;

            resistivityPowerConsumptionTable.ColumnCount = 9;

            string[] row;

            joulesMin30Sec = 197.0192 / 60;

            watts30sec = 197.0192 / 60 / 28;

            mA30Sec = 197.0192 / 60 / 28 * 1000;

            row = new string[] { "30 Sec", "197.0192", "Joules/ Min", String.Format("{0:0.000000}", joulesMin30Sec), "W", String.Format("{0:0.000000}", watts30sec), "A", String.Format("{0:0.00}", mA30Sec ), "mA" };

            resistivityPowerConsumptionTable.Rows.Add(row);

            joulesMin20Sec = 227.0488 / 60;

            watts30sec = 227.0488 / 60 / 28;

            mA20Sec = 227.0488 / 60 / 28 * 1000;

            row = new string[] { "20 Sec", "227.0488", "Joules/ Min", String.Format("{0:0.000000}", joulesMin20Sec), "W", String.Format("{0:0.000000}", watts20sec), "A", String.Format("{0:0.00}", mA20Sec), "mA" };

            resistivityPowerConsumptionTable.Rows.Add(row);
        }

        public void InitializePowerConsumptionTable()
        {
            powerConsumptionTable.Rows.Clear();

            powerConsumptionTable.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            powerConsumptionTable.EnableHeadersVisualStyles = false;

            powerConsumptionTable.RowHeadersVisible = true;
            powerConsumptionTable.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;

            powerConsumptionTable.ColumnCount = 2;

            powerConsumptionTable.Columns[0].Name = "Tool";
            powerConsumptionTable.Columns[1].Name = "Power Consumption (mA)";

            string[] row = new string[] { "Wolverine TMP w/ Smart Solenoid", String.Format("{0:0}", Total)};
            powerConsumptionTable.Rows.Add(row);

            row = new string[] { "Wolverine Master w/ Directional", "60" };
            powerConsumptionTable.Rows.Add(row);

            row = new string[] { "Gamma", "21" };
            powerConsumptionTable.Rows.Add(row);

            row = new string[] { "Wolverine Translator", "14" };
            powerConsumptionTable.Rows.Add(row);

            row = new string[] { "Wolverine LNDC", "202" };
            powerConsumptionTable.Rows.Add(row);

            row = new string[] { "APS WPR", "175" };
            powerConsumptionTable.Rows.Add(row);

            if (resistivitySampleRateComboBox.SelectedIndex == 0)
            {
                rmsResistivityConsumption = mA30Sec;
                row = new string[] { "RMS Resistivity", String.Format("{0:0.00}", rmsResistivityConsumption) };
                powerConsumptionTable.Rows.Add(row);
            }
            else
            {
                rmsResistivityConsumption = mA20Sec;
                row = new string[] { "RMS Resistivity", String.Format("{0:0.00}", rmsResistivityConsumption) };
                powerConsumptionTable.Rows.Add(row);
            }

            rmsPressureConsumption = 15.0 + 10.0 * (1.0 / 8.0);
            row = new string[] { "RMS Pressure Tool", String.Format("{0:0.00}", rmsPressureConsumption) };
            powerConsumptionTable.Rows.Add(row);

            row = new string[] { "Wolverine Microhop", "32" };
            powerConsumptionTable.Rows.Add(row);

            row = new string[] { "Single Battery Life", "26" };
            powerConsumptionTable.Rows.Add(row);

            singleBatteryLife = 26.0;

            checkToolString();
            updatePieChart();
        }

        private void smartSolenoidCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            InitializePulserConsumptionTable();
            processButton.PerformClick();
        }

        private void pulseWidthTextBox_TextChanged(object sender, EventArgs e)
        {
            pulseWidth = Convert.ToDouble(pulseWidthTextBox.Text);
            InitializeTD05Table();
            InitializeTD067Table();
            InitializeTD625Table();
            InitializePowerConsumptionTable();
        }

        private void setPwButton_Click(object sender, EventArgs e)
        {
            pulseWidthTextBox.BackColor = Color.Yellow;
            pulseWidth = Convert.ToDouble(pulseWidthTextBox.Text);
            InitializeTD05Table();
            InitializeTD067Table();
            InitializeTD625Table();
            InitializePowerConsumptionTable();
            processButton.PerformClick();
        }

        public void InitializePulserSettings()
        {
            pulseWidth = Convert.ToDouble(pulseWidthTextBox.Text);
            tdTmpComboBox.SelectedIndex = tdTmpComboBox.Items.IndexOf("1.50, 0.50");
            resistivitySampleRateComboBox.SelectedIndex = resistivitySampleRateComboBox.Items.IndexOf("30 Sec");
        }

        public void checkToolString()
        {
            totalCurrentDraw = 0;
            toolTable.Rows.Clear();
            string[] row;

            //Wolverine TMP
            if (wolverineTmpSmartSolenoid.Checked)
            {
                row = new string[] { "Wolverine TMP", "YES", String.Format("{0:0.00}", Total) };
                totalCurrentDraw += Total;
            }
            else
                row = new string[] { "Wolverine TMP", "NO", String.Format("{0:0.00}", Total) };

            toolTable.Rows.Add(row);

            //Wolverine Master
            if (wolverineMasterWDirectional.Checked)
            {
                row = new string[] { "Wolverine Master", "YES", "60" };
                totalCurrentDraw += 60;
            }
            else
                row = new string[] { "Wolverine Master", "NO", "60"};

            toolTable.Rows.Add(row);

            //Gamma
            if (gamma.Checked)
            {
                row = new string[] { "Gamma", "YES", "21" };
                totalCurrentDraw += 21;
            }
            else
                row = new string[] { "Gamma", "NO", "21" };

            toolTable.Rows.Add(row);

            //Wolverine Translator
            if (wolverineTranslator.Checked)
            {
                row = new string[] { "Wolverine Translator", "YES", "14" };
                totalCurrentDraw += 14;
            }
            else
                row = new string[] { "Wolverine Translator", "NO", "14" };

            toolTable.Rows.Add(row);

            //RMS RESISTIVITY
            if (rmsResistivtyCheckbox.Checked)
            {
                row = new string[] { "RMS Resistivity", "YES", String.Format("{0:0.00}", rmsResistivityConsumption) };
                totalCurrentDraw += rmsResistivityConsumption;
            }
            else
                row = new string[] { "RMS Resistivity", "NO", String.Format("{0:0.00}", rmsResistivityConsumption) };

            toolTable.Rows.Add(row);

            //RMS Pressure Tool
            if (rmsPressureToolCheckbox.Checked)
            {
                row = new string[] { "RMS Pressure Tool", "YES", String.Format("{0:0.00}", rmsPressureConsumption) };
                totalCurrentDraw += rmsPressureConsumption;
            }
            else
                row = new string[] { "RMS Pressure Tool", "NO", String.Format("{0:0.00}", rmsPressureConsumption) };

            toolTable.Rows.Add(row);

            //Wolverine Microhop
            if (wolverineMicrohopCheckbox.Checked)
            {
                row = new string[] { "Wolverine Microhop", "YES", "32" };
                totalCurrentDraw += 32;
            }
            else
                row = new string[] { "Wolverine Microhop", "NO", "32" };

            toolTable.Rows.Add(row);

            //Wolverine LNDC
            if (wolverineLndcCheckbox.Checked)
            {
                row = new string[] { "Wolverine LNDC", "YES", "202" };
                totalCurrentDraw += 202;
            }
            else
                row = new string[] { "Wolverine LNDC", "NO", "202" };

            toolTable.Rows.Add(row);

            //APS WPR
            if (apsWpr.Checked)
            {
                row = new string[] { "APS WPR", "YES", "165" };
                totalCurrentDraw += 175;
            }
            else
                row = new string[] { "APS", "NO", "165" };

            toolTable.Rows.Add(row);

            //Total
            row = new string[] { "", "Total", String.Format("{0:0.00}", totalCurrentDraw) };
            toolTable.Rows.Add(row);

            RunConsumptionCalcuation();

            updatePieChart();
        }

        private void wolverineTmpSmartSolenoid_CheckedChanged(object sender, EventArgs e)
        {
            checkToolString();
        }

        private void wolverineMasterWNovDirectional_CheckedChanged(object sender, EventArgs e)
        {
            checkToolString();
        }

        private void gamma_CheckedChanged(object sender, EventArgs e)
        {
            checkToolString();
        }

        private void wolverineTranslator_CheckedChanged(object sender, EventArgs e)
        {
            checkToolString();
        }

        private void rmsResistivtyCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            checkToolString();
        }

        private void rmsPressureToolCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            checkToolString();
        }

        private void wolverineMicrohopCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            checkToolString();
        }

        private void wolverineLndcCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            checkToolString();
        }

        private void numberOfBatteriesTextBox_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {
            numberOfBatteries = Convert.ToInt32(numberOfBatteriesTextBox.Text);
            numberOfBatteriesTextBox.BackColor = Color.Yellow;
            RunConsumptionCalcuation();
        }
    }
}
