﻿//--------------------------------------|---------------------------------------
//                                PC RMS V1.0.0.0
//                                File: TIB Commands.cs
//------------------------------------------------------------------------------
// Author: Thuan Vu
// Created on April 14, 2021
//--------------------------------------|---------------------------------------
//  This drawing/document is copyright and the property of
//  Wolverine OilField Technologies LLC. and Remote Measurement Systems Ltd.
//  It must not be copied (in whole or in part),
//  used for manufacture or otherwise disclosed without prior written consent.
//  Any  copies of this drawing/document made by any method must also
//  include a copy of this legend. This document is supplied  without
//  liability for errors or omissions.
//
// (c) COPYRIGHT WOLVERINE OILFIELD TECHNOLOGIES LLC. AND
//               REMOTE MEASUREMENT SYSTEMS LIMITED 2021.
//-----------------------------------------------------------------------------
using NodeCommon;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Threading.Tasks;

namespace PC_PDD
{
    class TIB_Commands
    {
        CommandProcessor cp = new CommandProcessor();
        MessageHandler MH = new MessageHandler();

        public static bool qbus;

        public async Task<bool> qBusEnabled(Wolverine_Field_Applications.MainForm neutrinoTracker, ToolComms tc, SerialPort serialPort)
        {
            byte[] baudBytes = Common.ACTION_QBUS_ON;
            byte[] baudBytesPadded = new byte[Common.ACTION_PARAMETER_LENGTH];
            var startAt = baudBytesPadded.Length - baudBytes.Length;
            Array.Copy(baudBytes, 0, baudBytesPadded, 0, baudBytes.Length);
            List<byte> baudList = baudBytesPadded.ToList();
            List<byte> baudCMD = new List<byte>();
            baudCMD.Add(Common.TIC_ACTION_SET_BAUDRATE);
            //byte[] message = cp.buildMessage(Common.NODE_TIC, Common.CMD_ACTION, baudCMD);
            byte[] message = cp.buildMessage(Common.NODE_TIC, Common.CMD_ACTION, Common.ACTION_QBUS_ON[0], Common.ACTION_QBUS_ON[1]);

            if (!serialPort.IsOpen) // Is our serial port open?
                return false;

            if (await tc.sendCommand(message, serialPort))
                neutrinoTracker.Log(Wolverine_Field_Applications.MainForm.LogMsgType0.Outgoing, "TX: " + BitConverter.ToString(message), 1);
            else
            {
                return false; // message failed
            }

            string msgString = BitConverter.ToString(tc.rxNoEcho.ToArray());
            neutrinoTracker.Log(Wolverine_Field_Applications.MainForm.LogMsgType0.Outgoing, "RX: " + msgString + '\n', 1);

            if (MH.checkSumVerify(tc.rxNoEcho.ToArray()))
            {
                if (tc.rxNoEcho.ToArray()[5] == Common.ACK)
                {
                    await Task.Delay(10);
                    qbus = true;
                }
            }
            return true;
        }

        public async Task<bool> qBusEnabled(Wolverine_Field_Applications.MainForm neutrinoTracker, ToolComms tc, FTD2XX_NET.FTDI myFtdiDevice, FTD2XX_NET.FTDI.FT_STATUS ftStatus)
        {
            byte[] baudBytes = Common.ACTION_QBUS_ON;
            byte[] baudBytesPadded = new byte[Common.ACTION_PARAMETER_LENGTH];
            var startAt = baudBytesPadded.Length - baudBytes.Length;
            Array.Copy(baudBytes, 0, baudBytesPadded, 0, baudBytes.Length);
            List<byte> baudList = baudBytesPadded.ToList();
            List<byte> baudCMD = new List<byte>();
            baudCMD.Add(Common.TIC_ACTION_SET_BAUDRATE);
            //byte[] message = cp.buildMessage(Common.NODE_TIC, Common.CMD_ACTION, baudCMD);
            byte[] message = cp.buildMessage(Common.NODE_TIC, Common.CMD_ACTION, Common.ACTION_QBUS_ON[0], Common.ACTION_QBUS_ON[1]);

            if (!myFtdiDevice.IsOpen) // Is our serial port open?
                return false;

            if (await tc.FTDI_SEND(message, myFtdiDevice, ftStatus, neutrinoTracker, neutrinoTracker.getLocalEchoStatus())) { }
            else
            {
                return false; // message failed
            }

            if (MH.checkSumVerify(tc.rxNoEcho.ToArray()))
            {
                if (tc.rxNoEcho.ToArray()[5] == Common.ACK)
                {
                    await Task.Delay(10);
                    qbus = true;
                }
            }
            return true;
        }

        public async Task<bool> qBusDisabled(Wolverine_Field_Applications.MainForm neutrinoTracker, ToolComms tc, SerialPort serialPort)
        {
            byte[] baudBytes = Common.ACTION_QBUS_OFF;
            byte[] baudBytesPadded = new byte[Common.ACTION_PARAMETER_LENGTH];
            var startAt = baudBytesPadded.Length - baudBytes.Length;
            Array.Copy(baudBytes, 0, baudBytesPadded, 0, baudBytes.Length);
            List<byte> baudList = baudBytesPadded.ToList();
            List<byte> baudCMD = new List<byte>();
            baudCMD.Add(Common.TIC_ACTION_SET_BAUDRATE);
            //byte[] message = cp.buildMessage(Common.NODE_TIC, Common.CMD_ACTION, baudCMD);
            byte[] message = cp.buildMessage(Common.NODE_TIC, Common.CMD_ACTION, Common.ACTION_QBUS_OFF[0], Common.ACTION_QBUS_OFF[1]);

            if (!serialPort.IsOpen) // Is our serial port open?
                return false;

            if (await tc.sendCommand(message, serialPort)) { }
            //neutrinoTracker.Log(global::NeutrinoTracker.Log.Outgoing, "TX: " + BitConverter.ToString(message), 1);
            else
            {
                return false; // message failed
            }

            string msgString = BitConverter.ToString(tc.rxNoEcho.ToArray());
            //neutrinoTracker.Log(global::NeutrinoTracker.Log.Incoming, "RX: " + msgString + '\n', 1);

            if (MH.checkSumVerify(tc.rxNoEcho.ToArray()))
            {
                if (tc.rxNoEcho.ToArray()[5] == Common.ACK)
                {
                    await Task.Delay(10);
                    qbus = false;
                }
            }
            return true;
        }

        public async Task<bool> qBusDisabled(Wolverine_Field_Applications.MainForm neutrinoTracker, ToolComms tc, FTD2XX_NET.FTDI myFtdiDevice, FTD2XX_NET.FTDI.FT_STATUS ftStatus)
        {
            byte[] baudBytes = Common.ACTION_QBUS_OFF;
            byte[] baudBytesPadded = new byte[Common.ACTION_PARAMETER_LENGTH];
            var startAt = baudBytesPadded.Length - baudBytes.Length;
            Array.Copy(baudBytes, 0, baudBytesPadded, 0, baudBytes.Length);
            List<byte> baudList = baudBytesPadded.ToList();
            List<byte> baudCMD = new List<byte>();
            baudCMD.Add(Common.TIC_ACTION_SET_BAUDRATE);
            //byte[] message = cp.buildMessage(Common.NODE_TIC, Common.CMD_ACTION, baudCMD);
            byte[] message = cp.buildMessage(Common.NODE_TIC, Common.CMD_ACTION, Common.ACTION_QBUS_OFF[0], Common.ACTION_QBUS_OFF[1]);

            if (!myFtdiDevice.IsOpen) // Is our serial port open?
                return false;

            if (await tc.FTDI_SEND(message, myFtdiDevice, ftStatus, neutrinoTracker, neutrinoTracker.getLocalEchoStatus())) { }
            else
            {
                return false; // message failed
            }

            if (MH.checkSumVerify(tc.rxNoEcho.ToArray()))
            {
                if (tc.rxNoEcho.ToArray()[5] == Common.ACK)
                {
                    await Task.Delay(10);
                    qbus = false;
                }
            }
            return true;
        }

        public async Task<bool> setBaud(Wolverine_Field_Applications.MainForm neutrinoTracker, int setBoxBaudRate, ToolComms tc, SerialPort serialPort)
        {
            // Send Baud Rate change to TIB
            byte[] baudBytes = BitConverter.GetBytes(setBoxBaudRate);
            byte[] baudBytesPadded = new byte[Common.ACTION_PARAMETER_LENGTH];
            var startAt = baudBytesPadded.Length - baudBytes.Length;
            Array.Copy(baudBytes, 0, baudBytesPadded, 0, baudBytes.Length);
            List<byte> baudList = baudBytesPadded.ToList();
            List<byte> baudCMD = new List<byte>();
            baudCMD.Add(Common.TIC_ACTION_SET_BAUDRATE);
            baudCMD.AddRange(baudList);
            byte[] message = cp.buildMessage(Common.NODE_TIC, Common.CMD_ACTION, baudCMD);
            //mbyte[] message = cp.buildMessage(Common.NODE_TIC, Common.CMD_ACTION, baudBytes.ToList());

            if (!serialPort.IsOpen)
                return false;

            neutrinoTracker.Log(Wolverine_Field_Applications.MainForm.LogMsgType0.Outgoing, "TX: " + BitConverter.ToString(message), 1);

            if (await tc.sendCommand(message, serialPort))
            {
                await Task.Delay(100);
            }
            else
            {
                neutrinoTracker.Log(Wolverine_Field_Applications.MainForm.LogMsgType0.Outgoing, "RX: ", 1);
                serialPort.BaudRate = setBoxBaudRate;
                if (await tc.sendCommand(message, serialPort))
                {
                    await Task.Delay(100);
                }
                else
                {
                    serialPort.BaudRate = 9600;
                    return false; // message failed
                }
            }

            string msgString = BitConverter.ToString(tc.rxNoEcho.ToArray());
            neutrinoTracker.Log(Wolverine_Field_Applications.MainForm.LogMsgType0.Outgoing, "RX: " + msgString + '\n', 1);

            if (MH.checkSumVerify(tc.rxNoEcho.ToArray()))
            {
                if (tc.rxNoEcho.ToArray()[5] == Common.ACK)
                {
                    serialPort.BaudRate = setBoxBaudRate;
                    await Task.Delay(50);
                    return true;
                }
            }

            //Log(global::NeutrinoTracker.Log.Outgoing, "Set Baud " + setBoxBaudRate + " Unsuccessful!", 2);
            return false;
        }

        public async Task<bool> setBaud(Wolverine_Field_Applications.MainForm neutrinoTracker, int setBoxBaudRate, ToolComms tc, FTD2XX_NET.FTDI myFtdiDevice, FTD2XX_NET.FTDI.FT_STATUS ftStatus)
        {
            // Send Baud Rate change to TIB
            byte[] baudBytes = BitConverter.GetBytes(setBoxBaudRate);
            byte[] baudBytesPadded = new byte[Common.ACTION_PARAMETER_LENGTH];
            var startAt = baudBytesPadded.Length - baudBytes.Length;
            Array.Copy(baudBytes, 0, baudBytesPadded, 0, baudBytes.Length);
            List<byte> baudList = baudBytesPadded.ToList();
            List<byte> baudCMD = new List<byte>();
            baudCMD.Add(Common.TIC_ACTION_SET_BAUDRATE);
            baudCMD.AddRange(baudList);
            byte[] message = cp.buildMessage(Common.NODE_TIC, Common.CMD_ACTION, baudCMD);
            //mbyte[] message = cp.buildMessage(Common.NODE_TIC, Common.CMD_ACTION, baudBytes.ToList());

            if (!myFtdiDevice.IsOpen)
                return false;

            if (await tc.FTDI_SEND(message, myFtdiDevice, ftStatus, neutrinoTracker, neutrinoTracker.getLocalEchoStatus())){}
            else
            {
                ftStatus = myFtdiDevice.SetBaudRate(Convert.ToUInt32(setBoxBaudRate));
                if (await tc.FTDI_SEND(message, myFtdiDevice, ftStatus, neutrinoTracker, neutrinoTracker.getLocalEchoStatus()))
                {
                    await Task.Delay(100);
                }
                else
                {
                    ftStatus = myFtdiDevice.SetBaudRate(9600);
                    return false; // message failed
                }
            }

            if (MH.checkSumVerify(tc.rxNoEcho.ToArray()))
            {
                if (tc.rxNoEcho.ToArray()[5] == Common.ACK)
                {
                    ftStatus = myFtdiDevice.SetBaudRate(Convert.ToUInt32(setBoxBaudRate));
                    await Task.Delay(50);
                    return true;
                }
            }

            //Log(global::NeutrinoTracker.Log.Outgoing, "Set Baud " + setBoxBaudRate + " Unsuccessful!", 2);
            return false;
        }

        public async Task<bool> getTIBFwId(Wolverine_Field_Applications.MainForm neutrinoTracker, ToolComms tc, SerialPort serialPort, StructureVariables sV)
        {
            deserializeStruct conv = new deserializeStruct();
            byte[] message = cp.buildMessage(Common.NODE_TIC, Common.CMD_FIRMWARE_ID_RD);
            if (!serialPort.IsOpen)
                return false;

            if (await tc.sendCommand(message, serialPort))
                neutrinoTracker.Log(Wolverine_Field_Applications.MainForm.LogMsgType0.Outgoing, "TX: " + BitConverter.ToString(message), 1);
            else
            {
                neutrinoTracker.Log(Wolverine_Field_Applications.MainForm.LogMsgType0.Outgoing, "FW ID Read TimeOut", 1);
            }

            string msgString = BitConverter.ToString(tc.rxNoEcho.ToArray());
            neutrinoTracker.Log(Wolverine_Field_Applications.MainForm.LogMsgType0.Outgoing, "RX: " + msgString + '\n', 1);

            if (MH.checkSumVerify(tc.rxNoEcho.ToArray()))
            {
                try
                {
                    sV.ticFwId = conv.FromByteArray<
                        StructureVariables.TIBFirmwareID>(tc.rxData.ToArray());

                    char result;
                    var results = new char[2];
                    var i = 0;
                    foreach (var value in sV.ticFwId.STRING)
                        try
                        {
                            result = Convert.ToChar(value);
                            results[i++] = result;
                        }
                        catch
                        {
                            return false;
                        }

                    return true;
                }
                catch
                {
                    return false;
                }
            }
            else
            {
                //neutrinoTracker.Log(global::NeutrinoTracker.Log.Outgoing, "CheckSum FAIL", 2);
                //neutrinoTracker.Log(global::NeutrinoTracker.Log.Outgoing, "-----------------------------------------", 2);
                return false;
            }
        }

        public async Task<bool> getTIBFwId(Wolverine_Field_Applications.MainForm neutrinoTracker, ToolComms tc, FTD2XX_NET.FTDI myFtdiDevice, FTD2XX_NET.FTDI.FT_STATUS ftStatus, StructureVariables sV)
        {
            deserializeStruct conv = new deserializeStruct();
            byte[] message = cp.buildMessage(Common.NODE_TIC, Common.CMD_FIRMWARE_ID_RD);

            if (!myFtdiDevice.IsOpen)
                return false;

            if (await tc.FTDI_SEND(message, myFtdiDevice, ftStatus, neutrinoTracker, neutrinoTracker.getLocalEchoStatus())) { }
            else
            {
                neutrinoTracker.Log(Wolverine_Field_Applications.MainForm.LogMsgType0.Outgoing, "FW ID Read TimeOut", 1);
            }

            if (MH.checkSumVerify(tc.rxNoEcho.ToArray()))
            {
                try
                {
                    sV.ticFwId = conv.FromByteArray<
                        StructureVariables.TIBFirmwareID>(tc.rxData.ToArray());

                    char result;
                    var results = new char[2];
                    var i = 0;
                    foreach (var value in sV.ticFwId.STRING)
                        try
                        {
                            result = Convert.ToChar(value);
                            results[i++] = result;
                        }
                        catch
                        {
                            return false;
                        }

                    return true;
                }
                catch
                {
                    return false;
                }
            }
            else
            {
                //neutrinoTracker.Log(global::NeutrinoTracker.Log.Outgoing, "CheckSum FAIL", 2);
                //neutrinoTracker.Log(global::NeutrinoTracker.Log.Outgoing, "-----------------------------------------", 2);
                return false;
            }
        }
    }
}
