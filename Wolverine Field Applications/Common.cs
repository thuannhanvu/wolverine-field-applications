﻿//--------------------------------------|---------------------------------------
//                                PC RMS V1.0.0.0
//                                File: Common.cs
//------------------------------------------------------------------------------
// Author: Thuan Vu
// Created on April 14, 2021
//--------------------------------------|---------------------------------------
//  This drawing/document is copyright and the property of
//  Wolverine OilField Technologies LLC. and Remote Measurement Systems Ltd.
//  It must not be copied (in whole or in part),
//  used for manufacture or otherwise disclosed without prior written consent.
//  Any  copies of this drawing/document made by any method must also
//  include a copy of this legend. This document is supplied  without
//  liability for errors or omissions.
//
// (c) COPYRIGHT WOLVERINE OILFIELD TECHNOLOGIES LLC. AND
//               REMOTE MEASUREMENT SYSTEMS LIMITED 2021.
//-----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;

namespace PC_PDD
{
    internal class Common
    {
        #region Properties  ---------------------------------------------------------------------------------------------------

        // Constants
        public const string CONFIG_FILENAME = "PcMwdConfig.xml";

        #region Node Constants
        //Hardware Revisions
        public const string MWD_HARDWARE = "Rev. 3", RNAV_HARDWARE = "Rev. 1", TMP_HARDWARE = "Rev. 0", NDCALIPER_HARDWARE = "Rev. 1";

        public enum DATETIME { YEAR = 0, MONTH, DAY, HOUR, MINUTE, SECOND };

        // Memory sync
        public const byte MASTER_MEMORY_PAGE_HEADER = 0xAB;
        public const byte MASTER_MEMORY_RECORD_SYNC_HEADER = 0xCD;

        //Communication Protocols
        public enum COM_PROTOCOL { MSG_TO = 0, MSG_FROM, MSG_LENGTH0, MSG_LENGTH1, MSG_CMD, MSG_DATA };

        // Node Addresses
        public static readonly uint[] NodeIdMpdMaster = { 0x83, 0x03, 115200, 9600, 230400, 8, 1 }; // Test code only 12-19-18 RP

        public const byte NODE_COMPUTER = 0x01;
        public const byte NODE_TMP = 0x02;
        public const byte NODE_PULSER_DRIVER = 0x06;
        public const byte NODE_MASTER = 0x03;
        public const byte NODE_RNAV = 0x05;
        public const byte NODE_TIC = 0x11;
        public const byte NODE_NDCALIPER = 0x13;
        public const byte NODE_NDMASTER = 0x14;
        public const byte NODE_PDD = 0x40;
        public const byte NODE_MFPWR = 0x41;
        public const byte NODE_DIGITAL_GAMMA = 0x04;
        public const byte NODE_UVT = 0x07;

        // Communication Protocol
        public const byte ACK = 0x00;
        public const byte NACK = 0xFF;

        // Command States
        public const int STATE_SYSTEM_RECORD_READ = 1;
        public const int STATE_DATAGROUP1_READ = 2;
        public const int STATE_CONNECT_TO_TOOL = 3;
        public const int STATE_RUN_TOOL = 4;
        public const int STATE_DOWNLOAD_TOOL = 5;
        public const int STATE_ERASE_TOOL = 6;

        // Action Types
        public const int TIC_ACTION_NONE = 0;
        public const int TIC_ACTION_FORCE_FLOW = 1;
        public const int TIC_ACTION_FORCE_PULSE = 2;
        public const int TIC_ACTION_FORCE_PULSE_TOGGLE = 3;
        public const int TIC_ACTION_GET_CURRENT = 4;
        public const int TIC_ACTION_GAMMA_COUNTS = 5;
        public const int TIC_ACTION_SET_BAUDRATE = 6;
        public const int TIC_ACTION_SET_COM_TYPE = 7;
        public const int TIC_ACTION_SET_RS485_9BIT_MODE = 8;
        public const int TIC_ACTION_SET_ALTERNATE_PROTOCOL_MODE = 9;
        public const int TIC_ACTION_SET_PORT_FORWARDING = 10;

        //ND MASTER SET BAUD ACTION CMD
        public const byte ND_MASTER_SET_BAUD = 0x0E;

        // Action Command Qbus Enable
        public static readonly byte[] ACTION_QBUS_ON = { 0x07, 0x00 };
        public static readonly byte[] ACTION_QBUS_OFF = { 0x07, 0x01 };

        // Action Command Parameter Size
        public static int ACTION_PARAMETER_LENGTH = 16;

        public const int BAUD_APPLICATION = 9600;
        public const int BAUD_DOWNLOAD = 115200;
        public const int BAUD_RNAV_DOWNLOAD = 333333;
        public const int BAUD_TMP_DOWNLOAD = 307200;
        public const int BAUD_ND_APPLICATION = 9600;
        public const int BAUD_ND_DOWNLOAD = 333333;
        public const int BAUD_NM_DOWNLOAD = 333333;

        // Message Protocol
        public const byte MESSAGE_OVERHEAD = 6;
        public const byte BROADCAST = 0xFF;

        // Command Codes (All Nodes)
        public const byte CMD_FIRMWARE_ID_RD = 0x01;
        public const byte CMD_MODE_RDWR = 0x02;
        public const byte CMD_SERIALNUMBER_RDWR = 0x03;
        public const byte CMD_NONVOLATILE_RDWR = 0x04;
        public const byte CMD_DATAGROUP_RDWR = 0x05;
        public const byte CMD_ACTION = 0x06;
        public const byte CMD_SYSTEM_RDWR = 0x07;
        public const byte CMD_TIME_RDWR = 0x08;
        public const byte CMD_MEMORY_RDWR = 0x09;
        public const byte CMD_MEMORY_ERASE = 0x0A;
        public const byte CMD_PROGRAM_WR = 0x0B;
        public const byte CMD_SET_BAUD = 0x0E;
        public const byte CMD_SERVICERECORD_RDWR = 0x0F;

        // Action Command
        public const byte ACTION_WRITE_MEM_PAGE = 0x08;
        public static readonly byte[] ACTION_SET_BAT1 = { 0x01, 0x00 };
        public static readonly byte[] ACTION_SET_BAT2 = { 0x01, 0x01 };
        public static readonly byte[] ACTION_FLOW_ON = { 0x02, 0x01 };
        public static readonly byte[] ACTION_FLOW_OFF = { 0x02, 0x00 };
        public static readonly byte[] NDCC_FLOW_ON = { 0x01, 0x01 };
        public static readonly byte[] NDCC_FLOW_OFF = { 0x01, 0x00 };
        public static readonly byte[] ACTION_GO_TO_BOOT = { 0x07, 0x01 };
       

        // Operating modes (All Nodes)
        public const byte MODE_NONE = 0x00;
        public const byte MODE_STARTUP = 0x01;
        public const byte MODE_IDLE = 0x02;
        public const byte MODE_TRIP = 0x03;
        public const byte MODE_ACTIVE = 0x04;
        public const byte MODE_DOWNLOAD = 0x05;
        public const byte MODE_CALIBRATION = 0x06;
        public const byte MODE_ROLLTEST = 0x06;
        public const byte MODE_BOOT = 0x0B;

        // Datagroups
        public const byte DATAGROUP_1 = 0x01;
        public const byte DATAGROUP_2 = 0x02;
        public const byte DATAGROUP_3 = 0x03;
        public const byte DATAGROUP_4 = 0x04;
        public const byte DATAGROUP_5 = 0x05;
        public const byte DATAGROUP_6 = 0x06;
        public const byte DATAGROUP_7 = 0x07;
        public const byte DATAGROUP_8 = 0x08;
        public const byte DATAGROUP_9 = 0x09;
        public const byte DATAGROUP_10 = 0x0A;
        public const byte DATAGROUP_11 = 0x0B;
        public const byte DATAGROUP_12 = 0x0C;
        #endregion Constants

        #region PDD V1.02
        //Tool Selection
        public const bool PDD_SELECT = false;

        //Action Type
        public const int PDD_ACTION_NONE = 0;
        public const int PDD_ACTION_TEST_WRITE_MEMPAGE = 1;
        public const int PDD_ACTION_ACQUIRE_PRESSURE = 2;
        public const int PDD_ACTION_FORCE_FLOW = 3;
        public const int PDD_ACTION_TAKE_SURVEY = 4;

        // Datagroup types
        public const byte PDD_DATAGROUP_1 = 0x01;
        public const byte PDD_DATAGROUP_2 = 0x02;
        public const byte PDD_DATAGROUP_3 = 0x03;

        // NonVolatile types
        public const byte PDD_NONVOLATILE_CONFIGURATION = 0x01;
        public const byte PDD_NONVOLATILE_CALIBRATION = 0x02;
        public const byte PDD_NONVOLATILE_SERVICE_RECORD = 0x03;

        // Generic variable assignments
        enum PDD_GV { PDD_GV_DISABLED = 0, PDD_GV_PB_STATIC, PDD_GV_PB_CIRC, PDD_GV_PA_CIRC };

        //RG INSERT RMS CODE
        // Default configuration
        public const int PDD_DEFAULT_CONFIG_ADC_SAMPLE_DURATION_PERIOD_SECONDS = 1;
        public const int PDD_DEFAULT_CONFIG_ADC_SAMPLE_INTERVAL_PERIOD_SECONDS = 8;
        public const int PDD_DEFAULT_CONFIG_FLOW_SAMPLE_INTERVAL = 60;
        public const int PDD_DEFAULT_CONFIG_PDD_DEFAULT_CONFIG_FLOW_OFF_TIMEOUT = 300;
        public const double PDD_DEFAULT_CONFIG_QBUS_ADDRESS = 21.0;
        public const int PDD_DEFAULT_CONFIG_SURVEY_DELAY_SECONDS = 300;
        public const double PDD_DEFAULT_CONFIG_BATTERY_VOLTS_SCALING = 0.059135;     // Scaling 59.135mV per lsb
        public const double PDD_DEFAULT_CONFIG_BATTERY_CURRENT_SCALING = 0.00015;    // Scaling 15mA per lsb
        public const double PDD_DEFAULT_CONFIG_BOARD_TEMP_SCALING = 1.0;             // Board Temperature scale factor
        public const double PDD_DEFAULT_CONFIG_BOARD_TEMP_OFFSET = 0.0;              // Board Temperature scale factor

        public const int PDD_DEFAULT_CONFIG_NOT_USED = 0;
        public const int PDD_DEFAULT_CONFIG_STATIC_ANNULAR = 1;
        public const int PDD_DEFAULT_CONFIG_CIRCULAR_ANNULAR = 2;
        public const int PDD_DEFAULT_CONFIG_CIRCULAR_BORE = 3;

        public const int PDD_DEFAULT_CONFIG_GV0_ASSIGNMENT = PDD_DEFAULT_CONFIG_STATIC_ANNULAR;
        public const int PDD_DEFAULT_CONFIG_GV1_ASSIGNMENT = PDD_DEFAULT_CONFIG_CIRCULAR_ANNULAR;
        public const int PDD_DEFAULT_CONFIG_GV2_ASSIGNMENT = PDD_DEFAULT_CONFIG_CIRCULAR_BORE;
        public const int PDD_DEFAULT_CONFIG_GV3_ASSIGNMENT = PDD_DEFAULT_CONFIG_NOT_USED;
        public const int PDD_DEFAULT_CONFIG_GV4_ASSIGNMENT = PDD_DEFAULT_CONFIG_NOT_USED;
        public const int PDD_DEFAULT_CONFIG_GV5_ASSIGNMENT = PDD_DEFAULT_CONFIG_NOT_USED;
        public const int PDD_DEFAULT_CONFIG_GV6_ASSIGNMENT = PDD_DEFAULT_CONFIG_NOT_USED;
        public const int PDD_DEFAULT_CONFIG_GV7_ASSIGNMENT = PDD_DEFAULT_CONFIG_NOT_USED;

        // Default calibration
        // Annular Pressure Coefficients, default values    
        // AX^4 
        public const double PDD_DEFAULT_CAL0_PA_SCALE_K4 = 0.000000039732;
        public const double PDD_DEFAULT_CAL0_PA_SCALE_K3 = -0.0000121493;
        public const double PDD_DEFAULT_CAL0_PA_SCALE_K2 = 0.001150557;
        public const double PDD_DEFAULT_CAL0_PA_SCALE_K1 = -0.03475493;
        public const double PDD_DEFAULT_CAL0_PA_SCALE_K0 = 0.653507437;
        // BX^3 
        public const double PDD_DEFAULT_CAL1_PA_SCALE_K4 = -0.000000385362;
        public const double PDD_DEFAULT_CAL1_PA_SCALE_K3 = 0.000116822;
        public const double PDD_DEFAULT_CAL1_PA_SCALE_K2 = -0.010746194;
        public const double PDD_DEFAULT_CAL1_PA_SCALE_K1 = 0.290478229;
        public const double PDD_DEFAULT_CAL1_PA_SCALE_K0 = -5.681086946;
        // CX^2 
        public const double PDD_DEFAULT_CAL2_PA_SCALE_K4 = 0.00000108467;
        public const double PDD_DEFAULT_CAL2_PA_SCALE_K3 = -0.000328571;
        public const double PDD_DEFAULT_CAL2_PA_SCALE_K2 = 0.028645683;
        public const double PDD_DEFAULT_CAL2_PA_SCALE_K1 = -0.48117063;
        public const double PDD_DEFAULT_CAL2_PA_SCALE_K0 = 9.523450136;
        // DX 
        public const double PDD_DEFAULT_CAL3_PA_SCALE_K4 = -0.000000404956;
        public const double PDD_DEFAULT_CAL3_PA_SCALE_K3 = 0.000191553;
        public const double PDD_DEFAULT_CAL3_PA_SCALE_K2 = -0.016753097;
        public const double PDD_DEFAULT_CAL3_PA_SCALE_K1 = -0.488198394;
        public const double PDD_DEFAULT_CAL3_PA_SCALE_K0 = 3620.01305;
        // E 
        public const double PDD_DEFAULT_CAL4_PA_SCALE_K4 = 0.00000111891;
        public const double PDD_DEFAULT_CAL4_PA_SCALE_K3 = -0.000394344;
        public const double PDD_DEFAULT_CAL4_PA_SCALE_K2 = 0.039311207;
        public const double PDD_DEFAULT_CAL4_PA_SCALE_K1 = -1.281163543;
        public const double PDD_DEFAULT_CAL4_PA_SCALE_K0 = -1753.323476;

        // Bore Pressure Coefficients, default values    
        // AX^4 
        public const double PDD_DEFAULT_CAL0_PB_SCALE_K4 = 0.00000004903;
        public const double PDD_DEFAULT_CAL0_PB_SCALE_K3 = -0.0000198157;
        public const double PDD_DEFAULT_CAL0_PB_SCALE_K2 = 0.002750366;
        public const double PDD_DEFAULT_CAL0_PB_SCALE_K1 = -0.152394295;
        public const double PDD_DEFAULT_CAL0_PB_SCALE_K0 = 2.981013321;
        // BX^3 
        public const double PDD_DEFAULT_CAL1_PB_SCALE_K4 = -0.00000047859;
        public const double PDD_DEFAULT_CAL1_PB_SCALE_K3 = 0.000195463;
        public const double PDD_DEFAULT_CAL1_PB_SCALE_K2 = -0.027294489;
        public const double PDD_DEFAULT_CAL1_PB_SCALE_K1 = 1.513223104;
        public const double PDD_DEFAULT_CAL1_PB_SCALE_K0 = -29.4934544;
        // CX^2 
        public const double PDD_DEFAULT_CAL2_PB_SCALE_K4 = 0.00000135004;
        public const double PDD_DEFAULT_CAL2_PB_SCALE_K3 = -0.000571452;
        public const double PDD_DEFAULT_CAL2_PB_SCALE_K2 = 0.081358873;
        public const double PDD_DEFAULT_CAL2_PB_SCALE_K1 = -4.46554539;
        public const double PDD_DEFAULT_CAL2_PB_SCALE_K0 = 83.88685207;
        // DX 
        public const double PDD_DEFAULT_CAL3_PB_SCALE_K4 = -0.000000389211;
        public const double PDD_DEFAULT_CAL3_PB_SCALE_K3 = 0.000318549;
        public const double PDD_DEFAULT_CAL3_PB_SCALE_K2 = -0.062348453;
        public const double PDD_DEFAULT_CAL3_PB_SCALE_K1 = 3.521307252;
        public const double PDD_DEFAULT_CAL3_PB_SCALE_K0 = 3564.807668;
        // E 
        public const double PDD_DEFAULT_CAL4_PB_SCALE_K4 = 0.000000492515;
        public const double PDD_DEFAULT_CAL4_PB_SCALE_K3 = -0.000202293;
        public const double PDD_DEFAULT_CAL4_PB_SCALE_K2 = 0.029174242;
        public const double PDD_DEFAULT_CAL4_PB_SCALE_K1 = -1.500445382;
        public const double PDD_DEFAULT_CAL4_PB_SCALE_K0 = -1759.366014;

        // Memory
        public const int PDD_MEMORY_CHIPS = 8;
        public const int PDD_MEMORY_BLOCKS_PER_CHIP = 128;
        public const int PDD_MEMORY_PAGES_PER_CHIP = 32768;
        public const int PDD_MEMORY_PAGES_PER_BLOCK =256;
        public const int PDD_MEMORY_PAGE_SIZE = 256;
        public const int PDD_MEMORY_PARAGRAPH_SIZE = 256;
        public const int PDD_MEMORY_START_PAGE = 32;

        // Memory sync
        public const UInt16 PDD_MEMORY_PAGE_HEADER = 0xABAB;
        public const UInt16 PDD_MEMORY_RECORD_SYNC_HEADER = 0xCDCD;

        // Memory record IDs
        public const int PDD_MEMORY_RECORD_TIMESTAMP = 1;
        public const int PDD_MEMORY_RECORD_PWR_DATA_LOGGING = 2;
        public const int PDD_MEMORY_RECORD_PDD_DATA_LOGGING = 3;

        // Pressure Transducers - Custom Sensor Design p/n 52434-200
        // Full scale 2.5mV/V x 5V supply = 12.5mV @20Kpsi
        // Add a little headroom in case of positive offset... say 13.75mV @22Kpsi.
        // Analog range is from 1250mVoffset to 4000mVmax (2750mVdiff).
        // Gain = 2750mV/13.75mV = 200.
        // Actual gain is 1 +(100K/Rg) = 201.4, Rg = 499.
        // Actual range is (2750mV /201.4) *(20Kpsi/12.5mV) = 21,847psi.
        public const double PDD_AMPLIFIER_GAIN = 201.4;
        public const double PDD_PRESSURE_OFFSET_MILLIVOLTS = 1250.0;
        public const double PDD_PRESSURE_PSI_PER_MILLIVOLT = (20000.0 / (2.5 * 5.0 * PDD_AMPLIFIER_GAIN));

        enum PDD_STATE_MACHINE
        {
            PDD_STATE_IDLE = 0,
            PDD_STATE_START,
            PDD_MCP3551_POWER_UP_DELAY,
            PDD_MCP3551_CHIP_SELECT_DELAY,
            PDD_MCP3551_WAIT_CONVERSION_COMPLETE,
            PDD_MCP3551_READ_TEMPERATURE_SENSOR,
            PDD_MCP3551_CHIP_SELECT_ANNULAR_PRESSURE,
            PDD_MCP3551_READ_ANNULAR_PRESSURE,
            PDD_MCP3551_CHIP_SELECT_BORE_PRESSURE,
            PDD_MCP3551_READ_BORE_PRESSURE,
            PDD_MCP3551_POWER_OFF,
        };
        #endregion

        #region MFPWR V2.02
        //
        public const bool MFPWR_SELECT = true;
        public const double NULL = -999.25;

        // Telemetry Error Status bits
        // phase
        public const uint MFPWR_ERROR_CODE_RF2P_UNDER = 0x00000001;
        public const uint MFPWR_ERROR_CODE_RF2P_OVER = 0x00000002;
        public const uint MFPWR_ERROR_CODE_RF2P_NODATA = 0x00000004;
        public const uint MFPWR_ERROR_CODE_RN2P_UNDER = 0x00000008;
        public const uint MFPWR_ERROR_CODE_RN2P_OVER = 0x00000010;
        public const uint MFPWR_ERROR_CODE_RN2P_NODATA = 0x00000020;
        public const uint MFPWR_ERROR_CODE_RF4P_UNDER = 0x00000040;
        public const uint MFPWR_ERROR_CODE_RF4P_OVER = 0x00000080;
        public const uint MFPWR_ERROR_CODE_RF4P_NODATA = 0x00000100;
        public const uint MFPWR_ERROR_CODE_RN4P_UNDER = 0x00000200;
        public const uint MFPWR_ERROR_CODE_RN4P_OVER = 0x00000400;
        public const uint MFPWR_ERROR_CODE_RN4P_NODATA = 0x00000800;
        // atten
        public const uint MFPWR_ERROR_CODE_RF2A_UNDER = 0x00001000;
        public const uint MFPWR_ERROR_CODE_RF2A_OVER = 0x00002000;
        public const uint MFPWR_ERROR_CODE_RF2A_NODATA = 0x00004000;
        public const uint MFPWR_ERROR_CODE_RN2A_UNDER = 0x00008000;
        public const uint MFPWR_ERROR_CODE_RN2A_OVER = 0x00010000;
        public const uint MFPWR_ERROR_CODE_RN2A_NODATA = 0x00020000;
        public const uint MFPWR_ERROR_CODE_RF4A_UNDER = 0x00040000;
        public const uint MFPWR_ERROR_CODE_RF4A_OVER = 0x00080000;
        public const uint MFPWR_ERROR_CODE_RF4A_NODATA = 0x00100000;
        public const uint MFPWR_ERROR_CODE_RN4A_UNDER = 0x00200000;
        public const uint MFPWR_ERROR_CODE_RN4A_OVER = 0x00400000;
        public const uint MFPWR_ERROR_CODE_RN4A_NODATA = 0x00800000;

        public const uint MFPWR_ERROR_CODE_BIT24 = 0x01000000;
        public const uint MFPWR_ERROR_CODE_BIT25 = 0x02000000;
        public const uint MFPWR_ERROR_CODE_BIT26 = 0x04000000;
        public const uint MFPWR_ERROR_CODE_BIT27 = 0x08000000;
        public const uint MFPWR_ERROR_CODE_BIT28 = 0x10000000;
        public const uint MFPWR_ERROR_CODE_BIT29 = 0x20000000;
        public const uint MFPWR_ERROR_CODE_BIT30 = 0x40000000;
        public const uint MFPWR_ERROR_CODE_BIT31 = 0x80000000;

        //Action Type
        public const int MFPWR_ACTION_NONE = 0;
        public const int MFPWR_ACTION_TEST_WRITE_MEMPAGE = 1;
        public const int MFPWR_ACTION_WRITE_ADC_SAMPLES = 2;
        public const int MFPWR_ACTION_READ_UPPER_ADC = 3;
        public const int MFPWR_ACTION_READ_LOWER_ADC = 4;
        public const int MFPWR_ACTION_SET_RING_DOWN = 5;
        public const int MFPWR_ACTION_READ_DAP_VER = 6;
        public const int MFPWR_ACTION_TEST_MEM_START_PAGE = 7;
        public const int MFPWR_ACTION_SET_DIAGNOSTICS = 8;
        public const int MFPWR_ACTION_PROGRAM_DAP = 9;
        public const int MFPWR_ACTION_TX_NEARS_400KHZ = 10;
        public const int MFPWR_ACTION_TX_NEARS_2MHZ = 11;
        public const int MFPWR_ACTION_TX_FARS_400KHZ = 12;
        public const int MFPWR_ACTION_TX_FARS_2MHZ = 13;
        public const int MFPWR_ACTION_FORCE_FLOW = 14;

        //Datagroup Type 
        public const byte MFPWR_DATAGROUP_1 = 0x01;
        public const byte MFPWR_DATAGROUP_2 = 0x02;
        public const byte MFPWR_DATAGROUP_3 = 0x03;
        public const byte MFPWR_DATAGROUP_4 = 0x04;
        public const byte MFPWR_DATAGROUP_5 = 0x05;
        public const byte MFPWR_DATAGROUP_6 = 0x06;
        public const byte MFPWR_DATAGROUP_7 = 0x07;
        public const byte MFPWR_DATAGROUP_8 = 0x08;
        public const byte MFPWR_DATAGROUP_9 = 0x09;
        public const byte MFPWR_DATAGROUP_10 = 0x0A;
        public const byte MFPWR_DATAGROUP_11 = 0x0B;
        public const byte MFPWR_DATAGROUP_12 = 0x0C;
        public const byte MFPWR_DATAGROUP_13 = 0x0D;
        public const byte MFPWR_DATAGROUP_14 = 0x0E;
        public const byte MFPWR_DATAGROUP_15 = 0x0F;
        public const byte MFPWR_DATAGROUP_16 = 0x10;
        public const byte MFPWR_DATAGROUP_17 = 0x11;

        // NonVolatile types
        public const byte MFPWR_NONVOLATILE_CONFIGURATION = 0x01;
        public const byte MFPWR_NONVOLATILE_CALIBRATION = 0x02;
        public const byte MFPWR_NONVOLATILE_SERVICE_RECORD = 0x03;

        // Generic variable assignments
        public enum MFPWR_GV { MFPWR_GV_DISABLED = 0, MFPWR_GV_ZN4KA, MFPWR_GV_ZN4KP, MFPWR_GV_ZN2MA, MFPWR_GV_ZN2MP, MFPWR_GV_ZF4KA, MFPWR_GV_ZF4KP, MFPWR_GV_ZF2MA, MFPWR_GV_ZF2MP };

        //RG INSERT RMS CODE
        // Default configuration
        public const UInt16 MFPWR_DEFAULT_CONFIG_TRANSMITTER_WARMUP_TIME = 100;
        public const UInt16 MFPWR_DEFAULT_CONFIG_TRANSMITTER_SWITH_DELAY = 200;
        public const UInt16 MFPWR_DEFAULT_CONFIG_RECEIVER_MEASUREMENT_TIME = 1250;
        public const UInt16 MFPWR_DEFAULT_CONFIG_AUTOMATIC_ACQUISITION_UPDATE_TIME = 20000;
        public const float MFPWR_DEFAULT_CONFIG_QBUS_ADDRESS = 25;
        public const float MFPWR_DEFAULT_CONFIG_SURVEY_DELAY_SECONDS = 300;
        public const double MFPWR_DEFAULT_CONFIG_BATTERY_VOLTS_SCALING = 0.059135;     // Scaling 59.135mV per lsb
        public const double MFPWR_DEFAULT_CONFIG_BATTERY_CURRENT_SCALING = 0.00015;     // Scaling 15mA per lsb
        public const double MFPWR_DEFAULT_CONFIG_BOARD_TEMP_SCALING = 1.0;         // Board Temperature scale factor
        public const double MFPWR_DEFAULT_CONFIG_BOARD_TEMP_OFFSET = 0.0;         // Board Temperature scale factor

        public const int MFPWR_DEFAULT_CONFIG_NOT_USED = (int)MFPWR_GV.MFPWR_GV_DISABLED;
        public const int MFPWR_DEFAULT_CONFIG_NEAR_2MHZ_PHASE = (int)MFPWR_GV.MFPWR_GV_ZN2MP;
        public const int MFPWR_DEFAULT_CONFIG_FAR_400KHZ_ATTENUATION = (int)MFPWR_GV.MFPWR_GV_ZF4KA;
        public const int MFPWR_DEFAULT_CONFIG_FAR_400KHZ_PHASE = (int)MFPWR_GV.MFPWR_GV_ZF4KP;
        public const int MFPWR_DEFAULT_CONFIG_NEAR_400KHZ_ATTENUATION = (int)MFPWR_GV.MFPWR_GV_ZN4KA;
        public const int MFPWR_DEFAULT_CONFIG_NEAR_400KHZ_PHASE = (int)MFPWR_GV.MFPWR_GV_ZN4KP;
        public const int MFPWR_DEFAULT_CONFIG_NEAR_2MHZ_ATTENUATION = (int)MFPWR_GV.MFPWR_GV_ZN2MA;
        public const int MFPWR_DEFAULT_CONFIG_FAR_2MHZ_PHASE = (int)MFPWR_GV.MFPWR_GV_ZF2MP;
        public const int MFPWR_DEFAULT_CONFIG_FAR_2MHZ_ATTENUATION = (int)MFPWR_GV.MFPWR_GV_ZF2MA;

        public const int MFPWR_DEFAULT_CONFIG_GV0_ASSIGNMENT = (int)MFPWR_GV.MFPWR_GV_DISABLED;
        public const int MFPWR_DEFAULT_CONFIG_GV1_ASSIGNMENT = (int)MFPWR_GV.MFPWR_GV_DISABLED;
        public const int MFPWR_DEFAULT_CONFIG_GV2_ASSIGNMENT = (int)MFPWR_GV.MFPWR_GV_DISABLED;
        public const int MFPWR_DEFAULT_CONFIG_GV3_ASSIGNMENT = (int)MFPWR_GV.MFPWR_GV_ZN2MP;
        public const int MFPWR_DEFAULT_CONFIG_GV4_ASSIGNMENT = (int)MFPWR_GV.MFPWR_GV_ZF4KA;
        public const int MFPWR_DEFAULT_CONFIG_GV5_ASSIGNMENT = (int)MFPWR_GV.MFPWR_GV_ZF4KP;
        public const int MFPWR_DEFAULT_CONFIG_GV6_ASSIGNMENT = (int)MFPWR_GV.MFPWR_GV_DISABLED;
        public const int MFPWR_DEFAULT_CONFIG_GV7_ASSIGNMENT = (int)MFPWR_GV.MFPWR_GV_DISABLED;

        //Default Calibration
        public const double MFPWR__DEFAULT_CAL_ZF2MA_K0 = 133.45;
        public const double MFPWR__DEFAULT_CAL_ZF2MA_K1 = 4.8844;
        public const double MFPWR__DEFAULT_CAL_ZF2MA_K2 = 0.0718;
        public const double MFPWR__DEFAULT_CAL_ZF2MA_K3 = 0.0004;
        public const double MFPWR__DEFAULT_CAL_ZF2MA_K4 = 0.0004;
        public const double MFPWR__DEFAULT_CAL_ZF2MP_K0 = 133.45;
        public const double MFPWR__DEFAULT_CAL_ZF2MP_K1 = 4.8844;
        public const double MFPWR__DEFAULT_CAL_ZF2MP_K2 = 0.0718;
        public const double MFPWR__DEFAULT_CAL_ZF2MP_K3 = 0.0004;
        public const double MFPWR__DEFAULT_CAL_ZF2MP_K4 = 0.0004;

        public const double MFPWR__DEFAULT_CAL_ZN2MA_K0 = 133.45;
        public const double MFPWR__DEFAULT_CAL_ZN2MA_K1 = 4.8844;
        public const double MFPWR__DEFAULT_CAL_ZN2MA_K2 = 0.0718;
        public const double MFPWR__DEFAULT_CAL_ZN2MA_K3 = 0.0004;
        public const double MFPWR__DEFAULT_CAL_ZN2MA_K4 = 0.0004;
        public const double MFPWR__DEFAULT_CAL_ZN2MP_K0 = 133.45;
        public const double MFPWR__DEFAULT_CAL_ZN2MP_K1 = 4.8844;
        public const double MFPWR__DEFAULT_CAL_ZN2MP_K2 = 0.0718;
        public const double MFPWR__DEFAULT_CAL_ZN2MP_K3 = 0.0004;
        public const double MFPWR__DEFAULT_CAL_ZN2MP_K4 = 0.0004;

        public const double MFPWR__DEFAULT_CAL_ZF4KA_K0 = 133.45;
        public const double MFPWR__DEFAULT_CAL_ZF4KA_K1 = 4.8844;
        public const double MFPWR__DEFAULT_CAL_ZF4KA_K2 = 0.0718;
        public const double MFPWR__DEFAULT_CAL_ZF4KA_K3 = 0.0004;
        public const double MFPWR__DEFAULT_CAL_ZF4KA_K4 = 0.0004;

        public const double MFPWR__DEFAULT_CAL_ZF4KP_K0 = 133.45;
        public const double MFPWR__DEFAULT_CAL_ZF4KP_K1 = 4.8844;
        public const double MFPWR__DEFAULT_CAL_ZF4KP_K2 = 0.0718;
        public const double MFPWR__DEFAULT_CAL_ZF4KP_K3 = 0.0004;
        public const double MFPWR__DEFAULT_CAL_ZF4KP_K4 = 0.0004;

        public const double MFPWR__DEFAULT_CAL_ZN4KA_K0 = 133.45;
        public const double MFPWR__DEFAULT_CAL_ZN4KA_K1 = 4.8844;
        public const double MFPWR__DEFAULT_CAL_ZN4KA_K2 = 0.0718;
        public const double MFPWR__DEFAULT_CAL_ZN4KA_K3 = 0.0004;
        public const double MFPWR__DEFAULT_CAL_ZN4KA_K4 = 0.0004;
        public const double MFPWR__DEFAULT_CAL_ZN4KP_K0 = 133.45;
        public const double MFPWR__DEFAULT_CAL_ZN4KP_K1 = 4.8844;
        public const double MFPWR__DEFAULT_CAL_ZN4KP_K2 = 0.0718;
        public const double MFPWR__DEFAULT_CAL_ZN4KP_K3 = 0.0004;
        public const double MFPWR__DEFAULT_CAL_ZN4KP_K4 = 0.0004;

        // Memory
        public const int MFPWR_MEMORY_CHIPS = 8;
        public const int MFPWR_MEMORY_BLOCKS_PER_CHIP = 128;
        public const int MFPWR_MEMORY_PAGES_PER_CHIP = 32768;
        public const int MFPWR_MEMORY_PAGES_PER_BLOCK = 256;
        public const int MFPWR_MEMORY_PAGE_SIZE  = 256;
        public const int MFPWR_MEMORY_PARAGRAPH_SIZE = 256;
        public const int MFPWR_MEMORY_START_PAGE = 32;

        // Memory sync
        public const UInt16 MFPWR_MEMORY_PAGE_HEADER = 0xABAB;
        public const UInt16 MFPWR_MEMORY_RECORD_SYNC_HEADER= 0xCDCD;

        // Memory record IDs
        public const int MFPWR_MEMORY_RECORD_TIMESTAMP = 1;
        public const int MFPWR_MEMORY_RECORD_PWR_DATA_LOGGING = 2;
        public const int MFPWR_MEMORY_RECORD_MFPWR_DATA_LOGGING = 3;
        public const int MFPWR_MEMORY_RECORD_DIAGNOSTICS_MODE0_LOGGING = 4;
        public const int MFPWR_MEMORY_RECORD_DIAGNOSTICS_MODE1_LOGGING = 5;
        public const int MFPWR_MEMORY_RECORD_DIAGNOSTICS_MODE2_LOGGING = 6;
        public const int MFPWR_MEMORY_RECORD_DIAGNOSTICS_MODE3_LOGGING = 7;
        public const int MFPWR_MEMORY_RECORD_RING_DOWN_LOGGING = 8;
        public const int MFPWR_MEMORY_RECORD_DIAGNOSTICS_LOGGING = 9;

        // Misc defines
        public const double PHASE_SHIFT_2M_MIN = -10.0;     //In the future we could lower the min phase shift from -10(deg) to -5(deg)
        public const double PHASE_SHIFT_2M_MAX = 120.0;
        public const double PHASE_SHIFT_400K_MIN = -10.0;  //In the future we could lower the min phase shift from -10(deg) to -5(deg)
        public const double PHASE_SHIFT_400K_MAX = 60.0;

        public const double ATTEN_MIN = 0.0;
        public const double ATTEN_MAX = 2.5;
        #endregion

        #region MWD V15.50
        // Action types
        public const int MWD_MASTER_ACTION_NONE = 0;
        public const int MWD_MASTER_ACTION_SET_BAT2 = 1;
        public const int MWD_MASTER_ACTION_SET_PULSE_OUT = 2;
        public const int MWD_MASTER_ACTION_TAKE_SURVEY = 3;
        public const int MWD_MASTER_ACTION_READ_EEPROM = 4;
        public const int MWD_MASTER_ACTION_SIM_DOWNLINK = 5;
        public const int MWD_MASTER_ACTION_MFPWR_ENCODE = 6;

        // NonVolatile types
        public const int MWD_MASTER_NONVOLATILE_TFO = 1;
        public const int MWD_MASTER_NONVOLATILE_CONFIGURATION = 2;
        public const int MWD_MASTER_NONVOLATILE_TELEMETRY0 = 3;
        public const int MWD_MASTER_NONVOLATILE_TELEMETRY1 = 4;
        public const int MWD_MASTER_NONVOLATILE_TELEMETRY2 = 5;
        public const int MWD_MASTER_NONVOLATILE_TELEMETRY3 = 6;
        public const int MWD_MASTER_NONVOLATILE_TELEMETRY4 = 7;
        public const int MWD_MASTER_NONVOLATILE_TELEMETRY5 = 8;
        public const int MWD_MASTER_NONVOLATILE_TELEMETRY6 = 9;
        public const int MWD_MASTER_NONVOLATILE_TELEMETRY7 = 10;
        #endregion

        #region UVT V0.53
        // Status bits
        public const UInt16 UVT_STATUS_ACTIVE = 0x0001;
        public const UInt16 UVT_STATUS_FLOW_ON = 0x0002;
        public const UInt16 UVT_STATUS_VBUSX_ON = 0x0004;
        public const UInt16 UVT_STATUS_APS_SYNC = 0x0008;
        public const UInt16 UVT_STATUS_APS_INIT_ERR = 0x0010;
        public const UInt16 UVT_STATUS_MEM_SCAN = 0x0020;
        public const UInt16 UVT_STATUS_MEM_ERASE = 0x0040;
        public const UInt16 UVT_STATUS_MEM_FULL = 0x0080;
        public const UInt16 UVT_STATUS_TIME_NOINIT = 0x0100;
        public const UInt16 UVT_STATUS_ROLLTEST = 0x0200;
        public const UInt16 UVT_STATUS_BIT10 = 0x0400;
        public const UInt16 UVT_STATUS_JOB_RECORD_ERR = 0x0800;
        public const UInt16 UVT_STATUS_SERIALNUMBER_ERR = 0x1000;
        public const UInt16 UVT_STATUS_BIT13 = 0x2000;
        public const UInt16 UVT_STATUS_CONFIG_ERR = 0x4000;
        public const UInt16 UVT_STATUS_SYSTEMRECORD_ERR = 0x8000;

        // Action types
        public const int UVT_ACTION_NONE = 0;
        public const int UVT_ACTION_SET_VBUSX_ENABLE = 1;
        public const int UVT_ACTION_SET_STATEMACHINE_ENABLE = 2;
        public const int UVT_ACTION_FORCE_APS_SYNC = 3;
        public const int UVT_ACTION_CAPTURE_SURVEY = 4;
        public const int UVT_ACTION_SET_TEST_CODE = 5;

        // Datagroup types
        public const byte UVT_DATAGROUP_1 = 0x01;
        public const byte UVT_DATAGROUP_2 = 0x02;
        public const byte UVT_DATAGROUP_3 = 0x03;
        public const byte UVT_DATAGROUP_4 = 0x04;
        public const byte UVT_DATAGROUP_5 = 0x05;
        public const byte UVT_DATAGROUP_6 = 0x06;
        public const byte UVT_DATAGROUP_7 = 0x07;
        public const byte UVT_DATAGROUP_8 = 0x08;
        public const byte UVT_DATAGROUP_9 = 0x09;
        public const byte UVT_DATAGROUP_10 = 0x0A;
        public const byte UVT_DATAGROUP_11 = 0x0B;
        public const byte UVT_DATAGROUP_12 = 0x0C;
        public const byte UVT_DATAGROUP_13 = 0x0D;
        public const byte UVT_DATAGROUP_14 = 0x0E;
        public const byte UVT_DATAGROUP_15 = 0x0F;
        public const byte UVT_DATAGROUP_16 = 0x10;
        public const byte UVT_DATAGROUP_17 = 0x11;
        #endregion

        #region Program global variables

        // ******************* Main program ********************
        public static List<byte> firmwareID = new List<byte>();
        public static string serialNumber = "";

        // **************** Program Config Form ****************
        public static byte nodeID = 0x00;
        public static int baudRate = 0;
        public static int dataBits = 0;

        #endregion global variables

        #endregion Properties

        #region Methods  ---------------------------------------------------------------------------------------------------
        /// <summary>
        /// Read XML config file
        /// </summary>
        public static void ReadConfig()
        {
            // Check for a config.ini file and if it doesn't exist create one
            if (!File.Exists(CONFIG_FILENAME))      // NOTE: If the SDconfig.xml file doesn't exist it will be created with default values
            {
                Defaults();         // Pre-Set all lists to program default values         
                WriteConfig();      // And then save them to the new SDconfig.xml file
            }

            ClearAllLists();        // Must clear static lists before reading in config file 11-6-13 RP  

            try
            {
                XmlTextReader reader = new XmlTextReader(CONFIG_FILENAME);
                while (reader.Read())
                {
                    try
                    {
                        if (reader.IsStartElement())
                        {
                            switch (reader.Name)
                            {
                                #region Program Config Parameters
                                // Serial Port
                                case "Baud":
                                    baudRate = Convert.ToInt16(reader.ReadString());
                                    break;
                                case "Bits":
                                    dataBits = Convert.ToInt16(reader.ReadString());
                                    break;
                                    #endregion
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.Write("List update error reading configuration file ("
                            + CONFIG_FILENAME + ")\n"
                            + ex.Message + "\n"
                            + ex.InnerException + "\n"
                            + "Config file may be corrupt. Exit program and contact support.\r\n",
                            "Error: XML file read");
                        return;
                    }
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                Console.Write("SdConfig file reader error ("
                    + CONFIG_FILENAME + ")\n"
                    + ex.Message + "\n"
                    + ex.InnerException + "\n"
                    + "Config file may be corrupt. Exit program and contact support.\r\n",
                    "Error: XML file read");
                return;
            }
        }
        /// <summary>
        /// Write XML config file
        /// </summary>
        public static void WriteConfig()
        {
            XmlTextWriter writer = null;
            writer = new XmlTextWriter(CONFIG_FILENAME, Encoding.UTF8);
            writer.Formatting = Formatting.Indented;
            writer.Indentation = 4;

            // Root
            writer.WriteStartDocument();
            writer.WriteStartElement("Config");

            // Configuration data
            try
            {
                #region Program Config data
                // Serial Port
                writer.WriteStartElement("ComPort");
                {
                    writer.WriteElementString("Baud", baudRate.ToString());
                    writer.WriteElementString("Bits", dataBits.ToString());
                }
                writer.WriteEndElement();
                #endregion
            }
            catch (Exception ex)
            {
                Console.Write("Error: WriteConfig()\r\n" + ex.Message, "Error");
                return;
            }

            writer.WriteEndDocument();
            writer.Close();
        }
        /// <summary>
        /// Pre-Set Default values 
        /// </summary>
        private static void Defaults()
        {
            nodeID = 0x03;  // MwdMaster

            // Serial Port
            baudRate = 9600;
            dataBits = 8;

            ClearAllLists();
        }
        /// <summary>
        /// Clear all static list
        /// </summary>
        private static void ClearAllLists()
        {
            // Example List
            firmwareID.Clear();
        }
        # endregion Methods
    }
}